﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindSlider();
            BindProductsBrands();
        }
    }

    [WebMethod]
    public static string GetBrand()
    {
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        List<Brand> lst = new List<Brand>();
        lst = new BrandBLL().GetAll();
        var Json = new
        {

            ListOption = lst

        };

        return ser.Serialize(Json);

    }

    void BindSlider()
    {
        int flag = 0, count = 0; ;
        List<Sliders> sliders = new SlidersBLL().GetAll().Where(x => x.IsActive == true).ToList();
        string strSliders = "", ltrTargetSlider = "";
        foreach (var i in sliders)
        {
            if (i.IsActive == true)
            {
                if (flag == 0)
                {
                    strSliders += "<div class='item active'><a href='" + i.UrlName.Trim() + "'><img src = 'Images/Slider/home/" + i.ImageUrl + "' /></a><div class='carousel-caption'></div></div>";
                    ltrTargetSlider += "<li data-target='#carousel-example-generic' data-slide-to=" + count + " class='active'></li>";
                    flag = 1;
                }
                else
                {
                    ltrTargetSlider += "<li data-target='#carousel-example-generic' data-slide-to=" + count + "s></li>";
                    strSliders += "<div class='item '><a href='" + i.UrlName.Trim() + "'><img src = 'Images/Slider/home/" + i.ImageUrl + "' /></a><div class='carousel-caption'></div></div>";
                }
                count++;
            }
        }
        ltSlider.Text = strSliders;
        ltslidertarget.Text = ltrTargetSlider;
    }



    void BindProductsBrands()
    {

        List<Products> ListProducts = new ProductsBLL().GetAllHomeProducts();

        var lst_bestseller = ListProducts.Where(x => x.type_id == 1);

        foreach (var item in lst_bestseller)
        {
            string ButtonType = "";
            decimal Price =Math.Round(Convert.ToDecimal(item.Price));
            decimal Mrp = Math.Round(Convert.ToDecimal(item.MRP));
            decimal Discount_per = Math.Round(item.Discount_per);

            string OfferHtml = "";
            string WasPrice = "";
            if (Mrp > Price)
            {
                OfferHtml = "<div style='position: absolute; height: 70px; width: 66px; background: url(&quot;images/offerbg.png&quot;) no-repeat scroll 0px 0px / 100% auto transparent;'>";
                OfferHtml += "<div style='color:white;margin-right:20px;font-weight:bold;font-size:10px'><b style='font-size:20px;'>" + Discount_per + "</b>%</div></div>";
                WasPrice = "<p class='wasnow_home'><span class='was'>WAS &#8377;" + Mrp + "</span><span class='now'>NOW &#8377;" + Price + "</span></p>";
            }
            if (Mrp == Price)
            {

                WasPrice = "<span  class='productBlock_priceValue' content=" + Price + ">&#8377;" + Price + "</span>";
            }
            if (item.VCount == 1 && item.attr_type_id == 5)
            {
                ButtonType = "<a>" +
"<div class='productBlockButtonLink trackwidget'>" +
"<span id = 'a_" + item.VariationId + "' attr_id=" + item.attr_id + " class='productBlock_button productBlock_button-buyNow btn-shop-now btnAddToCart'>ADD TO CART</span>" +
 "</div></a>";
            }
            else
            {
                ButtonType = "<a href ='productdetail.aspx?p=" + item.ProductId + "&v=" + item.VariationId + "'>" +
                                "<div class='productBlockButtonLink trackwidget'>" +
                                 "<span id = '" + item.VariationId + "' class='productBlock_button productBlock_button-buyNow btn-shop-now'>ADD TO CART</span></div></a>";
            }

            ltr_bestseller.Text += "<div class='item'>" +
                                         "<div class='pad15 best-seller'>" +
                                            "<a href = 'productdetail.aspx?p=" + item.ProductId + "&v=" + item.VariationId + "'>" +
                                            "" + OfferHtml + "<img class='medium-img img-hvr second_img crouser_img' src='images/product/" + item.PhotoUrl + "' firstimg='images/product/" + item.PhotoUrl + "' secondimg='images/product/" + item.second_photourl + "'/>" +
                                                "</a>" +
                                                "<h3 class='productBlock_titleContainer trackwidget title-height' style='text-align: left;'>" +
											"<a class='threeItemEditorial_itemTitle best-seller-title' href = 'productdetail.aspx?p=" + item.ProductId + "&v=" + item.VariationId + "'>" + item.Name + "</a>" +
                                        "</h3>" +

                                            "<p class='desc-p best-seller-desc'>" + item.ShortDescription + "</p>" +
                                                "<div class='productBlock_priceBlock' style='text-align: left;'>" +
                                            "<div class='productBlock_price'>" +
                                                "<span class='productBlock_priceCurrency' content=''></span>" +
                                                //"<span class='productBlock_priceValue' content='£145.00'>₹" + item.Price + "</span>" +
                                                WasPrice+
                                            "</div>" +
                                        "</div>" + ButtonType + "</div></div>";
        }
        //rpt_bestseller.DataSource = ListProducts.Where(x => x.type_id == 1);
        //rpt_bestseller.DataBind();

        var lst_discoversmthnew = ListProducts.Where(x => x.type_id == 2);

        foreach (var item in lst_discoversmthnew)
        {
            string ButtonType_bs = "";
            decimal Price = Math.Round(Convert.ToDecimal(item.Price));
            decimal Mrp = Math.Round(Convert.ToDecimal(item.MRP));
            decimal Discount_per = Math.Round(item.Discount_per);

            string OfferHtml = "";
            string WasPrice = "";
            if (Mrp > Price)
            {
                OfferHtml = "<div style='position: absolute; height: 70px; width: 66px; background: url(&quot;images/offerbg.png&quot;) no-repeat scroll 0px 0px / 100% auto transparent;'>";
                OfferHtml += "<div style='color:white;margin-right:20px;font-weight:bold;font-size:10px'><b style='font-size:20px;'>" + Discount_per + "</b>%</div></div>";
                WasPrice = "<p class='wasnow_home'><span class='was'>WAS &#8377;" + Mrp + "</span><span class='now'>NOW &#8377;" + Price + "</span></p>";
            }
            if (Mrp == Price)
            {

                WasPrice = "<span  class='productBlock_priceValue' content=" + Price + ">&#8377;" + Price + "</span>";
            }
            if (item.VCount == 1 && item.attr_type_id == 5)
            {
                ButtonType_bs = "<a>" +
"<div class='productBlockButtonLink trackwidget'>" +
"<span id = 'a_" + item.VariationId + "' attr_id=" + item.attr_id + " class='productBlock_button productBlock_button-buyNow btn-shop-now btnAddToCart'>ADD TO CART</span>" +
                            "</div></a>";
            }
            else
            {
                ButtonType_bs = "<a href ='productdetail.aspx?p=" + item.ProductId + "&v=" + item.VariationId + "'>" +
                                "<div class='productBlockButtonLink trackwidget'>" +
                                 "<span id = '" + item.VariationId + "' class='productBlock_button productBlock_button-buyNow btn-shop-now'>ADD TO CART</span></div></a>";
            }

            ltr_discoversmthnew.Text += "</a>  <div class='item'>" +
                                        "<div class='pad15'>" +
                                                "<a href ='productdetail.aspx?p=" + item.ProductId + "&v=" + item.VariationId + "'>" +
                                            ""+ OfferHtml + "<img class='medium-img img-hvr second_img crouser_img' src='images/product/" + item.PhotoUrl + "' firstimg='images/product/" + item.PhotoUrl + "' secondimg='images/product/" + item.second_photourl + "'/>" +
                                                    "</a>" +
                                            "<div class='productBlock_detailsContainer discover-product '>" +

                                        "<h3 class='productBlock_titleContainer trackwidget title-height' >" +
											"<a class='threeItemEditorial_itemTitle dicover-title' href='productdetail.aspx?p=" + item.ProductId + "&v=" + item.VariationId + "'>" + item.Name + "</a>" +
                                        "</h3>" +
                                            "<p class='desc-p discover-desc'>" + item.ShortDescription + "</p>" +


                                        "<div class='productBlock_priceBlock'>" +
                                            "<div class='productBlock_price'>" +
                                                "<span class='productBlock_priceCurrency' content=''></span>" +
                                                WasPrice+
                                                //"<span class='productBlock_priceValue' content='£145.00'>₹" + item.Price + "</span>" +
                                            "</div></div>" + ButtonType_bs +




                                                    "</div></div></div>";
        }
        //rpt_discoversmthnew.DataSource = ListProducts.Where(x => x.type_id == 2);
        //rpt_discoversmthnew.DataBind();
        var lst_ebsartcmpny = ListProducts.Where(x => x.type_id == 4 && x.Type == "S");

        foreach (var item in lst_ebsartcmpny)
        {
            string ButtonType_ec = "";
            decimal Price = Math.Round(Convert.ToDecimal(item.Price));
            decimal Mrp = Math.Round(Convert.ToDecimal(item.MRP));
            decimal Discount_per = Math.Round(item.Discount_per);

            string OfferHtml = "";
            string WasPrice = "";
            if (Mrp > Price)
            {
                OfferHtml = "<div style='position: absolute; height: 70px; width: 66px; background: url(&quot;images/offerbg.png&quot;) no-repeat scroll 0px 0px / 100% auto transparent;'>";
                OfferHtml += "<div style='color:white;margin-right:20px;font-weight:bold;font-size:10px'><b style='font-size:20px;'>" + Discount_per + "</b>%</div></div>";
                WasPrice = "<p class='wasnow_home'><span class='was'>WAS &#8377;" + Mrp + "</span><span class='now'>NOW &#8377;" + Price + "</span></p>";
            }
            if (Mrp == Price)
            {

                WasPrice = "<span  class='productBlock_priceValue' content=" + Price + ">&#8377;" + Price + "</span>";
            }
            if (item.VCount == 1 && item.attr_type_id == 5)
            {
                ButtonType_ec = "<a>" +
"<div class='productBlockButtonLink trackwidget'>" +
"<span id = 'a_" + item.VariationId + "' attr_id=" + item.attr_id + " class='productBlock_button productBlock_button-buyNow btn-shop-now btnAddToCart'>ADD TO CART</span>" +
                            "</div></a>";
            }
            else
            {
                ButtonType_ec = "<a href ='productdetail.aspx?p=" + item.ProductId + "&v=" + item.VariationId + "'>" +
                                "<div class='productBlockButtonLink trackwidget'>" +
                                 "<span id = '" + item.VariationId + "' class='productBlock_button productBlock_button-buyNow btn-shop-now'>ADD TO CART</span></div></a>";
            }


            ltr_ebsartcmpny.Text += "<div class='item'>" +
                                                "<div class='pad15'>" +
                                                        "<a href ='productdetail.aspx?p=" + item.ProductId + "&v=" + item.VariationId + "'>" +
                                                        ""+ OfferHtml + "<img class='medium-img img-hvr second_img' src='images/product/" + item.PhotoUrl + "' firstimg='images/product/" + item.PhotoUrl + "' secondimg='images/product/" + item.second_photourl + "'/></a>" +
                                                    "<p class='threeItemEditorial_itemTitle art-title'>" + item.Name + "</p>" +
                                                    "<p class='desc-p art-section-p'>" + item.ShortDescription + "</p>" +
                                                    "<div class='productBlock_priceBlock' data-is-link='true'>" +
                                                        "<div class='productBlock_price'>" +
                                                            "<span class='productBlock_priceCurrency' content='GBP'></span>" +
                                                            WasPrice+
                                                            //"<span class='productBlock_priceValue' content='£25.00'>&#8377;" + item.Price + "</span>" +
                                                        "</div>" + ButtonType_ec + "</div>" +

                                                "</div>" +
                                            "</div>";
        }
        //rpt_ebsartcmpny.DataSource = ListProducts.Where(x => x.type_id == 4 && x.Type == "S");
        //rpt_ebsartcmpny.DataBind();


        List<Products> ListBrands = new ProductsBLL().GetAllHomeBrands();
        rpt_brandsofweek.DataSource = ListBrands.Where(x => x.type_id == 5 && x.Type == "S");
        rpt_brandsofweek.DataBind();
        rpt_rpt_brandsofweek_SL.DataSource = ListBrands.Where(x => x.type_id == 5 && x.Type == "SL");
        rpt_rpt_brandsofweek_SL.DataBind();

        rpt_shopsale.DataSource = new ProductsBLL().GetHomeShopSaleList("userlist", 7);
        rpt_shopsale.DataBind();
        rpt_ultimtstationary.DataSource = new ProductsBLL().GetHomeShopSaleList("userlist", 3);
        rpt_ultimtstationary.DataBind();

        rpt_ebsartcmpny_SL.DataSource = new ProductsBLL().GetHomeShopSaleList("userlist", 4);
        rpt_ebsartcmpny_SL.DataBind();


    }
}