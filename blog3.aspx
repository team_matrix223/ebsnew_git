﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="blog3.aspx.cs" Inherits="blog3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
		<link href="customcss/blog.css" rel="stylesheet" />
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9 col-sm-9 col-xs-12">
				<div class="blog">
					<h2 class="blog-title">Back to school supplies list 2021 </h2>
					<div class="post-info">
							<div class="item post-posed-date">
							<span class="label">Posted:</span>
							<span class="value">January 20, 2021</span>
							</div>
							<div class="item post-categories">
							<span class="label">Categories:</span>
							<a title="Paper Stationery" href="#">Paper Stationery</a></div>
							<div class="item post-author">
							<span class="label">Author:</span>
							<span class="value">
							<a title="Varun Chaudhary" href="#">
							Varun Chaudhary
							</a>
							</span>
							</div>
					</div>
					<div class="blog-img-div">
						<img src="images/blog/blog3.jpeg" />
					</div>
					<div class="blog-desc">
					<p>A good start to a new school year&nbsp;is indeed really essential.&nbsp;Students need to make the most&nbsp;out&nbsp;of&nbsp;the coming academic year.&nbsp;Well, we have prepared a list of the&nbsp;<strong>basic</strong><strong>&nbsp;</strong><strong>back</strong><strong>-</strong><strong>to</strong><strong>-</strong><strong>school supplies&nbsp;</strong><strong>for all the&nbsp;</strong><strong>kids as well as high school students<strong>.&nbsp;</strong><span data-contrast="auto">These pocket-friendly back-to-school items are&nbsp;</span><span data-contrast="auto">something</span><span data-contrast="auto">&nbsp;for you</span><span data-contrast="auto">&nbsp;to keep your hands on</span><span data-contrast="auto">, yet again.</span><span data-contrast="auto">&nbsp;</span><span data-contrast="auto">Buckle up your shoes and&nbsp;</span><span data-contrast="auto">begin</span><span data-contrast="auto">&nbsp;to add the items on the following list in your cart.&nbsp;</span><span data-ccp-props="{&quot;201341983&quot;:0,&quot;335559739&quot;:160,&quot;335559740&quot;:259}">&nbsp;</span></p>
					<p><a href="#"><strong><span data-contrast="none">Mini pencil pouch</span></strong></a><strong><span data-contrast="auto">&nbsp;</span></strong><strong><span data-contrast="auto">–</span></strong><strong><span data-contrast="auto">&nbsp;</span></strong><span data-contrast="auto">This&nbsp;</span><strong><span data-contrast="auto">back-to-school must-have</span></strong><span data-contrast="auto">&nbsp;is something that every student&nbsp;</span><span data-contrast="auto">needs.&nbsp;</span><span data-contrast="auto">There is a good lot of&nbsp;</span><span data-contrast="auto">space</span><span data-contrast="auto">&nbsp;</span><span data-contrast="auto">in these pouches where you can&nbsp;</span><span data-contrast="auto">keep your pens, pencils, erasers, etc. The pouch </span><span data-contrast="auto">usually has 2-3 pockets. The main pocket is usually used to keep&nbsp;</span><span data-contrast="auto">highlighters, scale,s</span><span data-contrast="auto">&nbsp;and all such items. You can stock up your</span><span data-contrast="auto">&nbsp;paper clips, mini</span><span data-contrast="auto">&nbsp;</span><span data-contrast="auto">pencils, etc. in the small outer pouch. </span><span data-contrast="auto">Be it junior wing kids or high school students, a pencil pouch is&nbsp;</span><span data-contrast="auto">absolutely necessary.&nbsp;</span><span data-ccp-props="{&quot;201341983&quot;:0,&quot;335559739&quot;:160,&quot;335559740&quot;:259}">&nbsp;</span></p>
					<p><a href="#" target="_blank" rel="noopener"><strong><span data-contrast="none">Manual pencil sharpeners</span></strong></a><strong><span data-contrast="auto">&nbsp;</span></strong><strong><span data-contrast="auto">–</span></strong><strong><span data-contrast="auto">&nbsp;</span></strong><span data-contrast="auto">A sharpened pencil is the first thing that makes you look&nbsp;</span><span data-contrast="auto">prepared</span><span data-contrast="auto">&nbsp;</span><span data-contrast="auto">while</span><span data-contrast="auto">&nbsp;carrying</span><span data-contrast="auto">&nbsp;your school supplies.&nbsp;</span><span data-contrast="auto">Well, it is an important item to carry along. Nowadays, you will find&nbsp;</span><span data-contrast="auto">different&nbsp;</span><span data-contrast="auto">types</span><span data-contrast="auto">&nbsp;of sharpeners.&nbsp;</span><span data-contrast="auto">Get yourself the ‘two-in-one’ sharpeners.&nbsp;</span><span data-contrast="auto">They will help you sharpen </span><span data-contrast="auto">standard</span><span data-contrast="auto">&nbsp;as well as&nbsp;</span><span data-contrast="auto">jumbo pencils.&nbsp;</span><span data-contrast="auto">Make a permanent room in your pouch for&nbsp;</span><span data-contrast="auto">this</span><span data-contrast="auto">&nbsp;essential little thing.&nbsp;</span><span data-ccp-props="{&quot;201341983&quot;:0,&quot;335559739&quot;:160,&quot;335559740&quot;:259}">&nbsp;</span></p>
					<p><a href="#" target="_blank" rel="noopener"><strong><span data-contrast="none">Notebook set</span></strong></a><strong><span data-contrast="auto">&nbsp;–&nbsp;</span></strong><span data-contrast="auto">How can we forget one of the most&nbsp;</span><span data-contrast="auto">important items of back-to-school supplies?&nbsp;</span><span data-contrast="auto">Write down your notes, jot down your thoughts, inscribe your rough designs and all&nbsp;</span><span data-contrast="auto">the related</span><span data-contrast="auto">&nbsp;</span><span data-contrast="auto">stuff on your notebook. Since there are various things to be done, get yourself&nbsp;</span><span data-contrast="auto">more than 2 notebooks. This is a must-have&nbsp;</span><span data-contrast="auto">for all school-going students.&nbsp;</span><span data-ccp-props="{&quot;201341983&quot;:0,&quot;335559739&quot;:160,&quot;335559740&quot;:259}">&nbsp;</span></p>
					<p><a href="#" target="_blank" rel="noopener"><strong><span data-contrast="none">Tip highlighter pens</span></strong></a><strong><span data-contrast="auto">&nbsp;–&nbsp;</span></strong><span data-contrast="auto">Certainly,&nbsp;</span><span data-contrast="auto">many</span><span data-contrast="auto">&nbsp;students&nbsp;</span><span data-contrast="auto">find</span><span data-contrast="auto">&nbsp;that</span><span data-contrast="auto">&nbsp;highlighting&nbsp;</span><span data-contrast="auto">some</span><span data-contrast="auto">&nbsp;</span><span data-contrast="auto">important</span><span data-contrast="auto">&nbsp;stuff is&nbsp;</span><span data-contrast="auto">a&nbsp;</span><span data-contrast="auto">meaningful and interesting task</span><span data-contrast="auto">&nbsp;to do.&nbsp;</span><span data-contrast="auto">It helps a student to flip through the&nbsp;</span><span data-contrast="auto">vital</span><span data-contrast="auto">&nbsp;</span><span data-contrast="auto">part of the notes. Adding to this, it is an&nbsp;</span><span data-contrast="auto">important back-to-school supply for high school students. Get yourself&nbsp;</span><span data-contrast="auto">a pack of 6 highlighters. It will</span><span data-contrast="auto">, however,</span><span data-contrast="auto">&nbsp;be</span><span data-contrast="auto">&nbsp;sufficient</span><span data-contrast="auto">&nbsp;</span><span data-contrast="auto">for&nbsp;</span><span data-contrast="auto">one whole year.</span><span data-ccp-props="{&quot;201341983&quot;:0,&quot;335559739&quot;:160,&quot;335559740&quot;:259}">&nbsp;</span></p>
					<p><a href="#" target="_blank" rel="noopener"><strong><span data-contrast="none">Folders</span></strong></a><strong><span data-contrast="auto">&nbsp;–&nbsp;</span></strong><span data-contrast="auto">Make a purchase of unique-looking folders and arrange your assignments in the same. You will find a huge variety of different folders in an online marketplace, get some cute-looking folders. The whole concept of completing the assignments and studying for the tests will somehow turn easier for you.</span><span data-ccp-props="{&quot;201341983&quot;:0,&quot;335559739&quot;:160,&quot;335559740&quot;:259}">&nbsp;</span></p>
					<p><a href="#" target="_blank" rel="noopener"><strong><span data-contrast="none">A backpack</span></strong></a><strong><span data-contrast="auto">&nbsp;</span></strong><strong><span data-contrast="auto">–&nbsp;</span></strong><span data-contrast="auto">These days, you will find&nbsp;</span><span data-contrast="auto">vintage backpacks that can be considered valuable for </span><span data-contrast="auto">keeping a laptop and many other important school items.&nbsp;</span><span data-contrast="auto">If you will carry your research for the same, you might find some backpacks having&nbsp;</span><span data-contrast="auto">a built-in portable charger as well.&nbsp;</span><span data-contrast="auto">A normal backpack is already&nbsp;</span><span data-contrast="auto">a must-have. However, this advanced</span><span data-contrast="auto">&nbsp;high school&nbsp;</span><span data-contrast="auto">item is likely&nbsp;</span><span data-contrast="auto">to become really important in the coming academic session.&nbsp;&nbsp;</span><span data-ccp-props="{&quot;201341983&quot;:0,&quot;335559739&quot;:160,&quot;335559740&quot;:259}">&nbsp;</span></p>
					<p><a href="#" target="_blank" rel="noopener"><strong><span data-contrast="none">A set of pencils</span></strong></a><strong><span data-contrast="auto">&nbsp;</span></strong><strong><span data-contrast="auto">–</span></strong><strong><span data-contrast="auto">&nbsp;</span></strong><span data-contrast="auto">A large set of&nbsp;</span><span data-contrast="auto">30-40 pencils will be good&nbsp;</span><span data-contrast="auto">to keep you stocked-up for the entire year.&nbsp;</span><span data-contrast="auto">Get yourself a set of&nbsp;</span><span data-contrast="auto">high-quality</span><span data-contrast="auto">&nbsp;pencils</span><span data-contrast="auto">. You may not have&nbsp;</span><span data-contrast="auto">the need</span><span data-contrast="auto">&nbsp;</span><span data-contrast="auto">to sharpen them every 5 minutes.&nbsp;</span><span data-contrast="auto">Fill up your pencil pouch with these pencils.&nbsp;</span><span data-ccp-props="{&quot;201341983&quot;:0,&quot;335559739&quot;:160,&quot;335559740&quot;:259}">&nbsp;</span></p>
					<p><a href="#" target="_blank" rel="noopener"><strong><span data-contrast="none">Permanent markers</span></strong></a><strong><span data-contrast="auto">&nbsp;</span></strong><span data-contrast="auto">–</span><span data-contrast="auto">&nbsp;</span><span data-contrast="auto">Over the years, it has become obvious that permanent&nbsp;</span><span data-contrast="auto">markers are a vital side item in back-to-school&nbsp;</span><span data-contrast="auto">suppl</span><span data-contrast="auto">y.&nbsp;</span><span data-contrast="auto">A single marker in the pouch would be good to go for a month or so.</span><span data-ccp-props="{&quot;201341983&quot;:0,&quot;335559739&quot;:160,&quot;335559740&quot;:259}">&nbsp;</span></p>
					<p><a href="#" target="_blank" rel="noopener"><strong><span data-contrast="none">Gel pens</span></strong></a><strong><span data-contrast="auto">&nbsp;–&nbsp;</span></strong><span data-contrast="auto">A set of gel pens is surely an exciting stationery item to carry. A massive set of these pens would do fine for the whole session. The&nbsp;</span><span data-contrast="auto">colorful</span><span data-contrast="auto">&nbsp;gel pens would make your already pretty-looking pouch a little more exhilarating.&nbsp;</span><span data-ccp-props="{&quot;201341983&quot;:0,&quot;335559739&quot;:160,&quot;335559740&quot;:259}">&nbsp;</span></p>
					<p><a href="#" target="_blank" rel="noopener"><strong><span data-contrast="none">The school reopening&nbsp;</span></strong><strong><span data-contrast="none">post-covid-19</span></strong></a><strong><span data-contrast="auto">&nbsp;</span></strong><span data-contrast="auto">calls for the purchase of all the&nbsp;</span><span data-contrast="auto">essential back-to-school supplies.&nbsp;</span><span data-contrast="auto">Therefore, get started with it.&nbsp;</span><span data-ccp-props="{&quot;201341983&quot;:0,&quot;335559739&quot;:160,&quot;335559740&quot;:259}">&nbsp;</span></p>
					<p><a href="#" target="_blank" rel="noopener"><strong><span data-contrast="none">Biggest online stationery store in India</span></strong></a><strong><span data-contrast="auto">&nbsp;</span></strong><span data-ccp-props="{&quot;201341983&quot;:0,&quot;335559739&quot;:160,&quot;335559740&quot;:259}">&nbsp;</span></p>
					<p><a href="#" target="_blank" rel="noopener"><span data-contrast="none">EBS is one of the biggest online stationery stores in the country</span></a><span data-contrast="auto">. There is no need for you to get out of your places to buy the back-to-school must-haves. EBS has all of it for you. You will get the smallest of items to the biggest of stationery articles at this store, that too, at reasonable prices. So, stop wandering around the web and the stores.&nbsp;</span><a href="https://www.ebs1952.com/contact/" target="_blank" rel="noopener"><span data-contrast="none">EBS is a one-stop destination for all your stationery supplies.</span></a><span data-contrast="auto">&nbsp;</span><span data-ccp-props="{&quot;201341983&quot;:0,&quot;335559739&quot;:160,&quot;335559740&quot;:259}">&nbsp;</span></p>

					</div>
			</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="recent-section">
					<div class="recent-title">
						<strong>Recent Posts</strong>
					</div>
					<div class="recent-des">
						<div class="recent-img"><a href="/blog.aspx"><img src="images/blog/blog1.jpeg" /></a>
							
						</div>
						<div class="recent-tle">
							<a href="/blog.aspx">Functional planners and journals for women </a>
						</div>
						<div class="blog-date">January 27,2021</div>
					</div>
					<div class="recent-des">
							<div class="recent-img"><a href="/blog4.aspx"><img src="images/blog/blog4.jpeg" /></a></div>
						<div class="recent-tle">
							<a href="/blog4.aspx">FUN ART AND CRAFT DIY IDEAS TO DO AT HOME</a>
						</div>
						<div class="blog-date">January 13,2021</div>
					</div>
				
				
					<div class="recent-des">
							<div class="recent-img"><a href="/blog5.aspx"><img src="images/blog/blog5.jpeg" /></a></div>
						<div class="recent-tle">
							<a href="/blog5.aspx">DIY CRAFT IDEAS AND ACTIVITIES FOR ADULTS & KIDS.</a>
						</div>
						<div class="blog-date">January 09,2021</div>
					</div>
				
				
					<div class="recent-des">
							<div class="recent-img"><a href="/blog2.aspx"><img src="images/blog/blog2.jpeg" /></a></div>
						<div class="recent-tle">
							<a href="/blog2.aspx">EVERYTHING YOU NEED TO KNOW ABOUT MANDALA</a>
						</div>
						<div class="blog-date">November  18,2020</div>
					</div>
				
				</div>
			</div>
		</div>
	</div>
</asp:Content>

