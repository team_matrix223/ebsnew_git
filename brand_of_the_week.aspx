﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="brand_of_the_week.aspx.cs" Inherits="brand_of_the_week" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="customcss/brand-of-the-week-page.css" rel="stylesheet" />

    <div class="constraint no-padding">
        <div class="breadcrumbs">
            <ul class="breadcrumbs_container">
                <li class="breadcrumbs_item">
                    <a class="breadcrumbs_link" href="/index.aspx">Home</a>
                </li>
                <li class="breadcrumbs_item breadcrumbs_item-active">Brands of The Week</li>
            </ul>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="brand-week-head">
                    <h1 class="responsiveProductListHeader_title">Brand of The Week</h1>
                    <div class="readmore_content">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- widget responsiveSlot6 start -->




    <div class="responsiveSlot6">
        <div class="twoItemEditorial trackwidget "
            data-block-name="CHRISTMAS - URBAN DECAY BOTM"
            data-component="twoItemEditorial"
            data-widget-id="1891509"
            data-widget-gtm-only-tracking
            data-component-tracked-viewed
            data-component-tracked-clicked>
            <div class="container-fluid">
                <asp:Repeater ID="rpt_getbrands" runat="server">
                    <ItemTemplate>
                        <!-- itemOne -->
                        <div class="col-md-6 col-sm-6 col-xs-12 cst-bottom-margin">
                            <a href="list.aspx?b=<%#Eval("brandid") %>" class="twoItemEditorial_link">
                                <div class="twoItemEditorial_imageWrapper">
                                    <img src="images/brand/<%#Eval("photourl")%>" alt="" class="twoItemEditorial_image big_image" />
                                </div>
                                <div class="twoItemEditorial_textContainer">
                                    <h3 class="twoItemEditorial_itemTitle"><%#Eval("Title")%> </h3>
                                    <%--<p class="twoItemEditorial_itemDescription">Discover the very best vegan products this Veganuary from brands including Aveda, Bulldog Skincare and Ouai.</p>--%>
                                    <div class="twoItemEditorial_itemCTAText">SHOP NOW</div>
                                </div>
                            </a>
                        </div>
                    </ItemTemplate>

                </asp:Repeater>
            </div>
        </div>
    </div>



</asp:Content>

