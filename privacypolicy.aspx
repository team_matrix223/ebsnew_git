﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="privacypolicy.aspx.cs" Inherits="privacypolicy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="customcss/aboutus.css" rel="stylesheet" />
	  <div class="constraint no-padding">
        <div class="breadcrumbs">
            <ul class="breadcrumbs_container">
                <li class="breadcrumbs_item">
                    <a class="breadcrumbs_link" href="/index.aspx">Home</a>
                </li>
                <li class="breadcrumbs_item breadcrumbs_item-active">Privacy Policy</li>
            </ul>
        </div>
    </div>
	<div class="container-fluid">
		  <h1 class="responsiveProductListHeader_title">Privacy Policy</h1>
		
		<div class="about-us-content">
			<h4>WE PROTECT YOUR PRIVACY</h4>
			<p>Our privacy policy is simple and transparent: any information you share with us, stays with us. We do not rent, sell, lend, or otherwise distribute your personal information to anyone for any reason. This includes your contact information, as well as specific order information. We limit data access to those who need to know to process your order. Within our organization, your personal data is accessible to only a limited number of employees with special access privileges. Although we may, from time to time, compile general demographic information based on your order, this information is shared within our organization only and has no identifiable personal data associated with it.</p>
			<h4>INFORMATION COLLECTED</h4>
			<p>To enable you to place an order on our site, we need to have the following basic information about you: first name, last name, and address including city, zip code, state, country, phone number and contact email. Apart from this, our system gathers certain details about your computer's internet connection like your IP address when you visit our site. Your IP address does not identify you personally. We use this information to deliver our web pages to you upon request, to customize our site as per your interest, to calculate the number of visitors on our site and to know the geographic locations from where our visitors come. We do not allow any unauthorized person or organization be it other members, visitors, and anyone not in our organization to use any information collected from you.</p>
			<h4>INFORMATION SHARING</h4>
			<p>We do not rent, sell, barter, or give away your information to anyone. To some extent, information has to be passed on to the courier companies, credit card processing companies, vendors, etc. to enable them to perform their functions related to your order fulfillment. Apart from this normal business requirement, information may also be needed to be shared with law authorities, for fraud detection, and for the safety of our site, employees, management, users, members and other affiliates associated with us.</p>
			<h4>INFORMATION USAGE</h4>
			<p>The most important information collected from you is your email which is used to inform you that your order has been confirmed / executed. Your email is also used to inform you for any customer service related queries and for newsletters. All other information collected is confidentially stored and will not be disclosed unless needed as per the requirement of the law authorities or in case of any disputes.</p>
			<h4>SMS POLICY</h4>
			<p>Our customers may receive transactional or promotional messages on their registered mobile numbers.</p>
			<p>Further we declare that it will not contain any content and/or any material that:</p>
			<ul class="about-ul">
				<li>is infringing, libelous, defamatory, obscene, pornographic, abusive, harmful, threatening, harassing, tortuous, offensive, hateful, or racially, ethnically or otherwise objectionable, misleading or violates any law or right of any third party;</li>
				<li>is any unsolicited or unauthorized advertising, promotional materials, junk mail, spam, chain, letters, pyramid schemes or any other form of solicitation.</li>
			</ul>
			<p>In addition to the above, we assure and assert that we will not send “Unsolicited Commercial Content” messages to consumers that have registered with the NCPR database maintained centrally by TRAI and we will be solely responsible and liable for any claim and/or allegation and/or complaint from third parties or authorities relating to such content and/ or material.</p>

		</div>
		
	</div>
</asp:Content>

