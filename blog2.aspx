﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="blog2.aspx.cs" Inherits="blog2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="customcss/blog.css" rel="stylesheet" />
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9 col-sm-9 col-xs-12">
				<div class="blog">
					<h2 class="blog-title">Everything You Need to Know About Mandala</h2>
					<div class="post-info">
							<div class="item post-posed-date">
							<span class="label">Posted:</span>
							<span class="value">November 18, 2020</span>
							</div>
							<div class="item post-categories">
							<span class="label">Categories:</span>
							<a title="Paper Stationery" href="#">Art and Craft</a></div>
							<div class="item post-author">
							<span class="label">Author:</span>
							<span class="value">
							<a title="Varun Chaudhary" href="#">
							Varun Chaudhary
							</a>
							</span>
							</div>
					</div>
					<div class="blog-img-div">
						<img src="images/blog/blog2.jpeg" />
					</div>
					<div class="blog-desc">
					<p>If you have ever come across a spiritual Asian art piece, t may most probably have been mandala artwork.
						 In Sanskrit, mandala means “discoid object,” or “circle”. It is primarily a geometric design incorporating heavy Buddhist and 
						Hindu symbolism. Mandalas are considered to portray the multifaceted aspects of our universe. Moreover, 
						<a href="https://www.timesunion.com/living/article/Mandalas-may-boost-benefits-of-meditation-4152298.php#:~:text=Mandalas%2C%20meaning%20%22circles%22%20in,promote%20sleep%20and%20ease%20depression." target="_blank" rel="noopener">
							they are used as instruments of meditation</a> and prayer most often in Japan, China, as well as Tibet. </p>
					<p>Mandalas are circles inscribed within a square, in their most fundamental format, arranged into organized sections around a single, focal point. They are essentially made on cloth or paper, fashioned with threads, bronze, or even stone. Although extraordinary by themselves, mandalas contain high meditative and symbolic connotations that go beyond their attractive appearance.
					</p>
					<h4><strong>What exactly is a Mandala? </strong></h4>
					<p>A mandala is a ritual and spiritual design used in many Asian cultures. It can be interpreted in two ways: on a surface level as a visual representation of the universe which is different from its internal meaning: as a reference point for different Asian traditions, including but not limited to: meditation. <a href="https://www.invaluable.com/blog/what-is-a-mandala/#:~:text=A%20mandala%20is%20a%20spiritual%20and%20ritual%20symbol%20in%20Asian%20cultures.&amp;text=In%20Hinduism%20and%20Buddhism%2C%20the,one%20of%20joy%20and%20happiness." target="_blank" rel="noopener">In Buddhism and Hinduism, it is believed that your consciousness enters the mandala and goes towards the symbol's center.</a> You are essentially guided by the collective cosmic sequence of transforming your internal universe from suffering to that of happiness and joy.</p>
					<h4><strong>What is mandala art used for now? </strong></h4>
					<p>Mandalas, as mentioned before translate to "circles" in Sanskrit. These are sacred designs that find use in prayer, art therapy, meditation, healing, and for both adults and children. Mandalas have been proven to reduce pain and stress, lower blood pressure, boost the immune system, and ease depression and promote sleep.&nbsp;</p>
					<h4><strong>How to make a mandala? </strong></h4>
					<p>When you are deep in the process of making your own mandala, think of it as your soul's echo. Creating and coloring a mandala artwork can be a greatly enriching experience where you might be encouraged to peep inside of yourself for finding the colors, patterns, and shapes, that can be symbolic of either your current mental state or your most deeply buried inner wish for yourself, for a loved one, or for humanity.</p>
					<p>You can also design a mandala piece for symbolizing your potential state of mind. Mandalas are potent tools to achieve heightened self-awareness and a meditational state. Several different cultures all over the world make use of mandalas as a part of their spiritual practices.</p>
					<p>The best part of creating your own mandalas is that you own the freedom to select the colors and shapes best reflecting your reality and sense of self. You own your mandala, and you can your creativity for drawing a mandala drawing that is uniquely you.</p>
					<p>Once you know the basic steps of how to draw a mandala, you can look for and try new designs incorporating new colors every time you create a new mandala.</p>
					<h4><strong>Materials needed for mandala art: </strong></h4>
					<p>You do not necessarily need a lot of materials for learning how you can draw a mandala. All that you would require is a <a href="#" target="_blank" rel="noopener">sheet of paper</a>, a ruler, <a href="#" target="_blank" rel="noopener">an eraser</a>, <a href="#" target="_blank" rel="noopener">and a pencil</a>. You can even choose&nbsp;</p>
					<p>To color the mandala: you would need your choice of <a href="#" target="_blank" rel="noopener">crayons</a>, <a href="#" target="_blank" rel="noopener">watercolors</a>, <a href="#" target="_blank" rel="noopener">colored pencils</a>, or any other type of art material for coloring. You can even make use of a compass if you would so like.</p>
					<h4><strong>Tips for creating mandala art for beginners: </strong></h4>
					<ul>
					<li>The very first step towards drawing a mandala is by measuring your paper in the shape of a square. Your square can be as small or big as you would want. However, you can fill in more detail and color by making your square as big as possible. To begin with, your square can be 8" x 8".</li>
					<li>Now, you would need to take out your pencil and ruler to make a dot at the exact center of your square. From here on, make equally placed dots on lines joining the central dot to each of the corners of your square.</li>
					<li>Connect your dots after joining them. Next, draw vertical lines connecting the dots and going up and down, while drawing a straight horizontal line that connects dots that go on either side.</li>
					<li>Ultimately, your rows of dots should resemble a giant 'x' shape.</li>
					<li>Next, connect the dots with your ruler.</li>
					</ul>
					<h4><strong>How to Draw a Mandala Zentangle? </strong></h4>.
					<p>A mandala Zentangle incorporates creating beautiful images using repetitive patterns. It is a delightful art form that is also relaxing and fun. It improves your creativity and focuses drastically. Zentangle lends artistic satisfaction with an improved sense of wellbeing. Drawing Zentangles involve a wide range of ages and skills as well as fields of interest.</p>
					<p><strong>The Zentangle Drawing Process: </strong></p>
					<p>Art is said to be far more than a collection of aesthetically pleasing things; it majorly incorporates the process of creating those very things. Art allows us to encounter facets of our inner being for discovering deep-meaning, rewarding meaning patterns. To be creatively minded can be a transformative tool for collective as well as personal transformation. Zentangles are said to be transformative since the very process is able to change your life's quality from its current state to a far deeper understanding of yourself.&nbsp;</p>
					<p>Furthermore, the Zentangle Mandala Drawing Process, when done in Small Group Situations accords access to your emotionally repressed side, the side most predominantly governed by your right brain. The right brain communicates neurological information about your attitudes, values, feelings, and beliefs. Zentangle Mandala Art is that one tool that can help you to express your feelings and thoughts even more readily since these feelings are not generally filtered via your brain's left side.</p>
					<h4><strong>Conclusion </strong></h4>
					<p>As you practice drawing even more number of designs, your mandalas will eventually start looking more complex.</p>
					<p>The key to creating perfect mandalas is by taking it slow, making one shape at one time while manifesting the shape going around in the circle.</p>
					<p>Lastly, you build upon your shape by creating other shapes around the circle in the same way.</p>
				</div>
			</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="recent-section">
					<div class="recent-title">
						<strong>Recent Posts</strong>
					</div>
					<div class="recent-des">
						<div class="recent-img"><a href="/blog.aspx"><img src="images/blog/blog1.jpeg" /></a>
							
						</div>
						<div class="recent-tle">
							<a href="/blog.aspx">Functional planners and journals for women </a>
						</div>
						<div class="blog-date">January 27,2021</div>

					</div>


					<div class="recent-des">
							<div class="recent-img"><a href="/blog3.aspx"><img src="images/blog/blog3.jpeg" /></a></div>
						<div class="recent-tle">
							<a href="/blog3.aspx">BACK TO SCHOOL SUPPLIES LIST 2021</a>
						</div>
						<div class="blog-date">January 20,2021</div>
					</div>

					<div class="recent-des">
							<div class="recent-img"><a href="/blog4.aspx"><img src="images/blog/blog4.jpeg" /></a></div>
						<div class="recent-tle">
							<a href="/blog4.aspx">FUN ART AND CRAFT DIY IDEAS TO DO AT HOME</a>
						</div>
						<div class="blog-date">January 13,2021</div>
					</div>
				
				
					<div class="recent-des">
							<div class="recent-img"><a href="/blog5.aspx"><img src="images/blog/blog5.jpeg" /></a></div>
						<div class="recent-tle">
							<a href="/blog5.aspx">DIY CRAFT IDEAS AND ACTIVITIES FOR ADULTS & KIDS.</a>
						</div>
						<div class="blog-date">January 09,2021</div>
					</div>
				
				
					
				
				</div>
			</div>
		</div>
	</div>
</asp:Content>

