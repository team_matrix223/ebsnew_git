﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="subscription-box.aspx.cs" Inherits="subscription_box" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="customcss/home-page.css" rel="stylesheet" />
	<link href="customcss/subscription-box-page.css" rel="stylesheet" />
    	<!-- widget responsiveSlot1 start -->
				<div class="responsiveSlot1">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
							<li data-target="#carousel-example-generic" data-slide-to="1"></li>
							<li data-target="#carousel-example-generic" data-slide-to="2"></li>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<img src="images/slider/Fancy_Banner.jpg" alt="...">
								<div class="carousel-caption">
									
								</div>
							</div>
							<div class="item">
								<img src="images/slider/New_Year_Banner.jpg" />
								<div class="carousel-caption">

								</div>
							</div>
							<div class="item">
								<img src="images/slider/Web_Banner.jpg" />
								<div class="carousel-caption">
								
								</div>
							</div>
							
						</div>

						<!-- Controls -->
						<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
				<!-- widget responsiveSlot1 stop -->
		<div class="container">
			<ul class="button-sectoin">
				<li><a href="#" class="">HOW IT WORKS</a></li>
				<li><a href="#" class="">FAQS</a></li>
				<li><a href="#" class="">LIMITED EDITIONS</a></li>
				<li><a href="#" class="">BUY NOW</a></li>
			</ul>
		</div>
       
 
    	
    		
			
    	
				<!-- widget responsiveSlot7 start -->
				
					<div class="MultiCarousel extra-mul" data-items="1,2,3,4" data-slide="1" id="MultiCarousel" data-interval="1000">
									
								</div>
					
				
				<!-- widget responsiveSlot7 stop -->
    	<!-- widget responsiveSlot9 start -->
				<div class="responsiveSlot9">
					<div class="promoProductSlider">
						<div class="promoProductSlider_title">JANUARY BOX</div>
						
						<div class="container-fluid">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="sub-box-cnt">
									<div class="promoProductSlider_title">JOIN OUR EBS SIGNATURE STATIONERY BOX COMMUNITY TODAY</div>
									<div class="promoProductSlider_subtitle">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
										 Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a 
										galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,
										 but also the leap into electronic typesetting, remaining essentially unchanged. 
										It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
										 and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<a href="#" class="promoProductSlider_imageLink" aria-hidden="false">
									<%--<img src="images/others/o1.jpg"
										 alt="Clinique"
										 class="promoProductSlider_image" />--%>
									<div class="twoItemEditorial_imageWrapper">
										<img src="images/others/o1.jpg" alt="" class="twoItemEditorial_image">
									</div>
								</a>

							</div>

					
						</div>
					</div>
				</div>
		<!-- widget responsiveSlot9 stop -->
		<!-- widget responsiveSlot10 start -->
				<div class="responsiveSlot10">
					<div class="promoProductSlider"
						 data-block-name="29/12. The Inkey List Salon Space"
						 data-block-type="Product"
						 data-widget-id="2300613"
						 data-widget-gtm-only-tracking
						 data-component-tracked-viewed
						 data-component="promoProductSlider">
						<span data-component="productQuickbuy"></span>
						<div class="promoProductSlider_title">EBS LIMITED EDITION COLLECTIONS</div>
						<div class="promoProductSlider_subtitle">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
							Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
							 type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into 
							electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
							 sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker 
							including versions of Lorem Ipsum.
						</div>
						<div class="container-fluid">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<a href="#" class="promoProductSlider_imageLink" aria-hidden="false">
									<%--<img src="images/others/o1.jpg"
										 alt="Clinique"
										 class="promoProductSlider_image" />--%>
									<div class="twoItemEditorial_imageWrapper">
										<img src="images/others/o1.jpg" alt="" class="twoItemEditorial_image">
									</div>
								</a>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<!--data-items="1,3,5,6"-->
								<div class="row">
									<div class="MultiCarousel product-cro" data-items="1,2,3,4" data-slide="1" id="MultiCarousel" data-interval="1000">
										<div class="MultiCarousel-inner">
											<div class="item">
												<div class="pad15">
													<img class="medium-img" src="images/artist/a2.jpg" />
													<p class="sub-title">BRUSTRO ARTIST ACRYLIC COLOUR 5 PCS 120ML </p>
													<div class="papBannerWrapper" data-component="papBanner">
														<button data-read-more-translation=", read more" class="papBanner papBanner_popupLink" data-js-element="popupLink" aria-label="Complimentary gift, read more">
															<span class="papBanner_text">
																Complimentary gift
															</span>
														</button>
														<div class="papPopup" data-js-element="papPopup">
															<div class="papPopup_container" data-pappopup-contents="">
																<div class="papFreeGift" data-component="papFreeGift">
																	<div class="papFreeGift_imageContainer">
																		<div class="papFreeGift_cropImage">
																			<img src="https://s2.thcdn.com//productimg/300/300/12778612-1364832892700089.jpg" alt="Estée Lauder New Year 6-Pc GWP" class="papFreeGift_image">
																		</div>
																	</div>

																	<div class="papFreeGift_text">
																		<span class="papFreeGift_title">
																			Estée Lauder New Year 6-Pc GWP
																		</span>

																		<span class="papFreeGift_saving">
																			Worth £109.00
																		</span>
																	</div>
																</div>


																<h3 data-popup-title="" class="papPopup_title">Complimentary gift</h3>
																<div class="papPopup_text">Receive a complimentary Estée Lauder 6-Piece-Set when you spend £70 on the brand.Complimentary gift will be awarded at the basket. Offer valid for a limited time only, while stocks last.</div>

																<a href="/brands/estee-lauder/all.list" class="papPopup_link">Shop the offer</a>
															</div>
														</div>

													</div>
													<span class="productBlock_rating_container">
														<span class="productBlock_rating" data-is-link="true">
															<span class="visually-hidden productBlock_rating_hiddenLabel">4.83 Stars 6 Reviews</span>
															<span class="productBlock_ratingStarsContainer">
																<span class="productBlock_ratingStars">
																	<span>
																		<svg xmlns="http://www.w3.org/2000/svg" width="80" height="16" viewBox="0 0 84 16">

																			<linearGradient id="grad-12024036-96.6" x1="0" x2="100%" y1="0" y2="0">
																				<stop class="productBlock_ratingStars-fill" offset="96.6%"></stop>
																				<stop class="productBlock_ratingStars-background" offset="3.4000000000000057%"></stop>
																			</linearGradient>

																			<path fill="url(#grad-12024036-96.6)" d="M8 12.5 3.004 16 4.403 10 0 6 5.777 5.44 8 0 10.223 5.44 16 6 11.597 10 12.996 16 M25 12.5 20.004 16 21.403 10 17 6 22.777 5.44 25 0 27.223 5.44 33 6 28.597 10 29.996 16 M42 12.5 37.004 16 38.403 10 34 6 39.777 5.44 42 0 44.223 5.44 50 6 45.597 10 46.996 16 M59 12.5 54.004 16 55.403 10 51 6 56.777 5.44 59 0 61.223 5.44 67 6 62.597 10 63.996 16 M76 12.5 71.004 16 72.403 10 68 6 73.777 5.44 76 0 78.223 5.44 84 6 79.597 10 80.996 16"></path>

																		</svg>
																	</span>
																</span>
															</span>
															<span class="productBlock_ratingValue" aria-hidden="true">4.83</span>
															<span class="productBlock_reviewCount" aria-hidden="true">6</span>
														</span>
													</span>
													<div class="productBlock_priceBlock" data-is-link="true">
														<div class="productBlock_price">
															<span class="productBlock_priceCurrency" content="GBP"></span>
															<span class="productBlock_priceValue" content="£25.00">&#8377;985.50</span>
														</div>

													</div>
													<div class="productBlock_actions">
														<span data-component="productQuickbuy"></span>
														<a href="/estee-lauder-double-wear-instant-fix-concealer-12ml-various-shades/12024036.html" class="productBlock_button productBlock_button-moreInfo" data-sku="12024036" data-open-product-quickbuy="" data-from-wishlist="false" data-subscribe-and-save="">
															Quick Buy
															<span class="visually-hidden">
																Estée Lauder Double Wear Instant Fix Concealer 12ml (Various Shades)
															</span>
														</a>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="pad15">
													<img class="medium-img" src="images/artist/a3.jpg" />
													<p class="sub-title">BRUSTRO ARTIST GOUACHE COLOUR TITANIUM WHITE 2 X 40ML </p>
													<div class="papBannerWrapper" data-component="papBanner">
														<button data-read-more-translation=", read more" class="papBanner papBanner_popupLink" data-js-element="popupLink" aria-label="Complimentary gift, read more">
															<span class="papBanner_text">
																Complimentary gift
															</span>
														</button>
														<div class="papPopup" data-js-element="papPopup">
															<div class="papPopup_container" data-pappopup-contents="">
																<div class="papFreeGift" data-component="papFreeGift">
																	<div class="papFreeGift_imageContainer">
																		<div class="papFreeGift_cropImage">
																			<img src="https://s2.thcdn.com//productimg/300/300/12778612-1364832892700089.jpg" alt="Estée Lauder New Year 6-Pc GWP" class="papFreeGift_image">
																		</div>
																	</div>

																	<div class="papFreeGift_text">
																		<span class="papFreeGift_title">
																			Estée Lauder New Year 6-Pc GWP
																		</span>

																		<span class="papFreeGift_saving">
																			Worth £109.00
																		</span>
																	</div>
																</div>


																<h3 data-popup-title="" class="papPopup_title">Complimentary gift</h3>
																<div class="papPopup_text">Receive a complimentary Estée Lauder 6-Piece-Set when you spend £70 on the brand.Complimentary gift will be awarded at the basket. Offer valid for a limited time only, while stocks last.</div>

																<a href="/brands/estee-lauder/all.list" class="papPopup_link">Shop the offer</a>
															</div>
														</div>

													</div>
													<span class="productBlock_rating_container">
														<span class="productBlock_rating" data-is-link="true">
															<span class="visually-hidden productBlock_rating_hiddenLabel">4.83 Stars 6 Reviews</span>
															<span class="productBlock_ratingStarsContainer">
																<span class="productBlock_ratingStars">
																	<span>
																		<svg xmlns="http://www.w3.org/2000/svg" width="80" height="16" viewBox="0 0 84 16">

																			<linearGradient id="grad-12024036-96.6" x1="0" x2="100%" y1="0" y2="0">
																				<stop class="productBlock_ratingStars-fill" offset="96.6%"></stop>
																				<stop class="productBlock_ratingStars-background" offset="3.4000000000000057%"></stop>
																			</linearGradient>

																			<path fill="url(#grad-12024036-96.6)" d="M8 12.5 3.004 16 4.403 10 0 6 5.777 5.44 8 0 10.223 5.44 16 6 11.597 10 12.996 16 M25 12.5 20.004 16 21.403 10 17 6 22.777 5.44 25 0 27.223 5.44 33 6 28.597 10 29.996 16 M42 12.5 37.004 16 38.403 10 34 6 39.777 5.44 42 0 44.223 5.44 50 6 45.597 10 46.996 16 M59 12.5 54.004 16 55.403 10 51 6 56.777 5.44 59 0 61.223 5.44 67 6 62.597 10 63.996 16 M76 12.5 71.004 16 72.403 10 68 6 73.777 5.44 76 0 78.223 5.44 84 6 79.597 10 80.996 16"></path>

																		</svg>
																	</span>
																</span>
															</span>
															<span class="productBlock_ratingValue" aria-hidden="true">4.83</span>
															<span class="productBlock_reviewCount" aria-hidden="true">6</span>
														</span>
													</span>
													<div class="productBlock_priceBlock" data-is-link="true">
														<div class="productBlock_price">
															<span class="productBlock_priceCurrency" content="GBP"></span>
															<span class="productBlock_priceValue" content="£25.00">&#8377;269.10</span>
														</div>

													</div>
													<div class="productBlock_actions">
														<span data-component="productQuickbuy"></span>
														<a href="/estee-lauder-double-wear-instant-fix-concealer-12ml-various-shades/12024036.html" class="productBlock_button productBlock_button-moreInfo" data-sku="12024036" data-open-product-quickbuy="" data-from-wishlist="false" data-subscribe-and-save="">
															Quick Buy
															<span class="visually-hidden">
																Estée Lauder Double Wear Instant Fix Concealer 12ml (Various Shades)
															</span>
														</a>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="pad15">
													<img class="medium-img" src="images/artist/a4.jpg" />
													<p class="sub-title">BRUSTRO ARTIST ACRYLIC FLUORESCENT PINK 120ML </p>
													<div class="papBannerWrapper" data-component="papBanner">
														<button data-read-more-translation=", read more" class="papBanner papBanner_popupLink" data-js-element="popupLink" aria-label="Complimentary gift, read more">
															<span class="papBanner_text">
																Complimentary gift
															</span>
														</button>
														<div class="papPopup" data-js-element="papPopup">
															<div class="papPopup_container" data-pappopup-contents="">
																<div class="papFreeGift" data-component="papFreeGift">
																	<div class="papFreeGift_imageContainer">
																		<div class="papFreeGift_cropImage">
																			<img src="https://s2.thcdn.com//productimg/300/300/12778612-1364832892700089.jpg" alt="Estée Lauder New Year 6-Pc GWP" class="papFreeGift_image">
																		</div>
																	</div>

																	<div class="papFreeGift_text">
																		<span class="papFreeGift_title">
																			Estée Lauder New Year 6-Pc GWP
																		</span>

																		<span class="papFreeGift_saving">
																			Worth £109.00
																		</span>
																	</div>
																</div>


																<h3 data-popup-title="" class="papPopup_title">Complimentary gift</h3>
																<div class="papPopup_text">Receive a complimentary Estée Lauder 6-Piece-Set when you spend £70 on the brand.Complimentary gift will be awarded at the basket. Offer valid for a limited time only, while stocks last.</div>

																<a href="/brands/estee-lauder/all.list" class="papPopup_link">Shop the offer</a>
															</div>
														</div>

													</div>
													<span class="productBlock_rating_container">
														<span class="productBlock_rating" data-is-link="true">
															<span class="visually-hidden productBlock_rating_hiddenLabel">4.83 Stars 6 Reviews</span>
															<span class="productBlock_ratingStarsContainer">
																<span class="productBlock_ratingStars">
																	<span>
																		<svg xmlns="http://www.w3.org/2000/svg" width="80" height="16" viewBox="0 0 84 16">

																			<linearGradient id="grad-12024036-96.6" x1="0" x2="100%" y1="0" y2="0">
																				<stop class="productBlock_ratingStars-fill" offset="96.6%"></stop>
																				<stop class="productBlock_ratingStars-background" offset="3.4000000000000057%"></stop>
																			</linearGradient>

																			<path fill="url(#grad-12024036-96.6)" d="M8 12.5 3.004 16 4.403 10 0 6 5.777 5.44 8 0 10.223 5.44 16 6 11.597 10 12.996 16 M25 12.5 20.004 16 21.403 10 17 6 22.777 5.44 25 0 27.223 5.44 33 6 28.597 10 29.996 16 M42 12.5 37.004 16 38.403 10 34 6 39.777 5.44 42 0 44.223 5.44 50 6 45.597 10 46.996 16 M59 12.5 54.004 16 55.403 10 51 6 56.777 5.44 59 0 61.223 5.44 67 6 62.597 10 63.996 16 M76 12.5 71.004 16 72.403 10 68 6 73.777 5.44 76 0 78.223 5.44 84 6 79.597 10 80.996 16"></path>

																		</svg>
																	</span>
																</span>
															</span>
															<span class="productBlock_ratingValue" aria-hidden="true">4.83</span>
															<span class="productBlock_reviewCount" aria-hidden="true">6</span>
														</span>
													</span>
													<div class="productBlock_priceBlock" data-is-link="true">
														<div class="productBlock_price">
															<span class="productBlock_priceCurrency" content="GBP"></span>
															<span class="productBlock_priceValue" content="£25.00">&#8377;224.10</span>
														</div>

													</div>
													<div class="productBlock_actions">
														<span data-component="productQuickbuy"></span>
														<a href="/estee-lauder-double-wear-instant-fix-concealer-12ml-various-shades/12024036.html" class="productBlock_button productBlock_button-moreInfo" data-sku="12024036" data-open-product-quickbuy="" data-from-wishlist="false" data-subscribe-and-save="">
															Quick Buy
															<span class="visually-hidden">
																Estée Lauder Double Wear Instant Fix Concealer 12ml (Various Shades)
															</span>
														</a>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="pad15">
													<img class="medium-img" src="images/artist/a5.jpeg" />
													<p class="sub-title">BRUSTRO WIRO ARTIST DRAWING BOOK 116 PAGES 160GSM A6 </p>
													<div class="papBannerWrapper" data-component="papBanner">
														<button data-read-more-translation=", read more" class="papBanner papBanner_popupLink" data-js-element="popupLink" aria-label="Complimentary gift, read more">
															<span class="papBanner_text">
																Complimentary gift
															</span>
														</button>
														<div class="papPopup" data-js-element="papPopup">
															<div class="papPopup_container" data-pappopup-contents="">
																<div class="papFreeGift" data-component="papFreeGift">
																	<div class="papFreeGift_imageContainer">
																		<div class="papFreeGift_cropImage">
																			<img src="https://s2.thcdn.com//productimg/300/300/12778612-1364832892700089.jpg" alt="Estée Lauder New Year 6-Pc GWP" class="papFreeGift_image">
																		</div>
																	</div>

																	<div class="papFreeGift_text">
																		<span class="papFreeGift_title">
																			Estée Lauder New Year 6-Pc GWP
																		</span>

																		<span class="papFreeGift_saving">
																			Worth £109.00
																		</span>
																	</div>
																</div>


																<h3 data-popup-title="" class="papPopup_title">Complimentary gift</h3>
																<div class="papPopup_text">Receive a complimentary Estée Lauder 6-Piece-Set when you spend £70 on the brand.Complimentary gift will be awarded at the basket. Offer valid for a limited time only, while stocks last.</div>

																<a href="/brands/estee-lauder/all.list" class="papPopup_link">Shop the offer</a>
															</div>
														</div>

													</div>
													<span class="productBlock_rating_container">
														<span class="productBlock_rating" data-is-link="true">
															<span class="visually-hidden productBlock_rating_hiddenLabel">4.83 Stars 6 Reviews</span>
															<span class="productBlock_ratingStarsContainer">
																<span class="productBlock_ratingStars">
																	<span>
																		<svg xmlns="http://www.w3.org/2000/svg" width="80" height="16" viewBox="0 0 84 16">

																			<linearGradient id="grad-12024036-96.6" x1="0" x2="100%" y1="0" y2="0">
																				<stop class="productBlock_ratingStars-fill" offset="96.6%"></stop>
																				<stop class="productBlock_ratingStars-background" offset="3.4000000000000057%"></stop>
																			</linearGradient>

																			<path fill="url(#grad-12024036-96.6)" d="M8 12.5 3.004 16 4.403 10 0 6 5.777 5.44 8 0 10.223 5.44 16 6 11.597 10 12.996 16 M25 12.5 20.004 16 21.403 10 17 6 22.777 5.44 25 0 27.223 5.44 33 6 28.597 10 29.996 16 M42 12.5 37.004 16 38.403 10 34 6 39.777 5.44 42 0 44.223 5.44 50 6 45.597 10 46.996 16 M59 12.5 54.004 16 55.403 10 51 6 56.777 5.44 59 0 61.223 5.44 67 6 62.597 10 63.996 16 M76 12.5 71.004 16 72.403 10 68 6 73.777 5.44 76 0 78.223 5.44 84 6 79.597 10 80.996 16"></path>

																		</svg>
																	</span>
																</span>
															</span>
															<span class="productBlock_ratingValue" aria-hidden="true">4.83</span>
															<span class="productBlock_reviewCount" aria-hidden="true">6</span>
														</span>
													</span>
													<div class="productBlock_priceBlock" data-is-link="true">
														<div class="productBlock_price">
															<span class="productBlock_priceCurrency" content="GBP"></span>
															<span class="productBlock_priceValue" content="£25.00">&#8377;178.20</span>
														</div>

													</div>
													<div class="productBlock_actions">
														<span data-component="productQuickbuy"></span>
														<a href="/estee-lauder-double-wear-instant-fix-concealer-12ml-various-shades/12024036.html" class="productBlock_button productBlock_button-moreInfo" data-sku="12024036" data-open-product-quickbuy="" data-from-wishlist="false" data-subscribe-and-save="">
															Quick Buy
															<span class="visually-hidden">
																Estée Lauder Double Wear Instant Fix Concealer 12ml (Various Shades)
															</span>
														</a>
													</div>

												</div>
											</div>
										
										</div>
										<button class="btn btn-primary leftLst" type="button"><img src="images/slider/left1.png" /></button>
										<button class="btn btn-primary rightLst" type="button"><img src="images/slider/right1.png" /></button>
									</div>
								</div>


							</div>

					
						</div>
					</div>
				</div>
				<!-- widget responsiveSlot10 stop -->
			
			


</asp:Content>

