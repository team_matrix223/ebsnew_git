﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class addaddress : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
        HttpContext.Current.Session[Constants.CustType] = "Guest";

    }

    [WebMethod]
    public static void insert_into_mstusercart(Int64 DeliveryAddressId)
    {
      
      
        // Int64 _DeliveryAddressId = CommonFunctions.IsNumeric(DeliveryAddressId);
        UserCartMst UCM = new UserCartMst()
        {
            SessionId = HttpContext.Current.Session.SessionID,
            DeliveryDate = Convert.ToDateTime(DateTime.Now),
            DeliverySlot = "",
            UserId = Convert.ToInt64(0),
            PaymentMode = "",
            DeliveryAddressId = DeliveryAddressId
        };
        new UserCartMstBLL().InsertUpdate(UCM);
    }



    [WebMethod]
    public static string checkmail(string Email)
    {
        JavaScriptSerializer ser = new JavaScriptSerializer();
        string _Firstname = "";
          string _Lastname = "";
        string _Address = "";
        string _City = "";
        string _Area = "";
        string _Pincode = "";
        string _Mobileno = "";
        string _CusType = "";
        DataSet ds = new UsersDAL().CheckMail(Email);
        if (ds.Tables[0].Rows.Count>0)
        {
            _Firstname = ds.Tables[0].Rows[0]["RecipientFirstName"].ToString();
            _Lastname = ds.Tables[0].Rows[0]["RecipientLastName"].ToString();
            _Address = ds.Tables[0].Rows[0]["Address"].ToString();
            _City = ds.Tables[0].Rows[0]["CityId"].ToString();
            _Area = ds.Tables[0].Rows[0]["Area"].ToString();
            _Pincode = ds.Tables[0].Rows[0]["Pincode"].ToString();
            _Mobileno = ds.Tables[0].Rows[0]["Mobileno"].ToString();
            _CusType = ds.Tables[0].Rows[0]["custype"].ToString();
        }
        var JsonData = new
        {
          Firstname= _Firstname,
          Lastname = _Lastname,
            Address = _Address,
            City = _City,
            Area = _Area,
            Pincode = _Pincode,
            Mobileno = _Mobileno,
            CusType= _CusType



        };
        return ser.Serialize(JsonData);

    }
}