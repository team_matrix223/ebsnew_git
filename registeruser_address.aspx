﻿<%@ Page Title="" EnableEventValidation="false" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="registeruser_address.aspx.cs" Inherits="registeruser_address" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="customcss/registeruser_address.css" rel="stylesheet" />
    <style>
        .box5 {
            border: 1px solid #d6d3d3;
            border-radius: 5px;
            box-shadow: 5px 5px 5px #888888;
            width: 250px;
            margin: 10px;
            text-decoration: none;
            float: left;
            padding: 5px;
        }
    </style>
    <script src="js/customjs/registeruser_address.js"></script>
    <asp:HiddenField ID="hdn_addressid" ClientIDMode="Static" runat="server" />
<div  class="col-md-12" style=" border-radius: 1px; border:solid 1px silver ;">
 
 <div class="row">
 <div class="col-md-12">
<h2 class="select-deleviery-address-heading">SELECT DELIVERY ADDRESS</h2>
 <hr  style="margin:2px;"/>

 <div id="dvDeliveryAddresses" >
 
  <asp:Literal ID="ltDeliveryAddress" runat="server"></asp:Literal>
 </div>
 
 

  

 </div>
 
 </div>

 <div class="next-btn-div">
    <asp:Button ID="btnsubmit" CssClass="signUp_button"  runat="server" Text="Next" OnClick="btnsubmit_Click" />
         <asp:Button ID="btnadd_address" Visible="false" CssClass="signUp_button"  runat="server" Text="Add Address" OnClick="btnadd_address_Click" />
</div>
</div>
</asp:Content>

