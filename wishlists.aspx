﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="wishlists.aspx.cs" Inherits="wishlists" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="customcss/wishlists.css" rel="stylesheet" />
    <script src="js/customjs/wishlist.js"></script>
	<div class="container-fluid">
		<div class="product-main-div">
					<div class="title">Your Wishlist</div>
<%--		<div class="whislist-time">4 itmes</div>--%>
		<div class="row">
			
			    <asp:Repeater ID="rpt_wishlist" runat="server">
                 <ItemTemplate>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="product-div">
					<span class="close-icon btndel" data="<%#Eval("variationid") %>"><i class="fa fa-times" aria-hidden="true"></i></span>
					<div class="product-image">
						<img class="medium-img" src="images/product/<%#Eval("photourl") %>" />
					</div>
					<div class="product-desc"><%#Eval("name") %></div>
					<div class="product-price">	₹<%#Eval("price") %></div>
					<div class="prodcuct-button"><a href="productdetail.aspx?p=<%#Eval("productid") %>&v=<%#Eval("variationid") %>">BUY NOW</a></div>
				</div>
			</div>
                    </ItemTemplate>
                    </asp:Repeater>


			
			
			
			
		</div>
			
			</div>
		<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">

					<div class="continue-section">
						<div class="add-more">Add more products to your wishlist</div>
						<div class="continue-btn">
							<a>Continue Shoppinng</a>
						</div>
					</div>
				</div>
			</div>

	</div>
</asp:Content>

