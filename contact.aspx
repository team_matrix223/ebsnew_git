﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="contact.aspx.cs" Inherits="contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="customcss/contact.css" rel="stylesheet" />
	<div class="container-fluid">
		  <h1 class="responsiveProductListHeader_title">Contact us</h1>
		<div class="contact-banner-section">
			<img src="images/about/Contact_Us.jpg" />
		</div>
		<div class="row">
			<div class="col-md-8 col-sm-8 col-xs-12">
					<div class="contact-form-section">
						<h1 class="responsiveProductListHeader_title form-title">Write to us</h1>
						<form>
							   <div class="form-group">
								<label for="exampleInputEmail1" class="accountSignUp_label">Name</label>
								<input type="text" class="form-control" id="exampleInputEmail1" >
							  </div>
							  <div class="form-group">
								<label for="exampleInputEmail1" class="accountSignUp_label">Email address</label>
								<input type="email" class="form-control" id="exampleInputEmail1" >
							  </div>
							  <div class="form-group">
								<label for="exampleInputPassword1" class="accountSignUp_label">Phone Number</label>
								<input type="text" class="form-control" id="exampleInputPassword1" >
							  </div>
							  <div class="form-group">
								<label for="exampleInputFile" class="accountSignUp_label">What’s on your mind?</label>
								<textarea class="form-control" rows="3"></textarea>
							  </div>
							  
							  <button type="submit" class="contact-btn">Send Messenger</button>
						</form>
					</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="bulk-section">
					<p class="accountSignUp_label">For any Bulk Orders or Purchase Concern or Any Query or Inquiries, kindly fill out the Contact Form or WhatsApp us at <a href="tel:9189685 30136">+91 8968530136</a></p>
				</div>
				<div class="contact-section">
					<p class="accountSignUp_label"><strong>Phone: </strong>+91 8968530136 (10am to 7pm)</p>
					<p class="accountSignUp_label"><strong>WhatsApp:</strong> +91 8968530136</p>
					<p class="accountSignUp_label"><strong>Email:</strong> contact@ebs1952.com</p>
				</div>
			</div>
		</div>
	</div>
</asp:Content>

