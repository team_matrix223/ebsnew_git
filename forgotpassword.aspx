﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="forgotpassword.aspx.cs" Inherits="forgotpassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="customcss/forgotpassword.css" rel="stylesheet" />
	<div class="container-fluid">
		<div class="row">
			<div class="form-div">
				<div class="col-md-12 col-sm-12 col-xs-12">
					 <label class="accountLogin_label">
							EmailId:
							<asp:TextBox ID="tbemailid" runat="server" CssClass="accountLogin_input"></asp:TextBox>
					 </label>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12">
					 <label class="accountLogin_label">
							New Password:
							<asp:TextBox ID="tbpwd" runat="server" CssClass="accountLogin_input" TextMode="Password"></asp:TextBox>
					 </label>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<asp:Button ID="btnsubmit" runat="server" CssClass="accountLogin_button btn-login" Text="Submit" OnClick="btnsubmit_Click" />
				</div>
			</div>
		</div>
	</div>
        
</asp:Content>

