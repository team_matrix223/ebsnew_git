﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class productdetail : System.Web.UI.Page
{
    static string ProductQueryString = "", VariationQueryString = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnProductId.Value = Request.QueryString["p"] != null ? Request.QueryString["p"] : "0";
            hdnVariationId.Value = Request.QueryString["v"] != null ? Request.QueryString["v"] : "0";
            GetImgs();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "GetData();", true);
            GetRelatedProduct(Convert.ToInt32(hdnProductId.Value));
        }

    }

    //[WebMethod]
    //public static string GetRelatedProducts(int CategoryId)
    //{
    //    StringBuilder str = new StringBuilder();

    //}

    public void GetImgs() {

        DataSet ds = new DataSet();
        ds = new ProductsBLL().GetImgs(Convert.ToInt32(hdnProductId.Value));
        string imgs = "";
        string color_dd = "";
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            imgs += "<li><img class='img second_img' color="+ ds.Tables[0].Rows[i]["Color"].ToString() + " id=" + ds.Tables[0].Rows[i]["variationid"].ToString() + " src='images/product/" + ds.Tables[0].Rows[i]["photourl"].ToString() + "' firstimg='images/product/" + ds.Tables[0].Rows[i]["photourl"].ToString() + "' secondimg='images/product/" + ds.Tables[0].Rows[i]["photourl2"] + "'></li>";
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Color"].ToString()))
            {
                dv_color_dd.Visible = true;
                 color_dd += "<option value=" + ds.Tables[0].Rows[i]["variationid"].ToString() + ">" + ds.Tables[0].Rows[i]["Color"].ToString() + "</option>";
            }
           

        }
        ltr_imglist.Text = imgs;
        ltr_color_dd.Text = color_dd;


    }


    [WebMethod]
    public static string GetData(int ProductId, int VariationId,int attr_id)
    {
        string Url = "";
        string name = "";
        string mrp = "";
        string price = "";
        string long_desc = "";
        string short_desc = "";
        string category_id = "";
        decimal left_stock = 0;
        decimal total_left_stock = 0;
        string ImgUrl = "";
        string attr_id_db = "";
        SqlDataReader rd = null;
        string SessionId = HttpContext.Current.Session[Constants.CartSession].ToString();
        rd = new ProductsBLL().GetProductDetailByProductIdZoom(SessionId, ProductId, VariationId, attr_id);
        if (rd.Read())
        {
            mrp =Math.Round(Convert.ToDecimal(rd["Mrp"])).ToString();
            long_desc = rd["Description"].ToString();
            short_desc = rd["ShortDescription"].ToString();
            price = Math.Round(Convert.ToDecimal(rd["Price"])).ToString();
            Url = rd["PhotoUrl"].ToString();
            name = rd["Name"].ToString();
            category_id = rd["Categoryid"].ToString();
            attr_id_db = rd["attr_id"].ToString();
            //left_stock= rd["left_stock"].ToString()==""?0:Convert.ToDecimal(rd["left_stock"].ToString());
            total_left_stock = rd["total_left_stock"].ToString() == "" ? 0 : Convert.ToDecimal(rd["total_left_stock"].ToString());
        }

        DataSet ds = new DataSet();
        ds = new ProductsBLL().GetAttrByProductId(ProductId, VariationId);
        string Attributes = "";
        if (ds.Tables[0].Rows.Count>0)
        {

   

        Attributes = "<option value=0>-Please Select-</option>";
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            Attributes += "<option value=" + ds.Tables[0].Rows[i]["attr_id"].ToString() + "> " + ds.Tables[0].Rows[i]["Title"].ToString() + "</option>";
        }
        }
        if (ds.Tables[1].Rows.Count > 0)
        {

            for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
            {
                ImgUrl += "<li><img class='rel_img' id=" + ds.Tables[1].Rows[i]["attr_id"].ToString() + " src='images/product/" + ds.Tables[1].Rows[i]["photourl"].ToString() + "'></li>";
            }
        }

       
        var JsonData = new
        {
            PhotoUrl = Url,
            Name = name,
            MRP = mrp,
            LongDesc = long_desc,
            ShortDesc = short_desc,
            Price = price,
            CategoryId= category_id,
            Attributes= Attributes,
            //left_stock = left_stock,
            relImgUrl = ImgUrl,
            attr_id_db= attr_id_db,
            total_left_stock = total_left_stock

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string AddToWishList(string req, int Productid, int Variationid)
    { int userid = HttpContext.Current.Session[Constants.UserId] != null ? Convert.ToInt32(HttpContext.Current.Session[Constants.UserId]) : 0;
        string flag = "0";
        if (userid!=0)
        {
            new ProductsDAL().InsertWishList(req,Productid, Variationid, userid.ToString());
        }
        else
        {
             flag = "1";
        }
        return flag;

    }

    [WebMethod]
    public static string FirstTimeATC(string vid, string qty, string st, string type,int attr_id)
    {


        string m_Status = "Plus";

        if (st == "m")
        {
            m_Status = "Minus";

        }



        Int16 IsError = 0;




        int m_Qty = CommonFunctions.ValidateQuantity(qty);
        if (!CommonFunctions.IsValidProductId(vid))
        {
            IsError = 1;
        }

        Calc objCalc = new Calc();
        UserCart objUserCart = new UserCart();
        string html = "";
        string ProductHTML = "";
        if (IsError == 0)
        {
            objUserCart.SessionId = HttpContext.Current.Session[Constants.CartSession].ToString();
            objUserCart.ProductId = 0;
            objUserCart.CustomerId = HttpContext.Current.Session[Constants.UserId] != null ? Convert.ToInt32(HttpContext.Current.Session[Constants.UserId]) : 0;
            objUserCart.VariationId = Convert.ToInt64(vid);
            objUserCart.ProductDesc = "";
            objUserCart.Qty = m_Qty;
            objUserCart.Price = 0;
            objUserCart.type = type;
            objUserCart.attr_id = attr_id;

            html = new CartBLL().UserCartInsertUpdate(objUserCart, m_Status, objCalc, out ProductHTML);

        }
        var JsonData = new
        {

            qty = objUserCart.Qty,
            error = IsError,
            cartHTML = html,
            Calc = objCalc,
            productHTML = ProductHTML
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);

    }
    public void GetRelatedProduct(int Productid) {


        rpt_relatedpro.DataSource = new ProductsBLL().GetRelatedProducts(Productid);
        rpt_relatedpro.DataBind();

    }

}