﻿<%@ Page Title="" Language="C#" EnableEventValidation="false" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="new_page.aspx.cs" Inherits="new_page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="customcss/new_page.css" rel="stylesheet" />	
<link href="customcss/shop-page.css" rel="stylesheet" />
<link href="customcss/brand-page.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="s1.thcdn.com/www/styles/css/lfint/rebrand/screen-7ed573230a.css" type="text/css" media="screen" />
<link rel="stylesheet" href="s1.thcdn.com/www/styles/css/lfint/sharded-font/font-face-c334387487.css" type="text/css" media="screen" />
<link rel="stylesheet" href="s1.thcdn.com/takeover-manager/edd7c0a978/lfint/lfint-takeover.css" type="text/css" media="screen" />
       <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="js/customjs/new_page.js"></script>

        <asp:HiddenField ID="hdnLevel1" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="hdnLevel2" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="hdnLevel3" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="hdnBrandId" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="hdnGroupId" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="hdnSubGroupId" runat="server" ClientIDMode="Static"/>
    <asp:HiddenField ID="hdnGetMethod" runat="server" ClientIDMode="Static"/>
        <asp:HiddenField ID="hdnbrands" runat="server" ClientIDMode="Static"/>
      <asp:HiddenField ID="hdnminprice" runat="server" ClientIDMode="Static"  Value="-1"/>
        <asp:HiddenField ID="hdnmaxprice" runat="server" ClientIDMode="Static" Value="-1"/>
        <asp:HiddenField ID="hdnsliderminprice" runat="server" ClientIDMode="Static"/>
        <asp:HiddenField ID="hdnslidermaxprice" runat="server" ClientIDMode="Static"/>
<%--    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:
    
     ID="UpdatePanel1" runat="server">

      <ContentTemplate>--%>
   
	<div data-component="ajaxFacets" data-componentload="helper" class="ajax-facets cf">
        <div style="display:none">
           <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" ClientIDMode="Static"/>
            </div>
			<!-- widget responsiveSlot1 start -->
				<div class="responsiveSlot1">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner" role="listbox">
                            <asp:Literal ID="ltr_slider" runat="server"></asp:Literal>
						</div>

						<!-- Controls -->
						<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
				<!-- widget responsiveSlot1 stop -->

		<div class="js-widgets-wrapper widgets-wrapper">
			<div class="panel-head">
			</div>
		</div>


		<div data-component="" data-componentload="helper" data-product-list-wrapper="">
			<span data-elysium-property-name="facetsMobileEditRefinementText" data-elysium-property-value="Edit Refinement"></span>
			<span data-elysium-property-name="facetsMobileRefineText" data-elysium-property-value="Refine"></span>
			<div class="responsiveProductListPage" data-component="responsiveProductListPage" data-section-path="health-beauty/trends/glossy-make-up" data-is-search="false" data-price-facet-category="" data-horizontal-facets="false" data-nav-offset="48">

				<main id="mainContent" class="responsiveProductListPage_mainContent responsiveProductListPage_mainContent_withFacets" aria-labelledby="responsive-product-list-title">
					

					<div class="responsiveProductListPage_sortAndPagination ">
				<%--		<div class="category-name-div">
							<h1 id="responsive-product-list-title" class="responsiveProductListHeader_title">
								Glossy Make-Up
							</h1>
						</div>
						<div class="responsiveProductListPage_sort">
							<div class="responsiveSort">
								<span class="responsiveSort_label">Sort by</span>
								<svg class="responsiveSort_selectSVG" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<polygon points="16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793"></polygon>
								</svg>

								<select class="responsiveSort_select" data-sort-by="" aria-label="Sort by">
									<option value="default" selected="">Default</option>
									<option value="salesRank">Best Selling</option>
									<option value="priceAscending">Price: Low to high</option>
									<option value="priceDescending">Price: High to low</option>
									<option value="title">A - Z</option>
									<option value="releaseDate">Newest Arrivals</option>
									<option value="percentageDiscount">Percentage Discount</option>
									<option value="reviewCount_auto_int">Review Count</option>

								</select>
							</div>


						</div>--%>
						<button type="button" class="visually-hidden responsiveProductListPage_goToRefineSectionButton">Go to refine section</button>
			
						<div class="responsiveProductListPage_refine ">
	
							<div class="mob-ref">Refine<i class="fa fa-bars"></i></div>
							<div class="mob-side-menu">
								<button type="button" class="price responsiveFacets_overrideButtonStyle responsiveFacets_sectionHead" data-selected="false" aria-expanded="true" tabindex="0">
									<h3 class="responsiveFacets_sectionTitle"><span class="visually-hidden"> </span>Price</h3><i class="fa fa-angle-down" aria-hidden="true"></i>

								</button>
																		<div class="responsiveFacets_sectionContentWrapper price-wrap">
											<fieldset class="responsiveFacets_sectionContent " aria-hidden="false">
												<legend class="visually-hidden responsiveFacets_sectionContent_legend"> Price</legend>
												<label class="responsiveFacets_sectionItemLabel">
																 <div class="price" >
        
    <p style="padding:10px 0 5px 0">
  
  <input type="text" id="amount2" readonly style="border:0; color:BLACK; font-weight:bold;FONT-WEIGHT: NORMAL">
</p>
 
<div id="slider-range2"></div>
												</label>
	
                                                		</div>
	
                                                		</fieldset>
											    </div>
								
								<button type="button" class="brand responsiveFacets_overrideButtonStyle responsiveFacets_sectionHead" data-selected="false" aria-expanded="true" tabindex="0">
									<h3 class="responsiveFacets_sectionTitle"><span class="isually-hidden"> </span>Brand</h3>
									<i class="fa fa-angle-down" aria-hidden="true"></i>
								</button>
								<div class="responsiveFacets_sectionContentWrapper brand-wrap">
									<fieldset class="responsiveFacets_sectionContent " aria-hidden="false">
										<legend class="visually-hidden responsiveFacets_sectionContent_legend"> Brand</legend>
										<label class="responsiveFacets_sectionItemLabel">
											<input type="checkbox" class="responsiveFacets_sectionItemCheckbox" name="en_brand_content" data-facet-value="3INA+Makeup" aria-label="3INA Makeup (10 available products)" tabindex="0">
											<span class="responsiveFacets_sectionItem " data-facet-key="en_brand_content" data-facet-value="3INA+Makeup">
												<span class="responsiveFacets_sectionItemValue " aria-hidden="true">
													3INA Makeup (10)
												</span>
											</span>
										</label>
										

									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</fieldset>
									</div>			
</div>

						</div>
					</div>


			
						<div class="productListProducts" data-horizontal-facets="false">
						<h2 id="product-list-heading" class="visually-hidden">Products</h2>
                            <div id="facet-loading-wrap" style="display:none;text-align: center;"><div id="facet-loading"><img src="images/loading_brown.gif"><span>Loading...</span></div><div id="facet-loading-mask"></div></div>
						<ul class="productListProducts_products" id="productlist" aria-labelledby="product-list-heading">
                            <asp:Repeater ID="RptProduct" runat="server">
                                <ItemTemplate>

                       
                <li class='productListProducts_product 'data-horizontal-facets='false'>
              <div class='productBlock 'data-component='productBlock' rel='11519320' role='group' aria-labelledby='productBlock_productName-11519320'>
           <div><div class='productBlock_imageLinkWrapper'><a class='productBlock_link' href='productdetail.aspx?p=<%# Eval("ProductId") %>&v=<%# Eval("VariationId") %>'><div class='productBlock_imageContainer'>
           <img src = 'images/product/<%# Eval("PhotoUrl") %>' data-alt-src='' firstimg="images/product/<%#Eval("photourl") %>" secondimg="images/product/<%#Eval("second_photourl") %>" class='productBlock_image second_img' data-track='product-image' alt=<%# Eval("Name") %>></div></a></div><a class='productBlock_link' href='productdetail.aspx?p=<%# Eval("ProductId") %>&v=<%# Eval("VariationId") %>'>
           <div class='productBlock_title'><h3 class='productBlock_productName' data-track='product-title' id='productBlock_productName-11519320'><%# Eval("Name") %></h3></div></a>
           <div class='productBlock_priceBlock' data-is-link='true'>
           <div class='productBlock_price'><span class='productBlock_priceCurrency' content='GBP'></span><span class='productBlock_priceValue' content=<%# Eval("Price") %>>&#8377;<%# Eval("Price") %></span></div></div></div>
           <div class='productBlock_actions'><span data-component='productQuickbuy'></span>
           <span id="a_<%# Eval("VariationId") %>" class='productBlock_button productBlock_button-moreInfo btnAddToCart' data-sku='11519320' data-open-product-quickbuy='' data-from-wishlist='false' data-subscribe-and-save=''>
           ADD TO CART<span class='visually-hidden'><%# Eval("Name") %></span></span></div></div>

                </li>
                                     </ItemTemplate>

                            </asp:Repeater>
						</ul>
  <div style="margin-top: 20px;" class="page-no-div-new">
                    <table class="page-no" style="width: 660px;">
                        <tr>
           
                            <td>
                                <asp:LinkButton ID="lbPrevious" runat="server" OnClick="lbPrevious_Click" CssClass="pre-page"><img src="images/slider/left1.png" /></asp:LinkButton>
                            </td>
                            <td>
                                <asp:DataList ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand" OnItemDataBound="rptPaging_ItemDataBound" RepeatDirection="Horizontal">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbPaging" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CommandName="newPage" Text='<%# Eval("PageText") %> ' CssClass="new-page"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </td>
                            <td>
                                <asp:LinkButton ID="lbNext" runat="server" OnClick="lbNext_Click" CssClass="next-page"><img src="images/slider/right1.png" /></asp:LinkButton>
                            </td>
             
                            <td>
                                <asp:Label ID="lblpage" runat="server" Text="" CssClass="page-label"></asp:Label>
                            </td>
                        </tr>
                    </table>

                </div>
                            <div  style="text-align: center;">
                             <img id="loader" alt="" src="images/loading.gif" style="display: none;" />
                                </div>
					</div>

					

					<div class="responsiveProductListPage_loaderOverlay" data-show="false">
						<div class="responsiveProductListPage_loader"></div>
					</div>
				</main>

				<aside class="responsiveProductListPage_facets" aria-labelledby="responsive-facets-title">
					<div class="responsiveFacets">

						<div class="responsiveFacets_container responsiveFacets_container-transitioned" data-show="false" data-child-open="false">
							<div class="responsiveFacets_head">
								<h2 id="responsive-facets-title" class="responsiveFacets_title">Refine</h2>

								<button type="button" class="responsiveFacets_overrideButtonStyle responsiveFacets_menuClose" aria-label="Close" title="Close" tabindex="0">
									<svg width="40" height="40" xmlns="http://www.w3.org/2000/svg">
										<g fill-rule="nonzero" stroke-width="2" fill="none">
											<path d="M13 13l14 14M27 13L13.004 27">
											</path>
										</g>
									</svg>
								</button>
							</div>

							<button type="button" class="visually-hidden responsiveFacets_goToProductSectionButton" tabindex="0">Go to product section</button>

							<div class="responsiveFacets_content">

								<div class="responsiveFacets_section">
									<button type="button" class="price responsiveFacets_overrideButtonStyle responsiveFacets_sectionHead-mobile" tabindex="0">
										Price
										<svg class="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<polygon points="16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793"></polygon>
										</svg>


									</button>

									<div class="responsiveFacets_sectionContainer accordionWidget_expanded" data-show="false" data-closed="false" data-selected="false" style="max-height: none;">

										<div class="responsiveFacets_sectionHeadWrapper">

											<button type="button" class="price responsiveFacets_overrideButtonStyle responsiveFacets_sectionHead" data-selected="false" aria-expanded="true" tabindex="0">
												<h3 class="responsiveFacets_sectionTitle"><span class="visually-hidden"> </span>Price</h3>

												<span class="responsiveFacets_sectionCaret" data-closed="false">
													<svg class="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
														<polygon points="16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793"></polygon>
													</svg>

												</span>
											</button>

											<div class="responsiveFacets_mobileSectionTitle">
												<span class="responsiveFacets_sectionTitle">
													Price
												</span>

												<span class="responsiveFacets_sectionCaret" data-closed="false">
													<svg class="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
														<polygon points="16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793"></polygon>
													</svg>

												</span>
											</div>

											<button type="button" class="responsiveFacets_overrideButtonStyle responsiveFacets_sectionBackArrow" tabindex="0" aria-label="Back" title="Back">
												<svg class="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<polygon points="16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793"></polygon>
												</svg>

											</button>

										</div>

											<div class="responsiveFacets_sectionContentWrapper price-wrap">
											<fieldset class="responsiveFacets_sectionContent " aria-hidden="false">
												<legend class="visually-hidden responsiveFacets_sectionContent_legend"> Price</legend>
												<label class="responsiveFacets_sectionItemLabel">
																 <div class="price" >
        
    <p style="padding:10px 0 5px 0">
  
  <input type="text" id="amount" readonly style="border:0; color:BLACK; font-weight:bold;FONT-WEIGHT: NORMAL">
</p>
 
<div id="slider-range"></div>
												</label>
	
                                                		</div>
	
                                                		</fieldset>
											    </div>
									</div>
									<button type="button" class="brand responsiveFacets_overrideButtonStyle responsiveFacets_sectionHead-mobile" tabindex="0">
										Brand
										<svg class="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<polygon points="16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793"></polygon>
										</svg>


									</button>

									<div class="responsiveFacets_sectionContainer accordionWidget_expanded" data-show="false" data-closed="false" data-selected="false" style="max-height: none;">

										<div class="responsiveFacets_sectionHeadWrapper">

											<button type="button" class="brand responsiveFacets_overrideButtonStyle responsiveFacets_sectionHead" data-selected="false" aria-expanded="true" tabindex="0">
												<h3 class="responsiveFacets_sectionTitle"><span class="visually-hidden"> </span>Brand</h3>

												<span class="responsiveFacets_sectionCaret" data-closed="false">
													<svg class="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
														<polygon points="16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793"></polygon>
													</svg>

												</span>
											</button>

											<div class="responsiveFacets_mobileSectionTitle">
												<span class="responsiveFacets_sectionTitle">
													Brand
												</span>

												<span class="responsiveFacets_sectionCaret" data-closed="false">
													<svg class="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
														<polygon points="16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793"></polygon>
													</svg>

												</span>
											</div>

											<button type="button" class="responsiveFacets_overrideButtonStyle responsiveFacets_sectionBackArrow" tabindex="0" aria-label="Back" title="Back">
												<svg class="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<polygon points="16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793"></polygon>
												</svg>

											</button>

										</div>

										<div class="responsiveFacets_sectionContentWrapper brand-wrap">
											<fieldset class="responsiveFacets_sectionContent " aria-hidden="false">
												<legend class="visually-hidden responsiveFacets_sectionContent_legend"> Brand</legend>
                                              
                                                <asp:CheckBoxList ID="chk_brands" runat="server"  DataTextField="BrandName"   
            DataValueField="BrandId" AutoPostBack="True" RepeatLayout="OrderedList" OnSelectedIndexChanged="chk_brands_SelectedIndexChanged">
                                         
                                                </asp:CheckBoxList>

                                    <%--            <asp:Repeater ID="rpt_brands" runat="server">

                                                    <ItemTemplate>

												<label class="responsiveFacets_sectionItemLabel">
													<input type="checkbox" class="responsiveFacets_sectionItemCheckbox" name="brandfilter" data-facet-value=<%# Eval("BrandId") %> value="<%# Eval("BrandId") %>" aria-label="3INA Makeup (10 available products)" tabindex="0">
													<span class="responsiveFacets_sectionItem " data-facet-key="en_brand_content" data-facet-value=	<%# Eval("BrandName") %> >
														<span class="responsiveFacets_sectionItemValue " aria-hidden="true">
														<%# Eval("BrandName") %> 
														</span>
													</span>
												</label>
                                                        
                                                    </ItemTemplate>
                                                </asp:Repeater>--%>
					

											</fieldset>
										</div>
									</div>
									<button type="button" class="save responsiveFacets_overrideButtonStyle responsiveFacets_sectionHead-mobile" tabindex="0">
										SavingswBox="0 0 24 24" version="1.1">
											<polygon points="16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793"></polygon>
										</svg>


									</button>


								</div>
							</div>

							<div class="responsiveFacets_saveContainer">
								<button type="button" class="responsiveFacets_save" tabindex="0">Save &amp; View</button>
							</div>

							<div class="responsiveFacets_error " data-show="false" aria-hidden="true">We're sorry, we couldn't load the products. Please try again.</div>
						</div>
						<button type="button" class="visually-hidden responsiveFacets_goToProductSectionButton responsiveFacets_goToProductSectionButton_bottom">Go to product section</button>
					</div>

				</aside>
			</div>




			<span data-component="productQuickbuy"></span>
			<div class="addedToBasketModal" data-component="addedToBasketModal" data-cdn-url="https://s2.thcdn.com//" data-secure-url="https://www.ebs1952.com/" role="dialog" tabindex="-1">

				<span data-elysium-property-name="maxQuantityReached" data-elysium-property-value="Item cannot be added to your basket. This product is limited to a quantity of %d per order."></span>
				<span data-elysium-property-name="partialMaxQuantityReached" data-elysium-property-value="%1 items cannot be added to your basket. This product is limited to a quantity of %2 per order."></span>

				<div class="addedToBasketModal_container" role="dialog" tabindex="-1" aria-labelledby="added-to-basket-modal-title0">
					<div class="addedToBasketModal_titleContainer">
						<h2 id="added-to-basket-modal-title0" class="addedToBasketModal_title">
							Added to your basket
						</h2>

						<button type="button" class="addedToBasketModal_closeContainer" aria-label="Close" title="Close" data-close="">
							<svg class="addedToBasketModal_close" viewBox="-3 -4 20 20" xmlns="http://www.w3.org/2000/svg">
								<path d="M8.414 7l5.293-5.293c.391-.391.391-1.023 0-1.414s-1.023-.391-1.414 0l-5.293 5.293-5.293-5.293c-.391-.391-1.023-.391-1.414 0s-.391 1.023 0 1.414l5.293 5.293-5.293 5.293c-.391.391-.391 1.023 0 1.414.195.195.451.293.707.293.256 0 .512-.098.707-.293l5.293-5.293 5.293 5.293c.195.195.451.293.707.293.256 0 .512-.098.707-.293.391-.391.391-1.023 0-1.414l-5.293-5.293"></path>
							</svg>

						</button>
					</div>

					<div class="addedToBasketModal_error " data-error="">
						Sorry, there seems to have been an error. Please try again.
					</div>

					<div class="addedToBasketModal_error " data-quantity-limit-reached="">
					</div>

					<div class="addedToBasketModal_warning" data-quantity-limit-partially-reached="">
					</div>

					<div class="addedToBasketModal_item">
						<div class="addedToBasketModal_imageContainer">
							<a href="" data-product-url="">
								<img src="//:0" alt="Loading..." class="addedToBasketModal_image" data-product-image="">
							</a>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<div class="addedToBasketModal_itemDetails">
							<a href="" data-product-url="" class="addedToBasketModal_itemName" data-product-title="">Product Name</a>
							<p class="addedToBasketModal_itemQuantity">
								Quantity
								<span class="addedToBasketModal_itemQuantity addedToBasketModal_itemQuantity-number" data-product-quantity=""></span>
							</p>


							<p class="addedToBasketModal_itemPrice" data-product-price=""></p>
						</div>
					</div>

					<div class="addedToBasketModal_subtotal">
						<p class="addedToBasket_subtotalTitle">
							Subtotal:
							<span class="addedToBasket_subtotalItemCount">
								(<span class="addedToBasket_subtotalItemCount-number" data-basket-total-items=""></span>
								items in your basket)
							</span>
						</p>

						<p class="addedToBasket_subtotalAmount" data-basket-total=""></p>
					</div>

					<div class="addedToBasketModal_loyaltyPointsMessage">
					</div>

					<div class="addedToBasketModal_ctas">

						<div class="addedToBasketModal_ctaContainerLeft">
							<button type="button" class="addedToBasket_continueShoppingButton" data-continue-shopping="">
								Continue Shopping
							</button>
						</div>
						<div class="addedToBasketModal_ctaContainerRight">
							<a href="/my.basket" class="addedToBasketModal_viewBasketButton js-e2e-quickView-basket" data-view-basket="">
								View Basket
							</a>
						</div>
					</div>

					<div class="addedToBasketModal_productRecommendations" data-recommendations="">
						<span class="addedToBasketModal_loading">
							<span class="addedToBasketModal_loadingSpinny"></span>
						</span>
					</div>
				</div>
			</div>






		</div>



	</div>
<%--</ContentTemplate>
    </asp:UpdatePanel>--%>
	<script>
		$(document).ready(function () {
			var price_val = 0;
			var brand_val = 0;
			var save_val = 0;
			var skin_val = 0;
			var s_tool_val = 0;
			var s_con_val = 0;
			var make_p_val = 0;
			var make_t_val = 0;
			var make_c_val = 0;
			var body_p_val = 0;
			var body_t_val = 0;
			var mob_icon_val = 0;
			var m_price_val = 0;
			var m_brand_val = 0;
			var m_save_val = 0;
			var m_skin_val = 0;
			var m_s_tool_val = 0;
			var m_s_con_val = 0;
			var m_make_p_val = 0;
			var m_make_t_val = 0;
			var m_make_c_val = 0;
			var m_body_p_val = 0;
			var m_body_t_val = 0;
			

  $(".price").click(function(){
  	$(".price-wrap").toggle();
  	if (price_val == 0) {
  		$(".price").attr("aria-expanded", "false");
  		price_val = 1;
  	}
  	else  {
  		$(".price").attr("aria-expanded", "true");
  		price_val = 0;
  	}
  
  });
  $(".brand").click(function () {

  	$(".brand-wrap").toggle();
  	if (brand_val == 0) {
  		$(".brand").attr("aria-expanded", "false");
  		brand_val = 1;
  	}
  	else {
  		$(".brand").attr("aria-expanded", "true");
  		brand_val = 0;
  	}
  
  });
  $(".save").click(function () {
  	$(".save-wrap").toggle();
  	if (save_val == 0) {
  		$(".save").attr("aria-expanded", "false");
  		save_val = 1;
  	}
  	else {
  		$(".save").attr("aria-expanded", "true");
  		save_val = 0;
  	}

  });
  $(".skin").click(function () {
  	$(".skin-wrap").toggle();
  	if (skin_val == 0) {
  		$(".skin").attr("aria-expanded", "false");
  		skin_val = 1;
  	}
  	else {
  		$(".skin").attr("aria-expanded", "true");
  		skin_val = 0;
  	}

  });
  $(".s-tool").click(function () {
  	$(".s-tool-wrap").toggle();
  	if (s_tool_val == 0) {
  		$(".s-tool").attr("aria-expanded", "false");
  		s_tool_val = 1;
  	}
  	else {
  		$(".s-tool").attr("aria-expanded", "true");
  		s_tool_val = 0;
  	}

  });
  $(".s-con").click(function () {
  	$(".s-con-wrap").toggle();
  	if (s_con_val == 0) {
  		$(".s-con").attr("aria-expanded", "false");
  		s_con_val = 1;
  	}
  	else {
  		$(".s-con").attr("aria-expanded", "true");
  		s_con_val = 0;
  	}
  });
  $(".make-p").click(function () {
  	$(".make-p-wrap").toggle();
  	if (make_p_val == 0) {
  		$(".make-p").attr("aria-expanded", "false");
  		make_p_val = 1;
  	}
  	else {
  		$(".make-p").attr("aria-expanded", "true");
  		make_p_val = 0;
  	}
  });
  $(".make-t").click(function () {
  	$(".make-t-wrap").toggle();
  	if (make_t_val == 0) {
  		$(".make-t").attr("aria-expanded", "false");
  		make_t_val = 1;
  	}
  	else {
  		$(".make-t").attr("aria-expanded", "true");
  		make_t_val = 0;
  	}
  });
  $(".make-c").click(function () {
  	$(".make-c-wrap").toggle();
  	if (make_c_val == 0) {
  		$(".make-c").attr("aria-expanded", "false");
  		make_c_val = 1;
  	}
  	else {
  		$(".make-c").attr("aria-expanded", "true");
  		make_c_val = 0;
  	}
  });
  $(".body-p").click(function () {
  	$(".body-p-wrap").toggle();
  	if (body_p_val == 0) {
  		$(".body-p").attr("aria-expanded", "false");
  		body_p_val = 1;
  	}
  	else {
  		$(".body-p").attr("aria-expanded", "true");
  		body_p_val = 0;
  	}
  });
  $(".body-t").click(function () {
  	$(".body-t-wrap").toggle();
  	if (body_t_val == 0) {
  		$(".body-t").attr("aria-expanded", "false");
  		body_t_val = 1;
  	}
  	else {
  		$(".body-t").attr("aria-expanded", "true");
  		body_t_val = 0;
  	}
  });

  $(".mob-ref").click(function () {
  	$(".mob-side-menu").toggle();
  	if(mob_icon_val==0)
  	{
  		
  		$(".mob-ref i").attr("class", "fa fa-times");
  		mob_icon_val = 1;
  	}
  	else {
  		mob_icon_val = 1;
  		$(".mob-ref i").attr("class", "fa fa-bars");
  		mob_icon_val = 0;	
  	}
  });
  $(".mob-side-menu .price").click(function () {
  
  	if (m_price_val == 0) {
  		$(".mob-side-menu .price i").attr("class", "fa fa-angle-up");
  		m_price_val = 1;
  	}
  	else {
  		$(".mob-side-menu  .price i").attr("class", "fa fa-angle-down");
  		m_price_val = 0;
  	}

  });
  $(".mob-side-menu .brand").click(function () {

  	if (m_brand_val == 0) {
  		$(".mob-side-menu .brand i").attr("class", "fa fa-angle-up");
  		m_brand_val = 1;
  	}
  	else {
  		$(".mob-side-menu  .brand i").attr("class", "fa fa-angle-down");
  		m_brand_val = 0;
  	}

  });
  $(".mob-side-menu .save").click(function () {

  	if (m_save_val == 0) {
  		$(".mob-side-menu .save i").attr("class", "fa fa-angle-up");
  		m_save_val = 1;
  	}
  	else {
  		$(".mob-side-menu  .save i").attr("class", "fa fa-angle-down");
  		m_save_val = 0;
  	}

  });
  $(".mob-side-menu .skin").click(function () {

  	if (m_skin_val == 0) {
  		$(".mob-side-menu .skin i").attr("class", "fa fa-angle-up");
  		m_skin_val = 1;
  	}
  	else {
  		$(".mob-side-menu  .skin i").attr("class", "fa fa-angle-down");
  		m_skin_val = 0;
  	}

  });
  $(".mob-side-menu .s-tool").click(function () {

  	if (m_s_tool_val == 0) {
  		$(".mob-side-menu .s-tool i").attr("class", "fa fa-angle-up");
  		m_s_tool_val = 1;
  	}
  	else {
  		$(".mob-side-menu  .s-tool i").attr("class", "fa fa-angle-down");
  		m_s_tool_val = 0;
  	}

  });
  $(".mob-side-menu .s-con").click(function () {

  	if (m_s_con_val == 0) {
  		$(".mob-side-menu .s-con i").attr("class", "fa fa-angle-up");
  		m_s_con_val = 1;
  	}
  	else {
  		$(".mob-side-menu  .s-con i").attr("class", "fa fa-angle-down");
  		m_s_con_val = 0;
  	}

  });
  $(".mob-side-menu .make-p").click(function () {

  	if (m_make_p_val == 0) {
  		$(".mob-side-menu .make-p i").attr("class", "fa fa-angle-up");
  		m_make_p_val = 1;
  	}
  	else {
  		$(".mob-side-menu .make-p i").attr("class", "fa fa-angle-down");
  		m_make_p_val = 0;
  	}

  });
  $(".mob-side-menu .make-t").click(function () {

  	if (m_make_t_val == 0) {
  		$(".mob-side-menu .make-t i").attr("class", "fa fa-angle-up");
  		m_make_t_val = 1;
  	}
  	else {
  		$(".mob-side-menu .make-t i").attr("class", "fa fa-angle-down");
  		m_make_t_val = 0;
  	}

  });
  $(".mob-side-menu .make-c").click(function () {

  	if (m_make_c_val == 0) {
  		$(".mob-side-menu .make-c i").attr("class", "fa fa-angle-up");
  		m_make_c_val = 1;
  	}
  	else {
  		$(".mob-side-menu .make-c i").attr("class", "fa fa-angle-down");
  		m_make_c_val = 0;
  	}

  });
  $(".mob-side-menu .body-p").click(function () {

  	if (m_body_p_val == 0) {
  		$(".mob-side-menu .body-p i").attr("class", "fa fa-angle-up");
  		m_body_p_val = 1;
  	}
  	else {
  		$(".mob-side-menu .body-p i").attr("class", "fa fa-angle-down");
  		m_body_p_val = 0;
  	}

  });
  $(".mob-side-menu .body-t").click(function () {

  	if (m_body_t_val == 0) {
  		$(".mob-side-menu .body-t i").attr("class", "fa fa-angle-up");
  		m_body_t_val = 1;
  	}
  	else {
  		$(".mob-side-menu .body-p i").attr("class", "fa fa-angle-down");
  		m_body_t_val = 0;
  	}

  });

});	</script>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

	<!-- Include all compiled plugins (below), or include individual files as needed -->

</asp:Content>

