﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="email.aspx.cs" Inherits="email" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <html>
        <body>
   <script src="../js/vendor/jquery-3.2.1.min.js"></script>
     
	<style>
		.email-main-section{
			float:left;
			width:100%;
			padding-top:40px;
			padding-bottom:40px;
		}
		.email-container {
			display: block;
			width: 60%;
			padding-left: 150px;
			border-left: #1010ff 2px solid;
			margin: 0 auto;
		}
		.email-logo {
			float: left;
			width: 100%;
		}
		.email-logo img {
			width: 180px;
		}
		.email-user,.email-thanks,.email-order-time,.bill-address,.payment-mode,.gst {
			
			width: 100%;
			font-family: 'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;
			font-size: 13px;
			margin-bottom: 12px;
		}
		a, a:hover{
			color:#1979c3;
			font-family: 'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;
			font-size: 13px;
			text-decoration:none;
		}
		.email-order-heading {
			color: #222;
			font-family: 'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;
			font-size: 26px;
			font-weight: 300;
			margin-bottom: 6px;
		}
		.email-section {
			float: left;
			width: 100%;
		}
		.email-bill-info {
			float: left;
			width: 50%;
		}
		.email-shipping-info {
			width: 50%;
			float: left;
		}
		.bill-heading,.payment-method-heading {
			color: #222;
			font-family: 'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;
			font-size: 18px;
			font-weight: 300;
			margin-bottom: 12px;
		}
		.email-payment-method {
			float: left;
			width: 100%;
			padding-top: 30px;
		}
		.payment-method {
			float: left;
			width: 50%;
		}
		.shipping-method {
			float: left;
			width: 50%;
		}
		.order-detail {
			float: left;
			width: 100%;
		}
		.order-detail table {
		   border: 1px solid #d1d1d1;
		}
		.order-detail table th {
			font-family: 'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;
			text-align: left;
			vertical-align: bottom;
			background-color: #f2f2f2;
			padding: 11.5px;
			font-size: 13px;
			font-weight: 700;
		}
		.order-detail table td {
			font-family: 'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;
			vertical-align: top;
			padding: 11.5px;
			font-size: 13px;
		}
		tfoot {
			background: #f2f2f2;
		}
		tfoot th {
			font-weight: normal !important;
			text-align: right !important;
		}
	</style>
	<div class="email-main-section">
	<div class="email-container">
		<div class="email-logo">
			<img src="images/logo/logo.png" />
		</div>
		<div class="email-user">Priya Gupta ,</div>
		<div class="email-thanks">Thank you for your order from EBS1952. Once your package ships we will send an email with a<br /> link to track your order.
			 If you have questions about your order, you can email us at<br /> <a href="mailto:contact@ebs1952.com">contact@ebs1952.com</a>.
		</div>
		<div class="email-order-heading">Your Order #000001420</div>
		<div class="email-order-time">Placed on Mar 6, 2021, 2:52:01 PM</div>
		<div class="email-section">
			<div class="email-bill-info">
				<div class="bill-heading">Billing Info</div>
				<div class="bill-address">Priya Gupta<br />
					Ca- 46 Salt Lake<br />
					Sector - 1<br />
					Kolkata , West Bengal, 700064<br />
					India<br />
					T: <a href="tel:9830449321">9830449321</a>
				</div>
			</div>
			<div class="email-shipping-info">
				<div class="bill-heading">Shipping Info</div>
				<div class="bill-address">Priya Gupta<br />
					Ca- 46 Salt Lake<br />
					Sector - 1<br />
					Kolkata , West Bengal, 700064<br />
					India<br />
					T: <a href="tel:9830449321">9830449321</a>
				</div>
			</div>
		</div>
		<div class="email-payment-method">
			<div class="payment-method">
				<div class="payment-method-heading">Payment Method</div>
				<div class="payment-mode">Cash On Delivery</div>
			</div>
			<div class="shipping-method">
				<div class="payment-method-heading">Shipping Method</div>
				<div class="payment-mode">Free Shipping - Free</div>
			</div>
		</div>

		<div class="order-detail">
			<table>
				<tr>
					<th>Items</th>
					<th>Qty</th>
					<th>Price</th>
				</tr>
				<tr>
					<td><p style="margin-top:0;font-weight:bold;margin-bottom:5.75px;font-size:13px;">BRUSTRO DIY ACRYLIC MARKER 2MM TIP SET OF 12</p><p style="margin-top:0;margin-bottom:0;font-size:13px;">SKU: br5568</p></td>
					<td>1</td>
					<td>₹1,043.84</td>
				</tr>
				<tfoot>
				<tr>
					<th colspan="2">Subtotal</th>
					<td>₹1,043.84</td>
				</tr>
				<tr>
					<th colspan="2">Shipping & Handling</th>
					<td>₹0.00</td>
				</tr>
				<tr>
					<th colspan="2">Grand Total (Excl.Tax)</th>
					<td>₹1,043.84</td>
				</tr>
				<tr>
					<th colspan="2">IGST-12 (W.B) (12%)</th>
					<td>₹125.26</td>
				</tr>
				<tr>
					<th colspan="2">Tax</th>
					<td>₹125.26</td>
				</tr>
				<tr>
					<th colspan="2">Grand Total (Incl.Tax)</th>
					<td>₹1,169.10</td>
				</tr>
					</tfoot>
			</table>
			
		</div>
		<div class="gst">'EBS RETAIL GST NO. 04BCFPC7639P1Z9</div>
	</div>
	</div>
                    </body>
    </html>
</asp:Content>

