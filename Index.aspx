﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="customcss/home-page.css" rel="stylesheet" />

    	<!-- widget responsiveSlot1 start slider -->
				<div class="responsiveSlot1">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
                            		 <asp:Literal ID="ltslidertarget" runat="server"></asp:Literal>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
					 <asp:Literal ID="ltSlider" runat="server"></asp:Literal>
							
						</div>
                     

						<!-- Controls -->
						<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
				<!-- widget responsiveSlot1 stop -->
       	<!-- widget responsiveSlot4 start BEST SELLERS -->
				<div class="responsiveSlot4">
					<div id="imageCardSet_title-2323383" class="imageCardSet_title">
                        	BEST SELLERS
					</div>
					
					<div class="container-fluid"><!--data-items="1,3,5,6" data-items="1,2,3,4"--> 
						<div class="row">
							<div class="MultiCarousel cst-p" data-items="2,2,3,4" data-slide="1" id="MultiCarousel" data-interval="1000">
								<div class="MultiCarousel-inner">
                                    <asp:Literal ID="ltr_bestseller" runat="server"></asp:Literal>
          <%--                          <asp:Repeater ID="rpt_bestseller" runat="server">
                                    <ItemTemplate>
									<div class="item">
										<div class="pad15 best-seller">
                                            <a href="productdetail.aspx?p=<%#Eval("productid") %>&v=<%#Eval("variationid") %>">
											<img class="medium-img img-hvr second_img crouser_img" src="images/product/<%#Eval("photourl") %>" firstimg="images/product/<%#Eval("photourl") %>" secondimg="images/product/<%#Eval("second_photourl") %>"/>
                                                </a>
                                            	<h3 class="productBlock_titleContainer trackwidget title-height" style="text-align: left;">
											<a class="threeItemEditorial_itemTitle best-seller-title" href="#"><%#Eval("name") %></a>
										</h3>
											<p class="desc-p best-seller-desc"><%#Eval("ShortDescription") %></p>
                                            	<div class="productBlock_priceBlock" style="text-align: left;">
											<div class="productBlock_price">
												<span class="productBlock_priceCurrency" content=""></span>
												<span class="productBlock_priceValue" content="£145.00">
													₹<%#Eval("price") %>
												</span>
											</div>
										</div>
                              
                                              <a href="productdetail.aspx?p=<%#Eval("productid") %>&v=<%#Eval("variationid") %>">
										<div class="productBlockButtonLink trackwidget" >
											<span id="a_<%#Eval("variationid") %>" class="productBlock_button productBlock_button-buyNow btn-shop-now">
												SHOP NOW
											</span>
										</div>
                                                </a>

							
										</div>
									</div>
                                    </ItemTemplate>
                                    </asp:Repeater>--%>
				

								
																	</div>
								<button class="btn btn-primary leftLst" type="button"><img src="images/slider/left1.png" /></button>
								<button class="btn btn-primary rightLst" type="button"><img src="images/slider/right1.png" /></button>
							</div>
						</div>

					</div>
					</div>
				<!-- widget responsiveSlot4 stop -->
    	<!-- widget responsiveSlot2 start 	DISCOVER SOMETHING NEW -->
				<div class="responsiveSlot2">
					<div data-component="fourBestSellers"class="fourBestSellers trackwidget" data-block-name="hOMEPAGE" data-widget-id="2330045" data-widget-gtm-tracking data-component-tracked-viewed data-component-tracked-clicked>
						<h2 class="fourBestSellers_title" id="fourBestSellers_title-2330045">
							DISCOVER SOMETHING NEW
						</h2>
						
						<div class="container-fluid"><!--data-items="1,3,5,6"--> 
						<div class="row">
							<div class="MultiCarousel cst-p" data-items="2,2,3,4" data-slide="1" id="MultiCarousel" data-interval="1000">
								<div class="MultiCarousel-inner">
                                      <asp:Literal ID="ltr_discoversmthnew" runat="server"></asp:Literal>
                              <%--      <asp:Repeater ID="rpt_discoversmthnew" runat="server">
                                        <ItemTemplate>  
  
									<div class="item">
										<div class="pad15">
                                            	<a href="productdetail.aspx?p=<%#Eval("productid") %>&v=<%#Eval("variationid") %>">
											<img class="medium-img img-hvr second_img crouser_img" src="images/product/<%#Eval("photourl") %>" firstimg="images/product/<%#Eval("photourl") %>" secondimg="images/product/<%#Eval("second_photourl") %>"/>
                                                    </a>
											<div class="productBlock_detailsContainer discover-product ">

										<h3 class="productBlock_titleContainer trackwidget title-height" >
											<a class="threeItemEditorial_itemTitle dicover-title" href="#"><%#Eval("name") %></a>
										</h3>
											<p class="desc-p discover-desc"><%#Eval("ShortDescription") %></p>
											<div class="productBlock_rating">
											<span class="visually-hidden productBlock_rating_hiddenLabel">
												4.8 Stars 54 Reviews
											</span>
											<span class="productBlock_ratingStarsContainer">
												<div class="productBlock_ratingStars" style="width:96.0%">
													<span>
														<svg class="productBlock_reviewStar" viewBox="0 0 26 26" xmlns="http://www.w3.org/2000/svg">
															<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
														</svg>
													</span>
													<span>
														<svg class="productBlock_reviewStar" viewBox="0 0 26 26" xmlns="http://www.w3.org/2000/svg">
															<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
														</svg>
													</span>
													<span>
														<svg class="productBlock_reviewStar" viewBox="0 0 26 26" xmlns="http://www.w3.org/2000/svg">
															<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
														</svg>
													</span>
													<span>
														<svg class="productBlock_reviewStar" viewBox="0 0 26 26" xmlns="http://www.w3.org/2000/svg">
															<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
														</svg>
													</span>
													<span>
														<svg class="productBlock_reviewStar" viewBox="0 0 26 26" xmlns="http://www.w3.org/2000/svg">
															<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
														</svg>
													</span>
												</div>
											</span>
											<span class="productBlock_ratingValue" aria-hidden="true">4.8</span>
											<span class="productBlock_reviewCount" aria-hidden="true">54</span>
										</div>

										<div class="productBlock_priceBlock">
											<div class="productBlock_price">
												<span class="productBlock_priceCurrency" content=""></span>
												<span class="productBlock_priceValue" content="£145.00">
													₹<%#Eval("price") %>
												</span>
											</div>
										</div>
                                                   <a href="productdetail.aspx?p=<%#Eval("productid") %>&v=<%#Eval("variationid") %>">
										<div class="productBlockButtonLink trackwidget" data-block-name="hOMEPAGE" data-block-type="" data-widget-id="2330045">
										
													<span id="a_<%#Eval("variationid") %>" class="productBlock_button productBlock_button-buyNow btn-shop-now">
												SHOP NOW
											</span>
										</a>
										
										</div>
									</div>

										</div>
								
                                              </ItemTemplate>
                                        </asp:Repeater>--%>
								
									
                                  </div>
								<button class="btn btn-primary leftLst" type="button"><img src="images/slider/left1.png" /></button>
								<button class="btn btn-primary rightLst" type="button"><img src="images/slider/right1.png" /></button>
							</div>
						</div>

					</div>
					</div>
				</div>
				<!-- widget responsiveSlot2 stop -->
 
    		<!-- widget responsiveSlot3 start Shop Sale-->
				<div class="responsiveSlot3 shop-sale-section">
					<div class="threeItemEditorial trackwidget shop-section">
						<div class="threeItemEditorial_titleContainer">
							<h2 class="threeItemEditorial_title">SHOP SALE</h2>
						</div>
						
						<div class="container-fluid"><!--data-items="1,3,5,6"--> 
						<div class="row">
							<%--<div class="MultiCarousel cst-p" data-items="1,2,3,3" data-slide="1" id="MultiCarousel" data-interval="1000">
								<div class="MultiCarousel-inner">--%>
                                            <asp:Repeater ID="rpt_shopsale" runat="server">
                                <ItemTemplate>  
									
									        
									<%--<div class="item">
										<div class="pad15 shop-div">--%>
									<div class="col-md-4 col-sm-4 col-xs-12 shop-sale-img-section">  
										                        <a href="<%#Eval("url") %>">
												<img class="shop-img" src=images/shop_sale/<%#Eval("image") %> />
                                                    </a>

										</div>

										<%--<div class="productBlockButtonLink trackwidget" >
											<a href="#" class="productBlock_button productBlock_button-buyNow">ADD TO CART</a>
										</div>--%>
											<%--<p>SHEAFFER 100 80007PC PENCIL </p>--%>
										<%--</div>
									</div>--%>
                                        </ItemTemplate>
                            </asp:Repeater>
						
                                <%--  </div>
								<button class="btn btn-primary leftLst" type="button"><img src="images/slider/left1.png" /></button>
								<button class="btn btn-primary rightLst" type="button"><img src="images/slider/right1.png" /></button>
							</div>--%>
						</div>

					</div>
					</div>
				</div>
				<!-- widget responsiveSlot3 stop -->
    		
				<!-- widget responsiveSlot5 start BRANDS OF THE WEEK -->
				<div class="responsiveSlot5">
					<div class="imageCardSet" data-component="cardScroller" data-card-scroller-target-class="imageCardSet">
						<div id="imageCardSet_title-2178316" class="imageCardSet_title">
                            BRANDS OF THE WEEK
						</div>
						
						<div class="container-fluid">
							<!--data-items="1,3,5,6"-->
							<div class="row">
								<div class="MultiCarousel" data-items="2,2,3,4" data-slide="1" id="MultiCarousel" data-interval="1000">
									<div class="MultiCarousel-inner">
                                        <asp:Repeater ID="rpt_brandsofweek" runat="server">
                                            <ItemTemplate>

                                           
										<div class="item">
											<div class="pad15">
                                                	<a href="list.aspx?b=<%#Eval("brandid") %>" >
												<img class="medium-img crouser_img" src="images/brand/<%#Eval("photourl")%>" />
                                                         </a>
                                                      <%--  <p class="brand-week-title title-space"><%#Eval("Title")%></p>--%>
                                                <h3 class="threeItemEditorial_itemTitle sopt-title"><%#Eval("Title")%></h3>
                                                	<p class="desc-p "><%#Eval("Description") %></p>
									<%--			<p><span class="was">WAS &#8377;225.00</span><span class="now">NOW &#8377;202.50</span></p>--%>
                                   <%--             	<p><span class="now">NOW &#8377;<%#Eval("Price")%></span></p>--%>
                                               
											</div>
										</div>

                                                 </ItemTemplate>

                                        </asp:Repeater>
									</div>
									<button class="btn btn-primary leftLst" type="button"><img src="images/slider/left1.png" /></button>
									<button class="btn btn-primary rightLst" type="button"><img src="images/slider/right1.png" /></button>
								</div>
							</div>

				
						</div>

					</div>
				</div>
				<!-- widget responsiveSlot5 stop -->
    	<!-- widget responsiveSlot6 start     SPOTLIGHT -->
				<div class="responsiveSlot6">
					<div  class="imageCardSet_title spot-ligth-head">
                           SPOTLIGHT
						</div>
					<div class="twoItemEditorial trackwidget"
						 data-block-name="CHRISTMAS - URBAN DECAY BOTM"
						 data-component="twoItemEditorial"
						 data-widget-id="1891509"
						 data-widget-gtm-only-tracking
						 data-component-tracked-viewed
						 data-component-tracked-clicked>
						<div class="twoItemEditorial_container">
                       
							<!-- itemOne -->
                            <asp:Repeater ID="rpt_rpt_brandsofweek_SL" runat="server">
                                <ItemTemplate>
							<div class="twoItemEditorial_item twoItemEditorial_itemOne spotlight-shop-section">
								<a href="list.aspx?b=<%#Eval("brandid") %>" class="twoItemEditorial_link">
									<div class="twoItemEditorial_imageWrapper">
										<img src="images/brand/<%#Eval("photourl")%>"  alt="No Image" class="twoItemEditorial_image big-img" />
									</div>
									<div class="twoItemEditorial_textContainer spot-shop-section">
										<h3 class="threeItemEditorial_itemTitle sopt-title"><%#Eval("Title")%></h3>
										
											<p class="desc-p spotlight-p"><%#Eval("Description") %></p>
										<%--<p class="twoItemEditorial_itemDescription"><%#Eval("ShortDescription")%></p>--%>
										<div class="twoItemEditorial_itemCTAText">SHOP NOW</div>
									</div>
								</a>
							</div>
                                    </ItemTemplate>

                            </asp:Repeater>

						</div>
					</div>
				</div>
				<!-- widget responsiveSlot6 stop -->
				<!-- widget responsiveSlot7 start ULTIMATE STATIONARY SAVINGS -->
				<div class="responsiveSlot7">
					<div class="threeItemEditorial trackwidget"
						 data-component="threeItemEditorial"
						 data-block-name="Homepage 22.12 Burberry/Laura Mercier/Christophe Robin"
						 data-widget-id="2221774"
						 data-widget-gtm-tracking
						 data-component-tracked-viewed>
						<div class="threeItemEditorial_titleContainer">
							<h2 class="threeItemEditorial_title">ULTIMATE STATIONERY SAVINGS</h2>
						</div>
							<div class="container-fluid">
							<!--data-items="1,3,5,6"-->
							<div class="row">
								<%--<div class="MultiCarousel" data-items="1,2,3,3" data-slide="1" id="MultiCarousel" data-interval="1000">--%>
								<%--	<div class="MultiCarousel-inner">--%>
                                        <asp:Repeater ID="rpt_ultimtstationary" runat="server">
                                            <ItemTemplate>
									<%--	<div class="item">
											<div class="pad15">
										--%>
                                            <div class="col-md-4 col-sm-4 col-xs-12 ultimate-img-section">  
													<a href="<%#Eval("url") %>">
										
												<img class="ultimate-img" src="images/shop_sale/<%#Eval("image") %>" />
                                                 </a>
										
												<div class="twoItemEditorial_textContainer"> 
                                                    <p class="desc-p"><%#Eval("Description") %></p>            
									</div>
                      
											</div>                
										<%--</div>
                                   </div>--%>
                                                </ItemTemplate>
                                            </asp:Repeater>
										
							<%--		</div>--%>
									<%--<button class="btn btn-primary leftLst" type="button"><img src="images/slider/left1.png" /></button>
									<button class="btn btn-primary rightLst" type="button"><img src="images/slider/right1.png" /></button>
								</div>--%>
							</div>

				
						</div>

					</div>
				</div>
				<!-- widget responsiveSlot7 stop -->
    	<!-- widget responsiveSlot9 start EBS art company-->
				<div class="responsiveSlot9">
					<div class="promoProductSlider"
						 data-block-name="29/12. The Inkey List Salon Space"
						 data-block-type="Product"
						 data-widget-id="2300613"
						 data-widget-gtm-only-tracking
						 data-component-tracked-viewed
						 data-component="promoProductSlider">
						<span data-component="productQuickbuy"></span>
						<div class="promoProductSlider_title">EBS ART COMPANY</div>
						<div class="promoProductSlider_subtitle"><%#Eval("ShortDescription")%></div>
						<div class="container-fluid">
							<div class="col-md-6 col-sm-6 col-xs-12">
									<div  class="imageCardSet_title"> SPOTLIGHT </div>
                                <asp:Repeater ID="rpt_ebsartcmpny_SL" runat="server">
                                    <ItemTemplate>
                            
								<a href="<%#Eval("url") %>" class="promoProductSlider_imageLink" aria-hidden="false">
					
									<div class="twoItemEditorial_imageWrapper">
										<img src="images/shop_sale/<%#Eval("image") %>" alt="" class="twoItemEditorial_image big-img ebs-art-big-img">
									</div>
								<%--	<p class="desc-p"><%#Eval("ShortDescription") %></p>--%>
								</a>
                                                 </ItemTemplate>
                                </asp:Repeater>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<!--data-items="1,3,5,6"-->
								<div class="row">
									<div class="MultiCarousel product-cro" data-items="1,2,3,4" data-slide="1" id="MultiCarousel" data-interval="1000">
										<div class="MultiCarousel-inner">
                                               <asp:Literal ID="ltr_ebsartcmpny" runat="server"></asp:Literal>
                          <%--                    <asp:Repeater ID="rpt_ebsartcmpny" runat="server">
                                    <ItemTemplate>

											<div class="item">
												<div class="pad15">
                                                    	<a href="productdetail.aspx?p=<%#Eval("productid")%>&v=<%#Eval("variationid") %>">
														<img class="medium-img img-hvr second_img" src="images/product/<%#Eval("photourl") %>" firstimg="images/product/<%#Eval("photourl") %>" secondimg="images/product/<%#Eval("second_photourl") %>"/>
                                                            </a>
													<p class="threeItemEditorial_itemTitle art-title"><%#Eval("name") %></p>
													<p class="desc-p art-section-p"><%#Eval("ShortDescription") %></p>
													<div class="papBannerWrapper" data-component="papBanner">
		

													</div>

													<div class="productBlock_priceBlock" data-is-link="true">
														<div class="productBlock_price">
															<span class="productBlock_priceCurrency" content="GBP"></span>
															<span class="productBlock_priceValue" content="£25.00">&#8377;<%#Eval("price")%></span>
														</div>

													</div>
                                                       <a href="productdetail.aspx?p=<%#Eval("productid") %>&v=<%#Eval("variationid") %>">
													<div class="productBlock_actions">
														<span data-component="productQuickbuy"></span>
														<span class="ebs-art-shop-btn productBlock_button productBlock_button-moreInfo" id="a_<%#Eval("variationid")%>" data-sku="12024036" data-open-product-quickbuy="" data-from-wishlist="false" data-subscribe-and-save="">
														SHOP NOW

														</span>
													</div>
                                                           </a>
												</div>
											</div>
                                        </ItemTemplate>
                                                  </asp:Repeater>--%>
						
										
										</div>
										<button class="btn btn-primary leftLst" type="button"><img src="images/slider/left1.png" /></button>
										<button class="btn btn-primary rightLst" type="button"><img src="images/slider/right1.png" /></button>
									</div>
								</div>


							</div>

					
						</div>
					</div>
				</div>

			
				<!-- widget responsiveSlot10 start -->
				<div class="responsiveSlot10">
				</div>
				<!-- widget responsiveSlot10 stop -->
				<!-- widget responsiveSlot11 start blog-->
				<div class="responsiveSlot11">
					<div class="imageCardSet" data-component="cardScroller" data-card-scroller-target-class="imageCardSet">
						<div id="imageCardSet_title-1849333" class="imageCardSet_title">
							THE EBS BLOG
						</div>
						<div class="container-fluid">
							<!--data-items="1,3,5,6"-->
							<div class="row">
								<div class="blog-p MultiCarousel cst-p" data-items="1,2,3,4" data-slide="1" id="MultiCarousel" data-interval="1000">
									<div class="MultiCarousel-inner">
										<div class="item">
											<div class="pad15">
											<a href="/blog.aspx">	<img src="images/blog/blog1.jpeg" /></a>
												<%--<p>Back to school supplies list 2021 </p>--%>
											</div>
										</div>
										<div class="item">
											<div class="pad15">
										<a href="/blog2.aspx">		<img src="images/blog/blog2.jpeg" /></a>
											<%--	<p>	Fun art and craft DIY ideas to do at home </p>--%>
											</div>
										</div>
										<div class="item">
											<div class="pad15">
											<a href="/blog3.aspx">	<img src="images/blog/blog3.jpeg" /></a>
												<%--<p>DIY craft ideas and activities for adults & kids 
												</p>--%>
											</div>
										</div>
										<div class="item">
											<div class="pad15">
												<a href="/blog4.aspx"><img src="images/blog/blog4.jpeg" /></a>
												<%--<p>Planners, Organizers & Diaries for 2021 </p>--%>

											</div>
										</div>
										<div class="item">
											<div class="pad15">
											<a href="/blog5.aspx">	<img src="images/blog/blog5.jpeg" /></a>
												<%--<p>	Exciting and quirky range of pens for adults and kids </p>--%>
											</div>
										</div>
										
									</div>
									<button class="btn btn-primary leftLst" type="button"><img src="images/slider/left1.png" /></button>
									<button class="btn btn-primary rightLst" type="button"><img src="images/slider/right1.png" /></button>
								</div>
							</div>

				
						</div>
						
					</div>
				</div>
				<!-- widget responsiveSlot11 stop -->
				<!-- widget responsiveSlot12 start -->
				<%--<div class="responsiveSlot12">
					<div class="brandLogos trackwidget"
						 data-component="brandLogos"
						 data-block-type="2 - 20200107 - Logos - ghd,urban decay, kera, ghd,ysl, elemis"
						 data-block-name="2 - 20200107 - Logos - ghd,urban decay, kera, ghd,ysl, elemis"
						 data-widget-id="818934"
						 data-widget-gtm-only-tracking
						 data-component-tracked-viewed
						 data-component-tracked-clicked>
						<a href="brands/estee-lauder.html" class="brandLogos_link">
							<img src=../s3.thcdn.com/widgets/95-en/54/Estee-Lauder-035854.png
								 alt="Estee Lauder"
								 class="brandLogos_image" />
						</a>
						<a href="brands/pixi/view-all.html" class="brandLogos_link">
							<img src=../s1.thcdn.com/widgets/95-en/23/Pixi_Logo_197x110-021123.jpg
								 alt="Pixi"
								 class="brandLogos_image" />
						</a>
						<a href="brands/kerastase/kerastase.html" class="brandLogos_link">
							<img src=../s2.thcdn.com/widgets/95-en/20/Kerastase_Logo-100140-104317-035620.png
								 alt="Keratase"
								 class="brandLogos_image" />
						</a>
						<a href="health-beauty/ghd/view-all-ghd.html" class="brandLogos_link">
							<img src=../s4.thcdn.com/widgets/95-en/29/ghd_logo-043529.png
								 alt="ghd - good hair day"
								 class="brandLogos_image" />
						</a>
						<a href="brands/chantecaille/view-all.html" class="brandLogos_link">
							<img src=../s1.thcdn.com/widgets/95-en/09/chantecaille_logo-100009.png
								 alt="Chantecaille"
								 class="brandLogos_image" />
						</a>
						<a href="brands/espa/view-all.html" class="brandLogos_link">
							<img src=../s1.thcdn.com/widgets/95-en/05/Espa-035905.png
								 alt="ESPA"
								 class="brandLogos_image" />
						</a>
					</div>
				</div>--%>
				<!-- widget responsiveSlot12 stop -->
			
  <script>  
//$(document).ready(function(){  
//	$(".img-hvr").hover(function () {
//		$imgsrc = $(this).attr('src');
//		$(this).attr('src', 'images/product/080Feb021051051230PM9031img.png');
//    }, function(){  
//    $(this).attr('src',$imgsrc);  
//  });  
//});
//$(document.body).on('mouseover', '.img-hvr', function (e) {
	
//		$imgsrc = $(this).attr('src');
//		alert($imgsrc);
//		$(this).attr('src', 'images/product/080Feb021051051230PM9031img.png');
	
//});

</script>  
</asp:Content>


