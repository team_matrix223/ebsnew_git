﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="addaddress.aspx.cs" Inherits="addaddress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



	<link href="customcss/addaddress.css" rel="stylesheet" />
    <script src="js/customjs/mycart.js"></script>
    <script src="js/customjs/addaddress.js"></script>



	 <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
     <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-ui.js"></script>
     <link rel="stylesheet" type="text/css" href="css/jquery-uiJQGrid.css" />  
     <script src="../js/customValidation.js"></script>

    <script type="text/javascript" src="js/jquery.uilock.js"></script>

<%--    <style>
        .mycartminus {
            display:none
        }
            .mycartadd {
            display:none
        }
    </style>--%>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-7 col-sm-7 col-xs-12 add-cst-col">
				<h2 class="shipping-heading">Shipping Address</h2>
				<div class="form-horizontal shipping-frm">
			<%--		  <div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label">Email</label>
						<div class="col-sm-9">
						  <input type="email" class="form-control" id="inputEmail3" placeholder="" required>
						</div>
					  </div>--%>
					  <%--<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-10">
						  <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
						</div>
					  </div>
					  <div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
						  <div class="checkbox">
							<label>
							  <input type="checkbox"> Remember me
							</label>
						  </div>
						</div>
					  </div>--%>
                               		<div class="form-group">
						<label for="inputEmail3"  class="col-sm-3 control-label">Email</label>
						<div class="col-sm-9">
						  <input type="text" id="txtGuestEmail" autocomplete="off"   class="form-control" placeholder=" " required>
						</div>
					  </div>
					 <div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label">First Name</label>
						<div class="col-sm-9">
						  <input type="text" id="txtFirstName" autocomplete="off"  class="form-control" placeholder=" " required>
						</div>
					  </div>
					<div class="form-group">
						<label for="inputEmail3"  class="col-sm-3 control-label">Last Name</label>
						<div class="col-sm-9">
						  <input type="text" autocomplete="off" id="txtLastName"  class="form-control" placeholder=" " required>
						</div>
					  </div>
         
			<%--			<div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label">Company</label>
						<div class="col-sm-9">
						  <input type="text" class="form-control" placeholder=" " >
						</div>
					  </div>--%>
						<div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label"> Address</label>
						<div class="col-sm-9">
						  <textarea  id="txtAddress"  rows="20"  class="form-control" placeholder=" " required></textarea>
						</div>
					  </div>
					 	<div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label">City</label>
						<div class="col-sm-9">
			    <select id="ddlCities" class="form-control"></select>
						</div>
					  </div>
					 <div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label">Area</label>
						<div class="col-sm-9">
                            	  <input type="text" id="txtArea" class="form-control" placeholder=" " required>
			<%--			  <select  class="form-control">
							  <option>Please select s region, state or province</option>
							  <option>Chandigarh</option>
							  <option>Mohali</option>
							  
							</select>--%>
						</div>
					  </div>
						<div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label">Zip/Postal Code</label>
						<div class="col-sm-9">
						  <input type="number" id="txtPinCode" autocomplete="off"  class="form-control" placeholder=" " required>
						</div>
					  </div>

					<div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label">Mobile Number</label>
						<div class="col-sm-9">
						  <input type="number" id="txtMobileNo" autocomplete="off"  maxlength="10"  class="form-control" placeholder=" " required>
						</div>
					  </div>

	<%--<div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label">Country</label>
						<div class="col-sm-9">
						  <select  class="form-control">
							  <option>India</option>
							  <option>USA</option>
							  <option>UAE</option>
							  
							</select>
						</div>
					  </div>--%>

					  <div class="form-group">
						<div class="col-sm-offset-3 col-sm-9">
						  <button type="button" id ="submit" class="btn ">Next</button>
						</div>
					  </div>
					</div>
			</div>
			<div class="col-md-5 col-sm-5 col-xs-12  add-cst-col">
				<h2 class="order-head">Order Summary</h2>
				<div class="order-summary">
					
					<h3><span class="cartcount"></span> Item(s) in Cart</h3>
						<%--<div class="responsiveBasket_head responsiveBasket_row">
	<div class="responsiveBasket_headItem responsiveBasket_headItem-descriptionTitle">
	Items
	</div>
	<div class="responsiveBasket_headItem responsiveBasket_headItem-unitPriceTitle">
	Price
	</div>
	<div class="responsiveBasket_headItem responsiveBasket_headItem-quantityTitle">
	Quantity
	</div>
	<div class="responsiveBasket_headItem responsiveBasket_headItem-subTotalTitle">
	Subtotal
	</div>
	<div class="responsiveBasket_headItem responsiveBasket_removeColumn">
	&nbsp;
	</div>
	</div>--%>
					<div class="order-heading-div">
						<div class="order-item">Items</div>
						<div class="order-price">Price</div>
						<div class="order-qty">Quantity</div>
						<div class="order-subtotal">Subtotal</div>
					</div>
					<div class="order-details" id="cartcontainer">
			<%--<div class="order-img">
							<img src="images/product/080Feb021051051230PM9031img.png" class="small-img" />
						</div>
						<div class="order-desc">
							<h2>ADD GEL SILVER DIAMOND ROLLER GEL PEN </h2>
							<p>Qty:1</p>
						</div>
						<div class="order-price">₹140.00</div>--%>
					</div>
                    
				</div>
					<div class="order-summary-basket">
						<div class="order-basket"><span>Basket Subtotal: </span><span class="baskettotal"></span>  </div>
					</div>

			</div>
		</div>
	</div>
</asp:Content>

