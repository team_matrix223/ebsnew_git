﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Web.UI;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class list : System.Web.UI.Page
{


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnLevel1.Value = Request.QueryString["c"] != null ? Request.QueryString["c"] : "0";
            hdnLevel2.Value = Request.QueryString["s"] != null ? Request.QueryString["s"] : "0";
            hdnLevel3.Value = Request.QueryString["sc"] != null ? Request.QueryString["sc"] : "0";
            hdnBrandId.Value = Request.QueryString["b"] != null ? Request.QueryString["b"] : "0";
            hdnGroupId.Value = Request.QueryString["g"] != null ? Request.QueryString["g"] : "0";
            hdnSubGroupId.Value = Request.QueryString["sg"] != null ? Request.QueryString["sg"] : "0";
            hdnGetMethod.Value = Request.QueryString["gm"] != null ? Request.QueryString["gm"] : "0";
            hdnsearch.Value = Request.QueryString["search"] != null ? Request.QueryString["search"] : "";
            hdnamt_type.Value = Request.QueryString["amt_type"] != null ? Request.QueryString["amt_type"] : "0";
            hdnamt.Value = Request.QueryString["amt"] != null ? Request.QueryString["amt"] : "0";
        }

      
    }



    [WebMethod]
    public static string AdvancedSearch(string Brands, string Level1, string Level2, string Level3, int PageId, int MinPrice, int MaxPrice, string BrandId, string GroupId, string SubGroupId,string GetMethod,string attrValues, string sortby,string colorValues,string searchValues, string amt_type, decimal amt)
    {
  
        Int64 l1 = CommonFunctions.IsNumeric(Level1);
        Int64 l2 = CommonFunctions.IsNumeric(Level2);
        Int64 l3 = CommonFunctions.IsNumeric(Level3);

        Int64 BID = CommonFunctions.IsNumeric(BrandId);
        Int64 GID = CommonFunctions.IsNumeric(GroupId);
        Int64 SGID = CommonFunctions.IsNumeric(SubGroupId);


        if (l3 > 0)
        {
            l1 = 0;
            l2 = 0;
        }
        else if (l2 > 0)
        {
            l1 = 0;
            l3 = 0;
        }
        else if (l1 > 0)
        {
            l2 = 0;
            l3 = 0;
        }
        else
        {
            l1 = 0;
            l2 = 0;
            l3 = 0;
        }

        string Minprice_slider = "";
        string Maxprice_slider = "";
        string BrandString = "";
        string AttrString = "";
        string CategoryString = "";
        string ColorString = "";
        string CategoryName = "";
        string Desc = "";
        int totalRecords = 0;
        int lPrice = 0;
        int hPrice = 0;
        string products = new ProductsBLL().AdvancedSearch(amt_type,amt, searchValues, colorValues, sortby,GetMethod, HttpContext.Current.Session.SessionID,attrValues, Brands, l1, l2, l3, out  Minprice_slider, out  Maxprice_slider, out BrandString,out AttrString,out ColorString, out CategoryString, out CategoryName, out Desc, out totalRecords, PageId, MinPrice, MaxPrice, out lPrice, out hPrice, BID, GID, SGID);
        var JsonData = new
        {
         
            ProductData = products,
            CategoryData = CategoryString,
            BrandData = BrandString,
            Minprice_slider = Minprice_slider,
            Maxprice_slider = Maxprice_slider,
            AttrData= AttrString,
            ColorData =ColorString,
            CatTitle = CategoryName,
            CatDesc = Desc,
            TotalRecords = totalRecords,
            MinP = lPrice,
            MaxP = hPrice

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);

    }



}