﻿ <%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentSuccess.aspx.cs" Inherits="Payments_PaymentSuccess" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../js/vendor/jquery-3.2.1.min.js"></script>
	<link href="customcss/PaymentSuccess.css" rel="stylesheet" />

    <script>
        $(document).ready(function () {
          DeleteFromCart("All", 0);
            getOrder();
            function DeleteFromCart(req, productid) {
                $.ajax({
                    type: "POST",
                    data: '{"req":"' + req + '","productid":"' + productid + '","attr_id":"0"}',
                    url: "/mycart.aspx/DelFromCart",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {
                        var obj = jQuery.parseJSON(msg.d);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {

                    }

                });





            }
      


            function getOrder() {
                $.ajax({
                    type: "POST",
                    data: '{"orderid":"' + $("#hdnorderid").val() + '"}',
                    url: "PaymentSuccess.aspx/GetorderData",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {


                        var obj = jQuery.parseJSON(msg.d);
                        var Orderdetail = obj.OrderDetail;
                
                      
   
                        PostOrderShiprocket(obj.orderdate, obj.firstname, obj.lastname, obj.address, obj.city, obj.pincode, obj.state, obj.email, obj.phoneno, obj.paymode, obj.shipping_charges, obj.discountamt, obj.subtotal, Orderdetail)
                        !function (f, b, e, v, n, t, s) {
                        	if (f.fbq) return; n = f.fbq = function () {
                        		n.callMethod ?
								n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                        	};
                        	if (!f._fbq) f._fbq = n; n.push = n; n.loaded = !0; n.version = '2.0';
                        	n.queue = []; t = b.createElement(e); t.async = !0;
                        	t.src = v; s = b.getElementsByTagName(e)[0];
                        	s.parentNode.insertBefore(t, s)
                        }(window, document, 'script',
					'https://connect.facebook.net/en_US/fbevents.js');
                        fbq('init', '1689688837856597');
                        fbq('track', 'Purchase', { currency: "INR", value: obj.subtotal });
                       
                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {


                    }

                });
            }
            function getOrderdetail() {





            }

            function PostOrderShiprocket(orderdate, firstname, lastname, address, city, pincode, state, email, phoneno, paymode, shipping_charges, discountamt, subtotal, Orderdetail) {
                var token = "";
             
            
                    $.ajax({
                        url: 'https://apiv2.shiprocket.in/v1/external/auth/login?email=rahulthakur9942@gmail.com&password=Ebsretail1952@',

                        type: 'POST',

                        dataType: 'json',

                        success: function (data, textStatus, xhr) {

                            token = data.token;
                            var jsonData = JSON.stringify({ "order_id": $("#hdnorderid").val(), "order_date": orderdate, "pickup_location": "Primary", "comment": "Reseller: M/s Goku", "billing_customer_name": firstname, "billing_last_name": '', "billing_address": address, "billing_address_2": "", "billing_city": city, "billing_pincode": pincode, "billing_state": state, "billing_country": "India", "billing_email": email, "billing_phone": phoneno, "shipping_is_billing": true, "shipping_customer_name": firstname, "shipping_last_name": lastname, "shipping_address": address, "shipping_address_2": "", "shipping_city": city, "shipping_pincode": pincode, "shipping_country": "", "shipping_state": state, "shipping_email": email, "shipping_phone": phoneno, "order_items": Orderdetail, "payment_method": paymode, "shipping_charges": shipping_charges, "giftwrap_charges": 0, "transaction_charges": 0, "total_discount": discountamt, "sub_total": subtotal, "weight": 0.5, "length": 10, "breadth": 10, "height": 10 });
                            var settings = {
                                "async": true,
                                "crossDomain": true,
                                "url": "https://apiv2.shiprocket.in/v1/external/orders/create/adhoc",
                                "method": "POST",
                                "dataType": "json",
                                "headers": {
                                    "authorization": "Bearer " + token + "",
                                    "content-type": "application/json",
                                    "cache-control": "no-cache",
                                    "postman-token": "f90c527f-b03c-da0e-3956-01a2569d0866"
                                },
                                "processData": false,
                                "data": jsonData
                            }

                            $.ajax(settings).done(function (response) {
                                console.log(response);
                            });

                        },
                        error: function (xhr, textStatus, errorThrown) {
                            console.log('Error in Database');


                        }
                    });
    
               
            }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="hdnorderid" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="hdntxnid" runat="server" />
               <asp:HiddenField ID="hdnroid" runat="server" />
    <div class="payment-div">
		<img src="../images/checked.png" />
        <h1>Order Placed Successfully !!</h1>
		<h3 id="h_orderid" runat="server">#Order id: <%=hdnorderid.Value %></h3>
        	<h3 id="h_txnid" visible="false" runat="server">#Payment id: <%=hdntxnid.Value %></h3>
<%--		<h3 id="hdnrz_oid" runat="server" visible="false">#Payment id:<%=hdnpaymentid.Value %></h3>--%>
		<p>Thanks for being awesome,<br />we hope you enjoy your purchase!</p>
		
	<a href="/index.aspx">	Continue Shopping	</a>

		<%--<div class="delivery-section">
		
				<h2>Delivery details</h2>
				<div class="delivery-fro">
					<h3>Delivery for</h3>
					<div>Mr Sham</div>
					<div>Phone no: 1234567890</div>
					<h3>Address</h3>
					<div>#123 Sector 11A Chandigarh<br />
						160010, India
					</div>
				</div>
				<div class="delivery-method">
					<h3>Delivery method</h3>
					<div>Standard Delivery - Please read below to see when your items will be delivered</div>
				</div>
			</div>--%>
		
    
	</div>
		</form>

</body>
</html>
