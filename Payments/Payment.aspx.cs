﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Razorpay.Api;
using System.Net;
using System.Web.Script.Serialization;

public partial class Payments_Payment : System.Web.UI.Page
{
    public string orderId;
    public decimal amt;
    public string name;
    public string email;
    public string mobileno;
    protected void Page_Load(object sender, EventArgs e)
    {
      string orderid = Request.QueryString["oid"] != null ? Request.QueryString["oid"] : "0";

        hdnorderid.Value = orderid;
        string orderdate = "", firstname = "", lastname = "", address = "", city = "", pincode = "", state = "", cst_email = "", phoneno = "", paymode = "", shipping_charges = "", discountamt = "", subtotal = "";
        List<Order> lst = new OrderBLL().GetOrderbyOrderId(Convert.ToInt32(orderid)).ToList();
        JavaScriptSerializer ser = new JavaScriptSerializer();
        foreach (var item in lst)
        {

            firstname = item.FirstName.ToString();
            lastname = item.LastName.ToString();

            cst_email = item.Email.ToString();
            phoneno = item.RecipientMobile.ToString();


      


        amt = Convert.ToDecimal(HttpContext.Current.Session[Constants.NetAmount]);
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        Dictionary<string, object> input = new Dictionary<string, object>();
        input.Add("amount",amt); // this amount should be same as transaction amount
        input.Add("currency", "INR");
        input.Add("receipt", "12121");
        input.Add("payment_capture", 1);
        name = firstname+" "+ lastname;
        email = cst_email.ToString();
        mobileno = phoneno;

        string key = "rzp_live_0BIFSXMMETySFO";
        string secret = "OctFNt5AYly07IR1DTxf21Be";

        RazorpayClient client = new RazorpayClient(key, secret);

        Razorpay.Api.Order order = client.Order.Create(input);
        orderId = order["id"].ToString();
        }
    }
}