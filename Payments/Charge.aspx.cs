﻿using Razorpay.Api;
using System;
using System.Collections.Generic;
using System.Web;

public partial class Payments_Charge : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string paymentId = Request.Form["razorpay_payment_id"];

        Dictionary<string, object> input = new Dictionary<string, object>();
        input.Add("amount", HttpContext.Current.Session[Constants.NetAmount].ToString()); // this amount should be same as transaction amount

        string key = "rzp_test_WLKtxbG2ybyCwJ";
        string secret = "LwmYNAMc4ps9jr60TzzYYgGu";

        RazorpayClient client = new RazorpayClient(key, secret);

        Dictionary<string, string> attributes = new Dictionary<string, string>();

        attributes.Add("razorpay_payment_id", paymentId);
        attributes.Add("razorpay_order_id", Request.Form["razorpay_order_id"]);
        attributes.Add("razorpay_signature", Request.Form["razorpay_signature"]);
   

        Utils.verifyPaymentSignature(attributes);

        //             please use the below code to refund the payment 
        //             Refund refund = new Razorpay.Api.Payment((string) paymentId).Refund();

        Console.WriteLine(paymentId);
    }
}