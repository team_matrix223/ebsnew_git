﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentFail.aspx.cs" Inherits="Payments_PaymentFail" %>
<link href="customcss/PaymentFail.css" rel="stylesheet" />
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
                <asp:HiddenField ID="hdnorderid" runat="server" />
               <asp:HiddenField ID="hdnres" runat="server" />
            <asp:HiddenField ID="hdndesc" runat="server" />
    <div class="payment-fail">
	  <img src="../images/wrong.png" />
      <h1>Payment Failed !!</h1>
        <h3 id="h_orderid" runat="server">#Order id:<%=hdnorderid.Value %></h3>
       <%--<h3 id="h_reason" runat="server">Reason:<%=hdnres.Value %></h3>--%>
          <h3 id="h_desc" runat="server">Reason:<%=hdndesc.Value %></h3>
	  <a href="/index.aspx">Go to home</a>
	
    </div>
    </form>
</body>
</html>
