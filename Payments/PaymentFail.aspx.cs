﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Payments_PaymentFail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        hdnorderid.Value = Request.QueryString["oid"] != null ? Request.QueryString["oid"] : "0";
        hdnres.Value = Request.QueryString["res"] != null ? Request.QueryString["res"] : "0";
        hdndesc.Value = Request.QueryString["des"] != null ? Request.QueryString["des"] : "0";
        new OrderBLL().UpdatePaymentStatus(Convert.ToInt32(hdnorderid.Value), hdndesc.Value, "0");
    }
}