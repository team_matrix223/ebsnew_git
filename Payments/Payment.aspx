﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Payment.aspx.cs" Inherits="Payments_Payment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Razorpay</title>
<script src="../js/vendor/jquery-3.2.1.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script>
        $(document).ready(function () {
            $("#rzp-button1").click();


        })
    </script>
</head>

<body>
 
        <button id="rzp-button1"  type="button" style="display:none">Pay</button>
    <form action="Charge.aspx" method="post" runat="server">
           <asp:HiddenField ID="hdnorderid" runat="server" ClientIDMode="Static"/>
<script>

    var options = {
        "key": "rzp_live_0BIFSXMMETySFO", // Enter the Key ID generated from the Dashboard
    "amount": "<%=amt%>", // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
    "currency": "INR",
    "name": "<%=name%>",
    "description": "Transaction",
    "image": "../images/logo/logo.png",
    "order_id": "<%=orderId%>", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
    "handler": function (response){
        //alert(response.razorpay_payment_id);
        //alert(response.razorpay_order_id);
        //alert(response.razorpay_signature);
        window.location = "PaymentSuccess.aspx?oid=" + $("#hdnorderid").val() + "&pid=" + response.razorpay_payment_id + "&rz_oid=" + response.razorpay_order_id + "";
    },
    "prefill": {
        "name": "<%=name%>",
        "email": "<%=email%>",
        "contact": "<%=mobileno%>"
    },
    "notes": {
        "address": "Razorpay Corporate Office"
    },
    "theme": {
        "color": "#3399cc"
    }
};
var rzp1 = new Razorpay(options);
rzp1.on('payment.failed', function (response){
        //alert(response.error.code);
        //alert(response.error.description);
        //alert(response.error.source);
        //alert(response.error.step);
        //alert(response.error.reason);
        //alert(response.error.metadata.order_id);
        //alert(response.error.metadata.payment_id);
    window.location = "PaymentFail.aspx?oid=" + $("#hdnorderid").val() + "&res=" + response.error.reason + "&des=" + response.error.description + "";
});
document.getElementById('rzp-button1').onclick = function(e){
    rzp1.open();
    e.preventDefault();
}

    </script>
<input type="hidden" value="Hidden Element" name="hidden">
</form>
</body>
</html>

