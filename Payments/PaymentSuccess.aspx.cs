﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Net;
using System.Text;



public partial class Payments_PaymentSuccess : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        hdnorderid.Value = Request.QueryString["oid"] != null ? Request.QueryString["oid"] : "0";
        hdnroid.Value = Request.QueryString["rz_oid"] != null ? Request.QueryString["rz_oid"] : "0";
        hdntxnid.Value = Request.QueryString["pid"] != null ? Request.QueryString["pid"] : "0";
        UserCart objUserCart = new UserCart();
        if (hdntxnid.Value != "0")
        {

            h_txnid.Visible = true;
            new OrderBLL().UpdatePaymentStatus(Convert.ToInt32(hdnorderid.Value), "PaymentSuccess", hdntxnid.Value);

        }
        if (Session[Constants.UserId] != null)
        {
            objUserCart.SessionId = HttpContext.Current.Session.SessionID;
            objUserCart.ProductId = 0;
            objUserCart.CustomerId = Convert.ToInt32(Session[Constants.UserId]);

            new CartBLL().UpdateStockCart(objUserCart);

        }
        else
        {
            objUserCart.SessionId = HttpContext.Current.Session.SessionID;
            objUserCart.ProductId = 0;
            objUserCart.CustomerId = Convert.ToInt32(Session[Constants.UserId]);
            new CartBLL().UpdateStockCart(objUserCart);

        }

        sendmail();

    }

    //private async static Task DeleteValue(int id)
    //{
    //    using (var client = new HttpClient())
    //    {
    //        client.BaseAddress = new Uri("http://localhost:9000/");
    //        client.DefaultRequestHeaders.Accept.Clear();
    //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

    //        // New code:
    //        HttpResponseMessage response = await client.GetAsync("api/products/1");
    //        if (response.IsSuccessStatusCode)
    //        {
    //            //var a = await response.Content.ReadAsAsync> Product> ();
    //            //Console.WriteLine("{0}\t${1}\t{2}", product.Name, product.Price, product.Category);
    //        }
    //    }
    //}
    [WebMethod]
    public static string GetorderData(int orderid) {
        string PointsRedeemVal="",CouponVal = "",orderdate = "", firstname = "", lastname = "", address = "", city = "", pincode="", state = "",email="" ,phoneno="", paymode = "",shipping_charges="",discountamt="", subtotal = "";
        List<Order> lst = new OrderBLL().GetOrderbyOrderId(orderid).ToList();
        JavaScriptSerializer ser = new JavaScriptSerializer();
        foreach (var item in lst)
        {
            orderdate = item.OrderDate.ToString();
            firstname = item.Recipient.ToString();
            lastname = item.RecipientLastName.ToString();
            address = item.Address.ToString();
            city = item.City.ToString();
            pincode = item.Pincode.ToString();
            state = item.City.ToString();
            email = item.Email.ToString();
            phoneno = item.RecipientMobile.ToString();
            paymode = item.PaymentMode.ToString();
            shipping_charges = item.DeliveryCharges.ToString();
            discountamt = item.DisAmt.ToString();
            subtotal = item.BillValue.ToString();
            CouponVal = item.CouponVal.ToString();
            PointsRedeemVal = item.PointsRedeemVal.ToString();


        }

        OrderDetail od = new OrderDetail();
  
        od.OrderId = orderid;
        List<OrderDetail> lstdet = new OrderBLL().GetOrderDetailByOrderId(od);
        StringBuilder str = new StringBuilder();
        List<ShipRocket> SrList = new List<ShipRocket>();   

        //foreach (var item in lstdet)
        //{

        //    str.Append("{'name': '" + item.ProductName + "', 'sku':'" + item.ItemCode + "', 'units': '" + item.Qty + "', 'selling_price': '" + item.Price + "', 'discount': '0', 'tax':'0', 'hsn': 441122}");
        //}

        foreach (var item in lstdet)
        {
            ShipRocket SR = new ShipRocket();
            SR.name = item.ProductName;
            SR.sku = item.ItemCode;
            SR.units = item.Qty;
            SR.selling_price = item.Price.ToString();
            SR.discount = item.DisAmt.ToString();
            SR.tax = item.ServiceTaxAmt.ToString();
            SR.hsn =Convert.ToInt32(0002);
            SrList.Add(SR);
        }
        var JsonData = new
        {
            orderdate = orderdate,
            firstname = firstname,
            lastname = lastname,
            address = address,
            city = city,
            pincode = pincode,
            state = state,
            email = email,
            phoneno = phoneno,
            paymode = paymode,
            shipping_charges = shipping_charges,
            discountamt = discountamt,
            subtotal = subtotal,
            //OrderDetail= str.ToString().TrimEnd(',')

            OrderDetail = SrList.ToArray()

        };
        return ser.Serialize(JsonData);


    }


    public string OrderSummery() {
        int orderid = Convert.ToInt32(hdnorderid.Value);
        string color = "", title = "";
        OrderDetail od = new OrderDetail();
        od.OrderId = orderid;
        List<OrderDetail> lst = new OrderBLL().GetOrderDetailByOrderId(od);
        StringBuilder str = new StringBuilder();
        foreach (var item in lst)
        {
            if (item.Title!= "No Attribute")
            {
                title =","+ item.Title;
            }

            if (item.Color != "-")
            {
                color = "," + item.Color;
            }

        
            str.Append("<tr><td style='font-family: 'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;vertical-align: top;padding: 11.5px;font-size: 13px;'><p style = 'margin-top:0;font-weight:bold;margin-bottom:5.75px;font-size:13px;color:#333;' >"+item.ProductName+" "+ color + " "+Title+" </p><p style = 'margin-top:0;margin-bottom:0;font-size:13px;' ></p></td><td style='font-family: 'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;vertical-align: top;padding: 11.5px;font-size: 13px;color:#333;'>"+item.Qty+ "</td><td style='font-family: 'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;vertical-align: top;padding: 11.5px;font-size: 13px;color:#333;'>₹" +item.Price+"</td></tr>");
        }

        return str.ToString();
    }



    public void sendmail()
    {

        int orderid = Convert.ToInt32(hdnorderid.Value);
        string PointsRedeemVal = "", CouponVal = "", CouponNo ="",taxamt = "",taxper="",netamount="",orderdate = "", firstname = "", lastname = "", address = "", city = "", pincode = "", state = "", email = "", phoneno = "", paymode = "", shipping_charges = "", discountamt = "", subtotal = "";
        List<Order> lst = new OrderBLL().GetOrderbyOrderId(orderid).ToList();

        foreach (var item in lst)
        {
            orderdate = item.OrderDate.ToString();
            firstname = item.Recipient.ToString();
            lastname = item.LastName.ToString();
            address = item.Address.ToString();
            city = item.City.ToString();
            pincode = item.Pincode.ToString();
            state = item.City.ToString();
            email = item.Email.ToString();
            phoneno = item.RecipientMobile.ToString();
            paymode = item.PaymentMode.ToString();
            shipping_charges = item.DeliveryCharges.ToString();
            discountamt = item.DisAmt.ToString();
            subtotal = item.BillValue.ToString();
            netamount = item.NetAmount.ToString();
            taxper = item.ServiceTaxPer.ToString();
            taxamt = item.ServiceTaxAmt.ToString();
            CouponNo = item.CouponNo.ToString();
            CouponVal = item.CouponVal.ToString();
            PointsRedeemVal = item.PointsRedeemVal.ToString();

        }

        using (MailMessage mail = new MailMessage())
        {
            mail.From = new MailAddress("Orders.ebsretail@gmail.com");
            mail.To.Add(email);
            mail.CC.Add("Orders@ebs1952.com");
            mail.Subject = "EBS Order";
            mail.Body = "<html><body>" +

    "<div class='email-main-section' style='float:left;width:100%;padding-top:40px;padding-bottom:40px;'><div class='email-container' style='display: block;width: 70%;padding-left: 150px;border-left: #1010ff 2px solid;margin: 0 auto;'><div class='email-logo' style='float: left;width: 100%;'><img style='width: 180px;' src='http://matrixrfid.co.in:8130/images/logo/logo.png' /></div>" +
    "<div class='email-user' style='float:left; width: 100%;font-family: Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;font-size: 13px;margin-bottom: 12px;color:#333 !important;'>"+ firstname + " ,</div>" +
    "<div class='email-thanks' style='float:left;width: 100%;font-family: Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;font-size: 13px;margin-bottom: 12px;color:#333;'>Thank you for your order from EBS1952. Once your package ships we will send an email with a<br /> link to track your order.If you have questions about your order, you can email us at <br/> <a href = 'mailto:contact@ebs1952.com' > contact@ebs1952.com </a>.</div>" +
    "<div class='email-order-heading' style='color: #222; font-family:Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;font-size: 26px;font-weight: 300;margin-bottom: 6px;margin-top:20px;padding-top:30px;'>Your Order #"+orderid+"</div>" +
        "<div class='email-order-time' style='width: 100%;font-family: Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;font-size: 13px;margin-bottom: 12px;color:#333;'>Placed on "+orderdate+"</div>" +
        "<div class='email-section' style='float: left;width: 100%;margin-top:20px; margin-bottom:20px;'><div class='email-bill-info' style='float: left;width: 50%;'><div class='bill-heading' style='color: #222;font-family:Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;font-weight: 300;margin-bottom: 12px;'>Billing Info</div><div class='bill-address'>" + firstname + "<br />" + address+"<br />"+city+ " , " + state + ", "+pincode+"<br />India<br />T: <a style='color:#1979c3;font-family: Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;font-size: 13px;text-decoration:none;' href = 'tel:"+phoneno+ " '> " + phoneno + " </a></div></div><div class='email-shipping-info' style='float:left; width:50%;'>" +
        "<div class='bill-heading' style='color: #222;font-family: Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;font-weight: 300;margin-bottom: 12px;'>Shipping Info</div><div class='bill-address' style='width: 100%;font-family: Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;font-size: 13px;margin-bottom: 12px;color:#333;'>" + firstname + "<br />" + address+ "<br />" + city + " , " + state + ", " + pincode + "<br />India<br />T: <a style='color:#1979c3;font-family: Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;font-size: 13px;text-decoration:none;' href = 'tel:" + phoneno + "'> " + phoneno + " </a>" +
        "</div></div></div><div class='email-payment-method' style='float: left;width: 100%; padding-bottom:30px;'><div class='payment-method' style='float: left;width: 50%;'><div class='payment-method-heading' style='color: #222;font-family: Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;font-weight: 300;margin-bottom: 12px;font-size:18px;'>Payment Method</div><div class='payment-mode'>"+paymode+"</div></div>" +
            "<div class='shipping-method' style='float: left;width: 50%;'><div class='payment-method-heading' style='color: #222;font-family:Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;font-weight: 300;margin-bottom: 12px;font-size:18px;'>Coupon Code</div><div class='payment-mode' style='width: 100%;font-family: Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;font-size: 13px;margin-bottom: 12px;color:#333;'>"+CouponNo+"</div></div></div>" +

        "<div class='order-detail' style='float: left;width: 100%;'>" +
            "<table style='border: 1px solid #d1d1d1;width:75%;'>" +
                "<tr>" +
                    "<th style='font-family: Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;text-align: left;vertical-align: bottom;background-color: #f2f2f2;padding: 11.5px;font-size: 13px;font-weight: 700;color:#333;'>Items</th>" +
                    "<th style='font-family: Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;text-align: left;vertical-align: bottom;background-color: #f2f2f2;padding: 11.5px;font-size: 13px;font-weight: 700;color:#333;'>Qty</th>" +
                    "<th style='font-family: Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;text-align: left;vertical-align: bottom;background-color: #f2f2f2;padding: 11.5px;font-size: 13px;font-weight: 700;color:#333;'>Price</th>" +
                "</tr>" 
                
                + OrderSummery()+


                "<tfoot style='background: #f2f2f2;'>" +
                "<tr>" +
                    "<th colspan = '2' style='font-weight: normal !important;text-align: right !important;color:#333;'> Subtotal </th>" +

                    "<td style='font-family: Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;vertical-align: top;padding: 11.5px;font-size: 13px;'><p style = 'margin-top:0;font-weight:bold;margin-bottom:5.75px;font-size:13px;color:#333;'>₹" + subtotal+"</td>" +
                "</tr>" +
                "<tr>" +
                    "<th colspan = '2' style='font-weight: normal !important;text-align: right !important;color:#333;'>Shipping & Handling</th>" +

                    "<td style='font-family: Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;vertical-align: top;padding: 11.5px;font-size: 13px;'><p style = 'color:#333;margin-top:0;font-weight:bold;margin-bottom:5.75px;font-size:13px;'>₹"+shipping_charges+"</td>" +
                "</tr>" +
                //"<tr>" +
                //    "<th colspan = '2' style='font-weight: normal !important;text-align: right !important;color:#333;'> Grand Total(Excl.Tax)</th>" +
                //    "<td style='font-family: 'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;vertical-align: top;padding: 11.5px;font-size: 13px;'><p style = 'color:#333;margin-top:0;font-weight:bold;margin-bottom:5.75px;font-size:13px;'>₹"+netamount+"</td>" +
                //"</tr>" +
                "<tr>" +
                    "<th colspan = '2' style='font-weight: normal !important;text-align: right !important;color:#333;'>IGST - "+taxper+ "(W.B)(" + taxper + " %)</th>" +

                    "<td style='font-family: Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;vertical-align: top;padding: 11.5px;font-size: 13px;'><p style = 'margin-top:0;font-weight:bold;margin-bottom:5.75px;font-size:13px;color:#333;'>₹"+taxamt+"</td>" +
                "</tr>" +

                        "<tr>" +
                    "<th colspan = '2' style='font-weight: normal !important;text-align: right !important;color:#333;'>Discount</th>" +

                    "<td style='font-family:Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;vertical-align: top;padding: 11.5px;font-size: 13px;'><p style = 'margin-top:0;font-weight:bold;margin-bottom:5.75px;font-size:13px;color:#333;'>₹" + discountamt + "</td>" +
                "</tr>" +
                                        "<tr>" +
                    "<th colspan = '2' style='font-weight: normal !important;text-align: right !important;color:#333;'>Coupon Discount</th>" +

                    "<td style='font-family:Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;vertical-align: top;padding: 11.5px;font-size: 13px;'><p style = 'margin-top:0;font-weight:bold;margin-bottom:5.75px;font-size:13px;color:#333;'>₹" + CouponVal + "</td>" +
                "</tr>" +
                                                "<tr>" +
                    "<th colspan = '2' style='font-weight: normal !important;text-align: right !important;color:#333;'>Points Discount</th>" +

                    "<td style='font-family:Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;vertical-align: top;padding: 11.5px;font-size: 13px;'><p style = 'margin-top:0;font-weight:bold;margin-bottom:5.75px;font-size:13px;color:#333;'>₹" + PointsRedeemVal + "</td>" +
                "</tr>" +
                "<tr>" +
                    "<th colspan = '2' style='font-weight: normal !important;text-align: right !important;color:#333;'>Tax</th>" +

                    "<td style='font-family:Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;vertical-align: top;padding: 11.5px;font-size: 13px;'><p style = 'margin-top:0;font-weight:bold;margin-bottom:5.75px;font-size:13px;color:#333;'>₹" + taxamt + "</td>" +
                "</tr>" +
                "<tr>" +
                    "<th colspan = '2' style='font-weight: normal !important;text-align: right !important;color:#333;'>Grand Total(Incl.Tax)</th>" +
                    "<td style='font-family: Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;vertical-align: top;padding: 11.5px;font-size: 13px;'><p style = 'color:#333;margin-top:0;font-weight:bold;margin-bottom:5.75px;font-size:13px;'>₹" + netamount + "</td>" +
                "</tr>" +
                    "</tfoot>" +
            "</table>" +

        "</div>" +
        "<div class='gst' style='width: 100%;font-family: Open Sans,Helvetica Neue,Helvetica,Arial,sans-serif;font-size: 13px;margin-bottom: 12px;color:#333;'>EBS RETAIL GST NO. 04BCFPC7639P1Z9</div>" +

    "</div>" +
    "</div>" +
     "</body>" +
    "</html>";
            mail.IsBodyHtml = true;

            using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
            {
                smtp.Credentials = new NetworkCredential("Orders.ebsretail@gmail.com", "Ebsretail1952@");
                smtp.EnableSsl = true;
                smtp.Send(mail);
            }
         
        }


    }
}