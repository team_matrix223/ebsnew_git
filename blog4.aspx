﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="blog4.aspx.cs" Inherits="blog4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="customcss/blog.css" rel="stylesheet" />
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9 col-sm-9 col-xs-12">
				<div class="blog">
					<h2 class="blog-title">Fun art and craft DIY ideas to do at home </h2>
					<div class="post-info">
							<div class="item post-posed-date">
							<span class="label">Posted:</span>
							<span class="value">January 13, 2021</span>
							</div>
							<div class="item post-categories">
							<span class="label">Categories:</span>
							<a title="Paper Stationery" href="#">Art and Craft</a></div>
							<div class="item post-author">
							<span class="label">Author:</span>
							<span class="value">
							<a title="Varun Chaudhary" href="#">
							Varun Chaudhary
							</a>
							</span>
							</div>
					</div>
					<div class="blog-img-div">
						<img src="images/blog/blog4.jpeg" />
					</div>
					<div class="blog-desc">
						<p>There is nothing creative minds cannot do. They can create some incredible stuff with the least of items.&nbsp; Art and craft is the first thing a creative mind puts his hands on. There are different types of craft. It ranges from the simplest crafts to really complex techniques of the craft. However, there are many fun art &amp; craft DIY ideas that even an amateur can effortlessly do at home. In this article, you will get to read about the <strong>fun art &amp; craft DIY ideas that you can do at your place. </strong>Give a read to some of the budget-friendly and really fun craft ideas.</p>
						<p><strong>Tattooed patterned tumbler – </strong>Use a tattooed sheet that you can get from a printer or simply buy it from the market. Wrap it up around the tumblers, old pen stands, mugs, planter etc. It will make these items look pretty. Well, it will be much more relevant if you dress up your planter. The whole concept of a planter being dressed up using elegant tattooed paper will give way to an elegant-looking item and will be suitable enough.</p>
						<p><strong>Paper minions – </strong>When it comes to easy craft, making of a minion is the primary idea that strikes. It is one of the easiest things to be made at home. Just cut down the papers and sheets into a cylindrical shape and use your sketch pens to draw the features. This DIY art is certainly the simplest and the best.</p>
						<p><strong>Lanterns made using paper – </strong>Paper lanterns are one of the first things you can make while exploring your craft skills. They are mainly associated with festivities. You can easily make one. Whether you are an <strong>adult or a kid, this easy craft would be good for a neat start. </strong>Take a sheet and cut two strips down the long side of the sheet. Fold the second sheet into half giving it a rectangular shape. Draw pencil marks going down the rectangular area. Cut the marked lines and leave an inch towards the end. Now, just open it up and staple both the ends. The first sheet needs to be rolled up and inserted inside the lanterns. This is how you can make lanterns.</p>
						<p><strong>Make a flower using socks and wires – </strong>This may sound a little unusual, but yes, you can make a flower using craft wires. Twirl the wire into the shape of a flower and cover it with a used sock. It is an easy way of utilizing your skills for the best craftwork, however, if you do it carefully, you will be good to go.</p>
						<p><strong>Painted market basket – </strong>Get yourself an ordinarily used basket and paint it up accordingly. Adding to this, you can put some stones and artificial ornaments on the basket. This will add to the bold design look. Chose the colour of your choice and place it at your aesthetic corner to enhance the overall view.</p>
						<p><strong>Racing cars – </strong>You can get your kids to make these cute little racing cars. This can be considered as one of the best <strong><a href="#" target="_blank" rel="noopener">art and craft DIY ideas for kids</a>. </strong>&nbsp;You can help the minors with this. Use toilet paper rolls, craft sheets and cardboard papers for the same. You can use the cardboard to make the tyres. It is all about cutting and pasting. Engage the kids and they will adore it.</p>
						<p><strong>Teacup candles – </strong>Get yourself a gorgeous-looking vintage teacup. Put melted wax inside of the cup and give way to tea-cup candles. It is a unique <strong>DIY idea for home decorations. </strong>Place it in your room or office and enjoy the way it soothes the atmosphere.</p>
						<p>So, these are some of the <strong>DIY craft ideas for adults and kids.</strong></p>
						<p><strong><a href="https://www.ebs1952.com/" target="_blank" rel="noopener">India's biggest online stationery store</a></strong></p>
						<p><a href="#" target="_blank" rel="noopener">EBS is one of the biggest online stationery stores in India.</a> You will get the best of art and craft items in the store.&nbsp; Adding to this, you will get every stationery item here. Be it different pens of different types, craft papers of numerous types, colours, pencils, stands and whatnot. In addition to this, you will get much of these items at pocket friendly rates.</p>
						<p>So, EBS is one-stop-shop for all the craft and stationery lovers to buy all such countless Items.</p>
						<p>So, pull up your socks and engage yourself in these easy-to-do crafts and polish your related skills.</p>

					</div>
			</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="recent-section">
					<div class="recent-title">
						<strong>Recent Posts</strong>
					</div>
					<div class="recent-des">
						<div class="recent-img"><a href="/blog.aspx"><img src="images/blog/blog1.jpeg" /></a>
							
						</div>
						<div class="recent-tle">
							<a href="/blog.aspx">Functional planners and journals for women </a>
						</div>
						<div class="blog-date">January 27,2021</div>
					</div>
					<div class="recent-des">
							<div class="recent-img"><a href="/blog3.aspx"><img src="images/blog/blog3.jpeg" /></a></div>
						<div class="recent-tle">
							<a href="/blog3.aspx">BACK TO SCHOOL SUPPLIES LIST 2021</a>
						</div>
						<div class="blog-date">January 20,2021</div>
					</div>
				
				
					<div class="recent-des">
							<div class="recent-img"><a href="/blog5.aspx"><img src="images/blog/blog5.jpeg" /></a></div>
						<div class="recent-tle">
							<a href="/blog5.aspx">DIY CRAFT IDEAS AND ACTIVITIES FOR ADULTS & KIDS.</a>
						</div>
						<div class="blog-date">January 09,2021</div>
					</div>
				
				
					<div class="recent-des">
							<div class="recent-img"><a href="/blog2.aspx"><img src="images/blog/blog2.jpeg" /></a></div>
						<div class="recent-tle">
							<a href="/blog2.aspx">EVERYTHING YOU NEED TO KNOW ABOUT MANDALA</a>
						</div>
						<div class="blog-date">November  18,2020</div>
					</div>
				
				</div>
			</div>
		</div>
	</div>
</asp:Content>

