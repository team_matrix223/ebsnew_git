﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="shippingandreturns.aspx.cs" Inherits="shippingandreturns" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
		<link href="customcss/aboutus.css" rel="stylesheet" />
	  <div class="constraint no-padding">
        <div class="breadcrumbs">
            <ul class="breadcrumbs_container">
                <li class="breadcrumbs_item">
                    <a class="breadcrumbs_link" href="/index.aspx">Home</a>
                </li>
                <li class="breadcrumbs_item breadcrumbs_item-active">Shipping & Returns</li>
            </ul>
        </div>
    </div>
	<div class="container-fluid">
		<h1 class="responsiveProductListHeader_title">Shipping & Returns</h1>
		
		<div class="about-us-content">
			<ul class="about-ul">
				<li>Ebs retail offers 10 days replacement / return gap only if the product&nbsp;received is&nbsp;damaged. Mail the pictures&nbsp;of the defected product on the day of receiving the shipment and we will send you&nbsp;the replacement within a few days.<br><br></li>
				<li>All exchanges must be in unused condition with all original tags and packaging intact.<br><br></li>
				<li>To schedule an exchange please drop us an email at&nbsp;sales@ebsretail.com&nbsp;along with your order and exchange details.<br><br></li>
				<li>Exchanges are only applicable to items within the same price range.<br>You can only apply for a Return/Exchange from your ebs retail account. Login to your account and go the ‘<strong>My Orders</strong>’ section. Click on ‘<strong>View/Edit Order Details</strong>’ for the respective order.</li>
			</ul>
			<p></p>
			<ul class="about-ul">
				<li>Ebs retail has self ship policy for returning and exchange of a product. Since we do not have a reverse pick up facility, we would request the customer to self-ship the product to us. When we would receive the product then only we can initiate the process of return or exchange. The expense of the self-shipping would entirely be borne by the customer.</li>
				<li>While exchanging the products which are purchased, in case the products requested for exchange are not available in stock the amount will be refunded as cash.<br><br></li>
				<li>If you have to return or exchange anything from a combo pack, the whole pack will have to be returned. There will not be any partial returns accepted for this. If there is a manufacturing issue, or if you have any other query regarding this, you can contact us.<br><br></li>
				<li>If an order placed during a sale is returned to us, only the amount paid by you will be refunded, not the current product price.<br><br></li>
				<li>Gift cards and vouchers are non-refundable.</li>
			</ul>
			<h4>DISCLAIMER</h4>
			<p>The images of the products shown on the website may be slightly different than the product delivered in terms of shade and color as the images shown on the website are graphical representation of the product.</p>
			<p>Ebs retail takes no responsibility if in the due course of usage of the product there are any&nbsp;damages. If the customer has any query about the product they can email us and ask us beforehand.&nbsp;Damage to the product after it is used is sole responsibility of the customer. To claim an exchange or refund for a clearance product, please get in touch with us as soon as you&nbsp;get the delivery.</p>
			<p>Please note shipping charges applied will not be refunded.</p>
			<h4>CANCELLATION POLICY</h4>
			<ul  class="about-ul">
				<li>To cancel an order, please contact us&nbsp;and get a refund for your payment.<br>The refund amount will be refunded to you within 7 - 10 working days .</li>
				<li>Orders that have not been shipped can be cancelled by placing a request over sales@ebs retail.com.&nbsp;If the order is shipped already, unfortunately, we wouldn’t be able to cancel it.</li>
				<li>Once you have accepted the shipment, you cannot return the products.</li>
				<li>Please note if the order is cancelled after shipping or if you refuse to accept the shipment then shipping charges applied would not be refunded.<br><br></li>
			</ul>
			<p>In case you desire any changes or edits in your order, please write to us. Our customer care representatives will help you out with customized solutions wherever possible.</p>
			<h4>PAYMENT</h4>
			<p>Ebs Retail accepts all major debit cards and credit cards. The payment option available for your location will be visible upon checkout.</p>

		</div>
		
	</div>
</asp:Content>

