﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class new_page : System.Web.UI.Page
{
    readonly PagedDataSource _pgsource = new PagedDataSource();
    int _firstIndex, _lastIndex;
    private int _pageSize = 10;

    private int CurrentPage
    {
        get
        {
            if (ViewState["CurrentPage"] == null)
            {
                return 0;
            }
            return ((int)ViewState["CurrentPage"]);
        }
        set
        {
            ViewState["CurrentPage"] = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
	{
        if (!IsPostBack)
        {
            hdnGetMethod.Value = Request.QueryString["gm"] != null ? Request.QueryString["gm"] : "0";
            BindDataIntoRepeater();
            BindSlider(hdnGetMethod.Value);


        }
	}



    public void BindSlider(string SliderType) {
        DataSet ds = new DataSet();
        ds = new SlidersBLL().PageSlider("get", SliderType, 0);
        StringBuilder str = new StringBuilder();
        string classactive = "";
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            if (i == 0)
            {
                classactive = "item active";
            }
            else {

                classactive = "item";
            }
            str.Append("<div class='"+ classactive + "'><img src = " + ds.Tables[0].Rows[i]["ImageUrl"] + " alt = '...' /><div class='carousel-caption'></div></div>");
        }

        ltr_slider.Text = str.ToString();

        }
    public DataTable BindProduct()
    {
        for (int i = 0; i < chk_brands.Items.Count; i++)
        {
            if (chk_brands.Items[i].Selected)
            {

                hdnbrands.Value = hdnbrands.Value + chk_brands.Items[i].Value + ",";
            }

        }
        string Brands = hdnbrands.Value.TrimEnd(',');
        string Level1= hdnLevel1.Value;
        string Level2 = hdnLevel2.Value;
        string Level3 = hdnLevel3.Value;
        int PageId=1;
        int MinPrice =Convert.ToInt32(hdnminprice.Value);
        int MaxPrice = Convert.ToInt32(hdnmaxprice.Value);
        string BrandId= hdnBrandId.Value;
        string GroupId = hdnGroupId.Value;
        string SubGroupId=hdnGroupId.Value;
        string GetMethod = hdnGetMethod.Value;





        Int64 l1 = CommonFunctions.IsNumeric(Level1);
        Int64 l2 = CommonFunctions.IsNumeric(Level2);
        Int64 l3 = CommonFunctions.IsNumeric(Level3);

        Int64 BID = CommonFunctions.IsNumeric(BrandId);
        Int64 GID = CommonFunctions.IsNumeric(GroupId);
        Int64 SGID = CommonFunctions.IsNumeric(SubGroupId);


        if (l3 > 0)
        {
            l1 = 0;
            l2 = 0;
        }
        else if (l2 > 0)
        {
            l1 = 0;
            l3 = 0;
        }
        else if (l1 > 0)
        {
            l2 = 0;
            l3 = 0;
        }
        else
        {
            l1 = 0;
            l2 = 0;
            l3 = 0;
        }

        DataSet ds = new ProductsBLL().GetNewProducts(GetMethod, HttpContext.Current.Session.SessionID, Brands, l1, l2, l3,PageId, MinPrice, MaxPrice,  BID, GID, SGID);
        DataTable dt = new DataTable();
        dt = ds.Tables[1];
        if (dt.Rows.Count>0)
        {

     
        if (!IsPostBack)
        {
            chk_brands.DataSource = ds.Tables[2];
            chk_brands.DataBind();
        }
 
        hdnsliderminprice.Value = dt.Rows[0]["min_price"].ToString();
        hdnslidermaxprice.Value = dt.Rows[0]["max_price"].ToString();
        }
 
        hdnbrands.Value = "";
        //hdnminprice.Value = "";
        //hdnmaxprice.Value = "";
        return dt;
    }
    // Bind PagedDataSource into Repeater
    private void BindDataIntoRepeater()
    {
        var dt = BindProduct();
        _pgsource.DataSource = dt.DefaultView;
        _pgsource.AllowPaging = true;
        // Number of items to be displayed in the Repeater
        _pgsource.PageSize = _pageSize;
        _pgsource.CurrentPageIndex = CurrentPage;
        // Keep the Total pages in View State
        ViewState["TotalPages"] = _pgsource.PageCount;
        // Example: "Page 1 of 10"
        lblpage.Text = "Page " + (CurrentPage + 1) + " of " + _pgsource.PageCount;
        // Enable First, Last, Previous, Next buttons
        lbPrevious.Enabled = !_pgsource.IsFirstPage;
        lbNext.Enabled = !_pgsource.IsLastPage;
        //lbFirst.Enabled = !_pgsource.IsFirstPage;
        //lbLast.Enabled = !_pgsource.IsLastPage;

        // Bind data into repeater
        RptProduct.DataSource = _pgsource;
        RptProduct.DataBind();

        // Call the function to do paging
        HandlePaging();
    }
    private void HandlePaging()
    {
        var dt = new DataTable();
        dt.Columns.Add("PageIndex"); //Start from 0
        dt.Columns.Add("PageText"); //Start from 1

        _firstIndex = CurrentPage - 5;
        if (CurrentPage > 5)
            _lastIndex = CurrentPage + 5;
        else
            _lastIndex = 10;

        // Check last page is greater than total page then reduced it 
        // to total no. of page is last index
        if (_lastIndex > Convert.ToInt32(ViewState["TotalPages"]))
        {
            _lastIndex = Convert.ToInt32(ViewState["TotalPages"]);
            _firstIndex = _lastIndex - 10;
        }

        if (_firstIndex < 0)
            _firstIndex = 0;

        // Now creating page number based on above first and last page index
        for (var i = _firstIndex; i < _lastIndex; i++)
        {
            var dr = dt.NewRow();
            dr[0] = i;
            dr[1] = i + 1;
            dt.Rows.Add(dr);
        }

        rptPaging.DataSource = dt;
        rptPaging.DataBind();
    }

    protected void lbPrevious_Click(object sender, EventArgs e)
    {
        CurrentPage -= 1;
        BindDataIntoRepeater();
    }
    protected void lbNext_Click(object sender, EventArgs e)
    {
        CurrentPage += 1;
        BindDataIntoRepeater();
    }

    protected void rptPaging_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (!e.CommandName.Equals("newPage")) return;
        CurrentPage = Convert.ToInt32(e.CommandArgument.ToString());
        BindDataIntoRepeater();
    }

    protected void rptPaging_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        var lnkPage = (LinkButton)e.Item.FindControl("lbPaging");
        if (lnkPage.CommandArgument != CurrentPage.ToString()) return;
        lnkPage.Enabled = false;
 
        lnkPage.BackColor = Color.FromName("#000");
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        BindDataIntoRepeater();
    }




    protected void chk_brands_SelectedIndexChanged(object sender, EventArgs e)
    {
    
       
        BindDataIntoRepeater();
    }
}