﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="aboutus.aspx.cs" Inherits="aboutus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="customcss/aboutus.css" rel="stylesheet" />
	  <div class="constraint no-padding">
        <div class="breadcrumbs">
            <ul class="breadcrumbs_container">
                <li class="breadcrumbs_item">
                    <a class="breadcrumbs_link" href="/index.aspx">Home</a>
                </li>
                <li class="breadcrumbs_item breadcrumbs_item-active">About us</li>
            </ul>
        </div>
    </div>
	<div class="container-fluid">
		  <h1 class="responsiveProductListHeader_title">About us</h1>
		<div class="about-banner-section">
			<img src="images/about/About_us_Page_banner.jpg" />
		</div>
		<div class="about-us-content">
			<h2>ES - The Best Online Stationery Store<br><br></h2>
			<h4>Who We Are</h4>
			<p>EBS is one of the best online stationery stores in India. We started our journey seven decades ago to rise to the challenge of India's growing stationery demand. Today we have a collection of more than 10,000 online stationery products ready for delivery pan India. We are the go-to place for the widest range of paper products, writing equipment (premium pens, fountain pen, highlighters, calligraphy pens, markers, notebooks, journals, diaries, planners and many more) office stationery and school stationery as well as fancy stationery items in India. We are committed to the delivery of premium stationery products online in response to our customers' diverse demands. Being the best Online stationery suppliers, we cater to all interests in India's stationery domain: whether individual, retail or wholesale. </p>
			<h4>Our Philosophy</h4>
			<p>We at EBS are passionate about plugging the gaps in India's online stationery supply demand. Perhaps this is what makes us a reliable online stationery shop for all of our loyal customers. We ensure to offer the best of affordable art and craft supplies, paper products and other stationery items under one website. For this, we have a dedicated team of highly experienced professionals who work hard to keep track of the latest stationery product launches. These products belong to both Indian as well as international brands. This explains our ever-growing collection of the best online stationery available. </p>
			<h4>Our Objectives</h4>
			<p>EBS is guided by a crystal-clear objective: to become the one-stop destination for all of India's online school stationery, online office stationery and other stationery requirements. Whether you are looking to buy the best art and craft material online for personal use or buy fancy stationery gift option for your loved ones, we have got you covered. By virtue of our extensive experience, we understand India's highly specific online stationery requirements. <br>Having worked for nearly seven decades in the field, we at EBS are highly familiar with the most suitable school, office and other stationery items in demand online. Our team regularly monitors our exhaustive collection to update it with every possible stationery product for retail or wholesale purchase.</p>
			<h4>Our Vision</h4>
			<p>We at EBS strongly believe that every person has a creative mind which needs to be unleashed. We aim at giving an opportunity to each person by bringing together a diversified range of best stationery items online. We understand that stationery supplies happen to be one of the initial tools which you use for your literary or artistic creations. This is the reason why we are so strongly committed to providing access to top-quality yet affordable stationery products on our website. Our vision is to become the connecting link between people's innate creativity and its outward expression. </p>

		</div>
		<div class="expect-section">
			<h1>What To Expect ?</h1>
			<h3>Nothing we would't buy ourselves</h3>
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="about-img">
						<img src="images/about/ABout_Us_page_content.jpg" />
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="about-img">
						<img src="images/about/ABout_Us_page_content_-_2.jpg" />
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="about-img">
						<img src="images/about/ABout_Us_page_content_-_3.jpg" />
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="about-img">
						<img src="images/about/ABout_Us_page_content_-_4.jpg" />
					</div>
				</div>
			</div>
		</div>
	</div>
</asp:Content>

