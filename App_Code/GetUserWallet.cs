﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService] 

public class GetUserWallet : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string GetWalletList(int UserId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@UserId", UserId);
        DataSet ds = null;
        ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        "Shopping_sp_WalletGetBalanceByUser", objParam);

        List<WSWallet> lstWallet = new List<WSWallet>();

        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            WSWallet objWallet = new WSWallet();
            objWallet.UserId = Convert.ToInt32(ds.Tables[0].Rows[i]["UserId"]);
            objWallet.Total = Convert.ToDecimal(ds.Tables[0].Rows[i]["Total"].ToString());
            objWallet.Used = Convert.ToDecimal(ds.Tables[0].Rows[i]["Used"].ToString());
            objWallet.Left = Convert.ToDecimal(objWallet.Total) - Convert.ToDecimal(objWallet.Used);
            lstWallet.Add(objWallet);

        }

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            WalletList = lstWallet,
        };
        return ser.Serialize(JsonData);


    }
    
}
