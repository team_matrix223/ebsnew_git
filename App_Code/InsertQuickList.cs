﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class InsertQuickList : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string InsertData(string ProductNameArr, string qtyArr, string unitArr, Int32 UserId, Int32 DeliveryAddressId, DateTime DeliveryDate, Int32 DeliverySlot, string OrderType)
    {





        ProductNameArr = ProductNameArr.Substring(0, ProductNameArr.Length - 1);
        qtyArr = qtyArr.Substring(0, qtyArr.Length - 1);
        unitArr = unitArr.Substring(0, unitArr.Length - 1);

        Int32 uId = UserId;
        QuickList objQuickList = new QuickList()
        {
            UserId = uId,
            DeliveryAddressId = Convert.ToInt32(DeliveryAddressId),
            DeliveryDate = Convert.ToDateTime(DeliveryDate),
            DeliverySlot = DeliverySlot,


        };


        string[] ProductName = ProductNameArr.Split(',');
        string[] Qty = qtyArr.Split(',');
        string[] Unit = unitArr.Split(',');





        DataTable dt = new DataTable();
        dt.Columns.Add("ProductName");
        dt.Columns.Add("Qty");
        dt.Columns.Add("Unit");





        for (int i = 0; i < ProductName.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["ProductName"] = ProductName[i];
            dr["Qty"] = Qty[i];
            dr["Unit"] = Unit[i];

            dt.Rows.Add(dr);
        }



        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new QuickListBLL().InsertQuickList(objQuickList, dt,OrderType);
        var JsonData = new
        {
            list = status,

        };
        return ser.Serialize(JsonData);


    }
    
}
