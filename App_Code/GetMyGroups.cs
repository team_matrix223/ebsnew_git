﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class GetMyGroups : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string MyGroups()
    {


        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@Location", "HomeGuide");
        DataSet ds = null;
        ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        "shopping_sp_GetMyGroupsByLocation", objParam);



        List<WSMyGroups> lstGroups = new List<WSMyGroups>();

        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            WSMyGroups  objGroup = new WSMyGroups(){
            Title = ds.Tables[0].Rows[i]["Title"].ToString(),
            HasSubGroup = Convert.ToBoolean(ds.Tables[0].Rows[i]["HasSubGroup"].ToString()),
            Id = Convert.ToInt32(ds.Tables[0].Rows[i]["Id"].ToString()),       
            PhotoUrl1 =  ds.Tables[0].Rows[i]["MobileImgUrl"].ToString(),
        };
            lstGroups.Add(objGroup);
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            MyGroups = lstGroups,
        };
        return ser.Serialize(JsonData);


    }  
}
