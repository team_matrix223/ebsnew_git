﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

 [System.Web.Script.Services.ScriptService]
public class GetDiscountCoupon : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string GetDiscount(string CouponNo, Int64 CustomerId)
    {
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@CouponNo", CouponNo);
        objParam[1] = new SqlParameter("@CustomerId", CustomerId);
        DataSet ds = null;
        ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        "shopping_sp_OffersApplyForMobile", objParam);

        List<WSCoupon> lstWallet = new List<WSCoupon>();

        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            WSCoupon objWallet = new WSCoupon();
            objWallet.Message = Convert.ToString(ds.Tables[0].Rows[i]["Message"]);
            objWallet.DisAmt = Convert.ToInt32(ds.Tables[0].Rows[i]["DisAmt"]);
            objWallet.DisType = Convert.ToString(ds.Tables[0].Rows[i]["DisType"]);
            objWallet.CouponNo = Convert.ToString(ds.Tables[0].Rows[i]["CouponNo"]);
            lstWallet.Add(objWallet);

        }

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            WalletList = lstWallet,
        };
        return ser.Serialize(JsonData);


    }
    
}
