﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class GetWalletGift : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string GetWalletList(int UserId)
    {
        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        "shopping_sp_ReimbursementGiftsGetByIsActive", objParam);

        List<WalletGift> lstWallet = new List<WalletGift>();

        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            WalletGift objWallet = new WalletGift();
            objWallet.GiftId = Convert.ToInt32(ds.Tables[0].Rows[i]["GiftId"]);
            objWallet.Name = Convert.ToString(ds.Tables[0].Rows[i]["Name"].ToString());
            objWallet.PhotoUrl = Convert.ToString(ds.Tables[0].Rows[i]["PhotoUrl"].ToString());
            objWallet.Points = Convert.ToDecimal(ds.Tables[0].Rows[i]["Points"].ToString());
            objWallet.Cash = Convert.ToDecimal(ds.Tables[0].Rows[i]["Cash"].ToString());
            objWallet.DOC = Convert.ToDateTime(ds.Tables[0].Rows[i]["DOC"].ToString());
            objWallet.IsActive = Convert.ToBoolean(ds.Tables[0].Rows[i]["IsActive"].ToString());
            lstWallet.Add(objWallet);

        }

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            WalletList = lstWallet,
        };
        return ser.Serialize(JsonData);


    }
    
}
