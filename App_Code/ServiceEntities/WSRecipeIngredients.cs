﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class WSRecipeIngredients
{ 
   
    public int RecipeId { get; set; }
    public decimal Qty { get; set; }
    public string ProductName { get; set; }
    public string Unit { get; set; }
    public string Url { get; set; }
    public string Title { get; set; }
    public string Process { get; set; }
    public string ShortDesc { get; set; }

	public WSRecipeIngredients()
	{
		Url = string.Empty;
        RecipeId = 0;
        Qty = 0;
        ProductName = string.Empty;
        Unit = string.Empty;
        Title = string.Empty;
        Process = string.Empty;
        ShortDesc = string.Empty;

	}
}