﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for WSPincode
/// </summary>
public class WSPincode
{
    public int PinCodeId { get; set; }
    public string Sector { get; set; }

	public WSPincode()
	{
        PinCodeId = 0;
        Sector = "";
	}
}