﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class WSProduct
{
    public int VariationId { get; set; }
    public int ProductId { get; set; }
    public string Name { get; set; }
    public string ShortName { get; set; }
    public int Qty { get; set; }
    public decimal   Price { get; set; }
    public decimal  Mrp { get; set; }
    public string BrandName { get; set; }
    public int BrandId { get; set; }
    public string Unit { get; set; }
    public string Type { get; set; }
    public string PhotoUrl { get; set; }
	public WSProduct()
	{
        ProductId = 0;
        VariationId = 0;
        Name = string.Empty;
        ShortName  = string.Empty;
        Qty = 0;
        Price = 0;
        Mrp = 0;
        BrandName = string.Empty;
        Unit = string.Empty;
        Type = string.Empty;
        PhotoUrl = string.Empty;
        BrandId = 0;
	}
}