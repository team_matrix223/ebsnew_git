﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class WSUser
{
    public int UserId { get; set; }
    public string FirstName { get; set; } 
    public string Password { get; set; }
    public bool IsActive { get; set; } 
    public string MobileNo { get; set; }  
    public string EmailId { get; set; }
    public string DOB { get; set; }
    public string DOA { get; set; }
	public WSUser()
	{
        UserId = 0;
        FirstName = "";
        MobileNo = "";
        Password = "";
        EmailId = "";
        DOB = "";
        DOA = "";
	}
}