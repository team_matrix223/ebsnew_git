﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class WSMyGroups
{	public int Id { get; set; }
    public string Title { get; set; }    
    public string PhotoUrl1 { get; set; }
    public int GroupId { get; set; }
    public bool HasSubGroup { get; set; }
    public List<WSSubCategories> SubCategories { get; set; }

	public WSMyGroups()
	{
        Id = 0;
        Title = string.Empty;
        PhotoUrl1 = string.Empty;
        GroupId = 0;
        HasSubGroup = false;
	}
}