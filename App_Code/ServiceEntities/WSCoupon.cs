﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for WSCoupon
/// </summary>
public class WSCoupon
{
    public string Message { get; set; }
    public decimal DisAmt { get; set; }
    public string DisType { get; set; }
    public string CouponNo { get; set; }

	public WSCoupon()
	{
        Message = string.Empty;
        DisType = string.Empty;
        DisAmt = 0;
        CouponNo = string.Empty; 
    }
}