﻿using System;
using System.Collections.Generic;
using System.Web;

public class WSCategory
{
    public int CategoryId { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string PhotoUrl { get; set; }
    public List<WSSubCategories> SubCategories { get; set; }

	public WSCategory()
	{
        CategoryId = 0;
        Title = string.Empty;
        Description = string.Empty;
        PhotoUrl = string.Empty;
       
      
	}
}

 