﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class WSMySubGroup
{	public int SubGroupId { get; set; }
    public string SubGroupTitle { get; set; }    
    public string PhotoUrl { get; set; }
    public int CategoryId { get; set; }
    public int LevelNo { get; set; }
	public WSMySubGroup()
	{
        SubGroupId = 0;
        SubGroupTitle = string.Empty;
        PhotoUrl = string.Empty;
        CategoryId = 0;
        LevelNo = 0;
	}
}