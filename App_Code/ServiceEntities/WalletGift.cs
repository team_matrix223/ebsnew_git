﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for WalletGift
/// </summary>
public class WalletGift
{
    public int GiftId { get; set; }
    public string Name { get; set; }
    public string PhotoUrl { get; set; }
    public decimal Points { get; set; }
    public decimal Cash { get; set; }
    public DateTime DOC { get; set; }
    public bool IsActive { get; set; }

	public WalletGift()
	{
        GiftId = 0;
        Name = string.Empty;
        PhotoUrl = string.Empty;
        Points = 0;
        Cash = 0;
        DOC = DateTime.Now;
        IsActive = false;
	}
}