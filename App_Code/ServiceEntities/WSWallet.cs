﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for WSPincode
/// </summary>
public class WSWallet
{
    public int UserId { get; set; }
    public decimal Points { get; set; }

    public bool Status { get; set; }
    public int DisAmt { get; set; }
    public decimal DisVal { get; set; }
    public int PointRate { get; set; }
    public decimal Total { get; set; }
    public decimal Used { get; set; }
    public decimal Left { get; set; }

    public WSWallet()
	{
        UserId = 0;
        Points = 0;
        Status = false;
        DisAmt = 0;
        DisVal = 0;
        PointRate = 0;
        Total = 0;
        Used = 0;
        Left = 0;
	}
}