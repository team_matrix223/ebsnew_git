﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SubCategory
/// </summary>
public class WSSubCategories
{
	public int SubCategoryId { get; set; }
    public string Title { get; set; }
    public string MainCategoryName { get; set; }

    public WSSubCategories()
	{
        SubCategoryId = 0;
        Title = string.Empty;
        MainCategoryName = string.Empty;
      
	}
}