﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for WSCity
/// </summary>
public class WSCity
{
    public int CityId { get; set; }
    public string Title { get; set; }
	public WSCity()
	{
        CityId = 0;
        Title = "";
	}
}