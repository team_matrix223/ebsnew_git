﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class GetRecipeById : System.Web.Services.WebService {
    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string GetById(int RecipeId)
    {
        List<WSRecipeIngredients> listRec = new List<WSRecipeIngredients>();
        SqlDataReader dr = null;
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@RecipeId", RecipeId); ;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
              "shopping_sp_RecipesIngredientsGetByIdMobile", objParam);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    WSRecipeIngredients objRec = new WSRecipeIngredients()
                    {
                        Unit = dr["Unit"].ToString(),
                        ProductName = dr["ProductName"].ToString(),
                        RecipeId = Convert.ToInt32(dr["RecipeId"].ToString()),
                        Qty =Convert.ToDecimal ( dr["Qty"].ToString()),
                        Url = dr["Url"].ToString(),
                        Title = dr["Title"].ToString(),
                        Process = dr["Process"].ToString(),
                        ShortDesc = dr["ShortDescription"].ToString(),
                    };
                    listRec.Add(objRec);
                }  
               
            }
        }
        catch
        {
            dr.Close();
            dr.Dispose();
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            RecipeDetail = listRec,
        };
        return ser.Serialize(JsonData);


    }   
}
