﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class GetProductListBySubGroupId : System.Web.Services.WebService {
    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string GetProductsByLevelNo(int CategoryId)
    {

        List<WSSubCategories> lstSubCat = new List<WSSubCategories>();
        List<WSProduct> lstProducts = new List<WSProduct>();
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@CategoryId", CategoryId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ProductsGetByCategoryandLevelNo", objParam);


            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    WSProduct objProduct = new WSProduct()
                    {
                        BrandId = Convert.ToInt32(dr["BrandId"].ToString()),
                        BrandName = dr["BrandName"].ToString(),
                        Mrp = Convert.ToDecimal(dr["Mrp"].ToString()),
                        Name = dr["Name"].ToString(),
                        PhotoUrl = dr["PhotoUrl"].ToString(),
                        Price = Convert.ToDecimal(dr["Price"].ToString()),
                        ProductId = Convert.ToInt32(dr["ProductId"].ToString()),
                        Qty = Convert.ToInt32(dr["Qty"].ToString()),
                        ShortName = dr["ShortName"].ToString(),
                        Type = dr["Type"].ToString(),
                        Unit = dr["Unit"].ToString(),
                        VariationId = Convert.ToInt32(dr["VariationId"].ToString()),
                    };
                    lstProducts.Add(objProduct);
                }
            }

            dr.NextResult();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    WSSubCategories objSubCat = new WSSubCategories()
                    {
                        Title = dr["Title"].ToString(),
                        SubCategoryId = Convert.ToInt32(dr["CategoryId"].ToString()),
                        MainCategoryName = dr["MainCategoryName"].ToString(),
                    };
                    lstSubCat.Add(objSubCat);
                }
            }
           
        }
        catch
        {
            dr.Close();
            dr.Dispose();
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            SubCategories = lstSubCat,
            Products = lstProducts
        };
        return ser.Serialize(JsonData);


    }  
      
}
