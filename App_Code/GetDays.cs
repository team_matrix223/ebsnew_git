﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for GetDays
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class GetDays : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string DaysList()
    {


        List<Days> Days = new List<Days>();

        DateTime dd;
        DateTime totaldd;
        dd = Convert.ToDateTime(DateTime.Now.ToString("d"));
        totaldd = dd.AddDays(7);

        while (dd != totaldd)
        {


            Days obj = new Days();
            obj.Day= dd.ToLongDateString();
            //obj.Value= dd.DayOfWeek.ToString();
            obj.Value =Convert.ToDateTime( dd).ToString("yyyy-MM-dd");
          
            if (obj.Value != "Sunday")
            {

                Days.Add(obj);
            }

            dd = dd.AddDays(1);
        }






        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

           Days=Days 
        };
        return ser.Serialize(JsonData);

       
    }



    
}

public class Days
{
    public string Day { get; set; }
    public string Value { get; set; }

}
