﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for GetDeliveryAddressList
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class GetDeliveryAddressList : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string GetDeliveryAddressListByUserId(int UserId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@UserId", UserId);
        DataSet ds = null;
        ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        "Shopping_sp_DeliveryAddressGetByUserId", objParam);

        List<WSDeliveryAddress> lstDelAdde = new List<WSDeliveryAddress>();

        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            WSDeliveryAddress objDelAdd = new WSDeliveryAddress();
            objDelAdd.DeliveryAddressId = Convert.ToInt32(ds.Tables[0].Rows[i]["DeliveryAddressId"]);
            objDelAdd.UserId = Convert.ToInt32(ds.Tables[0].Rows[i]["UserId"]);
            objDelAdd.CityName = ds.Tables[0].Rows[i]["CityName"].ToString();
            objDelAdd.RecipientFirstName = ds.Tables[0].Rows[i]["RecipientFirstName"].ToString();
            objDelAdd.RecipientLastName = ds.Tables[0].Rows[i]["RecipientLastName"].ToString();
            objDelAdd.MobileNo = ds.Tables[0].Rows[i]["MobileNo"].ToString();
            objDelAdd.Telephone = ds.Tables[0].Rows[i]["Telephone"].ToString();
            objDelAdd.CityId =Convert.ToInt32( ds.Tables[0].Rows[i]["CityId"].ToString());
            objDelAdd.Area = ds.Tables[0].Rows[i]["Area"].ToString();
            objDelAdd.Street = ds.Tables[0].Rows[i]["Street"].ToString();
            objDelAdd.Address = ds.Tables[0].Rows[i]["Address"].ToString();
            objDelAdd.PinCode = ds.Tables[0].Rows[i]["PinCode"].ToString();
            objDelAdd.DOC =Convert.ToDateTime( ds.Tables[0].Rows[i]["DOC"].ToString());
            objDelAdd.PinCodeId =Convert.ToInt32( ds.Tables[0].Rows[i]["PinCodeId"].ToString());
           
            lstDelAdde.Add(objDelAdd);

        }

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

           DeliveryAddressList = lstDelAdde,
        };
        return ser.Serialize(JsonData);



    }
    
}
