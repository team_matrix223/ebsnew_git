﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;

/// <summary>
/// Summary description for InsertMobileData
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class InsertMobileData : System.Web.Services.WebService {
    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string InsertData(Int32 UserId, Int32 DeliveryAddressId, string ArrVariatonIds, string ArrQtys, DateTime   DeliveryDate, Int32 DeliverySlotId, string PaymentMode,string Coupon,string Point)
    {


        ArrVariatonIds = ArrVariatonIds.Substring(0, ArrVariatonIds.Length - 1);
        ArrQtys = ArrQtys.Substring(0, ArrQtys.Length - 1);
        Int32 retval = 0;
        DataTable dtt = new DataTable();
        dtt.Clear();
        dtt.Columns.Add("VariationId",typeof(int ));
        dtt.Columns.Add("Qty", typeof(int ));
        string[] arrVariationId = ArrVariatonIds.Split(',');
        string[] arrQty = ArrQtys.Split(',');
        for (int i = 0; i <= arrVariationId.Length - 1; i++)
        {
            if (arrVariationId[i].ToString().Trim() != "")
            {
                dtt.Rows.Add();
                dtt.Rows[dtt.Rows.Count - 1]["VariationId"] = arrVariationId[i].ToString();
                dtt.Rows[dtt.Rows.Count - 1]["Qty"] = arrQty[i].ToString();
            }
        }
        SqlParameter[] objParam = new SqlParameter[9];
        objParam[0] = new SqlParameter("@UserId", UserId);
        objParam[1] = new SqlParameter("@DeliveryAddressId", DeliveryAddressId);
        objParam[2] = new SqlParameter("@ProductAndQtyDetail", dtt );
        objParam[3] = new SqlParameter("@DeliveryDate", DeliveryDate);
        objParam[4] = new SqlParameter("@DeliverySlotId", DeliverySlotId);
        objParam[5] = new SqlParameter("@PaymentMode", PaymentMode);
        objParam[7] = new SqlParameter("@Coupon", Coupon);
        objParam[8] = new SqlParameter("@Point", Point);
        objParam[6] = new SqlParameter("@RetVal", SqlDbType.Int,4);
        objParam[6].Direction = ParameterDirection.ReturnValue;
      
        SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        "shopping_sp_ProductInsertMobile", objParam);
        retval = Convert.ToInt32(objParam[6].Value);

       

            MessageCredentials objMsg = new MessageCredentials();
            objMsg = new OrderBLL().GetMobileNos(Convert.ToInt64(retval.ToString()));
            objMsg.CustomerMessageText = objMsg.CustomerMessageText.Replace("@OrderNo", retval.ToString());
            string cusMsg = objMsg.CustomerMessageText.Replace("@CName", objMsg.CustomerName);
            sendMsg(objMsg.CustomerMobileNo, cusMsg);
            objMsg.AdminMessageText = objMsg.AdminMessageText.Replace("@OrderNo", retval.ToString());
            string AdminSms = objMsg.AdminMessageText.Replace("@CName", objMsg.CustomerName);
            sendMsg(objMsg.AdminMobileNo, AdminSms);
       

       JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            Result = retval.ToString(),
        };

        return ser.Serialize(JsonData);


    }


    public void sendMsg(string MobileNo, string MsgText)
    {
        try
        {
            WebClient Client = new WebClient();
            string baseurl = "http://alerts.sinfini.com/api/web2sms.php?username=peshawari&password=Rohit@123&to=" + MobileNo.ToString() + "&sender=Peshaw&message=" + MsgText.ToString() + "";
            Stream data = Client.OpenRead(baseurl);
            StreamReader reader = new StreamReader(data);
            string s1 = reader.ReadToEnd();
            data.Close();
            reader.Close();
        }
        catch
        {

        }
    }
}
