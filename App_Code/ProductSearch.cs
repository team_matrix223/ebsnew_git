﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
/// <summary>
/// Summary description for ProductSearch
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
  [System.Web.Script.Services.ScriptService]
public class ProductSearch : System.Web.Services.WebService {

   [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]

    public string Productsearch(string Keyword ="")
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@Keyword", Keyword);
        DataSet ds = null;
        ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        "shopping_sp_ProductsKeywordSearch", objParam);



        List<WSProduct> lstProducts = new List<WSProduct>();

        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            WSProduct objProduct = new WSProduct();
            objProduct.ProductId = Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"]);
            objProduct.VariationId = Convert.ToInt32(ds.Tables[0].Rows[i]["VariationId"].ToString());
            objProduct.Name = ds.Tables[0].Rows[i]["Name"].ToString();
            objProduct.ShortName = ds.Tables[0].Rows[i]["ShortName"].ToString();
            objProduct.Qty = Convert.ToInt32(ds.Tables[0].Rows[i]["Qty"].ToString());
            objProduct.Price = Convert.ToDecimal(ds.Tables[0].Rows[i]["Price"].ToString());
            objProduct.Mrp = Convert.ToDecimal(ds.Tables[0].Rows[i]["Mrp"].ToString());
            objProduct.Unit = ds.Tables[0].Rows[i]["Unit"].ToString();
            objProduct.Type = ds.Tables[0].Rows[i]["type"].ToString();
            objProduct.BrandName = ds.Tables[0].Rows[i]["BrandName"].ToString();
            objProduct.PhotoUrl = "ProductImages/" + ds.Tables[0].Rows[i]["PhotoUrl"].ToString();
            objProduct.BrandId = 0;

            //DataView dvSub = ds.Tables[0].DefaultView;
            //dv.RowFilter = "ParentId=" + Convert.ToInt16(dtCategories.Rows[i]["CategoryId"]);
            //DataTable dtSubCategories = dv.ToTable();

            //List<WSSubCategories> lstSubCategories = new List<WSSubCategories>();
            //for (int j = 0; j < dtSubCategories.Rows.Count; j++)
            //{
            //    WSSubCategories objSubCategories = new WSSubCategories();
            //    objSubCategories.SubCategoryId = Convert.ToInt16(dtSubCategories.Rows[j]["CategoryId"]);
            //    objSubCategories.Title = dtSubCategories.Rows[j]["Title"].ToString();
            //    lstSubCategories.Add(objSubCategories);
            //}

            //objCategory.SubCategories = lstSubCategories;


            lstProducts.Add(objProduct);
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            ProductsList = lstProducts,
        };
        return ser.Serialize(JsonData);
    }
    
}
