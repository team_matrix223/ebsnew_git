﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using System.Web.Script.Serialization;
using Microsoft.ApplicationBlocks.Data;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]

public class GetSubCategories : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string SubCategoryList(int CategoryId)
    {


        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@ParentId", CategoryId);
        DataSet ds = null;
        ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        "shopping_sp_ProductCategoriesGetByParentId", objParam);


        
        List<WSCategory> lstCategories = new List<WSCategory>();

        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            WSCategory objCategory = new WSCategory();
            objCategory.CategoryId = Convert.ToInt16(ds.Tables[0].Rows[i]["CategoryId"]);
            objCategory.Title = ds.Tables[0].Rows[i]["Title"].ToString();
            objCategory.Description = ds.Tables[0].Rows[i]["Description"].ToString();
            objCategory.PhotoUrl = "CategoryImages/" + ds.Tables[0].Rows[i]["PhotoUrl"].ToString();


            //DataView dvSub = ds.Tables[0].DefaultView;
            //dv.RowFilter = "ParentId=" + Convert.ToInt16(dtCategories.Rows[i]["CategoryId"]);
            //DataTable dtSubCategories = dv.ToTable();

            //List<WSSubCategories> lstSubCategories = new List<WSSubCategories>();
            //for (int j = 0; j < dtSubCategories.Rows.Count; j++)
            //{
            //    WSSubCategories objSubCategories = new WSSubCategories();
            //    objSubCategories.SubCategoryId = Convert.ToInt16(dtSubCategories.Rows[j]["CategoryId"]);
            //    objSubCategories.Title = dtSubCategories.Rows[j]["Title"].ToString();
            //    lstSubCategories.Add(objSubCategories);
            //}

            //objCategory.SubCategories = lstSubCategories;


            lstCategories.Add(objCategory);
        }









        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            SubCategoryList = lstCategories,
        };
        return ser.Serialize(JsonData);

    
    }
    
}
