﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class CategoriesService : System.Web.Services.WebService
{
    //public MyService_JSONP() : base()
    //{
    //}

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public  string CategoryList(int Id)
    {


        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        "shopping_sp_ProductCategoriesGetAll", objParam);


        DataView dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "ParentId=0";
        DataTable dtCategories = dv.ToTable();

        List<WSCategory> lstCategories = new List<WSCategory>();
 
        for (int i = 0; i < dtCategories.Rows.Count; i++)
        {
            WSCategory objCategory = new WSCategory();
            objCategory.CategoryId = Convert.ToInt16(dtCategories.Rows[i]["CategoryId"]);
            objCategory.Title = dtCategories.Rows[i]["Title"].ToString() ;
            objCategory.Description = dtCategories.Rows[i]["Description"].ToString();
            objCategory.PhotoUrl ="CategoryImages/"+ dtCategories.Rows[i]["PhotoUrl"].ToString();


            DataView dvSub = ds.Tables[0].DefaultView;
            dv.RowFilter = "ParentId=" + Convert.ToInt16(dtCategories.Rows[i]["CategoryId"]);
            DataTable dtSubCategories = dv.ToTable();

            List<WSSubCategories> lstSubCategories = new List<WSSubCategories>();
            for (int j = 0; j < dtSubCategories.Rows.Count; j++)
            {
                WSSubCategories objSubCategories = new WSSubCategories();
                objSubCategories.SubCategoryId = Convert.ToInt16(dtSubCategories.Rows[j]["CategoryId"]);
                objSubCategories.Title = dtSubCategories.Rows[j]["Title"].ToString();
                lstSubCategories.Add(objSubCategories);
            }

            objCategory.SubCategories = lstSubCategories;


            lstCategories.Add(objCategory);
        }

         





 

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            CategoryList = lstCategories,
        };
        return ser.Serialize(JsonData);

        //StringBuilder sb = new StringBuilder();
        //JavaScriptSerializer js = new JavaScriptSerializer();
        //sb.Append("(");
        //sb.Append(js.Serialize(lst));
        //sb.Append(");");

        //return sb.ToString();
        // return "Humara Bajaj";
    }
}
