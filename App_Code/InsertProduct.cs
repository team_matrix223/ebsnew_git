﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class InsertProduct : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string InsertData(string Data)
    {

        Int32 retval = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Data",Data);
        objParam[1] = new SqlParameter("@RetVal", SqlDbType.Int,4);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        
        SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        "shopping_sp_InsertProductsApp", objParam);
        retval = Convert.ToInt16(objParam[1].Value);


        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            Result = retval,
        };
        return ser.Serialize(JsonData);


    }
    
}
