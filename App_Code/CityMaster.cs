﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for CityMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class CityMaster : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string GetCityList()
    {
        SqlParameter[] objParam = new SqlParameter[0];
      
        DataSet ds = null;
        ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        "shopping_sp_CitiesGetAll", objParam);

        List<WSCity> lstCity = new List<WSCity>();

        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            if (Convert.ToBoolean(ds.Tables[0].Rows[i]["IsActive"].ToString()) == true)
            {
                WSCity objCity = new WSCity();
                objCity.CityId = Convert.ToInt32(ds.Tables[0].Rows[i]["CityId"]);
                objCity.Title = ds.Tables[0].Rows[i]["Title"].ToString();
                lstCity.Add(objCity);
            }
           
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            CityList = lstCity,
        };
        return ser.Serialize(JsonData);



    }

    
}
