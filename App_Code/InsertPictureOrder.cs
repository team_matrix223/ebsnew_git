﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using System.Text;
using System.Drawing;
using System.IO;



[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class InsertPictureOrder : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string InsertData(string PhotoUrl, Int32 DeliveryAddress, string DeliveryDay, Int32 DeliverySlot,Int32 UserId)
    {
        string PhotoName = "";
        Int32 retValue = 0;

        SqlParameter[] objParam = new SqlParameter[6];
        try
        {
      
        PhotoName = CommonFunctions.GenerateRandomPassword(4) + DateTime.Now.Millisecond + ".jpg";
        Image convertedImage = Base64ToImage(PhotoUrl);
        convertedImage.Save(Server.MapPath("~/PictureOrder/" + PhotoName + ""), System.Drawing.Imaging.ImageFormat.Jpeg);
      
        objParam[0] = new SqlParameter("@UserId", UserId);
        objParam[1] = new SqlParameter("@PhotoUrl", PhotoName);
        objParam[2] = new SqlParameter("@DeliveryAddressId", DeliveryAddress);
        objParam[3] = new SqlParameter("@DeliveryDate", DeliveryDay);
        objParam[4] = new SqlParameter("@DeliverySlot", DeliverySlot);


        objParam[5] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[5].Direction = ParameterDirection.ReturnValue;


        


            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_PictureOrderInsert", objParam);
            retValue = Convert.ToInt32(objParam[5].Value);




        }
        catch (Exception ex)
        {
            JavaScriptSerializer ser1 = new JavaScriptSerializer();

            var JsonData1 = new
            {

                Status = ex.Message,
            };
            return ser1.Serialize(JsonData1);

        
        }
        finally
        {
            objParam = null;
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            Status = retValue.ToString(),
        };
        return ser.Serialize(JsonData);



    }


    private Image Base64ToImage(string base64String)
    {
        // Convert Base64 String to byte[]
        byte[] imageBytes = Convert.FromBase64String(base64String);
        using (var ms = new MemoryStream(imageBytes, 0,
                                         imageBytes.Length))
        {
            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }
    }
}
