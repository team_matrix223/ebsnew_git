﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

 [System.Web.Script.Services.ScriptService]
public class GetMyGroupRLCR : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string MyGroupsRLCR()
    {


        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@Location", "HomeGuide");
        DataSet ds = null;
        ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        "shopping_sp_GetMyGroupsByLocationNEW", objParam);

        DataView dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "ParentId=0";
        DataTable dtCategories = dv.ToTable();

        List<WSMyGroups> lstGroups = new List<WSMyGroups>();

        for (int i = 0; i < dtCategories.Rows.Count; i++)
        {
            WSMyGroups objGroup = new WSMyGroups()
            {
                Title = dtCategories.Rows[i]["Title"].ToString(),
                HasSubGroup = Convert.ToBoolean(dtCategories.Rows[i]["HasSubGroup"].ToString()),
                Id = Convert.ToInt32(dtCategories.Rows[i]["Id"].ToString()),
                PhotoUrl1 = dtCategories.Rows[i]["PhotoUrl1"].ToString(),
            };

            DataView dvSub = ds.Tables[0].DefaultView;
            dv.RowFilter = "ParentId=" + Convert.ToInt16(dtCategories.Rows[i]["Id"]);
            DataTable dtSubCategories = dv.ToTable();

            List<WSSubCategories> lstSubCategories = new List<WSSubCategories>();
            for (int j = 0; j < dtSubCategories.Rows.Count; j++)
            {
                WSSubCategories objSubCategories = new WSSubCategories();
                objSubCategories.SubCategoryId = Convert.ToInt16(dtSubCategories.Rows[j]["Id"]);
                objSubCategories.Title = dtSubCategories.Rows[j]["Title"].ToString();
                lstSubCategories.Add(objSubCategories);
            }
            objGroup.SubCategories = lstSubCategories;
            lstGroups.Add(objGroup);
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            MyGroups = lstGroups,
        };
        return ser.Serialize(JsonData);


    }  
    
}
