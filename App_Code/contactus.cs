﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class contactus : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string InsertData(string FirstName, string Email, string Phoneno, string Subject, string Enquiry)
    {


        ContactUs objcon = new ContactUs()
        {
            ContactId = 0,
            FirstName = FirstName,
            LastName = "",
            Email = Email,
            Phoneno = Phoneno,
            Subject = Subject,
            Enquiry = Enquiry,
            AdminId = 0,
            Sdate = DateTime.Now,
            ParentId = 0
        };




        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new ContactUsBLL().Insert(objcon);
        var JsonData = new
        {
            list = status,

        };
        return ser.Serialize(JsonData);


    }
    
}
