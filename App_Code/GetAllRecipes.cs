﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class GetAllRecipes : System.Web.Services.WebService {
    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string GetRecipes()
    {
        List<WSRecipe> listrecipe = new List<WSRecipe>();
        SqlDataReader  dr = null;
        SqlParameter[] objParam = new SqlParameter[0];
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
              "shopping_sp_RecipesGetAll", objParam);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (Convert.ToBoolean(dr["IsActive"].ToString()) == true)
                    {
                        WSRecipe objRec = new WSRecipe()
                        {
                            ImageUrl = dr["ImageUrl"].ToString(),
                            Process = dr["Process"].ToString(),
                            RecipeId = Convert.ToInt32(dr["RecipeId"].ToString()),
                            ShortDescription = dr["ShortDescription"].ToString(),
                            Title = dr["Title"].ToString()
                        };
                        listrecipe.Add(objRec);
                    }
                }
            }
        }
        catch
        {
            dr.Close();
            dr.Dispose();
        }
      
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            Recipes = listrecipe,
        };
        return ser.Serialize(JsonData);


    }   
}
