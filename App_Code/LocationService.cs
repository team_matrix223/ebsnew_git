﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class LocationService : System.Web.Services.WebService
{
    //public MyService_JSONP() : base()
    //{
    //}

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string GetLocationData(string Key, string Data)
    {

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            LocationList = "Success"+Key+Data,
        };

        return ser.Serialize(JsonData);


        //return ser.Serialize(JsonData);
        //SqlParameter[] objParam = new SqlParameter[0];
        //DataSet ds = null;
        //ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        //"shopping_sp_CitiesGetAll", objParam);

        //List<WSLocation> lstLocation = new List<WSLocation>();

        // for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        // {
        //     if(Convert.ToBoolean(ds.Tables[0].Rows[i]["IsActive"]))
        //     {
        //            WSLocation objLocation = new WSLocation();
        //            objLocation.LocationId = Convert.ToInt16(ds.Tables[0].Rows[i]["CityId"]);
        //            objLocation.Title = ds.Tables[0].Rows[i]["Title"].ToString();
        //            lstLocation.Add(objLocation);
        //     }
        // }
         

        //JavaScriptSerializer ser = new JavaScriptSerializer();

        //var JsonData = new
        //{

        //    LocationList = lstLocation,
        //};
        //return ser.Serialize(JsonData);

     
    }
}
