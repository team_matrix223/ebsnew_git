﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for GetSettings
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]



public class GetSettings : System.Web.Services.WebService {

    public GetSettings () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string GetDeliverySettings()
    {


     
       List<Settings>  Settings= new  List<Settings>();
       Settings= new SettingsDAL().GetSettings();


        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            Settings = Settings,
        };
        return ser.Serialize(JsonData);


    }
    
}
