﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for WebServiceBLL
/// </summary>
public class WebServiceBLL
{
    public List<WebServices> GetAllCategories()
    {
        List<WebServices> CategoryList = new List<WebServices>();

        SqlDataReader dr = new WebServiceDAL().GetAllCategories();
        try
        {



            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    WebServices objCategory = new WebServices()
                    {
                        CategoryId = Convert.ToInt16(dr["CategoryId"]),
                        Title = dr["Title"].ToString(),
                        Description = dr["Description"].ToString(),
                        ParentId = Convert.ToInt32(dr["ParentId"]),
                        Level = Convert.ToInt32(dr["Level"]),
                       
                    };
                    CategoryList.Add(objCategory);
                }
            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return CategoryList;

    }
}