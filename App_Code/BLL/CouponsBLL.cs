﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data.SqlClient;

/// <summary>
/// Summary description for CouponsBLL
/// </summary>
public class CouponsBLL
{
    public Coupons  GetByCouponNo(string CouponNo)
    {
        Coupons objCpn = new Coupons();

        SqlDataReader dr = null;
        try
        {
            dr = new CouponsDAL().GetByCouponNo(CouponNo);
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    objCpn.Title = dr["Title"].ToString();
                    objCpn.Description = dr["Description"].ToString();

                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return objCpn;

    }
    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new CouponsDAL().GetActiveCoupons();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["CouponId"].ToString(), dr["CouponNo"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }
    public List<Coupons> GetAll()
    {
        List<Coupons> headingList = new List<Coupons>();

        SqlDataReader dr = new CouponsDAL().GetAll();
        try
        {



            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Coupons objCoupons = new Coupons()
                    {
                        CouponId = Convert.ToInt32(dr["CouponId"]),
                        CouponNo = dr["CouponNo"].ToString(),
                        Title = dr["Title"].ToString(),
                        Description = dr["Description"].ToString(),
                        AdminId = Convert.ToInt32(dr["AdminId"]),
                    };
                    headingList.Add(objCoupons);
                }
            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return headingList;

    }

    public int InsertUpdate(Coupons objCoupons)
    {

        return new CouponsDAL().InsertUpdate(objCoupons);
    }
}