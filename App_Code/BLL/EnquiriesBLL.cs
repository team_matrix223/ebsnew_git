﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for EnquiriesBLL
/// </summary>
public class EnquiriesBLL
{
    public List<Enquiries> GetAll()
    {
        List<Enquiries> EnquiryList = new List<Enquiries>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new EnquiriesDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Enquiries objEnquiries = new Enquiries()
                    {
                        EnquiryId = Convert.ToInt32(dr["EnquiryId"]),
                        PersonName = dr["PersonName"].ToString(),
                        EmailId = dr["EmailId"].ToString(),
                        Enquiry = dr["Enquiry"].ToString(),
                        MobileNo = dr["MobileNo"].ToString(),
                        DOC = Convert.ToDateTime(dr["DOC"]),
                       
                     

                    };
                    EnquiryList.Add(objEnquiries);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return EnquiryList;

    }

    public void GetByName(Enquiries objEnquiries)
    {
        List<Enquiries> headingList = new List<Enquiries>();
        SqlDataReader dr = null;

        try
        {
            dr = new EnquiriesDAL().GetByName(objEnquiries);



            if (dr.HasRows)
            {
                dr.Read();



                objEnquiries.EnquiryId = Convert.ToInt32(dr["EnquiryId"]);
                objEnquiries.PersonName = dr["PersonName"].ToString();
                objEnquiries.EmailId = dr["EmailId"].ToString();
                objEnquiries.Enquiry = dr["Enquiry"].ToString();
                objEnquiries.MobileNo = dr["MobileNo"].ToString();
                objEnquiries.DOC = Convert.ToDateTime(dr["DOC"]);
               

            }



        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }


    }

    public Int32 InsertUpdate(Enquiries objEnquiries)
    {
        return new EnquiriesDAL().InsertUpdate(objEnquiries);
    }
}