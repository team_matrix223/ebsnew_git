﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for NewsLetterUsersBLL
/// </summary>
public class NewsLetterUsersBLL
{
    public List<NewsLetterUsers> GetAll()
    {
        List<NewsLetterUsers> objList = new List<NewsLetterUsers>();
        SqlDataReader dr = null;
        try
        {
            dr = new NewsLetterUsersDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    NewsLetterUsers objNewsLetter = new NewsLetterUsers()
                    {
                        NewsLetterUserId = Convert.ToInt32(dr["NewsLetterUserId"]),
                        Name = dr["Name"].ToString(),
                        Email = dr["Email"].ToString(),
                        IsSubscribed = Convert.ToBoolean(dr["IsSubscribed"]),
                    };
                    objList.Add(objNewsLetter);
                }
            }
        }
        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return objList;
    }
    public Int16 Update(NewsLetterUsers objNUser)
    {
        return new NewsLetterUsersDAL().Update(objNUser);
    }
    public Int16 Insert(NewsLetterUsers objNUser)
    {
        return new NewsLetterUsersDAL().Insert(objNUser);
    }
    public Int16 ConfirmVerfication(string emailid)
    {
        return new NewsLetterUsersDAL().ConfirmVerfication(emailid);
    }
}