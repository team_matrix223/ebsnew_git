﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

public class ComboBLL
{
    public class ProductDetail
    {
        public int ProductId { get; set; }
        public string  ProductName { get; set; }
        public decimal  Qty { get; set; }
        public decimal  Price { get; set; }
        public string Units { get; set; }
    }
    public List<Combo > GetAll()
    {
        List<Combo> AlbumList = new List<Combo>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new ComboDAL ().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Combo objAlbum = new Combo()
                    {
                        Title = dr["Title"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        ComboId = Convert.ToInt16(dr["ComboId"]),
                        ComboTypeId = Convert.ToInt16(dr["ComboTypeId"]),
                        Description = dr["Description"].ToString(),
                        AdminId = Convert.ToInt16(dr["AdminId"]),
                        DOC = Convert.ToDateTime(dr["DOC"]),
                        Price = Convert.ToDecimal(dr["price"]),
                        ComboTypeName = dr["ComboTypeName"].ToString(),
                    };
                    AlbumList.Add(objAlbum);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return AlbumList;

    }
    public List<ProductDetail> GetProductDetailByID(Combo ObjCombo)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = new ComboDAL().GetProductDetailByID(ObjCombo);

        List<ProductDetail> lstPDetail = new List<ProductDetail>();
        try
        {

            
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    ProductDetail objProducteDetail = new ProductDetail();
                    objProducteDetail.ProductId = Convert.ToInt32(dr["Productid"]);
                    objProducteDetail.ProductName = dr["ProductName"].ToString();
                    objProducteDetail.Qty = Convert.ToDecimal(dr["Qty"]);
                    objProducteDetail.Price = Convert.ToDecimal(dr["Price"]);
                    objProducteDetail.Units = Convert.ToString(dr["Units"]);


                    lstPDetail.Add(objProducteDetail);



                }

            }


        }



        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return lstPDetail;


    }
    public Int16 InsertUpdate(Combo  objCombo,DataTable dt)
    {

        return new ComboDAL().InsertUpdate(objCombo, dt);
    }
}