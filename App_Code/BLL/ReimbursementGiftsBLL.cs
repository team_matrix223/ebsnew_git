﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Text;

public class ReimbursementGiftsBLL
{
    public Int16 InsertUpdate(ReimbursementGifts objGifts)
    {

        return new ReimbursementGiftsDAL().InsertUpdate(objGifts);
    }

    public string GetGiftsHTML()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new ReimbursementGiftsDAL().GetByIsActive();
            if (dr.HasRows)
            {

                while (dr.Read())
                {


                    strBuilder.Append(string.Format("<div class='col-sm-4'><a style='text-decoration:none' ><div class='fruitbox'><img src='../ReimbursementGiftsImages/{4}' style='height:120px' alt='' class='img-responsive' /><h2 style = 'font-weight:normal;font-size:medium'>{1}</h2><h2>{3} Points</h2><input type='radio'  value='" + dr["GiftId"].ToString() + "'  id='chk_" + dr["GiftId"].ToString() + "' name='Gift'  /></div></a></div>", dr["PhotoUrl"].ToString(), dr["Name"].ToString(), dr["GiftId"].ToString(), dr["Points"].ToString(), dr["PhotoUrl"].ToString()));

                    //strBuilder.Append ("<div class='col-sm-3' ><a style='text-decoration:none' ><div class='fruitbox'><img src='ReimbursementGiftsImages/{3}' style='height:90px' alt='' class='img-responsive' />" +
                    //      "<table><tr><td>" + dr["Name"].ToString() + "</td></tr><tr><td>Points:</td><td>" + dr["Points"].ToString() + "</td></tr>" +

                    //      "<tr><td></td><td align='center'><input type='radio'  value='" + dr["GiftId"].ToString() + "'  id='chk_" + dr["GiftId"].ToString() + "' name='Gift'  /></td></tr></table></div>"

                    //  );


                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }


    public List<ReimbursementGifts> GetAll()
    {
        List<ReimbursementGifts> GiftList = new List<ReimbursementGifts>();
        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = new ReimbursementGiftsDAL().GetAll();
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ReimbursementGifts objproduct = new ReimbursementGifts()
                    {
                        GiftId = Convert.ToInt32(ds.Tables[0].Rows[i]["GiftId"]),
                        Name = ds.Tables[0].Rows[i]["Name"].ToString(),
                        Points = Convert.ToDecimal(ds.Tables[0].Rows[i]["Points"].ToString()),
                        Cash = Convert.ToDecimal( ds.Tables[0].Rows[i]["Cash"].ToString()),
                        PhotoUrl = ds.Tables[0].Rows[i]["PhotoUrl"].ToString(),
                        IsActive = Convert.ToBoolean(ds.Tables[0].Rows[i]["IsActive"]),

                    };
                    GiftList.Add(objproduct);
                }
            }

        }

        finally
        {
            objParam = null;

        }
        return GiftList;
    }
}