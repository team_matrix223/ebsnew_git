﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;

/// <summary>
/// Summary description for CartBLL
/// </summary>
public class UserCartMstBLL
{

    public void GetBySessionId(UserCartMst objUCM)
    {
       
        SqlDataReader dr = null;
        try
        {
            dr = new UserCartMstDAL().GetBySessionId(objUCM.SessionId);
            if (dr.HasRows)
            {
              dr.Read() ;

               objUCM.Telephone = dr["Telephone"].ToString();
              objUCM.Street = dr["Street"].ToString();
              objUCM.SessionId = Convert.ToString(dr["SessionId"]);
              objUCM.RecipientName = dr["RecipientName"].ToString();
              objUCM.PinCode = dr["PinCode"].ToString();
              objUCM.PaymentMode = dr["PaymentMode"].ToString();
              objUCM.MobileNo = dr["MobileNo"].ToString();
              objUCM.Address = dr["Address"].ToString();
              objUCM.Area = dr["Area"].ToString();
              objUCM.City = dr["City"].ToString();
           
              objUCM.DeliveryDate = Convert.ToDateTime(dr["DeliveryDate"]);
              objUCM.DeliverySlot = dr["DeliverySlot"].ToString();
                                              
            }
        }
        finally
        {

            dr.Close();
            dr.Dispose();
        }

    }

   
   
    public void InsertUpdate(UserCartMst objCart)
    {
        new UserCartMstDAL().UserCartInsertUpdate(objCart);
    }
 
}