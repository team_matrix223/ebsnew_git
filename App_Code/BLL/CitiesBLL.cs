﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
/// <summary>
/// Summary description for CitiesBLL
/// </summary>
public class CitiesBLL
{
    public string GetActiveOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        List<Cities> headingList = new List<Cities>();

        SqlDataReader dr = new CitiesDAL().GetAll();

        try
        {

            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    if (Convert.ToBoolean(dr["IsActive"].ToString()) == true)
                    {
                        strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["CityId"].ToString(), dr["Title"].ToString()));
                    }
                }
            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();
        }

        dr.Close();
        dr.Dispose();
        return strBuilder.ToString();

    }
    public List<Cities> GetAll()
    {
        List<Cities> headingList = new List<Cities>();

        SqlDataReader dr = new CitiesDAL().GetAll();
        try
        {
            

           
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Cities objCities = new Cities()
                    {
                        CityId = Convert.ToInt32(dr["CityId"]),
                        Title = dr["Title"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                       };
                    headingList.Add(objCities);
                }
            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return headingList;

    }

    public int InsertUpdate(Cities objCities)
    {

        return new CitiesDAL().InsertUpdate(objCities);
    }
     
}