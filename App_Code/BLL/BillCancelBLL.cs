﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for BillCancelBLL
/// </summary>
public class BillCancelBLL
{
    public Int32 InsertUpdate(BillCancel objBillCancel)
    {
        return new BillCancelDAL().InsertUpdate(objBillCancel);
    }


    public List<BillCancel> CancelBillGetByDate(DateTime FromDate, DateTime ToDate)
    {
        List<BillCancel> BillList = new List<BillCancel>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new BillCancelDAL().CancelBillsGetByDate(FromDate, ToDate);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    BillCancel objbills = new BillCancel()
                    {
                        BillId = Convert.ToInt32(dr["BillId"].ToString()),
                        OrderId = Convert.ToInt32(dr["OrderId"].ToString()),
                        BillDate = Convert.ToDateTime(dr["BillDate"].ToString()),

                        CustomerId = Convert.ToInt32(dr["CustomerId"]),
                        RecipientFirstName = dr["RecipientFirstName"].ToString(),
                        RecipientLastName = Convert.ToString(dr["RecipientLastName"]),
                        RecipientMobile = Convert.ToString(dr["RecipientMobile"]),
                        RecipientPhone = Convert.ToString(dr["RecipientPhone"].ToString()),
                        City =dr["City"].ToString(),
                        Area = Convert.ToString(dr["Area"]),
                        Street = Convert.ToString(dr["Street"]),
                        Address = Convert.ToString(dr["Address"]),
                        Pincode = Convert.ToString(dr["PinCode"]),
                        BillValue = Convert.ToDecimal(dr["BillValue"]),
                        DisPer = Convert.ToDecimal(dr["DisPer"]),
                        DisAmt = Convert.ToDecimal(dr["DisAmt"]),
                        NetAmount = Convert.ToDecimal(dr["NetAmount"]),
                        ServiceTaxPer = Convert.ToDecimal(dr["ServiceTaxPer"]),
                        ServiceTaxAmt = Convert.ToDecimal(dr["ServiceTaxAmt"]),
                        ServiceChargePer = Convert.ToDecimal(dr["ServiceChargePer"]),
                        ServiceChargeAmt = Convert.ToDecimal(dr["ServiceChargeAmt"]),
                        VatPer = Convert.ToDecimal(dr["VatPer"]),
                        VatAmt = Convert.ToDecimal(dr["VatAmt"]),
                        Remarks = dr["Remarks"].ToString(),
                        ExecutiveId = Convert.ToInt32(dr["ExecutiveId"]),
                        IPAddress = dr["IPAddress"].ToString(),

                        Recipient = dr["Recipient"].ToString(),
                        CompleteAddress = dr["CompleteAddress"].ToString(),
                        CustomerName = dr["CustomerName"].ToString(),
                        CancelDate = Convert.ToDateTime(dr["CancelDate"]),
                        CancelRemarks = dr["CancelRemarks"].ToString()
                    };
                    BillList.Add(objbills);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return BillList;

    }
    public void TempInsert(BillCancel objBillCancel)
    {
        new BillCancelDAL().InsertTemp(objBillCancel);
    }

}