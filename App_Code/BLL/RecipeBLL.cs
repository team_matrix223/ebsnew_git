﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Text;

/// <summary>
/// Summary description for AdminBLL
/// </summary>
/// 

public class ProductList
{
    public int ProductId { get; set; }
    public string Title { get; set; }

}


public class RecipeBLL
{
    public List<RecipeIngredients> GetRecipeIngredients(int RecipeId)
    {
        List<RecipeIngredients> list = new List<RecipeIngredients>();
        SqlDataReader dr = new RecipeDAL().GetRecipeIngredients(RecipeId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    RecipeIngredients objList = new RecipeIngredients()
                    {
                        ProductId = Convert.ToInt32(dr["ProductId"]),
                        ProductName = dr["ProductName"].ToString(),
                        Qty = Convert.ToDecimal(dr["Qty"]),
                        RecipeId = Convert.ToInt32(dr["RecipeId"]),
                        Unit = dr["Unit"].ToString(),
                        Url=dr["Url"].ToString()

                    };
                    list.Add(objList);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return list;

    }

    public string GetRecipeIngredientHtml(int RecipeId)
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlDataReader dr = new RecipeDAL().GetRecipeIngredients(RecipeId);


        try
        {

            if (dr.HasRows)
            {
                int counter = 1;
                while (dr.Read())
                {
                    

                    strBuilder.Append(
                        string.Format("<tr><td style='width:25px'>" + counter + "</td><td  style='width:150px'><a href='{1}'>{0}</a></td><td  style='width:100px'>{2}</td></tr>", dr["ProductName"], dr["Url"], dr["Qty"] + " " + dr["Unit"].ToString())
                        

                        );
                    counter = counter + 1;
                  
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return strBuilder.ToString();

    }

    public string GetById(Recipe objRecipe)
    {
        SqlDataReader dr = new RecipeDAL().GetById(objRecipe.RecipeId);
        StringBuilder strBuilder = new StringBuilder();
        try
        {

            if (dr.HasRows)
            {
                dr.Read();


                objRecipe.RecipeId = Convert.ToInt16(dr["RecipeId"]);
                objRecipe.Title = dr["Title"].ToString();
                objRecipe.ShortDescription = Convert.ToString(dr["ShortDescription"]);
                objRecipe.Process = Convert.ToString(dr["Process"]);
                objRecipe.DOC = Convert.ToDateTime(dr["DOC"]);
                objRecipe.IsActive = Convert.ToBoolean(dr["IsActive"]);
                objRecipe.ImageUrl = dr["ImageUrl"].ToString();



            }





        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return strBuilder.ToString();
    }


    public List<ProductList> GetProducts()
    {
        List<ProductList> productList = new List<ProductList>();
        SqlDataReader dr = new RecipeDAL().GetProducts();


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    ProductList objList = new ProductList()
                    {
                        ProductId = Convert.ToInt16(dr["ProductId"]),
                        Title = dr["Title"].ToString(),


                    };
                    productList.Add(objList);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return productList;

    }



    public List<Recipe> GetAll()
    {
        List<Recipe> recipeList = new List<Recipe>();
        SqlDataReader dr = new RecipeDAL().GetAll();


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    Recipe objRecipe = new Recipe()
                    {
                        RecipeId = Convert.ToInt16(dr["RecipeId"]),
                        Title = dr["Title"].ToString(),
                        ShortDescription = Convert.ToString(dr["ShortDescription"]),
                        Process = Convert.ToString(dr["Process"]),
                        DOC = Convert.ToDateTime(dr["DOC"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),


                    };
                    recipeList.Add(objRecipe);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return recipeList;

    }


    public string GetRecipiesHtml()
    {

        SqlDataReader dr = new RecipeDAL().GetAll();
        StringBuilder strBuilder = new StringBuilder();

        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (Convert.ToBoolean(dr["IsActive"]))
                    {

                        strBuilder.Append(

                            string.Format(
" <div class='col-sm-3'><a target='_blank' href='recipe.aspx?id={0}' style='text-decoration:none'>" +
"<div class='fruitbox'><img class='img-responsive' alt='' style='height:120px' src='RecipeImages/{2}'>" +
"<h2>{1}</h2><div class='explorebutton'><h3>Explore</h3></div></div></a></div>", dr["RecipeId"], dr["Title"], dr["ImageUrl"]

                            )
                            );



                    }
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return strBuilder.ToString();

    }



    public Int16 InsertRecipeIngredients(int RecipeId, DataTable dt)
    {
        return new RecipeDAL().InsertRecipeIngredients(RecipeId, dt);
    }


    public Int16 InsertUpdate(Recipe objRecipe)
    {

        return new RecipeDAL().InsertUpdate(objRecipe);
    }
}