﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for AdminBLL
/// </summary>
public class AdminBLL
{
    public string ChangePassword(Admin objAdmin)
    {
     return   new AdminDAL().ChangePassword(objAdmin);
    }

    public Int32  CheckOnChangePassword(Admin objAdmin)
    {
        return new AdminDAL().CheckOnChangePassword(objAdmin);
    }


    public void LoginCheck(Admin objAdmin)
    {

        SqlDataReader dr = null;

        try
        {
            dr = new AdminDAL().LoginCheck(objAdmin);



            if (dr.HasRows)
            {
                dr.Read();



                objAdmin.AdminId = Convert.ToInt32(dr["AdminId"].ToString());
                objAdmin.AdminName = dr["AdminName"].ToString();
                objAdmin.Roles = dr["Roles"].ToString();
                objAdmin.IsActive = Convert.ToBoolean(dr["IsActive"]);
            }
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }


    }



    public List<Admin> GetAll()
    {
        List<Admin> bomList = new List<Admin>();
        SqlDataReader dr = new AdminDAL().GetAll();


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    Admin objissue = new Admin()
                    {
                        AdminId=Convert.ToInt16(dr["AdminId"]),
                        AdminName = dr["AdminName"].ToString(),
                        Password = Convert.ToString(dr["Password"]),
                        Name = Convert.ToString(dr["Name"]),
                        Mobile = Convert.ToString(dr["Mobile"]),
                        Roles = Convert.ToString(dr["Roles"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),


                    };
                    bomList.Add(objissue);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return bomList;

    }


    public Int16 InsertUpdate(Admin objAdmin)
    {

        return new AdminDAL().InsertUpdate(objAdmin);
    }
}