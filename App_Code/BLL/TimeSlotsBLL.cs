﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for TimeSlotsBLL
/// </summary>
public class TimeSlotsBLL
{
    public int InsertUpdate(TimeSlots objTimeSlots)
    {

        return new TimeSlotsDAL().InsertUpdate(objTimeSlots);
    }



    public string GetSlotsoption()
    {

        StringBuilder str = new StringBuilder();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new TimeSlotsDAL().GetSlots();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    str.Append(string.Format("<option value={0}>{1}</option>", Convert.ToInt32(dr["Id"].ToString()), dr["SlotEndTime"].ToString()));
                }

            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return str.ToString();

    }

    public string GetAllSlotsOptions()
    {
        StringBuilder str = new StringBuilder();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new TimeSlotsDAL().GetAllSlots();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    str.Append(string.Format("<option value={0}>{1}</option>", Convert.ToInt32(dr["Id"].ToString()), dr["SlotEndTime"].ToString()));
                }

            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return str.ToString();

    }


    public string GetTimeSlotsHTML()
    {
        StringBuilder strb = new StringBuilder();
        SqlDataReader dr = new TimeSlotsDAL().GetAllSlots();
        try
        {



            if (dr.HasRows)
            {

                while (dr.Read())
                {

                    strb.Append("<tr><td>" + dr["SlotEndTime"].ToString() + "</td></tr>");

                }
                strb.Append("<tr><td colspan='100%' align='center' style='color:red'>No Delivery On Sunday</td></tr>");
                strb.Append("</div></table>");
            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return strb.ToString();

    }
    public List<TimeSlots> GetAll()
    {
        List<TimeSlots> headingList = new List<TimeSlots>();

        SqlDataReader dr = new TimeSlotsDAL().GetAll();
        try
        {



            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    TimeSlots objTimeSlots = new TimeSlots()
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        SlotStartTime = dr["SlotStartTime"].ToString(),
                        SlotEndTime = dr["SlotEndTime"].ToString(),
                        StartTime = dr["StartTime"].ToString(),
                        EndTime = dr["EndTime"].ToString(),
                        AdminId = Convert.ToInt32(dr["AdminId"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                    };
                    headingList.Add(objTimeSlots);
                }
            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return headingList;

    }


    public List<TimeSlots> GetSlots()
    {
        List<TimeSlots> headingList = new List<TimeSlots>();

        SqlDataReader dr = new TimeSlotsDAL().GetSlots();
        try
        {



            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    TimeSlots objTimeSlots = new TimeSlots()
                    {
                        Id = Convert.ToInt32(dr["Id"].ToString()),
                        SlotEndTime = dr["SlotEndTime"].ToString(),

                    };
                    headingList.Add(objTimeSlots);
                }
            }
           

        }

        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return headingList;

    }



    public List<TimeSlots> GetAllSlots()
    {
        List<TimeSlots> SlotsList = new List<TimeSlots>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new TimeSlotsDAL().GetAllSlots();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    TimeSlots objTimeSlot = new TimeSlots()
                    {
                        Id = Convert.ToInt32(dr["Id"].ToString()),
                      SlotEndTime = dr["SlotEndTime"].ToString(),
                    };
                    SlotsList.Add(objTimeSlot);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return SlotsList;

    }
}