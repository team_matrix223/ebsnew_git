﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;


public class UserComboBLL
{
    
    public string GetUserComboHTML(Int64  UserId, Calc objCalc)
    {

        DataSet ds = new UserComboDAL().GetHtmlComboByUserId(UserId);
        StringBuilder str = new StringBuilder();
        if (ds.Tables[0].Rows.Count > 0)
        {
            objCalc.TotalItems = ds.Tables[0].Rows.Count;
        }



        int m_sTotal = 0;
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            m_sTotal = m_sTotal + (Convert.ToInt32(ds.Tables[0].Rows[i]["Price"]) * Convert.ToInt16(ds.Tables[0].Rows[i]["Qty"]));

            str.Append("<tr style='border-bottom:dotted 1px silver' id='tp_" + ds.Tables[0].Rows[i]["ProductId"].ToString() + ds.Tables[0].Rows[i]["VariationId"] + "'><td><img src='../ProductImages/T_" + ds.Tables[0].Rows[i]["PhotoUrl"] + "' style='width:45px;height:46px;margin:5px' alt='' /></td><td><b>" + ds.Tables[0].Rows[i]["Name"] + "</b><br/>" + ds.Tables[0].Rows[i]["ProductDesc"] + "</td><td>Rs " + ds.Tables[0].Rows[i]["Price"] + "</td>");

            str.Append("<td><div class='button'>" +
 "<div style='width:61px;float:left;border:solid 1px silver;border-radius:5px;background:#d2cfcf;'>" +
 "<div  style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' v='" + ds.Tables[0].Rows[i]["VariationId"] + "' name='cartminus' id='cm_" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "'>-</div>" +
 "<div style='width:22px;height:20px;float:left;background:white;padding:0px 0 0 3px;'  id='cq_" + ds.Tables[0].Rows[i]["ProductId"].ToString() + ds.Tables[0].Rows[i]["VariationId"] + "'>" + ds.Tables[0].Rows[i]["Qty"] + "</div>" +
 "<div style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' v='" + ds.Tables[0].Rows[i]["VariationId"] + "'   name='cartadd' id='cp_" + ds.Tables[0].Rows[i]["ProductId"].ToString() + "'>+</div>" +
 "</div></td>");

            str.Append("<td>Rs. <span id='span" + ds.Tables[0].Rows[i]["ProductId"].ToString() + ds.Tables[0].Rows[i]["VariationId"] + "' >" + Convert.ToInt64(ds.Tables[0].Rows[i]["Qty"]) * Convert.ToInt64(ds.Tables[0].Rows[i]["Price"]) + "</span></td></tr>");



        }

        objCalc.SubTotal = m_sTotal;

        return str.ToString();
    }
    public Int16 DeleteTemp(Int64 UserId)
    {

        return new UserComboDAL().DeleteTemp(UserId );
    }
    public Int16 DeleteCombo(Int64 ComboId)
    {
        return new UserComboDAL().DeleteCombo(ComboId);
    }
    public Int16 InsertUpdate(UserCombo objCombo)
    {

        return new UserComboDAL().InsertUpdate(objCombo);
    }
    public Int16 InsertTemp(UserComboDetail objCombo)
    {

        return new UserComboDAL().InsertTemp(objCombo);
    }

    public List<UserCombo> GetAllByUserId(Int32  UserId)
    {
        int SessionId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId]);
        List<UserCombo> ComboList = new List<UserCombo>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new UserComboDAL().GetAllByUserId(UserId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    UserCombo objCombo = new UserCombo()
                    {
                        Title = dr["Title"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        ComboId = Convert.ToInt16(dr["ComboId"]),
                        DOC = Convert.ToDateTime(dr["DOC"]),
                        PhotoUrl = dr["PhotoUrl"].ToString(),
                    };
                    ComboList.Add(objCombo);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return ComboList;

    }
    public void   GetProductDetailByID(UserComboDetail ObjCombo)
    {
  new UserComboDAL().GetProductDetailByID(ObjCombo);
     }
    public string UserComboIncrDecr(UserComboDetail objCombo, string Mode, Calc objCalc, int Type = 1)
    {

        StringBuilder str = new StringBuilder();
        SqlDataReader dr = new UserComboDAL().UserComboIncrDecr(objCombo, Mode);
        try
        {
            if (dr.HasRows)
            {
                dr.Read();

                objCombo.ProductId = Convert.ToInt32(dr["ProductId"]);
                objCombo.Qty = Convert.ToInt32(dr["CurrentQty"]);

                objCalc.DeliveryCharges = Convert.ToInt32(dr["DeliveryCharges"]);
                objCalc.Price = Convert.ToInt32(dr["CurrentPrice"]);
                objCalc.Qty = Convert.ToInt32(dr["CurrentQty"]);
                objCalc.DeliveryCharges = Convert.ToInt32(dr["DeliveryCharges"]);
                objCalc.ProductSubTotal = Convert.ToInt32(dr["CurrentPrice"]) * Convert.ToInt32(dr["CurrentQty"]);
                objCalc.SubTotal = Convert.ToInt32(dr["SubTotal"]);
                objCalc.VariationId = dr["VariationId"].ToString();

                if (Type == 1)
                {
                    string Unit1 = dr["Unit1"].ToString();
                    string Unit2 = dr["Unit2"].ToString();
                    string Unit3 = dr["Unit3"].ToString();

                    string ProductId = dr["ProductId"].ToString();
                    int Variation1 = Convert.ToInt16(dr["Variation1"]);
                    int Variation2 = Convert.ToInt16(dr["Variation2"]);
                    int Variation3 = Convert.ToInt16(dr["Variation3"]);
                    string currentVariationId = dr["VariationId"].ToString();

                    string s1 = currentVariationId == "a" ? "selected=selected" : "";
                    string s2 = currentVariationId == "b" ? "selected=selected" : "";
                    string s3 = currentVariationId == "c" ? "selected=selected" : "";

                    string bgColor = "white";
                    if (Variation1 > 0)
                    {
                        bgColor = "#D8EDC0";
                    }



                    str.Append("<img  src='ProductImages/T_" + dr["PhotoUrl"] + "'  class='boximage' alt=''>");
                    str.Append("<span><b>" + dr["Name"] + "</b></span><div align='center'>");


                    str.Append("<select name='variation' id='d_" + dr["ProductId"].ToString() + "' style='margin-top:5px; padding:3px;width:112px'>");
                    decimal mrp1 = Convert.ToDecimal(dr["Mrp1"]);
                    decimal price1 = Convert.ToDecimal(dr["Price1"]);

                    if (Unit1 != "")
                    {
                        decimal qty1 = Convert.ToDecimal(dr["Qty1"]);
                        string desc1 = Convert.ToString(dr["Desc1"]);

                        str.Append("<option " + s1 + " value='a' v='" + Variation1 + "' m='" + mrp1 + "' p='" + price1 + "' style='font-size:12px;'>" + qty1 + " " + Unit1 + " " + desc1 + "- Rs. " + price1 + " </option>");
                    }


                    if (Unit2 != "")
                    {
                        decimal price = Convert.ToDecimal(dr["Price2"]);
                        decimal qty2 = Convert.ToDecimal(dr["Qty2"]);
                        string desc2 = Convert.ToString(dr["Desc2"]);

                        decimal mrp = Convert.ToDecimal(dr["Mrp2"]);
                        str.Append("<option " + s2 + "  value='b' v='" + Variation2 + "'  m='" + mrp + "' p='" + price + "'  style='font-size:12px;'>" + qty2 + " " + Unit2 + " " + desc2 + "- Rs. " + price + " </option>");

                    }



                    if (Unit3 != "")
                    {
                        decimal qty3 = Convert.ToDecimal(dr["Qty3"]);
                        string desc3 = Convert.ToString(dr["Desc3"]);

                        decimal mrp = Convert.ToDecimal(dr["Mrp3"]);
                        decimal price = Convert.ToDecimal(dr["Price3"]);

                        str.Append("<option " + s3 + "  value='c' v='" + Variation3 + "'  m='" + mrp + "' p='" + price + "'  style='font-size:12px;'> " + qty3 + " " + Unit3 + " " + desc3 + "- Rs. " + price + " </option>");

                    }


                    str.Append("</select>");

                    str.Append("</div><table style='width:100%;margin-left:30px'><tr><td><h2 style='text-decoration:line-through' id='m_" + dr["ProductId"].ToString() + "'>" + mrp1 + "</h2></td><td><h2 id='p_" + dr["ProductId"].ToString() + "'>" + price1 + "</h2></td></tr></table><div id='dvChangable" + dr["ProductId"].ToString() + "' style='margin:0px;padding:0px;float:left;'>");

                    if (objCombo.Qty == 0)
                    {

                        str.Append("<button name='btnAddToCart' type='button' id='a_" + dr["ProductId"].ToString() + "' style='padding:2px; margin-left:10px; margin-top:10px;' class='btn btn-success'><img src='images/addimg.png' alt='' style='padding-right:5px;' >Add</button>" +
                        "<div class='qty'> <div align='center'><div style='margin-left:10px;background-color:#f4f2f2;width:68px;border:solid 1px silver;border-radius:4px'>" +
                        "Qty<select name='qty'  id='q_" + dr["ProductId"].ToString() + "' style='margin:1px 0px 1px 0px; padding:3px;border:0px'>" +
                        "<option value='1' style='font-size:12px;'>1</option>" +
                        "<option value='2' style='font-size:12px;'>2</option>" +
                        "<option value='3' style='font-size:12px;'>3</option><option value='4' style='font-size:12px;'>4</option><option value='5' style='font-size:12px;'>5</option></select></div></div></div>");
                    }
                    else
                    {
                        str.Append(

                            "<div style='margin:15px 0px 0px 20px'><div style='width:115px;float:left;border:solid 1px silver;border-radius:5px;background:#d2cfcf;'>" +
             "<div name='decr' style='width:15px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' id='decr_" + dr["ProductId"].ToString() + "' >-</div><div style='width:80px;text-align:center;float:left;background:white;padding:0px 0 0 3px;' id='cqty_" + dr["ProductId"].ToString() + "'>" + objCombo.Qty + "</div>" +
             "<div name='incr' style='width:10px;float:left;background:#d2cfcf;padding:0px 0 0 3px;cursor:pointer' id='incr_" + dr["ProductId"].ToString() + "' >+</div></div></div>"

                            );
                    }


                    str.Append("<br/><br/></div>");


                }



            }
            else
            {
                objCombo.Qty = Convert.ToInt32(0);

            }
            return str.ToString();
        }
        finally
        {

            dr.Close();
            dr.Dispose();
        }

    }
}