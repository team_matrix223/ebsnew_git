﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class SubCategoryBLL
{


    public List<SubCategory > GetByCategoryId(int CategoryId)
    {
        List<SubCategory > AlbumList = new List<SubCategory >();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new SubCategoryDAL ().GetByCategoryId(CategoryId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    SubCategory objAlbum = new SubCategory()
                    {
                        Title = dr["Title"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        CategoryId = Convert.ToInt16(dr["CategoryId"]),
                        SubCategoryId = Convert.ToInt16(dr["SubCategoryId"]),
                        Description = dr["Description"].ToString(),
                        AdminId = Convert.ToInt16(dr["AdminId"]),
                        DOC = Convert.ToDateTime(dr["DOC"]),
                        CategoryName = dr["CategoryName"].ToString(),


                    };
                    AlbumList.Add(objAlbum);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return AlbumList;

    }



    public List<SubCategory > GetAll()
    {
        List<SubCategory> AlbumList = new List<SubCategory>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new SubCategoryDAL ().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    SubCategory objAlbum = new SubCategory()
                    {
                        Title = dr["Title"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        CategoryId = Convert.ToInt16(dr["CategoryId"]),
                        SubCategoryId = Convert.ToInt16(dr["SubCategoryId"]),
                        Description = dr["Description"].ToString(),
                        AdminId = Convert.ToInt16(dr["AdminId"]),
                        DOC = Convert.ToDateTime(dr["DOC"]),
                        CategoryName = dr["CategoryName"].ToString(),



                    };
                    AlbumList.Add(objAlbum);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return AlbumList;

    }







    public Int16 InsertUpdate(SubCategory  objAlbum)
    {

        return new SubCategoryDAL().InsertUpdate(objAlbum);
    }
}