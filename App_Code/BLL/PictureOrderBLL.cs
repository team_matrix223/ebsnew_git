﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for PictureOrderBLL
/// </summary>
public class PictureOrderBLL
{
    public Int32 InsertPictureOrder(PictureOrders objPictureOrder)
    {

        return new PictureOrderDAL().InsertPictureOrder(objPictureOrder);
    }


    public List<PictureOrders> GetByDate(DateTime FromDate, DateTime ToDate)
    {
        List<PictureOrders> OrderList = new List<PictureOrders>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new PictureOrderDAL().GetByDate(FromDate, ToDate);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    PictureOrders objPictureList = new PictureOrders()
                    {
                        PictureOrderId = Convert.ToInt32(dr["PictureOrderId"].ToString()),

                        UserId = Convert.ToInt32(dr["UserId"].ToString()),
                        PhotoUrl = dr["PhotoUrl"].ToString(),
                        UserName = dr["UserName"].ToString(),
                        Recipient = Convert.ToString(dr["Recipient"]),
                        CompleteAddress = Convert.ToString(dr["CompleteAddress"]),
                        DeliveryDate = Convert.ToDateTime(dr["DeliveryDate"].ToString()),
                        DeliveryAddressId = Convert.ToInt32(dr["DeliveryAddressId"].ToString()),
                        DeliverySlot = Convert.ToInt32(dr["DeliverySlot"].ToString()),
                        Slot = Convert.ToString(dr["Slot"]),
                        CreatedDate = Convert.ToDateTime(dr["CreatedDate"]),
                        Status = Convert.ToString(dr["CurrentStatus"]),
                    };
                    OrderList.Add(objPictureList);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return OrderList;

    }

    public Int32 InsertUpdate(Order objOrder, DataTable dt)
    {
        return new PictureOrderDAL().InsertUpdate(objOrder, dt);
    }

}