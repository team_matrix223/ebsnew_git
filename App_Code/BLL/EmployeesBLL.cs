﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for EmployeesBLL
/// </summary>
public class EmployeesBLL
{
    public Int32 InsertUpdate(Employees objUsers)
    {

        return new EmployeesDAL().InsertUpdate(objUsers);
    }

    public List<Employees> GetAll()
    {
        return new EmployeesDAL().GetAll();

    }

    public Int32 InsertEmpAssigned(Empassigned objEmp,DataTable dt)
    {

        return new EmployeesDAL().InsertEmpAssigned(objEmp,dt);
    }

    public Int32 InsertReceivedPayment(Empassigned objEmp, DataTable dt)
    {

        return new EmployeesDAL().InsertReceivedPayment(objEmp, dt);
    }

    public Int32 InsertDeallocation(Empassigned objEmp, DataTable dt)
    {

        return new EmployeesDAL().InsertDealloation(objEmp, dt);
    }


    public List<Bills> GetByDate(DateTime FromDate, DateTime ToDate)
    {
        List<Bills> BillList = new List<Bills>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];
        try
        {
            dr = new EmployeesDAL().GetByDate(FromDate, ToDate);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Bills objbills = new Bills()
                    {
                        BillId = Convert.ToInt32(dr["BillId"].ToString()),
                        OrderId = Convert.ToInt32(dr["OrderId"].ToString()),
                        BillDate = Convert.ToDateTime(dr["BillDate"].ToString()),

                        CustomerId = Convert.ToInt32(dr["CustomerId"]),
                        RecipientFirstName = dr["RecipientFirstName"].ToString(),
                        RecipientLastName = Convert.ToString(dr["RecipientLastName"]),
                        RecipientMobile = Convert.ToString(dr["RecipientMobile"]),
                        RecipientPhone = Convert.ToString(dr["RecipientPhone"].ToString()),
                        City = dr["City"].ToString(),
                        Area = Convert.ToString(dr["Area"]),
                        Street = Convert.ToString(dr["Street"]),
                        Address = Convert.ToString(dr["Address"]),
                        Pincode = Convert.ToString(dr["PinCode"]),
                        BillValue = Convert.ToDecimal(dr["BillValue"]),
                        DisPer = Convert.ToDecimal(dr["DisPer"]),
                        DisAmt = Convert.ToDecimal(dr["DisAmt"]),
                        NetAmount = Convert.ToDecimal(dr["NetAmount"]),
                        ServiceTaxPer = Convert.ToDecimal(dr["ServiceTaxPer"]),
                        ServiceTaxAmt = Convert.ToDecimal(dr["ServiceTaxAmt"]),
                        ServiceChargePer = Convert.ToDecimal(dr["ServiceChargePer"]),
                        ServiceChargeAmt = Convert.ToDecimal(dr["ServiceChargeAmt"]),
                        VatPer = Convert.ToDecimal(dr["VatPer"]),
                        VatAmt = Convert.ToDecimal(dr["VatAmt"]),
                        Remarks = dr["Remarks"].ToString(),
                        ExecutiveId = Convert.ToInt32(dr["ExecutiveId"]),
                        IPAddress = dr["IPAddress"].ToString(),

                        Recipient = dr["Recipient"].ToString(),
                        CompleteAddress = dr["CompleteAddress"].ToString(),
                        CustomerName = dr["CustomerName"].ToString(),
                        DeliveryCharges = Convert.ToDecimal(dr["DeliveryCharges"]),
                    };
                    BillList.Add(objbills);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return BillList;

    }



    public List<Bills> GetByDateallocatedOrders(int EmpId)
    {
        List<Bills> BillList = new List<Bills>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];
        try
        {
            dr = new EmployeesDAL().GetByDateAllocatedOrders(EmpId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Bills objbills = new Bills()
                    {
                        BillId = Convert.ToInt32(dr["BillId"].ToString()),
                        OrderId = Convert.ToInt32(dr["OrderId"].ToString()),
                        BillDate = Convert.ToDateTime(dr["BillDate"].ToString()),

                        CustomerId = Convert.ToInt32(dr["CustomerId"]),
                        RecipientFirstName = dr["RecipientFirstName"].ToString(),
                        RecipientLastName = Convert.ToString(dr["RecipientLastName"]),
                        RecipientMobile = Convert.ToString(dr["RecipientMobile"]),
                        RecipientPhone = Convert.ToString(dr["RecipientPhone"].ToString()),
                        City = dr["City"].ToString(),
                        Area = Convert.ToString(dr["Area"]),
                        Street = Convert.ToString(dr["Street"]),
                        Address = Convert.ToString(dr["Address"]),
                        Pincode = Convert.ToString(dr["PinCode"]),
                        BillValue = Convert.ToDecimal(dr["BillValue"]),
                        DisPer = Convert.ToDecimal(dr["DisPer"]),
                        DisAmt = Convert.ToDecimal(dr["DisAmt"]),
                        NetAmount = Convert.ToDecimal(dr["NetAmount"]),
                        ServiceTaxPer = Convert.ToDecimal(dr["ServiceTaxPer"]),
                        ServiceTaxAmt = Convert.ToDecimal(dr["ServiceTaxAmt"]),
                        ServiceChargePer = Convert.ToDecimal(dr["ServiceChargePer"]),
                        ServiceChargeAmt = Convert.ToDecimal(dr["ServiceChargeAmt"]),
                        VatPer = Convert.ToDecimal(dr["VatPer"]),
                        VatAmt = Convert.ToDecimal(dr["VatAmt"]),
                        Remarks = dr["Remarks"].ToString(),
                        ExecutiveId = Convert.ToInt32(dr["ExecutiveId"]),
                        IPAddress = dr["IPAddress"].ToString(),

                        Recipient = dr["Recipient"].ToString(),
                        CompleteAddress = dr["CompleteAddress"].ToString(),
                        CustomerName = dr["CustomerName"].ToString(),
                        DeliveryCharges = Convert.ToDecimal(dr["DeliveryCharges"]),
                        PaymentStatus = dr["PaymentStatus"].ToString(),
                       
                    };
                    BillList.Add(objbills);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return BillList;

    }


    public string GetByEmpOrders(Bills objEmployees, out int cntr)
    {
        StringBuilder strBuilder = new StringBuilder();
        List<Bills> PurchaseDetailList = new List<Bills>();
        SqlDataReader dr = new EmployeesDAL().GetByEmpOrders(Convert.ToInt32(objEmployees.EmpID));
        StringBuilder strProducts = new StringBuilder();
        int counterId = 0;

        try
        {

            if (dr.HasRows)
            {

                while (dr.Read())
                {
                    counterId += 1;


                  

                    strBuilder.Append("<tr><td><label id='lblSerialNo" + counterId + "'  class='form-control input-small' name='lblSerialNo' counter='" + counterId + "'>" + counterId + "</label></td><td><label id='txtbillno" + counterId + "'class='form-control input-small' name='txtproductname' counter='" + counterId + "'   style='width:70px'>" + dr["BillId"].ToString() + "</label></td>" +
              "<td><label id='txtcustomername" + counterId + "'class='form-control input-small' name='txtproductname' counter='" + counterId + "'   style='width:70px'>" + dr["Customername"].ToString() + "</label></td>" +
             "<td><label id='txtamount" + counterId + "'class='form-control input-small' name='txtproductname' counter='" + counterId + "'   style='width:70px'>" + dr["NetAmount"].ToString() + "</label></td>" +
             "<td><input id='txtreceived" + counterId + "'  type='text' ' counter='" + counterId + "' class='form-control input-small' name='txtreceivedamt'   style='width:100px' value='" + dr["ReceivedAmt"].ToString() + "'/></td></tr>");






                }
                cntr = counterId;
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        cntr = counterId;
        return strBuilder.ToString();

    }





    public Int32 UpdateBillMaster(DataTable dt)
    {

        return new EmployeesDAL().UpdateBillMaster(dt);
    }
}