﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

/// <summary>
/// Summary description for ProductsBLL
/// </summary>
public class SlotsBLL
{


    public void GetByDayName(Slots objSlots)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new SlotsDAL().GetByDayName(objSlots.DayName);
            if (dr.HasRows)
            {
                dr.Read();


                objSlots.DayName = dr["DayName"].ToString();
                objSlots.Slot1Start = dr["Slot1Start"].ToString();
                objSlots.Slot1End = dr["Slot1End"].ToString();
                objSlots.Slot2Start = dr["Slot2Start"].ToString();
                objSlots.Slot2End = dr["Slot2End"].ToString();
                objSlots.Slot3Start = dr["Slot3Start"].ToString();
                objSlots.Slot3End = dr["Slot3End"].ToString();
                objSlots.Slot4Start = dr["Slot4Start"].ToString();
                objSlots.Slot4End = dr["Slot4End"].ToString();



            }
        }
        finally
        {

            dr.Close();
            dr.Dispose();
        }

    }

}