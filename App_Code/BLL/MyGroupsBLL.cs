﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for MyGroupsBLL
/// </summary>
public class MyGroupsBLL
{

    public string GetGroupHTML()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new MyGroupsDAL().GetAll();
            if (dr.HasRows)
            {
 
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<div class='col-sm-3'><a style='text-decoration:none' href='groups.aspx?id={2}'><div class='fruitbox'><img src='GroupImages/{3}' style='height:120px' alt='' class='img-responsive' /><h2>{1}</h2><div class='explorebutton'><h3>Explore</h3></div></div></a></div>", dr["PhotoUrl1"].ToString(), dr["Title"].ToString(), dr["Id"].ToString(), dr["PhotoUrl1"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }


    public string GetGroupHTMLByLocation(string Location)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new MyGroupsDAL().GetByLocation(Location);
            if (dr.HasRows)
            {

                while (dr.Read())
                {
                    if (Convert.ToBoolean(dr["HasSubGroup"]) == true)
                    {
                        strBuilder.Append(string.Format("<div class='col-sm-3'><a style='text-decoration:none' href='groups.aspx?id={2}' target ='_blank'><div class='fruitbox'><img src='GroupImages/{3}' style='height:120px' alt='' class='img-responsive' /><h2>{1}</h2><div class='explorebutton'><h3>Explore</h3></div></div></a></div>", dr["PhotoUrl1"].ToString(), dr["Title"].ToString(), dr["Id"].ToString(), dr["PhotoUrl1"].ToString()));
                    }
                    else
                    {
                        string str ="http://"+ dr["LinkUrl"].ToString();
                        strBuilder.Append(string.Format("<div class='col-sm-3'><a style='text-decoration:none' href='" + str + "' target ='_blank'><div class='fruitbox'><img src='GroupImages/{3}' style='height:120px' alt='' class='img-responsive' /><h2>{1}</h2><div class='explorebutton'><h3>Explore</h3></div></div></a></div>", dr["PhotoUrl1"].ToString(), dr["Title"].ToString(), dr["Id"].ToString(), dr["PhotoUrl1"].ToString()));
                    }
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }



    public string GetAddsGroupHTMLByLocation(string Location,int Top)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new MyGroupsDAL().GetAddsByLocation(Location,Top);
            if (dr.HasRows)
            {

                while (dr.Read())
                {
                    if (Convert.ToBoolean(dr["HasSubGroup"]) == true)
                    {
                        strBuilder.Append(string.Format("<div class='col-sm-6'><a href='groups.aspx?id={2}' target ='_blank'><img src='GroupImages/{3}' style='height:200px;width:100%' /></div></a></div>", dr["PhotoUrl1"].ToString(), dr["Title"].ToString(), dr["Id"].ToString(), dr["PhotoUrl1"].ToString()));
                    }
                    else
                    {
                        string str = "http://" + dr["LinkUrl"].ToString();
                        strBuilder.Append(string.Format("<div class='col-sm-6'><a href='" + str + "' target ='_blank'><img src='GroupImages/{3}' style='height:200px;width:100%' /></a></div>", dr["PhotoUrl1"].ToString(), dr["Title"].ToString(), dr["Id"].ToString(), dr["PhotoUrl1"].ToString()));
                    }
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }


    public string GetSideAddsGroupHTMLByLocation(string Location, int Top)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new MyGroupsDAL().GetAddsByLocation(Location, Top);
            if (dr.HasRows)
            {

                while (dr.Read())
                {
                    if (Convert.ToBoolean(dr["HasSubGroup"]) == true)
                    {
                        strBuilder.Append(string.Format("<a href='groups.aspx?id={2}' target ='_blank'><img src='GroupImages/{3}' style='width:100%' /></a></div>", dr["PhotoUrl1"].ToString(), dr["Title"].ToString(), dr["Id"].ToString(), dr["PhotoUrl1"].ToString()));
                    }
                    else
                    {
                        string str = "http://" + dr["LinkUrl"].ToString();
                        strBuilder.Append(string.Format("<a href='" + str + "' target ='_blank'><img src='GroupImages/{3}' style='width:100%' /></a>", dr["PhotoUrl1"].ToString(), dr["Title"].ToString(), dr["Id"].ToString(), dr["PhotoUrl1"].ToString()));
                    }
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }

    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new MyGroupsDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Id"].ToString(), dr["Title"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }
    public List<MyGroups> GetAll()
    {
        List<MyGroups> headingList = new List<MyGroups>();

        SqlDataReader dr = new MyGroupsDAL().GetAll();
        try
        {



            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    MyGroups objMyGroups = new MyGroups()
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Title = dr["Title"].ToString(),
                        PhotoUrl1 = dr["PhotoUrl1"].ToString(),
                        PhotoUrl2 = dr["PhotoUrl2"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        ShowOn = dr["ShowOn"].ToString(),
                        LinkUrl = dr["LinkUrl"].ToString(),
                        HasSubGroup = Convert.ToBoolean(dr["HasSubGroup"]),
                        MobileImgUrl = dr["MobileImgUrl"].ToString(),
                    };
                    headingList.Add(objMyGroups);
                }
            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return headingList;

    }



    public List<MyGroups> GetAllHasSubgroups()
    {
        List<MyGroups> headingList = new List<MyGroups>();

        SqlDataReader dr = new MyGroupsDAL().GetAllHasSubgroups();
        try
        {



            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    MyGroups objMyGroups = new MyGroups()
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Title = dr["Title"].ToString(),
                        PhotoUrl1 = dr["PhotoUrl1"].ToString(),
                        PhotoUrl2 = dr["PhotoUrl2"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        ShowOn = dr["ShowOn"].ToString(),
                        LinkUrl = dr["LinkUrl"].ToString(),
                        HasSubGroup = Convert.ToBoolean(dr["HasSubGroup"]),
                        MobileImgUrl = dr["MobileImgUrl"].ToString(),
                    };
                    headingList.Add(objMyGroups);
                }
            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return headingList;

    }




    public int InsertUpdate(MyGroups objMyGroups)
    {

        return new MyGroupsDAL().InsertUpdate(objMyGroups);
    }

    public Int32 InsertGroupProducts(MyGroups objMyGroups, DataTable dt)
    {

        return new MyGroupsDAL().InsertProducts(objMyGroups, dt);
    }

}