﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;


public class ComboTypeBLL
{
   
    public Int16 InsertUpdate(ComboType  objCategories)
    {

        return new ComboTypeDAL ().InsertUpdate(objCategories);
    }
    public List<ComboType > GetAll()
    {
        List<ComboType> catList = new List<ComboType>();

        SqlDataReader dr = null;
        try
        {
            dr = new ComboTypeDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    ComboType objCategories = new ComboType()
                    {
                        Title = dr["Title"].ToString(),
                        Description = dr["Description"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        ComboTypeId = Convert.ToInt16(dr["ComboTypeId"]),
                        AdminId = Convert.ToInt32(dr["AdminId"]),


                    };
                    catList.Add(objCategories);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return catList;

    }

	
}