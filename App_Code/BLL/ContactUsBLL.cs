﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

public class ContactUsBLL
{
    public List<ContactUs> GetAll()
    {
        List<ContactUs> objlist = new List<ContactUs>();
        SqlDataReader dr = null;
        try
        {
            dr = new ContactUsDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    ContactUs objcont = new ContactUs()
                    {
                        ContactId = Convert.ToInt32(dr["ContactId"]),
                        FirstName = dr["FirstName"].ToString(),
                        LastName = dr["LastName"].ToString(),
                        Email = dr["Email"].ToString(),
                        Enquiry = dr["Enquiry"].ToString(),
                        Phoneno = dr["Phoneno"].ToString(),
                        Subject = dr["Subject"].ToString(),
                        Sdate = Convert.ToDateTime(dr["Sdate"]),
                        ParentId = Convert.ToInt32(dr["ParentId"]),
                        Status = dr["Status"].ToString(),
                    };
                    objlist.Add(objcont );
                }
            }
        }
        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return objlist;
    }
    public List<ContactUs> GetById(string Email)
    {
        List<ContactUs> objlist = new List<ContactUs>();
        SqlDataReader dr = null;
        try
        {
            dr = new ContactUsDAL().GetById (Email  );
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    ContactUs objcont = new ContactUs()
                    {
                        FirstName = dr["FirstName"].ToString(),
                        FirstName1 = dr["FirstName1"].ToString(),
                        Enquiry = dr["Enquiry"].ToString(),
                        Enquiry1 = dr["Enquiry1"].ToString(),
                    };
                    objlist.Add(objcont);
                }
            }
        }
        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return objlist;
    }
    public Int16 Insert(ContactUs objcon)
    {
        return new ContactUsDAL().Insert(objcon );
    }



    public List<ContactUs> GetByDate(DateTime FromDate, DateTime ToDate)
    {
        List<ContactUs> ContactUsList = new List<ContactUs>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];
        try
        {
            dr = new ContactUsDAL().GetByDate(FromDate, ToDate);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    ContactUs objContact = new ContactUs()
                    {
                        ContactId = Convert.ToInt32(dr["ContactId"]),
                        FirstName = dr["FirstName"].ToString(),
                        LastName = dr["LastName"].ToString(),
                        Email = dr["Email"].ToString(),
                        Phoneno = dr["Phoneno"].ToString(),
                        Subject = dr["Subject"].ToString(),
                        Enquiry = dr["Enquiry"].ToString(),
                        AdminId = Convert.ToInt32(dr["AdminId"]),
                        Sdate = Convert.ToDateTime(dr["Sdate"]),
                        FullName = dr["FullName"].ToString(),
                    };
                    ContactUsList.Add(objContact);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return ContactUsList;

    }


}