﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class VisitingUsersBLL
{
  
    public List<VisitingUsers> GetAllUsersMonthWise(out string TodayUsers)
    {
        TodayUsers = "";
        List<VisitingUsers> UserList = new List<VisitingUsers>();
        SqlDataReader dr = new VisitingUsersDAL().GetAllUsersMonthWise();


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    TodayUsers = dr["TodaysUsers"].ToString();
                    VisitingUsers objissue = new VisitingUsers()
                    {
                       
                        Month = dr["Month"].ToString(),
                        MonthlyUsers = Convert.ToString(dr["MonthlyUsers"]),
                    };
                    UserList.Add(objissue);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return UserList;

    }
    public List<VisitingUsers> GetAllUsersYearWise()
    {
        List<VisitingUsers> UserList = new List<VisitingUsers>();
        SqlDataReader dr = new VisitingUsersDAL().GetAllUsersYearWise();


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    VisitingUsers objissue = new VisitingUsers()
                    {
                        Year = dr["Year"].ToString(),
                        YearlyUsers = Convert.ToString(dr["YearlyUsers"]),
                    };
                    UserList.Add(objissue);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return UserList;

    }
    public Int16 InsertUpdate(VisitingUsers objUsers)
    {
        return new VisitingUsersDAL().InsertUpdate(objUsers);
    }
}