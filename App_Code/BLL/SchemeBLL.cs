﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;


public class SchemeBLL
{
    public class MasterProductDetail
    {
        public Int32  VariationId { get; set; }
        public string  PhotoUrl { get; set; }
        public string Name { get; set; }
        public decimal  Price { get; set; }
    }
    public class FreeProductDetail
    {
        public Int32 VariationId { get; set; }
        public string PhotoUrl { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
    public List<FreeProductDetail> GetFreeProductDetailByID(Schemes objScheme)
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlDataReader dr = new SchemeDAL().GeFreeProductDetailByID(objScheme);
        List<FreeProductDetail> lstPDetail = new List<FreeProductDetail>();
        try
        {
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    FreeProductDetail objProducteDetail = new FreeProductDetail();
                    objProducteDetail.VariationId = Convert.ToInt32(dr["VariationId"]);
                    objProducteDetail.PhotoUrl = Convert.ToString(dr["PhotoUrl"]);
                    objProducteDetail.Name = Convert.ToString(dr["Name"]);
                    objProducteDetail.Price = Convert.ToDecimal(dr["Price"]);
                    lstPDetail.Add(objProducteDetail);
                }
            }
        }
        finally
        {
            dr.Close();
            dr.Dispose();
        }
        dr.Close();
        dr.Dispose();
        return lstPDetail;
    }
    public Int16 CheckAllReadyExistId(Int32 VariaionId)
    {
        Int16 retval = 0;
        try
        {
            retval = new SchemeDAL().CheckAllReadyExistId(VariaionId);
        }
        finally
        {
           
        }
        return retval;
    }
    public List<MasterProductDetail> GetMasterProductDetailByID(Schemes ObjScheme)
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlDataReader dr = new SchemeDAL().GetMasterProductDetailByID(ObjScheme);
        List<MasterProductDetail> lstPDetail = new List<MasterProductDetail>();
        try
        {
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    MasterProductDetail objProducteDetail = new MasterProductDetail();
                    objProducteDetail.VariationId = Convert.ToInt32(dr["VariationId"]);
                    objProducteDetail.PhotoUrl = Convert.ToString(dr["PhotoUrl"]);
                    objProducteDetail.Name = Convert.ToString(dr["Name"]);
                    objProducteDetail.Price = Convert.ToDecimal(dr["Price"]);
                    lstPDetail.Add(objProducteDetail);
                }
            }
        }
        finally
        {
            dr.Close();
            dr.Dispose();
        }
        dr.Close();
        dr.Dispose();
        return lstPDetail;
    }
    public List<Schemes> GetAll()
    {
        List<Schemes> SchemeList = new List<Schemes>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new SchemeDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Schemes  objScheme = new Schemes()
                    {
                        Title = dr["Title"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        SchemeId = Convert.ToInt32(dr["SchemeId"]),
                        Description = dr["Description"].ToString(),
                        Qty = Convert.ToInt32(dr["Qty"]),
                        PackScheme = Convert.ToBoolean(dr["PackScheme"]),
                        StartDate = Convert.ToDateTime(dr["StartDate"]),
                        EndDate = Convert.ToDateTime(dr["EndDate"]),
                        PhotoUrl = dr["PhotoUrl"].ToString(),

                    };
                    SchemeList.Add(objScheme);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return SchemeList;

    }


    public List<Schemes> GetAllActiveSchemes()
    {
        List<Schemes> SchemeList = new List<Schemes>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new SchemeDAL().GetAllActiveSchemes();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Schemes objScheme = new Schemes()
                    {
                        Title = dr["Title"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        SchemeId = Convert.ToInt32(dr["SchemeId"]),
                        Description = dr["Description"].ToString(),
                        Qty = Convert.ToInt32(dr["Qty"]),
                        PackScheme = Convert.ToBoolean(dr["PackScheme"]),
                        StartDate = Convert.ToDateTime(dr["StartDate"]),
                        EndDate = Convert.ToDateTime(dr["EndDate"]),
                        PhotoUrl = dr["PhotoUrl"].ToString(),

                    };
                    SchemeList.Add(objScheme);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return SchemeList;

    }
 

    public List<Schemes> GetActiveSchemes()
    {
        List<Schemes> SchemeList = new List<Schemes>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new SchemeDAL().GetActiveSchemes();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Schemes objScheme = new Schemes()
                    {
                        Title = dr["Title"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        SchemeId = Convert.ToInt32(dr["SchemeId"]),
                        Description = dr["Description"].ToString(),
                        Qty = Convert.ToInt32(dr["Qty"]),
                        PackScheme = Convert.ToBoolean(dr["PackScheme"]),
                        StartDate = Convert.ToDateTime(dr["StartDate"]),
                        EndDate = Convert.ToDateTime(dr["EndDate"]),
                        PhotoUrl = dr["PhotoUrl"].ToString(),

                    };
                    SchemeList.Add(objScheme);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return SchemeList;

    }
 
    
    
    
    
    
    public Int32 InsertUpdate(Schemes objScheme, DataTable dtMaster, DataTable dtFree)
    {
        return new SchemeDAL().InsertUpdate(objScheme,dtMaster,dtFree);
    }
}