﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for RechargeWalletBLL
/// </summary>
public class RechargeWalletBLL
{
    public List<RechargeWallet> GetAll()
    {
        List<RechargeWallet> headingList = new List<RechargeWallet>();

        SqlDataReader dr = new RechargeWalletDAL().GetAll();
        try
        {



            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    RechargeWallet objRechargeWallet = new RechargeWallet()
                    {
                        RechargeID = Convert.ToInt32(dr["RechargeId"]),
                        DateOfRecharge = Convert.ToDateTime(dr["DateOfRecharge"]),
                        Amount = Convert.ToDecimal(dr["Amount"]),
                        UserId = Convert.ToInt32(dr["UserId"]),
                    };
                    headingList.Add(objRechargeWallet);
                }
            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();
        }
        return headingList;

    }

    public int Insert(RechargeWallet objRechargeWallet)
    {

        return new RechargeWalletDAL().Insert(objRechargeWallet);
    }

    public List<RechargeWallet> GetByUserId(Int32 UserId)
    {

        List<RechargeWallet> RechargeList = new List<RechargeWallet>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new RechargeWalletDAL().RechargesByUserId(UserId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    RechargeWallet objRecharges = new RechargeWallet()
                    {
                        RechargeID = Convert.ToInt32(dr["RechargeID"].ToString()),
                        DateOfRecharge = Convert.ToDateTime(dr["DateOfRecharge"].ToString()),
                        Amount = Convert.ToDecimal(dr["Amount"].ToString()),

                        UsedAmount = Convert.ToDecimal(dr["UsedAmount"]),
                       
                    };
                    RechargeList.Add(objRecharges);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return RechargeList;

    }
}