﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for GetActiveOffers
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class GetActiveOffers : System.Web.Services.WebService
{



    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string OfferList()
    {
        List<Offers> lstOffers = new List<Offers>();
        SqlDataReader dr = null;
        SqlParameter[] objParam = new SqlParameter[0];
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
              "shopping_sp_OffersGetActiveMobile", objParam);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                     
                        Offers objOffer = new Offers()
                        {
                            OfferId = Convert.ToInt32(dr["OfferId"].ToString()),
                            OfferType = dr["OfferType"].ToString(),
                            Title = dr["Title"].ToString(),
                            Description = dr["Description"].ToString(),
                            CouponNo = dr["CouponNo"].ToString(),
                            FromDate = Convert.ToDateTime(dr["FromDate"]),
                            ToDate = Convert.ToDateTime(dr["ToDate"]),


                        };
                        lstOffers.Add(objOffer);
                     
                }
            }
        }
        catch
        {
            dr.Close();
            dr.Dispose();
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            Offers = lstOffers,
        };
        return ser.Serialize(JsonData);


    }

}
