﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Wallet
/// </summary>
public class Wallet
{
    public int WalletId { get; set; }
    public int UserId { get; set; }
    public string Type { get; set; }
    public decimal Value { get; set; }
    public string Status { get; set; }
    public decimal Total { get; set; }
    public decimal Used { get; set; }
    public int GiftId { get; set; }
    public decimal DisAmt { get; set; }
    public int Points { get; set; }

	public Wallet()
	{
        WalletId = 0;
        UserId = 0;
        Type = "";
        Value = 0;
        Status = "";
        Total = 0;
        Used = 0;
        GiftId = 0;
        DisAmt = 0;
        Points = 0;
	}
}