﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class UserComboDetail
{
    public int ComboId { get; set; }
    public int CustomerId { get; set; }
    public Int64 ProductId { get; set; }
    public string VariationId { get; set; }
    public int Qty { get; set; }
	public UserComboDetail()
    {
        ComboId = 0;
        CustomerId = 0;
        ProductId = 0;
        VariationId = string.Empty ;
        Qty = 0;
	}
}