﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for NewsLetterUsers
/// </summary>
public class NewsLetterUsers
{
    public int NewsLetterUserId { get; set; }
    public string Name { get; set; }
    public string Email { get; set; }
    public bool IsSubscribed { get; set; }
    public bool IsVerified { get; set; }

	public NewsLetterUsers()
	{
        NewsLetterUserId = 0;
        Name = "";
        Email = "";
        IsSubscribed = true;
        IsVerified = false;
	}
}