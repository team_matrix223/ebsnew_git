﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for WebServices
/// </summary>
public class WebServices
{
    public int CategoryId { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public int ParentId { get; set; }
    public int Level { get; set; }
	public WebServices()
	{
        CategoryId = 0;
        Title = string.Empty;
        Description = string.Empty;
        ParentId = 0;
        Level = 0;
	}
}