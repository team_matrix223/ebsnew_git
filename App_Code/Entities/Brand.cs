﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Brand
/// </summary>
public class Brand
{
    public int BrandId { get; set; }
    public string Title { get; set; }
    public bool IsActive { get; set; }
    public int AdminId { get; set; }
    public string Image { get; set; }
    public string Description { get; set; }
    public Brand()
	{
        AdminId = 0;
        BrandId = 0;
        Title = string.Empty;
        Image = string.Empty;
        IsActive = true;
        Description = string.Empty;

    }
}