﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Admin
/// </summary>
public class RecipeIngredients
{


    public int ProductId { get; set; }
    public int RecipeId { get; set; }
    public int VariationId { get; set; }
    public decimal Qty { get; set; }
    public string ProductName { get; set; }
    public string Unit { get; set; }
    public string Url { get; set; }

    public RecipeIngredients()
    {
        Url = string.Empty;
        ProductId = 0;
        RecipeId = 0;
        VariationId = 0;
        Qty = 0;
        ProductName = string.Empty;
        Unit = string.Empty;
    }
}