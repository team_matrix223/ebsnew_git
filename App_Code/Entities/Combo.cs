﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Combo
{
    public int ComboId { get; set; }
    public int ComboTypeId { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public decimal  Price { get; set; }
    public bool IsActive { get; set; }
    public int AdminId { get; set; }
    public DateTime  DOC { get; set; }
    public string ComboTypeName { get; set; }
	public Combo()
	{ComboId=0;
      ComboTypeId=0;
     Title="";
     Description="";
      Price=0;
     IsActive=true;
     AdminId=0;
     DOC = System.DateTime.Now ;
     ComboTypeName = "";
	}
}