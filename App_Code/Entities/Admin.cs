﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Admin
/// </summary>
public class Admin
{

    public int AdminId { get; set; }
   public string AdminName { get; set; }  
   public string Password { get; set; }  
   public string Name { get; set; }  
   public string Mobile { get; set; }  
   public string Roles { get; set; }  
   public bool IsActive { get; set; }  
    
    
 
	public Admin()
	{
        AdminId = 0;
		AdminName=string.Empty;  
        Password=string.Empty; 
        Name=string.Empty; 
        Mobile=string.Empty; 
        Roles=string.Empty;
        IsActive = true ;
	}
}