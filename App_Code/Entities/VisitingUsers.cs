﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class VisitingUsers
{
    public string  IPAddress { get; set; }
    public DateTime  SDate { get; set; }
    public string strDate { get { return  SDate.ToString("d"); } }
    public string Month { get; set; }
    public string Year { get; set; }
    public string MonthlyUsers { get; set; }
    public string YearlyUsers { get; set; }
    public VisitingUsers()
    {
        IPAddress = "";
        SDate = DateTime.Now;
        Month = "";
        Year = "";
        MonthlyUsers = "";
        YearlyUsers = "";
    }
}