﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Common
/// </summary>
public class Common
{

    public int ID { get; set; }
    public string Title { get; set; }
    public string Status { get; set; }
	public Common()
	{
        ID = 0;
        Title = string.Empty;
        Status = string.Empty;
	}
}