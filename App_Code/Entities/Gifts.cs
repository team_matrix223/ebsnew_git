﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Gifts
/// </summary>
public class Gifts
{
        public int GiftId { get; set; }
        public string GiftName { get; set; }
        public string GiftDesc { get; set; }
        public string GiftPhotoUrl { get; set; }

	public Gifts()
	{
		GiftId=0;
        GiftName="";
        GiftDesc ="";
        GiftPhotoUrl="";
	}
}