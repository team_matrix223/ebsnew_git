﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Notification
/// </summary>
public class Notification
{
    public int Id { get; set; }
    public string Img { get; set; }
    public string Title { get; set; }
    public int Code { get; set; }
    public string Mobile { get; set; }
    public string Time { get; set; }
    public string TT { get; set; }
    public string Url { get; set; }
    public string Type { get; set; }

	public Notification()
	{
        Id = 0;
        Img = string.Empty;
        Title = string.Empty;
        Code = 0;
        Mobile = string.Empty;
        Time = string.Empty;
        TT = string.Empty;
        Url = string.Empty;
        Type = string.Empty;
	}
}