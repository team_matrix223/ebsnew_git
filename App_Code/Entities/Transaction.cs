﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Transaction
/// </summary>
public class Transaction
{

    public int TransactionId { get; set; }
    public string Responsecode { get; set; }
    public string ResponseMessage { get; set; }
    public string MerchantTxnId { get; set; }
    public string EpgTxnId { get; set; }
    public string AuthIdCode { get; set; }
    public string RRN { get; set; }
    public string CVRESPCode { get; set; }
    public string CookieString { get; set; }
    public string FDMSScore { get; set; }
    public string FDMSResult { get; set; }
 
	public Transaction()
	{
        TransactionId = 0;
        Responsecode = "";
        ResponseMessage = "";
        MerchantTxnId = "";
        EpgTxnId = "";
        AuthIdCode = "";
        RRN = "";
        CVRESPCode = "";
        CookieString = "";
        FDMSResult = "";
        FDMSScore = "";
	}
}