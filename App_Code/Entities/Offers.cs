﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Offers
{
    public int OfferId { get; set; }
    public string  OfferType { get; set; }
    public string  Title { get; set; }
    public string  Description { get; set; }
    public string  CouponNo { get; set; }
    public decimal OrderAmount { get; set; }
    public string  DiscountType { get; set; }
    public int Points { get; set; }
    public string  DiscountMode { get; set; }
    public decimal Discount { get; set; }
    public DateTime  FromDate { get; set; }
    public string strFromDate { get { return FromDate.ToString("d"); } }
    public DateTime ToDate { get; set; }
    public string strToDate { get { return ToDate.ToString("d"); } }
    public string  PaymentOption { get; set; }
    public bool IsActive { get; set; }
    public DateTime  DOC { get; set; }
    public int AdminId { get; set; }
    public int OrdersPerCustomer { get; set; }
    public int MaxLimit { get; set; }
	public Offers()
    {
        OfferId = 0;
        OfferType = "";
        Title  = "";
        Description = "";
        CouponNo = "";
        DiscountType = "";
        Points = 0;
        DiscountMode = "";
        Discount = 0;
        FromDate  = DateTime.Now;
        ToDate = DateTime.Now;
        PaymentOption = "";
        IsActive = true;
        DOC = DateTime.Now;
        AdminId = 0;
        OrdersPerCustomer = 0;
        MaxLimit = 0;
	}
}