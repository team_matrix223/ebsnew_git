﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GroupLinks
/// </summary>
public class GroupLinks
{

    public int SubGroupId { get; set; }
    public int GroupId { get; set; }
    public string SubGroupTitle { get; set; }
    public string PhotoUrl { get; set; }
    public int CatgoryId { get; set; }
    public string CatgoryName { get; set; }
    public int AdminId { get; set; }
    public string CategoryLevel1 { get; set; }
    public string CategoryLevel2 { get; set; }
    public string CategoryLevel3 { get; set; }
    public string LinkUrl { get; set; }
    public int LevelNo { get; set; }
    public int ProductId { get; set; }
	public GroupLinks()
	{
        SubGroupId = 0;
        GroupId = 0;
        SubGroupTitle = "";
        PhotoUrl = "";
        CatgoryId = 0;
        CatgoryName = "";
        AdminId = 0;
        CategoryLevel1 = "";
        CategoryLevel2 = "";
        CategoryLevel3 = "";
        LinkUrl = string.Empty;
        ProductId = 0;
        LevelNo = 0;
	}
}