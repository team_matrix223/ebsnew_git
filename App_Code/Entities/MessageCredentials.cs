﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class MessageCredentials
{
    public string AdminMobileNo { get; set; }
    public string CustomerMobileNo { get; set; }
    public string CustomerName { get; set; }
    public string AdminMessageText { get; set; }
    public string CustomerMessageText { get; set; }
    public string OrderOnCall { get; set; }
    public string CallUs { get; set; }
	public MessageCredentials()
    {
        CustomerMobileNo = "";
        CustomerName = "";
        AdminMobileNo = "";
        CustomerMessageText = "";
        AdminMessageText = "";
        OrderOnCall = "";
        CallUs = "";
	}
}
public class MessageCredentialsDAL
{
    public MessageCredentials   GetAll()
    { 
        MessageCredentials objMsg=new MessageCredentials();
        SqlParameter[] objparam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_MessageCredentialsGetAll", objparam);
            if (dr.HasRows)
            {
                while(dr.Read())
                {
                    objMsg.AdminMobileNo = dr["AdminMobileNo"].ToString();
                    objMsg.AdminMessageText = dr["AdminMessageText"].ToString();
                    objMsg.CustomerMessageText = dr["CustomerMessageText"].ToString();
                    objMsg.OrderOnCall = dr["OrderOnCall"].ToString();
                    objMsg.CallUs = dr["CallUs"].ToString();
                }
            }
        }
        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return objMsg; 
    }
    public Int16 Insert(MessageCredentials objMsg)
    {
        Int16 retVal = 0;
        SqlParameter[] objparam = new SqlParameter[6];
        objparam[0] = new SqlParameter("@AdminMobileNo", objMsg.AdminMobileNo);
        objparam[1] = new SqlParameter("@AdminMessageText", objMsg.AdminMessageText);
        objparam[2] = new SqlParameter("@CustomerMessageText", objMsg.CustomerMessageText);
        objparam[3] = new SqlParameter("@retval", SqlDbType.Int,4);
        objparam[3].Direction = ParameterDirection.ReturnValue;
        objparam[4] = new SqlParameter("@OrderOnCall", objMsg.OrderOnCall);
        objparam[5] = new SqlParameter("@CallUs", objMsg.CallUs);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_MessageCredentialsInsert", objparam);
            retVal = Convert.ToInt16(objparam[3].Value );
        }
        finally
        {
            objparam = null;
        }
        return retVal;
    }
}