﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Enquiries
/// </summary>
public class Enquiries
{
    public int EnquiryId { get; set; }
    public string PersonName { get; set; }
    public string EmailId { get; set; }
    public string Enquiry { get; set; }
    public string MobileNo { get; set; }
    public DateTime DOC { get; set; }
    public string strOD { get { return DOC.ToString("d"); } }
	public Enquiries()
	{
        EnquiryId = 0;
        PersonName = string.Empty;
        EmailId = string.Empty;
        Enquiry = string.Empty;
        MobileNo = string.Empty;
        DOC = DateTime.Now;

	}
}