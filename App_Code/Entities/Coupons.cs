﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Coupons
/// </summary>
public class Coupons
{
    public int CouponId { get; set; }
    public string CouponNo { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public DateTime DOC { get; set; }
    public int AdminId { get; set; }


	public Coupons()
	{
        CouponId = 0;
        CouponNo = string.Empty;
        Title = string.Empty;
        Description = string.Empty;
        DOC = DateTime.Now;
        AdminId = 0;

	}
}