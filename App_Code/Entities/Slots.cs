﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Slots
/// </summary>
public class Slots
{
  public  string DayName    { get; set; }
  public string Slot1Start { get; set; }
  public string Slot1End { get; set; }
  public string Slot2Start { get; set; }
  public string Slot2End { get; set; }
  public string Slot3Start { get; set; }
  public string Slot3End { get; set; }
  public string Slot4Start { get; set; }
  public string Slot4End { get; set; }
    public Slots()
    {

        DayName = string.Empty;
        Slot1Start = string.Empty;
        Slot1End = string.Empty;
        Slot2Start = string.Empty;
        Slot2End = string.Empty;
        Slot3Start = string.Empty;
        Slot3End = string.Empty;
        Slot4Start = string.Empty;
        Slot4End = string.Empty;
    }
}