﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Variations
{
    public Int32 ProductId { get; set; }
    public Int64 VariationId { get; set; }
    public string Unit { get; set; }
    public decimal Price { get; set; }
    public decimal Mrp { get; set; }
    public decimal Qty { get; set; }
    public string Description { get; set; }
    public string type { get; set; }
    public string PhotoUrl { get; set; }
    public string Name { get; set; }
    public string Brand { get; set; }
    public string ItemCode { get; set; }
    public bool IsActive { get; set; }
	public Variations()
    {
        ProductId = 0;
        VariationId = 0;
        Unit = string.Empty;
        Price = 0;
        Mrp = 0;
        Qty = 0;
        Description = string.Empty;
        type = "";
        PhotoUrl = "";
        ItemCode = string.Empty;
        IsActive = false;
        Brand = string.Empty;
	}
}