﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Cities
/// </summary>
public class Cities
{
    public int CityId { get; set; }
    public string Title { get; set; }
    public bool IsActive { get; set; }
    public int AdminId { get; set; }
    public Cities()
    {
        AdminId = 0;
        CityId = 0;
        Title = string.Empty;
        IsActive = true;
    }
}