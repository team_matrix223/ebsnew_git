﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for OuickList
/// </summary>
/// 

public class QuickDetail
{
    public int ListId { get; set; }
    public int UserId { get; set; }
    public string ProductName { get; set; }
    public decimal Qty { get; set; }
    public string Unit { get; set; }
}


public class QuickList
{
    public int ListId { get; set; }
    public int UserId { get; set; }
    public int DeliveryAddressId { get; set; }
    public DateTime CreatedDate { get; set; }
    public string strOD { get { return CreatedDate.ToString("d"); } }
    public int DeliverySlot { get; set; }
    public string Slot { get; set; }
    public DateTime DeliveryDate { get; set; }
    public string strDD { get { return DeliveryDate.ToString("d"); } }
    public string UserName { get; set; }
    public string CompleteAddress { get; set; }
    public string Recipient { get; set; }

    public string Status { get; set; }

	public QuickList()
	{
        ListId = 0;
        UserId = 0;
        DeliveryAddressId = 0;
        CreatedDate = DateTime.Now;
        DeliverySlot = 0;
        DeliveryDate = DateTime.Now;
        UserName = string.Empty;
        CompleteAddress = "";
        Recipient = "";
        Status = "";
		
	}
}