﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ShipRocket
/// </summary>
public class ShipRocket
{
   
     public string name { get; set; }
    public string sku { get; set; }
    public int units { get; set; }
    public string selling_price { get; set; }
    public string discount { get; set; }
    public string tax { get; set; }
    public int hsn { get; set; }

}