﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RechargeWallet
/// </summary>
public class RechargeWallet
{
    public int RechargeID { get; set; }
    public DateTime  DateOfRecharge { get; set; }
    public string strRD { get { return DateOfRecharge.ToString("d"); } }
    public decimal Amount { get; set; }
    public decimal UsedAmount { get; set; }
    public int UserId { get; set; }
	public RechargeWallet()
	{
        RechargeID = 0;
        DateOfRecharge = DateTime.Now;
        Amount = 0;
        UserId = 0;
        UsedAmount = 0;
	}
}