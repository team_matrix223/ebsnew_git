﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Schemes
/// </summary>
public class Schemes
{
    public Int32 SchemeId { get; set; }
    public string  Title { get; set; }
    public string Description { get; set; }
    public string PhotoUrl { get; set; }
    public int Qty { get; set; }
    public bool  PackScheme { get; set; }
    public DateTime  StartDate { get; set; }
    public string StrStartDate { get { return StartDate.ToString("d"); } }
    public DateTime EndDate { get; set; }
    public string StrEndDate { get { return EndDate.ToString("d"); } }
    public bool IsActive { get; set; }
	public Schemes()
	{
        SchemeId = 0;
        Title = "";
        Description = "";
        PhotoUrl = "";
        Qty = 0;
        PackScheme = true;
        StartDate = DateTime.Now;
        EndDate = DateTime.Now;
        IsActive = true;

	}
}