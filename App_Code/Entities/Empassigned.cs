﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Empassigned
/// </summary>
public class Empassigned
{
    public int Id { get; set; }
    public int Emp_Id { get; set; }
    public int BillNo { get; set; }

	public Empassigned()
	{
        Id = 0;
        Emp_Id = 0;
        BillNo = 0;
	}
}