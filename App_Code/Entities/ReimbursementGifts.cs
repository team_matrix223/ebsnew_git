﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class ReimbursementGifts
{
    public int GiftId { get; set; }
    public string  Name { get; set; }
    public decimal  Points { get; set; }
    public decimal  Cash { get; set; }
    public string  PhotoUrl { get; set; }
    public DateTime  Doc { get; set; }
    public bool  IsActive { get; set; }
	public ReimbursementGifts()
	{
        GiftId = 0;
        Name = "";
        Points = 0;
        Cash = 0;
        PhotoUrl = "";
        Doc = DateTime.Now;
        IsActive = true;
	}
}