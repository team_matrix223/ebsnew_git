﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Employees
/// </summary>
public class Employees
{

    public int Emp_Id { get; set; }
    public string Prefix { get; set; }
    public string Emp_Name { get; set; }
    public decimal NetAmount { get; set; }
    public decimal BillNo { get; set; }
    public string Address { get; set; }
    public string ContactNo { get; set; }
    public string Email { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }
    public bool IsActive { get; set; }

	public Employees()
	{
        Emp_Id = 0;
        Prefix = string.Empty;
        Emp_Name = string.Empty;
        Address = string.Empty;
        ContactNo = string.Empty;
        Email = string.Empty;
        UserName = string.Empty;
        Password = string.Empty;
        IsActive = true;
  
	}
}