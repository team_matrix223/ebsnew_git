﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PictureOrders
/// </summary>
public class PictureOrders
{

    public int PictureOrderId { get; set; }
    public int UserId { get; set; }
    public string PhotoUrl { get; set; }
    public int DeliveryAddressId { get; set; }
    public DateTime DeliveryDate { get; set; }
    public string strDD { get { return DeliveryDate.ToString("d"); } }
    public int DeliverySlot { get; set; }
    public DateTime CreatedDate { get; set; }
    public string strOD { get { return CreatedDate.ToString("d"); } }
    public string Slot { get; set; } 
    public string UserName { get; set; }
    public string CompleteAddress { get; set; }
    public string Recipient { get; set; }
    public string Status { get; set; }
	
    public PictureOrders()
	{
        PictureOrderId = 0;
        UserId = 0;
        PhotoUrl = "";
        DeliveryAddressId = 0;
        DeliveryDate = DateTime.Now;
        DeliverySlot = 0;
        CreatedDate = DateTime.Now;
        Recipient = "";
        UserName = "";
        CompleteAddress = "";
        Slot = "";
	}
}