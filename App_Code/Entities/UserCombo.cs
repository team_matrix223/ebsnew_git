﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class UserCombo
{
    public int ComboId { get; set; }
    public int CustomerId { get; set; }
    public string Title { get; set; }
    public bool IsActive { get; set; }
    public DateTime DOC { get; set; }
    public string PhotoUrl { get; set; }
	public UserCombo()
	{
        ComboId = 0;
        CustomerId = 0;
        Title = "";
        IsActive = true;
        DOC = System.DateTime.Now;
        PhotoUrl = "";
	}
}