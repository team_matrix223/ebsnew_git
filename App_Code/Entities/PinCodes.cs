﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PinCodes
/// </summary>
public class PinCodes
{
    public int PinCodeId { get; set; }
    public string PinCode { get; set; }
    public string Sector { get; set; }
    public int CityId { get; set; }
    public string CityName { get; set; }
    public bool IsActive { get; set; }
    public PinCodes()
	{
        PinCode = "";
        PinCodeId = 0;
        Sector = "";
        CityId = 0;
        CityName = "";
        IsActive = true;
	}
}