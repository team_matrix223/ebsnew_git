﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Headings
/// </summary>
public class Sliders 
{
    public string req { get; set; }
    public string SliderType { get; set; }
    public int SliderId { get; set; }
    public string ImageUrl{ get; set; }
    public string UrlName { get; set; }
    public bool IsActive { get; set; }
    public int AdminId { get; set; }
    public DateTime DOC { get; set; }
    public Sliders()
    {
        req = string.Empty;
        SliderId = 0;
        ImageUrl = string.Empty;
        IsActive = false;
        AdminId = 0;
        DOC = DateTime.Now;
        UrlName = "";

    }
}