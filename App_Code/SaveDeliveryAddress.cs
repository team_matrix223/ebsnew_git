﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Services;

/// <summary>
/// Summary description for SaveDeliveryAddress
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
 [System.Web.Script.Services.ScriptService]
public class SaveDeliveryAddress : System.Web.Services.WebService {


    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string InsertDeliveryAddress(int UserId=0,string FirstName="",string LastName = "", string MobileNo = "",string Telephone="",string Street="",string Area="",int CityId=0,int SectorId=0,string Address="")
    {


        DeliveryAddress obj = new DeliveryAddress()
        {
            Address=Address,
            RecipientFirstName=FirstName,
            RecipientLastName=LastName,
            MobileNo=MobileNo,
            Telephone=Telephone,
            Street=Street,
            Area=Area,
            CityId=CityId,
            PinCodeId=SectorId,
            IsPrimary=true,
            UserId=UserId

        };
     
      int retVal=  new DeliveryAddressBLL().InsertUpdate(obj);


        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            Status = retVal
        };
        return ser.Serialize(JsonData);


    }
    
}
