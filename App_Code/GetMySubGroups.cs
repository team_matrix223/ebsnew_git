﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class GetMySubGroups : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string GetSubGroupsByGroupId(int GroupId)
    {


        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@GroupId", GroupId);
        DataSet ds = null;
        ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        "shopping_sp_CategoriesLinksGetByGroupId", objParam);



        List<WSMySubGroup> lstGroups = new List<WSMySubGroup>();

        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            WSMySubGroup objGroup = new WSMySubGroup()
            {
                SubGroupTitle = ds.Tables[0].Rows[i]["SubGroupTitle"].ToString(),
                SubGroupId = Convert.ToInt32(ds.Tables[0].Rows[i]["SubGroupId"].ToString()),
                PhotoUrl = ds.Tables[0].Rows[i]["PhotoUrl"].ToString(),
                CategoryId = Convert.ToInt32(ds.Tables[0].Rows[i]["CatgoryId"].ToString()),
                LevelNo = Convert.ToInt32(ds.Tables[0].Rows[i]["LevelNo"].ToString()),
            };
            lstGroups.Add(objGroup);
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            MySubGroups = lstGroups,
        };
        return ser.Serialize(JsonData);


    }  
    
}
