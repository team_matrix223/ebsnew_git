﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for AutoCompletProducts
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class AutoCompletProducts : System.Web.Services.WebService
{

    public AutoCompletProducts()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public List<string> GetProducts(string term, string date_from, string date_to)
    {
        List<string> list = new List<string>();
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@keyword", term);
        objParam[1] = new SqlParameter("@date_from", date_from);
        objParam[2] = new SqlParameter("@date_to", date_to);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "strp_search_products", objParam);

            while (dr.Read())
            {
                list.Add(dr["name"].ToString());
            }
        }
        finally
        {
            objParam = null;
        }
        return list;
    }
    [WebMethod]
    public List<string> GetBrands(string term)
    {
        List<string> list = new List<string>();
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@keyword", term);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "strp_search_brands", objParam);

            while (dr.Read())
            {
                list.Add(dr["title"].ToString());
            }
        }
        finally
        {
            objParam = null;
        }
        return list;
    }

}
