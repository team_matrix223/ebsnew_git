﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;


public class ReimbursementGiftsDAL
{
    public SqlDataReader GetByIsActive()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ReimbursementGiftsGetByIsActive", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }
    public DataSet GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ReimbursementGiftsGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
    public Int16 InsertUpdate(ReimbursementGifts objGifts)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[7];

        objParam[0] = new SqlParameter("@GiftId", objGifts.GiftId);
        objParam[1] = new SqlParameter("@Name", objGifts.Name);
        objParam[2] = new SqlParameter("@Points", objGifts.Points);
        objParam[3] = new SqlParameter("@Cash", objGifts.Cash);
        objParam[4] = new SqlParameter("@PhotoUrl", objGifts.PhotoUrl);
        objParam[5] = new SqlParameter("@IsActive", objGifts.IsActive);
        objParam[6] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[6].Direction = ParameterDirection.ReturnValue;
        
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_ReimbursementGiftsInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[6].Value);
            objGifts.GiftId  = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}