﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for PictureOrderDAL
/// </summary>
public class PictureOrderDAL
{
    public Int32 InsertPictureOrder(PictureOrders objPictureOrder)
    {

        Int32 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];

        objParam[0] = new SqlParameter("@UserId", objPictureOrder.UserId);
        objParam[1] = new SqlParameter("@PhotoUrl", objPictureOrder.PhotoUrl);
        objParam[2] = new SqlParameter("@DeliveryAddressId", objPictureOrder.DeliveryAddressId);
        objParam[3] = new SqlParameter("@DeliveryDate", objPictureOrder.DeliveryDate);
        objParam[4] = new SqlParameter("@DeliverySlot", objPictureOrder.DeliverySlot);
      
      
        objParam[5] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[5].Direction = ParameterDirection.ReturnValue;


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_PictureOrderInsert", objParam);
            retValue = Convert.ToInt32(objParam[5].Value);
            objPictureOrder.PictureOrderId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_PictureOrdersGetByDate", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


    public Int32 InsertUpdate(Order objOrder, DataTable dt)
    {

        Int32 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[9];
        objParam[0] = new SqlParameter("@PictureOrderId", objOrder.PictureOrderId);
        objParam[1] = new SqlParameter("@CustomerId", objOrder.CustomerId);
        objParam[2] = new SqlParameter("@OrderDate", objOrder.OrderDate);
        objParam[3] = new SqlParameter("@BillAmount", objOrder.BillValue);
        objParam[4] = new SqlParameter("@DeliveryAddressId", objOrder.DeliveryAddressId);
        objParam[5] = new SqlParameter("@DeliveryDate", objOrder.DeliveryDate);
        objParam[6] = new SqlParameter("@DeliverySlot", objOrder.DeliverySlot);
        objParam[7] = new SqlParameter("@ProductId", dt);
        objParam[8] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[8].Direction = ParameterDirection.ReturnValue;



        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_PictureOrderInsertInOrders", objParam);
            retValue = Convert.ToInt32(objParam[8].Value);
            objOrder.OrderId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


}