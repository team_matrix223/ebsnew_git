﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for AdminDAL
/// </summary>
public class AdminDAL
{

    public string ChangePassword(Admin objAdmin)
    {
        string retVal = "0";
       SqlParameter[] objParam = new SqlParameter[2];
     objParam[0] = new SqlParameter("@Password", objAdmin.Password);
     objParam[1] = new SqlParameter("@AdminName", objAdmin.AdminName);

         try
        {
      retVal=  SqlHelper.ExecuteScalar(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_AdminChangePassword", objParam).ToString();
              

        }
        finally
        {
            objParam = null;
        }

         return retVal;

    }

    public SqlDataReader LoginCheck(Admin objAdmin)
    {


        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@AdminName", objAdmin.AdminName);
        objParam[1] = new SqlParameter("@Password", objAdmin.Password);


        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
              "shopping_sp_AdminLoginCheck", objParam);


        }
        finally
        {
            objParam = null;
        }
        return dr;
    }


    public Int32  CheckOnChangePassword(Admin objAdmin)
    {

        Int32 retVal = 0;
        SqlParameter[] objParam = new SqlParameter[3];

        objParam[0] = new SqlParameter("@AdminName", objAdmin.AdminName);
        objParam[1] = new SqlParameter("@Password", objAdmin.Password);
        objParam[2] = new SqlParameter("@RetVal", SqlDbType.Int,4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        try
        {
             SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
              "Shopping_sp_AdminCheckChangePassword", objParam);
             retVal = Convert.ToInt32(objParam[2].Value );

        }
        finally
        {
            objParam = null;
        }
        return retVal ;
    }

    public SqlDataReader GetAll()
    {
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "ratwara_sp_GetAll", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }


    public Int16 InsertUpdate(Admin objAdmin)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[8];

        objParam[0] = new SqlParameter("@AdminName", objAdmin.AdminName);
        objParam[1] = new SqlParameter("@Password", objAdmin.Password);
        objParam[2] = new SqlParameter("@Name", objAdmin.Name);
        objParam[3] = new SqlParameter("@Mobile", objAdmin.Mobile);
        objParam[4] = new SqlParameter("@Roles", objAdmin.Roles);
        objParam[5] = new SqlParameter("@IsActive", objAdmin.IsActive);

        objParam[6] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[6].Direction = ParameterDirection.ReturnValue;
        objParam[7] = new SqlParameter("@AdminId", objAdmin.AdminId);
        
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "ratwara_sp_InsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[6].Value);
             
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


}