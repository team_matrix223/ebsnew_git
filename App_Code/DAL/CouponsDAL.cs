﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for CouponsDAL
/// </summary>
public class CouponsDAL
{
    public SqlDataReader GetByCouponNo(string CouponNo)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@CouponNo", CouponNo);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_CouponsGetByCouponNo", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }
    public SqlDataReader GetActiveCoupons()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_CouponsGetAactive", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }
    public SqlDataReader GetAll()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_CouponsGetAll", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }

    public int InsertUpdate(Coupons objCoupons)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];
        objParam[0] = new SqlParameter("@CouponNo", objCoupons.CouponNo);
        objParam[1] = new SqlParameter("@Title", objCoupons.Title);
        objParam[2] = new SqlParameter("@Description", objCoupons.Description);
        objParam[3] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[3].Direction = ParameterDirection.ReturnValue;
        objParam[4] = new SqlParameter("@CouponId ", objCoupons.CouponId);
        objParam[5] = new SqlParameter("@AdminId ", objCoupons.AdminId);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_CouponsInsertUpdate", objParam);
            retValue = Convert.ToInt32(objParam[3].Value);
            objCoupons.CouponId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }
}