﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for CancelOrderDAL
/// </summary>
public class CancelOrderDAL
{
    public int CancelOrder(CancelOrders objCancelOrder)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@OrderId", objCancelOrder.OrderId);
        objParam[1] = new SqlParameter("@CancelRemarks", objCancelOrder.CancelRemarks);
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_CancelOrder", objParam);
            retValue = Convert.ToInt32(objParam[2].Value);
            objCancelOrder.OrderCancelId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }



    public SqlDataReader CancelOrdersGetByDate(DateTime DateFrom, DateTime DateTo)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_CancelOrdersGetByDate", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


    public void TempInsert(OrderDetail objOrder)
    {
        //Int32 retval = 0;
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@OrderId", objOrder.OrderId);


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_TempCancelOrderDetailInsert", ObjParam);
            //retval = Convert.ToInt32(ObjParam[2].Value);
            //objOrder.OrderId = retval;
        }
        finally
        {
            ObjParam = null;

        }

    }

}