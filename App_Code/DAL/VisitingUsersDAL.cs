﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class VisitingUsersDAL
{
    public SqlDataReader GetAllUsersYearWise()
    {
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "ratwara_sp_VisitingUserGetAllYearWise", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }
    public SqlDataReader GetAllUsersMonthWise()
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@Date",DateTime.Now.ToShortDateString());
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "ratwara_sp_VisitingUserGetAllMonthWise", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }
    public Int16 InsertUpdate(VisitingUsers objUsers)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@IPAddress", objUsers.IPAddress);
        //objParam[1] = new SqlParameter("@SDate", objUsers.SDate.ToShortDateString());
        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[1].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "ratwara_sp_VisitingUsersInsertUpdate", objParam);

            retValue = Convert.ToInt16(objParam[1].Value);
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}