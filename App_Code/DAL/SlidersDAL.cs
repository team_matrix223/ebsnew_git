﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for SlidersDAL
/// </summary>
public class SlidersDAL
{

    
  
 

    public SqlDataReader GetAll()
    {
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_SlidersGetAll", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }

    public DataSet PageSlider(string req, string SliderType,int? SliderId)
    {
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@slider_type", SliderType);
        objParam[1] = new SqlParameter("@req", req);
        objParam[2] = new SqlParameter("@sliderid", SliderId);





        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "strp_pagesliders", objParam);



        }

        finally
        {
            objParam = null;
        }
        return ds;


    }
    public Int16 InsertUpdate(Sliders objSliders)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];

        objParam[0] = new SqlParameter("@SliderId", objSliders.SliderId);
        objParam[1] = new SqlParameter("@ImageUrl", objSliders.ImageUrl);
        objParam[2] = new SqlParameter("@IsActive", objSliders.IsActive);
        objParam[3] = new SqlParameter("@AdminId", objSliders.AdminId);
        objParam[4] = new SqlParameter("@UrlName", objSliders.UrlName);
        objParam[5] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[5].Direction = ParameterDirection.ReturnValue;
         
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_SlidersInsertUpdate", objParam);
            
            retValue = Convert.ToInt16(objParam[5].Value);
            objSliders.SliderId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
    public void PageSliderInsertDelete(Sliders objSliders)
    {


        SqlParameter[] objParam = new SqlParameter[5];
        objParam[0] = new SqlParameter("@req", objSliders.req);
        objParam[1] = new SqlParameter("@SliderId", objSliders.SliderId);
        objParam[2] = new SqlParameter("@ImageUrl", objSliders.ImageUrl); 
        objParam[3] = new SqlParameter("@slider_type", objSliders.SliderType);
        objParam[4] = new SqlParameter("@UrlName", objSliders.UrlName);
    


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "strp_pageslider_insertdelete", objParam);

        }
        finally
        {
            objParam = null;
        }
  
    }

}