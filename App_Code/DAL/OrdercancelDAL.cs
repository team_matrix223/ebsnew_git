﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for OrdercancelDAL
/// </summary>
public class OrdercancelDAL
{
    public int CancelOrder(CancelOrders objCancelOrders)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@OrderId", objCancelOrders.OrderId);
        
        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_CancelOrder", objParam);
            retValue = Convert.ToInt32(objParam[5].Value);
            objCancelOrders.OrderCancelId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }
}