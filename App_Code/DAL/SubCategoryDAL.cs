﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class SubCategoryDAL
{

    public SqlDataReader GetByCategoryId(int CategoryId)
    {

        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@CategoryId", CategoryId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "Shopping_sp_SubCategoryGetByCategoryId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


   



    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_SubCategoryGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(SubCategory objAlbum)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[7];

        objParam[0] = new SqlParameter("@CategoryId", objAlbum.CategoryId);
        objParam[1] = new SqlParameter("@Title", objAlbum.Title);
        objParam[2] = new SqlParameter("@IsActive", objAlbum.IsActive);

        objParam[3] = new SqlParameter("@SubCategoryId", objAlbum.SubCategoryId);
        objParam[4] = new SqlParameter("@Description", objAlbum.Description);
        objParam[5] = new SqlParameter("@AdminId", objAlbum.AdminId);


        objParam[6] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[6].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_SubCategoryInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[6].Value);
            objAlbum.CategoryId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}