﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

public class ContactUsDAL
{
    public SqlDataReader GetAll()
    {
        SqlDataReader dr = null;
        SqlParameter[] objparam=new SqlParameter[1];
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_ContactUsGetAll", objparam);
        }
        finally
        {
            objparam = null;
        }
        return dr;
    }
    public SqlDataReader GetById(string Email)
    {
        SqlDataReader dr = null;
        SqlParameter[] objparam = new SqlParameter[1];
        objparam[0] = new SqlParameter("@Email",Email );
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "ratwara_sp_EquiriesReplyGetByEmail", objparam);
        }
        finally
        {
            objparam = null;
        }
        return dr;
    }
    public Int16 Insert(ContactUs objContact)
    {
        Int16 RetVal = 0;
        SqlParameter[] objparam = new SqlParameter[10];
        objparam[0] = new SqlParameter("@FirstName", objContact.FirstName);
        objparam[1] = new SqlParameter("@LastName", objContact.LastName);
        objparam[2] = new SqlParameter("@Email", objContact.Email);
        objparam[3] = new SqlParameter("@Phoneno", objContact.Phoneno);
        objparam[4] = new SqlParameter("@Subject", objContact.Subject);
        objparam[5] = new SqlParameter("@Enquiry", objContact.Enquiry);
        objparam[6] = new SqlParameter("@AdminId", objContact.AdminId);
        objparam[7] = new SqlParameter("@Sdate", objContact.Sdate);
        objparam[8] = new SqlParameter("@ParentId", objContact.ParentId);
        objparam[9] = new SqlParameter("@RetVal", SqlDbType.Int ,4);
        objparam[9].Direction = ParameterDirection.ReturnValue;
       
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_ContactUsInsert", objparam);
            RetVal = Convert.ToInt16(objparam[9].Value);
        }
        finally
        {
        }
        return RetVal;
    }


    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_ContactUsGetByDate", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }

}