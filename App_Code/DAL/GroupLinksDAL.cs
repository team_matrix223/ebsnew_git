﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for GroupLinksDAL
/// </summary>
public class GroupLinksDAL
{
    public SqlDataReader GetByGroupId(int GroupId)
    {

        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@GroupId", GroupId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_CategoriesLinksGetByGroupId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public Int16 InsertUpdate(GroupLinks objGroupLink)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[9];

        objParam[0] = new SqlParameter("@SubGroupId", objGroupLink.SubGroupId);
        objParam[1] = new SqlParameter("@GroupId", objGroupLink.GroupId);
        objParam[2] = new SqlParameter("@SubGroupTitle", objGroupLink.SubGroupTitle);

        objParam[3] = new SqlParameter("@PhotoUrl", objGroupLink.PhotoUrl);
        objParam[4] = new SqlParameter("@CatgoryId", objGroupLink.CatgoryId);
        objParam[5] = new SqlParameter("@LevelNo", objGroupLink.LevelNo);
        objParam[8] = new SqlParameter("@LinkUrl", objGroupLink.LinkUrl);
        objParam[6] = new SqlParameter("@AdminId", objGroupLink.AdminId);


        objParam[7] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[7].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_SubGroupInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[7].Value);
            objGroupLink.SubGroupId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public SqlDataReader GetBySubGroupId(int SubGroupId)
    {

        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@SubGroupId", SubGroupId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shoppping_sp_GetCategoryLevelsBySubGroupId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int32 InsertProducts(GroupLinks objGroupLinks, DataTable dt)
    {

        Int32 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[7];

        objParam[0] = new SqlParameter("@SubGroupId", objGroupLinks.SubGroupId);
        objParam[1] = new SqlParameter("@GroupId", objGroupLinks.GroupId);
        objParam[2] = new SqlParameter("@SubGroupTitle", objGroupLinks.SubGroupTitle);
        objParam[3] = new SqlParameter("@PhotoUrl", objGroupLinks.PhotoUrl);
        objParam[4] = new SqlParameter("@AdminId", objGroupLinks.AdminId);
        objParam[5] = new SqlParameter("@ProductId", dt);
        objParam[6] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[6].Direction = ParameterDirection.ReturnValue;
        
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_SubGroupProductInsert", objParam);
            retValue = Convert.ToInt32(objParam[6].Value);
            objGroupLinks.SubGroupId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


}