﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for BillCancelDAL
/// </summary>
public class BillCancelDAL
{
    public int InsertUpdate(BillCancel objBillCancel)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];
        objParam[0] = new SqlParameter("@BillId", objBillCancel.BillId);
        objParam[1] = new SqlParameter("@OrderId", objBillCancel.OrderId);
        objParam[2] = new SqlParameter("@CancelRemarks", objBillCancel.CancelRemarks);
        objParam[3] = new SqlParameter("@CancelDate", objBillCancel.CancelDate);
        objParam[4] = new SqlParameter("@Status", objBillCancel.Status);
        objParam[5] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[5].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_BillOrderCancel", objParam);
            retValue = Convert.ToInt32(objParam[5].Value);
            objBillCancel.BillCancelId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }


    public SqlDataReader CancelBillsGetByDate(DateTime DateFrom, DateTime DateTo)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@FromDate", DateFrom);

        ObjParam[1] = new SqlParameter("@ToDate", DateTo);
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_CancelBillsGetByDate", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }



    public void InsertTemp(BillCancel objBillCancel)
    {
        //Int32 retval = 0;
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@BillId", objBillCancel.BillId);


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_TempCancelBillDetailInsert", ObjParam);
            //retval = Convert.ToInt32(ObjParam[2].Value);
            //objOrder.OrderId = retval;
        }
        finally
        {
            ObjParam = null;

        }

    }
}