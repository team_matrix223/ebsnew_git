﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for CommonDAL
/// </summary>
public class CommonDAL
{
    public int InsertUpdate(Common objCommon)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[3];
        //objParam[0] = new SqlParameter("@ID", objCommon.ID);
        objParam[0] = new SqlParameter("@Title", objCommon.Title);
        objParam[1] = new SqlParameter("@Status", objCommon.Status);
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
      
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_GrpSgrpInsertUpdate", objParam);
            retValue = Convert.ToInt32(objParam[2].Value);
            objCommon.ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }
}