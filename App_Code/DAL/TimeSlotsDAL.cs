﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for TimeSlotsDAL
/// </summary>
public class TimeSlotsDAL
{
    public int InsertUpdate(TimeSlots objTimeSLots)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];
        objParam[0] = new SqlParameter("@StartTime", objTimeSLots.StartTime);
        objParam[1] = new SqlParameter("@EndTime", objTimeSLots.EndTime);
        objParam[2] = new SqlParameter("@AdminId", objTimeSLots.AdminId);
        objParam[3] = new SqlParameter("@IsActive", objTimeSLots.IsActive);

        objParam[4] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[4].Direction = ParameterDirection.ReturnValue;
        objParam[5] = new SqlParameter("@Id", objTimeSLots.Id);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_TimeSlotInsertUpdate", objParam);
            retValue = Convert.ToInt32(objParam[4].Value);
            objTimeSLots.Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }

    public SqlDataReader GetSlots()
    {
        
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
           dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_GetSlots", objParam);
           
        }
        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetAllSlots()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
           dr =  SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_GetAllSlots", objParam);

        }
        finally
        {
            objParam = null;
        }
        return dr;

    }
    public SqlDataReader GetAll()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "GetAllSlots", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }
}