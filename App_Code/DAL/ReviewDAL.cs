﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for ReviewDAL
/// </summary>
public class ReviewDAL
{
    public SqlDataReader GetAll()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ReviewsGetAll", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }

    public int InsertUpdate(Reviews objReviews)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];
        objParam[0] = new SqlParameter("@UserId", objReviews.UserId);
        objParam[1] = new SqlParameter("@Title", objReviews.Title);
        objParam[2] = new SqlParameter("@Description", objReviews.Description);
        objParam[3] = new SqlParameter("@Rating", objReviews.Rating);
        objParam[4] = new SqlParameter("@IsApproved", objReviews.IsApproved);
        objParam[5] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[5].Direction = ParameterDirection.ReturnValue;
       
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_ReviewInsertUpdate", objParam);
            retValue = Convert.ToInt32(objParam[5].Value);
            objReviews.ReviewId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;

    }


    public Int32 InsertApproval(DataTable dt)
    {

        Int32 retValue = 0;
        Reviews objReviews = new Reviews();
        SqlParameter[] objParam = new SqlParameter[2];

        
        objParam[0] = new SqlParameter("@ReviewId", dt);
        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[1].Direction = ParameterDirection.ReturnValue;


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_ReviewApprovalUpdate", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objReviews.ReviewId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }



    public Int32 InsertDisApproval(DataTable dt)
    {

        Int32 retValue = 0;
        Reviews objReviews = new Reviews();
        SqlParameter[] objParam = new SqlParameter[2];


        objParam[0] = new SqlParameter("@ReviewId", dt);
        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[1].Direction = ParameterDirection.ReturnValue;


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_ReviewDisApprovalUpdate", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objReviews.ReviewId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}