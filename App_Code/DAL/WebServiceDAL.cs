﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for WebServiceDAL
/// </summary>
public class WebServiceDAL
{
    public SqlDataReader GetAllCategories()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shoppingsp_GetallCategories", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }
}