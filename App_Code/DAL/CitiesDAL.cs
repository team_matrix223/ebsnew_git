﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for CitiesDAL
/// </summary>
public class CitiesDAL
{

     

   
    public SqlDataReader GetAll()
    {
         SqlParameter[] objParam = new SqlParameter[0];
      
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_CitiesGetAll", objParam);
            
            

        }

        finally
        {
            objParam = null;
        }
        return dr;
       
        
    }

    public int InsertUpdate(Cities objCities)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];
        objParam[0] = new SqlParameter("@Title", objCities.Title);
        objParam[1] = new SqlParameter("@IsActive", objCities.IsActive);
        objParam[2] = new SqlParameter("@AdminId", objCities.AdminId);
        objParam[3] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[3].Direction = ParameterDirection.ReturnValue;
        objParam[4] = new SqlParameter("@CityId",objCities.CityId);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_CitiesInsertUpdate", objParam);
            retValue = Convert.ToInt32(objParam[3].Value);
            objCities.CityId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    
      
       

    }

     

}