﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for MyGroupsDAL
/// </summary>
public class MyGroupsDAL
{
    public SqlDataReader GetAll()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_MyGroupsGetAll", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }
   
    


   public SqlDataReader GetAllHasSubgroups()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_MyGroupsGetAllHavingSbgrps", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }

    public SqlDataReader GetByLocation(string Location)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@Location", Location);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GetMyGroupsByLocation", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }

    public SqlDataReader GetAddsByLocation(string Location,int Top)
    {
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Location", Location);
        objParam[1] = new SqlParameter("@Top", Top);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GetAdds", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }


    public int InsertUpdate(MyGroups objMyGroups)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[11];
        objParam[0] = new SqlParameter("@Title", objMyGroups.Title);
        objParam[1] = new SqlParameter("@PhotoUrl1", objMyGroups.PhotoUrl1);
        objParam[2] = new SqlParameter("@PhotoUrl2", objMyGroups.PhotoUrl2);
        objParam[3] = new SqlParameter("@ShowOn", objMyGroups.ShowOn);
        objParam[4] = new SqlParameter("@LinkUrl", objMyGroups.LinkUrl);
        objParam[5] = new SqlParameter("@IsActive", objMyGroups.IsActive);
        objParam[6] = new SqlParameter("@AdminId", objMyGroups.AdminId);
        objParam[7] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[7].Direction = ParameterDirection.ReturnValue;
        objParam[8] = new SqlParameter("@Id ", objMyGroups.Id);
        objParam[9] = new SqlParameter("@HasSubGroup ", objMyGroups.HasSubGroup);
        objParam[10] = new SqlParameter("@MobileImgUrl", objMyGroups.MobileImgUrl);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_MyGroupInsertUpdate", objParam);
            retValue = Convert.ToInt32(objParam[7].Value);
            objMyGroups.Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }


    public Int32 InsertProducts(MyGroups objMyGroups, DataTable dt)
    {

        Int32 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[10];

        objParam[0] = new SqlParameter("@Title", objMyGroups.Title);
        objParam[1] = new SqlParameter("@PhotoUrl1", objMyGroups.PhotoUrl1);
        objParam[2] = new SqlParameter("@PhotoUrl2", objMyGroups.PhotoUrl2);
        objParam[3] = new SqlParameter("@ShowOn", objMyGroups.ShowOn); 
        objParam[4] = new SqlParameter("@IsActive", objMyGroups.IsActive);
        objParam[5] = new SqlParameter("@AdminId", objMyGroups.AdminId);
        objParam[6] = new SqlParameter("@ProductId", dt);
        objParam[7] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[7].Direction = ParameterDirection.ReturnValue;
        objParam[8] = new SqlParameter("@Id ", objMyGroups.Id);
        objParam[9] = new SqlParameter("@HasSubGroup ", objMyGroups.HasSubGroup);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_GroupProductInsert", objParam);
            retValue = Convert.ToInt32(objParam[7].Value);
            objMyGroups.Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}