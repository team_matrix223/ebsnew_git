﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;


/// <summary>
/// Summary description for NotificationDAL
/// </summary>
public class NotificationDAL
{
    public SqlDataReader GetNotification()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "Shopping_sp_Notification", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }

    public Int16 UpdateNotification(Notification objNotify)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@Id", objNotify.Id);


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_UpdateNotification", objParam);
            retValue = 1;

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public SqlDataReader GetNotificationAccToType(string type)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@Type", type);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GetNotificationAcctotype", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }

    public Int16 CountUnreadNotification()
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[0].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_NotificationUnread", objParam);
            retValue = Convert.ToInt16(objParam[0].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public Int16 CountUnreadNotificationBirthday()
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[0].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_NotificationUnreadBirthday", objParam);
            retValue = Convert.ToInt16(objParam[0].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public SqlDataReader GetNotificationOrders()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "Shopping_sp_NotificationOrders", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }

    public SqlDataReader GetNotificationBirthdayAnniversary()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "Shopping_sp_NotificationBirthDayAnniversary", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }

}