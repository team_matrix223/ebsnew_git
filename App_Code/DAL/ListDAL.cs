﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;


public class ListDAL
{
    public SqlDataReader GetByUserId(int UserId)
    {

        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@UserId", UserId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ListsGetByUserId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public SqlDataReader GetProductsById(int ListId)
    {

        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@ListId", ListId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ListsGetProductsByListId", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public Int16 InsertUpdate(List objList,string SessionId)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[4];
        objParam[0] = new SqlParameter("@Title", objList .Title);
        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        objParam[2] = new SqlParameter("@UserId", objList.UserId);
        objParam[3] = new SqlParameter("@SessionId", SessionId);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_ListsInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[1].Value);
            objList.ListId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}