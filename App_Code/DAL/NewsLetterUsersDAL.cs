﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for NewsLetterUsersDAL
/// </summary>
public class NewsLetterUsersDAL
{
    public SqlDataReader GetAll()
    {
        SqlDataReader dr = null;
        SqlParameter[] objparam = new SqlParameter[1];
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_NewsLetterUserGetAll", objparam);
        }
        finally
        {
            objparam = null;
        }
        return dr;
    }
    public Int16 Update(NewsLetterUsers objNUser)
    {
        Int16 RetVal = 0;
        SqlParameter[] objparam = new SqlParameter[3];
        objparam[0] = new SqlParameter("@NewsLetterUserId", objNUser.NewsLetterUserId);
        objparam[1] = new SqlParameter("@IsSubscribed", objNUser.IsSubscribed);
        objparam[2] = new SqlParameter("@RetVal", SqlDbType.Int, 4);
        objparam[2].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_NewsLetterUserUpdate", objparam);
            RetVal = Convert.ToInt16(objparam[2].Value);
            objNUser.NewsLetterUserId = RetVal;
        }
        finally
        {
            objparam = null;
        }
        return RetVal;
    }
    public Int16 Insert(NewsLetterUsers objNUser)
    {
        Int16 RetVal = 0;
        SqlParameter[] objparam = new SqlParameter[6];
        objparam[0] = new SqlParameter("@NewsLetterUserId", objNUser.NewsLetterUserId);
        objparam[1] = new SqlParameter("@Name", objNUser.Name);
        objparam[2] = new SqlParameter("@Email", objNUser.Email);
        objparam[3] = new SqlParameter("@IsSubscribed", objNUser.IsSubscribed);
        objparam[4] = new SqlParameter("@IsVerified", objNUser.IsVerified);
        objparam[5] = new SqlParameter("@RetVal", SqlDbType.Int, 4);
        objparam[5].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_NewsLetterUserInsert", objparam);
            RetVal = Convert.ToInt16(objparam[5].Value);
            objNUser.NewsLetterUserId = RetVal;
        }
        finally
        {
            objparam = null;
        }
        return RetVal;
    }
    public Int16 ConfirmVerfication(string emailId)
    {
        Int16 RetVal = 0;
        SqlParameter[] objparam = new SqlParameter[3];
        objparam[0] = new SqlParameter("@Email", emailId);
        objparam[1] = new SqlParameter("@IsVerified", true);
        objparam[2] = new SqlParameter("@RetVal", SqlDbType.Int, 4);
        objparam[2].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_NewsLetterUserConfirmVer", objparam);
            RetVal = Convert.ToInt16(objparam[2].Value);
        }
        finally
        {
            objparam = null;
        }
        return RetVal;
    }
}