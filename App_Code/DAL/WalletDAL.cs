﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for WalletDAL
/// </summary>
public class WalletDAL
{
    public void WalletUpdateDispatchStatus(int WalletConsumptionId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@WalletConsumptionId", WalletConsumptionId);

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_WalletUpdateDispatchStatus", objParam);
        }
        finally
        {
            objParam = null;
        }

    }
    public SqlDataReader GetAllWalletGifts()
    {
        SqlParameter[] objParam = new SqlParameter[0];


        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GetAllWalletGifts", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }

    public SqlDataReader GetWalletBalanceByUserId(Int64 UserId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@UserId", UserId);
        
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "Shopping_sp_WalletGetBalanceByUser", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }


    public int InsertUpdate(Wallet objWallet)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@UserId", objWallet.UserId);
        objParam[1] = new SqlParameter("@GiftId", objWallet.GiftId);
       
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;

        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_WalletConsumptionInsert", objParam);
            retValue = Convert.ToInt32(objParam[2].Value);
            objWallet.WalletId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;

    }

    public SqlDataReader GetWalletStatusCart(Int64 UserId, string SessionId)
    {
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@UserId", UserId);
        objParam[1] = new SqlParameter("@SessionId", SessionId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_WalletGetWalletStatusCart ", objParam);
        }
        finally
        {
            objParam = null;
        }
        return dr;


    }

}