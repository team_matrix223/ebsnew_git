﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class SchemeDAL
{
    public SqlDataReader GeFreeProductDetailByID(Schemes objScheme)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@SchemeId", objScheme.SchemeId);

        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_SchemesFreeProductsGetByID", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }
    public Int16 CheckAllReadyExistId(Int32 VariationId)
    {
        Int16 retVal = 0;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@VariationId", VariationId);
        ObjParam[1] = new SqlParameter("@RetVal", SqlDbType.Int, 4);
        ObjParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_SchemesAllReadyExistsID", ObjParam);
            retVal = Convert.ToInt16(ObjParam[1].Value);
        }
        finally
        {
            ObjParam = null;
        }
        return retVal;

    }
    public SqlDataReader GetMasterProductDetailByID(Schemes objScheme)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@SchemeId", objScheme.SchemeId);

        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_SchemesMasterProductsGetByID", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }
    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_SchemesGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetAllActiveSchemes()
    {
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_SchemesGetAllActiveSchemes", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetActiveSchemes()
    {
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_SchemesGetActiveSchemes", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int32 InsertUpdate(Schemes objScheme, DataTable dtMaster, DataTable dtFree)
    {
        Int32 retVal = 0;
        SqlParameter[] objParam = new SqlParameter[12];
        objParam[0] = new SqlParameter("@SchemeId", objScheme.SchemeId);
        objParam[1] = new SqlParameter("@Title", objScheme.Title);
        objParam[2] = new SqlParameter("@Description", objScheme.Description);
        objParam[3] = new SqlParameter("@PhotoUrl", objScheme.PhotoUrl);
        objParam[4] = new SqlParameter("@Qty", objScheme.Qty);
        objParam[5] = new SqlParameter("@PackScheme", objScheme.PackScheme);
        objParam[6] = new SqlParameter("@StartDate", objScheme.StartDate);
        objParam[7] = new SqlParameter("@EndDate", objScheme.EndDate);
        objParam[8] = new SqlParameter("@IsActive", objScheme.IsActive);
        objParam[9] = new SqlParameter("@SchemeMasterProducts", dtMaster);
        objParam[10] = new SqlParameter("@SchemeFreeProducts", dtFree);
        objParam[11] = new SqlParameter("@RetVal", SqlDbType.Int, 4);
        objParam[11].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_SchemesInsertUpdate", objParam);
            retVal = Convert.ToInt32(objParam[11].Value);
        }
        finally
        {
            objParam = null;
        }
        return retVal;
    }

}