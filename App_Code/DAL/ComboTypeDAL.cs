﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class ComboTypeDAL
{
    public Int16 InsertUpdate(ComboType  objCategories)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];

        objParam[0] = new SqlParameter("@ComboTypeId", objCategories.ComboTypeId);
        objParam[1] = new SqlParameter("@Title", objCategories.Title);
        objParam[2] = new SqlParameter("@Description", objCategories.Description);
        objParam[3] = new SqlParameter("@IsActive", objCategories.IsActive);
        objParam[4] = new SqlParameter("@AdminId", objCategories.AdminId);
        objParam[5] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[5].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_ComboTypeInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[5].Value);
            objCategories.ComboTypeId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ComboTypeGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
}