﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for RechargeWalletDAL
/// </summary>
public class RechargeWalletDAL
{
    public SqlDataReader GetAll()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_RechargesGetAll", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }

    public int Insert(RechargeWallet objRechargeWallet)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@Amount", objRechargeWallet.Amount);
       
        objParam[1] = new SqlParameter("@UserId", objRechargeWallet.UserId);
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
       
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_RechargeInsert", objParam);
            retValue = Convert.ToInt32(objParam[2].Value);
            objRechargeWallet.RechargeID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }



    public SqlDataReader RechargesByUserId(Int32 UserId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@UserId", UserId);

       
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_rechargewallet", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


}