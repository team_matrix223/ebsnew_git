﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for ProductsDAL
/// </summary>
public class SlotsDAL
{





    public SqlDataReader GetByDayName(string DayName)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@DayName", DayName);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_SlotsGetByDayName", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



}