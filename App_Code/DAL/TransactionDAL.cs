﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for TransactionDAL
/// </summary>
public class TransactionDAL
{
    public int InsertUpdate(Transaction objTransaction)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[11];
        objParam[0] = new SqlParameter("@Responsecode", objTransaction.Responsecode);
        objParam[1] = new SqlParameter("@ResponseMessage", objTransaction.ResponseMessage);
        objParam[2] = new SqlParameter("@MerchantTxnId", objTransaction.MerchantTxnId);
        objParam[3] = new SqlParameter("@EpgTxnId", objTransaction.EpgTxnId);
        objParam[4] = new SqlParameter("@AuthIdCode", objTransaction.AuthIdCode);
        objParam[5] = new SqlParameter("@RRN", objTransaction.RRN);
        objParam[6] = new SqlParameter("@CVRESPCode", objTransaction.CVRESPCode);
        objParam[7] = new SqlParameter("@CookieString", objTransaction.CookieString);
        objParam[8] = new SqlParameter("@FDMSScore", objTransaction.FDMSScore);

        objParam[9] = new SqlParameter("@FDMSResult", objTransaction.FDMSResult);
        
        objParam[10] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[10].Direction = ParameterDirection.ReturnValue;
      
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_TransactionInsert", objParam);
            retValue = Convert.ToInt32(objParam[10].Value);
            objTransaction.TransactionId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }
}