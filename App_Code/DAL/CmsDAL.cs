﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;


public class Cms
{



    public int PageId { get; set; }
    public Int16 ShowOn { get; set; }

    public Int16 PageOrder { get; set; }
    public int ParentPage { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public Boolean IsActive { get; set; }
    public Boolean Footer { get; set; }
    public string Keywords { get; set; }
    public string MetaTitle { get; set; }
    public string PageDescription { get; set; }
    public bool IsFullPage { get; set; }
    public string PunjabiTitle { get; set; }
    public string PunjabiDescription { get; set; }
    public string ParentPageName { get; set; } 
    public Cms()
    {
        PageId = 1;
        PageOrder = 1;
        ParentPage = 0;
        Title = string.Empty;
        Description = string.Empty;
        IsActive = true;
        Footer = true;
        Keywords = string.Empty;
        MetaTitle = string.Empty;
        PageDescription = string.Empty;
        ShowOn = 0;
        IsFullPage = false;
        PunjabiDescription = string.Empty;
        PunjabiTitle = string.Empty;
        ParentPageName = "";
    }

    public string InsertUpdate()
    {
        string retVal = "0";
        SqlParameter[] objParam = new SqlParameter[14];


        objParam[0] = new SqlParameter("@PageId", PageId);
        objParam[1] = new SqlParameter("@Title", Title);
        objParam[2] = new SqlParameter("@Description", Description);
        objParam[3] = new SqlParameter("@ParentPage", ParentPage);
        objParam[4] = new SqlParameter("@PageOrder", PageOrder);
        objParam[5] = new SqlParameter("@IsActive", IsActive);
        objParam[6] = new SqlParameter("@Footer", Footer);
        objParam[7] = new SqlParameter("@Keywords", Keywords);
        objParam[8] = new SqlParameter("@MetaTitle", MetaTitle);
        objParam[9] = new SqlParameter("@PageDescription", PageDescription);
        objParam[10] = new SqlParameter("@ShowOn", ShowOn);
        objParam[11] = new SqlParameter("@IsFullPage", IsFullPage);
        objParam[12] = new SqlParameter("@PunjabiDescription", PunjabiDescription);
        objParam[13] = new SqlParameter("@PunjabiTitle", PunjabiTitle);


        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
                  "ratwara_sp_CMSPagesInsertUpdate", objParam).ToString();


        }
        finally
        {
            objParam = null;
        }

        return retVal;

    }









    //public void tbCM_PagesDelete()
    //{
    //    try
    //    {
    //        SqlCommand cmd = new SqlCommand();
    //        cmd.Connection = con;
    //        cmd.CommandText = "sp_tbCMPagesDelete";
    //        cmd.Parameters.AddWithValue("@iPageId_PK", PageId);
    //        cmd.CommandType = CommandType.StoredProcedure;
    //        con.Open();
    //        cmd.ExecuteNonQuery();
    //        con.Close();
    //    }
    //    finally { }

    //}





    public DataSet GetAll()
    {
        DataSet ds = new DataSet();
        SqlParameter[] objParam = new SqlParameter[0];

  
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
                   "ratwara_sp_CMSGetAllFrontEnd", objParam);


        }
        finally
        {
            objParam = null;
        }
        return ds;

    }



    




    public DataSet GetByParentId()
    {
        DataSet ds = new DataSet();
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@ParentPage", ParentPage);
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
                   "ratwara_sp_CmsGetByParentId", objParam);


        }
        finally
        {
            objParam = null;
        }
        return ds;

    }


    public List<Cms> GetByParentIdFrontEnd(int ParentId)
    {

        List<Cms> pageList = new List<Cms>();
        SqlDataReader dr = null;

        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@ParentPage", ParentId);
   
        try
  
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
                   "ratwara_sp_CmsGetByParentIdFrontEnd", objParam);

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Cms obj = new Cms()
                    {

                        Title =  dr["Title"].ToString() ,
                        Description =  dr["Description"].ToString() ,
                        PageId = Convert.ToInt16(dr["PageId"]),
                        Footer = Convert.ToBoolean(dr["Footer"]),
                        Keywords = Convert.ToString(dr["Keywords"]),
                        MetaTitle = Convert.ToString(dr["MetaTitle"]),
                        PageDescription = Convert.ToString(dr["PageDescription"]),
                        
                    };
                    pageList.Add(obj);

                }


            }

          
            return pageList;

        }
        finally
        {
            objParam = null;
        }


    }




    public void GetByPageTitle()
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@MetaTitle", MetaTitle);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "ratwara_sp_CMSGetByPageUrl", objParam);


            if (dr.HasRows)
            {
                dr.Read();

                PageId = Convert.ToInt16(dr["PageId"]);
                Title = dr["Title"].ToString();
                Description = dr["Description"].ToString();
                PageOrder = Convert.ToInt16(dr["PageOrder"]);
                ParentPage = Convert.ToInt16(dr["ParentPage"]);
                IsActive = Convert.ToBoolean(dr["IsActive"]);
                Footer = Convert.ToBoolean(dr["Footer"]);
                Keywords = Convert.ToString(dr["Keywords"]);
                MetaTitle = Convert.ToString(dr["MetaTitle"]);
                ShowOn = Convert.ToInt16(dr["ShowOn"]);
                PageDescription = Convert.ToString(dr["PageDescription"]);
                IsFullPage = Convert.ToBoolean(dr["IsFullPage"]);
                PunjabiDescription = Convert.ToString(dr["PunjabiDescription"]);
                PunjabiTitle = Convert.ToString(dr["PunjabiTitle"]);


            }
        }

        finally
        {
            objParam = null;
        }


    }



    public void GetById()
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@PageId", PageId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "ratwara_sp_CMSGetById", objParam);


            if (dr.HasRows)
            {
                dr.Read();

                PageId = Convert.ToInt16(dr["PageId"]);
                Title = dr["Title"].ToString();
                Description = dr["Description"].ToString();
                PageOrder = Convert.ToInt16(dr["PageOrder"]);
                ParentPage = Convert.ToInt16(dr["ParentPage"]);
                IsActive = Convert.ToBoolean(dr["IsActive"]);
                Footer = Convert.ToBoolean(dr["Footer"]);
                Keywords = Convert.ToString(dr["Keywords"]);
                MetaTitle = Convert.ToString(dr["MetaTitle"]);
                ShowOn = Convert.ToInt16(dr["ShowOn"]);
                PageDescription = Convert.ToString(dr["PageDescription"]);
                IsFullPage = Convert.ToBoolean(dr["IsFullPage"]);
                PunjabiDescription = Convert.ToString(dr["PunjabiDescription"]);
                PunjabiTitle = Convert.ToString(dr["PunjabiTitle"]);
                ParentPageName = Convert.ToString(dr["ParentPageName"]);
            }
        }

        finally
        {
            objParam = null;
        }


    }


}
