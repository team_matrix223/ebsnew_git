﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;


public class OffersDAL
{
    public SqlDataReader GetGiftsByOfferId(int OfferId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@OfferId", OfferId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GiftsGetByOfferId", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }
    public int RemoveOffer(string CouponNo, string SessionId)
    {
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@CouponNo", CouponNo);
        objParam[1] = new SqlParameter("@SessionId", SessionId);

        int Status = 0;
        try
        {
            Status = Convert.ToInt16(SqlHelper.ExecuteScalar(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_OffersRemove", objParam));



        }

        finally
        {
            objParam = null;
        }
        return Status;
    }


    public int RemovePointOffer(string SessionId)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@SessionId", SessionId);

        int Status = 0;
        try
        {
            Status = Convert.ToInt16(SqlHelper.ExecuteScalar(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_OffersPointRemove", objParam));



        }

        finally
        {
            objParam = null;
        }
        return Status;
    }

    public SqlDataReader ApplyOffer(string CouponNo, string SessionId, Int64 CustomerId, Int32 GiftId)
    {
        SqlParameter[] objParam = new SqlParameter[4];
        objParam[0] = new SqlParameter("@CouponNo", CouponNo);
        objParam[1] = new SqlParameter("@SessionId", SessionId);
        objParam[2] = new SqlParameter("@CustomerId", CustomerId);
        objParam[3] = new SqlParameter("@GiftId", GiftId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_OffersApply", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;
    }



    public int ApplyOfferForPoints(string SessionId, Int64 UserId, decimal DisAmt)
    {
        SqlParameter[] objParam = new SqlParameter[4];
        objParam[0] = new SqlParameter("@UserId", UserId);
        objParam[1] = new SqlParameter("@SessionId", SessionId);
        objParam[2] = new SqlParameter("@DisAmt", DisAmt);

        int Status = 0;
        try
        {
            Status = Convert.ToInt16(SqlHelper.ExecuteScalar(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_OfferPointApply", objParam));



        }

        finally
        {
            objParam = null;
        }
        return Status;
    }


    public DataSet GetGiftsByCouponNo(string CouponNo)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@CouponNo", CouponNo);
        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GiftsGetByCouponNo", objParam);



        }

        finally
        {
            objParam = null;
        }
        return ds;


    }

    public SqlDataReader GetActiveOfferForUser(string SessionId, Int64 CustomerId)
    {
        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@SessionId", SessionId);
        objParam[1] = new SqlParameter("@CustomerId", CustomerId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_OffersGetActiveForUser", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;
    }
    public SqlDataReader GetActiveOffers()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_OffersGetActive", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }
    public SqlDataReader GetAll()
    {
        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_OffersGetAll", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }


    public SqlDataReader GetUsersInfoForNewsletter(int Id)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@Type", Id);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GetUsersInfoForNewsletter", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }
    public Int16 InsertUpdate(Offers objOffer, DataTable dtGift)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[19];

        objParam[0] = new SqlParameter("@OfferId", objOffer.OfferId);
        objParam[1] = new SqlParameter("@OfferType", objOffer.OfferType);
        objParam[2] = new SqlParameter("@Title", objOffer.Title);
        objParam[3] = new SqlParameter("@Description", objOffer.Description);
        objParam[4] = new SqlParameter("@CouponNo", objOffer.CouponNo);
        objParam[5] = new SqlParameter("@OrderAmount", objOffer.OrderAmount);
        objParam[6] = new SqlParameter("@DiscountType", objOffer.DiscountType);
        objParam[7] = new SqlParameter("@Points", objOffer.Points);
        objParam[8] = new SqlParameter("@DiscountMode", objOffer.DiscountMode);
        objParam[9] = new SqlParameter("@Discount", objOffer.Discount);
        objParam[10] = new SqlParameter("@FromDate", objOffer.FromDate);
        objParam[11] = new SqlParameter("@ToDate", objOffer.ToDate);
        objParam[12] = new SqlParameter("@PaymentOption", objOffer.PaymentOption);
        objParam[13] = new SqlParameter("@IsActive", objOffer.IsActive);
        objParam[14] = new SqlParameter("@AdminId", objOffer.AdminId);
        objParam[15] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[15].Direction = ParameterDirection.ReturnValue;
        objParam[16] = new SqlParameter("@OrdersPerCustomer", objOffer.OrdersPerCustomer);
        objParam[17] = new SqlParameter("@MaxLimit", objOffer.MaxLimit);
        objParam[18] = new SqlParameter("@GiftType", dtGift);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "Shopping_sp_OffersInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[15].Value);
            objOffer.OfferId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}