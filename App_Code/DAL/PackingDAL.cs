﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using System.Text;

/// <summary>
/// Summary description for BrandDAL
/// </summary>
public class PackingDAL
{

    public List<Packing> GetAll()
    {
        List<Packing> lst = new List<Packing>();

        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_PackingBelongsGetAll", objParam);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Packing objPacking = new Packing()
                    {
                        ItemCode = dr["Item_Code"].ToString(),
                        ItemName = dr["Item_Name"].ToString(),
                        ShortName = dr["Short_Name1"].ToString(),
                        Department = dr["Department"].ToString(),
                        Price = Convert.ToDecimal(dr["Sale_Rate"]),
                        MRP = Convert.ToDecimal(dr["Max_Retail_Price"]),

                        Brand = dr["Brand"].ToString(),

                    };
                    lst.Add(objPacking);
                }
            }


        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }



        return lst;

    }



    public List<Packing> GetProductsAndVariations()
    {
        List<Packing> lst = new List<Packing>();

        SqlParameter[] objParam = new SqlParameter[0];

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GetProductsAndVariations", objParam);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Packing objPacking = new Packing()
                    {
                        ItemCode = dr["ItemCode"].ToString(),
                        ItemName = dr["Name"].ToString(),
                        Price = Convert.ToDecimal(dr["Price"]),
                        MRP = Convert.ToDecimal(dr["Mrp"]),
                        Unit = dr["Unit"].ToString(),
                        Qty =Convert.ToInt16(dr["Qty"]),
                         
                    };
                    lst.Add(objPacking);
                }
            }


        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }



        return lst;

    }



    public List<Packing> Search(string ProductName)
    {
        List<Packing> lst = new List<Packing>();

        SqlParameter[] objParam = new SqlParameter[1];

        SqlDataReader dr = null;
        objParam[0] = new SqlParameter("@ProductName", ProductName);
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_PackingBelongsGetRelatedProducts", objParam);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Packing objPacking = new Packing()
                    {
                        ItemCode = dr["Item_Code"].ToString(),
                        ItemName = dr["Item_Name"].ToString(),
                        Price = Convert.ToDecimal(dr["Sale_Rate"]),
                        MRP = Convert.ToDecimal(dr["Max_Retail_Price"]),

                    };
                    lst.Add(objPacking);
                }
            }


        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }



        return lst;

    }

    public Int16 Insert(int Cat1,int Cat2,int Cat3,int BrandId,string Name,string SName,string Description,DataTable dt,bool IsSingle)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[10];

  
        objParam[0] = new SqlParameter("@CatLevel1", Cat1);
        objParam[1] = new SqlParameter("@CatLevel2", Cat2);
        objParam[2] = new SqlParameter("@CatLevel3", Cat3);
        objParam[3] = new SqlParameter("@BrandId", BrandId);
        objParam[4] = new SqlParameter("@ProductName", Name);
        objParam[5] = new SqlParameter("@ShortName", SName);
        objParam[6] = new SqlParameter("@Description", Description);
        objParam[7] = new SqlParameter("@VariationList", dt);
        objParam[8] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[8].Direction = ParameterDirection.ReturnValue;
        objParam[9] = new SqlParameter("@IsSingle", IsSingle);
            try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_PackingBelongInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[8].Value);
             
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    //public string Search(string ProductName)
    //{
    //    StringBuilder str = new StringBuilder();
    //    SqlParameter[] objParam = new SqlParameter[1];

    //    SqlDataReader dr = null;
    //    objParam[0] = new SqlParameter("@ProductName", ProductName);
    //    try
    //    {
    //        dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
    //        "shopping_sp_PackingBelongsGetRelatedProducts", objParam);
    //        if (dr.HasRows)
    //        {
    //            while (dr.Read())
    //            {

    //                str.Append("<tr><td><input type='text' id='txtQty' style='width:40px'/></td><td><select id='ddlUnit'  style='width:61px'><option>KG</option>");
    //                str.Append("<option>GRAM</option><option>LITER</option><option>PIECE</option><option>DOZONS</option></select></td><td>");
    //                str.Append("<input type='text' id='txtMrp' value='" + dr["Max_Retail_Price"].ToString() + "' style='width:40px'/></td><td><input type='text' id='txtPrice' value='" + dr["Sale_Rate"].ToString()+ "' style='width:40px'/></td>");
    //                str.Append("<td><select id='ddlType'><option>CARTON</option><option>BOTTLE</option><option>POLY BAG</option><option>POLY PACK</option>");
    //                str.Append("<option>PACKET</option></select></td><td>");
    //                str.Append("<input type='text' id='txtDesc'  style='border:solid 1px silver;width:100px'/></td>");
    //                str.Append("<td><div id='btnDelete' style='cursor:pointer'><a >Delete</a></div></td></tr>");


    //            }
    //        }


    //    }

    //    finally
    //    {
    //        dr.Close();
    //        dr.Dispose();
    //        objParam = null;
    //    }



    //    return str.ToString();

    //}


}