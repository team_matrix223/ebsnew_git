﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class ComboDAL
{
    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_ComboGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public SqlDataReader GetProductDetailByID(Combo objcom)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@ComboId", objcom.ComboId);

        try
        {
            dr = SqlHelper.ExecuteReader(ParamsClass.sqlDataString, CommandType.StoredProcedure, "shopping_sp_ComboDetailGetByID", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }
    public Int16 InsertUpdate(Combo objcombo,DataTable dt)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[9];

        objParam[0] = new SqlParameter("@ComboId", objcombo.ComboId);
        objParam[1] = new SqlParameter("@ComboTypeId", objcombo.ComboTypeId);
        objParam[2] = new SqlParameter("@Title", objcombo.Title);
        objParam[3] = new SqlParameter("@Description", objcombo.Description);
        objParam[4] = new SqlParameter("@Price", objcombo.Price);
        objParam[5] = new SqlParameter("@IsActive", objcombo.IsActive);
        objParam[6] = new SqlParameter("@AdminId", objcombo.AdminId);
        objParam[7] = new SqlParameter("@ComboDetailType", dt);

        objParam[8] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[8].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_ComboInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[8].Value);
            objcombo.ComboId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}