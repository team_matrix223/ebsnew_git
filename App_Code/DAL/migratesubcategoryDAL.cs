﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class migratesubcategoryDAL
{
    public int InsertUpdateSGroups(Int32 GroupId, DataTable dt)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@GroupId", GroupId);
        objParam[1] = new SqlParameter("@GroupType", dt);
        try
        {
            SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
           "shopping_sp_SGroupsInsertUpdate", objParam);
            retValue = 1;

        }
        catch (Exception ex)
        {
            retValue = 0;
        }
        finally
        {
            objParam = null;
        }

        return retValue;

    }
	public   DataSet GetAllDepartment()
    {
        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds=null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_DepartmentsGetAllLevele1", objParam);
        }
        finally
        {
            objParam = null;
        }
        return ds;
    }

    public DataSet GetGroupsByDeptId(Int32  DepartmentId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@DepartmentId", DepartmentId);
        DataSet ds=null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_GroupsGetByDeptId", objParam);
        }
        finally
        {
            objParam = null;
        }
        return ds;
    }

    public DataSet GetSGroupsByGroupId(string  GroupId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@GroupId", GroupId);
        DataSet ds=null;
        try
        {
            ds = SqlHelper.ExecuteDataset(ParamsClass.sqlDataString, CommandType.StoredProcedure,
            "shopping_sp_SGroupsGetByGroupId", objParam);
        }
        finally
        {
            objParam = null;
        }
        return ds;
    }
}