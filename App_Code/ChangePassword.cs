﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class ChangePassword : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string InsertData(Int32 UserId, string Password)
    {


        string retval = "";
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Password", Password);
        objParam[1] = new SqlParameter("@userid", UserId);

        retval = SqlHelper.ExecuteScalar(ParamsClass.sqlDataString, CommandType.StoredProcedure,
                       "shopping_sp_UserChangePassword", objParam).ToString();

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            Result = retval.ToString(),
        };
        return ser.Serialize(JsonData);


    }   
    
}
