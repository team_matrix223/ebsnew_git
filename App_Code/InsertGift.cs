﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class InsertGift : System.Web.Services.WebService {

    [WebMethod, ScriptMethod(UseHttpGet = true, ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public string InsertData(int UserId,int GiftId)
    {

        Int32 retval = 0;
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@UserId", UserId);
        objParam[1] = new SqlParameter("@GiftId", GiftId);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;

        SqlHelper.ExecuteNonQuery(ParamsClass.sqlDataString, CommandType.StoredProcedure,
        "shopping_sp_WalletConsumptionInsert", objParam);
        retval = Convert.ToInt16(objParam[2].Value);


        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            Result = retval,
        };
        return ser.Serialize(JsonData);


    }
    
}
