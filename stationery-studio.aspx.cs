﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class stationery_studio : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetProducts();
        }
    }

    public void GetProducts()
    {

        rpt_getproducts.DataSource = new ProductsBLL().GetAll(192);
        rpt_getproducts.DataBind();
    }
}