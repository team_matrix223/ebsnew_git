﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class registeruser_address : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            BindDeliveryAddress();
        }
    }

        void BindDeliveryAddress()
    {
        Int64 DeliveryAddressId = 0;
        ltDeliveryAddress.Text = new DeliveryAddressBLL().GetHtmlByUserId(Convert.ToInt64(Session[Constants.UserId]), out DeliveryAddressId);
        if (string.IsNullOrEmpty( ltDeliveryAddress.Text))
        {
            btnsubmit.Visible = false;
            btnadd_address.Visible = true;
        }

        hdn_addressid.Value = DeliveryAddressId.ToString();
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        Int64 DeliveryAddressId = CommonFunctions.IsNumeric(hdn_addressid.Value);
        UserCartMst UCM = new UserCartMst()
        {
            SessionId = Session.SessionID,
            DeliveryDate = Convert.ToDateTime(DateTime.Now),
            DeliverySlot = "",
            UserId = Convert.ToInt64(Session[Constants.UserId]),
            PaymentMode = "",
            DeliveryAddressId = DeliveryAddressId
        };
        new UserCartMstBLL().InsertUpdate(UCM);
        Response.Redirect("paymentmode.aspx");
    }

    protected void btnadd_address_Click(object sender, EventArgs e)
    {

        Response.Redirect("/user/dashboard.aspx");
    }
}