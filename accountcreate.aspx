﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="accountcreate.aspx.cs" Inherits="accountcreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="customcss/accountcreate.css" rel="stylesheet" />
	<div class="middle-rail column-span24">
		
<span data-component="accountCreate"></span>

<main id="mainContent" class="accountSignUp_container">


<h1 class="accountSignUp_title ">About You </h1>

	<div class="alert alert-danger" runat="server" id="dv_alert" visible="false" role="alert">
  <asp:Label ID="lblMsg" runat="server" Text="" Font-Bold="true"></asp:Label>
</div>
	<fieldset class="accountSignUp_fieldset">
	<legend class="accountSignUp_legend visually-hidden">Create Account</legend>
	<div class="accountSignUp_cardRow accountSignUp_cardRow-center ">
	<div class="accountSignUp_cardWrapper">
	<div class="accountSignUp_card">
	<div id="account-payment-card-details" class="accountSignUp_list">
			<div class="accountSignUp_listItem">
                <div class="col-md-6 col-sm-6 col-xs-12">
				<label for="customerName" class="accountSignUp_label">First Name:</label>
				<div class="m-unit-main">
					<input id="firstName" autocomplete="off" required="required" runat="server" name="customerName" class="accountSignUp_input sessioncamexclude" type="text" value="" maxlength="64">
				</div>
                    </div>
                      <div class="col-md-6 col-sm-6 col-xs-12">
				<label for="customerName" class="accountSignUp_label">Last Name:</label>
				<div class="m-unit-main">
					<input id="lastName" required="required" autocomplete="off" runat="server" name="lastName" class="accountSignUp_input sessioncamexclude" type="text" value="" maxlength="64">
				</div>
                    </div>
			</div>
	<div class="accountSignUp_listItem">
		<div class="col-md-12 col-sm-12 col-xs-12">		
			<label for="customerEmail" class="accountSignUp_label">Email Address:</label>
			<div class="m-unit-main">
				<input id="customerEmail" autocomplete="off" required="required" runat="server" name="customerEmail" type="email" class="accountSignUp_input sessioncamexclude" value="">
				
			</div>
		</div>

	</div>
<%--		<div class="accountSignUp_listItem">
			<label for="confirmCustomerEmail" class="accountSignUp_label">Confirm Email:</label>
			<div class="m-unit-main">
				<input id="confirmCustomerEmail" name="confirmCustomerEmail" type="email" class="accountSignUp_input sessioncamexclude" value="">
				
			</div>
		</div>--%>
		<div class="accountSignUp_listItem">
			<div class="col-md-12 col-sm-12 col-xs-12">		
				<label for="customerPassword" class="accountSignUp_label">Password:</label>
				<div class="m-unit-main"> 
					<input id="customerPassword" required="required" runat="server" name="customerPassword" class="accountSignUp_input" data-e2e="passwordField" data-show-password-target="true" type="password" value="">
				</div>
			</div>
		</div>
		<div class="accountSignUp_listItem">
			<div class="col-md-12 col-sm-12 col-xs-12">		
				<p class="accountSignUp_labelPassword">
					Must be a minimum of 6 characters long.
				</p>
			</div>
<%--			<div class="section">
				<label class="showPasswordToggleComponent" data-component="showPasswordToggleComponent" data-hidden-message="Password is now hidden" data-visible-message="Password is now visible">
Show Password
<input type="checkbox" class="showPasswordToggleComponent_checkbox">
<span class="showPasswordToggleComponent_switch showpass"></span>
</label>

			</div>--%>

		</div>
<%--		<div class="accountSignUp_listItem">
			<label for="confirmPassword" class="accountSignUp_label">Confirm Password:</label>
			<div class="m-unit-main">
				<input id="confirmPassword" name="confirmPassword" class="accountSignUp_input" data-show-password-target="true" type="password" value="">
				
			</div>
		</div>--%>

        
            <input type="hidden" name="presentSetId" value="1">
            <div class="accountSignUp_listItem">
				<div class="col-md-12 col-sm-12 col-xs-12">		
                <label for="mobileNumber" class="accountSignUp_label">Mobile Number:</label>
                <div class="m-unit-main">
                    <input id="mobileNumber" maxlength="10" autocomplete="off" runat="server" required name="mobileNumber" class="accountSignUp_input" pattern="^[0123456789 ()\+]+$" type="tel">
                 
                </div>
                <p class="accountSignUp_label">
                    We will use this number to send occasional promotional messages.
                </p>
					</div>
            </div>

        

		

        

		
			<input type="hidden" name="presentSetId" value="11">
<%--			<div class="accountSignUp_listItem referralsAccountCreation_Code">
				<label for="referrerCode" class="accountSignUp_label">Referral Code</label>
				<div class="m-unit-main">
					
						
							
							
								<input id="referrerCode" name="referrerCode" class="accountSignUp_input" type="text" value="">
							
						
					
				</div>
				<p class="accountSignUp_referrals_code_text">* Your referrals discount is automatically applied at basket</p>
			</div>--%>
		

	</div>
			<div class="accountSignUp_subscriptionPreferences-hidden" data-component="displaySubscriptionPreferences">
				

		<p class="accountSignUp_optOutLabel_RadioButtonsLabel">
			From time to time we would like to send you emails containing:
		</p>

		<div class="accountSignUp_optOut">

				<ul class="accountSignUp_optOutList">
			        
			          <li class="accountSignUp_optOutListItem">
			            Early access to sales
			          </li>
			        

			        
			          <li class="accountSignUp_optOutListItem">
			            Exclusive offers, discounts and gifts
			          </li>
			        

			        
			          <li class="accountSignUp_optOutListItem">
			            Exclusive brand launches
			          </li>
			        
				</ul>
			
						<fieldset>
							<legend class="visually-hidden">
								Opt in for emails
							</legend>
							<label class="accountSignUp_optOutLabel_RadioButtons" data-component-tracked-clicked="" data-context="optIn">
								<input id="OptInReceiveNewsLetterRadio1" name="OptInReceiveNewsLetterRadio" type="radio" value="true"> Yes Please
							</label>
							<label class="accountSignUp_optOutLabel_RadioButtons" data-component-tracked-clicked="" data-context="optOut" path="OptInReceiveNewsLetterRadio">
								<input id="OptInReceiveNewsLetterRadio2" name="OptInReceiveNewsLetterRadio" type="radio" value="false"> No Thanks
							</label>
						</fieldset>
					
				
			
		</div>
		
			</div>
        <asp:Button ID="btnsubmit" runat="server" Text="Continue"  class="accountSignUp_submitButton btn-continue" aria-describedby="create-account-terms-statement" OnClick="btnsubmit_Click"/>

<%--	<input id="returnTo" name="returnTo" type="hidden" value="">
	<input name="isLinkingAccounts" value="" type="hidden">
	<input id="csrfToken" name="csrfToken" type="hidden" value="10402904851274460190">
	<input id="accountLinkingCsrfToken" name="accountLinkingCsrfToken" type="hidden" value="">--%>
	<div class="accountSignUp_instructionsText">
		
	</div>

	</div>
	</div>

	
		<div class="accountSignUp_cardWrapper">
			<div class="accountSignUp_card">
				
					<!-- Social-login-Component -->
					







	
	
		<%--<p class="socialProviderButtons_header">
<span class="socialProviderButtons_headerTextLine">
<span class="socialProviderButtons_headerText">
Or, Continue with
</span>
</span>
</p>--%>

<%--<ul class="socialProviderButtons" data-component="socialProviderButtons">

<style>
.socialProviderButtons_providerButton-facebook {
background-color: #3b5998;
}

.socialProviderButtons_providerButton-facebook:hover {
background-color: #5472b1;
}

.socialProviderButtons_providerButton-facebook .socialProviderButtons_providerButtonTitle {
color: #ffffff;
}
.socialProviderButtons_providerButton-google {
background-color: #ffffff;
}

.socialProviderButtons_providerButton-google:hover {
background-color: #eeeeee;
}

.socialProviderButtons_providerButton-google .socialProviderButtons_providerButtonTitle {
color: #000000;
}

</style>

<li>
<a href="#" class="socialProviderButtons_providerButton socialProviderButtons_providerButton-facebook" data-js-element="button" data-component-tracked-clicked="" data-component-tracked-viewed="" data-social-login-type="facebook" aria-label="Continue with Facebook">
<div class="socialProviderButtons_providerButtonInner">
<div class="socialProviderButtons_providerButtonLogo">
<img src="data:image/svg+xml;base64,PHN2ZyBjbGFzcz0ic29jaWFsTG9naW5fc3ZnSWNvbi1mYWNlYm9vayIgd2lkdGg9IjIwIiBoZWlnaHQ9IjIwIiB2aWV3Qm94PSIwIDAgMjkgMjkiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTSAxNy45OTYsMzJMIDEyLDMyIEwgMTIsMTYgbC00LDAgbDAtNS41MTQgbCA0LTAuMDAybC0wLjAwNi0zLjI0OEMgMTEuOTkzLDIuNzM3LCAxMy4yMTMsMCwgMTguNTEyLDBsIDQuNDEyLDAgbDAsNS41MTUgbC0yLjc1NywwIGMtMi4wNjMsMC0yLjE2MywwLjc3LTIuMTYzLDIuMjA5bC0wLjAwOCwyLjc2bCA0Ljk1OSwwIGwtMC41ODUsNS41MTRMIDE4LDE2TCAxNy45OTYsMzJ6IiBmaWxsPSJ3aGl0ZSI+PC9wYXRoPjwvc3ZnPg==" class="socialProviderButtons_providerButtonLogoImage" alt="" role="presentation">
</div>
<span class="socialProviderButtons_providerButtonTitle">
Facebook
</span>
</div>
</a>
</li>
<li>
<a href="#" class="socialProviderButtons_providerButton socialProviderButtons_providerButton-google" data-js-element="button" data-component-tracked-clicked="" data-component-tracked-viewed="" data-social-login-type="google" aria-label="Continue with Google">
<div class="socialProviderButtons_providerButtonInner">
<div class="socialProviderButtons_providerButtonLogo">
<img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4bWxuczpza2V0Y2g9Imh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaC9ucyIgdmlld0JveD0iMCAwIDE4IDE4IiB2ZXJzaW9uPSIxLjEiPgogIDxnIGlkPSJHb29nbGUtQnV0dG9uIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4KICAgIDxwYXRoIGQ9Ik0xNy42NCw5LjIwNDU0NTQ1IEMxNy42NCw4LjU2NjM2MzY0IDE3LjU4MjcyNzMsNy45NTI3MjcyNyAxNy40NzYzNjM2LDcuMzYzNjM2MzYgTDksNy4zNjM2MzYzNiBMOSwxMC44NDUgTDEzLjg0MzYzNjQsMTAuODQ1IEMxMy42MzUsMTEuOTcgMTMuMDAwOTA5MSwxMi45MjMxODE4IDEyLjA0NzcyNzMsMTMuNTYxMzYzNiBMMTIuMDQ3NzI3MywxNS44MTk1NDU1IEwxNC45NTYzNjM2LDE1LjgxOTU0NTUgQzE2LjY1ODE4MTgsMTQuMjUyNzI3MyAxNy42NCwxMS45NDU0NTQ1IDE3LjY0LDkuMjA0NTQ1NDUgTDE3LjY0LDkuMjA0NTQ1NDUgWiIgZmlsbD0iIzQyODVGNCIvPgogICAgPHBhdGggZD0iTTksMTggQzExLjQzLDE4IDEzLjQ2NzI3MjcsMTcuMTk0MDkwOSAxNC45NTYzNjM2LDE1LjgxOTU0NTUgTDEyLjA0NzcyNzMsMTMuNTYxMzYzNiBDMTEuMjQxODE4MiwxNC4xMDEzNjM2IDEwLjIxMDkwOTEsMTQuNDIwNDU0NSA5LDE0LjQyMDQ1NDUgQzYuNjU1OTA5MDksMTQuNDIwNDU0NSA0LjY3MTgxODE4LDEyLjgzNzI3MjcgMy45NjQwOTA5MSwxMC43MSBMMC45NTcyNzI3MjcsMTAuNzEgTDAuOTU3MjcyNzI3LDEzLjA0MTgxODIgQzIuNDM4MTgxODIsMTUuOTgzMTgxOCA1LjQ4MTgxODE4LDE4IDksMTggTDksMTggWiIgZmlsbD0iIzM0QTg1MyIvPgogICAgPHBhdGggZD0iTTMuOTY0MDkwOTEsMTAuNzEgQzMuNzg0MDkwOTEsMTAuMTcgMy42ODE4MTgxOCw5LjU5MzE4MTgyIDMuNjgxODE4MTgsOSBDMy42ODE4MTgxOCw4LjQwNjgxODE4IDMuNzg0MDkwOTEsNy44MyAzLjk2NDA5MDkxLDcuMjkgTDMuOTY0MDkwOTEsNC45NTgxODE4MiBMMC45NTcyNzI3MjcsNC45NTgxODE4MiBDMC4zNDc3MjcyNzMsNi4xNzMxODE4MiAwLDcuNTQ3NzI3MjcgMCw5IEMwLDEwLjQ1MjI3MjcgMC4zNDc3MjcyNzMsMTEuODI2ODE4MiAwLjk1NzI3MjcyNywxMy4wNDE4MTgyIEwzLjk2NDA5MDkxLDEwLjcxIEwzLjk2NDA5MDkxLDEwLjcxIFoiIGZpbGw9IiNGQkJDMDUiLz4KICAgIDxwYXRoIGQ9Ik05LDMuNTc5NTQ1NDUgQzEwLjMyMTM2MzYsMy41Nzk1NDU0NSAxMS41MDc3MjczLDQuMDMzNjM2MzYgMTIuNDQwNDU0NSw0LjkyNTQ1NDU1IEwxNS4wMjE4MTgyLDIuMzQ0MDkwOTEgQzEzLjQ2MzE4MTgsMC44OTE4MTgxODIgMTEuNDI1OTA5MSwwIDksMCBDNS40ODE4MTgxOCwwIDIuNDM4MTgxODIsMi4wMTY4MTgxOCAwLjk1NzI3MjcyNyw0Ljk1ODE4MTgyIEwzLjk2NDA5MDkxLDcuMjkgQzQuNjcxODE4MTgsNS4xNjI3MjcyNyA2LjY1NTkwOTA5LDMuNTc5NTQ1NDUgOSwzLjU3OTU0NTQ1IEw5LDMuNTc5NTQ1NDUgWiIgZmlsbD0iI0VBNDMzNSIvPgogIDwvZz4KPC9zdmc+" class="socialProviderButtons_providerButtonLogoImage" alt="" role="presentation">
</div>
<span class="socialProviderButtons_providerButtonTitle">
Google
</span>
</div>
</a>
</li>


</ul>--%>

	
		<div id="create-account-terms-statement" data-component="activatePopUp" data-more-info-on-hover-target-modifiers="combined,privacy">
			<p class="accountSignUp_termsAndConditions">By proceeding, you are confirming that you agree to our
				<a class="accountSignUp_termsAndConditionsLink" target="_blank" rel="noopener" href="#">Terms and Conditions</a>
				and
				<a class="accountSignUp_termsAndConditionsLink" target="_blank" rel="noopener" href="#">Privacy Policy</a>
			</p>
		</div>
	
				
			</div>
		</div>
	
	</div>
	</fieldset>


<%--<form method="post" action="password.reset?action=send" class="createAccount_hiddenForgotPasswordForm" id="hidden-forgot-password-form">
	<input name="elysium_username" class="input-white sessioncamexclude auto-forgot-password-username text" type="hidden" value="" id="forgot-password">
	<input type="hidden" name="csrf_token" value="10402904851274460190">
	<button type="submit" class="submit auto-forgot-password-button">Reset password</button>
</form>--%>

</main>
	
					</div>

	<script>
		$(document).ready(function () {
			var pass_val = 0;
			$(".showpass").click(function () {
				
				if (pass_val == 0) {
					$("#customerPassword").attr("type", "text");
					pass_val = 1;
				}
				else {
					$("#customerPassword").attr("type", "password");
					pass_val = 0;
				}

			});
		});
	</script>
</asp:Content>

