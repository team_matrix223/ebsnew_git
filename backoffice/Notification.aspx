﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="Notification.aspx.cs" Inherits="backoffice_Notification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
<script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

        <script type ="text/javascript">

            $(document).ready(
        function () {
            GetNotification();

            function GetNotification() {
                $.ajax({
                    type: "POST",
                    data: '',
                    url: "default.aspx/GetNotify",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);
                        $("#dvNotify").html(obj.Notify);


                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {



                    }

                });




            }

        }
        );

    </script>

    <style type="text/css">
 table{margin:0px;}
 
    td, th
 {
     padding:0px;
 }
 
 #tbNotification tr
 {
     
     border-bottom:dashed 1px silver;
     }
     
     table tr
     {
     border-bottom:solid 1px silver;    
     }
    </style>

<div id="content">
       			<div id="rightnow">
             

                <div class="youhave" style="padding-left: 30px">
                <div class="col-md-12">
                <h2 style="text-align:center;color:brown">NOTIFICATIONS</h2>
                <div id ="dvNotify"></div>
                </div>

              </div>     

			   </div>
               
            </div>
</asp:Content>

