﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;

public partial class backoffice_managepictureorders : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {


            hdnDate.Value = DateTime.Now.ToShortDateString();
            BindCategories();
        }
    }

    void BindCategories()
    {

        ddlCategories.DataSource = new CategoryBLL().GetByLevel(1);
        ddlCategories.DataValueField = "CategoryId";
        ddlCategories.DataTextField = "Title";
        ddlCategories.DataBind();
        ListItem li = new ListItem();
        li.Text = "--Catgory Level 1--";
        li.Value = "0";
        ddlCategories.Items.Insert(0, li);
    }



    [WebMethod]
    public static string InsertUpdate(int PictureOrderid, int CustomerId, DateTime OrderDate, decimal BillValue, Int32 DeliveryAddressId, DateTime DeliveryDate, string DeliverySlot, string productidArr, string productnameArr, string qtyArr, string priceArr, string variationArr)
    {
        Order objOrder = new Order()
        {
            PictureOrderId = PictureOrderid,
            CustomerId = CustomerId,
            OrderDate = OrderDate,
            BillValue = BillValue,
            DeliveryAddressId = DeliveryAddressId,
            DeliveryDate = DeliveryDate,
            DeliverySlot = DeliverySlot,
        };

        string[] ProductId = productidArr.Split(',');
        string[] ProductName = productnameArr.Split(',');
        string[] Qty = qtyArr.Split(',');
        string[] Price = priceArr.Split(',');
        string[] VariationId = variationArr.Split(',');


        DataTable dt = new DataTable();
        dt.Columns.Add("ProductId");
        dt.Columns.Add("ProductName");
        dt.Columns.Add("Qty");
        dt.Columns.Add("Price");
        dt.Columns.Add("VariationId");






        for (int i = 0; i < ProductId.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["ProductId"] = Convert.ToInt32(ProductId[i]);
            dr["ProductName"] = Convert.ToString(ProductName[i]);
            dr["Qty"] = Convert.ToDecimal(Qty[i]);
            dr["Price"] = Convert.ToDecimal(Price[i]);
            dr["VariationId"] = Convert.ToInt32(VariationId[i]);


            dt.Rows.Add(dr);
        }



        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new PictureOrderBLL().InsertUpdate(objOrder, dt);
        var JsonData = new
        {

            Status = status
        };
        return ser.Serialize(JsonData);
    }
}