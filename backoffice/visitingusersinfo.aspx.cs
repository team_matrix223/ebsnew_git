﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_vistingusersinfo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindControls();
        }

    }
    public void BindControls()
    {
        string TodayUsers = "";
        repMonthWiseUsers.DataSource = new VisitingUsersBLL().GetAllUsersMonthWise(out TodayUsers); 
        repMonthWiseUsers.DataBind();
        ltTodayUsers.Text =TodayUsers;
        repYearWiseUsers.DataSource = new VisitingUsersBLL().GetAllUsersYearWise();
        repYearWiseUsers.DataBind();
    }
}