﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_staticcategoryproduct : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if(!IsPostBack)
        //{
      
        //}
    }


    public void BindProductGrid(int type, string brandtype, string search)
    {
        gv_products.DataSource = new ProductsBLL().GethomeProductAdmin("get", 0, "", "", "", search);
        gv_products.DataBind();

        gtv_productlist.DataSource = new ProductsBLL().GetHomeListAdmin("list", 0, "", dd_ssid.SelectedValue);
        gtv_productlist.DataBind();

    }

    protected void dd_ssid_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dd_ssid.SelectedValue!="0")
        {
            div_products.Visible = true;
            BindProductGrid(0, "", "");

        }
        else
        {
            div_products.Visible = false;
        }
   
    }
    protected void OnPageIndexChanging_products(object sender, GridViewPageEventArgs e)
    {

        gv_products.PageIndex = e.NewPageIndex;
        BindProductGrid(0, "", "");
    }


    protected void gv_products_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "AddProduct")
        {
           int productid =Convert.ToInt32(e.CommandArgument);
            new ProductsBLL().InsertHomeProductsAdmin(productid, "insert", 0, "", dd_ssid.SelectedValue);

            if (!string.IsNullOrEmpty(txtautofill_products.Text))
            {
                BindProductGrid(0, "", txtautofill_products.Text.Trim());
            }
            else
            {
                BindProductGrid(0, "", "");
            }
        
        }
    }

    protected void btn_search_Click(object sender, EventArgs e)
    {
        BindProductGrid(0, "",txtautofill_products.Text.Trim());
    }

    protected void gtv_productlist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "DelProduct")
        {
            int productid = Convert.ToInt32(e.CommandArgument);
            new ProductsBLL().InsertHomeProductsAdmin(productid, "delete", 0, "", dd_ssid.SelectedValue);

            if (!string.IsNullOrEmpty(txtautofill_products.Text))
            {
                BindProductGrid(0, "", txtautofill_products.Text.Trim());
            }
            else
            {
                BindProductGrid(0, "", "");
            }

        }
    }
}