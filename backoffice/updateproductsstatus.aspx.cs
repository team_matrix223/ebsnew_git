﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_updaeproductsstatus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string Update(string  ProductIds)
    {
        string[] ProductData = ProductIds.Split(',');
        DataTable dt = new DataTable();
        dt.Columns.Add("ProductId");

        for (int i = 0; i < ProductData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["ProductId"] = Convert.ToInt32(ProductData[i]); ;
         

            dt.Rows.Add(dr);

        }
    
       int status = new ProductsBLL().UpdateStatus(dt);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            Status = status
        };
        return ser.Serialize(JsonData);
    }
}