﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;

public partial class backoffice_manageusers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string InsertUpdate(int UserId,bool Newsletter)
    {


        Users objusers = new Users()
        {
            UserId = UserId,
            NewsLetter = Newsletter,
        };

        JavaScriptSerializer ser = new JavaScriptSerializer();
       
        int status = new UsersBLL().UpdateNewlettersubscription(objusers);
        var JsonData = new
        {

            Status = status,
           
        };
        return ser.Serialize(JsonData);



    }


    [WebMethod]
    public static string UpdateActive(int UserId, bool IsActive)
    {


        Users objusers = new Users()
        {
            UserId = UserId,
            IsActive = IsActive,
        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new UsersBLL().UpdateUserActive(objusers);
        var JsonData = new
        {

            Status = status,

        };
        return ser.Serialize(JsonData);



    }
}