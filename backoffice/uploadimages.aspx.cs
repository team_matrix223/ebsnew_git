﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_uploadimages : System.Web.UI.Page
{
    public static string LastImage= "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCategory();
            bindgridWithImages();
        }

    }
    public void BindCategory()
    {
        ddlCategory.DataSource = new CategoryBLL().GetByLevel(1);
        ddlCategory.DataTextField = "Title";
        ddlCategory.DataValueField = "CategoryId";
        ddlCategory.DataBind();
        ddlCategory.Items.Insert(0, "--Select Category--");

    }
    public void BindProducts(Int32 CategoryId)
    {
        ddlProduct.DataSource = new ProductsDAL().GetAll(CategoryId );
        ddlProduct.DataTextField = "Name";
        ddlProduct.DataValueField = "ProductId";
        ddlProduct.DataBind();
        ddlProduct.Items.Insert(0, "--Select Product--");
       
    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindgrid();

    }
    public void bindgridWithImages()
    {
        if (txtSearch.Text.Trim() != "" && txtSearch.Text.Trim() != string.Empty)
        {
            GridView2.DataSource = new ProductsBLL().GetProductsWithImagesByName(txtSearch.Text );
            GridView2.DataBind();
        }
        else
        {
            GridView2.DataSource = new ProductsBLL().GetProductsWithImages();
            GridView2.DataBind();
        }
    }
    public void bindgrid()
    {
        if (ddlProduct.SelectedValue != "0")
        {
            GridView1.DataSource = new ProductsBLL().GetVariationsProductsByProductId(Convert.ToInt32(ddlProduct.SelectedValue.ToString()));
            GridView1.DataBind();
        }
    }
  
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
            foreach (GridViewRow row in GridView1.Rows)
            {
                Label lblVarId = (Label)row.FindControl("lblId");
                Int64 VariationId = Convert.ToInt64(lblVarId.Text);
                FileUpload flimage = (FileUpload)row.FindControl("FileUpload1");
                if (flimage.HasFile==false && row.RowIndex==0)
                {
                    Response.Write("<script>alert('First Choose Image');</script>");
                    return;
                }
                string image = "";
                if (row.RowIndex != 0)
                {
                    if (flimage.HasFile == false)
                    {
                        image = LastImage;
                    }
                    else
                    {
                        image = CommonFunctions.UploadImage(flimage, "~/ProductImages/", true, 150, 150, false, 0, 0);
                    }

                }
                else
                {
                    image = CommonFunctions.UploadImage(flimage, "~/ProductImages/", true, 150, 150, false, 0, 0);
                    LastImage = image;
                }               
                Int16 results = new ProductsBLL().InsertVariations(Convert.ToInt32(ddlProduct.SelectedValue.ToString()), VariationId, image);
            }
            Response.Write("<script>alert('Information is saved');</script>");
            btnReset_Click(null,null);
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        bindgridWithImages();
        //ddlCategory.SelectedIndex = 0;
       // ddlProduct.Text = "--Select--";
        ddlProduct.SelectedIndex = 0;
        GridView1.DataSource = null;
        GridView1.DataBind();
        
    }

    protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView2.EditIndex = e.NewEditIndex;
        bindgridWithImages();
    }
    protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        Label VId = (Label)GridView2.Rows[e.RowIndex].FindControl("lblVId");
        Label PId = (Label)GridView2.Rows[e.RowIndex].FindControl("lblPId");
        Label lblPhoto = (Label)GridView2.Rows[e.RowIndex].FindControl("lblPhotoUrl");
    
        FileUpload flimage = (FileUpload)GridView2.Rows[e.RowIndex].FindControl("FLUpload_Update");
        if (flimage.HasFile == false )
        {
            Response.Write("<script>alert('First Choose Image');</script>");
            return;
        }
        string path = Server.MapPath("../ProductImages/T_" + lblPhoto.Text);
        if (System.IO.File.Exists(path) == true)
        {
      //      System.IO.File.Delete(path);
        }
        string path1 = Server.MapPath("../ProductImages/" + lblPhoto.Text);
        if (System.IO.File.Exists(path1) == true)
        {
          //  System.IO.File.Delete(path1);
        }
        string image = CommonFunctions.UploadImage(flimage, "~/ProductImages/", true, 150, 150, false, 0, 0);
        Int16 results = new ProductsBLL().InsertVariations(Convert.ToInt32(PId.Text), Convert.ToInt32(VId.Text), image);
        GridView2.EditIndex = -1;
        bindgridWithImages();
    }
    protected void GridView2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView2.EditIndex = -1;
        bindgridWithImages();
    }
    protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView2.PageIndex = e.NewPageIndex;
        bindgridWithImages();
    }
    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {
        bindgridWithImages();
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCategory.SelectedIndex > 0)
        {
            BindProducts(Convert.ToInt32(ddlCategory.SelectedValue.ToString()));
        }
    }
}