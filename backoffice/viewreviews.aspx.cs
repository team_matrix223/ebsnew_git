﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;

public partial class backoffice_viewreviews : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

         
            
        }
    }

    [WebMethod]

    public static string Insert(string arrReview)
    {
       
        string[] ReviewData = arrReview.Split(',');
       
        DataTable dt = new DataTable();
        dt.Columns.Add("ReviewId");
     
        for (int i = 0; i < ReviewData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["ReviewId"] = Convert.ToInt32(ReviewData[i]); ;
         

            dt.Rows.Add(dr);

        }


        JavaScriptSerializer ser = new JavaScriptSerializer();

        int st = new ReviewsBLL().InsertReviewsApproval(dt);
        var JsonData = new
        {
         
           ReviewApproval = st,

        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]

    public static string InsertDisApprove(string arrReview)
    {

        string[] ReviewData = arrReview.Split(',');

        DataTable dt = new DataTable();
        dt.Columns.Add("ReviewId");

        for (int i = 0; i < ReviewData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["ReviewId"] = Convert.ToInt32(ReviewData[i]); ;


            dt.Rows.Add(dr);

        }


        JavaScriptSerializer ser = new JavaScriptSerializer();

        int st = new ReviewsBLL().InsertReviewsDisApproval(dt);
        var JsonData = new
        {

            ReviewApproval = st,

        };
        return ser.Serialize(JsonData);
    }

}