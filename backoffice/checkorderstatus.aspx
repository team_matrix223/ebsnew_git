﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="checkorderstatus.aspx.cs" Inherits="backoffice_checkorderstatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">

 <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
            <link href="css/custom-css/transaction.css" rel="stylesheet" />

    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

    <script type="text/javascript">

        var rdbbill = "";
        var OrderId = 0;
        var BillId = 0
        var m_BillNo = 0;
        var OrderId = 0;
        var Category = 0;
        var Unit1 = "";
        var Unit2 = "";
        var Unit3 = "";

        var ProductCollection = [];
        function clsProduct() {
            this.ProductId = 0;
            this.ProductName = 0;
            this.Qty = 0;
            this.Price = 0;
            this.VariationId = 0;
            this.ProductDescription = "";
            this.SubTotal = 0;
        }
        function Reset() {

            $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            $('#tbProductVariation tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            $("#dvOrderno").html('');
            $("#dvOrderDate").html('');
            $("#dvCustomer").html('');
            $("#dvOrderAmount").html('');
            $("#dvRecepient").html('');
            $("#dvRecepientMobile").html();
            $("#dvRecepientPhone").html();
            $("#dvAddress").html();
            $("#dvDeliverySlot").html();
            $("div[id='dvsbtotal']").html();
            $("div[id='dvdelivery']").html();
            $("div[id='dvnetAmount']").html();
            $("#txtOrder_No").val();
            
        }


        function BindTempOrders() {

            ProductCollection = [];

            $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

            $.ajax({
                type: "POST",
                data: '{ "Oid": "' + OrderId + '"}',
                url: "checkorderstatus.aspx/GetTemparoryOrders",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    ProductCollection = [];


                    var obj = jQuery.parseJSON(msg.d);

                    var tr = "";
                    var Total = 0;
                    for (var i = 0; i < obj.TempOrders.length; i++) {

                        var schemeid = obj.TempOrders[i]["SchemeId"];
                        if (schemeid == 0) {
                            tr = tr + "<tr><td><img src='../ProductImages/T_" + obj.TempOrders[i]["PhotoUrl"] + "' style='width:50px;height:50px'/></td><td>" + obj.TempOrders[i]["ProductDescription"] + "</td><td><input type='text' style ='width:35px' id='q_" + obj.TempOrders[i]["ProductId"] + "'  value =" + obj.TempOrders[i]["Qty"] + " readonly='readonly' /></td><td>" + obj.TempOrders[i]["Amount"] + "</td></tr>";
                            Total = Number(Total) + Number(obj.TempOrders[i]["Amount"]);
                        }
                        else {
                            tr = tr + "<tr><td><img src='../ProductImages/T_" + obj.TempOrders[i]["PhotoUrl"] + "' style='width:50px;height:50px'/></td><td>" + obj.TempOrders[i]["ProductDescription"] + "</td><td><input type='text' style ='width:20px' id='q_" + obj.TempOrders[i]["ProductId"] + "'  value =" + obj.TempOrders[i]["Qty"] + " readonly='readonly' /></td><td>" + obj.TempOrders[i]["Price"] + "</td><td>" + obj.TempOrders[i]["Amount"] + "</td><td><img src='../img/free.gif' /></td></tr>";
                        }

                        var PO = new clsProduct();
                        PO.ProductId = obj.TempOrders[i]["ProductId"];
                        PO.ProductName = obj.TempOrders[i]["ProductName"];
                        PO.ProductDescription = obj.TempOrders[i]["ProductDescription"];
                        PO.Qty = obj.TempOrders[i]["Qty"];
                        PO.Price = obj.TempOrders[i]["Price"];
                        PO.VariationId = obj.TempOrders[i]["VariationId"];
                        ProductCollection.push(PO);
                    }

                    $("div[id='dvsbtotal']").html(" Rs. " + Total);
                    $("div[id='dvdelivery']").html(" Rs. " + 20);
                    $("div[id='dvnetAmount']").html(" Rs. " + Number(Number(Total) + Number(20)));
                    $("#tbProductInfo").append(tr);

                },
                complete: function (msg) {

                    
                }

            });
        }




        $(document).ready(
        function () {




            $("#btnCheck").click(
        function () {

            if ($("#rdbOrder").prop("checked") == true) {
                rdbbill = "OrderNo";
            }
            else {
                rdbbill = "BillNo";
            }

            if (rdbbill == "OrderNo") {
                OrderId = $("#txtOrder_No").val();

                $.ajax({

                    type: "POST",
                    data: '{"OrderId":"' + OrderId + '"}',
                    url: "checkorderstatus.aspx/GetByOrderId",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {


                        var obj = jQuery.parseJSON(msg.d);




                        if (obj.Orders.OrderId != 0) {


                            $("#dvOrderno").html(obj.Orders.OrderId);
                            $("#dvOrderDate").html(obj.Orders.strOD);
                            $("#dvCustomer").html(obj.Orders.CustomerName);
                            $("#dvOrderAmount").html(obj.Orders.BillValue);
                            $("#dvRecepient").html(obj.Orders.RecipientFirstName);
                            $("#dvRecepientMobile").html(obj.Orders.RecipientMobile);
                            $("#dvRecepientPhone").html(obj.Orders.RecipientPhone);
                            $("#dvAddress").html(obj.Orders.CompleteAddress);
                            $("#dvDeliverySlot").html(obj.Orders.DeliverySlot);

                            $("#dvStatus").html(obj.Orders.Status);
                            //                        $("#<%=ltOrderNo.ClientID%>").val(obj.Orders.OrderId);
                            //                        $("#<%=ltOrderDate.ClientID%>").val(obj.Orders.strOD);

                            //                        $("#<%=ltCustomer.ClientID%>").val(obj.Orders.CustomerName);
                            //                        $("#<%=ltRecepientname.ClientID%>").val(obj.Orders.RecipientFirstName);

                            //                        $("#<%=ltRecepientMobile.ClientID%>").val(obj.Orders.RecipientMobile);
                            //                        $("#<%=ltRecepientPhone.ClientID%>").val(obj.Orders.RecipientPhone);

                            //                        $("#<%=ltAddress.ClientID%>").val(obj.Orders.CompleteAddress);
                            //                        $("#<%=ltOrderAmount.ClientID%>").val(obj.Orders.BillValue);
                            //                        $("#<%=ltDeliverySlot.ClientID%>").val(obj.Orders.DeliverySlot);


                        }
                        else {
                            $("#txtOrder_No").val('').focus();

                            alert("Invalid OrderNo");
                        }

                    },
                    complete: function () {

                        alert(OrderId);
                        $.ajax({
                            type: "POST",
                            data: '{ "OI": "' + OrderId + '"}',
                            url: "checkorderstatus.aspx/InsertTemp",
                            contentType: "application/json",
                            dataType: "json",
                            success: function (msg) {



                                var obj = jQuery.parseJSON(msg.d);

                            },
                            error: function (xhr, ajaxOptions, thrownError) {

                                var obj = jQuery.parseJSON(xhr.responseText);
                                alert(obj.Message);
                            },
                            complete: function () {

                                BindTempOrders();
                            }

                        });
                    }

                });
            }
            else if (rdbbill == "BillNo") {


                BillId = $("#txtOrder_No").val();
                alert(BillId);
                $.ajax({

                    type: "POST",
                    data: '{"BillId":"' + BillId + '"}',
                    url: "checkorderstatus.aspx/GetByBillId",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {


                        var obj = jQuery.parseJSON(msg.d);



                        OrderId = obj.Bill.OrderId;
                        alert(obj.Bill.Status);
                        if (obj.Bill.OrderId != 0) {


                            $("#dvOrderno").html(obj.Bill.OrderId);
                            $("#dvOrderDate").html(obj.Bill.strOD);
                            $("#dvCustomer").html(obj.Bill.CustomerName);
                            $("#dvOrderAmount").html(obj.Bill.BillValue);
                            $("#dvRecepient").html(obj.Bill.RecipientFirstName);
                            $("#dvRecepientMobile").html(obj.Bill.RecipientMobile);
                            $("#dvRecepientPhone").html(obj.Bill.RecipientPhone);
                            $("#dvAddress").html(obj.Bill.CompleteAddress);
                            $("#dvDeliverySlot").html(obj.Bill.DeliverySlot);

                            $("#dvStatus").html(obj.Bill.Status);

                            //                        $("#<%=ltOrderNo.ClientID%>").val(obj.Orders.OrderId);
                            //                        $("#<%=ltOrderDate.ClientID%>").val(obj.Orders.strOD);

                            //                        $("#<%=ltCustomer.ClientID%>").val(obj.Orders.CustomerName);
                            //                        $("#<%=ltRecepientname.ClientID%>").val(obj.Orders.RecipientFirstName);

                            //                        $("#<%=ltRecepientMobile.ClientID%>").val(obj.Orders.RecipientMobile);
                            //                        $("#<%=ltRecepientPhone.ClientID%>").val(obj.Orders.RecipientPhone);

                            //                        $("#<%=ltAddress.ClientID%>").val(obj.Orders.CompleteAddress);
                            //                        $("#<%=ltOrderAmount.ClientID%>").val(obj.Orders.BillValue);
                            //                        $("#<%=ltDeliverySlot.ClientID%>").val(obj.Orders.DeliverySlot);


                        }
                        else {
                            $("#txtOrder_No").val('').focus();

                            alert("Invalid OrderNo");
                        }

                    },
                    complete: function () {

                        alert(OrderId);
                        $.ajax({
                            type: "POST",
                            data: '{ "OI": "' + OrderId + '"}',
                            url: "checkorderstatus.aspx/InsertTemp",
                            contentType: "application/json",
                            dataType: "json",
                            success: function (msg) {



                                var obj = jQuery.parseJSON(msg.d);

                            },
                            error: function (xhr, ajaxOptions, thrownError) {

                                var obj = jQuery.parseJSON(xhr.responseText);
                                alert(obj.Message);
                            },
                            complete: function () {

                                BindTempOrders();
                            }

                        });
                    }

                });
            }

        }
      );






            $("#btnCancel").click(
        function () {

            $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            $('#tbProductVariation tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            $("#<%=ltOrderNo.ClientID%>").val("");
            $("#<%=ltOrderDate.ClientID%>").val("");
            $("#<%=ltCustomer.ClientID%>").val("");
            $("#<%=ltAddress.ClientID%>").val("");
            $("#<%=ltOrderAmount.ClientID%>").val("");
            $("#<%=ltRecepientname.ClientID%>").val("");
            $("#<%=ltRecepientPhone.ClientID%>").val("");
            $("#<%=ltRecepientMobile.ClientID%>").val("");
            $("#<%=ltDeliverySlot.ClientID%>").val("");




        }
        );



        });
    
    </script>



     <form   runat="server" id="formID" method="post">

     <asp:HiddenField ID="hdnDate" runat="server"/>
  
<div id="content">

             <div id="rightnow">
            <h3 class="reallynow">
                <span>Order Status</span>
                <br />
            </h3>
            <div class="youhave">
                            <table class="top_table" style="width:100% !important;">
                               <tr><td>  <label><input type="radio" name = "RadioButton" value= "Order" id="rdbOrder">OrderNo</label></td>
                                   <td><label><input type="radio" name = "RadioButton" value= "Bill" id="rdbBill">BillNo</label></td>
                               </tr>
                                <tr>
                                    <td><input type="text" class="form-control input-small"  id="txtOrder_No" /></td>
                                    <td><div id="btnCheck" class="btn btn-primary btn-small"  >Check</div></td>                           
                                </tr>
                               </table>


                        

                    <table cellpadding ="5" class="manage_category_table">
                         <tr>
                         <td colspan="100%"><h3>Bill Information</h3></td>
                         </tr>
                      <tr>
                     <td>Order No:</td><td><div id="dvOrderno"><asp:Literal ID="ltOrderNo" runat ="server"></asp:Literal></div></td>
                     </tr>
                     <tr>
                     <td>Order Date:</td><td><div id="dvOrderDate"><asp:Literal ID="ltOrderDate" runat ="server"></asp:Literal></div></td>
       
                     </tr>
                   

                     <tr>
                      <td>Customer Name:</td><td><div id ="dvCustomer"><asp:Literal ID="ltCustomer" runat ="server"></asp:Literal></div></td>
                       
                       </tr>
                         <tr>
                     <td>Order Amount:</td><td><div id="dvOrderAmount"><asp:Literal ID="ltOrderAmount" runat ="server"></asp:Literal></div></td>
                    
                     </tr>
                      <tr>
                      
                       <td>STATUS:</td><td><div id ="dvStatus"><asp:Literal ID="ltStatus" runat ="server"></asp:Literal></div></td><td></td>
                      
                      </tr>
                      </table>           


                     <table class="manage_category_table" cellpadding="1" style="float:right;" >
                         <tr style="background-color:#E6E6E6;font-weight:bold">
                         <td colspan="100%"><h3>Customer Information</h3></td>
                         </tr>
                    <tr>
                    <td colspan="100%">
                     

                    <table>
                       
                      <tr><td>Recepient Name:</td><td colspan="100%"><div id ="dvRecepient"><asp:Literal ID="ltRecepientname" runat ="server"></asp:Literal></div></td><td></td></tr>
                    <tr><td>Address:</td><td colspan="100%"><div id ="dvAddress"><asp:Literal ID="ltAddress" runat ="server"></asp:Literal></div></td><td></td></tr>
                    <tr><td>Recepient Mobile:</td><td colspan="100%"><div id ="dvRecepientMobile"><asp:Literal ID="ltRecepientMobile" runat ="server"></asp:Literal></div></td><td></td></tr>
                    <tr><td>Recepient Phone:</td><td colspan="100%"><div id ="dvRecepientPhone"><asp:Literal ID="ltRecepientPhone" runat ="server"></asp:Literal></div></td><td></td></tr>
                     <tr><td>Delivery Slot:</td><td colspan="100%"><div id ="dvDeliverySlot"><asp:Literal ID="ltDeliverySlot" runat ="server"></asp:Literal></div></td><td></td></tr>
                    </table>
                    
                    </td>
                    </tr>
                   
                     </table>
                      
                     <table class="table table-bordered table-striped table-hover item_table" style="width:100%; margin-top:20px;" id="tbProductInfo">
										<thead>
											<tr>
                                                	<th>
                                            Image
                                        </th>
												<th>
                                            Description
                                        </th>
                                     
                                        <th>
                                            Qty
                                        </th>
                                      
                                          <th>
                                            Price
                                        </th>
                                         <th>
                                            Amount
                                        </th>
                                        <th></th>
                                           

											</tr>
										</thead>
										 
										
										</table>

                     <table>
                     <tr><td >Gross Amount:</td><td><div id ="dvsbtotal"></div></td></tr>

                     <tr><td>Delivery Charges:</td><td><div id ="dvdelivery"></div></td></tr>
                     <tr><td >Net Amount:</td><td><div id ="dvnetAmount"></div></td></tr>
                    
                     </table>

                     <table class="category_table">
                    
                      <tr><td>  <div id="btnCancel" class="btn btn-primary btn-small"  >Cancel</div></td></tr>
                   
                   
                     </table>

            </div>

        </div>

            </div>



  



</form>

</asp:Content>

