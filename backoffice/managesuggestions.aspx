﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="managesuggestions.aspx.cs" Inherits="backoffice_managesuggestions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">

  <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
            <link href="css/custom-css/master.css" rel="stylesheet" />

    
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   
<script language="javascript" type="text/javascript">

    $(document).ready(
        function () {

            BindGrid();

        });

</script>


 <form   runat="server" id="formID" method="post">

     <asp:HiddenField ID="hdnDate" runat="server"/>
  
<div id="content" style="padding:6px">

             <div id="rightnow" style="margin-top:10px">
                    <h3 class="reallynow">
                        <span>Manage Enquiries </span>
                      
                        <br />
                    </h3>
				    <div class="youhave" style="padding-left:30px">
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>
			  </div>



              <div id="Popup"  style="display:none;">

  <table>
  <tr>
  <td style="width:150px" >
             <asp:Label ID= "lblEnquiry" runat="server"></asp:Label> 
            </td>
  </tr>
 </table>        


</div>
   
            </div>




  
</form>


 <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/manageenquiries.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Id', 'PersonName', 'EmailId','Enquiry','MobileNo','DOC'],
                        colModel: [
                                    { name: 'EnquiryId', key: true, index: 'EnquiryId', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
                                    { name: 'PersonName', index: 'PersonName', width: 200, stype: 'text', sortable: true, editable: true,hidden: false, editrules: { required: true } },
                                      { name: 'EmailId', index: 'EmailId', width: 200, stype: 'text', sortable: true, editable: true,hidden: false, editrules: { required: true } },
                                     { name: 'Enquiry', index: 'Enquiry', width: 200, stype: 'text', sortable: true, editable: true,hidden: true, editrules: { required: true } },

                                       { name: 'MobileNo', index: 'MobileNo', width: 200, stype: 'text', sortable: true, editable: true,hidden: false, editrules: { required: true } },
                                        { name: 'strOD', index: 'strOD', width: 200, stype: 'text', sortable: true, editable: true,hidden: false, editrules: { required: true } },
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'CityId',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Enquiries List",

                        editurl: 'handlers/manageenquiries.ashx',



                    });




                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                  

                  
                    $("#<%=lblEnquiry.ClientID %>").html($('#jQGridDemo').jqGrid('getCell', rowid, 'Enquiry'));
                     $('#Popup').dialog(
            {
            autoOpen: false,

            width:300,
            height:150,
            resizable: false,
            modal: true,
  
            });
            linkObj = $(this);
            var dialogDiv = $('#Popup');
            dialogDiv.dialog("option", "position", [400, 200]);
            dialogDiv.dialog('open');
            return false;
                  
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>





</asp:Content>

