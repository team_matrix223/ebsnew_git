﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="notficationorderdetail.aspx.cs" Inherits="backoffice_notficationorderdetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
<script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

     <script type="text/javascript">

         var rdbbill = "";
         var OrderId = 0;
         var BillId = 0
         var m_BillNo = 0;
         var OrderId = 0;
         var Category = 0;
         var Unit1 = "";
         var Unit2 = "";
         var Unit3 = "";

         var ProductCollection = [];
         function clsProduct() {
             this.ProductId = 0;
             this.ProductName = 0;
             this.Qty = 0;
             this.Price = 0;
             this.VariationId = 0;
             this.ProductDescription = "";
             this.SubTotal = 0;
         }
         

         function BindTempOrders() {

             ProductCollection = [];

             $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

             $.ajax({
                 type: "POST",
                 data: '{ "Oid": "' + OrderId + '"}',
                 url: "notficationorderdetail.aspx/GetTemparoryOrders",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     ProductCollection = [];


                     var obj = jQuery.parseJSON(msg.d);

                     var tr = "";
                     var Total = 0;
                     for (var i = 0; i < obj.TempOrders.length; i++) {

                         var schemeid = obj.TempOrders[i]["SchemeId"];
                         if (schemeid == 0) {
                             tr = tr + "<tr><td><img src='../ProductImages/T_" + obj.TempOrders[i]["PhotoUrl"] + "' style='width:50px;height:50px'/></td><td>" + obj.TempOrders[i]["ProductDescription"] + "</td><td><input type='text' style ='width:35px' id='q_" + obj.TempOrders[i]["ProductId"] + "'  value =" + obj.TempOrders[i]["Qty"] + " readonly='readonly' /></td></tr>";
                             Total = Number(Total) + Number(obj.TempOrders[i]["Amount"]);
                         }
                         else {
                             tr = tr + "<tr><td><img src='../ProductImages/T_" + obj.TempOrders[i]["PhotoUrl"] + "' style='width:50px;height:50px'/></td><td>" + obj.TempOrders[i]["ProductDescription"] + "</td><td><input type='text' style ='width:20px' id='q_" + obj.TempOrders[i]["ProductId"] + "'  value =" + obj.TempOrders[i]["Qty"] + " readonly='readonly' /></td><td>" + obj.TempOrders[i]["Price"] + "</td><td><img src='../img/free.gif' /></td></tr>";
                         }

                         var PO = new clsProduct();
                         PO.ProductId = obj.TempOrders[i]["ProductId"];
                         PO.ProductName = obj.TempOrders[i]["ProductName"];
                         PO.ProductDescription = obj.TempOrders[i]["ProductDescription"];
                         PO.Qty = obj.TempOrders[i]["Qty"];
                         PO.Price = obj.TempOrders[i]["Price"];
                         PO.VariationId = obj.TempOrders[i]["VariationId"];
                         ProductCollection.push(PO);
                     }

                     $("div[id='dvsbtotal']").html(" Rs. " + Total);
                     $("div[id='dvdelivery']").html(" Rs. " + 20);
                     $("div[id='dvnetAmount']").html(" Rs. " + Number(Number(Total) + Number(20)));
                     $("#tbProductInfo").append(tr);

                 },
                 complete: function (msg) {


                 }

             });
         }




         $(document).ready(
        function () {

            OrderId = $("#<%=hdnId.ClientID %>").val();

            $.ajax({

                type: "POST",
                data: '{"OrderId":"' + OrderId + '"}',
                url: "notficationorderdetail.aspx/GetByOrderId",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);




                    if (obj.Orders.OrderId != 0) {


                        $("#dvOrderno").html(obj.Orders.OrderId);
                        $("#dvOrderDate").html(obj.Orders.strOD);
                        $("#dvCustomer").html(obj.Orders.CustomerName);
                        $("#dvOrderAmount").html(obj.Orders.BillValue);
                        $("#dvRecepient").html(obj.Orders.RecipientFirstName);
                        $("#dvRecepientMobile").html(obj.Orders.RecipientMobile);
                        $("#dvRecepientPhone").html(obj.Orders.RecipientPhone);
                        $("#dvAddress").html(obj.Orders.CompleteAddress);
                        $("#dvDeliverySlot").html(obj.Orders.DeliverySlot);

                        $("#dvStatus").html(obj.Orders.Status);
                        //                        $("#<%=ltOrderNo.ClientID%>").val(obj.Orders.OrderId);
                        //                        $("#<%=ltOrderDate.ClientID%>").val(obj.Orders.strOD);

                        //                        $("#<%=ltCustomer.ClientID%>").val(obj.Orders.CustomerName);
                        //                        $("#<%=ltRecepientname.ClientID%>").val(obj.Orders.RecipientFirstName);

                        //                        $("#<%=ltRecepientMobile.ClientID%>").val(obj.Orders.RecipientMobile);
                        //                        $("#<%=ltRecepientPhone.ClientID%>").val(obj.Orders.RecipientPhone);

                        //                        $("#<%=ltAddress.ClientID%>").val(obj.Orders.CompleteAddress);
                        //                        $("#<%=ltOrderAmount.ClientID%>").val(obj.Orders.BillValue);
                        //                        $("#<%=ltDeliverySlot.ClientID%>").val(obj.Orders.DeliverySlot);


                    }
                    else {
                        $("#txtOrder_No").val('').focus();

                        alert("Invalid OrderNo");
                    }

                },
                complete: function () {


                    $.ajax({
                        type: "POST",
                        data: '{ "OI": "' + OrderId + '"}',
                        url: "notficationorderdetail.aspx/InsertTemp",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {



                            var obj = jQuery.parseJSON(msg.d);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {

                            BindTempOrders();
                        }

                    });
                }

            });
    


        });
    
    </script>


        <form   runat="server" id="formID" method="post">

     <asp:HiddenField ID="hdnId" runat="server" Value="0"/>

<div id="content" style="padding:6px">

             <div id="rightnow" style="margin-top: 10px">
           
            <div class="youhave" style="padding-left: 30px">
             
           <table width="100%" cellpadding="3" >
                    

                     <tr>
                     <td valign="top">
                     <table style="width:100%">
                     <tr>
                     <td valign="top">
                     <table  cellpadding="0" style="border:solid 1px silver; width:100%;height:172px">
                         <tr style="background-color:#E6E6E6;font-weight:bold;height:10px">
                         <td colspan="100%">Order Information</td>
                         </tr>
                         
                    <tr>
                    <td colspan="100%" valign="top">
                     

                    <table cellpadding ="5">
                      <tr>
                     <td align="right">Order No:</td><td><div id="dvOrderno"><asp:Literal ID="ltOrderNo" runat ="server"></asp:Literal></div></td>
                     </tr>
                     <tr>
                     <td align="right">Order Date:</td><td><div id="dvOrderDate"><asp:Literal ID="ltOrderDate" runat ="server"></asp:Literal></div></td>
       
                     </tr>
                   

                     <tr>
                      <td align="right">Customer Name:</td><td><div id ="dvCustomer"><asp:Literal ID="ltCustomer" runat ="server"></asp:Literal></div></td>
                       
                       </tr>
                         <tr>
                     <td align="right">Order Amount:</td><td><div id="dvOrderAmount"><asp:Literal ID="ltOrderAmount" runat ="server"></asp:Literal></div></td>
                    
                     </tr>
                      <tr>
                      
                       <td align="right" >STATUS:</td><td><div id ="dvStatus"><asp:Literal ID="ltStatus" runat ="server"></asp:Literal></div></td><td></td>
                      
                      </tr>
                      </table>

                      </td>
   
                     </tr>
                    
                     

                    
                    
                    
                    
                    </table>
                    
                    </td>
                    </tr>
                   
                     </table>
                  </td>
                    
                     <td  valign="top">
                     
                     <table  cellpadding="1" style="border:solid 1px silver;width:100%" >
                         <tr style="background-color:#E6E6E6;font-weight:bold">
                         <td colspan="100%">Customer Information</td>
                         </tr>
                    <tr>
                    <td colspan="100%">
                     

                    <table>
                       
                      <tr><td style="text-align:right">Recepient Name:</td><td colspan="100%"><div id ="dvRecepient"><asp:Literal ID="ltRecepientname" runat ="server"></asp:Literal></div></td><td></td></tr>
                    <tr><td style="text-align:right">Address:</td><td colspan="100%"><div id ="dvAddress"><asp:Literal ID="ltAddress" runat ="server"></asp:Literal></div></td><td></td></tr>
                    <tr><td style="text-align:right">Recepient Mobile:</td><td colspan="100%"><div id ="dvRecepientMobile"><asp:Literal ID="ltRecepientMobile" runat ="server"></asp:Literal></div></td><td></td></tr>
                    <tr><td style="text-align:right">Recepient Phone:</td><td colspan="100%"><div id ="dvRecepientPhone"><asp:Literal ID="ltRecepientPhone" runat ="server"></asp:Literal></div></td><td></td></tr>
                     <tr><td style="text-align:right">Delivery Slot:</td><td colspan="100%"><div id ="dvDeliverySlot"><asp:Literal ID="ltDeliverySlot" runat ="server"></asp:Literal></div></td><td></td></tr>
                    </table>
                    
                    </td>
                    </tr>
                   
                     </table>
                     </td>

                     </tr>
                    
                     </table>
                     <table>
                     <tr>
                     <td colspan="100%">
                  

                     
                      
                     <table class="table table-bordered table-striped table-hover" style="width:100%" id="tbProductInfo">
										<thead>
											<tr>
                                                	<th style="width: 150px">
                                            Image
                                        </th>
												<th style="width: 150px">
                                            Description
                                        </th>
                                     
                                        <th style="width: 35px">
                                            Qty
                                        </th>
                                      
                                          <th style="width: 80px">
                                            Price
                                        </th>
                                        
                                        <th style="width: 50px"></th>
                                           

											</tr>
										</thead>
										 
										
										</table>

                      
                     
                      
                     
                     
                     </td>
                     </tr>

                     <tr>
                     <td  >

                    <table>
                    <tr>
                    
                    <td valign="top"><table>
                     <tr>
                     <td valign="top">
                     <table>
                     <tr><td >Gross Amount:</td><td><div id ="dvsbtotal"></div></td></tr>

                     <tr><td>Delivery Charges:</td><td><div id ="dvdelivery"></div></td></tr>
                     <tr><td >Net Amount:</td><td><div id ="dvnetAmount"></div></td></tr>
                    
                     </table>
                     </td>
                   
                        
                     </tr>

                     </table></td>
                    </tr>
                    </table> 
                     </td>
                     </tr>
                     <tr>
                    
                     
                    
                     
                     </tr>
                    </table>
            </div>

        </div>

            </div>



  



</form>


</asp:Content>

