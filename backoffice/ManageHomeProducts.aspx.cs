﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_ManageHomeProducts : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetType();

        }

    }


    public void GetType()
    {

        using (SqlConnection con = new SqlConnection(ParamsClass.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select type_id,name from tbl_filter_type where isactive=1", con);
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adb.Fill(dt);
            dd_type.DataSource = dt;
            dd_type.DataValueField = "type_id";
            dd_type.DataTextField = "name";
            dd_type.DataBind();
            ListItem l1 = new ListItem();
            l1.Text = "--Select Type--";
            l1.Value = "0";
            dd_type.Items.Insert(0, l1);

        }
    }


    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        Int32 type = Convert.ToInt32(dd_type.SelectedValue);
        string brandtype = dd_brandtype.SelectedValue;
        txtautofill_products.Text = "";
        if (type == 5)
        {
            div_products.Visible = false;
            div_brands.Visible = true;
            BindBrandGirid(type, brandtype, "");
            dv_search.Visible = true;
        }
        else
        {
            div_products.Visible = true;
            div_brands.Visible = false;
            BindProductGrid(type, brandtype, "");
            dv_search.Visible = true;
        }

        DisplyContent();

    }

    public void BindProductGrid(int type, string brandtype, string search)
    {
        gv_products.DataSource = new ProductsBLL().GethomeProductAdmin("get", type, tbfrom.Text, tbto.Text, brandtype,search);
        gv_products.DataBind();

        gtv_productlist.DataSource = new ProductsBLL().GetHomeListAdmin("list", type,brandtype, hdnssid.Value);
        gtv_productlist.DataBind();
        //if (gv_products.Rows.Count > 0)
        //{
        //    dv_search.Visible = true;

        //}
        //else
        //{
        //    dv_search.Visible = true;
        //}
    }
    protected void gv_shopsale_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        div_products.Visible = true;
 
        Int32 type = Convert.ToInt32(dd_type.SelectedValue);
        string brandtype = dd_brandtype.SelectedValue;

        if (e.CommandName == "AddProduct")
        {
            hdnssid.Value = e.CommandArgument.ToString();
        }
        BindProductGrid(type, brandtype, "");
    }
    public void BindBrandGirid(int type, string brandtype,string search)
    {
        gv_brand.DataSource = new ProductsBLL().GethomeBrandAdmin("get", search);
        gv_brand.DataBind();
        gv_brandlist.DataSource = new ProductsBLL().GetHomeBrandListAdmin("list", brandtype, type);
        gv_brandlist.DataBind();
        if (gv_brand.Rows.Count > 0)
        {
            dv_search.Visible = true;

        }
        else
        {
            dv_search.Visible = true;
        }


    }
    protected void OnPageIndexChanging_products(object sender, GridViewPageEventArgs e)
    {
        Int32 type = Convert.ToInt32(dd_type.SelectedValue);
        string brandtype = dd_brandtype.SelectedValue;
        gv_products.PageIndex = e.NewPageIndex;
        this.BindProductGrid(type, brandtype,"");
    }
    protected void OnPageIndexChanging_brands(object sender, GridViewPageEventArgs e)
    {
        Int32 type = Convert.ToInt32(dd_type.SelectedValue);
        string brandtype = dd_brandtype.SelectedValue;
        gv_brand.PageIndex = e.NewPageIndex;
        this.BindBrandGirid(type, brandtype,"");
    }
    [WebMethod]
    public static void insert_product(int productid, string req, string type, int type_id,string ssid)
    {
        new ProductsBLL().InsertHomeProductsAdmin(productid, req, type_id, type, ssid);
    }
    [WebMethod]
    public static void insert_brand(int brandid, string req, string type, int type_id)
    {
        new ProductsBLL().InsertHomeBrandsAdmin(brandid, req, type, type_id);
    }

    [WebMethod]
    public static void delete_product(int bsid, string req,int type_id)
    {
        new ProductsBLL().DeleteHomeProductsAdmin(req, bsid,type_id);
    }

    [WebMethod]
    public static void delete_brand(int bid, string req)
    {
        new ProductsBLL().DeleteHomeBrandAdmin(req, bid);
    }

    protected void dd_type_SelectedIndexChanged(object sender, EventArgs e)
    {

        DisplyContent();
    }


    public void DisplyContent()
    {
        dv_search.Visible = true;
      
        string type = dd_type.SelectedValue;
        btnsubmit.Visible = true;

        if (type == "5")
        {
            dd_brandtype.Enabled = true;
            tbfrom.Visible = false;
            tbto.Visible = false;
            div_brands.Visible = true;
            div_products.Visible = false;
            div_ss.Visible = false;

        }
        else if (type == "7" || type == "3" || (type == "4" && dd_brandtype.SelectedValue == "SL"))
        {
            dd_brandtype.Enabled = false;
            tbfrom.Visible = false;
            tbto.Visible = false;
            div_brands.Visible = false;
            div_products.Visible = false;
            div_ss.Visible = true;
            btnsubmit.Visible = false;
            dv_search.Visible = false;

            if (type=="3")
            {
                tr_offeron.Visible = true;
            }

            else if (type == "7")
            {
                dv_search.Visible = true;
            }
            else
            {
                tr_offeron.Visible = false;
            }
            GetShopSaleList();

        }
        else
        {
            if (type == "4")
            {
                dd_brandtype.Enabled = true;

            }
            else
            {
                dd_brandtype.Enabled = false;
            }
            div_brands.Visible = false;
            div_products.Visible = true;
            tbfrom.Visible = true;
            tbto.Visible = true;
            div_ss.Visible = false;
            dd_brandtype.SelectedValue = "S";
            dv_search.Visible = true;
        }

    }

    public void BindCategories()
    {

        ddlCategory.DataSource = new CategoryBLL().GetByLevel(1);
        ddlCategory.DataValueField = "CategoryId";
        ddlCategory.DataTextField = "Title";
        ddlCategory.DataBind();
        ListItem li = new ListItem();
        li.Text = "--Category--";
        li.Value = "0";
        ddlCategory.Items.Insert(0, li);

        ddl_brand.DataSource = new BrandBLL().GetAll();
        ddl_brand.DataValueField = "BrandId";
        ddl_brand.DataTextField = "Title";
        ddl_brand.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Brand--";
        li1.Value = "0";
        ddl_brand.Items.Insert(0, li1);
    }

    protected void dd_dis_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindCategories();
        ddlCategory.Visible = false;
        ddlSubCat.Visible = false;
        ddl_brand.Visible = false;
        if (dd_offeron.SelectedValue=="C")
        {
            ddlCategory.Visible = true;
        }
      else if (dd_offeron.SelectedValue == "SC")
        {
            ddlCategory.Visible = true;
            ddlSubCat.Visible = true;
        }

        else if (dd_offeron.SelectedValue == "B")
        {
            ddl_brand.Visible = true;
        }
    
    }

    public void BindSubCategories(int ParentId)
    {
        try
        {


            ddlSubCat.DataSource = new CategoryBLL().GetByParentId(ParentId);
            ddlSubCat.DataTextField = "Title";
            ddlSubCat.DataValueField = "CategoryId";
            ddlSubCat.DataBind();
            ListItem li = new ListItem();
            li.Text = "--SubCategory--";
            li.Value = "0";
            ddlSubCat.Items.Insert(0, li);


        }
        finally
        {
        }

    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
       if (dd_offeron.SelectedValue == "SC")
        {

            BindSubCategories(Convert.ToInt32(ddlCategory.SelectedValue));
        }
   
    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        Int32 type = Convert.ToInt32(dd_type.SelectedValue);
        string brandtype = dd_brandtype.SelectedValue;
        if (type == 5)
        {
          
            BindBrandGirid(type, brandtype,txtautofill_products.Text.Trim());

        }
        else
        {
            BindProductGrid(type, brandtype, txtautofill_products.Text.Trim());
        }
    }

    protected void btnsubmit_shopsale_Click(object sender, EventArgs e)
    {
        string imageName = "";
        if (fuImage.HasFile)
        {
       
            imageName = CommonFunctions.UploadImage(fuImage, "~/images/shop_sale/", false, 507, 417, false, 0, 0);
        }
        int catid = ddlCategory.SelectedValue == "" ? 0 : Convert.ToInt32(ddlCategory.SelectedValue);
        int subcatid = ddlSubCat.SelectedValue == "" ? 0 : Convert.ToInt32(ddlSubCat.SelectedValue);
        int brandid = ddl_brand.SelectedValue == "" ? 0 : Convert.ToInt32(ddl_brand.SelectedValue);
            new ProductsBLL().InsertUpdateShopSale("insert", imageName, Convert.ToInt32(chkIsActive.Checked), txtUrlName.Text.Trim(),0,Convert.ToInt32(dd_type.SelectedValue), txtdesc.Text.Trim(),dd_amt_type.SelectedValue,Convert.ToDecimal(tbamt_dis.Text.Trim()), catid, subcatid, brandid);
            GetShopSaleList();


    }
    protected void btnsubmit_shopsale_update_Click(object sender, EventArgs e)
    {
        string imageName = "";
        if (fuImage.HasFile)
        {

            imageName = CommonFunctions.UploadImage(fuImage, "~/images/shop_sale/", false, 507, 417, false, 0, 0);
        }
        int catid = ddlCategory.SelectedValue == "" ? 0 : Convert.ToInt32(ddlCategory.SelectedValue);
        int subcatid = ddlSubCat.SelectedValue == "" ? 0 : Convert.ToInt32(ddlSubCat.SelectedValue);
        int brandid = ddl_brand.SelectedValue == "" ? 0 : Convert.ToInt32(ddl_brand.SelectedValue);
        new ProductsBLL().InsertUpdateShopSale("update", imageName, Convert.ToInt32(chkIsActive.Checked), txtUrlName.Text.Trim(), Convert.ToInt32(hdnssid.Value), Convert.ToInt32(dd_type.SelectedValue), txtdesc.Text.Trim(), dd_amt_type.SelectedValue, Convert.ToDecimal(tbamt_dis.Text.Trim()), catid, subcatid, brandid);

        GetShopSaleList();
        reset();

    }
    protected void btnreset_shopsale_Click(object sender, EventArgs e)
    {
        reset();
    }

    public void reset() {
        hdnssid.Value = "";
        btnsubmit.Text = "Add";
        txtUrlName.Text = "";
        txtdesc.Text = "";
        fuImage.Dispose();
    }
    public void GetShopSaleList() {

        gv_shopsale.DataSource = new ProductsBLL().GetHomeShopSaleList("adminlist", Convert.ToInt32(dd_type.SelectedValue));
        gv_shopsale.DataBind();

    }

    [WebMethod]
    public static void delete_shopsale(int ssid,string req) {


        new ProductsBLL().DeleteShopSale(req, ssid);

    }
}