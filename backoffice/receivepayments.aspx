﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="receivepayments.aspx.cs" Inherits="backoffice_receivepayments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">

<script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
            <link href="css/custom-css/transaction.css" rel="stylesheet" />

    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

    <script type="text/javascript">

        var Employee = 0;
        var OrderCollection = [];
        function clsOrder() {
            this.BillNo = 0;
            this.OrderNo = 0;
            this.NetAmount = 0;
            this.PaymentFeedback = 0;

        }

        function Reset() {
            $("#<%=ddlemployee.ClientID %>").val("0");
            $('#tbOrderInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
        }

        $(document).ready(
        function () {

            $("#btnSave").click(
        function () {

            InsertUpdate();

        }
        );


            $(document).on("keyup", "input[name='txtPay']", function (event) {

                var keycode = (event.keyCode ? event.keyCode : event.which);

                var billId = $(this).attr("bid");

                if (Number($("#Net_" + billId).html()) != Number($(this).val())) {
                    $("#chk_" + billId).prop("checked", false);
                }

            });


            $(document).on("change", "input[name='chkPass']", function () {

                var billId = $(this).attr("bid");

                if (Number($("#Net_" + billId).html()) != Number($("#q_" + billId).val())) {
                    $("#chk_" + billId).prop("checked", false);
                    alert("Passing Failed. Received amount should be equal to Net Amount");
                }
            });



            $("#<%=ddlemployee.ClientID%>").change(function () {

                if ($(this).val() == "0") {

                    jQuery("#jQGridDemo").GridUnload();
                    Employee = 0;
                    return;
                }

                Employee = $("#<%=ddlemployee.ClientID %>").val();

                BindOrders(Employee);

            });




            function BindOrders(Employee) {

                OrderCollection = [];

                $('#tbOrderInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

                $.ajax({
                    type: "POST",
                    data: '{ "Eid": "' + Employee + '"}',
                    url: "receivepayments.aspx/GetOrders",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        OrderCollection = [];


                        var obj = jQuery.parseJSON(msg.d);

                        var tr = "";
                        var Total = 0;
                       
                        for (var i = 0; i < obj.TempOrders.length; i++) {
                         
                         
                            if (obj.TempOrders[i]["PaymentStatus"] == "Success") {


                                tr = tr + "<tr><td style ='width:100px'>" + obj.TempOrders[i]["BillId"] + "</td><td style ='width:100px'><div id='Order_" + obj.TempOrders[i]["BillId"] + "'>" + obj.TempOrders[i]["OrderId"] + "</div></td><td style ='width:100px'><div id='Net_" + obj.TempOrders[i]["BillId"] + "'>" + obj.TempOrders[i]["NetAmount"] + "</div></td><td><input type='text' value='" + obj.TempOrders[i]["NetAmount"] + "' name='txtPay' disabled  style ='width:100px' bid='" + obj.TempOrders[i]["BillId"] + "' id='q_" + obj.TempOrders[i]["BillId"] + "' /></td><td><input type='checkbox' name ='chkPass' style ='width:100px'  bid='" + obj.TempOrders[i]["BillId"] + "'  id='chk_" + obj.TempOrders[i]["BillId"] + "' /></td></tr>";
                            }
                            else {
                                tr = tr + "<tr><td style ='width:100px'>" + obj.TempOrders[i]["BillId"] + "</td><td style ='width:100px'><div id='Order_" + obj.TempOrders[i]["BillId"] + "'>" + obj.TempOrders[i]["OrderId"] + "</div></td><td style ='width:100px'><div id='Net_" + obj.TempOrders[i]["BillId"] + "'>" + obj.TempOrders[i]["NetAmount"] + "</div></td><td><input type='text' name='txtPay' style ='width:100px' bid='" + obj.TempOrders[i]["BillId"] + "' id='q_" + obj.TempOrders[i]["BillId"] + "' /></td><td><input type='checkbox' name ='chkPass' style ='width:100px'  bid='" + obj.TempOrders[i]["BillId"] + "'  id='chk_" + obj.TempOrders[i]["BillId"] + "' /></td></tr>";
                            }

                            var TO = new clsOrder();
                            TO.BillNo = obj.TempOrders[i]["BillId"];
                            TO.OrderNo = obj.TempOrders[i]["OrderId"];
                            TO.NetAmount = obj.TempOrders[i]["NetAmount"];
                            OrderCollection.push(TO);
                        }


                        $("#tbOrderInfo").append(tr);

                    },
                    complete: function (msg) {


                    }

                });
            }


            function InsertUpdate() {

                if (!validateForm("addDialog")) {

                    return;
                }

                $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

                if (OrderCollection.length == 0) {
                    alert("Please first add a Order");

                    return;
                }


                var IsChked = false;

                var BillNo = [];
                var Orders = [];
                var ReceivedAmount = [];

                for (var i = 0; i < OrderCollection.length; i++) {
                    var billid = 0;
                    billid = OrderCollection[i]["BillNo"];


                    if ($("#chk_" + billid).prop('checked') == true) {


                        BillNo[i] = billid;
                        Orders[i] = $("#Order_" + billid).html();

                        ReceivedAmount[i] = $("#q_" + billid).val();
                        IsChked = true;
                    }


                }

                if (IsChked != true) {

                    alert("atleast one order should be there to pass");
                    $.uiUnlock();
                    return;
                }

                var Emp = $("#<%=ddlemployee.ClientID %>").val();
                if (Emp == "0") {
                    alert("Choose Employee");
                    $("#<%=ddlemployee.ClientID %>").focus();
                    $.uiUnlock();
                    return;
                }

                $.ajax({
                    type: "POST",
                    data: '{"BillIdarr": "' + BillNo + '","EmpId": "' + Emp + '","arrOrderNo": "' + Orders + '","arrReceivedAmount": "' + ReceivedAmount + '"}',

                    url: "receivepayments.aspx/Insert",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {


                        var obj = jQuery.parseJSON(msg.d);

                        alert("Payment Collected from Employee Successfully");

                        Reset();


                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                        $.uiUnlock();
                    }

                });
            }


        });

    
    </script>

  <form   runat="server" id="formID" method="post">

     <asp:HiddenField ID="hdnDate" runat="server"/>
  
<div id="content">

             <div id="rightnow">
            <h3 class="reallynow">
                <span>Receive Payments</span>
                <br />
            </h3>
            <div class="youhave">



               <table class="top_table">
                
                   <tr><td>Employee:</td><td><asp:DropDownList ID ="ddlemployee" runat="server" ></asp:DropDownList></td>
                   </tr>
                </table>
                       

                        <table class="tablesorter item_table"  id="tbOrderInfo"  cellspacing="0"> 
			                <thead> 
				                <tr> 
   					                  <th >
                                            BillNo
                                        </th>

                                           <th >
                                          OrderNo
                                        </th>

                                        <th >
                                           Net Amount
                                        </th>
                                        <th  >
                                            Payment FeedBack
                                        </th>
                                        <th>Pass</th>
				                </tr> 
			                </thead> 
			                <tbody>
       
			                </tbody> 
			                </table>
                                             <table cellspacing="0" cellpadding="0" class="category_table">
                                            <tr>
                                             <td> <div id="btnSave"  class="btn btn-primary btn-small" >Receive Payment</div></td>
                                            
                                            <td> <div id="btnPrint"  class="btn btn-primary btn-small" >Print</div></td>
                                            </tr>
                                            </table>
            </div>
        </div>
   
            </div>




  
</form>
</asp:Content>

