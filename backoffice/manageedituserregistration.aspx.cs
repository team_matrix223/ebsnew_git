﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_manageedituserregistration : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillDetail();
        }

    }
    public void FillDetail()
    {
        if (Convert.ToInt32(Session[Constants.UserId]) != 0)
        {
            Users objUsers = new Users() { UserId = Convert.ToInt32(Session[Constants.UserId]) };
            new UsersBLL().GetById(objUsers);

            chkAgree.Checked = true;
            hdnId.Value = objUsers.UserId.ToString();
            txtFirstName.Value = objUsers.FirstName;
            txtLastName.Value = objUsers.LastName;
            chkIsActive.Checked = objUsers.IsActive;


        }
        else
        {
            Response.Write("<script>alert('First SignIn');</script>");
            Response.Redirect("userlogin.aspx");

        }
    }

    public void ResetControls()
    {
        hdnId.Value = "0";
        txtFirstName.Value = "";
        txtLastName.Value = "";
        chkIsActive.Checked = true;

    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {


        if (chkAgree.Checked == false)
        {
            return;
        }
        Users objUser = new Users()
        {
            UserId = Convert.ToInt32(hdnId.Value),
            FirstName = txtFirstName.Value.Trim(),
            LastName = txtLastName.Value.Trim(),
            IsActive = chkIsActive.Checked,
            AdminId = 0,
        };
        int result = new UsersBLL().InsertUpdate(objUser);
        if (result == 0)
        {
            Response.Write("<script>alert('Your profile is not updated,Plz try again')</script>");
        }
        else
        {
            Response.Write("<script>alert('Your Profile is updated successfully')</script>");
            ResetControls();
        }
    }
}