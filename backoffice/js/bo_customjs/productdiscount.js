﻿/// <reference path="../jquery-1.9.0.min.js" />
$(document).ready(function () {
    
    $(document.body).on('click', '.btnedit', function (e) {  

        var currentRow = $(this).closest("tr");
        var dis_id = currentRow.find(".cls_dis_id").text();
        var dis_per = currentRow.find(".cls_dis_per").text();
        var start_date = currentRow.find(".cls_start_date").text();
        var end_date = currentRow.find(".end_date").text();
        $("#hdndisid").val(dis_id);
        $("#txtUrlName").val(url);
        if (IsActive == 1) {
            $('#chkIsActive').prop('checked', true);
        }
        else {
            $('#chkIsActive').prop('checked', false);
        }
        $("#txtUrlName").val(url);

        $("#img_shopsale").attr('src', '../images/shop_sale/' + images)
        $("#shopsale_update").show();
        $("#shopsale_insert").hide();
    
    });
    $(document.body).on('click', '.btndel', function (e) {
        if (!confirm("Do you want to delete?")) {
            return false;
        }
        var currentRow = $(this).closest("tr");
        var type = $("#dd_type").val();
        if (type == "5") {

            var bid = currentRow.find(".cls_bid").text();
            delete_brands(bid);

        }
        else {
            var bsid = currentRow.find(".cls_bsid").text();
            delete_products(bsid);
        }



        $(this).parent().parent().remove();
    });
});