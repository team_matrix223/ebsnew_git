﻿/// <reference path="../jquery-1.9.0.min.js" />

$(document).ready(function () {

    $(document.body).on('click', '.btnedit', function (e) {
        $("#lblerror").text("");
        var currentRow = $(this).closest("tr");
        $("#td_lessstock").show();
        var attr_id = currentRow.find(".cls_attr_id").text().trim();
        var title = currentRow.find(".cls_title").text().trim();
        var add_stock = currentRow.find(".cls_addstock").text().trim(); 
        var less_stock = currentRow.find(".cls_lessstock").text().trim();
        var type_id = currentRow.find(".cls_type_id").text().trim();
        var itemcode_sku = currentRow.find(".cls_itemcode_sku").text().trim();
        var mrp = currentRow.find(".cls_mrp").text().trim();
        var price = currentRow.find(".cls_price").text().trim();

        $("#tbitemcode_sku").val(itemcode_sku);
        $("#tbtitle").val(title);
        $("#tbaddstock").val(add_stock);
        $("#tblessstock").val(less_stock);
        $("#tbmrp").val(mrp);
        $("#tbprice").val(price);
        $("#hdn_attrid").val(attr_id);
        $("#ddl_type").val(type_id);
        $("#btnAdd").val("Update")

        if (title == "No Attribute") {
            $("#tbitemcode_sku").attr('readonly', 'readonly');
            $("#tbtitle").attr('readonly', 'readonly');
            $("#tbmrp").attr('readonly', 'readonly');
            $("#tbprice").attr('readonly', 'readonly');
            $("#ddl_type").attr("style", "pointer-events: none;");

        }
        else {
            $("#tbitemcode_sku").removeAttr('readonly', 'readonly');
            $("#tbtitle").removeAttr('readonly', 'readonly');
            $("#tbmrp").removeAttr('readonly', 'readonly');
            $("#tbprice").removeAttr('readonly', 'readonly');
            $("#ddl_type").removeAttr("style", "pointer-events: none;");
        }


    });
        $(document.body).on('click', '.btndelete', function (e) {
            if (!confirm("Do you want to delete?")) {
                return false;
            }
            var currentRow = $(this).closest("tr");
            var attr_id = currentRow.find(".cls_attr_id").text().trim();

            $.ajax({
                type: "POST",
                url: "manage_attr_stock.aspx/delete",
                contentType: "application/json",
                data: '{"attr_id":"' + attr_id + '"}',
                dataType: "json",
                success: function (data) {

                    alert("Item deleted successully!");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    //$.unblockUI();

                }
                      
            });
            $(this).parent().parent().remove();
        });
   
    $("#ddl_type").change(function () {


        if ($(this).val() == 5) {
            $("#tbtitle").val('No Attribute');
           $("#td_title").hide();
           

        }
        else {

            $("#td_title").show();
            $("#td_title").val('');
        }

    })
});