﻿/// <reference path="../jquery-1.9.0.min.js" />


$(document).ready(function () {



    $(document).on("click", ".cls_itemcode", function (event) {

        var itemcode = $(this).attr('id');
        window.open('/backoffice/manage_attr_stock_pop.aspx?q=' + itemcode, 'mywindow', 'width=10000,height=450')


    });
    $("#txtItemCode").blur(function () {


        var val = $(this).val();
        if (val!="") {

   
        $.ajax({
            type: "POST",
            url: "manageproducts.aspx/CheckItemcode",
            contentType: "application/json",
            data: '{"val":"' + val + '"}',
            dataType: "json",
            success: function (data) {

                var obj = jQuery.parseJSON(data.d);
                if (obj.isavail == 1) {
                    alert("ItemCode Already Exists!")
                    $("#txtItemCode").val('');
                    $("#txtItemCode").focus();

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

                //$.unblockUI();

            }

        });
        }

    })
});