﻿/// <reference path="../../handlers/AutoCompletProducts.asmx" />
/// <reference path="../jquery-1.9.0.min.js" />
$(document).ready(function () {
    
    $(document.body).on('click', '.btnedit', function (e) {

 
        var currentRow = $(this).closest("tr");
        var url = currentRow.find(".cls_url").text();
        var IsActive = currentRow.find(".cls_isactive").text();
        var ssid = currentRow.find(".cls_ssid").text();
        var images = currentRow.find(".cls_image").text();
        var desc = currentRow.find(".cls_desc").text();
        var amt_type = currentRow.find(".cls_amt_type").text();
        var amt = currentRow.find(".cls_amt").text();
  
        $("#hdnssid").val(ssid);
        $("#txtUrlName").val(url);
        $("#txtdesc").val(desc);
        if (IsActive == 1)
        {
            $('#chkIsActive').prop('checked', true);
        }
        else
        {
            $('#chkIsActive').prop('checked', false);
        }
        $("#txtUrlName").val(url); 
        $("#dd_amt_type").val(amt_type);
        $("#tbamt_dis").val(amt);
        $("#img_shopsale").attr('src','../images/shop_sale/'+ images)
        $("#shopsale_update").show();
        $("#shopsale_insert").hide(); 

        $("#tr_offeron").hide();

    });

    $(document.body).on('click', '.btnadd', function (e) {
     
        var type_id = $("#dd_type").val();
        var type_ = $("#dd_brandtype").val();
        var currentRow = $(this).closest("tr");
        if (type_id == "5") {
           
            var brandid = currentRow.find(".cls_brandid").text();
            var count = 0;
     
            if (type_ == "SL" && type_id == 5) {
            count = $(".cls_bid").length;
                }

                if (count == 2) {
                    alert('Only 2 Spotlight Products Are Allowed!')
                    count = 0;
                    return false;
                }
                
           
                insert_brand(type_, brandid, type_id);
           
           
        }
        else {
         if (type_ == "SL" && type_id == 4) {
             count = $(".cls_bsid").length;
         }
         if (count == 1) {
             alert('Only 1 Spotlight Product Is Allowed!')
             count = 0;
             return false;
         }
            var productid = currentRow.find(".productid").text();
            insert_product(type_id, type_, productid);
        }
 
        $("#btn_search").click();
    });
    function insert_brand(type, brandid,type_id) {

       
      
        var count = 0;
        if (type_id == "SL" && type_ == 5) {
            count= $(".cls_bid").length;
        }
   
        $.ajax({
            type: "POST",
            url: "ManageHomeProducts.aspx/insert_brand",
            contentType: "application/json",
            data: '{"brandid":"' + brandid + '","req":"insert","type":"' + type + '","type_id":"' + type_id + '"}',
            dataType: "json",
            success: function (data) {
                alert("Brand added successully!");
                $("#btnsubmit").click();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

                //$.unblockUI();

            }
        });

    }
    function insert_product(type_id,type, productid) {

       
 
        $.ajax({
            type: "POST",
            url: "ManageHomeProducts.aspx/insert_product",
            contentType: "application/json",
            data: '{"productid":"' + productid + '","req":"insert","type":"' + type + '","type_id":"' + type_id + '","ssid":"' + $("#hdnssid").val() + '"}',
            dataType: "json",
            success: function (data) {
                alert("Product added successully!");
                $("#btnsubmit").click();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

                //$.unblockUI();

            }
        });

    }
    $(document.body).on('click', '.btndel', function (e) {
        if (!confirm("Do you want to delete?")) {
            return false;
        }
        var currentRow = $(this).closest("tr");
        var type = $("#dd_type").val();
        if (type == "5") {
            
            var bid = currentRow.find(".cls_bid").text();
            delete_brands(bid);
           
        }
        else {
            var bsid = currentRow.find(".cls_bsid").text();
            delete_products(bsid);
        }
       
   

        $(this).parent().parent().remove();
    });

    $(document.body).on('click', '.btndel_shopsale', function (e) {
        if (!confirm("Do you want to delete?")) {
            return false;
        }
        var currentRow = $(this).closest("tr");

        var ssid = currentRow.find(".cls_ssid").text();

        $.ajax({
            type: "POST",
            url: "ManageHomeProducts.aspx/delete_shopsale",
            contentType: "application/json",
            data: '{"ssid":"' + ssid + '","req":"delete"}',
            dataType: "json",
            success: function (data) {

                alert("Item deleted successully!");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

                //$.unblockUI();

            }

        });


        $(this).parent().parent().remove();
    });
    function delete_products(bsid) {

        $.ajax({
            type: "POST",
            url: "ManageHomeProducts.aspx/delete_product",
            contentType: "application/json",
            data: '{"bsid":"' + bsid + '","req":"delete","type_id":"' + $("#dd_type").val() + '"}',
            dataType: "json",
            success: function (data) {

                alert("Product deleted successully!");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

                //$.unblockUI();

            }

        });

    }

    function delete_brands(bid) {

        $.ajax({
            type: "POST",
            url: "ManageHomeProducts.aspx/delete_brand",
            contentType: "application/json",
            data: '{"bid":"' + bid + '","req":"delete"}',
            dataType: "json",
            success: function (data) {

                alert("Product deleted successully!");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

                //$.unblockUI();

            }

        });

    }


    function BrandType() {

        var brandtype = $("#dd_brandtype").val();
        if (brandtype == 'S') {
            $("#div_s").show();
            $("#div_sr").hide();
        }
        else if (brandtype == 'SL') {
            $("#div_s").hide();

            $("#div_sl").show()
        }
        else {
            $("#div_s").hide();
            $("#div_sl").hide()
        }

    }


    var timer = '';

    $("#txtautofill_products").keypress(function () {
  clearTimeout(timer);
  timer = setTimeout(function() {
    $("#txtautofill_products").autocomplete({
        source: function (request, responce) {
            if (request.term.length>1) {
                var date_from = $("#tbfrom").val();
                var date_to = $("#tbto").val();
                var type = $("#dd_type").val();
                if (type == "5") {
                    $.ajax({

                        url: "handlers/AutoCompletProducts.asmx/GetBrands",
                        method: "post",
                        contentType: "application/json;charset=utf-8",
                        data: JSON.stringify({ term: request.term}),
                        dataType: 'json',
                        success: function (data) {
                            responce(data.d);
                        },
                        error: function (err) {
                            alert(err);
                        }
                    });
                }
                else
                {
            $.ajax({
           
                url: "handlers/AutoCompletProducts.asmx/GetProducts",
                method: "post",
                contentType: "application/json;charset=utf-8",
                data: JSON.stringify({ term: request.term, date_from: date_from, date_to: date_to }),
                dataType: 'json',
                success: function (data) {
                    responce(data.d);
                },
                error: function (err) {
                    alert(err);
                }
            });
                }
            }
        }
    });
  }, 1000);
    });



   
})