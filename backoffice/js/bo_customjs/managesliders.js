﻿/// <reference path="../vendor/jquery-3.2.1.min.js" />
$(document).ready(function () {

    $(document.body).on('click', '.btndel_addedslider', function (e) {
        if (!confirm("Do you want to delete?")) {
            return false;
        }
        var currentRow = $(this).closest("tr");
        var sliderid = currentRow.find(".cls_sliderid").text();
        delete_slider(sliderid);
        $(this).parent().parent().remove();
    });


    function delete_slider(sliderid)
    {

        $.ajax({
            type: "POST",
            url: "managesliders.aspx/delete_slider",
            contentType: "application/json",
            data: '{"sliderid":"' + sliderid + '","req":"delete"}',
            dataType: "json",
            success: function (data) {

                alert("Item deleted successully!");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

                //$.unblockUI();

            }

        });

    }

})