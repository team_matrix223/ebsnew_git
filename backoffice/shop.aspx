﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="shop.aspx.cs" Inherits="backoffice_shop" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">


     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />

  <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>
              <script src="js/jquery.uilock.js" type="text/javascript"></script>
   <style type="text/css">
         
        .ui-jqgrid .ui-userdata { height: auto; }
        .ui-jqgrid .ui-userdata div { margin: .1em .5em .2em;}
        .ui-jqgrid .ui-userdata div>* { vertical-align: middle; }
    </style>
 

<style type="text/css">
.addrHeading
{
    font-weight:bold;
    text-align:right
    
}
.customInput
{
    width:120px;
    }
</style>
<script language="javascript" type="text/javascript">

    $(document).on("change", "input[name='rbAddress']", function (event) {

        alert($(this).closest("tr").index());
    });

    $(document).ready(
    function () {

        $("#<%=ddlCategories.ClientID%>").change(
        function () {

            BindGridProducts();

        }
        );
        function GetUserData(Mobile) {

            $.ajax({
                type: "POST",
                data: '{"MobileNo":"' + Mobile + '"}',
                url: "shop.aspx/GetUserInfo",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    $("#txtFirstName").val(obj.User.FirstName)
                    $("#txtLastName").val(obj.User.LastName)
                    $("#txtEmail").val(obj.User.EmailId)

                    var tr = "";
                    if (obj.lstAddress.length > 0) {

                        m_CustomerId = obj.User.UserId;

                        $("#txtFirstName,#txtLastName,#txtEmail").prop("readonly", true);

                        for (var i = 0; i < obj.lstAddress.length; i++) {

                            tr = tr + "<tr><td>";
                            tr = tr + "<table>";
                            tr = tr + "<tr><td class='addrHeading'>Recipient Name:</td><td >" + obj.lstAddress[i].RecipientFirstName + " " + obj.lstAddress[i].RecipientLastName + "</td></tr>";
                            tr = tr + "<tr><td class='addrHeading'>Mobile:</td><td colspan='100%'><table style='margin:0px' cellspacing='0' cellpadding='0'><tr><td>" + obj.lstAddress[i].MobileNo + "</td><td class='addrHeading'>Telephone:</td><td>" + obj.lstAddress[i].Telephone + "</td></tr></table></td></tr>";
                            tr = tr + "<tr><td class='addrHeading'>City:</td><td colspan='100%'><table  style='margin:0px' cellspacing='0' cellpadding='0'><tr><td>" + obj.lstAddress[i].CityName + "</td><td class='addrHeading'>PinCode:</td><td>" + obj.lstAddress[i].PinCode + "</td></tr></table></td></tr>";
                            tr = tr + "<tr><td class='addrHeading'>Area:</td><td>" + obj.lstAddress[i].Area + "</td></tr>";
                            tr = tr + "<tr><td class='addrHeading'>Address:</td><td>" + obj.lstAddress[i].Address + "</td></tr>";
                            tr = tr + "</table></td>";


                            tr = tr + "<td><input type='radio' name='rbAddress' /></td></tr>";

                        }


                        $("#dvAddressList").html(tr);

                    }
                    else {
                        $("#txtFirstName,#txtLastName,#txtEmail").prop("readonly", false);

                        $("#dvAddressList").html("Please add Delivery Address");

                    }



                    $('#dvCustomerPopup').dialog(
                {
                    autoOpen: false,

                    width: 800,
                    resizable: false,
                    modal: true,
                    buttons: {

                    }
                });



                    var dialogDiv = $('#dvCustomerPopup');
                    dialogDiv.dialog("option", "position", [150, 50]);
                    dialogDiv.dialog('open');
                    return false;



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                    $.uiUnlock();
                }
            });

        }

        $("#btnGo").click(
        function () {
            // alert(m_CustomerId);

            // var Mobile = $("#txtMobileNumber").val();

            //GetUserData(Mobile);


            $.ajax({
                type: "POST",
                data: '{}',
                url: "shop.aspx/Test",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);






                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });



        }
        );

        $("#txtMobileNumber").keypress(
        function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);


            if (keycode == 13) {
                var Mobile = $("#txtMobileNumber").val();

                GetUserData(Mobile);

            }



        }

        );





        BindGridProducts();

    }
    );

    
</script>


<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
<div id="content" style="padding:6px">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Customer Information </span>
                     
                        <br />
                    </h3>
				   <div class="youhave" style="padding-left:30px">
                   <table cellpadding="0" cellspacing="0" border="0">
                     <tr><td class="headings">Customer Mobile:</td><td>  
                     <table cellpadding="0" cellspacing="0" border="0" style="margin:0px">
                     <tr>
                     <td><input type="text" id="txtMobileNumber" class="inputtxt validate ddlrequired" style="width:200px"/></td>
                     <td>
                     <div class="btn btn-primary btn-small" id="btnGo" >Go</button>
                     </td>
                     </tr>
                     </table>
         


                     </td></tr>

                    
                    
                    
                   
                                         

                     </table>



<div id="dvCustomerPopup" style="display:none">
<table>
<tr>
<td>
 <table style="background:#D9EAED">
                     <tr><td colspan="100%"><b>Customer Information</b></td></tr>
                        <tr>
                       <td class="headings">First Name:</td> 
                     
                    <td><input type="text" id="txtFirstName"  style="width:175px"  class="inputtxt validate ddlrequired customInput"/></td>

                    </tr>

                    <tr>
                     <td class="headings">Last Name:</td>
                     <td><input type="text" id="txtLastName" style="width:175px"  class="inputtxt validate ddlrequired customInput"/></td>

                     </tr>
                    

                   
                     <tr><td class="headings">Email:</td>
                     <td>  <input type="text" id="txtEmail" style="width:175px" class="inputtxt validate required alphanumeric customInput"  data-index="1"   /></td></tr>
                       
                       
                     </table>



</td>

<td>

<table cellpadding="0" cellspacing="0"   style="background:#D9EAED">
<tr><td colspan="100%"><b>RECIPIENT INFORMATION</b></td></tr>
<tr><td> First Name:</td><td><table  cellpadding="0" cellspacing="0" style="margin:0px" ><tr><td><input type="text" id="txtRecipientFirstName" class="customInput" /></td><td>Last Name:</td><td><input type="text" id="txtRecipientLastName"  class="customInput" /></td></tr></table></td></tr>
<tr><td> Mobile:</td><td><table  cellpadding="0" cellspacing="0"  style="margin:0px"><tr><td><input type="text" id="txtRecipientMobile"  class="customInput" /></td><td>Telephone:</td><td><input type="text" id="txtRecipientTelephone"  class="customInput" /></td></tr></table></td></tr>
<tr><td> City:</td><td><table  cellpadding="0" cellspacing="0" style="margin:0px" ><tr><td><input type="text" id="Text1"  class="customInput" /></td><td>Address:</td><td><input type="text" id="Text2" class="customInput"  /></td></tr></table></td></tr>
<tr><td></td><td>
<div>Add Information</div>

</td></tr>
</table>
</td>
 
</tr>
</table>




                       <table style="width:80%">
                               <tr>
                       <td colspan="100%">
                       
                           <table id="dvAddressList">
                           
                           </table>

                      
                       
                       
                       </td>
                       </tr>
                               </table> 

<table>
<tr><td><div id="btnContinueShopping"  class="btn btn-primary btn-small" >Continue Shopping</div></td></tr>
</table>
                        

</div>

                    			
                                
                             
       
                    </div>
			  </div>
               


               <div id="rightnow" style="margin-top:10px">
                    <h3 class="reallynow">
                        <span>Shop</span>
                      
                        <br />
                    </h3>
				    <div class="youhave" style="padding-left:30px">
                    <table>
                    <tr>
                    <td>
                    <table>
                    <tr><td>
                    <table>
                   <tr><td>Choose Category:</td><td><asp:DropDownList ID="ddlCategories" runat="server"></asp:DropDownList></td></tr>
                    </table>
                    
                    </td></tr>
          
          
                    <tr><td>
                    
                             <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
                    
                    </td></tr>
                    
                    </table>

                    
                    </td>


                    <td>
                    
                    
                    
                    </td>
                    </tr>
                    </table>

                    
                    
      	 
      
                    </div>
			  </div>

            </div>
</form>




           <script type="text/javascript">
                function BindGridProducts() {

  
                    var CatId = $("#<%=ddlCategories.ClientID%>").val();
                    jQuery("#jQGridDemo").GridUnload();
        jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/ManageProducts.ashx?CatId=' + CatId, 
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['ProductId', 'Name', 'MRP', 'QTY', 'Price'],
            colModel: [
                        { name: 'ProductId', key: true, index: 'ProductId', width: 100, stype: 'text', sorttype: 'int', hidden: true },
   		                 
                        { name: 'Name', index: 'Name', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		             
                        { name: 'MarketPrice', index: 'MarketPrice', width: 150, stype: 'text', sortable: true,hidden:true, editable: true ,editrules: { required: true },hidden:true},
   		               
   		             { name: 'Qty', index: 'Qty', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		               { name: 'Price', index: 'Price', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		             

                        
   		                  
                       ],
            rowNum: 5,
          
            mtype: 'GET',
            loadonce: true,
              ignoreCase: true,
              toolbar: [true, "top"],
            rowList: [5,10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'ProductId',
            viewrecords: true,
            height: "100%",
            width:"400px",
            sortorder: 'asc',
            caption: "Product List",
         
            editurl: 'handlers/ManageSubCategories.ashx',
         
                    
             
        });



             var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input  id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button style='display:none' id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
          
                 var key = e.charCode || e.keyCode || 0;
                 if (key === $.ui.keyCode.ENTER) { // 13
           
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });




        $("#jQGridDemo").jqGrid('setGridParam',
{
    onSelectRow: function (rowid, iRow, iCol, e) {
        m_SubCategoryId = 0;
        validateForm("detach");
        var txtTitle = $("#txtTitle");
        m_SubCategoryId = $('#jQGridDemo').jqGrid('getCell', rowid, 'CategoryId');


        if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
            $('#chkIsActive').prop('checked', true);
        }
        else {
            $('#chkIsActive').prop('checked', false);

        }
        txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Title'));

        $("#txtDescription").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Description'));

        var CatId = $('#jQGridDemo').jqGrid('getCell', rowid, 'ParentId');
 

               txtTitle.focus();
               $("#btnAdd").css({ "display": "none" });
               $("#btnUpdate").css({ "display": "block" });
               $("#btnReset").css({ "display": "block" });
               TakeMeTop();
             }
         });


         
       var DataGrid = jQuery('#jQGridDemo');
       DataGrid.jqGrid('setGridWidth', '400');

       $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                        {
                            refresh: false,
                            edit: false,
                            add: false,
                            del: false,
                            search: false,
                            searchtext: "Search",
                            addtext: "Add",
                        },

                        {//SEARCH
                            closeOnEscape: true

                        }

                          );

$("#t_jQGridDemo").css("width","388px");

}
 
      

                   
              
    </script>

</asp:Content>

