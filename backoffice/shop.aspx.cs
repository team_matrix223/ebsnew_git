﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class backoffice_shop : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCategories();
        }
    }

    void BindCategories()
    {

        ddlCategories.DataSource = new CategoryBLL().GetByLevel(1);
        ddlCategories.DataValueField = "CategoryId";
        ddlCategories.DataTextField = "Title";
        ddlCategories.DataBind();

       
        
        
       ListItem li = new ListItem();
        li.Text = "ALL CATEGORIES";
        li.Value = "-1";
        ddlCategories.Items.Insert(0, li);
    }

    [WebMethod(EnableSession=true)]
    public static string GetUserInfo(string MobileNo)
    {
        Users objUsers = new Users() {MobileNo=MobileNo };
        List<DeliveryAddress> lstDelivery = new UsersBLL().GetByMobileNo(objUsers);
        if (objUsers.UserId != 0)
        {
            HttpContext.Current.Response.Cookies[Constants.UserId].Value = objUsers.UserId.ToString();
        }
        var JsonData = new
        {
            User = objUsers,
            lstAddress = lstDelivery
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string Test(int CustomerId)
    {
        int Cid = CustomerId;
        string str = HttpContext.Current.Request.Cookies[Constants.UserId].Value;
        
        var JsonData = new
        {
         };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
}