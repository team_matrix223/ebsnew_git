﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;

public partial class backoffice_CustomerInfo : System.Web.UI.Page
{
    public int Id { get { return Request.QueryString["Id"] != null ? Convert.ToInt16(Request.QueryString["Id"]) : 0; } }
  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnUser.Value = Id.ToString();
        }
    }

    [WebMethod]

    public static string GetDetail(Int32 UserId)
    {
        Users objUsers = new Users()
        {
            UserId = Convert.ToInt32(UserId),
        };
        DeliveryAddress objDelivery = new DeliveryAddress()
        {
            UserId = Convert.ToInt32(UserId),
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        new UsersBLL().GetById(objUsers, objDelivery);
        var JsonData = new
        {
            Users = objUsers,
            Delivery = objDelivery,

        };
        return ser.Serialize(JsonData);
    }
}