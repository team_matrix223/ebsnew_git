﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true"
    CodeFile="migrateproducts.aspx.cs" Inherits="backoffice_migrateproducts" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" runat="Server">
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <%--<link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
    <script src="js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

     <style type="text/css">
         
        .ui-jqgrid .ui-userdata { height: auto;width:488px }
        .ui-jqgrid .ui-userdata div { margin: .1em .5em .2em;}
        .ui-jqgrid .ui-userdata div>* { vertical-align: middle;}
     
    </style>

    <script language="javascript" type="text/javascript">
        var m_Counter = 0;
     
        $(document).on("click", "#btnDelete", function (event) {
            var RowIndex = Number($(this).closest('tr').index());
            var tr = $(this).closest("tr");
            tr.remove();

          

        });
        $(document).ready(

       function () {

           BindGrid();
           BindGridProductAndVariations();

           $("#btnAddProducts").click(
           function () {


               arItemCodes = [];
               arUnits = [];
               arQty = [];
               arPrice = [];
               arMrp = [];
               arType = [];
               arDesc = [];
               arPName = [];



               $("input[name='chkSelected']:checked").each(
               function (x) {


                   var counterId = $(this).attr("counter");


                   arItemCodes[x] = $(this).val();
                   arUnits[x] = $("#ddlUnit" + counterId).val();
                   arQty[x] = $("#txtQty" + counterId).val();
                   arPrice[x] = $("#txtPrice" + counterId).val();
                   arMrp[x] = $("#txtMrp" + counterId).val();
                   arType[x] = $("#ddlType" + counterId).val();
                   arDesc[x] = $("#txtDesc" + counterId).val();
                   arPName[x] = $("#txtItemName" + counterId).val();

               }

               );

               var C1 = $("#<%=ddlCategory.ClientID%>").val();
               var C2 = $("#<%=ddlSubCategories.ClientID%>").val();
               var C3 = $("#<%=ddlCategoryLevel3.ClientID%>").val();
               var Brand = $("#<%=ddlBrand.ClientID%>").val();
               var Name = $("#txtProductName").val();
               var SName = $("#txtShortName").val();
               var Desc = $("#txtProductDescription").val();


               var IsSingle = false;


               if ($('#chkIsSingle').is(":checked")) {
                   IsSingle = true;
               }



               if (C1 == "0") {
                   alert("Please select Category Level 1");
                   return;
               }

               if (C2 == "0") {
                   alert("Please select Category Level 2");
                   return;
               }

               if (C3 == 0) {
                   alert("Please select Category Level 3");
                   return;

               }

               if (C3 == null) {
                   C3 = 0;
               }

               if (Brand == "") {

                   alert("Please select Brand");
                   return;
               }

               if (arItemCodes.length == 0) {
                   alert("Please select atleast one product");
                   return;
               }

               $.uiLock('');
               $.ajax({
                   type: "POST",
                   data: '{ "C1": "' + C1 + '", "C2": "' + C2 + '", "C3": "' + C3 + '", "Brand": "' + Brand + '", "Name": "' + Name + '", "SName": "' + SName + '", "Desc": "' + Desc + '", "arrItemCode": "' + arItemCodes + '", "arrUnits": "' + arUnits + '", "arrQty": "' + arQty + '", "arrMrp": "' + arMrp + '", "arrPrice": "' + arPrice + '", "arrType": "' + arType + '", "arrDesc": "' + arDesc + '","IsSingle":"' + IsSingle + '","arrPName":"' + arPName + '"}',
                   url: "migrateproducts.aspx/Insert",
                   contentType: "application/json",
                   dataType: "json",
                   success: function (msg) {
                       m_Counter = 0;
                       var obj = jQuery.parseJSON(msg.d);
                       $('#tbMainProductList tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                       $("#dvDialog").dialog("close");
                       m_Counter = 0;
                       BindGrid();
                       BindGridProductAndVariations();
                       $.uiUnlock();
                       alert("Products Inserted Successfully");



                   },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   },
                   complete: function (msg) {


                   }
               });








           }

           );

       }

       );


    </script>
    <form id="frmProducts" runat="server">
   
    <div id="dvDialog" style="display: none;background:#BBDADF">
         <table align="center" border="0" style="border: 0px solid black; margin: 1px" cellpadding="5"
            cellspacing="0" width="100%">
            <tr>
            <td colspan="100%">
            <table>
            <tr>
            <td>
                    <asp:ScriptManager ID="Scrpt1" runat="server">
                    </asp:ScriptManager>
                    <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="updCatgories" runat="server">
                        <ProgressTemplate>
                            <div id="dvProgress" style="position: absolute; width: 750px; height: 500px; background-color: Black;
                                color: White; text-align: center; opacity: 0.7; vertical-align: middle">
                                loading please wait..
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="updCatgories" runat="server">
                        <ContentTemplate>
                            
                                        
                               
                                        <table cellspacing="2" cellpadding="0" border="0" style="margin-left: 12px">
                                            <tr>
                                                <td colspan="100%" style="color: Red; font-style: italic">
                                                    <%=ErrorMessage %><asp:HiddenField ID="hdnProductId" runat="server" Value="0" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right" class="Titles">
                                                      Level 1:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True" Width="307px"
                                                        OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                                                        <asp:ListItem Text="--Category Level 1--" Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="reqCategory" ControlToValidate="ddlCategory" runat="server"
                                                        InitialValue="0" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    <asp:Label ID="lblMessage" runat="server" Text="No Sub Page Available" Visible="False"></asp:Label>
                                                    <span style="color: #D9395B; font-weight: normal">
                                                        <%=m_sMessage %></span>
                                                </td>
                                                <td style="color: Red">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right" class="Titles">
                                                     Level 2:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlSubCategories" Width="307px" runat="server" AutoPostBack="True"
                                                        OnSelectedIndexChanged="ddlSubCategories_SelectedIndexChanged">
                                                        <asp:ListItem Text="--Category Level 2--" Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlSubCategories"
                                                        ForeColor="Red" InitialValue="0" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    <span style="color: #D9395B; font-weight: normal">
                                                        <%=m_sSubMessage %></span>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr id="catLevel3" runat="server">
                                                <td style="text-align: right" class="Titles">
                                                 Level 3:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlCategoryLevel3" Width="307px" runat="server" AutoPostBack="True"
                                                        OnSelectedIndexChanged="ddlCategoryLevel3_SelectedIndexChanged">
                                                        <asp:ListItem Text="--Category Level 3--" Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <span style="color: #D9395B; font-weight: normal">
                                                        <%=m_sSubMessage %></span>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>

                                              <tr id="Tr1" runat="server">
                                                <td style="text-align: right" class="Titles">
                                                    Brand:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlBrand" Width="307px" runat="server" 
                                                      >
                                                        <asp:ListItem Text="--Select Manufacturer--" Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                     
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>

                                            <tr>
                                            <td colspan="100%">
                                            <table>
                                            <tr>
                                            <td>Brand</td>
                                            <td>
                                            <asp:TextBox ID="txtBrandName" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                            <asp:Button ID="btnAddBrand" CausesValidation="false" OnClick="btnAddBrand_Click" runat="server" Text="Add Brand" />
                                            </td>
                                            </tr>
                                            </table>
                                            </td>
                                            </tr>

                                            

                                        </table>
                                
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td   valign="top">
                <table>
                <tr><td>Product Name:</td><td>

               <table>
               <tr>
               <td><input type="text" id="txtProductName" class="inputtxt validate required"/></td>
               <td><input type="checkbox"  id="chkIsSingle"/>Is Single Product</td>
               </tr>
               </table>
                
                
                </td></tr>
                
                 <tr><td>Short Name:</td><td>
                <input type="text" id="txtShortName" class="inputtxt validate required"/>
                
                </td></tr>
                
                 <tr><td>Description:</td><td>
                <textarea type="text" id="txtProductDescription" class="inputtxt validate required" style="border:solid 1px silver"></textarea>
                
                </td></tr>

                
             
                </table>


                 </td>
            </tr>
            </table>
            </td>

                
            </tr>
             
            <tr>
            <td >
            <table>
            <tr>
              <td   valign="top">
                                              <table id="JQGridSearch">
                </table>
                <div id="JQGridSearchPager">
                </div>
                                            
                                            
                                            </td>
                 <td>
                                            <div style="max-height:400px;overflow-y:scroll;float:left">
                
                        <table id="tbMainProductList" >
                        <thead>
                        <tr style="background:#294145;color:white">
                        <th></th>
                        <th>Name</th>
                        <th style="width:40px">Qty</th>
                        <th style="width:40px">Units</th>
                        <th style="width:40px">Mrp</th>
                        <th style="width:40px">Price</th>

                        <th style="width:40px">Type</th>
                            
                        <th style="width:40px">Description</th>
                           <th></th>   
                        </tr>


                        </thead>
                        <tbody class="customTable">
                         
                        
                        </tbody>


                        </table>
                </div>  
                                            
                                            </td>
                                            </tr>

                  
                  
                  <tr>
 <td></td>
                                            <td> <div id="btnAddProducts" class="btn btn-primary btn-small">Add</div></td>
                                            </tr>                          
            </table>
            
            </td>
            </tr>
        </table>
  
    </div>



    <div id="content" style="padding: 6px">
        <div id="rightnow">
            <h3 class="reallynow">
                <span>Add/Update Products</span>
                <br />
            </h3>
            <div class="youhave" style="padding-left: 10px">
                <table id="jQGridDemo">
                </table>
                <div id="jQGridDemoPager">
                </div>
            </div>
        </div>


        <div id="rightnow">
            <h3 class="reallynow">
                <span>Product List</span>
                <br />
            </h3>
            <div class="youhave" style="padding-left: 10px">

            <div style="max-height:500px;overflow-y:scroll">
                <table id="jQGridDemo1">
                </table>
                <div id="jQGridDemoPager1">
                </div>
                </div>
            </div>
        </div>
    </div>



      
 

    </form>
    <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/managepacking.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Item Code', 'Item Name', 'Short Name','Department','Brand'],
                        colModel: [
                                    { name: 'ItemCode', key: true, index: 'ItemCode', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
                                    { name: 'ItemName', index: 'ItemName', width: 400, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                      { name: 'ShortName', index: 'ShortName',hidden:true, width: 50, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
  { name: 'Department', index: 'Department', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
  { name: 'Brand', index: 'Brand', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },

                                 

                        ],
                        rowNum: 10,
                        toolbar: [true, "top"],
                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'ItemCode',
                        viewrecords: true,
                        height: "100%",
                        width: "500px",
                        sortorder: 'asc',
                        caption: "Product List",
                        ignoreCase: true,
                        editurl: 'handlers/managepacking.ashx',



                    });

                    var   $grid = $("#jQGridDemo");
       
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button>&nbsp;</div>"));
            $("#globalSearchText").keypress(function (e) {
          
                 var key = e.charCode || e.keyCode || 0;
                 if (key === $.ui.keyCode.ENTER) { // 13
           
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
               
            });



                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                     $('#tbMainProductList tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

                    var ProductName = $('#jQGridDemo').jqGrid('getCell', rowid, 'ItemName');
                    var ShortName=$('#jQGridDemo').jqGrid('getCell', rowid, 'ShortName');

                    $("#txtProductName").val(ProductName);
                    $("#txtShortName").val(ShortName);
                    $("#txtProductDescription").val(ProductName);


                      $.ajax({
            type: "POST",
            data: '{ "Keyword": "' + ProductName + '"}',
            url: "migrateproducts.aspx/Search",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
               
                var obj = jQuery.parseJSON(msg.d);

                
             
              
                  m_Counter = 0;
                    var tr="";
           
                 for(var i=0;i<obj.Data.length;i++)
                 {  
                        m_Counter=m_Counter+1;

                        tr=tr+"<tr><td><input type='checkbox' name='chkSelected' value='"+obj.Data[i].ItemCode+"' counter='"+m_Counter+"' /></td><td><input type='text'  id='txtItemName"+m_Counter+"'  style='height:50px' value='"+obj.Data[i].ItemName+"'/></td><td><input type='text' name='txtItemCode' counter='"+m_Counter+"'  style='display:none' value='"+obj.Data[i].ItemCode+"'/><input type='text' counter='"+m_Counter+"' value='1'  name='txtQty' id='txtQty"+m_Counter+"' style='width:40px'/></td><td><select  name='ddlUnit' id='ddlUnit"+m_Counter+"'  style='width:61px'>";
                        tr=tr+"<option value='nos'>NOS</option><option value='kg'>KG</option><option value='gm'>GRAM</option><option value='lt'>LITER</option><option value='pcs'>PIECE</option> <option value='ml'>ML</option></select></td><td>";
                        tr=tr+"<input type='text' name='txtMrp' id='txtMrp"+m_Counter+"' value='"+obj.Data[i].MRP+"' style='width:40px'/></td><td><input name='txtPrice' type='txtPrice'  value='"+obj.Data[i].Price+"' id='txtPrice"+m_Counter+"' style='width:40px'/></td>";
                        tr=tr+"<td><select id='ddlType"+m_Counter+"' name='ddlType'><option value=''></option><option value='carton'>CARTON</option><option value='bottle'>BOTTLE</option><option value='poly bag'>POLY BAG</option><option value='poly pack'>POLY PACK</option>";
                        tr=tr+"<option value='packet'>PACKET</option><option value='cup'>CUP</option><option value='box'>BOX</option><option value='tin'>TIN</option><option value='pouch'>POUCH</option><option value='jar'>JAR</option></select></td><td>";
                        tr=tr+"<input type='text' id='txtDesc"+m_Counter+"' name='txtDesc' style='border:solid 1px silver;width:100px'/></td>";
                         tr=tr+"</tr>";
        
                  

                   }

                    $("#tbMainProductList").append(tr);
              
            },
            complete: function (msg) {
              

            } });


//                     $('#dvDialog').dialog({height: 280, width: 440, modal: true, appendTo:"form" });
BindGridSearch();
                          $('#dvDialog').dialog(
        {
            autoOpen: false,

            width: 950,
            resizable: false,
            modal: false,  appendTo:"form" 
        });


      
            linkObj = $(this);
            var dialogDiv = $('#dvDialog');
            dialogDiv.dialog("option", "position", [10, 50]);
             dialogDiv.dialog('open');
            return false;
 
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '600');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: true,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }




                function BindGridSearch() {

 
                    jQuery("#JQGridSearch").GridUnload();
                    jQuery("#JQGridSearch").jqGrid({
                        url: 'handlers/managepacking.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Item Code', 'Item Name', 'Short Name', 'Brand','Price','MRP'],
                        colModel: [
                                    { name: 'ItemCode', key: true, index: 'ItemCode', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
                                    { name: 'ItemName', index: 'ItemName', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'ShortName', index: 'ShortName', width: 200,hidden:true, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Brand', index: 'Brand', width: 200, stype: 'text', hidden:true,sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Price', index: 'Price', width: 200, stype: 'text', hidden:true,sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'MRP', index: 'MRP', width: 200, stype: 'text', hidden:true,sortable: true, editable: true, editrules: { required: true } },
 
                                 

                        ],
                        rowNum:10,
                   //       toolbar: [true, "top"],
                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#JQGridSearchPager',
                        sortname: 'ItemCode',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Product List",
                        ignoreCase: true,
                        editurl: 'handlers/managepacking.ashx',



                    });

//                    var   $grid = $("#JQGridSearch");
//            
//            $('#t_' + $.jgrid.jqID($grid[0].id))
//                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button>&nbsp;</div>"));
//            $("#globalSearchText").keypress(function (e) {
//          
//                 var key = e.charCode || e.keyCode || 0;
//                 if (key === $.ui.keyCode.ENTER) { // 13
//           
//                    $("#globalSearch").click();
//                }
//            });
//            $("#globalSearch").button({
//                icons: { primary: "ui-icon-search" },
//                text: false
//            }).click(function () {
//                var postData = $grid.jqGrid("getGridParam", "postData"),
//                    colModel = $grid.jqGrid("getGridParam", "colModel"),
//                    rules = [],
//                    searchText = $("#globalSearchText").val(),
//                    l = colModel.length,
//                    i,
//                    cm;
//                for (i = 0; i < l; i++) {
//                    cm = colModel[i];
//                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
//                        rules.push({
//                            field: cm.name,
//                            op: "cn",
//                            data: searchText
//                        });
//                    }
//                }
//                postData.filters = JSON.stringify({
//                    groupOp: "OR",
//                    rules: rules
//                });
//                $grid.jqGrid("setGridParam", { search: true });
//                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
//               
//            });



                    $("#JQGridSearch").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                     m_Counter=m_Counter+1;


                     var ShortName=$('#JQGridSearch').jqGrid('getCell', rowid, 'ShortName');
                    var ItemCode=$('#JQGridSearch').jqGrid('getCell', rowid, 'ItemCode');
                    var ItemName=$('#JQGridSearch').jqGrid('getCell', rowid, 'ItemName');
                    var MRP=$('#JQGridSearch').jqGrid('getCell', rowid, 'MRP');
                    var Price=$('#JQGridSearch').jqGrid('getCell', rowid, 'Price');
                    var tr="";
                  
                    tr=tr+"<tr><td><input type='checkbox' name='chkSelected' value='"+ItemCode+"' counter='"+m_Counter+"' /></td><td><input type='text'  id='txtItemName"+m_Counter+"'  style='height:50px' value='"+ItemName+"'/></td><td><input type='text' name='txtItemCode' counter='"+m_Counter+"'  style='display:none' value='"+ItemCode+"'/><input type='text' counter='"+m_Counter+"'   name='txtQty' id='txtQty"+m_Counter+"' value='1'  style='width:40px'/></td><td><select  name='ddlUnit' id='ddlUnit"+m_Counter+"'  style='width:61px'>";
                        tr=tr+"<option value='nos'>NOS</option><option value='kg'>KG</option><option value='gm'>GRAM</option><option value='lt'>LITER</option><option value='pcs'>PIECE</option> <option value='ml'>ML</option></select></td><td>";
                        tr=tr+"<input type='text' name='txtMrp' id='txtMrp"+m_Counter+"' value='"+ MRP+"' style='width:40px'/></td><td><input name='txtPrice' type='txtPrice'  value='"+ Price+"' id='txtPrice"+m_Counter+"' style='width:40px'/></td>";
                        tr=tr+"<td><select id='ddlType"+m_Counter+"' name='ddlType'><option value=''></option><option value='carton'>CARTON</option><option value='bottle'>BOTTLE</option><option value='poly bag'>POLY BAG</option><option value='poly pack'>POLY PACK</option>";
                        tr=tr+"<option value='packet'>PACKET</option><option value='cup'>CUP</option><option value='box'>BOX</option><option value='tin'>TIN</option><option value='pouch'>POUCH</option><option value='jar'>JAR</option></select></td><td>";
                        tr=tr+"<input type='text' id='txtDesc"+m_Counter+"' name='txtDesc' style='border:solid 1px silver;width:100px'/></td>";
                         tr=tr+"</tr>";
                  
            
                   $("#tbMainProductList").append(tr);
 
 
                }
            });

            var DataGrid = jQuery('#JQGridSearch');
            DataGrid.jqGrid('setGridWidth', '250');

            $('#JQGridSearch').jqGrid('navGrid', '#JQGridSearchPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: true,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }




         function BindGridProductAndVariations() {

 
                    jQuery("#jQGridDemo1").GridUnload();
                    jQuery("#jQGridDemo1").jqGrid({
                        url: 'handlers/managepackingproducts.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Item Code', 'Item Name', 'Short Name', 'Brand','Price','MRP'],
                        colModel: [
                                    { name: 'ItemCode', key: true, index: 'ItemCode', width: 100, stype: 'text', sorttype: 'int', hidden: false },
                                  
                                    { name: 'ItemName', index: 'ItemName', width: 400, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Unit', index: 'Unit', width: 200,hidden:false, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Qty', index: 'Qty', width: 200,hidden:false, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Price', index: 'Price', width: 200, stype: 'text', hidden:false,sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'MRP', index: 'MRP', width: 200, stype: 'text', hidden:false,sortable: true, editable: true, editrules: { required: true } },
 
                                 

                        ],
                        rowNum:2000,
                   //       toolbar: [true, "top"],
                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#JQGridSearchPager',
                        sortname: 'ItemCode',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Product List",
                        ignoreCase: true,
                        editurl: 'handlers/managepacking.ashx',
                         grouping:true, groupingView : { groupField : ['ItemName'],groupCollapse: true },


                    });

 

  

            var DataGrid = jQuery('#jQGridDemo1');
            DataGrid.jqGrid('setGridWidth', '600');

            $('#jQGridDemo1').jqGrid('navGrid', '#jQGridDemoPager1',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: true,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }


    </script>
</asp:Content>
