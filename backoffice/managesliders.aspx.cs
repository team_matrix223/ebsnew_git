﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data;
using System.Web.Services;

public partial class backoffice_managesliders : System.Web.UI.Page
{
    protected string m_sMessage = "";
    protected string m_sSubMessage = "";
    HttpWebRequest URLReq;
    HttpWebResponse URLRes;
    protected bool m_bShow = false;
    string imageName = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                //if (!User.IsInRole("ManageSliders"))
                //{
                //    //Response.Redirect("default.aspx");

                //}
                btnUpdate.Visible = false;
                btnCancel.Visible = false;
                FillDataList();
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void Reset()
    {
        txtUrlName.Text = "";
        chkIsActive.Checked = true;
        hdnPhoto.Value = "0";
        lblimg.Text = string.Empty;
        hdnlblimg.Value = string.Empty;
        btnUpdate.Visible = false;
        btnCancel.Visible = false;
        btnAdd.Visible = true;
    }

    public void FillDataList()
    {
        DataList1.DataSource = new SlidersBLL().GetAll();
        DataList1.DataBind();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            if (!fuImage.HasFile)
            {
                Response.Write("<script>alert('Choose Image For Photo Gallery');</script>");
                fuImage.Focus();
                return;
            }
            imageName = CommonFunctions.UploadImage(fuImage, "~/images/slider/home/", false, 980, 300, false, 0, 0);
            Sliders objSliders = new Sliders()
            {
                ImageUrl = imageName,
                IsActive = chkIsActive.Checked,
                UrlName = txtUrlName.Text,
            };
            int Status = new SlidersBLL().InsertUpdate(objSliders);
            if (Status == 0)
            {
                Response.Write("<script>alert('Insertion Failed.Please try again.');</script>");
            }
            else
            {
                Response.Write("<script>alert('Photo Added  Successfully!')</script>");
                FillDataList();
                Reset();
            }
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('" + ex.Message + "')</script>");
        }
    }

    protected void DataList1_EditCommand(object source, DataListCommandEventArgs e)
    {
        int PhotoId = (int)DataList1.DataKeys[e.Item.ItemIndex];
        hdnPhoto.Value = PhotoId.ToString();
        chkIsActive.Checked = Convert.ToBoolean(((HiddenField)e.Item.FindControl("hdnactive")).Value);

        lblimg.Text = ((HiddenField)e.Item.FindControl("hdnUrl")).Value;
        hdnlblimg.Value = ((HiddenField)e.Item.FindControl("hdnUrl")).Value;
        txtUrlName.Text = ((HiddenField)e.Item.FindControl("hdnUrlName")).Value;
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
    }

    protected void ddlSubPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillDataList();
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            imageName = hdnlblimg.Value;

            if (fuImage.HasFile)
            {
                imageName = CommonFunctions.UploadImage(fuImage, "~/images/slider/home/", false, 980, 300, false, 0, 0);
            }
            Sliders objSliders = new Sliders()
            {
                SliderId = Convert.ToInt32(hdnPhoto.Value),
                ImageUrl = imageName,
                IsActive = chkIsActive.Checked,
                UrlName = txtUrlName.Text
            };
            int Status = new SlidersBLL().InsertUpdate(objSliders);
            if (Status == 0)
            {
                Response.Write("<script>alert('Insertion Failed.Please try again.');</script>");
            }
            else
            {
                Response.Write("<script>alert('Photo Updated  Successfully!')</script>");
                FillDataList();
                Reset();
            }
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('" + ex.Message + "')</script>");
        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
    }


    protected void btn_slider_new_Click(object sender, EventArgs e)
    {
        string ddvl = ddslider.SelectedValue;
        if (file_new.HasFile)
        {
            imageName = CommonFunctions.UploadImage(file_new, "~/images/slider/new/", false, 980, 300, false, 0, 0);
            Sliders objSliders = new Sliders()
            {
                req="insert",
                SliderId = Convert.ToInt32(hdnPhoto.Value),
                ImageUrl = imageName,
                UrlName = tbnew_url.Text.Trim(),
                SliderType= ddvl
            };

            new SlidersBLL().PageSliderInsertDelete(objSliders);
            tbnew_url.Text = "";
            gv_addedslider.DataSource = new SlidersBLL().PageSlider("get", ddslider.SelectedValue, 0);
            gv_addedslider.DataBind();
        }


    }
    protected void btn_slider_trending_Click(object sender, EventArgs e)
    {
        string ddvl = ddslider.SelectedValue;
        if (file_trending.HasFile)
        {
            imageName = CommonFunctions.UploadImage(file_trending, "~/images/slider/trending/", false, 980, 300, false, 0, 0);
            Sliders objSliders = new Sliders()
            {
                req = "insert",
                SliderId = Convert.ToInt32(hdnPhoto.Value),
                ImageUrl = imageName,
                UrlName = tbtrending_url.Text.Trim(),
                SliderType = ddvl
            };

            new SlidersBLL().PageSliderInsertDelete(objSliders);
            tbtrending_url.Text = "";
            gv_addedslider.DataSource = new SlidersBLL().PageSlider("get", ddslider.SelectedValue, 0);
            gv_addedslider.DataBind();
        }


    }
    [WebMethod]
    public static void delete_slider(int sliderid,string req)
    {

        new SlidersBLL().PageSlider("delete", "", sliderid);

    }
    protected void ddslider_SelectedIndexChanged(object sender, EventArgs e)
    {
        home_slider.Visible = false;
        new_slider.Visible = false;
        trending_slider.Visible = false;
        added_slider.Visible = false;
        string DDVal = ddslider.SelectedValue;
        if (DDVal == "home")
        {
            home_slider.Visible = true;
        }
        else if (DDVal == "new")
        {
            new_slider.Visible = true;
            added_slider.Visible = true;
            gv_addedslider.DataSource = new SlidersBLL().PageSlider("get",DDVal,0);
            gv_addedslider.DataBind();
        }
        else if (DDVal == "trending")
        {
            trending_slider.Visible = true;
            added_slider.Visible = true;
            gv_addedslider.DataSource = new SlidersBLL().PageSlider("get",DDVal,0);
            gv_addedslider.DataBind();
        }


    }
}