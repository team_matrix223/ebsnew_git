﻿<%@ Page Title="" Language="C#" MasterPageFile="admin.master" AutoEventWireup="true" CodeFile="managesliders.aspx.cs" Inherits="backoffice_managesliders" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/custom-css/master.css" rel="stylesheet" />
	<link href="css/custom-css/managesliders.css" rel="stylesheet" />
<form id="frmVid" runat="server">
<asp:HiddenField ID= "hdnPhoto" runat="server" Value ="0" />

    <script src="js/bo_customjs/managesliders.js"></script>

  
 <div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Sliders</span>
                     
                        <br />
                    </h3>
                           <div>
							   <div class="slider-type">							   
								   <span>Select Slider Type:</span>
								<asp:DropDownList ID="ddslider" ClientIDMode="Static" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddslider_SelectedIndexChanged">
									<asp:ListItem Value="0">-Select Page-</asp:ListItem>
									 <asp:ListItem Value="home">Home</asp:ListItem>
											  <asp:ListItem Value="new">New</asp:ListItem>
											  <asp:ListItem Value="trending">Trending</asp:ListItem>

								</asp:DropDownList>
								   </div>

    </div>
                             <div id="new_slider" runat="server" visible="false" class="new-slider-div">
                                  <div class="col-md-6 cst-managehome-col new-slider-col">
									  <div class="new-img-div"> <span>Image:</span> <asp:FileUpload ID="file_new" runat="server" /></div>
									   <div class="new-url-div"><span>Url:</span><asp:TextBox TextMode="MultiLine" Rows="2" ID="tbnew_url" runat="server"></asp:TextBox></div> 

						<asp:Button ID="btn_slider_new" runat="server" Text="Submit" CssClass="btn btn-primary btn-small" OnClick="btn_slider_new_Click" />
									</div>
          
							 </div>

                                               <div id="trending_slider" runat="server" visible="false" class="trend-slider-div">
                                  <div class="col-md-6 cst-managehome-col trend-slider-col">
               <div class="trend-img-div"> <span>Image:</span>  <asp:FileUpload ID="file_trending" runat="server" /></div>
                  <div class="trend-url-div"><span>Url:</span><asp:TextBox ID="tbtrending_url" TextMode="MultiLine" Rows="2"  runat="server"></asp:TextBox></div>

           <asp:Button ID="btn_slider_trending" runat="server" Text="Submit" CssClass="btn btn-primary btn-small" OnClick="btn_slider_trending_Click" />
</div>
           </div>
                                      <div class="col-md-6 cst-managehome-col">
                       <div id="added_slider" runat="server" visible="false">
              
			    <h3 class="reallynow"> <span>Added Sliders</span> </h3>
                       <asp:GridView ID="gv_addedslider" AutoGenerateColumns="false" runat="server" class="table managehome_table tbl-width">
                            <Columns>
                                        <asp:TemplateField>
                                    <ItemTemplate>
                            
                                         <asp:Label ID="Label1" Style="display: none" runat="server" Text='<%# Eval("ImageUrl") %>' CssClass="cls_image_path" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Image">
                                    <ItemTemplate>
                                        <img src='<%# Eval("ImageUrl") %>' class="small-img">
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" Style="display: none" runat="server" Text='<%# Eval("Sliderid") %>' CssClass="cls_sliderid" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Url">
                                    <ItemTemplate>
               
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("UrlName") %>' CssClass="cls_url" />
                                    </ItemTemplate>
                                </asp:TemplateField>
           
                                 <asp:BoundField DataField="isactive_string" HeaderText="IsActive" />
                                    <asp:TemplateField>
                                    <ItemTemplate>
               
                                    <asp:Label ID="Label1" Style="display: none" runat="server" Text='<%# Eval("isactive") %>' CssClass="cls_isactive" />
                                    </ItemTemplate>
                                </asp:TemplateField>
       
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <button type='button' class='btndel_addedslider btn btn-primary btn-small'><i class="fa fa-trash"></i></button>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                           </div>
                       </div>
                         <div id="home_slider" runat="server" visible="false" class="_slider">
				   <div class="youhave">  
                        <table class="top_table">
                             <tr>
                                 <td class="Titles">
                                     Image:</td>
                                 <td>
                                      <asp:FileUpload ID="fuImage" runat="server" />
                                      <asp:Label ID = "lblimg"  runat="server"></asp:Label><asp:HiddenField ID="hdnlblimg" runat="server" />
                                 </td>
                             </tr>

                                              <tr>
                                 <td class="Titles">
                                     Url:</td>
                                 <td>
                                     <asp:TextBox ID="txtUrlName" runat="server" TextMode="MultiLine"></asp:TextBox>
                                 </td>
                             </tr>
                             <tr>
                                 <td class="Titles">
                                     IsActive:</td>
                                 <td>
                                       <input type="checkbox" id="chkIsActive" runat="server" checked="checked" data-index="2"  name="chkIsActive" />
                                 </td>
                             </tr>                             
                         </table>

                       <table class="category_table">
                           <tr>
                                 <td>
                                     <asp:Button ID="btnAdd" CssClass="btn btn-primary btn-small" runat="server" 
                                         Text="Add Photo" onclick="btnAdd_Click" />
                                         <asp:Button ID="btnUpdate" CssClass="btn btn-primary btn-small" runat="server" 
                                         Text="Update Photo" onclick="btnUpdate_Click"  />
                                          <asp:Button ID="btnCancel" CssClass="btn btn-primary btn-small" runat="server" 
                                         Text="Cancel" onclick="btnCancel_Click"  />
                                 </td>
                             </tr>
                       </table>

                         </div>

                     

                         
               <div id="rightnow" style="margin-top:10px">
                    <h3 class="reallynow">
                        <span>Manage Photo Sliders</span>
                      
                        <br />
                    </h3>


                            <div class="youhave" style="padding-left:0px">

        <asp:DataList ID="DataList1" DataKeyField="SliderId" runat="server"  BorderColor="#666666"

            BorderStyle="None" BorderWidth="2px"    CellPadding="5" CellSpacing="2"

            Font-Names="Verdana" Font-Size="Small"  GridLines="Both" RepeatColumns="5" RepeatDirection="Horizontal"

           oneditcommand="DataList1_EditCommand">

            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />

            <HeaderStyle   Font-Bold="True" Font-Size="Large" ForeColor="White"

                HorizontalAlign="Center" VerticalAlign="Middle" />

             

            <ItemStyle BackColor="White" ForeColor="Black" BorderWidth="2px" />

            <ItemTemplate>
        <div style="margin:1px;float:left">
        
        
                 <img src="../Images/Slider/home/<%#Eval("ImageUrl") %>" height="125px" width="125px" />

                <br />

                    
                  
                 <asp:HiddenField ID="hdnSliderId" runat="server" Value ='<%# Eval("SliderId") %>' />
                            <asp:HiddenField ID="hdnactive" runat="server" Value ='<%# Eval("IsActive") %>' />
                             <asp:HiddenField ID="hdnurl" runat="server" Value ='<%# Eval("ImageUrl") %>' />
                        <asp:HiddenField ID="hdnUrlName" runat="server" Value ='<%# Eval("UrlName") %>' />
                
                <br />
                <asp:LinkButton runat="server" ID="Lnkedit" CausesValidation="false" ForeColor="Blue" Font-Bold="false"
                    CommandName="edit" >
                    Edit
                </asp:LinkButton>
             

        </div>
       

            </ItemTemplate>
          


        </asp:DataList>

    </div>

			  </div>
     </div>
                           </div>

              </div>

    

                        
                 
                   
                

 
    
</form>

</asp:Content>


