﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="manage_attr_stock.aspx.cs" Inherits="backoffice_manage_attr_stock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
	<link href="css/custom-css/manage_attr_stock.css" rel="stylesheet" />
    <form runat="server" id="formID" method="post">
        <script src="js/bo_customjs/manage_attr_stock.js"></script>
        <asp:HiddenField ID="hdn_attrid" ClientIDMode="Static" runat="server" />
            <div id="content">
            
            <div id="rightnow">
                <h3 class="reallynow">
                    <span>Manage Attributes & Stock</span>
                </h3>
       
                <div class="mananagehome_top_panel">
                    <div class="mananagehome_top_panel_shadow">
                        <asp:TextBox ID="tbitemcode" ClientIDMode="Static" runat="server"></asp:TextBox>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator3"  runat="server" ErrorMessage="*" ControlToValidate="tbitemcode" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary btn-small hm_cst" ClientIDMode="Static" Text="Submit" OnClick="btnsubmit_Click"/>
                    </div>
                </div>
                <div id="dv_all" runat="server" visible="false">
                <div class="mananagehome_top_panel">
                    <table class="table img-table">
                        <thead>
                            <tr>
                                <th style="display:none">
                                    Variation Id
                                </th>
                                   <th>
                                    Image
                                </th>
                                         <th>
                                    MRP
                                </th>
                                         <th>
                                    Price
                                </th>
                            </tr>
                        </thead>
                        <asp:Literal ID="ltr_product" runat="server"></asp:Literal>
                    </table>
                </div>
				    <div class="mananagehome_top_panel">
                <div id="dv_table" runat="server" class="mananagehome_top_panel_shadow type-tbl">
<table>
    <tr>
        <td>
            <label>Type</label>  <asp:DropDownList ID="ddl_type" ClientIDMode="Static" runat="server"></asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfv1" runat="server" ValidationGroup="rt" ControlToValidate="ddl_type" InitialValue="Select Type" ErrorMessage="*" ForeColor="Red"/>
        </td>
        <td id="td_title">
          <label>Title</label> <asp:TextBox ID="tbtitle"  ClientIDMode="Static" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="rt" runat="server" ErrorMessage="*" ControlToValidate="tbtitle" ForeColor="Red"></asp:RequiredFieldValidator>
        </td>
        <td>
           <label>Add Stock</label>  <asp:TextBox ID="tbaddstock" TextMode="Number" ClientIDMode="Static" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="rt" runat="server" ErrorMessage="*" ControlToValidate="tbaddstock" ForeColor="Red"></asp:RequiredFieldValidator>
        </td>
            <td style="display:none" id="td_lessstock">
           <label>Less Stock</label>  <asp:TextBox ID="tblessstock" TextMode="Number" ClientIDMode="Static" runat="server"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="rt" runat="server" ErrorMessage="*" ControlToValidate="tblessstock" ForeColor="Red"></asp:RequiredFieldValidator>--%>
        </td>

               <td id="td_itemcode_sku">
           <label>Item/SKU</label>  <asp:TextBox ID="tbitemcode_sku"  ClientIDMode="Static" runat="server"></asp:TextBox>
                                 
        </td>
                             <td>
           <label>MRP</label>  <asp:TextBox ID="tbmrp"  ClientIDMode="Static" runat="server"></asp:TextBox>
                                 
        </td>
                     <td>
           <label>Price</label>  <asp:TextBox ID="tbprice"  ClientIDMode="Static" runat="server"></asp:TextBox>
                                 
        </td>

        <td>
               <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary btn-small hm_cst" ValidationGroup="rt" ClientIDMode="Static" Text="Add" OnClick="btnAdd_Click"/>
        </td>
        <td>
            <asp:Label ID="lblerror" ClientIDMode="Static" runat="server" ForeColor="Red" Text=""></asp:Label>
        </td>
    </tr>
</table>
                </div>
						</div>
                <div class="mananagehome_top_panel">
                    <table class="table">
                        <thead>
                            <tr>
                          
                                        <th style="display:none">id</th>
                                      <th>Type</th>
                                        <th>Title</th>
                                        <th>Add Stock</th>
                                 <th>Less Stock</th>
                                 <th>Left Stock</th>
                                         <th>MRP</th>
                                         <th>Price</th>
                                           <th>ItemCode/SKU</th>
                                      <th>Edit</th>
                                   <th>Delete</th>

                            </tr>
                        </thead>
                        <tbody>
                              <asp:Literal ID="ltr_list" runat="server"></asp:Literal>
                        </tbody>
                    </table>

                </div>
                </div>
                </div>
                </div>
        </form>
</asp:Content>

