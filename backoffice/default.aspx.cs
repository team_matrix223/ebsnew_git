﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class dashboard : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    [WebMethod]
    public static string GetNotify()
    {
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var Notify = new NotificationBLL().GetNotificationDatewise();

        var JsonData = new
        {
            Notify = Notify,
            
        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string Update(int Id)
    {
        Notification objnotify = new Notification
        {
            Id = Id,

        };
        Int16 Count;
        JavaScriptSerializer ser = new JavaScriptSerializer();
        Count = new NotificationBLL().UpdateNotification(objnotify);
        var JsonData = new
        {
            Count = Count,
        };
        return ser.Serialize(JsonData);
    }





}