﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="ManageHomeProducts.aspx.cs" Inherits="backoffice_ManageHomeProducts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" runat="Server">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link href="css/custom-css/ManageHomeProducts.css" rel="stylesheet" />
    <link href="css/custom-css/master.css" rel="stylesheet" />
    <script src="js/bo_customjs/ManageHomeProducts.js"></script>

    <form runat="server" id="formID" method="post">
        <asp:HiddenField ID="hdnRoles" runat="server" />
            <asp:HiddenField ID="hdnssid" ClientIDMode="Static" runat="server" />
        <div id="content">
            
            <div id="rightnow">
                <h3 class="reallynow">
                    <span>Add/Remove Home Products</span>
                </h3>

                <div class="mananagehome_top_panel">
                    <div class="mananagehome_top_panel_shadow">
                        <asp:DropDownList ID="dd_type" CssClass="select_dd_ype" ClientIDMode="Static" AutoPostBack="true" runat="server" OnSelectedIndexChanged="dd_type_SelectedIndexChanged"></asp:DropDownList>
                        <asp:TextBox ID="tbfrom" type="date" ClientIDMode="Static" runat="server"></asp:TextBox>
                        <asp:TextBox ID="tbto" type="date" ClientIDMode="Static" runat="server"></asp:TextBox>
                        <asp:DropDownList ID="dd_brandtype" CssClass="select_dd_brand" ClientIDMode="Static" runat="server" Enabled="false">
                            <asp:ListItem Value="S">For Slider</asp:ListItem>
                            <asp:ListItem Value="SL">For Spotlight</asp:ListItem>
                        </asp:DropDownList>
                       

                        <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary btn-small hm_cst" ClientIDMode="Static" Text="Submit" OnClick="btnsubmit_Click" />
                    </div>
                </div>
                

				<div id="div_ss" runat="server" visible="false" class="first-show-div">
          <div class="col-md-4 cst-managehome-col">
                     <h3 class="reallynow"> <span>Add/Edit Item</span> </h3>
         
                            <table class="table managehome_table add-edit-table">
                             <tr>
                                 <td class="Titles">
                                     Image:</td>
                          
                                 <td>
                                     <img id="img_shopsale"/>
                                      <asp:FileUpload ID="fuImage" runat="server" />
                                      <asp:Label ID = "lblimg"  runat="server"></asp:Label><asp:HiddenField ID="hdnlblimg" runat="server" />
                                 </td>
                             </tr>

                                              <tr style="display:none">
                                 <td class="Titles">
                                     Url:</td>
                                 <td>
                                     <asp:TextBox ReadOnly="true" ID="txtUrlName" runat="server" style="width: 100%;" ClientIDMode="Static"></asp:TextBox>
                                 </td>
                             </tr>
                                                                              <tr id="tr_offeron" clientidmode="Static" runat="server" visible="false" >
                                 <td class="Titles">
                                     OfferOn:</td>
                                 <td>
                                                            <asp:DropDownList ID="dd_offeron"  ClientIDMode="Static" runat="server"  AutoPostBack="true" OnSelectedIndexChanged="dd_dis_SelectedIndexChanged">
                             <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="C">Category</asp:ListItem>
                            <asp:ListItem Value="SC">SubCategory</asp:ListItem>
                            <asp:ListItem Value="B">Brand</asp:ListItem>
                        </asp:DropDownList>

                        <asp:DropDownList CssClass="form-control select-cat-main" ID="ddlCategory"   AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" ClientIDMode="Static"  Visible="false"></asp:DropDownList>
             
                         <asp:DropDownList CssClass="form-control select-cat-main" ID="ddlSubCat"  ClientIDMode="Static" runat="server"></asp:DropDownList>          
                          <asp:DropDownList CssClass="form-control select-cat-main" ID="ddl_brand"  ClientIDMode="Static" runat="server" Visible="false"></asp:DropDownList>          
                                                                                    </td>
                      
                             </tr>
                                                         <tr style="display:none">
                                 <td class="Titles">
                                     OfferIn:</td>
                                 <td>
                                     <asp:DropDownList ID="dd_amt_type" CssClass="form-control select-cat-main" style="display:none" runat="server" ClientIDMode="Static">
                                         <asp:ListItem Value="D">Discount</asp:ListItem>
                                           <asp:ListItem Value="A">Amount</asp:ListItem>
                                     </asp:DropDownList>
                                 </td>
                             </tr>
                                                         <tr style="display:none">
                                 <td class="Titles">
                                     Amount/Discount:</td>
                                 <td>
                                     <asp:TextBox ID="tbamt_dis"  runat="server" style="width: 100%;" Text="0" TextMode="Number" ClientIDMode="Static"></asp:TextBox>
                                 </td>
                             </tr>

                                                   <tr>
                                 <td class="Titles">
                                     Description:</td>
                                 <td>
                                     <asp:TextBox ID="txtdesc"  TextMode="MultiLine" runat="server" style="width: 100%;" ClientIDMode="Static"></asp:TextBox>
                                 </td>
                             </tr>
                             <tr>
                                 <td class="Titles">
                                     IsActive:</td>
                                 <td>
                                       <input type="checkbox" id="chkIsActive" clientidmode="Static" runat="server" checked="checked" data-index="2"  name="chkIsActive" />
                                 </td>
                             </tr>    
                                <tr>
                                    <td id ="shopsale_insert">
                                        <asp:Button CssClass="btn btn-primary" ID="btnsubmit_shopsale" runat="server"  ClientIDMode="Static" Text="Add" OnClick="btnsubmit_shopsale_Click" />
                                    </td>
                                      <td style="display:none" id ="shopsale_update">
                                        <asp:Button CssClass="btn btn-primary" ID="btnsubmit_shopsale_update" runat="server" ClientIDMode="Static"  Text="Update" OnClick="btnsubmit_shopsale_update_Click" />
                                    </td>
                                       <td>
                                        <asp:Button CssClass="btn btn-danger" ID="btnreset_shopsale" runat="server" Text="Reset" OnClick="btnreset_shopsale_Click" />
                                    </td>
                                </tr>                         
                         </table>
                           </div>
   
          <div class="col-md-8 cst-managehome-col">
			    <h3 class="reallynow"> <span>Added Items</span> </h3>
                       <asp:GridView ID="gv_shopsale" AutoGenerateColumns="false" runat="server" class="table managehome_table tbl-width" OnRowCommand="gv_shopsale_RowCommand">
                            <Columns>
                                        <asp:TemplateField>
                                    <ItemTemplate>
                            
                                         <asp:Label ID="Label1" Style="display: none" runat="server" Text='<%# Eval("image") %>' CssClass="cls_image" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Image">
                                    <ItemTemplate>
                                        <img src='../images/shop_sale/<%# Eval("image") %>' class="small-img">
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" Style="display: none" runat="server" Text='<%# Eval("ssid") %>' CssClass="cls_ssid" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField>
                                    <ItemTemplate>
               
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("url") %>' CssClass="cls_url" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("Description") %>' CssClass="cls_desc" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OfferIn:">
                                    <ItemTemplate>
                                    <asp:Label ID="Label5" Style="display: none" runat="server" Text='<%# Eval("amt_type") %>' CssClass="cls_amt_type" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount">
                                    <ItemTemplate>
                                    <asp:Label ID="Label6" Style="display: none" runat="server" Text='<%# Eval("amt") %>' CssClass="cls_amt" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:BoundField DataField="isactive_string" HeaderText="IsActive" />
                                    <asp:TemplateField>
                                    <ItemTemplate>
               
                                    <asp:Label ID="Label7" Style="display: none" runat="server" Text='<%# Eval("isactive") %>' CssClass="cls_isactive" />
                                    </ItemTemplate>
                                </asp:TemplateField>
              <asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton  runat="server" CommandName="AddProduct" CssClass="btn btn-primary btn-small" CommandArgument='<%#Eval("ssid") %>'><i class="fa fa-plus"></i></asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>

                                             <asp:TemplateField>
                                    <ItemTemplate>
                                        <button type='button' class='btnedit btn btn-primary btn-small'><i class="fa fa-edit"></i></button>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <button type='button' class='btndel_shopsale btn btn-primary btn-small'><i class="fa fa-trash"></i></button>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                           </div>
                       </div>
                    
                <div runat="server" id="div_products" visible="false" class="second-div-show">
					<div id ="dv_search" runat="server" visible="false" class="search-section">
                 <asp:TextBox ID="txtautofill_products" ClientIDMode="Static" runat="server"></asp:TextBox> 
               
                   <asp:Button ID="btn_search" runat="server" CssClass="btn btn-primary btn-small hm_cst"  ClientIDMode="Static" Text="Search" OnClick="btn_search_Click"/>
                    </div>
                    <div class="col-md-6 cst-managehome-col">
                        <h3 class="reallynow"><span>All Products</span> </h3>
                        <%--<table class="table managehome_table" id="tbl_product">
                           <asp:Literal ID="ltr_tabledata" runat="server"></asp:Literal>
                       </table>--%>
                        <asp:GridView ID="gv_products" AutoGenerateColumns="false" AllowPaging="true" OnPageIndexChanging="OnPageIndexChanging_products" PageSize="10" runat="server" class="table managehome_table">
                            <Columns>

                                <asp:TemplateField HeaderText="Image">
                                    <ItemTemplate>
                                        <img src='../../images/product/<%# Eval("PhotoUrl") %>' class="small-img">
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" Style="display: none" runat="server" Text='<%# Eval("ProductID") %>' CssClass="productid" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                <asp:BoundField DataField="Price" HeaderText="Price" />
                                <asp:BoundField DataField="No_of_sale" HeaderText="Sale" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <button type='button' class='btnadd btn btn-primary btn-small'>Add</button>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                    </div>
                    <div class="col-md-6 cst-managehome-col">
                        <h3 class="reallynow"><span>Added Products</span> </h3>

                        <asp:GridView ID="gtv_productlist" AutoGenerateColumns="false" runat="server" class="table managehome_table">
                            <Columns>

                                <asp:TemplateField HeaderText="Image">
                                    <ItemTemplate>
                                        <img src='../../images/product/<%# Eval("PhotoUrl") %>' class="small-img">
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" Style="display: none" runat="server" Text='<%# Eval("bsid") %>' CssClass="cls_bsid" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                <asp:BoundField DataField="Price" HeaderText="Price" />

                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <button type='button' class='btndel btn btn-primary btn-small'><i class="fa fa-trash"></i></button>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                    </div>
                </div>
                <div runat="server" id="div_brands" visible="false">

                    <div class="col-md-6 cst-managehome-col">
                        <h3 class="reallynow"><span>All Brands</span> </h3>

                        <asp:GridView ID="gv_brand" AutoGenerateColumns="false" AllowPaging="true" OnPageIndexChanging="OnPageIndexChanging_brands" PageSize="10" runat="server" class="table managehome_table">
                            <Columns>
                                <asp:BoundField DataField="Title" HeaderText="Name" />
                                <asp:TemplateField HeaderText="Image">
                                    <ItemTemplate>
                                        <img src='../../images/brand/<%# Eval("image") %>' class="small-img">
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" Style="display: none" runat="server" Text='<%# Eval("brandid") %>' CssClass="cls_brandid" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <button type='button' class='btnadd btn btn-primary btn-small'>Add</button>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                    </div>
                    <div class="col-md-6 cst-managehome-col">
                        <h3 class="reallynow"><span>Added Brands</span> </h3>
                        <asp:GridView ID="gv_brandlist" AutoGenerateColumns="false" runat="server" class="table managehome_table">
                            <Columns>
                                <asp:BoundField DataField="Title" HeaderText="Name" />
                                <asp:TemplateField HeaderText="Image">
                                    <ItemTemplate>
                                        <img src='../../images/brand/<%# Eval("image") %>' class="small-img">
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" Style="display: none" runat="server" Text='<%# Eval("bid") %>' CssClass="cls_bid" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <button type='button' class='btndel btn btn-primary btn-small'><i class="fa fa-trash"></i></button>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                    </div>
                </div>
                               
            </div>

        </div>



        

    </form>

</asp:Content>

