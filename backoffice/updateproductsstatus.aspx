﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="updateproductsstatus.aspx.cs" Inherits="backoffice_updaeproductsstatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">

    
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
     <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="css/custom-css/master.css" rel="stylesheet" />

     <script src="js/jquery-ui.js"></script>
     <link rel="stylesheet" type="text/css" href="css/jquery-uiJQGrid.css" />
   <script>
       var m_ProductId = 0;
      
       function ResetControls() {
           m_ProductId = 0;
           
       }
       function Update() {
           if (!confirm("Are you sure to update Current Product's Status?")) {
               return;
           }

           var selRow = jQuery("#jQGridDemo").jqGrid('getGridParam', 'selarrrow');


        var   ProductIds = [];
        ProductIds = selRow;
        if (ProductIds.toString().trim() == "") {
               alert("Choose Product from ProductList");
               
               return;
           }
           $.ajax({
               type: "POST",
               data: '{"ProductIds":"' + ProductIds + '"}',
               url: "updateproductsstatus.aspx/Update",
               contentType: "application/json",
               datatype: "json",
               success: function (msg) {
                   var obj = jQuery.parseJSON(msg.d);
                   if (obj.Status == 1) {
                       ResetControls();
                      
                       BindGrid();
                       alert("Information has been updated");
                       return;
                   }
               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {
                   $.uiUnlock();

               }
           });
       }
       $(document).ready(
           function () {
               $("#btnIsActive").click(
                   function () {
                       Update();
                   });
           });
   </script>
    <form id="Action" runat ="server"> 
<div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Update Products Status</span>
                    </h3>

               <div id="Div1">
				    <div class="youhave">                   
      	                <table id="jQGridDemo">
                        </table>
                        <div id="jQGridDemoPager">
                        </div>   
                        <div class="category_table">
                            <div id="btnIsActive" class="btn btn-primary btn-small">Update</div>
                    </div>
                        </div> 
			        </div>
              </div>
    </div>
        </form>
    <script language="javascript" type="text/javascript">
        BindGrid();
        function BindGrid() {
            jQuery("#jQGridDemo").GridUnload();
            jQuery("#jQGridDemo").jqGrid(
            {
                url: 'handlers/UpdateProductStatus.ashx/',
                ajaxGridOptions: { contentType: "Application/json" },
                datatype: "json",
                colNames: ['ProductId', 'Name', 'IsActive'],
                colModel: [{ name: 'ProductId', key: true, index: 'ProductId', stype: 'text', sorttype: 'int', hidden: true },
                        { name: 'Name', index: 'Name', stype: 'text', width: '500px', sortable: true, editable: true, editrules: { required: true } },
                        { name: 'IsActive', index: 'IsActive', width: '100', editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true },hidden:true },
                ],
                rowNum: 30,

                mtype: 'GET',
                toolbar: [true, "top"],
                loadonce: true,
                rowList: [10, 20, 30],
                pager: '#jQGridDemoPager',
                multiselect: true,
                sortname: 'ProductId',
                viewrecords: true,
                height: "100%",
                width: "500px",
                sortorder: 'asc',
                caption: "Products List",
                ignoreCase: true,
                editurl: 'handlers/UpdateProductStatus.ashx',
                rowattr: function (rd) {
                    if (rd.IsActive == false) {
                        return { "class": "myAltRowClass" };
                    }
                }
            });
            var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                       {
                           refresh: false,
                           edit: false,
                           add: false,
                           del: false,
                           search: false,
                           searchtext: "Search",
                           addtext: "Add",
                       },

                       {//SEARCH
                           closeOnEscape: true

                       }

                         );
            $("#jQGridDemo").jqGrid('setGridParam',
{
    onSelectRow: function (rowid, iRow, iCol, e) {
        m_ProductId = $('#jQGridDemo').jqGrid('getCell', rowid, 'ProductId');
       // if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
       //     $('#btnIsActive').html("InActive");
       //     IsActive = false;
      //  }
     //   else {
      //      $('#btnIsActive').html("Active");
        //    IsActive = true;

      //  }

      //  $("#btnIsActive").css({ display: "block" });
    }
});
        }

    </script>
</asp:Content>


