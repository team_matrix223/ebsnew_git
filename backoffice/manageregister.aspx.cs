﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_manageregister : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCities();
        }

    }
    public void BindCities()
    {
        ddlCities.DataSource = new CitiesBLL().GetAll();
        ddlCities.DataTextField = "Title";
        ddlCities.DataValueField = "CityId";
        ddlCities.DataBind();
        ddlCities.Items.Insert(0,"--Select City--");
    }
    public void ResetControls()
    {
        hdnId.Value = "0";
        txtFirstName.Value = "";
        txtLastName.Value = "";
        txtEmailId.Value = "";
        txtPassword.Value = "";
        txtCPassword.Value = "";
        txtMobileNo.Value = "";
        txtTelephone.Value = "";
        ddlCities.SelectedIndex = 0;
        txtAddress.Value = "";
        txtArea.Value = "";
        txtStreet.Value = ""; 
        txtPinCode.Value = "";
        chkIsActive.Checked = true;
        chkAgree.Checked = false;
        txtRLastName.Value = "";
        txtRFirstName.Value = "";
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        
        if (ddlCities.Value == "--Select City--")
        {
            Response.Write("<script>alert('Plz choose city')</script>");
            ddlCities.Focus();
            return;
        }
        if (chkAgree.Checked == false)
        {
            return;
        }
        Users objUser = new Users ()
        {   UserId =0,
            FirstName=txtFirstName.Value.Trim(),
            LastName=txtLastName.Value.Trim(),
            EmailId=txtEmailId.Value.Trim(),
            Password=txtPassword.Value.Trim(),
            IsActive =chkIsActive.Checked ,
            AdminId = 0,
            RecipientFirstName=txtRFirstName.Value.Trim(),
            RecipientLastName=txtRLastName.Value.Trim(),
            MobileNo=txtMobileNo.Value.Trim(),
            Telephone=txtTelephone.Value.Trim(),
            CityId=Convert.ToInt32( ddlCities.Value.Trim()),
            Area=txtArea.Value.Trim(),
            Street=txtStreet.Value.Trim(),
            Address=txtAddress.Value.Trim(),
            PinCode=txtPinCode.Value.Trim(),
            IsPrimary=true,
        };
        int result = new UsersBLL().InsertUpdate(objUser);
        if (result == 0)
        {
            Response.Write("<script>alert('User Registraion is not completed,Plz try again')</script>");
        }
        else if (result == -1)
        {
            Response.Write("<script>alert('Current EmailId is already In Use')</script>");
        }
        else
        {
            Response.Write("<script>alert('User Registraion is successful')</script>");
            ResetControls();
        }
    }
}