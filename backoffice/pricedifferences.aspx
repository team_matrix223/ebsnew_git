﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="pricedifferences.aspx.cs" Inherits="backoffice_pricedifferences" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/custom-css/master.css" rel="stylesheet" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <script language="javascript" type="text/javascript">
        
         function RefreshGrid() {
            $('#jQGridDemo').trigger('reloadGrid');

        }


        function Update() {
            if (!confirm("Are you sure to change price?")) {
                return;
            }

            var selRow = jQuery("#jQGridDemo").jqGrid('getGridParam', 'selarrrow');


            var ProductIds = [];
            ProductIds = selRow;
            if (ProductIds.toString().trim() == "") {
                alert("Choose Product from ProductList");

                return;
            }
            $.ajax({
                type: "POST",
                data: '{"ProductIds":"' + ProductIds + '"}',
                url: "pricedifferences.aspx/Update",
                contentType: "application/json",
                datatype: "json",
                success: function (msg) {
                    var obj = jQuery.parseJSON(msg.d);
                    if (obj.Status == 1) {
                        
                        BindGrid();
                        alert("Information has been updated");
                        return;
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();

                }
            });
        }

        $(document).ready(
        function () {

            $("#btnUpdate").click(
                   function () {
                       Update();
                   });

            
     
            BindGrid();

           


        });
    </script>
        <form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnDate" runat="server"/>
<div id="content">
      
                       <div id="rightnow">
                    <h3 class="reallynow">
                        <span>Compare Prices </span>
                      
                        <br />
                    </h3>

				    <div class="youhave">
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
       <div id="btnUpdate" class="btn btn-primary btn-small category_table">Update</div>
                    </div>
			  </div>
                   </div>
                   </form>
                        <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/PriceDifferences.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",
                        colNames: ['VariationId','ProductId','ItemCode', 'Name','Unit','OnlinePrice','LocalPrice'],
                        colModel: [
                                    { name: 'VariationId', key: true, index: 'VariationId', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                   { name: 'ProductId', index: 'ProductId', width: '150px', stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                                    { name: 'ItemCode', index: 'ItemCode', width: '120px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Name', index: 'Name', width: '300px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                      { name: 'Unit', index: 'Unit', width: '100px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                        { name: 'OnlinePrice', index: 'OnlinePrice', width: '100px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                 { name: 'LocalPrice', index: 'LocalPrice', width: '100px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                  
                           
                           
                          

                        ],
                        rowNum: 30,

                        mtype: 'GET',
                           toolbar: [true, "top"],
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        multiselect: true,
                        sortname: 'VariationId',
                        viewrecords: true,
                        height: "100%",
                        width: "800px",
                        sortorder: 'asc',
                        caption: "Product List",

                        editurl: 'handlers/PriceDifferences.ashx',



                    });

 var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                       {
                           refresh: false,
                           edit: false,
                           add: false,
                           del: false,
                           search: false,
                           searchtext: "Search",
                           addtext: "Add",
                       },

                       {//SEARCH
                           closeOnEscape: true

                       }

                         );



                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                  
               
                  
                   
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>
</asp:Content>

