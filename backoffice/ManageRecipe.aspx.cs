﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;

public partial class backoffice_ManageRecipe : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRecipies();
        }
    }

    void BindRecipies()
    {
        ddlRecipes.DataSource = new RecipeBLL().GetAll();
        ddlRecipes.DataTextField = "Title";
        ddlRecipes.DataValueField = "RecipeId";
        ddlRecipes.DataBind();

        ListItem li = new ListItem();
        li.Text = "New Recipe";
        li.Value = "0";
        ddlRecipes.Items.Insert(0, li);

        ddlRecipeList.DataSource = new RecipeBLL().GetAll();
        ddlRecipeList.DataTextField = "Title";
        ddlRecipeList.DataValueField = "RecipeId";
        ddlRecipeList.DataBind();

        li = new ListItem();
        li.Text = "";
        li.Value = "0";
        ddlRecipeList.Items.Insert(0, li);


    }

    [WebMethod]
    public static string InsertIngredients(int RecipeId, string arrProductName, string arrQty, string arrUnit, string arrUrl)
    {



        string[] ProductData = arrProductName.Split(',');
        string[] QtyData = arrQty.Split(',');
        string[] UnitData = arrUnit.Split(',');
        string[] UrlData = arrUrl.Split(',');

        DataTable dt = new DataTable();
        dt.Columns.Add("ProductName");
        dt.Columns.Add("Qty");
        dt.Columns.Add("Unit");
        dt.Columns.Add("Url");
       
        for (int i = 0; i < ProductData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["ProductName"] = ProductData[i] ;
            dr["Qty"] = Convert.ToDecimal(QtyData[i]); ;
            dr["Unit"] = Convert.ToString(UnitData[i]);
            dr["Url"] = Convert.ToString(UrlData[i]); 

            dt.Rows.Add(dr);

        }


        JavaScriptSerializer ser = new JavaScriptSerializer();

        int st = new RecipeBLL().InsertRecipeIngredients(RecipeId, dt);
        var JsonData = new
        {
            Status = st,


        };
        return ser.Serialize(JsonData);

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        Recipe objRecipe = new Recipe();
        objRecipe.RecipeId = Convert.ToInt16(ddlRecipes.SelectedValue);
        objRecipe.Title = txtTitle.Text;
        objRecipe.ShortDescription = txtShortDescription.Text;
        objRecipe.Process = EditorDescription.Value;
        objRecipe.IsActive = chkIsActive.Checked;
        string ImageName = hdnImage.Value;

        if (fuImage.HasFile)
        {


            string value = "";

            if (objRecipe.RecipeId == 0)
            {

            
                value = CommonFunctions.GenerateRandomPassword(3) + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".jpg";
            }
            else
            {
                value = ImageName;
            }

            ImageName = CommonFunctions.UploadImage(fuImage, "~/RecipeImages/", true, 300, 200, false, 0, 0, value);

        }



        objRecipe.ImageUrl = ImageName;
        new RecipeBLL().InsertUpdate(objRecipe);


        if (ddlRecipes.SelectedValue == "0")
        {
            ltMessage.Text = "Recipe Added Successfully";
        }
        else
        {
            ltMessage.Text = "Recipe Updated Successfully";

        }
        BindRecipies();
        txtShortDescription.Text = "";
        txtTitle.Text = "";
        chkIsActive.Checked = false;
        EditorDescription.Value = "";
        hdnImage.Value = "";
    }
    protected void ddlRecipes_SelectedIndexChanged(object sender, EventArgs e)
    {
        Recipe objRecipe = new Recipe();
        objRecipe.RecipeId = Convert.ToInt16(ddlRecipes.SelectedValue);
        new RecipeBLL().GetById(objRecipe);
        txtTitle.Text = objRecipe.Title;
        txtShortDescription.Text = objRecipe.ShortDescription;
        EditorDescription.Value = objRecipe.Process;
        chkIsActive.Checked = objRecipe.IsActive;
        hdnImage.Value = objRecipe.ImageUrl;
        ltMessage.Text = "";
        



    }

    [WebMethod]
    public static string GetIngredients(int RecipeId)
    {
       

        var lstProducts = new RecipeBLL().GetRecipeIngredients(RecipeId);
 
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
        ProductList=lstProducts
        };
        return ser.Serialize(JsonData);

    }

}