﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Web.Script.Serialization;

public partial class backoffice_reimbursementgifts : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    [WebMethod]
    public static string Insert(int GiftId, string GiftName, decimal  Points, decimal Cash, bool   IsActive, string  GiftPhotoUrl)
    {


        ReimbursementGifts objGifts = new ReimbursementGifts()
        {
            GiftId = GiftId,
            Name = GiftName,
            Points = Points,
            Cash  = Cash ,
            PhotoUrl = GiftPhotoUrl,
            IsActive = IsActive,
           
        };

        int status = new ReimbursementGiftsBLL().InsertUpdate(objGifts);
        var JsonData = new
        {
            Gifts = objGifts,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

}