﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;

public partial class backoffice_managepreferences : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        CheckRole();
    }
    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.EMPLOYEEPREFERENCES));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where    m == Convert.ToInt16(Enums.Roles.Update).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("default.aspx");

        }
         

    }



    [WebMethod]
    public static string InsertEmployeeOrder(string masterArr)
    {
        StringBuilder strBuilder = new StringBuilder();
        string[] empData = masterArr.Split(',');
        for (int i = 0; i < empData.Length; i++)
        {
            strBuilder.Append(string.Format("insert into EmpAppOrder(EmployeeId,OrderNo)values({0},{1}); ", empData[i], i + 1));


        }
        int status = new EmployeesBLL().ChangeOrder(strBuilder.ToString());
        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Status = status,

        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string BindEmployees()
    {
        string emplyeeData = new EmployeesBLL().GetEmployeeShowInAppointment();
        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Employees = emplyeeData,

        };
        return ser.Serialize(JsonData);

    }
}