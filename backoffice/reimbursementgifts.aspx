﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="reimbursementgifts.aspx.cs" Inherits="backoffice_reimbursementgifts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/custom-css/master.css" rel="stylesheet" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <script language="javascript" type="text/javascript">
        var m_GiftId = 0;

        var GiftPhotoUrl = "";
       
   
        function ResetControls() {
            m_GiftId = 0;

            $("#txtGiftName").val("");
            $("#txtGiftName").focus();
            var btnAdd = $("#btnAdd");
            var btnUpdate = $("#btnUpdate");
            btnAdd.css({ "display": "block" });
            btnAdd.html("Add");
          
            $("#txtPoints").val("");
            $("#txtCash").val("");
           
            btnUpdate.css({ "display": "none" });
            btnUpdate.html("Update");
            $("#chkIsActive").prop("checked", "checked");
            $("#btnReset").css({ "display": "none" });
            $("#<%=fuGiftPhotoUrl.ClientID %>").val("");
            validateForm("detach");
        }
        function TakeMeTop() {
            $("html, body").animate({ scrollTop: 0 }, 500);
        }

        function RefreshGrid() {
            $('#jQGridDemo').trigger('reloadGrid');

        }

        function InsertUpdate() {
            if (!validateForm("frmOffers")) {
                return;
            }
            var Id = m_GiftId;
            var GiftName = $("#txtGiftName").val();

            var IsActive = false;

            if ($('#chkIsActive').is(":checked")) {
                IsActive = true;
            }

            var Points = $("#txtPoints").val();
            var Cash = $("#txtCash").val();          
            var PhotoUrl = "";
            var fileUpload = $("#<%=fuGiftPhotoUrl.ClientID %>").get(0);
            var files = fileUpload.files;

            var data = new FormData();
            for (var i = 0; i < files.length; i++) {
                if (files[i].name.length > 0) {
                    PhotoUrl = files[i].name;
                }

                data.append(files[i].name, files[i]);
            }

            if (m_GiftId == 0 && PhotoUrl == "") {
                alert("Please choose Image");
                return;
            }
            var PhotoUrl01 = "";
           
                var options = {};
                options.url = "handlers/ReimbursementGiftsFileUploader.ashx";
                options.type = "POST";
                options.async = false;
                options.data = data;
                options.contentType = false;
                options.processData = false;
                options.success = function (msg) {


                    var obj = jQuery.parseJSON(msg);
                    PhotoUrl01 = obj.File1;
                  
                };

                options.error = function (err) { };
                $.ajax(options);
                if ($.trim(PhotoUrl01) != "") {
                    GiftPhotoUrl = PhotoUrl01;
                }
            
            $.ajax({
                type: "POST",
                data: '{"GiftId":"' + Id + '", "GiftName": "' + GiftName + '","Points":"' + Points + '","Cash":"' + Cash  + '","IsActive": "' + IsActive + '","GiftPhotoUrl":"' + GiftPhotoUrl + '"}',
                url: "reimbursementgifts.aspx/Insert",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == 0) {

                        alert("Insertion Failed.Gift with duplicate name already exists.");
                        return;
                    }
                    if (obj.Status == -1) {

                        alert("Insertion Failed.Gift with duplicate CouponNo already exists.");
                        return;
                    }
                    if (Id == "0") {
                        ResetControls();
                        BindGrid();
                        alert("Gift is added successfully.");
                    }
                    else {
                        ResetControls();
                        BindGrid();
                        alert("Gift is Updated successfully.");
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });

        }


        $(document).ready(
        function () {
           
            BindGrid();

            $("#btnReset").click(
        function () {
            ResetControls();
        });
            $("#btnAdd").click(
        function () {

            m_GiftId = 0;
            InsertUpdate();
        }
        );


            $("#btnUpdate").click(
        function () {
            InsertUpdate();
        }
        );
        });
    </script>
        <form   runat="server" id="formID" method="post">
   
<div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Reimbursement Gifts</span>
                     
                        <br />
                    </h3>
				   <div class="youhave">

                   <table id="frmOffers" class="top_table">

                <tr>
                <td>Gift Name:</td>
                <td><input type="text" id="txtGiftName" class="inputtxt validate required" /></td>
                </tr>
                 
             
                 <tr>
                <td>Points:</td>
                <td><input type="text"  id="txtPoints" class="inputtxt validate required valNumber" /> </td>
                </tr>
                 <tr>
                <td>Cash:</td>
                <td><input type="text"  id="txtCash" class="inputtxt validate required valNumber"/> </td>
                </tr>
                    <tr>
                <td>PhotoUrl:</td>
                <td><input type="file" id="fuGiftPhotoUrl" runat="server" /></td>
                </tr>
             <tr>
                                 <td>
                                     IsActive:</td>
                                 <td>
                                       <input type="checkbox" id="chkIsActive"  checked="checked" data-index="2"  name="chkIsActive" />
                                 </td>
                             </tr> 
                  
                                            <table cellspacing="0" cellpadding="0" class="category_table">
                                            <tr>
                                            <td> <div id="btnAdd"  class="btn btn-primary btn-small" >Add</div></td>
                                            <td><div id="btnUpdate"  class="btn btn-primary btn-small" style="display:none;" >Update</div></td>
                                            <td><div id="btnReset"  class="btn btn-primary btn-small" style="display:none;width:60px;" >Cancel</div></td>
                                            </tr>
                                            </table>
                  
                  
                   </table>
                   </div>
                   </div>
                       <div id="rightnow" style="margin-top:10px">
                    <h3 class="reallynow">
                        <span>Manage Reimbursement Gifts</span>
                      
                        <br />
                    </h3>
				    <div class="youhave" style="padding-left:30px">
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>
			  </div>
                   </div>
                   </form>
                        <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ReimbursementGiftsHandler.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",
                        colNames: ['GiftId','Image', 'Name','Points','Cash','IsActive'],
                        colModel: [
                                    { name: 'GiftId', key: true, index: 'GiftId', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                      { name: 'PhotoUrl', index: 'PhotoUrl', width: '60px', align: "center", editable: true, formatter: imageFormat, unformat: imageUnFormat },
                                   { name: 'Name', index: 'Name', width: '150px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'Points', index: 'Points', width: '150px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                      { name: 'Cash', index: 'Cash', width: '200px', stype: 'text', sortable: true, editable: true, editrules: { required: true },hidden:false },
                                     { name: 'IsActive', index: 'IsActive', width: '70px', editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },


                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'GiftId',
                        viewrecords: true,
                        height: "100%",
                        width: "800px",
                        sortorder: 'asc',
                        caption: "Gidts List",

                        editurl: 'handlers/ManageOffers.ashx',



                    });

                     function imageFormat(cellvalue, options, rowObject) {
            return '<img src="../ReimbursementGiftsImages/T_' + cellvalue + '" style="width:20px;height:20px"/>';
        }
        function imageUnFormat(cellvalue, options, cell) {
            return $('img', cell).attr('src');
        }


                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_GiftId = 0;
                    validateForm("detach");
                    m_GiftId = $('#jQGridDemo').jqGrid('getCell', rowid, 'GiftId');

                    if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                        $('#chkIsActive').prop('checked', true);
                    }
                    else {
                        $('#chkIsActive').prop('checked', false);

                    }
             
                       $("#txtGiftName").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Name'));
                      $("#txtPoints").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Points'));
                      $("#txtCash").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Cash'));
                     GiftPhotoUrl=$('#jQGridDemo').jqGrid('getCell', rowid, 'PhotoUrl');
                        
                     GiftPhotoUrl = GiftPhotoUrl.toString().substring(30, GiftPhotoUrl.toString().length);
                
                    $("#txtGiftName").focus();
                    $("#btnAdd").css({ "display": "none" });
                    $("#btnUpdate").css({ "display": "block" });
                    $("#btnReset").css({ "display": "block" });
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>
</asp:Content>

