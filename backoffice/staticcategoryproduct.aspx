﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="staticcategoryproduct.aspx.cs" Inherits="backoffice_staticcategoryproduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
	<link href="css/custom-css/staticcategoryproduct.css" rel="stylesheet" />
        <form runat="server" id="formID" method="post">
			 <div id="rightnow">
				   <div class="mananagehome_top_panel">
                    <div class="mananagehome_top_panel_shadow">
    <asp:DropDownList ID="dd_ssid" runat="server" AutoPostBack="true"  OnSelectedIndexChanged="dd_ssid_SelectedIndexChanged" CssClass="select-ssid">
           <asp:ListItem Value="0">-Select-</asp:ListItem>
        <asp:ListItem Value="-1">New</asp:ListItem>
          <asp:ListItem Value="-2">Trending</asp:ListItem>
    </asp:DropDownList>
						</div>
					   </div>
				 </div>
                 <div runat="server" id="div_products" visible="false"  class="second-div-show">
                    <div class="col-md-6 cst-managehome-col">
                        <h3 class="reallynow"><span>All Products</span> </h3>
                          <asp:TextBox ID="txtautofill_products" ClientIDMode="Static" runat="server"></asp:TextBox> 
               
                   <asp:Button ID="btn_search" runat="server" CssClass="btn btn-primary btn-small hm_cst"  ClientIDMode="Static" Text="Search" OnClick="btn_search_Click"/>
                        <%--<table class="table managehome_table" id="tbl_product">
                           <asp:Literal ID="ltr_tabledata" runat="server"></asp:Literal>
                       </table>--%>
                        <asp:GridView ID="gv_products" AutoGenerateColumns="false"  OnPageIndexChanging="OnPageIndexChanging_products"  AllowPaging="true" OnRowCommand="gv_products_RowCommand" PageSize="10" runat="server" class="table managehome_table">
                            <Columns>

                                <asp:TemplateField HeaderText="Image">
                                    <ItemTemplate>
                                        <img src='../../images/product/<%# Eval("PhotoUrl") %>' class="small-img">
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" Style="display: none" runat="server" Text='<%# Eval("ProductID") %>' CssClass="productid" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                <asp:BoundField DataField="Price" HeaderText="Price" />
                                <asp:BoundField DataField="No_of_sale" HeaderText="Sale" />
                                <asp:TemplateField>
                                    <ItemTemplate>

                                          <asp:LinkButton  runat="server" CommandName="AddProduct" CssClass="btn btn-primary btn-small" CommandArgument='<%#Eval("ProductID") %>'>Add</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                    </div>
                    <div class="col-md-6 cst-managehome-col">
                        <h3 class="reallynow"><span>Added Products</span> </h3>

                        <asp:GridView ID="gtv_productlist" AutoGenerateColumns="false" OnRowCommand="gtv_productlist_RowCommand"  runat="server" class="table managehome_table">
                            <Columns>

                                <asp:TemplateField HeaderText="Image">
                                    <ItemTemplate>
                                        <img src='../../images/product/<%# Eval("PhotoUrl") %>' class="small-img">
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" Style="display: none" runat="server" Text='<%# Eval("bsid") %>' CssClass="cls_bsid" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                <asp:BoundField DataField="Price" HeaderText="Price" />

                                <asp:TemplateField>
                                    <ItemTemplate>
                                           <asp:LinkButton  runat="server" OnClientClick="return confirm('Do you want to delete the record ? ');" CommandName="DelProduct" CssClass="btn btn-primary btn-small" class="btn btn-primary btn-small" CommandArgument='<%#Eval("ProductID") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                            
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                    </div>
                </div>
            </form>
</asp:Content>

