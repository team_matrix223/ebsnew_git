﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="managesettings.aspx.cs" Inherits="backoffice_managesettings" %>


    
<asp:Content ID="Content2" ContentPlaceHolderID="cntMaster" Runat="Server">
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
  <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
  <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/custom-css/settings.css" rel="stylesheet" />

   <script src="js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   
<script language="javascript" type="text/javascript">
   

    function ResetControls() {
        $("#txtDeliveryCharge").val("");
        $("#txtMinimummCheckOutAmt").val("");
        $("#txtFreeDeliveryAmt").val("");
        $("#txtPointRate").val("");
        $("#txtReferralPoint").val("");
        $("#txtRefereePoint").val("");
        $("#btnAdd").css({ "display": "block" });
        $("#btnAdd").html("Add");

        $("#btnUpdate").css({ "display": "none" });
        $("#btnUpdate").html("Update");

      
        $("#btnReset").css({ "display": "none" });

      
        validateForm("detach");
    }



    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }

    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }

    function InsertUpdate() {

        if (!validateForm("formID")) {
            return;
        }

        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");
 
        

       
        var DeliveryCharge = $("#txtDeliveryCharge").val();
        var MinimumCheckOutAmt = $("#txtMinimummCheckOutAmt").val();
        var FreeDeliveryAmt = $("#txtFreeDeliveryAmt").val();
        var PointRate = $("#txtPointRate").val();

        var ReferralPoint = $("#txtReferralPoint").val();
        var RefereePoint = $("#txtRefereePoint").val();

        var MiniReimbursementPoint = $("#txtMiniReimbursementPoint").val();
        var MiniReimbursementCash = $("#txtMiniReimbursementCash").val();
        $.uiLock('');
        $.ajax({
            type: "POST",
            data: '{"DeliveryCharge":"' + DeliveryCharge + '", "MinimumCheckOutAmt": "' + MinimumCheckOutAmt + '","FreeDeliveryAmt":"' + FreeDeliveryAmt + '","PointRate":"' + PointRate + '","ReferralPoint":"' + ReferralPoint + '","RefereePoint":"' + RefereePoint + '","MiniReimbursementPoint":"' + MiniReimbursementPoint + '","MiniReimbursementCash":"' + MiniReimbursementCash + '"}',
            url: "managesettings.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == 0) {
                    ResetControls();


                    alert("Insertion Failed.ComboType with duplicate name already exists.");
                    return;

                }


                alert("Settings is Updated successfully.");
                GetData();
                // ResetControls();




            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();

            }



        });

    }
    function GetData() {
     $.ajax({
            type: "POST",
            data: '{}',
            url: "managesettings.aspx/GetData",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                               $("#txtDeliveryCharge").val(obj.Setting.DeliveryCharge);
                             $("#txtMinimummCheckOutAmt").val(obj.Setting.MinimumCheckOutAmt);
                             $("#txtFreeDeliveryAmt").val(obj.Setting.FreeDeliveryAmt);
                              $("#txtPointRate").val(obj.Setting.PointRate);
                             $("#txtReferralPoint").val(obj.Setting.ReferralPoint);
                              $("#txtRefereePoint").val(obj.Setting.RefereePoint);
                              $("#txtMiniReimbursementPoint").val(obj.Setting.MiniReimbursementPoint);
                              $("#txtMiniReimbursementCash").val(obj.Setting.MiniReimbursementCash);

                  
                  

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
               // $.uiUnlock();

            }
        });

    }
    $(document).ready(
    function () {
   GetData();
       

        $("#btnAdd").click(
        function () {


            InsertUpdate();


        }
        );


     





    }
    );

</script>

<form id="Form1"   runat="server" method="post">
 
<div  id="content">
       			<div id="rightnow" >
                    <h3 class="reallynow">
                        <span>Update Settings </span>
                     
                        <br />
                    </h3>
				   <div class="youhave">
                   
                   <table cellpadding="0" cellspacing="0" border="0" id="formID" class="top_table">
                        <tr>
                            <td class="headings">Delivery Charge:</td>
                            <td>
                                <input type="text" name="txtDeliveryCharge" class="inputtxt validate required valNumber" data-index="1" id="txtDeliveryCharge" />
                            </td>
                        </tr>
                        <tr>
                            <td class="headings">Minimum CheckOut Amount:</td>
                            <td>
                                <input type="text" name="txtMinimummCheckOutAmt" class="inputtxt validate required valNumber" data-index="1" id="txtMinimummCheckOutAmt" />
                            </td>
                        </tr>
                        <tr>
                            <td class="headings">Free Delivery Amount:</td>
                            <td>
                                <input type="text" name="txtFreeDeliveryAmt" class="inputtxt validate required valNumber" data-index="1" id="txtFreeDeliveryAmt" />
                            </td>
                        </tr>
                        <tr>
                            <td class="headings">Point Rate:</td>
                            <td>
                                <input type="text" name="txtPointRate" class="inputtxt validate required valNumber" data-index="1" id="txtPointRate" />
                            </td>
                        </tr>
                        <tr>
                            <td class="headings">Referral Point:</td>
                            <td>
                                <input type="text" id="txtReferralPoint" class="inputtxt validate required valNumber" data-index="1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="headings">Referee Point:</td>
                            <td>
                                <input type="text" id="txtRefereePoint" class="inputtxt validate required valNumber" data-index="1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="headings">MiniReimbursement Point:</td>
                            <td>
                                <input type="text" id="txtMiniReimbursementPoint" class="inputtxt validate required valNumber" data-index="1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="headings">MiniReimbursement Cash:</td>
                            <td>
                                <input type="text" id="txtMiniReimbursementCash" class="inputtxt validate required valNumber" data-index="1" />
                            </td>
                        </tr>
                    </table> 	
                       
                                   <table cellspacing="0" cellpadding="0" class="category_table">
                                     <tr>
                                        <td></td>
                                        <td>
                                            <div id="btnAdd" class="btn btn-primary btn-small">Update</div>
                                        </td>

                                    </tr>
                                </table>	           	 
       
                    </div>
			  </div>
               


          

            </div>
</form>

         

</asp:Content>

