﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master"  EnableEventValidation="false" ViewStateEncryptionMode="Never"  AutoEventWireup="true" CodeFile="manageproductslist.aspx.cs" Inherits="backoffice_manageproducts" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
        <asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
            <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
            <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
            <script src="js/grid.locale-en.js" type="text/javascript"></script>
            <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
            <link href="css/custom-css/master.css" rel="stylesheet" />
            <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
            <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
            <script src="js/jquery-ui.js" type="text/javascript"></script>
			<link href="css/custom-css/manageproducts.css" rel="stylesheet" />
			<link href="css/custom-css/manageproducts.css" rel="stylesheet" />
            <script type="text/javascript" src="js/jquery.uilock.js"></script>
            <script src="js/bo_customjs/manageproduct.js"></script>
            <script type="text/javascript">
               

                $(document).ready(
                    function() {
						BindGrid();
                    });
            </script>
            <form id="frmProducts" runat="server">
                <asp:HiddenField ID="hdVariationId" Value="-1" runat="server" />
    <div class="right_col manageitemmaste_right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Manage Products</h3>
                        </div>
                       
                    

                    <div class="x_panel">
                         <div class="form-group">
                                
                              
                <div class="youhave manageitemmaster_youhave" >
                     <table cellspacing="0" cellpadding="0" class="youhave-btn-table">
                                            <tr>
                                        <td style="padding-top:5px"> <div id="btnDiscontinue"  class="btn btn-primary" ><i class="fa fa-external-link"></i> Disconitued Items</div></td>
                                        <td style="padding-top:5px"> <div id="btnRefresh"  class="btn btn-success" ><i class="fa fa-edit m-right-xs"></i> Refresh</div></td>
                                            
                                                 
                                            </tr>
                                            </table>



      	          <table id="jQGridDemo">
    </table>
       <table cellspacing="0" cellpadding="0">
                                         
                                            </table>
    <div id="jQGridDemoPager">
    </div>
                </div>


                        
                    </div>


                    </div>

                     
                </div>
                <!-- /page content -->

               

            </div>

       </div>

            </form>


              <script type="text/javascript">
				  function BindGrid() {
					  jQuery("#jQGridDemo").GridUnload();
					  jQuery("#jQGridDemo").jqGrid({
						  url: 'handlers/ManageProductsList.ashx',
						  ajaxGridOptions: { contentType: "application/json" },
						  datatype: "json",

						  colNames: ['VariationId', 'ProductId', 'ItemCode', 'Description', 'Price', 'Mrp',  'Qty'],

						  colModel: [
							  { name: 'VariationId', key: true, index: 'VariationId', width: 100, stype: 'text', sorttype: 'int', hidden: false },
							  { name: 'ProductId', index: 'ProductId', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
							  { name: 'ItemCode', index: 'ItemCode', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
							  { name: 'Description', index: 'Description', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
							 // { name: 'PhotoUrl', index: 'PhotoUrl', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },

							  { name: 'Qty', index: 'Qty', width: 200, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, editable: true, hidden: false, editrules: { required: true } },
							  { name: 'Price', index: 'Price', width: 200, stype: 'text', sortable: true, editable: true, formatter: 'number', formatoptions: { decimalPlaces: 2 }, hidden: false, editrules: { required: true } },
							  { name: 'Mrp', index: 'Mrp', width: 200, stype: 'text', sortable: true, editable: true, formatter: 'number', formatoptions: { decimalPlaces: 2 }, hidden: false, editrules: { required: true } },


						  ],
						  rowNum: 10,

						  mtype: 'GET',
						  loadonce: true,
						  rowList: [10, 20, 30, 'All'],
						  pager: '#jQGridDemoPager',
						  sortname: 'ItemID',
						  viewrecords: true,
						  height: "100%",
						  width: "400px",
						  sortorder: 'asc',
						  caption: "Products List",
						  rowList: [10, 20, 30, 100000000],
						  loadComplete: function () {
							  $("option[value=100000000]").text('All');
						  },
						  editurl: 'handlers/ManageProducts.ashx',
						  ignoreCase: true,
						  toolbar: [true, "top"],


					  });


					  var $grid = $("#jQGridDemo");
					  // fill top toolbar
					  $('#t_' + $.jgrid.jqID($grid[0].id))
						  .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
					  $("#globalSearchText").keypress(function (e) {
						  var key = e.charCode || e.keyCode || 0;
						  if (key === $.ui.keyCode.ENTER) { // 13
							  $("#globalSearch").click();
						  }
					  });
					  $("#globalSearch").button({
						  icons: { primary: "ui-icon-search" },
						  text: false
					  }).click(function () {
						  var postData = $grid.jqGrid("getGridParam", "postData"),
							  colModel = $grid.jqGrid("getGridParam", "colModel"),
							  rules = [],
							  searchText = $("#globalSearchText").val(),
							  l = colModel.length,
							  i,
							  cm;
						  for (i = 0; i < l; i++) {
							  cm = colModel[i];
							  if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
								  rules.push({
									  field: cm.name,
									  op: "cn",
									  data: searchText
								  });
							  }
						  }
						  postData.filters = JSON.stringify({
							  groupOp: "OR",
							  rules: rules
						  });
						  $grid.jqGrid("setGridParam", { search: true });
						  $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
						  return false;
					  });




					  $("#jQGridDemo").jqGrid('setGridParam',
						  {

							  onSelectRow: function (rowid, iRow, iCol, e) {

								 


							}
						});

					  var DataGrid = jQuery('#jQGridDemo');
					  DataGrid.jqGrid('setGridWidth', '700');

					  $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
						  {
							  refresh: false,
							  edit: false,
							  add: false,
							  del: false,
							  search: false,
							  searchtext: "Search",
							  addtext: "Add",
						  },

						  {//SEARCH
							  closeOnEscape: true

						  }

					  );



				  }





    </script>

        </asp:Content>