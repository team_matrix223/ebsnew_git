﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="managerecharge.aspx.cs" Inherits="backoffice_managerecharge" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">


  <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   
<script language="javascript" type="text/javascript">

    var m_RechargeId = 0;

    function ResetControls() {
        m_RechargeId = 0;
        $("#<%=ddlValues.ClientID%>").val() == "0";
        $("#hdnId").val("0");
        validateForm("detach");
    }
    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }

    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }

    



    function InsertUpdate() {
        if (!validateForm("frmRecharge")) {
            return;
        }
        var Id = m_RechargeId;
        var Amount = $("#<%=ddlValues.ClientID%>").val(); ;
        if ($.trim(Amount) == "") {
            $("#<%=ddlValues.ClientID%>").focus(); 

            return;
        }


        $.ajax({
            type: "POST",
            data: '{ "Amount": "' + Amount + '"}',
            url: "managerecharge.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == 0) {

                    alert("Insertion Failed.");
                    return;
                }

                if (Id == "0") {
                    ResetControls();
                    alert("Wallet Recharged successfully.");
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });

    }


    $(document).ready(
    function () {
       
       
       

        $("#btnAdd").click(
        function () {

            m_RechargeId = 0;
           
            InsertUpdate();
        }
        );



    }
    );

</script>



<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
<div id="content" style="padding:6px">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>My Wallet</span>
                     
                        <br />
                    </h3>
				   <div class="youhave" style="padding-left:30px">
                   <table cellpadding="0" cellspacing="0" border="0" id="frmCity">
                     
                   
                     <tr><td class="headings">Amount:</td>
                      <td><asp:DropDownList ID="ddlValues" runat="server" Width="80px">
                                      <asp:ListItem Text="1000" Value="1000"></asp:ListItem>                                    
                                     <asp:ListItem Text="2000" Value="2000"></asp:ListItem>
                                     <asp:ListItem Text="3000" Value="3000"></asp:ListItem>
                                     <asp:ListItem Text="4000" Value="4000"></asp:ListItem>
                                     <asp:ListItem Text="5000" Value="5000"></asp:ListItem>
                                     <asp:ListItem Text="6000" Value="6000"></asp:ListItem> 
                                     <asp:ListItem Text="7000" Value="7000"></asp:ListItem>  
                                     <asp:ListItem Text="8000" Value="8000"></asp:ListItem>
                                     <asp:ListItem Text="9000" Value="9000"></asp:ListItem> 
                                     <asp:ListItem Text="10000" Value="10000"></asp:ListItem>                                                                            
                                     </asp:DropDownList>


                            </td>
                     </tr>
                                                    
                       
                      
                     
                      
                                            <tr>
                                             
                                            <td colspan="100%" style="padding-left:45px" >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd"  class="btn btn-primary btn-small" >Recharge</div></td>
                                            
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>
                    			 
       
                    </div>
			  </div>
               


               <div id="Div1" style="margin-top:10px">
                    <h3 class="reallynow">
                        <span>My Recharges </span>
                      
                        <br />
                    </h3>
				  <table style="border: 0px solid black;margin: 1px" cellpadding="5" cellspacing="0" width="450px">
                  <tr>
                  <td>
                  
                  <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False"  
                          BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" 
                          CellPadding="3" GridLines="Vertical" 
                    AllowPaging="true" Width = "450px" onrowdatabound="GridView2_RowDataBound" 
                         >
                    <AlternatingRowStyle BackColor="#DCDCDC" />
                    <Columns>
                           <asp:TemplateField HeaderText="RechargeID" Visible="true">
                            <ItemTemplate>
                              <asp:Literal ID="ltRechargeID" runat="server" Text='<%#Eval("RechargeID") %>'></asp:Literal>                              
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="DateOfRecharge" Visible="true">
                            <ItemTemplate>
                              <asp:Literal ID="ltDateOfRecharge" runat="server" Text='<%#Eval("DateOfRecharge", "{0:dd/MM/yyyy}") %>'></asp:Literal>                              
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Amount" Visible="true">
                            <ItemTemplate>
                              <asp:Literal ID="ltAmount" runat="server" Text='<%#Eval("Amount") %>'></asp:Literal>                              
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="UsedAmount">
                            <ItemTemplate>
                                <asp:Literal ID="ltUsedAmount" runat="server" Text='<%#Eval("UsedAmount") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField>
                          
                       </asp:TemplateField>
                    </Columns>
                  <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"  FirstPageText="First" LastPageText="Last"/> 
                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#000065" />
                </asp:GridView>
      
                  
                  
                  </td>
                  
                  </tr>
                  
                  </table>
			  </div>

               <table>
                     <tr><td >Total Recharge:</td><td>
                     <asp:Label ID="lbltotal" runat="server" /> </td></tr>

                     <tr><td>Used Amount:</td><td> <asp:Label ID="lblusedamt" runat="server" /> </td></tr>
                     <tr><td >Left Amount:</td><td> <asp:Label ID="lblleft" runat="server" /> </td></tr>
                    
                     </table>


            </div>
</form>

 



</asp:Content>

