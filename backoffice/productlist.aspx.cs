﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_productlist : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string BindCat1()
    {

        string Cat1 = new CategoryBLL().GetOptions();
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            Category1 = Cat1,
        };
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string BindCat2(int CatId)
    {

        string Cat2 = new CategoryBLL().GetOptions(CatId);

        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {

            Category2 = Cat2,

        };
        return ser.Serialize(JsonData);
    }
}