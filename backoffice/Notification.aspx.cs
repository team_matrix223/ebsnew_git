﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class backoffice_Notification : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string GetNotify()
    {
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var Notify = new NotificationBLL().GetNotificationDatewise();
        
        var JsonData = new
        {
            Notify = Notify,

        };
        return ser.Serialize(JsonData);

    }
}