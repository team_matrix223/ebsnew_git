﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true"
    CodeFile="managecategory.aspx.cs" Inherits="backoffice_managecategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" runat="Server">
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/custom-css/master.css" rel="stylesheet" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

    <style>
#sortable { list-style-type: none;   padding: 0; width: 100%; }
#sortable li {margin: 0px; padding-left: 5px; font-size: 12px; height: auto;}
 #sortableFeatured { list-style-type: none;   padding: 0; width: 100%; }
#sortableFeatured li {margin: 0px; padding-left: 5px; font-size: 12px; height: auto;}

  .ui-state-default2 
{
background:#e6e6e6;
border:1px solid #d3d3d3;
color:#555555;
font-weight:normal;
text-indent:19px;
padding:5px;

}



.ui-state-default1  
{
background:#e6e6e6;
border:1px solid #d3d3d3;
color:#555555;
font-weight:normal;
text-indent:19px;
padding:5px;

}
</style>
    <script language="javascript" type="text/javascript">
        var m_CategoryId = 0;
        var EventPhotoUrl = "";
      
        function ResetControls() {
            m_CategoryId = 0;
            var txtTitle = $("#txtTitle");
            var btnAdd = $("#btnAdd");
            var btnUpdate = $("#btnUpdate");

            txtTitle.focus();
            txtTitle.val("");
            $("#txtDescription").val("");

            $("#txtMetaTitle").val("");
            $("#txtMetaDescription").val("");
            $("#txtMetaKeyword").val("");
          

            btnAdd.css({ "display": "block" });
            btnAdd.html("Add Category");

            btnUpdate.css({ "display": "none" });
            btnUpdate.html("Update Categroy");

            $("#chkIsActive").prop("checked", "checked");
            $("#chkShowOnHome").prop("checked", "");

            $("#btnReset").css({ "display": "none" });

            $("#hdnId").val("0");
            validateForm("detach");
        }



        function TakeMeTop() {
            $("html, body").animate({ scrollTop: 0 }, 500);
        }

        function RefreshGrid() {
            $('#jQGridDemo').trigger('reloadGrid');

        }

        function InsertUpdate() {

            if (!validateForm("formID")) {
                return;
            }

            var btnAdd = $("#btnAdd");
            var btnUpdate = $("#btnUpdate");

            var Id = m_CategoryId;


            var Title = $("#txtTitle").val();

            var MetaTitle = $("#txtMetaTitle").val();
            var MetaKeyword = $("#txtMetaKeyword").val();
            var MetaDescription = $("#txtMetaDescription").val();


            if ($.trim(Title) == "") {
                $("#txtTitle").focus();

                return;
            }
            $.uiLock('');
            if (Id == "0") {
                btnAdd.unbind('click');
            }
            else {
                btnUpdate.unbind('click');
            }

            btnAdd.html("<img src='img/loader.gif' alt='loading...'/>")
            btnUpdate.html("<img src='img/loader.gif' alt='loading...'/>")
            var Description = $("#txtDescription").val();
            var IsActive = false;
            var ShowOnHome = false;


            if ($('#chkIsActive').is(":checked")) {
                IsActive = true;
            }

            if ($('#chkShowOnHome').is(":checked")) {
                ShowOnHome = true;
            }


            var photourl = "";
            var fileUpload = $("#FileUpload1").get(0);
            var files = fileUpload.files;

            var data = new FormData();
            for (var i = 0; i < files.length; i++) {
                if (files[i].name.length > 0) {
                    photourl = files[i].name;
                }

                data.append(files[i].name, files[i]);
            }

            if (m_CategoryId == 0 && photourl == "") {
                alert("Please select Image for Category");
                return;
            }

            if (files.length > 0) {
                var options = {};
                options.url = "handlers/CategoryFileUploader.ashx";
                options.type = "POST";
                options.async = false;
                options.data = data;
                options.contentType = false;
                options.processData = false;
                options.success = function (msg) {

                    photourl = msg;

                };

                options.error = function (err) { };

                $.ajax(options);



                if (photourl.length > 0) {
                    EventPhotoUrl = photourl;
                }
            }

            
         


            $.ajax({
                type: "POST",
                data: '{"id":"' + Id + '", "title": "' + Title + '","Description":"' + Description + '","isActive": "' + IsActive + '","ShowOnHome": "' + ShowOnHome + '","MetaTitle": "' + MetaTitle + '","MetaKeyword": "' + MetaKeyword + '","MetaDescription": "' + MetaDescription + '","PhotoUrl": "' + EventPhotoUrl + '"}',
                url: "managecategory.aspx/Insert",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == 0) {
                        ResetControls();


                        if (Id == "0") {
                            btnAdd.bind('click', function () { InsertUpdate(); });
                        }
                        else {

                            btnUpdate.bind('click', function () { InsertUpdate(); });
                        }

                        alert("Insertion Failed.Category with duplicate name already exists.");
                        return;

                    }

                    if (Id == "0") {
                        btnAdd.bind('click', function () { InsertUpdate(); });
                        jQuery("#jQGridDemo").jqGrid('addRowData', obj.Category.CategoryId, obj.Category, "last");

                        ResetControls();

                        alert("Category is added successfully.");

                    }
                    else {
                        btnUpdate.bind('click', function () { InsertUpdate(); });
                        ResetControls();
                        var myGrid = $("#jQGridDemo");
                        var selRowId = myGrid.jqGrid('getGridParam', 'selrow');


                        myGrid.jqGrid('setRowData', selRowId, obj.Category);

                        alert("Category is Updated successfully.");


                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();

                }



            });

        }








        $(document).ready(
    function () {

        $.ajax({
            type: "POST",
            data: '{}',
            url: "managecategory.aspx/BindFeaturedCategories",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                var len = obj.CategoryData.length;
                var html = "";
                for (var i = 0; i < len; i++) {

                    html = html + "<li class='ui-state-default2'  id='" + obj.CategoryData[i].CategoryId + "'>" + obj.CategoryData[i].Title + "</li>";
                }


                $("#sortableFeatured").html(html);


            }


        });








        $.ajax({
            type: "POST",
            data: '{}',
            url: "managecategory.aspx/BindCategories",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                var len = obj.CategoryData.length;
                var html = "";
                for (var i = 0; i < len; i++) {

                    html = html + "<li class='ui-state-default1'  id='" + obj.CategoryData[i].CategoryId + "'>" + obj.CategoryData[i].Title + "</li>";
                }


                $("#sortable").html(html);


            }


        });



        $("#btnAdd").click(
        function () {

            m_CategoryId = 0;
            InsertUpdate();


        }
        );


        $("#btnUpdate").click(
        function () {
            InsertUpdate();


        }
        );


        $("#jQGridDemo").jqGrid('setGridParam',
         {
             onSelectRow: function (rowid, iRow, iCol, e) {
                 m_CategoryId = 0;
                 validateForm("detach");
                 var txtTitle = $("#txtTitle");
                 m_CategoryId = $('#jQGridDemo').jqGrid('getCell', rowid, 'CategoryId');


                 if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                     $('#chkIsActive').prop('checked', true);
                 }
                 else {
                     $('#chkIsActive').prop('checked', false);

                 }


                 if ($('#jQGridDemo').jqGrid('getCell', rowid, 'ShowOnHome') == "true") {
                     $('#chkShowOnHome').prop('checked', true);
                 }
                 else {
                     $('#chkShowOnHome').prop('checked', false);

                 }

                 var finalUrl = "";
                 txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Title'));
                 txtTitle.focus();
                 $("#txtDescription").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Description'))

                 $("#txtMetaTitle").val($('#jQGridDemo').jqGrid('getCell', rowid, 'MetaTitle'))
                 $("#txtMetaDescription").val($('#jQGridDemo').jqGrid('getCell', rowid, 'MetaDescription'))
                 $("#txtMetaKeyword").val($('#jQGridDemo').jqGrid('getCell', rowid, 'MetaKeyword'))
                 EventPhotoUrl = $('#jQGridDemo').jqGrid('getCell', rowid, 'PhotoUrl');
                
                 finalUrl = "../CategoryImages/" + EventPhotoUrl;

                 $("#CatImg").attr("src", finalUrl);
                 $("#FileUpload1").val("");
                 $("#FileUpload2").val("");
                 $("#btnAdd").css({ "display": "none" });
                 $("#btnUpdate").css({ "display": "block" });
                 $("#btnReset").css({ "display": "block" });
                 TakeMeTop();
             }
         });

        var DataGrid = jQuery('#jQGridDemo');
        DataGrid.jqGrid('setGridWidth', '');




        $('#txtTitle').focus();
        $('#txtTitle').keypress(function (event) {


            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {

                InsertUpdate();
            }


        });


        $("#btnReset").click(
        function () {

            ResetControls();

        }
        );


    }
    );








    $(function () {

        $("#sortableFeatured").sortable({ update: function (event, ui) {


            $.uiLock('');

            var masterArr = [];
            $(".ui-state-default2").each(
            function (x) {

              

                masterArr[x] = $(this).attr("id");

            }
            );


            $.ajax({
                type: "POST",
                data: '{"masterArr":"' + masterArr + '","Type":"' + "2" + '"}',
                url: "managecategory.aspx/ChangeOrder",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    if (obj.Status == "0") {

                        alert("Change Order Operation failed. Please try again later");
                        return;
                    }
                    alert("Orders Changed Successfully");


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }


            });

        }
        });
        $("#sortableFeatured").disableSelection();



        $("#sortable").sortable({ update: function (event, ui) {


            $.uiLock('');

            var masterArr = [];
            $(".ui-state-default1").each(
            function (x) {

                masterArr[x] = $(this).attr("id");

            }
            );


            $.ajax({
                type: "POST",
                data: '{"masterArr":"' + masterArr + '","Type":"' + "1" + '"}',
                url: "managecategory.aspx/ChangeOrder",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    if (obj.Status == "0") {

                        alert("Change Order Operation failed. Please try again later");
                        return;
                    }
                    alert("Orders Changed Successfully");


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }


            });

        }
        });
        $("#sortable").disableSelection();
    });
    </script>
    <form runat="server" id="formID" method="post">
    <asp:HiddenField ID="hdnCategory" runat="server" />
    <div id="content">
        <div id="rightnow">
            <h3 class="reallynow">
                <span>Add/Update Category</span>
                <br />
            </h3>
            <div class="youhave">
                <table cellpadding="0" cellspacing="0" border="0" class="top_table">
                    <tr>
                        <td class="headings">
                            Title:
                        </td>
                        <td>
                            <input type="text" name="txtTitle" class="inputtxt validate required"
                                data-index="1" id="txtTitle" />
                        </td>
                    </tr>
                    <tr>
                        <td class="headings">
                            Description:
                        </td>
                        <td>
                            <textarea rows="3" name="txtDescription" class="inputtxt" id="txtDescription"></textarea>
                        </td>
                    </tr>
                    <tr>

                   <td class="headings">PhotoUrl:</td><td> 
                    
                   <input type="file" id="FileUpload1" />
                      
                     
                     </td></tr>

                     
                      <tr>
                        <td class="headings">
                            Is Active:
                        </td>
                        <td>
                            <input type="checkbox" id="chkIsActive" checked="checked" data-index="2" name="chkIsActive" />
                        </td>
                    </tr>
                     <tr>
                        <td class="headings">
                           Show On Home:
                        </td>
                        <td>
                            <input type="checkbox" id="chkShowOnHome"   data-index="2" name="chkShowOnHome" />
                        </td>
                    </tr>
                    </table>

                <div class="upload_img">
                    <img id="CatImg" src=' ' />
                </div>


                   <table class="midle_table">
                   <tr>
                   <td colspan="100%"  > 
                    <h3 class="reallynow">
                <span>META INFORMATION FOR SEO</span>
                <br />
            </h3>
                   </td>
                   </tr>

                       <tr>
                        <td class="headings">
                            Meta Title:
                        </td>
                        <td>
                            <input type="text" name="txtMetaTitle" class="inputtxt validate required"
                                data-index="1" id="txtMetaTitle" />
                        </td>
                    </tr>


                      <tr>
                        <td class="headings">
                            Meta Keyword:
                        </td>
                        <td>
                            <input type="text" name="txtMetaKeyword" class="inputtxt validate required"
                                data-index="1" id="txtMetaKeyword" />
                        </td>
                    </tr>


                      <tr>
                        <td class="headings">
                            Meta Description:
                        </td>
                        <td>
                            <input type="text" name="txtMetaDescription" class="inputtxt validate required"
                                data-index="1" id="txtMetaDescription" />
                        </td>
                    </tr>

                   </table>
                    

                            <table cellspacing="0" cellpadding="0" class="category_table">
                                <tr>
                                    <td>
                                        <div id="btnAdd" class="btn btn-primary btn-small">
                                            Add Category</div>
                                    </td>
                                    <td>
                                        <div id="btnUpdate" class="btn btn-primary btn-small" style="display: none;">
                                            Update Category</div>
                                    </td>
                                    <td>
                                        <div id="btnReset" class="btn btn-primary btn-small" style="display: none;">
                                            Cancel</div>
                                    </td>
                                </tr>
                            </table>

            </div>
        </div>
        <div id="rightnow">
            <h3 class="reallynow">
                <span>Manage Categories</span>
                <br />
            </h3>
            <div class="youhave">

              <table id="jQGridDemo" class="categiry_grid">
                </table>
                <div id="jQGridDemoPager">
                </div>

            <table class="manage_category_table">
            <tr>
                <td><h3>MANAGE CATEGORY ORDER</h3></td>
            </tr>
            <tr>
                <td>
           		 <div style="height:255px;overflow-y:scroll" title="Select and Drag to Change Order">
                     <ul id="sortable" style="cursor:move" ></ul>
                 </div>
               </td>
            </tr>
            </table>

            <table class="manage_category_table" style="float:right;">
                <tr>
                    <td><h3>FEATURED CATEGORY ORDER</h3></td>
                </tr>
                <tr>
                    <td>
                        <div style="height:90px;overflow-y:scroll" title="Select and Drag to Change Order">               
                          <ul id="sortableFeatured" style="cursor:move" ></ul>
                        </div>            
                   </td>
                </tr>
            
            </table>
            


              
            </div>
        </div>
    </div>
    </form>
    <script type="text/javascript">
     
    
        jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/ManageCategories.ashx/',
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['Category Id', 'Title','Meta Title','Meta Keyword','Meta Description','PhotoUrl','Description', 'Active','Featured'],
            colModel: [
                        { name: 'CategoryId',key:true, index: 'CategoryId', width: 100, stype: 'text',sorttype:'int',hidden:true },
   		                { name: 'Title', index: 'Title', width: 170, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
                        { name: 'MetaTitle', index: 'MetaTitle', width: 170, stype: 'text', sortable: true, editable: true ,editrules: { required: true ,hidden:true}},
                        { name: 'MetaKeyword', index: 'MetaKeyword', width: 170, stype: 'text', sortable: true, editable: true ,editrules: { required: true  ,hidden:true}},
                        { name: 'MetaDescription', index: 'MetaDescription', width: 170, stype: 'text', sortable: true, editable: true ,editrules: { required: true ,hidden:true }},
                        { name: 'PhotoUrl', index: 'PhotoUrl', width: 150, stype: 'text', sortable: true, editable: true,hidden:true ,editrules: { required: true }},
                        
                        { name: 'Description', index: 'Description', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true },hidden:true },
   		                { name: 'IsActive', index: 'IsActive', width: 75, editable: true,edittype:"checkbox",editoptions: { value:"true:false" } , formatter: "checkbox", formatoptions: { disabled: true} },
                        { name: 'ShowOnHome', index: 'ShowOnHome', width: 85, editable: true,edittype:"checkbox",editoptions: { value:"true:false" } , formatter: "checkbox", formatoptions: { disabled: true} },
 
   		                  
                       ],
            rowNum: 10,
          
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'CategoryId',
            viewrecords: true,
            height: "100%",
            width:"400px",
            sortorder: 'asc',
            caption: "Category List",
         
            editurl: 'handlers/ManageCategories.ashx',
         
                    
             
        });


 
        $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: false,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }

                     );

                   
              
    </script>
</asp:Content>
