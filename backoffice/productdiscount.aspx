﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="productdiscount.aspx.cs" Inherits="backoffice_productdiscount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
	<link href="css/custom-css/productdiscount.css" rel="stylesheet" />
       <form id="frmProducts" runat="server">
           <asp:HiddenField ID="hdndisid" ClientIDMode="Static" runat="server" />
           <div id="content">
            
            <div id="rightnow">
                <h3 class="reallynow">
                    <span>Manage Discount</span>
                </h3>

                <div class="mananagehome_top_panel ">
					 <div class="mananagehome_top_panel_shadow">
						 <div class="headings">Discount On:</div>
                                 <asp:DropDownList ID="dd_dis"  ClientIDMode="Static" runat="server" CssClass="form-control select-cat-main" AutoPostBack="true" OnSelectedIndexChanged="dd_dis_SelectedIndexChanged">
                             <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="C">Category</asp:ListItem>
                            <asp:ListItem Value="SC">SubCategory</asp:ListItem>
                            <asp:ListItem Value="B">Brand</asp:ListItem>
                        </asp:DropDownList>
						 </div>
                    </div>
				 <div class="mananagehome_top_panel second-select" id="dv_cat" runat="server" visible="false">
					  <div class="mananagehome_top_panel_shadow">
									<label  class="headings">Category:</label>
								   <asp:DropDownList ID="ddlCategory"  ClientIDMode="Static" runat="server" CssClass="form-control"></asp:DropDownList>
						  <label  class="headings">Discount %:</label>
								<asp:TextBox ID="tbdis" ClientIDMode="Static" required="required" TextMode="Number" runat="server" CssClass="form-control"></asp:TextBox>
								   <asp:TextBox ID="tbstart_date" required="required" ClientIDMode="Static" TextMode="Date" runat="server" CssClass="form-control"></asp:TextBox>
								   <asp:TextBox ID="tbend_date" required="required" ClientIDMode="Static" TextMode="Date" runat="server" CssClass="form-control"></asp:TextBox>
								<asp:Button ID="btn_submit" runat="server" Text="Add" CssClass="btn btn-primary" OnClick="btn_submit_Click"/>
						
						</div>
					</div>
                		 <div class="mananagehome_top_panel second-select" id="dv_brand" runat="server" visible="false">
					  <div class="mananagehome_top_panel_shadow">
									<label  class="headings">Brand:</label>
								   <asp:DropDownList ID="ddl_brand"  ClientIDMode="Static" runat="server" CssClass="form-control"></asp:DropDownList>
						  <label  class="headings">Discount %:</label>
								<asp:TextBox ID="tbdis_b" ClientIDMode="Static" required="required" TextMode="Number" runat="server" CssClass="form-control"></asp:TextBox>
								   <asp:TextBox ID="tbstart_date_b" ClientIDMode="Static" required="required" TextMode="Date" runat="server" CssClass="form-control"></asp:TextBox>
								   <asp:TextBox ID="tbend_date_b" ClientIDMode="Static" required="required" TextMode="Date" runat="server" CssClass="form-control"></asp:TextBox>
								<asp:Button ID="btn_submit_b" runat="server" Text="Add" CssClass="btn btn-primary" OnClick="btn_submit_Click_b"/>
						
						</div>
					</div>
				 <div class="mananagehome_top_panel second-select" id="dv_subcat" runat="server" visible="false">
					  <div class="mananagehome_top_panel_shadow">
              
                    <label  class="headings">Category:</label>  <asp:DropDownList ID="ddlCategory_"  ClientIDMode="Static" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlCategory__SelectedIndexChanged"></asp:DropDownList>
                    <label  class="headings">Sub Category:</label>  <asp:DropDownList ID="ddlSubCat"  ClientIDMode="Static" runat="server"></asp:DropDownList>
                     <label  class="headings">Discount %:</label>      <asp:TextBox ID="tbdis_" required="required" ClientIDMode="Static" runat="server" TextMode="Number"></asp:TextBox>
                       <asp:TextBox ID="tbstart_date_" ClientIDMode="Static" required="required" TextMode="Date" runat="server"></asp:TextBox>
                       <asp:TextBox ID="tbend_date_" ClientIDMode="Static" required="required" TextMode="Date" runat="server"></asp:TextBox>
                    <asp:Button ID="btn_submit_" runat="server" Text="Add" CssClass="btn btn-primary" OnClick="btn_submit_Click_"/>
             
						  </div>
					 </div>
                <div class="table-div">
                    <asp:GridView ID="gv_display" OnRowCommand="gv_display_RowCommand" runat="server" AutoGenerateColumns="false" CssClass="table">
                        <Columns>
                  
                                        <asp:TemplateField HeaderText="Type">
                                    <ItemTemplate>
               
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("dis_type") %>' CssClass="cls_dis_type" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Title">
                                    <ItemTemplate>
               
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("title") %>' CssClass="cls_title" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Discount%">
                                    <ItemTemplate>
               
                                    <asp:Label ID="Label13" runat="server" Text='<%# Eval("dis_per") %>' CssClass="cls_dis_per" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                           <asp:TemplateField HeaderText="Start Date">
                                    <ItemTemplate>
               
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("start_date") %>' CssClass="cls_start_date" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="End Date">
                                    <ItemTemplate>
               
                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("end_date") %>' CssClass="cls_end_date" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField>
                                    <ItemTemplate>
                                       
                                              <asp:LinkButton OnClientClick="return confirm('Are you sure?');" runat="server" ID="Lnkedit" class='btnedit btn btn-primary btn-small' CausesValidation="false" ForeColor="Black" Font-Bold="false" CommandName="_delete" CommandArgument='<%# Eval("dis_id") %>'>
                                                                                          <i class="fa fa-trash"></i>
                                                                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
               
                        </Columns>

                    </asp:GridView>
                </div>
            </div>
               </div>
           </form>
</asp:Content>

