﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="viewreviews.aspx.cs" Inherits="backoffice_viewreviews" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
<script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/custom-css/master.css" rel="stylesheet" />
    
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

    <script type="text/javascript">


        $(document).ready(
        function () {
            BindGrid();


            $("#btnApproved").click(
        function () {


            InsertUpdate();

        }
        );

            $("#btnDisApproved").click(
        function () {


            InsertDisApprove();

        }
        );

        });




        function InsertUpdate() {

            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

            var selRow = jQuery("#jQGridDemo").jqGrid('getGridParam', 'selarrrow');

            var Reviews = [];
            for (i = 0; i < selRow.length; i++) {

                var ReviewId = $('#jQGridDemo').jqGrid('getCell', selRow[i], 'ReviewId');
                Reviews = Reviews + ReviewId + ',';
            }
            Reviews = Reviews.slice(0, -1);
           


            ReviewId = selRow;
            if (ReviewId.toString().trim() == "") {
                alert("Choose Review from Reviews List");
                $.uiUnlock();
                return;
            }

            $.ajax({
                type: "POST",
                data: '{ "arrReview": "' + Reviews + '"}',

                url: "viewreviews.aspx/Insert",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {


                    alert("Reviews Approved Successfully");
                    BindGrid();
                  

               },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    $.uiUnlock();
                }

            });
        }



        function InsertDisApprove() {

            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

            var selRow = jQuery("#jQGridDemo").jqGrid('getGridParam', 'selarrrow');

            var Reviews = [];
            for (i = 0; i < selRow.length; i++) {

                var ReviewId = $('#jQGridDemo').jqGrid('getCell', selRow[i], 'ReviewId');
                Reviews = Reviews + ReviewId + ',';
            }
            Reviews = Reviews.slice(0, -1);



            ReviewId = selRow;
            if (ReviewId.toString().trim() == "") {
                alert("Choose Review from Reviews List");
                $.uiUnlock();
                return;
            }

            $.ajax({
                type: "POST",
                data: '{ "arrReview": "' + Reviews + '"}',

                url: "viewreviews.aspx/InsertDisApprove",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {


                    alert("Reviews Disapproved Successfully");
                    BindGrid();


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    $.uiUnlock();
                }

            });
        }

    
    </script>


       <form   runat="server" id="formID" method="post">

     <asp:HiddenField ID="hdnDate" runat="server"/>
  
<div id="content">

             <div id="rightnow">
            <h3 class="reallynow">
                <span>Reviews</span>
                <br />
            </h3>
            <div class="youhave">

         

                <table id="jQGridDemo">
                </table>
                <div id="jQGridDemoPager">
                </div>
            


                                          <table class="category_table" cellspacing="0" cellpadding="0">
                                             <tr>
                                              <td> <div id="btnApproved"  class="btn btn-primary btn-small" >Approve</div></td>
                                              <td> <div id="btnDisApproved"  class="btn btn-primary btn-small" >DisApprove</div></td>                                           
                                             </tr>
                                            </table>
            </div>
        </div>
   
            </div>




  
</form>



<script type="text/javascript">
         function BindGrid()
     {
      
      jQuery("#jQGridDemo").GridUnload();

              jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/ManageReviews.ashx',
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['ReviewId','UserId','UserName','Title', 'Description', 'Rating','IsApproved','ReviewDate'],
            colModel: [
              { name: 'ReviewId',key:true, index: 'ReviewId', width: 100, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
                        { name: 'UserId', index: 'UserId', width: 150, stype: 'text', sortable: true, hidden: true, editable: true ,editrules: { required: true }},
                        { name: 'UserName', index: 'UserName', width: 150, stype: 'text', sortable: true, hidden: false, editable: true ,editrules: { required: true }},
                        { name: 'Title', index: 'Title', width: 150, stype: 'text', sortable: true, hidden: false, editable: true ,editrules: { required: true }},
   		                { name: 'Description', index: 'Description', width: 150, stype: 'text', sortable: true,hidden:false, hidden: false, editable: true ,editrules: { required: true }},
                        { name: 'Rating', index: 'Rating', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                         { name: 'IsApproved', index: 'IsApproved', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                        { name: 'strRD', index: 'strRD', width: 200, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                      
                      
                       ],
            rowNum: 10,
           toolbar: [true, "top"],
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
             multiselect:true,
            sortname: 'BillId',
            viewrecords: true,
            height: "100%",
            width:"400px",
            sortorder: 'desc',
            caption: "Reviews List",
            ignoreCase: true,
           
                    
             
        });

        var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });


           $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: false,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }

                     );
         
    
        var DataGrid = jQuery('#jQGridDemo');
        DataGrid.jqGrid('setGridWidth', '686');





    



        

      }
        
    </script>

</asp:Content>

