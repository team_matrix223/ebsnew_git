﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="migratecategory.aspx.cs" Inherits="backoffice_migratecategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
     
 <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
      <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
        <link rel="stylesheet" href="docsupport/prism.css">
  <link rel="stylesheet" href="js/chosen.css">
  <style type="text/css" media="all">
    /* fix rtl for demo */
    .chosen-rtl .chosen-drop { left: -9000px; }
  </style>


      <script src="js/chosen.jquery.js" type="text/javascript"></script>

 <script language="javascript" type="text/javascript">

     doChosen();
     function doChosen() {

         var config = {
             '.chosen-select': {},
             '.chosen-select-deselect': { allow_single_deselect: true },
             '.chosen-select-no-single': { disable_search_threshold: 10 },
             '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
             '.chosen-select-width': { width: "95%" }
         }
         for (var selector in config) {
             $(selector).chosen(config[selector]);
         }

     }
 </script>

<form   runat="server" id="formID" method="post">
 
<div id="content" style="padding:6px">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Migrate Categories</span>
                     
                        <br />
                    </h3>
				   <div class="youhave" style="padding-left:30px">
                   <table cellpadding="0" cellspacing="0" border="0" id="frmCity">
                     
                   
                     <tr><td class="headings">Department:</td><td> 
                     
                     
                     <asp:DropDownList ID="ddlDepartments" AutoPostBack="true" runat="server" 
                             onselectedindexchanged="ddlDepartments_SelectedIndexChanged">
                     <asp:ListItem Text="Account" Value="1"></asp:ListItem>
                     <asp:ListItem Text="Payroll" Value="2"></asp:ListItem>
                     <asp:ListItem Text="Management" Value="3"></asp:ListItem>
                     
                     </asp:DropDownList>
                     
                     </td></tr>
                                                    
                       
                      
                
                      
                                        

                                            <tr>
                                            <td>Groups:</td>
                                            <td  >
                                            
                                                  <asp:ListBox ID="ddlList" runat="server" SelectionMode="Multiple"   data-placeholder=""   class="chosen-select" style="width:421px;" tabindex="18">
    
      </asp:ListBox>
                                            </td>
                                            </tr>



                                                <tr>
                                             
                                            <td colspan="100%" style="padding-left:60px" >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td>
                                            <asp:Button ID="btnSubmit"  class="btn btn-primary btn-small" runat="server" 
                                                    Text="Submit" onclick="btnSubmit_Click" />
                                            
                                           </td>
                                             </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>



                     </table>
                    			 
       
                    </div>
			  </div>
               


             






             <div id="rightnow">
                    <h3 class="reallynow">
                        <span></span>
                     
                        <br />
                    </h3>
				   <div class="youhave" style="padding-left:30px">
               
               <table style="width:100%">
               <tr>
               <td colspan="100%">
               <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
               </td>
               </tr>
               <tr>
               <td>
               <table>
              <tr><td colspan="100%" style="background:gray;color:White">Add Department</td></tr>
<tr><td class="headings">Title:</td><td>  

<input type="text" runat="server"  name="txtDepartmentTitle" class="inputtxt validate required alphanumeric"   data-index="1" id="txtDepartmentTitle" style="width: 150px"/></td></tr>
<tr>
  <td>&nbsp;</td><td> 
  <asp:Button ID="btnAddDepartment" runat="server" 
        class="btn btn-primary btn-small" Text="Add Department" 
        onclick="btnAddDepartment_Click"/>
  </td>
                                         
   
</tr>
</table>


               </td>

               <td>
               
               
               <table>
                   <tr><td colspan="100%" style="background:gray;color:White">Add Group</td></tr>
<tr><td class="headings">Title:</td><td>  <input type="text" runat="server" name="txtGroupTitle" class="inputtxt validate required alphanumeric"   data-index="1" id="txtGroupTitle" style="width: 150px"/></td></tr>
<tr>
 
       <td>&nbsp;</td><td>
         <asp:Button ID="btnAddGroup"   class="btn btn-primary btn-small" 
             runat="server"  Text="Add Group" onclick="btnAddGroup_Click"/>
         </td>
   
</tr>
</table>


               </td>

               </tr>
               </table>
                    			 
       
                    </div>
			  </div>
               















            </div>
</form>

       <script type="text/javascript">
           var config = {
               '.chosen-select': {},
               '.chosen-select-deselect': { allow_single_deselect: true },
               '.chosen-select-no-single': { disable_search_threshold: 10 },
               '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
               '.chosen-select-width': { width: "95%" }
           }
           for (var selector in config) {
               $(selector).chosen(config[selector]);
           }
      
  </script>  

</asp:Content>
