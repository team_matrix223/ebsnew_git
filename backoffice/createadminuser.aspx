﻿<%@ Page Title="" Language="C#" MasterPageFile="admin.master" AutoEventWireup="true" CodeFile="createadminuser.aspx.cs" Inherits="createadminuser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">

     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/custom-css/master.css" rel="stylesheet" />

    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

    <script src="js/jquery.uilock.js" type="text/javascript"></script>
<script language="javascript">
    var m_AdminId = 0;

    function ResetControls() {
        m_AdminId = 0;
        var txtTitle = $("#txtName");
        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");

        $("#txtName").val("");
        $("#txtMobile").val("");
        $("#txtAdminName").val("").prop("disabled",false);
        $("#txtPassword").val("");


        $("input[name='Roles']").prop("checked", false);
        txtTitle.focus();
        txtTitle.val("");
       
        btnAdd.css({ "display": "block" });
        btnAdd.html("Add Admin User");

       
        btnUpdate.css({ "display": "none" });
        btnUpdate.html("Update Admin User");

        $("#chkIsActive").prop("checked", "checked");

        $("#btnReset").css({ "display": "none" });

       
        
    }



    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }

    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }

    function InsertUpdate() {

       




        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");
        var Id = m_AdminId;

        var Name=$("#txtName").val();
        var Mobile=$("#txtMobile").val();
        var AdminName=$("#txtAdminName").val() ;
        var Password=$("#txtPassword").val();



        var Roles = "";


        $("input[name='Roles']").each(
            function (x) {

                if ($(this).prop("checked")) {


                    Roles = Roles + $(this).val() + "|";
                }
            }

            );


            if (Roles== "") {
                alert("Please select atleast one Role");
                return;
            }

        var Title = $("#txtName").val();
        if ($.trim(Title) == "") {
            $("#txtName").focus();
             return;
        }
        btnAdd.unbind('click');
        btnUpdate.unbind('click');

        btnAdd.html("<img src='images/loader.gif' alt='loading...'/>")
        btnUpdate.html("<img src='images/loader.gif' alt='loading...'/>")

        var IsActive = false;


        if ($('#chkIsActive').is(":checked")) {
            IsActive = true;
        }

       // alert('{"Name":"' + Name + '", "Mobile": "' + Mobile + '", "AdminName": "' + AdminName + '", "Password": "' + Password + '", "Roles": "' + Roles + '","isActive": "' + IsActive + '"}');
        $.ajax({
            type: "POST",
            data: '{"AdminId":"' + Id + '","Name":"' + Name + '", "Mobile": "' + Mobile + '", "AdminName": "' + AdminName + '", "Password": "' + Password + '", "Roles": "' + Roles + '","isActive": "' + IsActive + '"}',
            url: "createadminuser.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == 0) {
                     ResetControls();
                    btnAdd.bind('click', function () { InsertUpdate(); });
                    btnUpdate.bind('click', function () { InsertUpdate(); });
                    alert("Insertion Failed.Admin with duplicate Admin Name already exists.");
                    return;

                }
            

                if (Id == "0") {
                    btnAdd.bind('click', function () { InsertUpdate(); });
                    btnUpdate.bind('click', function () { InsertUpdate(); });
                    jQuery("#jQGridDemo").jqGrid('addRowData', obj.Admin.AdminId, obj.Admin, "last");

                    ResetControls();

                    alert("Admin User added successfully.");

                }
                else {
                    btnAdd.bind('click', function () { InsertUpdate(); });
                    btnUpdate.bind('click', function () { InsertUpdate(); });
                    ResetControls();
                    var myGrid = $("#jQGridDemo");
                    var selRowId = myGrid.jqGrid('getGridParam', 'selrow');


                    myGrid.jqGrid('setRowData', selRowId, obj.Admin);

                    alert("Admin User Updated successfully.");


                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            }



        });

    }

    $(document).ready(
    function () {
        $('#txtTitle').focus();





        $("#btnReset").click(
        function () {

            ResetControls();

        }
        );

        $("#btnUpdate").click(
        function () {
            InsertUpdate();


        }
        );


        $("#btnAdd").click(
        function () {

            m_AdminId = 0;
            InsertUpdate();


        }
        );

        $("#jQGridDemo").jqGrid('setGridParam',
         {
             onSelectRow: function (rowid, iRow, iCol, e) {


                 var Roles = $('#jQGridDemo').jqGrid('getCell', rowid, 'Roles')
                 var arrRoles = [];


                 arrRoles = Roles.split('|');

                 $("input[name='Roles']").prop("checked", false);
                 for (var i = 0; i < arrRoles.length; i++) {
                    
                     $("#Chk" + arrRoles[i]).prop("checked", true);

                 }

                 var txtTitle = $("#txtName");
                 $("#txtAdminName").prop("disabled", true);
                 m_AdminId = $('#jQGridDemo').jqGrid('getCell', rowid, 'AdminId');
                 $("#txtAdminName").val($('#jQGridDemo').jqGrid('getCell', rowid, 'AdminName'));
                 $("#txtMobile").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Mobile'));
                 $("#txtPassword").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Password'));


                 if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                     $('#chkIsActive').prop('checked', true);
                 }
                 else {
                     $('#chkIsActive').prop('checked', false);

                 }
                 txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Name'));
                 txtTitle.focus();
                 $("#btnAdd").css({ "display": "none" });
                 $("#btnUpdate").css({ "display": "block" });
                 $("#btnReset").css({ "display": "block" });
                 TakeMeTop();
             }
         });

        var DataGrid = jQuery('#jQGridDemo');
        DataGrid.jqGrid('setGridWidth', '500');
    }
    );

</script>
<input type="hidden" id="hdnId" value="0"/>

<div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Admin User</span>
                       <a href="changepassword.aspx">Change Password</a> 
                        <br />
                    </h3>
				   <div class="youhave">
                <table class="top_table" cellpadding="0" cellspacing="0" border="0">
                     <tr><td class="headings">Full Name:</td><td>  <input type="text" name="txtName" class="inputtxt validate required alphanumeric"  data-index="1" id="txtName" /></td></tr>
                     <tr><td class="headings">Mobile:</td><td>  <input type="text" name="txtMobile" class="inputtxt validate required alphanumeric"  data-index="1" id="txtMobile" /></td></tr>
              
                     <tr><td class="headings">Admin Name:</td><td>  <input type="text" name="txtAdminName" class="inputtxt validate required alphanumeric"  data-index="1" id="txtAdminName" /></td></tr>
                     <tr><td class="headings">Password:</td><td>  <input type="password" name="txtPassword" class="inputtxt validate required alphanumeric"  data-index="1" id="txtPassword" /></td></tr>
                </table>
                     
                <table id="tbRoles" class="admin_user" style="border-collapse:collapse;" >
                     <tr>
                     <td >
                     <table class="admin_user_contant"> 
                       <tr><td><input type="checkbox" value="AdminUsers" id="ChkAdminUsers" name="Roles"/></td><td>AdminUsers</td></tr>       
                       <tr><td><input type="checkbox" value="CategoryLevel1" id="ChkCategoryLevel1" name="Roles"/></td><td>CategoryLevel1</td></tr>       
                       <tr><td><input type="checkbox" value="CategoryLevel2" id="ChkCategoryLevel2" name='Roles'/></td><td>CategoryLevel2</td></tr>      
                       <tr><td><input type="checkbox" value="CategoryLevel3" id="ChkCategoryLevel3" name='Roles'/></td><td>CategoryLevel3</td></tr> 
                       <tr><td><input type="checkbox" value="Products" id="ChkProducts" name="Roles"/></td><td>Products</td></tr>       
                       <tr><td><input type="checkbox" value="MigrateProducts" id="ChkMigrateProducts" name="Roles"/></td><td>MigrateProducts</td></tr>       
                       <tr><td><input type="checkbox" value="Schemes" id="ChkSchemes" name='Roles'/></td><td>Schemes</td></tr>      
                       <tr><td><input type="checkbox" value="ComboType" id="ChkComboType" name='Roles'/></td><td>ComboType</td></tr> 
                       <tr><td><input type="checkbox" value="ManageCombo" id="ChkManageCombo" name="Roles"/></td><td>ManageCombo</td></tr>       
                       <tr><td><input type="checkbox" value="ManageSliders" id="ChkManageSliders" name="Roles"/></td><td>ManageSliders</td></tr>       
                       <tr><td><input type="checkbox" value="RemoveImages" id="ChkRemoveImages" name='Roles'/></td><td>RemoveImages</td></tr>      
                       <tr><td><input type="checkbox" value="ManageBrands" id="ChkManageBrands" name='Roles'/></td><td>ManageBrands</td></tr> 
                       <tr><td><input type="checkbox" value="ManageCities" id="ChkManageCities" name="Roles"/></td><td>ManageCities</td></tr>       
                       <tr><td><input type="checkbox" value="ManagePinCodes" id="ChkManagePinCodes" name="Roles"/></td><td>ManagePinCodes</td></tr>       
                       <tr><td><input type="checkbox" value="ManageGroups" id="ChkManageGroups" name='Roles'/></td><td>ManageGroups</td></tr>      
                       <tr><td><input type="checkbox" value="ManageSubGroups" id="ChkManageSubGroups" name='Roles'/></td><td>ManageSubGroups</td></tr> 
                       <tr><td><input type="checkbox" value="InActiveProducts" id="ChkInActiveProducts" name="Roles"/></td><td>InActiveProducts</td></tr>      
                       <tr><td><input type="checkbox" value="UpdatePrices" id="ChkUpdatePrices" name="Roles"/></td><td>UpdatePrices</td></tr>       
                       <tr><td><input type="checkbox" value="Offers" id="ChkOffers" name='Roles'/></td><td>Offers</td></tr>      
                       <tr><td><input type="checkbox" value="Coupons" id="ChkCoupons" name='Roles'/></td><td>Coupons</td></tr> 
                       <tr><td><input type="checkbox" value="PriceDifferences" id="ChkPriceDifferences" name="Roles"/></td><td>PriceDifferences</td></tr>       
                       
                     </table>
                     </td>


                      <td>
                      <table class="admin_user_contant" style="float: right;">
                       <tr><td><input type="checkbox"   value="Statistics" id="ChkStatistics" name="Roles"/></td><td>Statistics</td></tr>       
                       <tr><td><input type="checkbox"    value="Enquiries" id="ChkEnquiries" name='Roles'/></td><td>Enquiries</td></tr>      
                       <tr><td><input type="checkbox"    value="ManageEmployees" id="ChkManageEmployees" name='Roles'/></td><td>ManageEmployees</td></tr> 
                       <tr><td><input type="checkbox"   value="PendingOrders" id="ChkPendingOrders" name="Roles"/></td><td>PendingOrders</td></tr>       
                       <tr><td><input type="checkbox"   value="InProcessOrders" id="ChkInProcessOrders" name="Roles"/></td><td>InProcessOrders</td></tr>       
                       <tr><td><input type="checkbox"    value="AllocateToEmployees" id="ChkAllocateToEmployees" name='Roles'/></td><td>AllocateToEmployees</td></tr>      
                       <tr><td><input type="checkbox"    value="ReceivePayments" id="ChkReceivePayments" name='Roles'/></td><td>ReceivePayments</td></tr> 
                       <tr><td><input type="checkbox"   value="DeliveredOrders" id="ChkDeliveredOrders" name="Roles"/></td><td>DeliveredOrders</td></tr>       
                       <tr><td><input type="checkbox"   value="DeAllocateOrders" id="ChkDeAllocateOrders" name="Roles"/></td><td>DeAllocateOrders</td></tr>       
                       <tr><td><input type="checkbox"    value="EditBill" id="ChkEditBill" name='Roles'/></td><td>EditBill</td></tr>      
                       <tr><td><input type="checkbox"    value="CanceledOrders" id="ChkCanceledOrders" name='Roles'/></td><td>CanceledOrders</td></tr> 
                       <tr><td><input type="checkbox"   value="CanceledBills" id="ChkCanceledBills" name="Roles"/></td><td>CanceledBills</td></tr>      
                       
                       
                       <tr><td><input type="checkbox"   value="CheckOrderStatus" id="ChkCheckOrderStatus" name="Roles"/></td><td>CheckOrderStatus</td></tr>       
                       <tr><td><input type="checkbox"    value="RechargeWallet" id="ChkRechargeWallet" name='Roles'/></td><td>RechargeWallet</td></tr>      
                       <tr><td><input type="checkbox"    value="AddParentPages" id="ChkAddParentPages" name='Roles'/></td><td>AddParentPages</td></tr> 
                       <tr><td><input type="checkbox"   value="EditParentPages" id="ChkEditParentPages" name="Roles"/></td><td>EditParentPages</td></tr>       
                       <tr><td><input type="checkbox"   value="AddSubPages" id="ChkAddSubPages" name="Roles"/></td><td>AddSubPages</td></tr>       
                       <tr><td><input type="checkbox"    value="EditSubPages" id="ChkEditSubPages" name='Roles'/></td><td>EditSubPages</td></tr>      
                       <tr><td><input type="checkbox"    value="ManageDeliverySlots" id="ChkManageDeliverySlots" name='Roles'/></td><td>ManageDeliverySlots</td></tr> 
                       <tr><td><input type="checkbox"   value="MessageCredentials" id="ChkMessageCredentials" name="Roles"/></td><td>MessageCredentials</td></tr>       
                       <tr><td><input type="checkbox"   value="Settings" id="ChkSettings" name="Roles"/></td><td>Settings</td></tr>       
    
                      </table>
                      </td>   
                     </tr>
                        
                     </table>

               
               <table class="top_table">         
                   <tr><td class="headings">Is Active:</td><td>     <input type="checkbox" id="chkIsActive" checked=checked data-index="2"  name="chkIsActive" /></td></tr>
               </table>

               <table class="category_table">
                     <tr>
                     <td></td><td> <div id="btnAdd"  class="btn btn-primary btn-small" >Add Admin User</div></td>
                     <td><div id="btnUpdate"  class="btn btn-primary btn-small" style="display:none;" >Update Admin User</div></td>
                     <td><div id="btnReset"  class="btn btn-primary btn-small" style="display:none;width:60px;" >Cancel</div></td>
                     </tr>                       
               </table>
                    			 
       
                    </div>
			  </div>             
    
            <div id="rightnow">
                <h3 class="reallynow">
                                    <span>Admin Users</span>

                                    <br />
                                </h3>
                <div class="youhave">

                    <table id="jQGridDemo">
                    </table>
                    <div id="jQGridDemoPager">
                    </div>

                </div>
            </div>
               
            </div>

            	 
    <script type="text/javascript">
     
    
        jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/manageadmin.ashx',
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['AdminId','AdminName', 'Password', 'Name', 'Mobile','Roles', 'IsActive'],
            colModel: [
              { name: 'AdminId',key:true, index: 'AdminId', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
                   
                         { name: 'AdminName', index: 'AdminName', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		                 { name: 'Password', index: 'Password', width: 150, stype: 'text', sortable: true,hidden:true, editable: true ,editrules: { required: true }},
   		                 { name: 'Name', index: 'Name', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		                
                         { name: 'Mobile', index: 'Mobile', width: 150, stype: 'text', sortable: true, editable: true ,editrules: { required: true }},
   		                 { name: 'Roles', index: 'Roles', width: 150, stype: 'text', sortable: true, editable: true,hidden:true ,editrules: { required: true }},
   		               
                        { name: 'IsActive', index: 'IsActive', width: 150, editable: true,edittype:"checkbox",editoptions: { value:"true:false" } , formatter: "checkbox", formatoptions: { disabled: true} },
 
   		                  
                       ],
            rowNum: 10,
          
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'AdminId',
            viewrecords: true,
            height: "100%",
            width:"400px",
            sortorder: 'desc',
            caption: "Admin List",
         
           
                    
             
        });


 
        $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: false,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }

                     );

                   
              
    </script>  

          
</asp:Content>

