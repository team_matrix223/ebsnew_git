﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="managewalletconsumption.aspx.cs" Inherits="backoffice_managewalletconsumption" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
        <link href="css/custom-css/master.css" rel="stylesheet" />
    
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <script language="javascript" type="text/javascript">
        var m_WalletConsumptionId = 0;

       

        function TakeMeTop() {
            $("html, body").animate({ scrollTop: 0 }, 500);
        }

        function RefreshGrid() {
            $('#jQGridDemo').trigger('reloadGrid');

        }

        function Update() {

            var Id = m_WalletConsumptionId;
            if (Id == 0) {
                alert("First select record  from List");
                return;
            }
            $.ajax({
                type: "POST",
                data: '{"WalletConsumptionId":"' + Id + '"}',
                url: "managewalletconsumption.aspx/Update",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                        
                        BindGrid();
                        alert("Information  is Updated successfully.");
                        $("#btnUpdate").css({ "display": "none" });

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });

        }


        $(document).ready(
        function () {

            BindGrid();

         
         


            $("#btnUpdate").click(
        function () {
            Update();
        }
        );
        });
    </script>
        <form   runat="server" id="formID" method="post">
   
<div id="content">
       		
                       <div id="rightnow">
                    <h3 class="reallynow">
                        <span>Manage Reimbursement Gifts</span>
                      
                        <br />
                    </h3>
				    <div class="youhave">
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
      <div id="btnUpdate"  class="btn btn-primary btn-small" style="display:none;width:150px;margin-top:15px" >Update Dispatch Status</div>
                    </div>
			  </div>
                   </div>
                   </form>
                        <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/WalletGiftsHandler.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",
                        colNames: ['WalletConsumptionId','UserId','UserName','Image','GiftName', 'Value','OrderId','BillId','Date','IsDispatched'],
                        colModel: [
                                    { name: 'WalletConsumptionId', key: true, index: 'WalletConsumptionId', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                     { name: 'UserId', index: 'UserId', width: '150px', stype: 'text', sortable: true, editable: true, editrules: { required: true } ,hidden: true },
                                     { name: 'UserName', index: 'UserName', width: '150px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                       { name: 'PhotoUrl', index: 'PhotoUrl', width: '60px', align: "center", editable: true, formatter: imageFormat, unformat: imageUnFormat },
                                     { name: 'GiftName', index: 'GiftName', width: '150px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    
                                   { name: 'Value', index: 'Value', width: '150px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'OrderId', index: 'OrderId', width: '70px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                      { name: 'BillId', index: 'BillId', width: '70px', stype: 'text', sortable: true, editable: true, editrules: { required: true },hidden:false },
                                       { name: 'strDOC', index: 'strDOC', width: '70px', stype: 'text', sortable: true, editable: true, editrules: { required: true },hidden:false },
                                     { name: 'IsDispatched', index: 'IsDispatched', width: '70px', editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },


                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'WalletConsumptionId',
                        viewrecords: true,
                        height: "100%",
                        width: "800px",
                        sortorder: 'asc',
                        caption: "Gidts List",

                        editurl: 'handlers/WalletGiftsHandler.ashx',



                    });

                     function imageFormat(cellvalue, options, rowObject) {
            return '<img src="../ReimbursementGiftsImages/T_' + cellvalue + '" style="width:20px;height:20px"/>';
        }
        function imageUnFormat(cellvalue, options, cell) {
            return $('img', cell).attr('src');
        }


                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_WalletConsumptionId = 0;
                  
                    m_WalletConsumptionId = $('#jQGridDemo').jqGrid('getCell', rowid, 'WalletConsumptionId');

                    if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsDispatched') == "true") {
                        $('#chkIsActive').prop('checked', true);
                         $("#btnUpdate").css({ "display": "none" });
                    }
                    else {
                        $('#chkIsActive').prop('checked', false);
                         $("#btnUpdate").css({ "display": "block" });
                    }
             
                     
                   
                   
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>
</asp:Content>


