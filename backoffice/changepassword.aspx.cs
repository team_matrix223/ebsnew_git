﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class changepassword : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }



   

    protected void btnChangePassword_Click(object sender, EventArgs e)
    {
         Admin objAdmin=new Admin();
         objAdmin.AdminName = Session[Constants.AdminId].ToString();
        objAdmin.Password=txtOldPassword.Value;
        Int32  status = new AdminBLL().CheckOnChangePassword(objAdmin);

        if (status.ToString() == "-2")
        {

            Response.Write("<script>alert('Invalid Old Password!');</script>");
        }
        else
        {
            objAdmin.Password = txtNewPassword.Value;
           string val= new AdminBLL().ChangePassword(objAdmin);
           if (val == "1")
           {
               Response.Write("<script>alert('Password Changed Successfully.');</script>");
           }
           else
           {
               Response.Write("<script>alert('Operation Failed. Please try again Later.');</script>");

           }

        }
    }
}