﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="MessageUsers.aspx.cs" Inherits="backoffice_MessageUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">

 <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
      <style type="text/css">
        .headings
        {
           
            font-weight:200px;
            }
    </style>


    <script type ="text/javascript">
        $(document).ready(
        function () {
            $("#btnSend").click(
            function () {
                if ($("#txtMessage").val() == "") {
                    $("#txtMessage").focus();
                    alert("Please Enter Message");
                    return;
                }
                else {
                    alert("Message Send Successfully");
                    $("#txtMessage").val("");
                }
            }
            );
        }
        );
    </script>
    
    <form id= "frm" runat="server">
    <asp:HiddenField  ID ="hdnusers" runat="server"/>
    <div id="content" style="padding:6px">

             <div id="rightnow" style="margin-top: 10px">
            <h3 class="reallynow">
                <span>Users List</span>
                <br />
            </h3>
            <div class="youhave" style="padding-left: 30px">
          
          <table>
          <tr class="headings"><td>Message Box:</td><td><textarea placeholder= "Write Message Here" style="height:100px;width:500px" id="txtMessage"></textarea></td></tr>
          </table>



                <table id="jQGridDemo">
                </table>
                <div id="jQGridDemoPager">
                </div>

                <table><tr><td><div id="btnSend"  class="btn btn-primary btn-small"  >Send</div></td></tr></table>



               
            </div>
        </div>

            </div>
            </form>

                    <script type="text/javascript">

    
    var users = $("#<%=hdnusers.ClientID %>").val();
   

        jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/notification.ashx?Type='+users,
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['Code','Title','MobilNo'],
            colModel: [
                        { name: 'Code',key:true, width: 50,index: 'Code', stype: 'text',sorttype:'int',hidden:false },
                        { name: 'Title',key:true, width: 450,index: 'Title', stype: 'text',sorttype:'int',hidden:false },
                         { name: 'Mobile',key:true, width: 150,index: 'Mobile', stype: 'text',sorttype:'int',hidden:false },
   		                
   		                
   		                
                       ],
            rowNum: 10,
            mtype: 'GET',

            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'StateId',
            viewrecords: true,
            multiselect:true,
            height: "100%",
            width:"400px",
            toolbar: [true, "top"],
            ignoreCase: true,
            sortorder: 'asc',
            caption: "Users List",
            editurl: 'handlers/notification.ashx?Type='+users,
             
        });

         var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });
 
        $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: false,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }



                     );
             
    </script>  

</asp:Content>

