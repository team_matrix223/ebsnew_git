﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="manageproductfinal.aspx.cs" Inherits="backoffice_manageproductfinal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
      <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
  <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
  <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
   <script src="js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

    <form id="frmVid" runat="server">
<asp:HiddenField ID= "hdnProductId" runat="server" Value ="0" />
 <div id="content" style="padding:6px">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Products</span>
                     
                        <br />
                    </h3>
				   <div class="youhave" style="padding-left:0px">
 
 
  

    <table align="center" border="0"  style="border: 0px solid black;
                margin: 1px" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td>
                             
                        <table style="width: 100%">
                             <tr>
                                 <td>
                                    </td>
                                 <td>
                                    </td>
                                 <td>
                                    </td>
                             </tr>
                             <tr>
                             <td colspan="100%">
                              <table cellspacing="2" cellpadding="0" border="0" style="margin-left:12px">
                            
                             <tr>
                                 <td style="text-align:right" class="Titles">
                                     Category:</td>
                                 <td>
                                 <select id="ddlCategory"  data-index="6" style="width:220px;height:30px"></select>
                                    
                                     </td>
                                 <td style="color:Red">
                                
                                   </td>
                             </tr>
                              
                             <tr>
                                 <td style="text-align:right" class="Titles">
                                     Sub Category:</td>
                                 <td>
                                     <select id="ddlSubCategories"  data-index="6" style="width:220px;height:30px"></select>
                                         </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>


                              <tr>
                                 <td style="text-align:right" class="Titles">
                                     Level 3 Category:</td>
                                 <td>
                                     <select id="ddlCategoryLevel3"  data-index="6" style="width:220px;height:30px"></select>

                                                   </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>


                              <tr>
                                 <td style="text-align:right" class="Titles">
                                    Name:</td>
                                 <td>
                                     <input type="text"  name="txtTitle" class="inputtxt validate required alphanumeric"   data-index="1" id="txtTitle" style="width: 213px"/>                               
                                   
                                 </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>
                                <tr>
                                 <td style="text-align:right" class="Titles">
                                    Short Name:</td>
                                 <td>
                                        <input type="text"  name="txtShortName" class="inputtxt validate required alphanumeric"   data-index="1" id="txtShortName" style="width: 213px"/>                               
                                     
         
                                 </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>


                              <tr>
                                 <td style="text-align:right" class="Titles">
                                    Market Price:</td>
                                 <td>
                                      <input type="text"  name="txtMarketPrice" class="inputtxt validate required alphanumeric"   data-index="1" id="txtMarketPrice" style="width: 213px"/>                               
                                      </td>
                         
                             </tr>

                              <tr>
                                 <td style="text-align:right" class="Titles" >
                                 Our Price:
                                 </td>
                                 <td colspan="100%">                                    <table>
                                    <tr>
                                     <td><select ID="ddlUnits" runat="server">
                                     <option value="Units" >Units</option>
                                     
                                     <option  value="Dozon">Dozon</option>
                                     <option  value="Kg">Kg</option>
                                     <option  value="Gram">Gram</option>
                                     <option  value="Piece">Piece</option>
                                    
                                    
                                     </select>

                                          </td>
                                     <td>
                                          <input type="text" placeholder="Qty" name="txtQty"  class="inputtxt validate required alphanumeric"   data-index="1" id="txtQty" style="width: 50px"/>                               
                                                          

                                     </td>
                                     <td>
                                            <input type="text" placeholder="Price" name="txtPrice" class="inputtxt validate required alphanumeric"   data-index="1" id="txtPrice" style="width: 50px"/>                               
 
                                     </td>
                                    <td>
                                           <input type="text"  name="txtQty1" placeholder="Qty1" class="inputtxt validate required alphanumeric"   data-index="1" id="txtQty1" style="width: 50px"/>                               

                                                                                                   

                                     </td>
                                     <td>
                                            <input type="text"  name="txtPrice1" placeholder="Price1" class="inputtxt validate required alphanumeric"   data-index="1" id="txtPrice1" style="width: 50px"/>                               
                                     </td>
                                    </tr>
                                    </table>
                                    
                                    </td>
                                    <td></td>
                                 
                                 
                             </tr>

                              <tr>
                                 <td style="text-align:right" class="Titles">
                                    Description:</td>
                                 <td>
                                      <textarea ID="txtDesc" rows ="4" runat="server"  ></textarea>
                                                                      </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>
                           
                                    <tr>
                                 <td style="text-align:right" class="Titles">
                                  Short Desc:</td>
                                 <td>
                                       <textarea ID="txtShortDesc" rows ="4" runat="server"  ></textarea>
                                                                        
                                 </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>
                           

                           <tr  >
                                 <td style="text-align:right" class="Titles">
                                     IsActive:</td>
                                 <td>
                                       <input type="checkbox" id="chkIsActive" runat="server" checked="checked" data-index="2"  name="chkIsActive" />
                                 </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>

                            </table>
                             </td>
                             </tr>



                            


                            
                         </table>

                         
               <table cellspacing="2" cellpadding="0" border="0" style="margin-left:96px">
                <tr>
                                 <td style="text-align:right" class="Titles">
                                     Url:</td>
                                 <td>
                                      <asp:FileUpload ID="fuImage" runat="server"  Width ="185px"   />
                                      <asp:Label ID = "lblimg"  runat="server" Width="175px"></asp:Label><asp:HiddenField ID="hdnlblimg" runat="server" />
                                 </td>
                                 <td>
                                     &nbsp;</td>
                             </tr>

                             
                               
                             
                             <tr>
                                 <td>
                                     &nbsp;</td>
                                 <td>
                                     <table>
                                         <tr>
                                             <td>  <div id="btnAdd" class="btn btn-primary btn-small">Add</div></td>
                                              <td>  <div id="btnUpdate" class="btn btn-primary btn-small">Update</div></td>
                                              <td>  <div id="btnCancel" class="btn btn-primary btn-small">Cancel</div></td>
                                         </tr>
                                     </table>
                                     
                                 </td>
                                  
                                 <td>
                                     &nbsp;</td>
                             </tr>
                          
                             <tr>
                                 <td>
                                     &nbsp;</td>
                                 <td>
                                     &nbsp;</td>
                                 <td>
                                     &nbsp;</td>
                             </tr>
               </table>

                        
                        
                 <%--   </ContentTemplate>
                    </asp:UpdatePanel>--%>
                   
                    </td>
                </tr>
            </table>


     </div>
     </div>

     <br />
     <div id="Div1">
                    <h3 class="reallynow">
                        <span>Add/Update Products</span>
                     
                        <br />
                    </h3>
				   <div class="youhave" style="padding-left:0px;float:left">


               
</div>
                   
 
     </div>

       

     </div>
</form>
</asp:Content>

