﻿<%@ Page Title="" Language="C#" MasterPageFile="admin.master" AutoEventWireup="true"
    CodeFile="managepreferences.aspx.cs" Inherits="backoffice_managepreferences" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" runat="Server">
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="js/jquery-ui.css">
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <style>
        #sortable
        {
            list-style-type: none;
            margin: 10px 0 10px 150px;
            padding: 0;
            width: 50%;
        }
        #sortable li
        {
            margin: 0 3px 3px 3px;
            padding-left: 5px;
            font-size: 15px;
            height: 25px;
        }
    </style>
    <script>

        $(document).ready(
    function () {


        $.ajax({
            type: "POST",
            data: '{}',
            url: "managepreferences.aspx/BindEmployees",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                $("#sortable").html(obj.Employees);


            }


        });


    }
    );

        $(function () {
            $("#sortable").sortable({ update: function (event, ui) {


                $.uiLock('');

                var masterArr = [];
                $(".ui-state-default").each(
            function (x) {

                masterArr[x] = $(this).attr("id");

            }
            );


                $.ajax({
                    type: "POST",
                    data: '{"masterArr":"' + masterArr + '","Type":"'+"1"+'"}',
                    url: "managepreferences.aspx/InsertEmployeeOrder",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);
                        if (obj.Status == "0") {

                            alert("Change Order Operation failed. Please try again later");
                            return;
                        }
                        alert("Orders Changed Successfully");


                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {
                        $.uiUnlock();
                    }


                });

            }
            });
            $("#sortable").disableSelection();
        });





    </script>
    <div id="content-header">
        <h1>
            Manage Preferences</h1>
        <div class="btn-group">
            <a class="btn btn-large" title="Manage Files"><i class="glyphicon glyphicon-file"></i>
            </a><a class="btn btn-large" title="Manage Users"><i class="glyphicon glyphicon-user">
            </i></a><a class="btn btn-large" title="Manage Comments"><i class="glyphicon glyphicon-comment">
            </i><span class="label label-danger">5</span></a> <a class="btn btn-large" title="Manage Orders">
                <i class="glyphicon glyphicon-shopping-cart"></i></a>
        </div>
    </div>
    <div id="breadcrumb">
        <a href="dashboard.aspx" title="Go to Home" class="tip-bottom"><i class="glyphicon glyphicon-home">
        </i>Home</a> <a href="#" class="current">Manage Preferences</a>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="glyphicon glyphicon-align-justify"></i></span>
                        <h5>
                            Manage Preferences</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <div class="form-horizontal">
                            <div style="height: 400px; overflow-y: scroll" title="Select and Drag to Change Order">
                                <ul id="sortable" style="cursor: move">
                                </ul>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
