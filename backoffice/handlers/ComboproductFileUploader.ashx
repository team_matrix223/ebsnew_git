﻿<%@ WebHandler Language="C#" Class="ComboproductFileUploader" %>

using System;
using System.Web;

public class ComboproductFileUploader : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.Files.Count > 0)
        {
            HttpFileCollection files = context.Request.Files;
            HttpPostedFile file = files[0];
            string FileName = CommonFunctions.UploadImage(file, "~/ProductImages/", true, 322, 180, false, 0, 0);

            context.Response.Write(FileName);
            //string fname = context.Server.MapPath("~/ComboImages/" + file.FileName);


        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}