﻿<%@ WebHandler Language="C#" Class="GroupFileUploader" %>

using System;
using System.Web;

public class GroupFileUploader : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.Files.Count > 0)
        {
            HttpFileCollection files = context.Request.Files;
            HttpPostedFile file = files[0];
            HttpPostedFile file2 = files[1];
            string FileName = CommonFunctions.UploadImage(file, "~/GroupImages/", true, 322, 180, false, 0, 0);
            string FileName1 = CommonFunctions.UploadImage(file2, "~/GroupImages/", true, 322, 180, false, 0, 0);
             
            //string fname = context.Server.MapPath("~/ComboImages/" + file.FileName);

            System.Web.Script.Serialization.JavaScriptSerializer ser = new System.Web.Script.Serialization.JavaScriptSerializer();
 
            var JsonData = new
            {

                File1 = FileName,
                File2 = FileName1

            };
            context.Response.Write(ser.Serialize(JsonData));
            

        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}