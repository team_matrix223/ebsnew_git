﻿<%@ WebHandler Language="C#" Class="ReimbursementGiftsHandler" %>

using System;
using System.Web;

public class ReimbursementGiftsHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;


      


            //oper = null which means its first load.
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var xss = jsonSerializer.Serialize(
             new ReimbursementGiftsBLL().GetAll()
               );

            context.Response.Write(xss);


    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}