﻿<%@ WebHandler Language="C#" Class="ReimbursementGiftsFileUploader" %>

using System;
using System.Web;

public class ReimbursementGiftsFileUploader : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        string FileName = "";
      
        if (context.Request.Files.Count > 0)
        {

            HttpFileCollection files = context.Request.Files;
            HttpPostedFile file = files[0];
            FileName = CommonFunctions.UploadImage(file, "~/ReimbursementGiftsImages/", true, 322, 180, false, 0, 0);

        }
        System.Web.Script.Serialization.JavaScriptSerializer ser = new System.Web.Script.Serialization.JavaScriptSerializer();

        var JsonData = new
        {

            File1 = FileName
            
        };
        context.Response.Write(ser.Serialize(JsonData));
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}