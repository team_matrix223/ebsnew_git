﻿<%@ WebHandler Language="C#" Class="ProductsByCategory" %>

using System;
using System.Web;

public class ProductsByCategory : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {

        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;


        string Category = context.Request.QueryString["Category"];
        if (Category != null)
        {


            //oper = null which means its first load.
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var xss = jsonSerializer.Serialize(
             new ProductsBLL().GetByCategoryId(Convert.ToInt32(Category))
               );

            context.Response.Write(xss);
        }
        
        





    }




    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}