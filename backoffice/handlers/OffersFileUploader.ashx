﻿<%@ WebHandler Language="C#" Class="OffersFileUploader" %>

using System;
using System.Web;

public class OffersFileUploader : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        string FileName = "";
        string FileName1 = "";
        string FileName2 = "";
        string FileName3 = "";
        string FileName4 = "";
        if (context.Request.Files.Count > 0)
        {   
            
            HttpFileCollection files = context.Request.Files;
            HttpPostedFile file = files[0];
            FileName = CommonFunctions.UploadImage(file, "~/OfferImages/", true, 322, 180, false, 0, 0);
            if (files.Count > 1)
            {
                HttpPostedFile file1 = files[1];
                FileName1 = CommonFunctions.UploadImage(file1, "~/OfferImages/", true, 322, 180, false, 0, 0);
            }
            if (files.Count >2)
            {
                HttpPostedFile file2 = files[2];
                FileName2 = CommonFunctions.UploadImage(file2, "~/OfferImages/", true, 322, 180, false, 0, 0);
            }
            if (files.Count > 3)
            {
                HttpPostedFile file3 = files[3];
                FileName3 = CommonFunctions.UploadImage(file3, "~/OfferImages/", true, 322, 180, false, 0, 0);
            }
            if (files.Count > 4)
            {
                HttpPostedFile file4 = files[4];
                FileName4 = CommonFunctions.UploadImage(file4, "~/OfferImages/", true, 322, 180, false, 0, 0);
            }
          
            //string fname = context.Server.MapPath("~/ComboImages/" + file.FileName);
        }
            System.Web.Script.Serialization.JavaScriptSerializer ser = new System.Web.Script.Serialization.JavaScriptSerializer();

            var JsonData = new
            {

                File1 = FileName,
                File2 = FileName1,
                File3= FileName2,
                File4 = FileName3,
                File5 = FileName4
            };
            context.Response.Write(ser.Serialize(JsonData));


        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}