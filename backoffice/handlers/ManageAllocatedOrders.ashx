﻿<%@ WebHandler Language="C#" Class="ManageAllocatedOrders" %>

using System;
using System.Web;

public class ManageAllocatedOrders : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;

       
        string EmpId = context.Request.QueryString["EmpId"];
        


        //oper = null which means its first load.
        var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        var xss = jsonSerializer.Serialize(
         new EmployeesBLL().GetByDateallocatedOrders(Convert.ToInt32(EmpId))
           );

        context.Response.Write(xss);

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}