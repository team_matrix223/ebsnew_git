﻿<%@ WebHandler Language="C#" Class="GetGroupLinks" %>

using System;
using System.Web;

public class GetGroupLinks : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;

        if (strOperation == null)
        {
            string GroupId = context.Request.QueryString["GroupId"];

            //oper = null which means its first load.
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var xss = jsonSerializer.Serialize(
            new GroupLinksBLL().GetByGroupId(Convert.ToInt32(GroupId))
                //  new SubCategoryBLL().GetAll()
               );

            context.Response.Write(xss);
        }


    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}