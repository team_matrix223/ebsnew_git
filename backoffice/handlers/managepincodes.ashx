﻿<%@ WebHandler Language="C#" Class="managepincodes" %>

using System;
using System.Web;

public class managepincodes : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;


        //oper = null which means its first load.
        var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        var xss = jsonSerializer.Serialize(
         new PinCodeDAL().GetAll ()
           );

        context.Response.Write(xss);



    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}