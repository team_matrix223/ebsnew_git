﻿<%@ WebHandler Language="C#" Class="WalletGiftsHandler" %>

using System;
using System.Web;

public class WalletGiftsHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;

        if (strOperation == null)
        {
            //oper = null which means its first load.
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var x = jsonSerializer.Serialize(

                new WalletBLL ().GetAllWalletGifts());

            context.Response.Write(x);
        }
        else if (strOperation == "del")
        {


        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}