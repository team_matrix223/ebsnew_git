﻿<%@ WebHandler Language="C#" Class="managesubcategories" %>

using System;
using System.Web;

public class managesubcategories : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;
        Int32 CatLevel2 =Convert.ToInt32( context.Request.QueryString["CatLevel2"].ToString());
        if (strOperation == null)
        {

            //oper = null which means its first load.
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            jsonSerializer.MaxJsonLength = int.MaxValue;
            var xss = jsonSerializer.Serialize(
            new ProductsBLL().GetVariationList(CatLevel2));

            context.Response.Write(xss);

        }
        else if (strOperation == "edit")
        {
            
            
            string str = forms.Get("VariationId");
            string Brand = forms.Get("BrandName");
            string Brand1 = forms.Get("Brand");
            Variations objFinal = new Variations()
            {
                
                Name = forms.Get("Name"),
                VariationId = Convert.ToInt16(forms.Get("VariationId")),
                ItemCode = Convert.ToString(forms.Get("ItemCode")),
                Brand = Convert.ToString(forms.Get("BrandName")),
                Unit = Convert.ToString(forms.Get("Unit")),
                Qty = Convert.ToInt16(forms.Get("Qty")),
                Price = Convert.ToDecimal(forms.Get("Price")),
                Mrp = Convert.ToDecimal(forms.Get("Mrp")),
                type = Convert.ToString(forms.Get("type")),
                IsActive = Convert.ToBoolean(forms.Get("IsActive")),
            };
            int status = new ProductsBLL().UpdateProductList(objFinal);
           //int status = 0;
            string strOut = "1";

            if (status == 0)
            {
                strOut = "0";
            }
            context.Response.Write(strOut);
        
        
        
        }

        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}