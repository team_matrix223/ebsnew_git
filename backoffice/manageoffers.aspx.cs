﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using System.Configuration;
using System.Net.Mail;


public partial class backoffice_manageoffers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnDate.Value = DateTime.Now.ToShortDateString();
        }
    }
    [WebMethod]
    public static string Insert(int OfferId, string OfferType, string Title, string Description, string CouponNo, decimal OrderAmount, string DiscountType, int Points, string DiscountMode, decimal Discount, DateTime DateFrom, DateTime DateTo, string PaymentOption, bool IsActive, int OrdersPerCustomer, int MaxLimit, string GiftName1, string GiftDesc1, string GiftPhotoUrl1, string GiftName2, string GiftDesc2, string GiftPhotoUrl2, string GiftName3, string GiftDesc3, string GiftPhotoUrl3, string GiftName4, string GiftDesc4, string GiftPhotoUrl4, string GiftName5, string GiftDesc5, string GiftPhotoUrl5,int Chknews,string Newsletter,int SendTo)
    {
        DataTable dtGift = new DataTable();
        dtGift.Clear();
        dtGift.Columns.Clear();
        dtGift.Columns.Add("GiftId", typeof(int));
        dtGift.Columns.Add("GiftName", typeof(string));
        dtGift.Columns.Add("GiftDesc", typeof(string));
        dtGift.Columns.Add("GiftPhotoUrl", typeof(string));
        
        Offers objOffers = new Offers()
        {
            OfferId = OfferId,
            OfferType =OfferType,
            Title = Title.Trim().ToUpper(),
            Description=Description,
            CouponNo =CouponNo.ToUpper(),
            OrderAmount=OrderAmount, 
            DiscountType=DiscountType,
            Points =Points,
            DiscountMode=DiscountMode,
            Discount =Discount,
            FromDate =DateFrom,
            ToDate  =DateTo,
            PaymentOption =PaymentOption,
            IsActive = IsActive,
            AdminId = 0,
            OrdersPerCustomer = OrdersPerCustomer,
            MaxLimit = MaxLimit
        };
        if (GiftName1.Trim() != "")
        {
            dtGift.Rows.Add();
            dtGift.Rows[dtGift.Rows.Count-1]["GiftId"] = 0;
            dtGift.Rows[dtGift.Rows.Count-1]["GiftName"] =GiftName1;
            dtGift.Rows[dtGift.Rows.Count-1]["GiftDesc"] = GiftDesc1 ;
            dtGift.Rows[dtGift.Rows.Count-1]["GiftPhotoUrl"] = GiftPhotoUrl1 ;

        }
        if (GiftName2.Trim() != "")
        {
            dtGift.Rows.Add();
            dtGift.Rows[dtGift.Rows.Count-1]["GiftId"] = 0;
            dtGift.Rows[dtGift.Rows.Count-1]["GiftName"] = GiftName2;
            dtGift.Rows[dtGift.Rows.Count-1]["GiftDesc"] = GiftDesc2;
            dtGift.Rows[dtGift.Rows.Count-1]["GiftPhotoUrl"] = GiftPhotoUrl2;
        }
        if (GiftName3.Trim() != "")
        {
            dtGift.Rows.Add();
            dtGift.Rows[dtGift.Rows.Count-1]["GiftId"] = 0;
            dtGift.Rows[dtGift.Rows.Count-1]["GiftName"] = GiftName3;
            dtGift.Rows[dtGift.Rows.Count-1]["GiftDesc"] = GiftDesc3;
            dtGift.Rows[dtGift.Rows.Count-1]["GiftPhotoUrl"] = GiftPhotoUrl3;
        }
        if (GiftName4.Trim() != "")
        {
            dtGift.Rows.Add();
            dtGift.Rows[dtGift.Rows.Count-1]["GiftId"] = 0;
            dtGift.Rows[dtGift.Rows.Count-1]["GiftName"] = GiftName4;
            dtGift.Rows[dtGift.Rows.Count-1]["GiftDesc"] = GiftDesc4;
            dtGift.Rows[dtGift.Rows.Count-1]["GiftPhotoUrl"] = GiftPhotoUrl4;
        }
        if (GiftName5.Trim() != "")
        {
            dtGift.Rows.Add();
            dtGift.Rows[dtGift.Rows.Count-1]["GiftId"] = 0;
            dtGift.Rows[dtGift.Rows.Count-1]["GiftName"] = GiftName5;
            dtGift.Rows[dtGift.Rows.Count-1]["GiftDesc"] = GiftDesc5;
            dtGift.Rows[dtGift.Rows.Count-1]["GiftPhotoUrl"] = GiftPhotoUrl5;
        }
        if (Chknews == 1)
        {
            List<Users> lstUsers = new List<Users>();
            lstUsers = new OffersBLL().GetUsersInfoForNewsletter(SendTo);

            /////////////////SENDING MESSAGES///////////////////////////
            string MobileNo = "";
            foreach(var item in lstUsers)
            {
               
                   
                    MobileNo = item.MobileNo;
                    string MsgText = Newsletter;
                    WebClient Client = new WebClient();
                    string baseurl = "http://india.timessms.com/http-api/receiverall.aspx?username=harjit&password=9617583&sender=Demo&cdmasender=&to=" + MobileNo.ToString() + "&message=  " + MsgText.ToString() + "";
                    Stream data = Client.OpenRead(baseurl);
                    StreamReader reader = new StreamReader(data);
                    string s1 = reader.ReadToEnd();
                    data.Close();
                    reader.Close();
               
            }



            /////////SENDING MAIL//////////////////


            //Reading sender Email credential from web.config file

            

       //     string ToEmail="";
       //     string FromEmailid ="";
       //     string cc ="";
       //     string bcc ="";
       //     string HostAdd = "";



       // HostAdd = ConfigurationManager.AppSettings["Host"].ToString();
       // FromEmailid = ConfigurationManager.AppSettings["FromMail"].ToString();
       //     = ConfigurationManager.AppSettings["word"].ToString();

       // //creating the object of MailMessage
       // MailMessage mailMessage = new MailMessage();
       // mailMessage.From = new MailAddress(FromEmailid); //From Email Id
       // mailMessage.Subject = Newsletter; //Subject of Email
       // mailMessage.Body = Newsletter; //body or message of Email
       // mailMessage.IsBodyHtml = true;

       //string[] ToMuliId = ToEmail.Split(',');
       //foreach(var item in lstUsers)
       //{
       //     mailMessage.To.Add(new MailAddress(item.EmailId)); //adding multiple TO Email Id
       //}
      
       // string[] CCId = cc.Split(',');

       // foreach (var item in lstUsers)
       // {
       //     mailMessage.CC.Add(new MailAddress(item.EmailId)); //Adding Multiple CC email Id
       // }

    
       // SmtpClient smtp = new SmtpClient();  // creating object of smptpclient
       // smtp.Host = HostAdd;              //host of emailaddress for example smtp.gmail.com etc

       // //network and security related credentials

       // smtp.EnableSsl = true;
       // NetworkCredential NetworkCred = new NetworkCredential();
       // NetworkCred.UserName = mailMessage.From.Address;
       // NetworkCred.word = ;
       // smtp.UseDefaultCredentials = true;
       // smtp.Credentials = NetworkCred;
       // smtp.Port = 587;
       // smtp.Send(mailMessage); //sending Email

               
        }

        int status =  new OffersBLL().InsertUpdate(objOffers,dtGift );
        var JsonData = new
        {
            Offers = objOffers,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string GetGiftsDetail(int OfferId)
    {
        List <Gifts> objGift = new List <Gifts> ();

        objGift = new OffersBLL().GetGiftsByOfferId(OfferId);

        var JsonData = new
        {

            DetailData = objGift
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
}