﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_manage_attr_stock : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            gettype();
        }
    }

    public void gettype() {

        ddl_type.DataSource = new ProductsBLL().GetAttrType();
        ddl_type.DataValueField = "type_id";
        ddl_type.DataTextField = "title";
        ddl_type.DataBind();
        ListItem li = new ListItem();
        li.Text = "Select Type";
        li.Value = "0";
        ddl_type.Items.Insert(0, li);


    }
    public void reset() {

        hdn_attrid.Value = "";
        tbaddstock.Text = "";
        tblessstock.Text = "";
        tbtitle.Text = "";
        tbitemcode_sku.Text = "";



    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        ds = new ProductsBLL().GetAttrStock("getproduct", tbitemcode.Text.Trim());
        if (ds.Tables[0].Rows.Count>0)
        {
            dv_all.Visible = true;
            ltr_product.Text = "<tr><td style='display: none' class='cls_var'>" + ds.Tables[0].Rows[0]["variationid"].ToString() + "</td><td class='cls_photourl'><img src='../images/product/" + ds.Tables[0].Rows[0]["PhotoUrl"].ToString() + "' class='small-img'/></td><td class='cls_mrp'>" + ds.Tables[0].Rows[0]["MRP"].ToString() + "</td><td class='cls_price'>" + ds.Tables[0].Rows[0]["Price"].ToString() + "</td></tr>";
            GeList();
        }
        else
        {
            dv_all.Visible = false;
        }
 
     
    }
    public void GeList()
    {

        DataSet ds = new DataSet();
        ds = new ProductsBLL().GetAttrStock("getlist", tbitemcode.Text.Trim());
        string str = "";
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            string isdisplay = "block";
            if (ds.Tables[0].Rows[i]["attr_title"].ToString() == "No Attribute")
            {
                isdisplay = "none";
            }

            str += "<tr><td class='cls_attr_id' style='display: none'>" + ds.Tables[0].Rows[i]["attr_id"].ToString() + " </td><td class='cls_type_id' style='display: none'>" + ds.Tables[0].Rows[i]["type_id"].ToString() + " </td><td class='cls_attr_title'>" + ds.Tables[0].Rows[i]["attr_title"].ToString() + " </td><td class='cls_title'>" + ds.Tables[0].Rows[i]["Title"].ToString() + " </td><td class='cls_addstock'>" + ds.Tables[0].Rows[i]["add_stock"].ToString() + " </td><td class='cls_lessstock'>" + ds.Tables[0].Rows[i]["less_stock"].ToString() + " </td><td class='cls_leftstock'>" + ds.Tables[0].Rows[i]["left_stock"].ToString() + " </td><td class='cls_mrp'>" + ds.Tables[0].Rows[i]["maxprice"].ToString() + "</td><td class='cls_price'>" + ds.Tables[0].Rows[i]["saleprice"].ToString() + "</td><td class='cls_itemcode_sku'>" + ds.Tables[0].Rows[i]["itemcode"].ToString() + "</td><td><button type='button' class='btnedit btn btn-primary'>Edit</button></td><td style='display:" + isdisplay+"'><button type='button' class='btndelete btn btn-danger'>Delete</button></td></tr>";
        }
        ltr_list.Text = str;
        reset();

    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        lblerror.Text = "";
        string AddStock = tbaddstock.Text.Trim() == "" ? "0.00" : tbaddstock.Text.Trim();
        string LessStock = tblessstock.Text.Trim() == "" ? "0.00" : tblessstock.Text.Trim();

        if (Convert.ToDecimal(AddStock) < Convert.ToDecimal(LessStock))
        {

            lblerror.Text = "**Less Stock Should't Be Greater Than Add Stock!";
            return;
        }
        int attr_id = hdn_attrid.Value == "" ? 0 : Convert.ToInt32(hdn_attrid.Value);
        new ProductsBLL().InsertUpdateAttrStock("insertupdate",Convert.ToInt32(ddl_type.SelectedValue),attr_id, tbtitle.Text.Trim(), tbitemcode.Text.Trim(),Convert.ToDecimal(AddStock), Convert.ToDecimal(LessStock), tbitemcode_sku.Text.Trim(), tbmrp.Text.Trim(), tbprice.Text.Trim());
        GeList();
    }

    [WebMethod]
    public static void delete(int attr_id)
    {
        new ProductsBLL().DeleteAttrStock("delete", attr_id);
    }
}