﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="uploadimages.aspx.cs" Inherits="backoffice_uploadimages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <form id="frmProducts" runat="server">

 <div id="content" style="padding:6px">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Products</span>
                     
                        <br />
                    </h3>
				   <div class="youhave" style="padding-left:0px">
 
 
  

    <table align="center" border="0"  style="border: 0px solid black;
                margin: 1px" cellpadding="5" cellspacing="0" width="100%">
         <tr>
                    <td>Category</td><td>
                        <asp:DropDownList ID="ddlCategory" width="300px" runat="server" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></td>
                    </tr>
                <tr>
                    <td>Choose Product</td><td>
                        <asp:DropDownList ID="ddlProduct" width="300px" runat="server" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></td>
                    </tr>
        <tr><td>Variation List</td><td>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None"
 Font-Size="Larger"              BorderWidth="2"  >
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
             <Columns>
                   <asp:TemplateField HeaderText="Id" Visible="false">
                     <ItemTemplate>
                         <asp:Label ID="lblId" runat="server" Text='<%#Eval("VariationId") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                
                   <asp:TemplateField  HeaderText="Name" HeaderStyle-Width="300px">
                     <ItemTemplate>
                         <asp:Label ID="lblUnit" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                     </ItemTemplate>

<HeaderStyle Width="300px"></HeaderStyle>
                 </asp:TemplateField>
                  
                 <asp:TemplateField HeaderText="Image">
                    <ItemTemplate>                     
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
             </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle ForeColor="#333333" BackColor="#F7F6F3" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
                                   </td></tr>
        <tr>
            <td></td>
            <td>
            <table><tr><td>
            <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary btn-small" OnClick="btnUpdate_Click" />
            </td>
            <td>
 <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-primary btn-small" OnClick="btnReset_Click" />
            </td></tr>
                </table>
                </td>
            </tr>
        <tr><td></td><td > <asp:TextBox ID="txtSearch" runat="server" placeholder="Enter ProductName to Search" AutoPostBack="true"  Width="438px" OnTextChanged="txtSearch_TextChanged"></asp:TextBox></td></tr>
        <tr>
            <td></td>
            <td >
                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False"  BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical"  OnRowCancelingEdit="GridView2_RowCancelingEdit" OnRowEditing="GridView2_RowEditing" OnRowUpdating="GridView2_RowUpdating" OnPageIndexChanging="GridView2_PageIndexChanging"
                    AllowPaging="true">
                    <AlternatingRowStyle BackColor="#DCDCDC" />
                    <Columns>
                           <asp:TemplateField HeaderText="ProductId" Visible="false">
                            <ItemTemplate>
                              <asp:Label ID="lblPId" runat="server" Text='<%#Eval("ProductId") %>'></asp:Label>                              
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="VariationId" Visible="false">
                            <ItemTemplate>
                              <asp:Label ID="lblVId" runat="server" Text='<%#Eval("VariationId") %>'></asp:Label>                              
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="PhotoUrl" Visible="false">
                            <ItemTemplate>
                              <asp:Label ID="lblPhotoUrl" runat="server" Text='<%#Eval("PhotoUrl") %>'></asp:Label>                              
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Image">
                            <ItemTemplate>
                                <img   src='../ProductImages/T_<%#Eval("PhotoUrl") %>' alt="No Image" style="width:150px;height:150px"/>
                              
                            </ItemTemplate>
                            <EditItemTemplate>
                                  <asp:FileUpload ID="FLUpload_Update" runat="server" />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ProductName">
                            <ItemTemplate>
                                <asp:Label ID="lblProductName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField>
                           <ItemTemplate>
                               <asp:LinkButton ID="LK_Edit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                           </ItemTemplate>
                           <EditItemTemplate>
                                 <asp:LinkButton ID="LK_Update" runat="server" CommandName="Update">Update</asp:LinkButton>
                                <asp:LinkButton ID="LK_Cancel" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>
                           </EditItemTemplate>
                       </asp:TemplateField>
                    </Columns>
                  <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"  FirstPageText="First" LastPageText="Last"/> 
                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#000065" />
                </asp:GridView>
            </td>
        </tr>
        </table>
                       </div>
                       </div>
     </div>
        </form>
</asp:Content>

