﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="manageusers.aspx.cs" Inherits="backoffice_manageusers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">

 <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/custom-css/master.css" rel="stylesheet" />

    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>


      <form id= "frm" runat="server">
    <asp:HiddenField  ID ="hdnusers" runat="server"/>
    <div id="content">

    <script type="text/javascript">
        $(document).ready(
        function () {

            BindGrid();

            $("#btnViewOrder").click(
            function () {


                var myGrid = $('#jQGridDemo'),
                selRowId = myGrid.jqGrid('getGridParam', 'selrow'),
               celValue = myGrid.jqGrid('getCell', selRowId, 'UserId');

                if (celValue == '') {
                    alert("Please select a row from Grid");
                    return;
                }

                window.location = "CustomerInfo.aspx?Id=" + celValue + "";
            }
            );



            $("#btnNewsletter").click(
            function () {


                var myGrid = $('#jQGridDemo'),
                selRowId = myGrid.jqGrid('getGridParam', 'selrow'),
               celValue = myGrid.jqGrid('getCell', selRowId, 'UserId');
                var newsletter = myGrid.jqGrid('getCell', selRowId, 'NewsLetter');

                if (celValue == '') {
                    alert("Please select a row from Grid");
                    return;
                }
                if (newsletter == "false") {
                    newsletter = "true";
                }
                else {
                    newsletter = "false";
                }

                UpdateNewsLetter(celValue, newsletter);
               
            }
            );


            $("#btnActive").click(
            function () {


                var myGrid = $('#jQGridDemo'),
                selRowId = myGrid.jqGrid('getGridParam', 'selrow'),
               celValue = myGrid.jqGrid('getCell', selRowId, 'UserId');
                var newsletter = myGrid.jqGrid('getCell', selRowId, 'IsActive');

                if (celValue == '') {
                    alert("Please select a row from Grid");
                    return;
                }
                if (newsletter == "false") {
                    newsletter = "true";
                }
                else {
                    newsletter = "false";
                }


                UpdateActiveUsers(celValue, newsletter);
               
            }
            );

            function UpdateNewsLetter(celValue, newsletter) {


                $.ajax({
                    type: "POST",
                    data: '{"UserId":"' + celValue + '", "Newsletter": "' + newsletter + '"}',
                    url: "manageusers.aspx/InsertUpdate",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        if (newsletter == "true") {

                            alert("User Subscribed successfully.");
                        }
                        else {
                            alert("User UnSubscribed successfully.");
                        }




                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                        BindGrid();
                    }


                });

            }




            function UpdateActiveUsers(celValue, newsletter) {


                $.ajax({
                    type: "POST",
                    data: '{"UserId":"' + celValue + '", "IsActive": "' + newsletter + '"}',
                    url: "manageusers.aspx/UpdateActive",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);



                        if (newsletter == "true") {

                            alert("User Active successfully.");
                        }
                        else {
                            alert("User InActive successfully.");
                        }




                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                        BindGrid();


                    }


                });

            }






        }
        );
    </script>



             <div id="rightnow">
            <h3 class="reallynow">
                <span>Users List</span>
                <br />
            </h3>
            <div class="youhave">
     



                <table id="jQGridDemo">
                </table>
                <div id="jQGridDemoPager">
                </div>

                <table class="category_table">
                    <tr>
                        <td><div id="btnNewsletter"  class="btn btn-primary btn-small">Subscribe/UnSubscribe Newsletter</div></td>
                        <td><div id="btnViewOrder"  class="btn btn-primary btn-small"  >View User's Order</div></td>
                        <td><div id="btnActive"  class="btn btn-primary btn-small"  >Active/InActive Users</div></td>
                    </tr>
                </table>



               
            </div>
        </div>

            </div>
            </form>

          <script type="text/javascript">


   function BindGrid()
   {
 
         jQuery("#jQGridDemo").GridUnload();  
        jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/manageusers.ashx',
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['UserId','FirstName','LastName','MobileNo','Email','DOB','DOA','Newsletter','IsActive'],
            colModel: [
                        { name: 'UserId',key:true, width: 50,index: 'UserId', stype: 'text',sorttype:'int',hidden:false },
                        { name: 'FirstName',key:true,width: 90,index: 'FirstName', stype: 'text',sorttype:'int',hidden:false },
                        { name: 'LastName',key:true,width: 90, index: 'LastName', stype: 'text',sorttype:'int',hidden:false },
                         { name: 'MobileNo',key:true,width: 80, index: 'MobileNo', stype: 'text',sorttype:'int',hidden:false },
                         { name: 'EmailId',key:true,width: 100, index: 'EmailId', stype: 'text',sorttype:'int',hidden:false },
                         { name: 'strDOB',key:true,width: 80,index: 'strDOB', stype: 'text',sorttype:'int',hidden:false },
                         { name: 'strDOA',key:true,width: 80, index: 'strDOA', stype: 'text',sorttype:'int',hidden:false },
                         { name: 'NewsLetter', index: 'NewsLetter', width: 40, editable: true,edittype:"checkbox",editoptions: { value:"true:false" } , formatter: "checkbox", formatoptions: { disabled: true} },
                         { name: 'IsActive', index: 'IsActive', width: 40, editable: true,edittype:"checkbox",editoptions: { value:"true:false" } , formatter: "checkbox", formatoptions: { disabled: true} },
                        
   		                
   		                
   		                
                       ],
            rowNum: 10,
            mtype: 'GET',

            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'UserId',
            viewrecords: true,
           
            height: "100%",
            width:"400px",
            toolbar: [true, "top"],
            ignoreCase: true,
            sortorder: 'asc',
            caption: "Users List",
            editurl: 'handlers/manageusers.ashx',
             
        });

         var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });
 
        $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: false,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }



                     );
                     }
             
    </script>  

</asp:Content>

