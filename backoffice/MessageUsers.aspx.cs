﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_MessageUsers : System.Web.UI.Page
{
    public string Type { get { return Request.QueryString["Type"] != null ? Convert.ToString(Request.QueryString["Type"]) : ""; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnusers.Value = Type;
        }

    }
}