﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="managecombotype.aspx.cs" Inherits="backoffice_managecombotype" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/custom-css/master.css" rel="stylesheet" />
  <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
  <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
   <script src="js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   
<script language="javascript" type="text/javascript">
    var m_CategoryId = 0;

    function ResetControls() {
        m_CategoryId = 0;
        var txtTitle = $("#txtTitle");
        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");

        txtTitle.focus();
        txtTitle.val("");
        $("#txtDescription").val("");
        btnAdd.css({ "display": "block" });
        btnAdd.html("Add ComboType");

        btnUpdate.css({ "display": "none" });
        btnUpdate.html("Update ComboType");

        $("#chkIsActive").prop("checked", "checked");

        $("#btnReset").css({ "display": "none" });

        $("#hdnId").val("0");
        validateForm("detach");
    }



    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }

    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }

    function InsertUpdate() {

        if (!validateForm("formID")) {
            return;
        }

        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");

        var Id = m_CategoryId;


        var Title = $("#txtTitle").val();
        if ($.trim(Title) == "") {
            $("#txtTitle").focus();

            return;
        }
        $.uiLock('');
        if (Id == "0") {
            btnAdd.unbind('click');
        }
        else {
            btnUpdate.unbind('click');
        }

        btnAdd.html("<img src='img/loader.gif' alt='loading...'/>")
        btnUpdate.html("<img src='img/loader.gif' alt='loading...'/>")
        var Description = $("#txtDescription").val();
        var IsActive = false;


        if ($('#chkIsActive').is(":checked")) {
            IsActive = true;
        }


        $.ajax({
            type: "POST",
            data: '{"id":"' + Id + '", "title": "' + Title + '","Description":"' + Description + '","isActive": "' + IsActive + '"}',
            url: "managecombotype.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == 0) {
                    ResetControls();


                    if (Id == "0") {
                        btnAdd.bind('click', function () { InsertUpdate(); });
                    }
                    else {

                        btnUpdate.bind('click', function () { InsertUpdate(); });
                    }

                    alert("Insertion Failed.ComboType with duplicate name already exists.");
                    return;

                }

                if (Id == "0") {
                    btnAdd.bind('click', function () { InsertUpdate(); });
                    jQuery("#jQGridDemo").jqGrid('addRowData', obj.Category.CategoryId, obj.Category, "last");

                    ResetControls();

                    alert("ComboType is added successfully.");

                }
                else {
                    btnUpdate.bind('click', function () { InsertUpdate(); });
                    ResetControls();
                    var myGrid = $("#jQGridDemo");
                    var selRowId = myGrid.jqGrid('getGridParam', 'selrow');


                    myGrid.jqGrid('setRowData', selRowId, obj.Category);

                    alert("ComboType is Updated successfully.");


                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();

            }



        });

    }

    $(document).ready(
    function () {



        $("#btnAdd").click(
        function () {

            m_CategoryId = 0;
            InsertUpdate();


        }
        );


        $("#btnUpdate").click(
        function () {
            InsertUpdate();


        }
        );


        $("#jQGridDemo").jqGrid('setGridParam',
         {
             onSelectRow: function (rowid, iRow, iCol, e) {
                 m_CategoryId = 0;
                 validateForm("detach");
                 var txtTitle = $("#txtTitle");
                 m_CategoryId = $('#jQGridDemo').jqGrid('getCell', rowid, 'ComboTypeId');


                 if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                     $('#chkIsActive').prop('checked', true);
                 }
                 else {
                     $('#chkIsActive').prop('checked', false);

                 }
                 txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Title'));
                 txtTitle.focus();
                 $("#txtDescription").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Description'))
                 $("#btnAdd").css({ "display": "none" });
                 $("#btnUpdate").css({ "display": "block" });
                 $("#btnReset").css({ "display": "block" });
                 TakeMeTop();
             }
         });

        var DataGrid = jQuery('#jQGridDemo');
        DataGrid.jqGrid('setGridWidth', '500');




        $('#txtTitle').focus();
        $('#txtTitle').keypress(function (event) {


            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {

                InsertUpdate();
            }


        });


        $("#btnReset").click(
        function () {

            ResetControls();

        }
        );


    }
    );

</script>

<form   runat="server" method="post">
   <asp:HiddenField ID="hdnCategory" runat="server"/>
<div id="content"  id="formID">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update ComboType</span>
                     
                        <br />
                    </h3>
				   <div class="youhave">
                   <table cellpadding="0" cellspacing="0" border="0" class="top_table">
                     <tr><td class="headings">Title:</td><td>  <input type="text" name="txtTitle" class="inputtxt validate required alphanumeric"  data-index="1" id="txtTitle" /></td></tr>
                    <tr><td class="headings">Description:</td><td>  <textarea  rows="3" name="txtDescription" class="inputtxt"   id="txtDescription" ></textarea></td></tr>
                   
                     <tr><td class="headings">Is Active:</td><td>     <input type="checkbox" id="chkIsActive" checked=checked data-index="2"  name="chkIsActive" />
                                            </td></tr>

                     </table>

                       <table cellspacing="0" cellpadding="0" class="category_table">
                          <tr>
                            <td> <div id="btnAdd"  class="btn btn-primary btn-small" >Add ComboType</div></td>
                            <td><div id="btnUpdate"  class="btn btn-primary btn-small" style="display:none;" >Update ComboType</div></td>
                            <td><div id="btnReset"  class="btn btn-primary btn-small" style="display:none;width:60px;" >Cancel</div></td>
                         </tr>
                       </table>
                    			 
       
                    </div>
			  </div>
               


               <div id="rightnow">
                    <h3 class="reallynow">
                        <span>Manage ComboTypes</span>
                      
                        <br />
                    </h3>
				    <div class="youhave" >
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>
			  </div>

            </div>
</form>

            <script type="text/javascript">


                jQuery("#jQGridDemo").jqGrid({
                    url: 'handlers/managecombotype.ashx/',
                    ajaxGridOptions: { contentType: "application/json" },
                    datatype: "json",

                    colNames: ['ComboType Id', 'Title', 'Description', 'Is Active'],
                    colModel: [
                                { name: 'ComboTypeId', key: true, index: 'ComboTypeId', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                { name: 'Title', index: 'Title', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                  { name: 'Description', index: 'Description', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                                { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },


                    ],
                    rowNum: 10,

                    mtype: 'GET',
                    loadonce: true,
                    rowList: [10, 20, 30],
                    pager: '#jQGridDemoPager',
                    sortname: 'ComboTypeId',
                    viewrecords: true,
                    height: "100%",
                    width: "400px",
                    sortorder: 'asc',
                    caption: "ComboType List",

                    editurl: 'handlers/managecombotype.ashx',



                });



                $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                           {
                               refresh: false,
                               edit: false,
                               add: false,
                               del: false,
                               search: false,
                               searchtext: "Search",
                               addtext: "Add",
                           },

                           {//SEARCH
                               closeOnEscape: true

                           }

                             );



    </script>
</asp:Content>



