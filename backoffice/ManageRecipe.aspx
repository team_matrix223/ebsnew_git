﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="ManageRecipe.aspx.cs" Inherits="backoffice_ManageRecipe" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
      <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/custom-css/master.css" rel="stylesheet" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <script language="javascript" type="text/javascript">

        var m_ProductId = 0;
        var m_ProductName = "";




        var ProductCollection = [];
        function clsProduct() {
            this.ProductId = 0;
            this.Qty = 0;
            this.Unit = "";
            this.Url = "";
            this.ProductName = "";
       
        }
        function Reset() {

            ProductCollection = [];
            $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            $("#tbProductInfo").append(" <tr><td colspan='100%' align='center'>Ingredients will be shown here</td></tr>");
            $("#ddlRecipeList option").prop("selected", false);
           
        }


        $(document).on("click", "#dvClose", function (event) {

            var RowIndex = Number($(this).closest('tr').index());
            var tr = $(this).closest("tr");
            tr.remove();

            ProductCollection.splice(RowIndex, 1);

            if (ProductCollection.length == 0) {

                $("#tbProductInfo").append(" <tr><td colspan='100%' align='center'>Recipe Products will be shown here</td></tr>");

            }

        });


        $(document).ready(

        function () {


            $("#ddlRecipeList").change(
            function () {
                $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                ProductCollection = [];
                var RecId = $("#ddlRecipeList").val();

                $.ajax({
                    type: "POST",
                    data: '{"RecipeId": "' + RecId + '"}',

                    url: "ManageRecipe.aspx/GetIngredients",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {


                        var obj = jQuery.parseJSON(msg.d);
                        var tr = "";
                        for (var i = 0; i < obj.ProductList.length; i++) {


                            var PName = obj.ProductList[i]["ProductName"];
                            var Qty = obj.ProductList[i]["Qty"];
                            var Unit = obj.ProductList[i]["Unit"];
                            var PId = obj.ProductList[i]["ProductId"];
                            var Url = obj.ProductList[i]["Url"];
                            tr = tr + "<tr><td >" + PName + "</td><td >" + Qty + " " + Unit + "</td> <td><i id='dvClose'><img src='images/trash.png'/></i> </td></tr>";




                            var TO = new clsProduct();

                            TO.ProductId = PId;
                            TO.Qty = Qty;
                            TO.Unit = Unit;
                            TO.Url = Url;
                            TO.ProductName = PName;

                            ProductCollection.push(TO);

                        }
                        $("#tbProductInfo").append(tr);





                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                        $.uiUnlock();
                    }

                });





            }

            );

            $("#btnAdd").click(
            function () {

                var RecId = $("#ddlRecipeList").val();

                if (RecId == 0) {

                    alert("Please select Recipe from List");
                    $("#ddlRecipeList").focus();
                    return;
                }


                var arrProducts = [];
                var arrQty = [];
                var arrUnit = [];
                var arrUrl = [];


                if (ProductCollection.length == 0) {

                    alert("Please add Recipe Products");
                    return;
                }
                $.uiLock('');
                for (var i = 0; i < ProductCollection.length; i++) {


                    arrProducts[i] = ProductCollection[i]["ProductName"];
                    arrQty[i] = ProductCollection[i]["Qty"];
                    arrUnit[i] = ProductCollection[i]["Unit"];
                    arrUrl[i] = ProductCollection[i]["Url"];



                }



                $.ajax({
                    type: "POST",
                    data: '{"RecipeId": "' + RecId + '","arrProductName": "' + arrProducts + '","arrQty": "' + arrQty + '","arrUnit": "' + arrUnit + '","arrUrl": "' + arrUrl + '"}',

                    url: "ManageRecipe.aspx/InsertIngredients",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {


                        var obj = jQuery.parseJSON(msg.d);


                        alert("Ingredients Added Successfully");

                        Reset();




                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                        $.uiUnlock();
                    }

                });








            }
            );

            $("#dvAddQty").click(
            function () {


               
                var Qty = $("#txtQty").val();

                if (!jQuery.isNumeric(Qty)) {
                    alert("Invalid Quantity");
                    $("#txtQty").focus();
                    return;
                }
                var Unit = $("#txtUnit").val();
                m_ProductName = $("#txtRecipeProduct").val();
                var m_Url = $("#txtUrl").val();

                var tr = "<tr><td >" + m_ProductName + "</td><td >" + Qty + " " + Unit + "</td> <td><i id='dvClose'><img src='images/trash.png'/></i> </td></tr>";

                $("#tbProductInfo").append(tr);


                var TO = new clsProduct();

                TO.ProductId = m_ProductId;
                TO.Qty = Qty;
                TO.Unit = Unit;
                TO.Url = m_Url;
                TO.ProductName = m_ProductName;

                ProductCollection.push(TO);

                $("#dvDialog").dialog("close");



            }

            );

        }

        );

    </script>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>


 

<div id="content">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Recipe</span>
                     
                        <br />
                    </h3>
				   <div class="youhave">
                   <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="top_table" style="width:100% !important;">
                     
                   <tr><td colspan="100%" style="color:Red;font-style:italic"><asp:Literal ID="ltMessage" runat="server"></asp:Literal></td></tr>

                    <tr><td class="headings">Choose Recipe:</td><td>  
                     
       <asp:DropDownList ID="ddlRecipes"  AutoPostBack="true" runat="server" 
                            onselectedindexchanged="ddlRecipes_SelectedIndexChanged"></asp:DropDownList>

                     
                     
                     </td></tr>

                     <tr><td class="headings">Title:</td><td>  
                     
                     <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>

                     <asp:RequiredFieldValidator id="reqTitle" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtTitle"></asp:RequiredFieldValidator>
                     
                     </td></tr>

                     
                     <tr><td class="headings">Mobile Recipe:</td><td>  
                     
                     <asp:TextBox ID="txtShortDescription" runat="server" TextMode="MultiLine" Height="100px"></asp:TextBox>

                     
                     
                     </td></tr>

                        <tr><td class="headings">Web Recipe:</td><td> 
                        
                           <FCKeditorV2:FCKeditor ID="EditorDescription" runat="server" BasePath="~/backoffice/fckeditor/"
                                Height="400px">
                            </FCKeditorV2:FCKeditor>

                         </td></tr>
                                                  
                          <tr><td class="headings">Image:</td><td>   
                          <asp:HiddenField ID="hdnImage" runat="server" />
<asp:FileUpload ID="fuImage" runat="server" />

                                            </td></tr>
                      
                     <tr><td class="headings">IsActive:</td><td>     <input type="checkbox" runat="server" id="chkIsActive" checked="checked" data-index="2"  name="chkIsActive" />
                                            </td></tr>

                                            

                                            <tr>
                                            <td></td>
                                            <td>
                                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"/>
                                            </td>
                                            </tr>
                      
                                  

                     </table>
                    			 
       
                    </div>
			  </div>
               


                
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Recipe Ingredients</span>
                     
                        <br />
                    </h3>
				   <div class="youhave">

                  <table class="top_table" style="width:100% !important;">
                      <tr>
                   <td  class="headings">Choose Recipe:</td>
                          <td  colspan="100%">
                   <asp:DropDownList ID="ddlRecipeList" ClientIDMode="Static" runat="server"></asp:DropDownList>
                   </td>
                   </tr>
                  <tr>
                  <td>Product Name</td>
                  <td><input type="text" id="txtRecipeProduct" /></td>
                  
                   <td>Qty:</td><td><input type="text" id="txtQty" /></td> 
                     <td>Unit:</td><td>
     
                    <select id="txtUnit">
                    <option value="Kg">KG</option>
                    <option value="Gram">Gram</option>
                    <option value="Lt">Ltr</option>
                    <option value="ML">ML</option>
     
                    </select>
    
                    </td>
                    <td>Url:</td>
                    <td>
                    <input type="text" id="txtUrl" />
                    </td>
                     
                   </tr>  
                      <tr>
                          <td>
                             <div id="dvAddQty"  class="btn btn-primary btn-small">Add</div>
                        </td> 
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                      </tr>      
                 </table>

                                    <table class="table table-bordered table-striped table-hover item_table" style="width:100%" id="tbProductInfo">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Name
                                                </th>
                                                <th>
                                                    Qty
                                                </th>

                                                <th></th>

                                            </tr>
                                        </thead>

                                    </table>

                                    <table class="category_table">
                                        <tr>
                                            <td>
                                                <div id="btnAdd" class="btn btn-primary btn-small">Submit</div>
                                            </td>
                                        </tr>

                                    </table>
                  
                        </div>
			  </div>
               


            </div>






 

                
 


</form>
 
</asp:Content>


