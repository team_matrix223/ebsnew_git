﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="managebillcancelation.aspx.cs" Inherits="backoffice_managebillcancelation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">

 <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 <link href="css/custom-css/transaction.css" rel="stylesheet" />

    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

    <script type="text/javascript">
      var BillId = 0;
      var Category = 0;
      var DeliveryChrg = 0;
    
        function Reset()
        {

          $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove(); 
          $('#tbProductVariation tr').not(function () { if ($(this).has('th').length) { return true } }).remove(); 
          $("#<%=ltBillNo.ClientID%>").val("");
          $("#<%=ltBillDate.ClientID%>").val("");
          $("#<%=ltCustomer.ClientID%>").val("");
          $("#<%=ltAddress.ClientID%>").val("");
          $("#<%=ltBillAmount.ClientID%>").val("");
          $("#<%=ltRecepientname.ClientID%>").val("");
          $("#<%=ltRecepientPhone.ClientID%>").val("");
          $("#<%=ltRecepientMobile.ClientID%>").val("");

            $("#<%=ltStatus.ClientID%>").val("");
            $("#<%=ltMessage.ClientID%>").val("");
            var dialogDiv = $('#dvEdit');
            dialogDiv.dialog("option", "position", [150, 100]);
            dialogDiv.dialog('close');
           BindGrid();

        
          $("#txtBillNo").val("");
          $("#txtRemarks").val("");
          $("#rdbOrder").prop("checked", false);
          
        }

        var ProductCollection = [];
        function clsProduct() {
            this.BrandName = "";
            this.ProductId = 0;
            this.ProductName = 0;
            this.Qty = 0;
            this.Price = 0;
            this.VariationId = 0;
            this.ProductDescription = "";
            this.SubTotal = 0;
        }



        function AddProductToList() {
            var Price = 0;

            var SelectedRow = jQuery('#jQGridDemoProduct').jqGrid('getGridParam', 'selrow');
            if ($.trim(SelectedRow) == "") {
                alert("No Product is selected to add");
                return;
            }

            var ProductId = $('#jQGridDemoProduct').jqGrid('getCell', SelectedRow, 'ProductId');
            var VariationId = $('#jQGridDemoProduct').jqGrid('getCell', SelectedRow, 'VariationId');

            var ProductName = $('#jQGridDemoProduct').jqGrid('getCell', SelectedRow, 'Name');

            var Qty = $("#txtQty").val();
            Price = $('#jQGridDemoProduct').jqGrid('getCell', SelectedRow, 'Price');

            var Mode = "Plus";

            $.ajax({
                type: "POST",
                data: '{ "BillId": "' + BillId + '","ProductId": "' + ProductId + '","VariationId": "' + VariationId + '","Qty": "' + Qty + '","Price": "' + Price + '","Mode": "' + Mode + '"}',
                url: "managebillcancelation.aspx/UpdateQtyByProductId",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {



                    var obj = jQuery.parseJSON(msg.d);
                    alert("Product is added successfully.");
                    $("#txtQty").val("");
                    var dialogDiv = $('#dvProductOptions');
                    dialogDiv.dialog("option", "position", [500, 200]);
                    dialogDiv.dialog('close');


                    var dialogDiv1 = $('#dvProducts');
                    dialogDiv1.dialog("option", "position", [500, 200]);
                    dialogDiv1.dialog('close');

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    BindTempBills();
                }

            });

        }
        function BindProducts(Category) {

            jQuery("#jQGridDemoProduct").GridUnload();

            jQuery("#jQGridDemoProduct").jqGrid({
                url: 'handlers/ProductsByCategory.ashx?Category=' + Category,
                ajaxGridOptions: { contentType: "application/json" },
                datatype: "json",

                colNames: ['VariationId', 'CategoryId', 'ProductId', 'ProductName', 'Description', 'ShortName', 'Price'],
                colModel: [

                  { name: 'VariationId', key: true, index: 'VariationId', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                  { name: 'CategoryId', key: true, index: 'CategoryId', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                  { name: 'ProductId', key: true, index: 'ProductId', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                  { name: 'Name', index: 'Name', width: '180px', stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                  { name: 'Description', key: true, index: 'Description', width: 100, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                  { name: 'ShortName', index: 'ShortName', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                    { name: 'Price', index: 'Price', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true }, hidden: true },
                ],

                rowNum: 10,

                mtype: 'GET',
              toolbar: [true, "top"],
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPagerProduct',
            sortname: 'ProductId',
            viewrecords: true,
            height: "100%",
            width:"300px",
            sortorder: 'desc',
             ignoreCase: true,
            caption: "Products List",
            
        });


         var $grid = $("#jQGridDemoProduct");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText1\">Global search for:&nbsp;</label><input id=\"globalSearchText1\" type=\"text\"></input>&nbsp;<button id=\"globalSearch1\" type=\"button\">Search</button></div>"));
            $("#globalSearchText1").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch1").click();
                }
            });
            $("#globalSearch1").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText1").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });




            $("#jQGridDemoProduct").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    var ProductId = $('#jQGridDemoProduct').jqGrid('getCell', rowid, 'ProductId');
                    var VariationId = $('#jQGridDemoProduct').jqGrid('getCell', rowid, 'VariationId');
                    var item = $.grep(ProductCollection, function (item) {
                        return item.VariationId == VariationId;
                    });
                    if (item.length) {
                        $("#txtQty").val("1");
                        AddProductToList();
                        return;
                    }
                    else {
                        $('#dvProductOptions').dialog(
                      {
                          autoOpen: false,
                          width: 300,
                          height: 150,
                          resizable: false,
                          modal: true,
                      });
                        linkObj = $(this);
                        var dialogDiv = $('#dvProductOptions');
                        dialogDiv.dialog("option", "position", [500, 200]);
                        dialogDiv.dialog('open');
                        return false;
                    }
                }
            });

            $('#jQGridDemoProduct').jqGrid('navGrid', '#jQGridDemoPagerProduct',
                       {
                           refresh: false,
                           edit: false,
                           add: false,
                           del: false,
                           search: false,
                           searchtext: "Search",
                           addtext: "Add",
                       },
                       {//SEARCH
                           closeOnEscape: true
                       }
                         );

            var DataGrid = jQuery('#jQGridDemoProduct');
            DataGrid.jqGrid('setGridWidth', '400');
        }


      function BindTempBills() {


             var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             DeliveryChrg =  $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'DeliveryCharges')
             $("#dvBillno").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillId'));
             $("#dvBillDate").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'strBD'));
             $("#dvCustomer").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'CustomerName'));
             $("#dvBillAmount").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillValue'));
             $("#dvRecepient").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'RecipientFirstName'));
             $("#dvRecepientMobile").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'RecipientMobile'));
             $("#dvRecepientPhone").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'RecipientPhone'));
             $("#dvAddress").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'CompleteAddress'));
               $("#dvStatus").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'PaymentStatus'));
              $("#dvMessage").html( $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'ResponseMessage'));
             ProductCollection=[];
             $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove(); 
                       
                         $.ajax({
                         type: "POST",
                         data: '{ "Bid": "' +  $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillId') + '"}',
                         url: "managebillcancelation.aspx/GetTemparoryBills",
                         contentType: "application/json",
                         dataType: "json",
                         success: function (msg) {
               
                           ProductCollection = [];

              
                        var obj = jQuery.parseJSON(msg.d);

                        var tr = "";
                        var Total  = 0;
                       
                        for (var i = 0; i < obj.TempBill.length; i++) {
                  
                            var schemeid = obj.TempBill[i]["SchemeId"];
                            if (schemeid == 0) {
                                tr = tr + "<tr><td><img src='../ProductImages/T_" + obj.TempBill[i]["PhotoUrl"] + "' style='width:50px;height:50px'/></td><td>" + obj.TempBill[i]["BrandName"] + "</td><td>" + obj.TempBill[i]["ProductDescription"] + "</td><td><div id='btnMinus'  class='btn btn-primary btn-small' style='height:6px'>-</div></td><td><input type='text' style ='width:35px' id='q_" + obj.TempBill[i]["ProductId"] + "'  value =" + obj.TempBill[i]["Qty"] + " readonly='readonly' /></td> <td><div id='btnPlus'  class='btn btn-primary btn-small' style='height:6px'>+</div></td><td>" + obj.TempBill[i]["Price"] + "</td><td>" + obj.TempBill[i]["SubTotal"] + "</td><td><div id='" + obj.TempBill[i]["ProductId"] + "_" + obj.TempBill[i]["VariationId"] + "'  name='dvClose' ><img src='images/trash.png'/></div> </td></tr>";
                                Total = Number(Total) + Number(obj.TempBill[i]["SubTotal"]);
                            }
                            else {
                                tr = tr + "<tr><td><img src='../ProductImages/T_" + obj.TempBill[i]["PhotoUrl"] + "' style='width:50px;height:50px'/></td><td>" + obj.TempBill[i]["BrandName"] + "</td><td>" + obj.TempBill[i]["ProductDescription"] + "</td><td></td><td><input type='text' style ='width:20px' id='q_" + obj.TempBill[i]["ProductId"] + "'  value =" + obj.TempBill[i]["Qty"] + " readonly='readonly' /></td> <td></td><td>" + obj.TempBill[i]["Price"] + "</td><td>" + obj.TempBill[i]["Amount"] + "</td><td><img src='../img/free.gif' /></td></tr>";
                            }
                        // tr = tr + "<tr><td>" + obj.TempBill[i]["ProductDescription"] + "</td><td><div id='btnMinus'  class='btn btn-primary btn-small'>-</div></td>";
                        // tr=tr+"<td><input type='text' style ='width:50px' id='q_"+obj.TempBill[i]["ProductId"]+"'  value =" +obj.TempBill[i]["Qty"] +" /></td>";
                        //tr=tr+"<td><div id='btnPlus'  class='btn btn-primary btn-small'>+</div></td><td>" + obj.TempBill[i]["Price"] + "</td>";
                        //tr=tr+"<td>" + obj.TempBill[i]["SubTotal"] + "</td>";
                        //tr=tr+"<td><div id='"+ obj.TempBill[i]["ProductId"]+"_"+ obj.TempBill[i]["VariationId"] +"'  name='dvClose' ><img src='images/trash.png'/></div> </td>";
                        //tr=tr+"</tr>";
                     
                         var PO = new clsProduct();
                         PO.ProductId = obj.TempBill[i]["ProductId"] ;
                         PO.ProductName = obj.TempBill[i]["ProductName"] ;
                         PO.ProductDescription = obj.TempBill[i]["ProductDescription"] ;
                         PO.Qty = obj.TempBill[i]["Qty"] ; 
                         PO.Price = obj.TempBill[i]["Price"] ; 
                         PO.VariationId = obj.TempBill[i]["VariationId"];

                         ProductCollection.push(PO);
                          
                         //Total = Number(Total) + Number (obj.TempBill[i]["SubTotal"] ); 
                        
                         
                      }
                      
                      
                       $("#tbProductInfo").append(tr);
                       $("div[id='dvsbtotal']").html(" Rs. " + Total);
                      $("div[id='dvdelivery']").html(" Rs. " + DeliveryChrg);
                      $("div[id='dvnetAmount']").html(" Rs. " + Number(Number(Total)+Number(DeliveryChrg)));

                 },
                complete: function (msg) {
              
            BindProducts(Category);
            }

        });
      }
        function CancelBill()
        {

               
           var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
           var BillId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillId');
           var OrderId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'OrderId');
           var Status = "";
            if ($("#rdbOrder").prop("checked") == true) {
                 status = "True";
             }
             else {
                 status = "False";
             }
           var CancelRemarks = $("#txtRemarks").val();
            if($.trim(CancelRemarks) == "")
           {
             alert("Enter Remarks For bill cancelation");
             return;
           }
            $.ajax({
            type: "POST",
            data: '{"BillId":"' + BillId + '", "OrderId": "' + OrderId + '","CancelRemarks": "' + CancelRemarks + '","Status": "' + status + '"}',
            url: "managebillcancelation.aspx/InsertCancelBill",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == 0) {

                    alert("Cancelation Failed");
                    return;
                }

               
                 
                alert("Bill has been Cancelled successfully.");
                Reset();
                
                var dialogDiv = $('#dvPopup');
                dialogDiv.dialog("option", "position", [150, 50]);
                dialogDiv.dialog('close');

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });


        }



        $(document).on("click", "div[name='dvClose']", function (event) {

          var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');

           if($('#jQGridDemo').jqGrid('getCell', SelectedRow, 'PaymentStatus') == "Success")
           {
           alert("This order can not be Edit ..... Payment has been done for this Bill");
           return;
           }
           else
           {


          var id=$(this).attr("id");
          var mm=[];
          mm=id.split('_');
          var ProductId = mm[0];
          var VariationId= mm[1];
         var RowIndex = Number($(this).closest('tr').index());
        

                $.ajax({
                    type: "POST",
                    data: '{ "BI": "' + BillId + '","ProductId": "' + ProductId  + '","VariationId": "' + VariationId  + '"}',
                    url: "managebillcancelation.aspx/DeleteTempBills",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {
                        


                        var obj = jQuery.parseJSON(msg.d);   
                       
                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {
                   
                    BindTempBills();
                    }
                 });
                 }
    });




     $(document).on("click", "#btnPlus", function (event) {
         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');

           if($('#jQGridDemo').jqGrid('getCell', SelectedRow, 'PaymentStatus') == "Success")
           {
           alert("This Bill can not be Edit ..... Payment has been done for this Bill");
           return;
           }
           else
           {

        var RowIndex = Number($(this).closest('tr').index());
        var PId = ProductCollection[RowIndex]["ProductId"];

        var Mode="Plus";
        var VariationId = ProductCollection[RowIndex]["VariationId"];
        var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"];

//        $("#cq_" + PId + VariationId).html("<img src='images/progressbar.gif' style='margin-top:4px' alt='...'/>");
        UpdateQty(BillId,PId,VariationId,Qty,Price,Mode);
        }
      
    });
    $(document).on("click", "#btnMinus", function (event) {
         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');

           if($('#jQGridDemo').jqGrid('getCell', SelectedRow, 'PaymentStatus') == "Success")
           {
           alert("This Bill can not be Edit ..... Payment has been done for this Bill");
           return;
           }
           else
           {
       
        var RowIndex = Number($(this).closest('tr').index());

        var PId = ProductCollection[RowIndex]["ProductId"];
        var Mode = "Minus";
        var VariationId = ProductCollection[RowIndex]["VariationId"];
         var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"]; 
         UpdateQty(BillId,PId,VariationId,Qty,Price,Mode);
         }
      
    });
  
    function UpdateQty(BillId,PId,VariationId,Qty,Price,Mode) {
     
        $.ajax({
            type: "POST",
            data: '{"BillId":"' + BillId + '","ProductId":"' + PId + '","VariationId":"' + VariationId + '","Qty":"' + Qty + '","Price":"' + Price + '","Mode":"' + Mode + '"}',
            url: "managebillcancelation.aspx/UpdateQtyByProductId",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
//                 $("#cq_" + PId+ VariationId).html(Qty);

//                $("#cq_" + PId + VariationId).html(Qty);
//                $("#span" +PId + VariationId).html(obj.Calc.ProductSubTotal);

//                 if (Qty == 0) {
//                    $("#tp_" +PId + VariationId).remove();
//                }
                    BindTempBills();
              
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
           
                $.uiUnlock();

            }
        });
    }

     function UpdateBill() {
        
         
         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
         BillId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillId');
         $.ajax({
            type: "POST",
            data: '{"BillId":"' + BillId + '"}',
            url: "managebillcancelation.aspx/BillUpdate",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                 
                alert("Bill Updated successfully.");
                 var dialogDiv = $('#dvEdit');
                dialogDiv.dialog("option", "position", [170, 150]);
                dialogDiv.dialog('close');
                Reset();
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });

    }



//     function UpdateStatusDeliver() {
//        
//         
//         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
//        if($.trim(SelectedRow) == "")
//           {
//             alert("No Record is selected to Mark as delivered");
//             return;
//           }
//        else
//        {
//        var OrderId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'OrderId');
//       
//        
//         $.ajax({
//            type: "POST",
//            data: '{"OI":"' + OrderId + '"}',
//            url: "managebillcancelation.aspx/UpdateStatusDeliver",
//            contentType: "application/json",
//            dataType: "json",
//            success: function (msg) {

//                var obj = jQuery.parseJSON(msg.d);

//                 
//                alert("Order Delivered successfully.");
//           
//                Reset();
//            },
//            error: function (xhr, ajaxOptions, thrownError) {

//                var obj = jQuery.parseJSON(xhr.responseText);
//                alert(obj.Message);
//            },
//            complete: function () {
//                $.uiUnlock();
//            }
//        });


//        BindGrid();
//        }

//    }



        $(document).ready(
        function () {

            $("#txtDateFrom").datepicker({
                yearRange: '1900:2030',
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm-dd-yy'
            });


            $("#txtDateTo").datepicker({
                yearRange: '1900:2030',
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm-dd-yy'
            });

            $("#txtDateTo,#txtDateFrom").val($("#<%=hdnDate.ClientID%>").val());
            BindGrid();




            $("#btnGo").click(
        function () {

            BindGrid();

        }
        );



           $("#btnSave").click(
        function () {

            UpdateBill();

        }
        );


//           $("#btnDeliver").click(
//        function () {
//            
//            
//            UpdateStatusDeliver();

//        }
//        );
        
        $("#btnCancelEdit").click(
        function () {
     
            Reset();
             
               });



        $("#btnCancel").click(
        function () {
             
               
           var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
           if($.trim(SelectedRow) == "")
           {
             alert("No Bill is selected to cancel");
             return;
           }
          else
          {

              var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');

               if($('#jQGridDemo').jqGrid('getCell', SelectedRow, 'PaymentStatus') == "Success")
              {
                     alert("This Bill can not be Cancelled ..... Payment has been done for this Bill");
                 return;
                }
              else
              {
             var BillId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillId');

             
             var OrderId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'OrderId');
             var Billstatus =  $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Status');

            if(Billstatus == "Billed")
            {
            alert("Payment Against this bill has been received ....this bill cann't be Cancelled");
            return;
           }
             $("#txtBillNo").val($('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillId'));
            $('#dvPopup').dialog(
            {
            autoOpen: false,

            width:400,
            height:250,
            resizable: false,
            modal: true,
  
            });
            linkObj = $(this);
            var dialogDiv = $('#dvPopup');
            dialogDiv.dialog("option", "position", [170, 150]);
            dialogDiv.dialog('open');
            return false;
            }
            }
        }
        );


             $("#btnCancelConfirm").click(
        function () {
        
           CancelBill();
        }
        );

         $("#btnEdit").click(
        function () {

         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
            if($.trim(SelectedRow) == "")
           {
             alert("No Bill is selected to Edit");
             return;
           }
          BillId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillId');
          var Billstatus =  $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Status');

          if(Billstatus == "Billed")
          {
          alert("Payment Against this bill has been received ....this bill cann't be Edit");
          return;
          }
            $.ajax({
                    type: "POST",
                    data: '{ "BI": "' + BillId + '"}',
                    url: "managebillcancelation.aspx/InsertTemp",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {
                        


                        var obj = jQuery.parseJSON(msg.d);   

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {
                    BindTempBills();
                    
                    }

                });
        
           
                   $('#dvEdit').dialog(
            {
            autoOpen: false,

            width:800,
            height:700,
            resizable: false,
            modal: true,
  
            });
            linkObj = $(this);
            var dialogDiv = $('#dvEdit');
            dialogDiv.dialog("option", "position", [150, 100]);
            dialogDiv.dialog('open');
            return false;
        }
        );




        
            $("#btnAddProduct").click(
        function () {
                   
        
         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');

           if($('#jQGridDemo').jqGrid('getCell', SelectedRow, 'PaymentStatus') == "Success")
           {
           alert("Products can not be added now ..... Payment has been done for this Bill");
           return;
           }
           else
           {

         $('#dvProducts').dialog(
            {
            autoOpen: false,

            width:450,
            height:440,
            resizable: false,
            modal: true,
  
            });
            linkObj = $(this);
            var dialogDiv = $('#dvProducts');
            dialogDiv.dialog("option", "position", [170, 120]);
            dialogDiv.dialog('open');
            return false;
                
        }

        }
        );



           $("#<%=ddlCategories.ClientID%>").change(function () {

            if ($(this).val() == "0") {

                jQuery("#jQGridDemo").GridUnload();
                Category = 0;
                return;
            }

          Category = $("#<%=ddlCategories.ClientID %>").val()
           
            BindProducts(Category);
        });



         $("#btnAdd").click(
        function () {
            if ($.trim($("#txtQty").val()) == "") {
                alert("Enter Product Qty");
                return;
            }
            AddProductToList();
        }
        );


        });
    
    </script>



        <form   runat="server" id="formID" method="post">

     <asp:HiddenField ID="hdnDate" runat="server"/>
  
<div id="content">

             <div id="rightnow">
            <h3 class="reallynow">
                <span>Bills List</span>
                <br />
            </h3>
            <div class="youhave">
                                <table class="top_table">
                                <tr><td>Date From:</td><td>                         
                                <input type="text" readonly="readonly"   class="form-control input-small" style="width:120px;background-color:White"  id="txtDateFrom" /></td><td>Date To:</td><td><input type="text" readonly="readonly"  class="form-control input-small" style="width:120px;background-color:White"    id="txtDateTo" />
                                </td>
                                <td><div id="btnGo"  class="btn btn-primary btn-small"  >Go</div></td>
                                </tr>
                                </table>

                <table id="jQGridDemo">
                </table>
                <div id="jQGridDemoPager">
                </div>

                                            <table class="category_table" cellspacing="0" cellpadding="0">
                                            <tr>
                                             <td> <div id="btnEdit"  class="btn btn-primary btn-small" >Edit</div></td>
                                            <td> <div id="btnCancel"  class="btn btn-primary btn-small" >Cancel Bill</div></td>
                                             <%--<td>&nbsp;</td><td> <div id="btnDeliver"  class="btn btn-primary btn-small" >Mark As Delivered</div></td>--%>
                                            <td> <div id="btnPrint"  class="btn btn-primary btn-small" >Print</div></td>
                                            </tr>
                                            </table>
            </div>
        </div>
        <div id="dvPopup"  style="display:none;">

<h3 class="reallynow">
                <span>Bill Detail</span>
                <br />
            </h3>
<table >
<tr>
<td>
 <label>BillNo</label>
</td>
<td>
<input type="text" readonly="readonly"   class="form-control input-small" style="width:120px;background-color:White"  id="txtBillNo" />
</td>
</tr>
<tr>
<td>
 <label>Cancelation Remarks *</label>
</td>
<td>
 <textarea type="text"  class="form-control input-small validate required " id = "txtRemarks" style="width:150px"></textarea>
</td>
</tr>
<tr>
<td>
<label><input type="checkbox" name = "CheckBox" value= "Order" id="rdbOrder">Cancel Order</label>
</td>
</tr>
<tr>
<td>

 <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnCancelConfirm"  class="btn btn-primary btn-small" >Cancel</div></td>
                                            
                                            </tr>
                                            </table>
</td>

</tr>



</table>
</div>
            </div>



            <div id="dvEdit" style="display:none">
  <table width="100%" cellpadding="0" >
                    

                     <tr>
                     <td valign="top">
                     <table style="width:100%">
                     <tr>
                     <td valign="top">
                     <table  cellpadding="1" style="border:solid 1px silver; width:100%;height:172px">
                         <tr style="background-color:#E6E6E6;font-weight:bold;height:10px">
                         <td colspan="100%">Bill Information</td>
                         </tr>
                         
                    <tr>
                    <td colspan="100%" valign="top">
                     

                    <table cellpadding ="5">
                      <tr>
                     <td align="right">Bill No:</td><td><div id="dvBillno"><asp:Literal ID="ltBillNo" runat ="server"></asp:Literal></div></td>
                     </tr>
                     <tr>
                     <td align="right">Bill Date:</td><td><div id="dvBillDate"><asp:Literal ID="ltBillDate" runat ="server"></asp:Literal></div></td>
       
                     </tr>
                   

                     <tr>
                      <td align="right">Customer Name:</td><td><div id ="dvCustomer"><asp:Literal ID="ltCustomer" runat ="server"></asp:Literal></div></td>
                       
                       </tr>
                         <tr>
                     <td align="right">Bill Amount:</td><td><div id="dvBillAmount"><asp:Literal ID="ltBillAmount" runat ="server"></asp:Literal></div></td>
       
                     </tr>
                      <tr><td style="text-align:right">Payment Status:</td><td colspan="100%"><div id ="dvStatus"><asp:Literal ID="ltStatus" runat ="server"></asp:Literal></div></td><td></td></tr>
                      <tr><td style="text-align:right">Error Message:</td><td colspan="100%"><div id ="dvMessage"><asp:Literal ID="ltMessage" runat ="server"></asp:Literal></div></td><td></td></tr>

                      </table>

                      </td>
   
                     </tr>
                    
                     

                    
                    
                    
                    
                    </table>
                    
                    </td>
                    </tr>
                   
                     </table>
                  </td>
                    
                     <td  valign="top">
                     
                     <table  cellpadding="1" style="border:solid 1px silver;width:100%" >
                         <tr style="background-color:#E6E6E6;font-weight:bold">
                         <td colspan="100%">Customer Information</td>
                         </tr>
                    <tr>
                    <td colspan="100%">
                     

                    <table>
                       
                      <tr><td style="text-align:right">Recepient Name:</td><td colspan="100%"><div id ="dvRecepient"><asp:Literal ID="ltRecepientname" runat ="server"></asp:Literal></div></td><td></td></tr>
                    <tr><td style="text-align:right">Address:</td><td colspan="100%"><div id ="dvAddress"><asp:Literal ID="ltAddress" runat ="server"></asp:Literal></div></td><td></td></tr>
                    <tr><td style="text-align:right">Recepient Mobile:</td><td colspan="100%"><div id ="dvRecepientMobile"><asp:Literal ID="ltRecepientMobile" runat ="server"></asp:Literal></div></td><td></td></tr>
                    <tr><td style="text-align:right">Recepient Phone:</td><td colspan="100%"><div id ="dvRecepientPhone"><asp:Literal ID="ltRecepientPhone" runat ="server"></asp:Literal></div></td><td></td></tr>
                 
                    </table>
                    
                    </td>
                    </tr>
                   
                     </table>
                     </td>

                     </tr>
                    
                     </table>
                     <tr>
                     <td colspan="100%">
                  

                     
                      
             <table class="table table-bordered table-striped table-hover" style="width:100%" id="tbProductInfo">
										<thead>
											<tr>
                                                	<th style="width: 150px">
                                            Image
                                        </th>
                                        <th style="width: 150px">
                                            BrandName
                                        </th>
												<th style="width: 150px">
                                            Description
                                        </th>
                                       <th style="width: 20px">
                                        </th>
                                        <th style="width: 35px">
                                            Qty
                                        </th>
                                        <th style="width: 20px">
                                        </th>
                                          <th style="width: 80px">
                                            Price
                                        </th>
                                         <th style="width: 100px">
                                            Amount
                                        </th>
                                        <th style="width: 50px"></th>
                                           

											</tr>
										</thead>
										 
										
										</table>

                      
                     
                      
                     
                     
                     </td>
                     </tr>

                     <tr>
                     <td  >

                    <table>
                    <tr>
                    <td valign="top" style="width:200px"><%--<table>
                    <tr><td>Remarks:</td><td> <textarea id="txtRemarks" rows="3" style="width:190px"></textarea></td></tr>
                      
                   
                   
                     </table>--%>
                     </td>
                    
                    <td valign="top"><table>
                     <tr>
                     <td valign="top">
                     <table>
                     <tr><td >Gross Amount:</td><td><div id ="dvsbtotal"></div></td></tr>

                     <tr><td>Delivery Charges:</td><td><div id ="dvdelivery"></div></td></tr>
                     <tr><td >Net Amount:</td><td><div id ="dvnetAmount"></div></td></tr>
                    
                     </table>
                     </td>
                     <td valign="top">
                     <table>
                     <tr><td>  <div id="btnSave"  class="btn btn-primary btn-small" style="width:100px" >Update Bill</div></td></tr>
                      <tr><td>  <div id="btnCancelEdit" style="background-color:Maroon;width:100px" class="btn btn-primary btn-small"  >Cancel</div></td></tr>
                   
                   
                     </table>
                     
                     </td>
                         <td>
                           <div id="btnAddProduct" class="btn btn-primary btn-small" style="width:150px;height:30px;background:green;vertical-align:middle;padding-top:25px">Add New Product</div>
  </td>
                     </tr>

                     </table></td>
                    </tr>
                    </table> 
                     </td>
                     </tr>
                  
  
  </div>

  
<div id="dvProducts"  style="display:none;">

<h3 class="reallynow">
                <span>Products</span>
                <br />
            </h3>
           
<table>
  <tr>
  <td style="width:150px" >
                <asp:DropDownList ID="ddlCategories" runat="server" style="width:150px" >
               
                </asp:DropDownList>
            </td>
  </tr>
 </table>        

<table>
<tr>
<td style ="width:300px">
<table id="jQGridDemoProduct">
                </table>
                <div id="jQGridDemoPagerProduct">
                </div>

</td>
</tr>

</table>

</div>

<div id="dvProductOptions"  style="display:none;">

<h3 class="reallynow">
                <span>Product Qty</span>
                <br />
            </h3>
        
<table id="tbProductVariation">
 
</table>
<table>
<tr>
<td>
 <table>
            <tr>
                <td>Qty:</td>
            <td style="width:150px" >
                <input id="txtQty" type="text"  class="form-control input-small" style="width:120px;background-color:White"/>
            </td>
            </tr>
            </table>

</td>

<td>
 <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd"  class="btn btn-primary btn-small" >Add</div></td>
                                            
                                            </tr>
                                            </table>

</td>
</tr>

</table>

</div>
</form>



<script type="text/javascript">
         function BindGrid()
     {
      var DateFrom=$("#txtDateFrom").val();
     var DateTo=$("#txtDateTo").val();
     jQuery("#jQGridDemo").GridUnload();

              jQuery("#jQGridDemo").jqGrid({
            url: 'handlers/BillsByDate.ashx?dateFrom='+DateFrom+'&dateTo='+DateTo,
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['BillNo','OrderId','BillDate', 'CustomerName', 'Recipient','Address','Mobile','BillValue','CustomerId','RecipientFirstName','RecipientLastName','RecipientMobile','RecipientPhone','City','Area','Street','Address','Pincode','BillValue','DisPer','DisAmt','ServiceTaxPer','ServiceTaxAmt','ServiceChargePer','ServiceChargeAmt','VatPer','VatAmt','NetAmount','Remarks','ExecutiveId','IPAddress','Status','DeliveryCharges','PaymentStatus','Message','Type'],
            colModel: [
              { name: 'BillId',key:true, index: 'BillId', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:false },
                        { name: 'OrderId', index: 'OrderId', width: 150, stype: 'text', sortable: true, hidden: false, editable: true ,editrules: { required: true }},
                        { name: 'strBD', index: 'strBD', width: 150, stype: 'text', sortable: true, hidden: false, editable: true ,editrules: { required: true }},
   		                { name: 'CustomerName', index: 'CustomerName', width: 150, stype: 'text', sortable: true,hidden:false, hidden: false, editable: true ,editrules: { required: true }},
                        { name: 'Recipient', index: 'Recipient', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'CompleteAddress', index: 'CompleteAddress', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                        { name: 'RecipientMobile', index: 'RecipientMobile', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
   		                { name: 'BillValue', index: 'BillValue', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                        { name: 'CustomerId',key:true, index: 'CustomerId', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
                        { name: 'RecipientFirstName', index: 'RecipientFirstName', width: 150, stype: 'text', sortable: true, hidden: true, editable: true ,editrules: { required: true }},
   		                { name: 'RecipientLastName', index: 'RecipientLastName', width: 150, stype: 'text', sortable: true,hidden:false, hidden: true, editable: true ,editrules: { required: true }},
                        { name: 'RecipientMobile', index: 'RecipientMobile', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'RecipientPhone', index: 'RecipientPhone', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},   
   		                { name: 'City', index: 'City', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'Area', index: 'Area', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'Street',key:true, index: 'Street', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:true },
                        { name: 'Address', index: 'Address', width: 150, stype: 'text', sortable: true, hidden: true, editable: true ,editrules: { required: true }},
   		                { name: 'Pincode', index: 'Pincode', width: 150, stype: 'text', sortable: true,hidden:false, hidden: true, editable: true ,editrules: { required: true }},
                        { name: 'BillValue', index: 'BillValue', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'DisPer', index: 'DisPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'DisAmt', index: 'DisAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
   		                { name: 'ServiceTaxPer', index: 'ServiceTaxPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'ServiceTaxAmt', index: 'ServiceTaxAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'ServiceChargePer', index: 'ServiceChargePer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'ServiceChargeAmt', index: 'ServiceChargeAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'VatPer', index: 'VatPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'VatAmt', index: 'VatAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
   		                { name: 'NetAmount', index: 'NetAmount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'Remarks', index: 'Remarks', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'ExecutiveId', index: 'ExecutiveId', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'IPAddress', index: 'IPAddress', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'Status', index: 'Status', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                        { name: 'DeliveryCharges', index: 'DeliveryCharges', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                         { name: 'PaymentStatus', index: 'PaymentStatus', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                           { name: 'ResponseMessage', index: 'ResponseMessage', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},

                           { name: 'OrderType', index: 'OrderType', width: 150, stype: 'text', sortable: true, editable: true, hidden: false ,editrules: { required: true }},
                       ],
            rowNum: 10,
            toolbar: [true, "top"],
            mtype: 'GET',
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'BillId',
            viewrecords: true,
            height: "100%",
            width:"400px",
            sortorder: 'desc',
            caption: "Bills List",
            ignoreCase: true,
           
                    
             
        });
        var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });


           $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: false,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }

                     );
         
    
        var DataGrid = jQuery('#jQGridDemo');
        DataGrid.jqGrid('setGridWidth', '600');
        

      }
        
    </script>
</asp:Content>

