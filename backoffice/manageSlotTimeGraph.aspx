﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="manageSlotTimeGraph.aspx.cs" Inherits="backoffice_manageSlotTimeGraph" %>
    <%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
        <asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
            <form runat="server" id="frmPurchase">
                <div id="content" style="padding:6px">

                    <div id="rightnow" style="margin-top: 10px">
                        <h3 class="reallynow">
                <span> Slot Time Graph(Slot Time v/s No Of Customers) on Daily Basis</span>
                <br />
            </h3>

                        <div>

                            <asp:Chart ID="Chart2" runat="server" Height="303px" Width="737px" Palette="SeaGreen" DataSourceID="SqlDataSource1">

                                <series>
                                    <asp:Series Name="Series1" XValueMember="SlotName" YValueMembers="Count">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                        <AxisX Title="Slot Time" Interval="1"></AxisX>
                                        <AxisY Title="No of Customers"></AxisY>
                                    </asp:ChartArea>
                                </chartareas>

                            </asp:Chart>

                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="shopping_sp_GraphicalDailySlotWiseReport" SelectCommandType="StoredProcedure"></asp:SqlDataSource>

                        </div>
                    </div>

                    <div id="rightnow" style="margin-top: 10px">
                        <h3 class="reallynow">
                <span> Slot Time Graph(Slot Time v/s No Of Customers) on Weekly Basis</span>
                <br />
            </h3>
                        <table style="text-align:center;padding-left:270px">
                            <tr>
                                <td style="font-weight:bold">Choose Week:</td>
                                <td>
                                    <asp:DropDownList ID="ddlweek" runat="server" AutoPostBack="True">
                                        <asp:ListItem Value="1">1st Week</asp:ListItem>
                                        <asp:ListItem Value="2">2nd Week</asp:ListItem>
                                        <asp:ListItem Value="3">3rd Week</asp:ListItem>
                                        <asp:ListItem Value="4">4th Week</asp:ListItem>

                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>

                        <div>

                            <asp:Chart ID="Chart3" runat="server" Height="303px" Width="737px" Palette="EarthTones" DataSourceID="SqlDataSource2">

                                <series>
                                    <asp:Series Name="Series1" XValueMember="SlotName" YValueMembers="Count" ChartType="StackedColumn">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                        <AxisX Title="Slot Time" Interval="1"></AxisX>
                                        <AxisY Title="No of Customers"></AxisY>
                                    </asp:ChartArea>
                                </chartareas>

                            </asp:Chart>

                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="shopping_sp_GraphicalWeeklySlotWiseReport" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="ddlweek" DefaultValue="1" Name="Week" PropertyName="SelectedValue" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>

                        </div>
                    </div>

                    <div id="rightnow" style="margin-top: 10px">
                        <h3 class="reallynow">
                <span> Slot Time Graph(Slot Time v/s No Of Customers) on Monthly Basis</span>
                <br />
            </h3>
                        <table style="text-align:center;padding-left:270px">
                            <tr>
                                <td style="font-weight:bold">Choose Month:</td>
                                <td>
                                    <asp:DropDownList ID="ddlmonth" runat="server" AutoPostBack="True">
                                        <asp:ListItem Value="1">January</asp:ListItem>
                                        <asp:ListItem Value="2">February</asp:ListItem>
                                        <asp:ListItem Value="3">March</asp:ListItem>
                                        <asp:ListItem Value="4">April</asp:ListItem>
                                        <asp:ListItem Value="5">May</asp:ListItem>
                                        <asp:ListItem Value="6">June</asp:ListItem>
                                        <asp:ListItem Value="7">July</asp:ListItem>
                                        <asp:ListItem Value="8">August</asp:ListItem>
                                        <asp:ListItem Value="9">September</asp:ListItem>
                                        <asp:ListItem Value="10">October</asp:ListItem>
                                        <asp:ListItem Value="11">November</asp:ListItem>
                                        <asp:ListItem Value="12">December</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        <div>
                            <asp:Chart ID="Chart1" runat="server" Height="303px" Width="737px" DataSourceID="SqlDataSource3">

                                <series>
                                    <asp:Series Name="Series1" ChartType="StackedColumn" XValueMember="SlotName" YValueMembers="Count">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                        <AxisX Title="Slot Time" Interval="1"></AxisX>
                                        <AxisY Title="No Of Customers"></AxisY>
                                    </asp:ChartArea>
                                </chartareas>

                            </asp:Chart>

                            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="shopping_sp_GraphicalMonthlSlotWiseReport" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="ddlmonth" DefaultValue="1" Name="Month" PropertyName="SelectedValue" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>

                        </div>
                    </div>

                    <div id="rightnow" style="margin-top: 10px">
                        <h3 class="reallynow">
                <span> Slot Time Graph(Slot Time v/s No Of Customers) on Yearly Basis</span>
                <br />
            </h3>
                        <table style="text-align:center;padding-left:270px">
                            <tr>
                                <td style="font-weight:bold">Choose Year:</td>
                                <td>
                                    <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True">
                                        <asp:ListItem Value="2015">2015</asp:ListItem>
                                        <asp:ListItem Value="2016">2016</asp:ListItem>
                                        <asp:ListItem Value="2017">2017</asp:ListItem>
                                        <asp:ListItem Value="2018">2018</asp:ListItem>
                                        <asp:ListItem Value="2019">2019</asp:ListItem>
                                        <asp:ListItem Value="2020">2020</asp:ListItem>
                                        <asp:ListItem Value="2021">2021</asp:ListItem>
                                        <asp:ListItem Value="2022">2022</asp:ListItem>
                                        <asp:ListItem Value="2023">2023</asp:ListItem>
                                        <asp:ListItem Value="2024">2024</asp:ListItem>
                                        <asp:ListItem Value="2025">2025</asp:ListItem>
                                        <asp:ListItem Value="2026">2026</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>

                        <div>
                            <asp:Chart ID="Chart4" runat="server" Height="303px" Width="737px" Palette="Excel" DataSourceID="SqlDataSource4">

                                <series>
                                    <asp:Series Name="Series1" ChartType="StackedColumn" XValueMember="SlotName" YValueMembers="Count">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                        <AxisX Title="Slot Time" Interval="1"></AxisX>
                                        <AxisY Title="No Of Customers"></AxisY>
                                    </asp:ChartArea>
                                </chartareas>

                            </asp:Chart>

                            <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:DB_CONN %>" SelectCommand="shopping_sp_GraphicalYearlySlotWiseReport" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DropDownList2" DefaultValue="2015" Name="Year" PropertyName="SelectedValue" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>

                        </div>
                    </div>

                </div>
            </form>
        </asp:Content>