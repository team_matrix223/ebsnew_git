﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_productdiscount : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCategories();

        }
    }

    public void BindCategories()
    {

        ddlCategory.DataSource = new CategoryBLL().GetByLevel(1);
        ddlCategory.DataValueField = "CategoryId";
        ddlCategory.DataTextField = "Title";
        ddlCategory.DataBind();
        ListItem li = new ListItem();
        li.Text = "--Category--";
        li.Value = "0";
        ddlCategory.Items.Insert(0, li);


        ddlCategory_.DataSource = new CategoryBLL().GetByLevel(1);
        ddlCategory_.DataValueField = "CategoryId";
        ddlCategory_.DataTextField = "Title";
        ddlCategory_.DataBind();
        ListItem li_ = new ListItem();
        li_.Text = "--Category--";
        li_.Value = "0";
        ddlCategory_.Items.Insert(0, li_);


        ddl_brand.DataSource = new BrandBLL().GetAll();
        ddl_brand.DataValueField = "BrandId";
        ddl_brand.DataTextField = "Title";
        ddl_brand.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Brand--";
        li1.Value = "0";
        ddl_brand.Items.Insert(0, li1);
    }

    protected void btn_submit_Click(object sender, EventArgs e)
    {
        DisData("insertupdate",null,dd_dis.SelectedValue,Convert.ToInt32(ddlCategory.SelectedValue), Convert.ToDecimal(tbdis.Text.Trim()),tbstart_date.Text.Trim(),tbend_date.Text.Trim());
       
    }

    protected void btn_submit_Click_(object sender, EventArgs e)
    {
        DisData("insertupdate", null, dd_dis.SelectedValue, Convert.ToInt32(ddlSubCat.SelectedValue), Convert.ToDecimal(tbdis_.Text.Trim()), tbstart_date_.Text.Trim(), tbend_date_.Text.Trim());

    }

    protected void btn_submit_Click_b(object sender, EventArgs e)
    {
        DisData("insertupdate", null, dd_dis.SelectedValue, Convert.ToInt32(ddl_brand.SelectedValue), Convert.ToDecimal(tbdis_b.Text.Trim()), tbstart_date_b.Text.Trim(), tbend_date_b.Text.Trim());

    }
    public void DisData(string req, int? dis_id, string dis_type, int? cat_subcat_id, decimal? dis_per, string start_date, string end_date)
    {

        DataSet ds = new CategoryBLL().InsertUpdateCatDis(req,dis_id,dis_type,cat_subcat_id,dis_per,start_date,end_date);
        gv_display.DataSource = ds.Tables[0];
        gv_display.DataBind();

    }
    protected void dd_dis_SelectedIndexChanged(object sender, EventArgs e)
    {
        dv_cat.Visible = false;
        dv_subcat.Visible = false;
        dv_brand.Visible = false;
        string dd_type = dd_dis.SelectedValue;
        if (dd_type=="C")
        {
            dv_cat.Visible = true;
        }
        else if (dd_type == "SC")
        {
            dv_subcat.Visible = true;
        }
        else if (dd_type == "B")
        {
            dv_brand.Visible = true;
        }
        DisData("", null, dd_type, null, null, null, null);
    }

    protected void gv_display_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "_delete")
        {
            string dd_type = dd_dis.SelectedValue;
            int dis_id = Convert.ToInt32(e.CommandArgument);

           DisData("delete", dis_id, dd_type, null, null, null, null);



        }
    }
 
    public void BindSubCategories(int ParentId)
    {
        try
        {
   

                ddlSubCat.DataSource = new CategoryBLL().GetByParentId(ParentId);
                ddlSubCat.DataTextField = "Title";
                ddlSubCat.DataValueField = "CategoryId";
                ddlSubCat.DataBind();
                ListItem li = new ListItem();
                li.Text = "--SubCategory--";
                li.Value = "0";
                ddlSubCat.Items.Insert(0, li);
          
 
        }
        finally
        {
        }

    }

    protected void ddlCategory__SelectedIndexChanged(object sender, EventArgs e)
    {
        BindSubCategories(Convert.ToInt32(ddlCategory_.SelectedValue));
    }
}
        
        
