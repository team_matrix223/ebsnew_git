﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="MonthlyVisit.aspx.cs" Inherits="backoffice_MonthlyVisit" %>
    <%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
        <asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
            <link href="css/custom-css/graph-chart.css" rel="stylesheet" />

            <form runat="server" id="frmMonthlyVisit">

                <div id="content">

                    <div id="rightnow">
                        <h3 class="reallynow">
                <span> Customer's Daily Visit</span>
                <br />
            </h3>
                        <table class="top_table">
                            <tr>
                                <td style="font-weight:bold">Choose Month:</td>
                                <td>
                                    <asp:DropDownList ID="ddlmonth" runat="server" AutoPostBack="True">
                                        <asp:ListItem Value="1">January</asp:ListItem>
                                        <asp:ListItem Value="2">February</asp:ListItem>
                                        <asp:ListItem Value="3">March</asp:ListItem>
                                        <asp:ListItem Value="4">April</asp:ListItem>
                                        <asp:ListItem Value="5">May</asp:ListItem>
                                        <asp:ListItem Value="6">June</asp:ListItem>
                                        <asp:ListItem Value="7">July</asp:ListItem>
                                        <asp:ListItem Value="8">August</asp:ListItem>
                                        <asp:ListItem Value="9">September</asp:ListItem>
                                        <asp:ListItem Value="10">October</asp:ListItem>
                                        <asp:ListItem Value="11">November</asp:ListItem>
                                        <asp:ListItem Value="12">December</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>

                        <div>

                            <asp:Chart ID="Chart2" runat="server" Height="303px" Width="737px" DataSourceID="SqlDataSource2" Palette="SeaGreen">

                                <series>
                                    <asp:Series Name="Series1" XValueMember="Date" YValueMembers="Data">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                        <AxisX Title="Date" Interval="1"></AxisX>
                                        <AxisY Title="No Of Visits"></AxisY>
                                    </asp:ChartArea>
                                </chartareas>

                            </asp:Chart>

                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:PSMCartConnectionString %>" SelectCommand="shopping_sp_GraphicalDailyVisitor" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="ddlmonth" DefaultValue="1" Name="Month" PropertyName="SelectedValue" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>

                        </div>
                    </div>

                    <div id="rightnow">
                        <h3 class="reallynow">
                <span> Customer's Weekly Visit</span>
                <br />
            </h3>

                        <div>

                            <asp:Chart ID="Chart3" runat="server" Height="303px" Width="737px" DataSourceID="SqlDataSource3" Palette="EarthTones">

                                <series>
                                    <asp:Series Name="Series1" XValueMember="Series" YValueMembers="Val" ChartType="StackedColumn">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                        <AxisX Title="Week" Interval="1"></AxisX>
                                        <AxisY Title="No Of Visits"></AxisY>
                                    </asp:ChartArea>
                                </chartareas>

                            </asp:Chart>

                            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:PSMCartConnectionString %>" SelectCommand="shopping_sp_GraphicalWeeklyVisitor" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="ddlmonth" DefaultValue="1" Name="Month" PropertyName="SelectedValue" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>

                        </div>
                    </div>

                    <div id="rightnow">
                        <h3 class="reallynow">
                <span> Customer's Monthly Visit</span>
                <br />
            </h3>
                        <div>
                            <asp:Chart ID="Chart1" runat="server" DataSourceID="SqlDataSource1" Height="303px" Width="737px">

                                <series>
                                    <asp:Series Name="Series1" XValueMember="mnth" YValueMembers="Data" ChartType="StackedColumn">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                        <AxisX Title="Month" Interval="1"></AxisX>
                                        <AxisY Title="No Of Visits"></AxisY>
                                    </asp:ChartArea>
                                </chartareas>

                            </asp:Chart>

                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:PSMCartConnectionString %>" SelectCommand="shopping_sp_GraphicalMonthlyVisitor" SelectCommandType="StoredProcedure"></asp:SqlDataSource>

                        </div>
                    </div>

                    <div id="rightnow">
                        <h3 class="reallynow">
                <span> Customer's Yearly Visit</span>
                <br />
            </h3>
                        <div>
                            <asp:Chart ID="Chart4" runat="server" DataSourceID="SqlDataSource4" Height="303px" Width="737px" Palette="Excel">

                                <series>
                                    <asp:Series Name="Series1" XValueMember="Year" YValueMembers="Value" ChartType="StackedColumn">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                        <AxisX Title="Year" Interval="1"></AxisX>
                                        <AxisY Title="No Of Visits"></AxisY>
                                    </asp:ChartArea>
                                </chartareas>

                            </asp:Chart>

                            <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:PSMCartConnectionString %>" SelectCommand="shopping_sp_GraphicalYearlyVisitor" SelectCommandType="StoredProcedure"></asp:SqlDataSource>

                        </div>
                    </div>

                    <div id="rightnow">
                        <h3 class="reallynow">
                <span> Users Registered City Wise</span>
                <br />
            </h3>
                        <div>
                            <asp:Chart ID="Chart5" runat="server" DataSourceID="SqlDataSource5" Height="303px" Width="737px" Palette="Pastel">

                                <series>
                                    <asp:Series Name="Series1" XValueMember="City" YValueMembers="Value" ChartType="StackedColumn">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1">
                                        <AxisX Title="Cities" Interval="1"></AxisX>
                                        <AxisY Title="No Of Visits"></AxisY>
                                    </asp:ChartArea>
                                </chartareas>

                            </asp:Chart>

                            <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:PSMCartConnectionString %>" SelectCommand="shopping_sp_GraphicalCityVisitor" SelectCommandType="StoredProcedure"></asp:SqlDataSource>

                        </div>
                    </div>

                </div>
            </form>

        </asp:Content>