﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="updateproductslist.aspx.cs" Inherits="backoffice_updateproductslist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
        <link href="css/custom-css/master.css" rel="stylesheet" />

    <form id="frmProducts" runat="server">

 <div id="content" style="padding:6px">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Update Products</span>
                     
                        <br />
                    </h3>
				   <div class="youhave">
 
 
  

    <table align="center" border="0" cellpadding="5" cellspacing="0" class="top_table">
        <tr>
            <td> Mode</td>
            <td>
                <asp:DropDownList ID="ddlMode" runat="server">
                    <asp:ListItem Value="Product" Selected="True" >Product</asp:ListItem>
                    <asp:ListItem Value="Variation">Variation</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
               <tr>
            <td> Enter Id</td>
            <td>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </td>
        </tr>
        </table>

                       <table class="category_table">
                            <tr>
                  
                                  <td>
                                    <asp:Button ID="btnDeleteProduct" runat="server" Text="Delete Product" CssClass='btn btn-primary btn-small' OnClick="btnDeleteProduct_Click" OnClientClick="return confirm('Are you sure to delete?')" />
                                </td>
                              <td>
                                 <asp:Button ID="btnClearImage" runat="server" Text="Clear Image" CssClass='btn btn-primary btn-small' OnClick="btnClearImage_Click"  OnClientClick="return confirm('Are you sure to delete?')"/>
                              </td>
                                <td>
                                     <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass='btn btn-primary btn-small' OnClick="btnReset_Click" />
                                </td>      
                            </tr>
                      </table>

                       </div>
                       </div>

     </div>
        </form>
</asp:Content>

