﻿<%@ Page Title="" Language="C#" MasterPageFile="~/backoffice/admin.master" AutoEventWireup="true" CodeFile="comboproducts.aspx.cs" Inherits="backoffice_comboproducts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
  <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/custom-css/master.css" rel="stylesheet" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   
<script language="javascript" type="text/javascript">
    var m_ProductId = 0;

    function ResetControls() {
        m_ProductId = 0;
        $("#<%=ddlCategory.ClientID %>").val("0");
        $("#<%=ddlSubCategories.ClientID %>").val("0");
        $("#<%=ddlCategoryLevel3.ClientID %>").val("0");
        $("#<%=ddlBrand.ClientID %>").val("0");
       
        $("#chkIsActive").prop("checked", "checked");
        $("#FileUpload1").val("");
       
        $("#txtName").val("");
        $("#<%=ddlCategory.ClientID %>").focus();
      
        $("#txtShortName").val("");
        $("#txtDesc").val("");
        $("#txtShortDesc").val("");

        $("#<%=ddlUnits.ClientID %>").val("");
       
        $("#txtQty").val("");
        $("#<%=ddltype.ClientID %>").val("");
        $("#txtPrice").val("");
        $("#txtMRP").val("");
       
        $("#txtItemCode").val("");
        $("#txtDescr").val("");



        btnAdd.css({ "display": "block" });
        btnAdd.html("Add Products");
       

        btnUpdate.css({ "display": "none" });
        btnUpdate.html("Update Products");

     
        $("#btnReset").css({ "display": "none" });

        BindGrid();
       
    }







    function InsertUpdate() {

        if (!validateForm("formID")) {
            return;
        }
        var ProductId = m_ProductId;
        var CategoryId = $("#<%=ddlCategory.ClientID %>").val();

     
        if (CategoryId == "") {
            $("#<%=ddlCategory.ClientID %>").focus();

            return;
        }
        var SubCategoryId = $("#<%=ddlSubCategories.ClientID %>").val();
        if (SubCategoryId == null) {
            SubCategoryId = "0";
        }
       
        var CategoryLevel3 = $("#<%=ddlCategoryLevel3.ClientID %>").val();

       
        if (CategoryLevel3 == null) {

            CategoryLevel3 = "0";
        }
        
        var Brand = $("#<%=ddlBrand.ClientID %>").val();
       
        if (Brand == "") {
            Brand = "0";
        }
       
        var Name = $("#txtName").val();
        if ($.trim(Name) == "") {
            $("#txtName").focus();

            return;
        }
      
        var SHortName = $("#txtShortName").val();
     
        var Description = $("#txtDesc").val();
      
        var ShortDesc = $("#txtShortDesc").val();
       
        var IsActive = false;

        if ($('#chkIsActive').is(":checked")) {
            IsActive = true;
        }
       
        var Unit = $("#<%=ddlUnits.ClientID %>").val();
      
        var Qty = $("#txtQty").val();
       
        var Type = $("#<%=ddltype.ClientID %>").val();
      
        var Price = $("#txtPrice").val();
      
        var MRP = $("#txtMRP").val();
      
        var ItemCode = $("#txtItemCode").val();
       
        var Descr = $("#txtDescr").val();
       
        var photourl = "";
        var fileUpload = $("#FileUpload1").get(0);
        var files = fileUpload.files;

        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            if (files[i].name.length > 0) {
                photourl = files[i].name;
            }

            data.append(files[i].name, files[i]);
        }

        if (m_ProductId == 0 && photourl == "") {
            alert("Please select Image for SubGroup");
            return;
        }

        if (files.length > 0) {
            var options = {};
            options.url = "handlers/ComboproductFileUploader.ashx";
            options.type = "POST";
            options.async = false;
            options.data = data;
            options.contentType = false;
            options.processData = false;
            options.success = function (msg) {

                photourl = msg;

            };

            options.error = function (err) { };

            $.ajax(options);


            if (photourl.length > 0) {
                EventPhotoUrl = photourl;
            }
        }

    
        $.ajax({
            type: "POST",
            data: '{"ProductId":"' + ProductId + '", "CategoryId": "' + CategoryId + '","SubCategoryId": "' + SubCategoryId + '","CategoryLevel3": "' + CategoryLevel3 + '","Brand": "' + Brand + '","Name": "' + Name + '","ShortName": "' + SHortName + '","Description": "' + Description + '","ShortDesc": "' + ShortDesc + '","IsActive": "' + IsActive + '","Unit": "' + Unit + '","Qty": "' + Qty + '","Type": "' + Type + '","Price": "' + Price + '","MRP": "' + MRP + '","ItemCode": "' + ItemCode + '","Descr": "' + Descr + '","PhotoUrl": "' + EventPhotoUrl + '"}',
            url: "comboproducts.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == 0) {

                    alert("Insertion Failed.Brand with duplicate name already exists.");
                    return;
                }

                if (ProductId == "0") {
                    BindGrid();

                    alert("Product is added successfully.");
                    ResetControls();
                }
                else {

                   
                    BindGrid();
                    alert("Product is Updated successfully.");
                    ResetControls();
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });

    }




    $(document).ready(
    function () {
        BindGrid();

        $("#btnReset").click(
        function () {

            ResetControls();
        }
        );

        $("#btnAdd").click(
        function () {

            m_ProductId = 0;
            InsertUpdate();
        }
        );


        $("#btnUpdate").click(
        function () {

            InsertUpdate();
        }
        );


        $("#<%=ddlCategoryLevel3.ClientID %>").change(
            function () {


                $("#dvrepr").html("");


                var sid = $("#<%=ddlCategoryLevel3.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');

                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managemygroups.aspx/BindCategoryLevel3",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);



                        $("#dvrepr").html(obj.ProductHtml);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();

                    }

                });


            }

            );




        $("#<%=ddlSubCategories.ClientID %>").change(
            function () {


                $("#dvrepr").html("");
                $("#<%=ddlCategoryLevel3.ClientID %>").html('');

                var sid = $("#<%=ddlSubCategories.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');


                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managemygroups.aspx/BindSubCategories",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        $("#<%=ddlCategoryLevel3.ClientID %>").html(obj.SubCatOptions);
                        $("#dvrepr").html(obj.ProductHtml);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();

                    }

                });


            }

            );










        $("#<%=ddlCategory.ClientID %>").change(
            function () {

                $("#dvrepr").html("");


                $("#<%=ddlSubCategories.ClientID %>").html('');
                $("#<%=ddlCategoryLevel3.ClientID %>").html('');
                var sid = $("#<%=ddlCategory.ClientID %>").val();
                if (sid == "0") {

                    return;
                }

                $.uiLock('');


                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' + $(this).val() + '"}',
                    url: "managemygroups.aspx/BindSubCategories",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        $("#<%=ddlSubCategories.ClientID %>").html(obj.SubCatOptions);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {
                        $.uiUnlock();

                    }

                });


            }

            );



    }
    );

</script>

<form runat="server" id="formID" method="post">
    <asp:HiddenField ID="hdnCategory" runat="server" />
    
    <div id="content">
     <div id="Div1">
         <div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Products</span>
                     
                        <br />
                    </h3>
				   <div class="youhave">
                   <table cellpadding="0" cellspacing="0" border="0" class="top_table">
                     <tr><td class="headings">Categroy Level 1:</td><td>  
                     
                      <asp:DropDownList ID="ddlCategory" runat="server"  class =" validate ddlrequired">
                                          <asp:ListItem Text="--Category Level 1--" Value="0"></asp:ListItem>
                                     </asp:DropDownList>
                                                                       
                                 
                     </td></tr>
                     <tr><td class="headings">Categroy Level 2:</td><td>  
                     
                      <asp:DropDownList ID="ddlSubCategories" runat="server" class =" validate ddlrequired" 
                                        
                                         Width="330px"  >
                                          <asp:ListItem Text="--Category Level 2--" Value="0"></asp:ListItem>
                                     </asp:DropDownList>
                                                                       
                              
                                        
                     </td></tr>

                      <tr><td class="headings">Categroy Level 3:</td><td>  
                     
                      <asp:DropDownList ID="ddlCategoryLevel3" runat="server">
                                          <asp:ListItem Text="--Category Level 3--" Value="0"></asp:ListItem>
                                     </asp:DropDownList>
             
                     </td></tr>

                       <tr><td class="headings">Brand:</td><td>  
                     
                      <asp:DropDownList ID="ddlBrand" runat="server">
                                          <asp:ListItem Text="--Choose Brand--" Value="0"></asp:ListItem>
                                     </asp:DropDownList>
             
                     </td></tr>

                     <tr><td class="headings">Name:</td><td>  <input type="text" name="txtName" class="inputtxt validate required"  data-index="1" id="txtName" /></td></tr>
                    
                     <tr><td class="headings">ShortName:</td><td>  <input type="text" name="txtShortName" class="inputtxt"  data-index="1" id="txtShortName" /></td></tr>
                    
                     <tr><td class="headings">Description:</td><td><textarea type="text"  class="form-control input-small " id = "txtDesc"></textarea></td></tr>
                      <tr><td class="headings">Short Desc:</td><td><textarea type="text"  class="form-control input-small " id = "txtShortDesc"></textarea></td></tr>
                       
                       
                     <tr>
                     <td class="Titles">
                            Is Active:
                        </td>
                        <td>
                            <input type="checkbox" id="chkIsActive" checked="checked" data-index="2" name="chkIsActive" />
                        </td>
                    </tr>          
                    
                </table>
                    
                       

                   <div id="dvTopBasket">
                    <table class="midle_table">
                        <tr>
                           <td colspan="100%"> 
                                    <h3 class="reallynow">
                                <span>Price List</span>
                                <br>
                               </h3>
                           </td>
                        </tr>
                        <tr>

                            <td colspan="100%">
                                <input type="file" id="FileUpload1" />
                            </td>
                        </tr>
                        <tr>

                            <td>
                                <asp:DropDownList ID="ddlUnits" runat="server" class=" validate ddlrequired">
                                    <asp:ListItem Text="Units" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="NOS" Value="nos"></asp:ListItem>
                                    <asp:ListItem Text="KG" Value="kg"></asp:ListItem>
                                    <asp:ListItem Text="GRAM" Value="gm"></asp:ListItem>
                                    <asp:ListItem Text="LITER" Value="lt"></asp:ListItem>
                                    <asp:ListItem Text="PIECE" Value="pcs"></asp:ListItem>
                                    <asp:ListItem Text="ML" Value="ml"></asp:ListItem>
                                </asp:DropDownList>
                            </td>

                            <td>
                                <input type="text" name="txtQty" placeholder="Qty" class="inputtxt validate required" data-index="1" id="txtQty" />
                            </td>

                            <td>
                                <asp:DropDownList ID="ddltype" runat="server" class=" validate ddlrequired">
                                    <asp:ListItem Text="Type" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="CARTON" Value="carton"></asp:ListItem>
                                    <asp:ListItem Text="BOTTLE" Value="bottle"></asp:ListItem>
                                    <asp:ListItem Text="POLY BAG" Value="poly bag"></asp:ListItem>
                                    <asp:ListItem Text="POLY PACK" Value="poly pack"></asp:ListItem>
                                    <asp:ListItem Text="PACKET" Value="packet"></asp:ListItem>
                                    <asp:ListItem Text="CUP" Value="cup"></asp:ListItem>
                                    <asp:ListItem Text="BOX" Value="box"></asp:ListItem>
                                    <asp:ListItem Text="TIN" Value="tin"></asp:ListItem>
                                    <asp:ListItem Text="POUCH" Value="pouch"></asp:ListItem>
                                    <asp:ListItem Text="JAR" Value="jar"></asp:ListItem>
                                </asp:DropDownList>
                            </td>

                        </tr>
                        <tr>

                            <td>
                                <input type="text" name="txtPrice" placeholder="Price" class="inputtxt validate required" data-index="1" id="txtPrice" />
                            </td>

                            <td>
                                <input type="text" name="txtMRP" placeholder="MRP" class="inputtxt validate required" data-index="1" id="txtMRP" />
                            </td>
                            <td>
                                <input type="text" name="txtItemCode" class="inputtxt validate required" data-index="1" id="txtItemCode" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="100%">
                                <input type="text" name="txtDescr" placeholder="Description" class="inputtxt" data-index="1" id="txtDescr" />
                            </td>
                        </tr>

                    </table>
                </div>	 

                        <table cellspacing="0" cellpadding="0" class="category_table">
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <div id="btnAdd" class="btn btn-primary btn-small">
                                                Add Products</div>
                                        </td>
                                        <td>
                                            <div id="btnUpdate" class="btn btn-primary btn-small" style="display: none;">
                                                Update Products</div>
                                        </td>
                                        <td>
                                            <div id="btnReset" class="btn btn-primary btn-small" style="display: none;">
                                                Cancel</div>
                                        </td>
                                    </tr>
                                </table>
                    </div>

                                

			  </div>

        </div>

        <div id="rightnow">
            <h3 class="reallynow">
                <span>Manage Combo Products</span>
                <br />
            </h3>
            <div class="youhave">
            <table>
            <tr>
            <td valign="top">
              <table id="jQGridDemo">
                </table>
                <div id="jQGridDemoPager">
                </div>
            </td>

           </tr>

            
            </table>
            

              
            </div>
        </div>
    </div>
    </form>
        <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/managecomboproducts.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['ProductId', 'CategoryId', 'SubCategoryId','CategoryLevel3','Brand','Name','ShortName','Description','ShortDescription','PhotoUrl','Unit','Qty','Type','Price','MRP','ItemCode','VDescription','IsActive'],
                        colModel: [
                                    { name: 'ProductId', key: true, index: 'ProductId', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
                                    { name: 'CategoryId', index: 'CategoryId', width: 200, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                                     { name: 'SubCategoryId', index: 'SubCategoryId', width: 200, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                                      { name: 'CategoryLevel3', index: 'CategoryLevel3', width: 200, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                                       { name: 'BrandId', index: 'BrandId', width: 200, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                                      { name: 'Name', index: 'Name', width: 200, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                                      { name: 'ShortName', index: 'ShortName', width: 200, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                                      { name: 'Description', index: 'Description', width: 200, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                                      { name: 'ShortDescription', index: 'ShortDescription', width: 200, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                                      { name: 'PhotoUrl', index: 'PhotoUrl', width: 200, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                                      { name: 'Unit', index: 'Unit', width: 200, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                                      { name: 'Qty', index: 'Qty', width: 200, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                                      { name: 'Type', index: 'Type', width: 200, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                                      { name: 'Price', index: 'Price', width: 200, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                                      { name: 'MRP', index: 'MRP', width: 200, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                                       { name: 'ItemCode', index: 'ItemCode', width: 200, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                                      { name: 'descr', index: 'descr', width: 200, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },


                                    { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox",hidden: true, editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },


                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'ProductId',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Combo Products List",

                        editurl: 'handlers/managecomboproducts.ashx',



                    });




                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_ProductId = 0;
                    validateForm("detach");
                    var txtName = $("#txtName");
                    m_ProductId = $('#jQGridDemo').jqGrid('getCell', rowid, 'ProductId');


                    if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                        $('#chkIsActive').prop('checked', true);
                    }
                    else {
                        $('#chkIsActive').prop('checked', false);

                    }
                    txtName.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Name'));
                     txtName.focus();
                    EventPhotoUrl=$('#jQGridDemo').jqGrid('getCell', rowid, 'PhotoUrl');
                    $("#txtShortName").val($('#jQGridDemo').jqGrid('getCell', rowid, 'ShortName'))
                    $("#txtDesc").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Description'))
                    $("#txtShortDesc").val($('#jQGridDemo').jqGrid('getCell', rowid, 'ShortDescription'))
                    $("#FileUpload1").val("");
                    $("#<%=ddlUnits.ClientID%>").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Unit'))
                    $("#<%=ddlBrand.ClientID%>").val($('#jQGridDemo').jqGrid('getCell', rowid, 'BrandId'))
                    $("#<%=ddltype.ClientID%>").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Type'))
                     $("#txtQty").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Qty'))
                    
                     $("#txtPrice").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Price'))
                    
                     $("#txtMRP").val($('#jQGridDemo').jqGrid('getCell', rowid, 'MRP'))
                     $("#txtItemCode").val($('#jQGridDemo').jqGrid('getCell', rowid, 'ItemCode'))
                     $("#txtDescr").val($('#jQGridDemo').jqGrid('getCell', rowid, 'descr'))
                     var Category = $('#jQGridDemo').jqGrid('getCell', rowid, 'CategoryId')
                     var SubCategory = $('#jQGridDemo').jqGrid('getCell', rowid, 'SubCategoryId')
                     var Category3 = $('#jQGridDemo').jqGrid('getCell', rowid, 'CategoryLevel3')
                    $("#<%=ddlCategory.ClientID%> option[value='" + Category + "']").prop("selected", true);



                    $("#<%=ddlCategory.ClientID %>").val(Category);
                     var Cat1 = Category;
                $.ajax({
                    type: "POST",
                    data: '{ "CatId": "' +Cat1 + '"}',
                    url: "comboproducts.aspx/BindSubCategories",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        $("#<%=ddlSubCategories.ClientID %>").html(obj.SubCatOptions);

                    }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function (msg) {

                    $("#<%=ddlSubCategories.ClientID %>").val(SubCategory);
                    var Subcat = SubCategory;
                      $.ajax({
                     type: "POST",
                     data: '{ "CatId": "' + Subcat + '"}',
                     url: "comboproducts.aspx/BindSubCategories",
                     contentType: "application/json",
                     dataType: "json",
                     success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        $("#<%=ddlCategoryLevel3.ClientID %>").html(obj.SubCatOptions);
                        $("#dvrepr").html(obj.ProductHtml);
 
                     }, error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                     },
                     complete: function (msg) {
                     $("#<%=ddlCategoryLevel3.ClientID %>").val(Category3)
                     

                        $.uiUnlock();

                    }

                });

                }


                });

                   
                   
                   
                   
                    $("#btnAdd").css({ "display": "none" });
                    $("#btnUpdate").css({ "display": "block" });
                    $("#btnReset").css({ "display": "block" });
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>

</asp:Content>

