﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class backoffice_managebrand : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    if (!User.IsInRole("ManageBrands"))
        //    {
        //        //Response.Redirect("default.aspx");

        //    }
        //}
    }
    [WebMethod]
    //public static string Insert(int BrandId, string Title, bool IsActive, HttpContext context)
    public static string Insert()
    {
        //Brand objBrand = new Brand()
        //{
        //    BrandId = Request,
        //    Title = Title.Trim().ToUpper(),
        //    IsActive = IsActive,
        //    AdminId = 0,

        //};
        //int status = new BrandBLL().InsertUpdate(objBrand);
        //var JsonData = new
        //{
        //    City = objBrand,
        //    Status = status
        //};
        //JavaScriptSerializer ser = new JavaScriptSerializer();
        //return ser.Serialize(JsonData);
        return "ok";
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {

        string str = "";
        if (FileUpload1.HasFile)
        {
            Random rnd = new Random();
            string rno = rnd.Next(100, 1000000).ToString();
            str = rno + FileUpload1.FileName;
            FileUpload1.PostedFile.SaveAs(Server.MapPath("~/images/brand/" + str));

            // System.IO.File.Delete(Request.PhysicalApplicationPath + "~/images/brand/" + hdnfilename_delete);


        }
        //else
        //{

        //    Response.Write("<script>alert('Choose Image');</script>");
        //    return;

        //}

        Brand objBrand = new Brand()
        {
            BrandId = hdnid.Value == "" ? 0 : Convert.ToInt32(hdnid.Value),
            Title = txtTitle.Value.Trim().ToUpper(),
            IsActive = chkIsActive.Checked,
            AdminId = 0,
            Image = str,
            Description = txtdesc.InnerText.Trim()

        };
        int status = new BrandBLL().InsertUpdate(objBrand);
    }
}