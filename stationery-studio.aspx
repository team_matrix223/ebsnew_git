﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="stationery-studio.aspx.cs" Inherits="stationery_studio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="customcss/stationery-studio-page.css" rel="stylesheet" />
		<div class="constraint no-padding">
			<div class="breadcrumbs">
				<ul class="breadcrumbs_container">
					<li class="breadcrumbs_item">
						<a class="breadcrumbs_link" href="/index.aspx">Home</a>
					</li>
					<li class="breadcrumbs_item breadcrumbs_item-active">Stationery Studio</li>
				</ul>
			</div>
		</div>

		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="brand-week-head">
						<h1 class="responsiveProductListHeader_title">Stationery Studio</h1>
						<%--<div class="readmore_content"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p></div>--%>
					</div>
				 </div>
				
			</div>
		</div>
		<!-- widget responsiveSlot6 start -->
				<div class="responsiveSlot6">
					<div class="twoItemEditorial trackwidget">
						<div class="container-fluid">
							<!-- itemOne -->
                            <asp:Repeater ID="rpt_getproducts" runat="server">
                                <ItemTemplate>


							<div class="col-md-6 col-sm-6 col-xs-12 cst-bottom-margin">
								<a href="productdetail.aspx?p=<%#Eval("productid")%>&v=<%#Eval("variationid") %>" class="twoItemEditorial_link">
									<div class="twoItemEditorial_imageWrapper">
										<img src="images/product/<%#Eval("photourl") %>"  alt="" class="twoItemEditorial_image big_image" />
									</div>
									<div class="twoItemEditorial_textContainer">
										<h3 class="twoItemEditorial_itemTitle"><%#Eval("name") %> </h3>
										<%--<p class="twoItemEditorial_itemDescription">Discover the very best vegan products this Veganuary from brands including Aveda, Bulldog Skincare and Ouai.</p>--%>
										<div class="twoItemEditorial_itemCTAText">SHOP NOW</div>
									</div>
								</a>
							</div>
                                    
                                </ItemTemplate>

                            </asp:Repeater>

						</div>
					</div>
				</div>

</asp:Content>

