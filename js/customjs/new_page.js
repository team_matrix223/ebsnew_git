﻿/// <reference path="../vendor/jquery-3.2.1.min.js" />

$(document).ready(function () {
    var m_MinPrice = -1;
    var m_MaxPrice = -1;
    $(document).on("change", "input[name='brandfilter']", function (event) {

        selValues = $("input[name='brandfilter']:checked").map(function () {
            return $(this).val();
        }).get();

        $("#hdnbrands").val(selValues);
  
        $("#Button1").click();

    });
    var min =Number($("#hdnsliderminprice").val());
    var max = Number($("#hdnslidermaxprice").val());
         $(function () {$("#slider-range").slider({
                        range: true,
                        min: min,
                        max: max,
                        values: [min, max],
                        slide: function (event, ui) {
                            $("#amount").val("₹" + ui.values[0] + " -  ₹" + ui.values[1]);
                         
                        },
                        stop: function (event, ui) {
                            var curVal = ui.value;
                            m_MinPrice = ui.values[0];
                            m_MaxPrice = ui.values[1];
                            $("#hdnminprice").val(m_MinPrice);
                            $("#hdnmaxprice").val(m_MaxPrice);
                            $("#Button1").click();
                        }
                    });
                    $("#amount").val("₹" + $("#slider-range").slider("values", 0) + " - ₹" + $("#slider-range").slider("values", 1));

                    //------------------------------------------------

                    $("#slider-range2").slider({
                        range: true,
                        min: min,
                        max: max,
                        values: [min, max],
                        slide: function (event, ui) {
                            $("#amount").val("₹" + ui.values[0] + " -  ₹" + ui.values[1]);
                        },
                        stop: function (event, ui) {
                            var curVal = ui.value;
                            m_MinPrice = ui.values[0];
                            m_MaxPrice = ui.values[1];
                            $("#Button1").click();
                       
                        }
                    });
                    $("#amount2").val("₹" + $("#slider-range2").slider("values", 0) +
  " - ₹" + $("#slider-range2").slider("values", 1));

                    //------------------------------------------------

                });

});