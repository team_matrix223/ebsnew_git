﻿/// <reference path="../vendor/jquery-3.2.1.min.js" />

$(document).ready(function () {
    var pageIndex = 0;
    var pageCount = 1;
    var show_page = 20;
    var disp_page = 1;
    var busy = false;
    var m_MinPrice = -1;
    var m_MaxPrice = -1;
    var prev_m_MinPrice = 0;
    var prev_m_MaxPrice = 0;
    var selValues = "";
    var attrValues = "";
    var colorValues = "";
    Search("", 1, 0);

   

   
    $(document).on("change", "input[name='brandfilter']", function (event) {

  
        selValues = $("input[name='brandfilter']:checked").map(function () {
            return $(this).val();
        }).get();

   
        Search(selValues, 2, pageIndex);


    });
    $(document).on("change", "input[name='attrfilter']", function (event) {
        if ($(this).is(':checked')) {

            $("input[name='attrfilter']:checkbox").attr("checked", false);
            $(this).prop('checked', true)
        }
        else {
            $(this).prop('checked', false)
        }
        attrValues = $("input[name='attrfilter']:checked").map(function () {
            return $(this).attr('value');
        }).get();
        Search(selValues, 2, pageIndex);
  
      


    });

    $(document).on("change", "input[name='colorfilter']", function (event) {
     
        if ($(this).is(':checked')) {
    
            $("input[name='colorfilter']:checkbox").attr("checked", false);
            $(this).prop('checked', true)
        }
        else {
            $(this).prop('checked', false)
        }
        colorValues = $("input[name='colorfilter']:checked").map(function () {
            return $(this).attr('data-facet-value');
        }).get();

     
    
        Search(selValues, 2, pageIndex);



    });

  
    //$(window).scroll(function () {


    //    if ($(window).scrollTop() + $(window).height() > $("#productlist").height() && !busy) {

    //        busy = true;
    //        if (attrValues.length == 0 && colorValues.length == 0 && $("#hdnsearch").val()=='') {
    //            GetRecords();
    //        }
           
    //    }
    //});
    $(document.body).on('click', '.responsivePageSelector', function (e) {

        pageIndex = $(this).text();
        Search(selValues, 2, $(this).text())
   


    });
    $(document.body).on('click', '#prevpage', function (e) {


        //pageIndex = pageIndex - show_page;
        pageIndex --
        if (pageIndex<0) {
            pageIndex = 0;
        }
        GetRecords();

        //if (pageIndex< 1) {
        //    $("#prevpage").prop('disabled', 'disabled');
        //}
        //$("#nxtpage").prop('disabled', 'disabled');
      

    })

    $(document.body).on('click', '#nxtpage', function (e) {
        //pageIndex = pageIndex + show_page;
        pageIndex++;
        GetRecords();
        //if (pageIndex > 1) {
          
        //    $("#prevpage").removeAttr('disabled');
        //}
    
    })
    function GetRecords() {


        //if (pageIndex != 1 && pageIndex <= pageCount) {

      
            var selValues = $("input[name='brandfilter']:checked").map(function () {
                return $(this).val();
            }).get();
            Search(selValues, 3, pageIndex);
        //}
    }
    $("#ddsort").change(function () {

        Search(selValues, 2, pageIndex);

    })
    var min = 0;
    var max = 0;
    var flag=0;
   function priceslider() {
        $("#slider-range").slider({
            range: true,
            min:Math.round(min),
            max: Math.round(max),
            values: [min, max],
            slide: function (event, ui) {
                $("#amount").val("₹" + ui.values[0] + " -  ₹" + ui.values[1]);
            },
            stop: function (event, ui) {
                var curVal = ui.value;
                m_MinPrice = ui.values[0];
                m_MaxPrice = ui.values[1];
                Search(selValues, 2, pageIndex)

            }
        });
        $("#amount").val("₹" + $("#slider-range").slider("values", 0) + " - ₹" + $("#slider-range").slider("values", 1));

        //------------------------------------------------

        $("#slider-range2").slider({
            range: true,
            min: Math.round(min),
            max: Math.round(max),
            values: [min, max],
            slide: function (event, ui) {
                $("#amount2").val("₹" + ui.values[0] + " -  ₹" + ui.values[1]);
            },
            stop: function (event, ui) {
                var curVal = ui.value;
                m_MinPrice = ui.values[0];
                m_MaxPrice = ui.values[1];

                Search(selValues, 2, pageIndex)
            }
        });
        $("#amount2").val("₹" + $("#slider-range2").slider("values", 0) +
" - ₹" + $("#slider-range2").slider("values", 1));

        //------------------------------------------------

    };
    function Search(Brands, type, pageid) {
        $("#productlist").empty();
        if (attrValues.length==0) {
            attrValues = ''
        }
     
        if (type == 1 || type == 2) {
            $("#facet-loading-wrap").css("display", "block");

        }
        else {
            $("#loader").show();
            $("#loader").css("display", "block");

        }

        var l1 = $("#hdnLevel1").val();
        var l2 = $("#hdnLevel2").val();
        var l3 = $("#hdnLevel3").val();

        var BID = $("#hdnBrandId").val();
        var GID = $("#hdnGroupId").val();
        var SGID = $("#hdnSubGroupId").val();
        var GetMethod = $("#hdnGetMethod").val(); 
        var sortby = $("#ddsort").val();
        var search = $("#hdnsearch").val();
        var amt_type = $("#hdnamt_type").val();
        var amt = $("#hdnamt").val();
        $.ajax({
            type: "POST",
            data: '{"Brands":"' + Brands + '","Level1":"' + l1 + '","Level2":"' + l2 + '","Level3":"' + l3 + '","PageId":"' + pageid + '","MinPrice":"' + m_MinPrice + '","MaxPrice":"' + m_MaxPrice + '","BrandId":"' + BID + '","GroupId":"' + GID + '","SubGroupId":"' + SGID + '","GetMethod":"' + GetMethod + '","attrValues":"' + attrValues + '","sortby":"' + sortby + '","colorValues":"' + colorValues + '","searchValues":"' + search + '","amt_type":"' + amt_type + '","amt":"' + amt + '"}',
            url: "list.aspx/AdvancedSearch",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);
                 min =Number(obj.Minprice_slider);
                 max =Number(obj.Maxprice_slider);
                 if (flag == 0)
                 {
                     priceslider();
                     flag = 1;
                 }
             
                $("#imgloading").remove();


                //if (pageIndex >= 2 && pageIndex <= pageCount) {

              
                //    $("#no_ofpage").text(pageIndex)
                //}
                if (type == 3) {
                    $("#productlist").append(obj.ProductData);
                }
                else {


                    //pageIndex = 1;
                    $("#productlist").html(obj.ProductData);
                }


                if (type == 1) {



                    $("#brandlist").html(obj.BrandData);
                    $("#brandlist2").html(obj.BrandData);

                    $("#CatTitle").html(obj.CatTitle);
                    $("#CatDesc").html(obj.CatDesc);

                    $("#categorylist").html(obj.CategoryData);
                    $("#categorylist2").html(obj.CategoryData);
                    $("#attr").html(obj.AttrData);
                    $("#color").html(obj.ColorData);

              

                }

                var totRec = obj.TotalRecords;
                pageCount = Math.ceil(totRec / show_page);
                var paginghtml = "";
                paginghtml=
							"<button type='button' class='responsivePaginationNavigationButton paginationNavigationButtonPrevious' data-direction='previous' id='prevpage'  aria-label='Previous page' title='Previous page'>"+
							"<svg class='' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' width='24px' height='24px' viewBox='0 0 24 24' version='1.1'>"+
							"<polygon points='16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793'></polygon>"+
							"</svg>"+
                            "</button>"+
							"<ul class='responsivePageSelectors'>"
                    for (var i = 1; i < pageCount+1; i++) {
                        paginghtml+="<li><a class='responsivePaginationButton responsivePageSelector' data-page-number='" + i + "'  data-page-active='' aria-label='Page, " + i + "'>" + i + "</a></li>"
                    }
                    paginghtml += "</ul>" +
									"<button id='nxtpage'  type='button' class='responsivePaginationNavigationButton paginationNavigationButtonNext' data-direction='next' aria-label='Next page' title='Next page'>"+
										"<svg class='' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' width='24px' height='24px' viewBox='0 0 24 24' version='1.1'>"+
											"<polygon points='16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793'></polygon>"+
										"</svg>"+

									"</button>"
								
          
                    $("#paging_li").html(paginghtml);
                    $("#total_pages").text(pageCount)
                    if (pageIndex==0) {
                        $("#no_ofpage").text(1)
                        $("#prevpage").prop('disabled', 'disabled');
                    }
                    else {
                        if (pageIndex == pageCount) {
                            $("#nxtpage").prop('disabled', 'disabled');
                        }
                        else {
                            $("#nxtpage").removeAttr('disabled');
                        }
                        $("#prevpage").removeAttr('disabled');
                        $("#no_ofpage").text(pageIndex)
                    }
            
                 
               
          
                 
                    //if (disp_page > pageCount)
                    //{
                       
                    //    $("#prevpage").removeAttr('disabled');
                    //}
                    //else if  (disp_page==pageCount) {
                    //    $("#nxtpage").prop('disabled', 'disabled');
                    //    $("#prevpage").removeAttr('disabled');
                    //}
                    //else 
                    //{
                    //    $("#prevpage").prop('disabled', 'disabled');
   
                    //    $("#nxtpage").removeAttr('disabled');
                    //}
                
     

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {

                busy = false;
                //pageIndex++;
                //if (type == 1 || type == 2) {
                    $("#facet-loading-wrap").css("display", "none");
                    $("html, body").animate({ scrollTop: 0 }, 800);
                //}
                $("#loader").hide();
                //$("html, body").animate({ scrollTop: 0 }, 800);


            }

        });
     
    }



	///////extra JS


    $('#prevpage, #nxtpage').on('click', function (e) {
    	var cur = $('ul#bullets li.style'),
			next = cur.next('li'),
			prev = cur.prev('li');
    	if (e.target.id == 'rightButton') {
    		if (next.length == 1) {
    			cur.removeClass('style');
    			next.addClass('style');
    		}
    	} else if (e.target.id = 'leftButton') {
    		if (prev.length == 1) {
    			cur.removeClass('style');
    			prev.addClass('style');
    		}
    	}
    });


	/////extra js
});