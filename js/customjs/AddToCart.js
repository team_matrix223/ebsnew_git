﻿
/// <reference path="../vendor/jquery-3.2.1.min.js" />

$(document).ready(function () {
    var attr_id = 0;
    $(document.body).on('click', '.btnAddToCart', function (e) {
        //$(".btnAddToCart").click(function(){
        var dataproduct_id = $(this).attr("data-product-id");
        if ($("#ddl_attri").val() == undefined) {
            attr_id = $(this).attr("attr_id");

        }
        else {
            attr_id = $("#ddl_attri").val();
        }
     
        try {
            if ($("#ddl_attri").val() == "0" && dataproduct_id==0)
            {
                Swal.fire({
                    position: 'center',
                    icon: 'warning',
                    toast: true,
                    title: 'Please Make At Least One Selection !',
                    showConfirmButton: false,

                    timer: 2000
                })
                return false;
            }
            e.stopPropagation();
        } 
        catch (e)  {

        }

        var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        //var qty = $("#q_" + arrData[1]).val();
        var qty = $("#product-quantity").val();
        var st = "p";
        var type = "Product";
 
        ATC(vid, qty, st, type);

    });

    function ATC(vid, qty, st, type) {
     

     
        $.ajax({
            type: "POST",
            data: '{"vid":"' + vid + '","qty":"' + qty + '","st":"' + st + '","type":"' + type + '","attr_id":"' + attr_id + '"}',
            url: "productdetail.aspx/FirstTimeATC",
            async: false,
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);

                if (obj.error == 1) {

                    alert("An Error occured during transaction. Please refresh the page and try again Later.");
                    return;
                }

                BindCartCountClick();
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    toast: true,
                    title: 'Added To Cart!',
                    showConfirmButton: false,
                    
                    timer: 1500
                })
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });

    }
    function BindCartCountClick() {

        $.ajax({
            type: "POST",
            data: '{}',
            url: "mycart.aspx/GetCartHTMLCount",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);
                $("#cartcount").text(obj.TotalItems);

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {

            }

        });

    }
});