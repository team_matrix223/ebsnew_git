﻿
/// <reference path="../vendor/jquery-3.2.1.min.js" />

$(document).ready(function () {





    $(document.body).on('click', '.btndel', function (e) {
        if (!confirm("Do you want to delete?")) {
            return false;
        }
        var vid = $(this).attr('data');

        $.ajax({
            type: "POST",
            url: "wishlists.aspx/delete",
            contentType: "application/json",
            data: '{"req":"delete","Variationid":"' + vid + '"}',
            dataType: "json",
            success: function (data) {

         
            },
            error: function (xhr, ajaxOptions, thrownError) {
                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

                //$.unblockUI();

            }

        });
        $(this).parent().parent().remove();
    });

});