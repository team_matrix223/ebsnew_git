﻿
/// <reference path="../vendor/jquery-3.2.1.min.js" />

$(document).ready(function () {
    var count = 1;
    $("#btn_increase").click(function () {

        count++
        $("#product-quantity").val(count);

    })

    $("#btn_decrease").click(function () {

      
        if (count<=1) {
            return false;
        }
        count--
        $("#product-quantity").val(count);
     

    })


    $("#color_dd").change(function () {

        var variation = $(this).val();

        $(".btnAddToCart").removeAttr("id");
        $(".btnAddToCart").attr("id", "a_" + variation);
        $("#hdnVariationId").val(variation);


        $("#ddl_attri").val(0);
        //$("#" + variation + "").click();
        GetData();
    })

    $("#ddl_attri").change(function () {
        GetData();
    })
    var req = "insert"
    $("#btnwishlist").click(function () {

        //if (req == "insert") {
        //    req="insert"
        //}
        //else if (req == "delete") {
        //    req = "delete"
        //}

        InsertDeleteWishlist();

    })

    function InsertDeleteWishlist() {



        $.ajax({
            type: "POST",
            data: '{"req":"' + req + '","Productid":"' + $("#hdnProductId").val() + '","Variationid":"' + $("#hdnVariationId").val() + '"}',
            url: "productdetail.aspx/AddToWishList",
            contentType: "application/json",
            datatype: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);
                if (msg.d == "1") {
                    req = "insert";
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        toast: true,
                        title: 'Please Login First!',
                        showConfirmButton: false,
                        timer: 2000
                    })

                }
                else {

                    if (req == "insert") {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            toast: true,
                            title: 'Added To Wishlist!',
                            showConfirmButton: false,
                            timer: 2000
                        })
                        req = "delete"
                    }
                    else {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            toast: true,
                            title: 'Removed From Wishlist!',
                            showConfirmButton: false,

                            timer: 2000
                        })
                        req = "insert"
                    }
                   
                }
 

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }
        });
    }
})