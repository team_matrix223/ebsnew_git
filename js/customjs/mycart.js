﻿/// <reference path="vendor/jquery-3.2.1.min.js" />

$(document).ready(function () {

    BindActiveOffer();

    BindActivePoints();
    BindCart();
    var walletpoint = 0;
    function BindCart() {

        $.ajax({
            type: "POST",
            data: '{}',
            url: "mycart.aspx/GetCartHTML",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);
                BindCart_Count();

                if (obj.html == "") {

                    $("#emptycart").show();
                    $("#filledcart").hide();
                }
                else {

                    $("#cartcontainer").html(obj.html);
                    $("#filledcart").show();
                    $("#emptycart").hide();

                    //$("#ltItemList").html(obj.IL);
                    $("#ltCartSubTotal").html("₹" + obj.ST);
                    $("#ltCartDeliveryCharges").html("₹" + obj.DC);
                    $("#ltCartDisAmount").html("₹" + obj.DA);
                    $("#ltPointsRedeemval").html("₹" + obj.PRV);
                    $("#ltCouponVal").html("₹" + obj.CV);
                    $(".baskettotal").html("₹" + obj.NA);
                    var FreeDelivery = obj.FRE;
                    var SST = obj.ST;
                    if (Number(obj.DA) == 0) {
                        $("#trDisAmt").css({ "display": "none" });
                    }
                    else {
                        $("#trDisAmt").css({ "display": "block" });
                    }
                    if (Number(obj.PRV) == 0) {
                        $("#trPointsRedeemval").css({ "display": "none" });
                    }
                    else {
                        $("#trPointsRedeemval").css({ "display": "block" });
                    }
                    if (Number(obj.CV) == 0) {
                        $("#trCouponVal").css({ "display": "none" });
                    }
                    else {
                        $("#trCouponVal").css({ "display": "block" });
                    }
                    if (Number(obj.NA) >= Number(FreeDelivery)) {

                        $("#sp2").html("");

                        $("#ltCartDeliveryCharges").html("0");


                        $(".baskettotal").html("₹" + '' + (Number(obj.NA)));
                    }
                    //$(".baskettotal").html("₹" + obj.NA);
           
                }

              
            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {

            }

        });

    }
    function BindCart_Count() {

        $.ajax({
            type: "POST",
            data: '{}',
            url: "mycart.aspx/GetCartHTMLCount",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);

                $(".cartcount").text(obj.TotalItems);


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {

            }

        });

    }
    $(document).on("click", "div[name='cartadd']", function (event) {



        var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = 1;
        var st = "p";
        var type = "Product";
        //$("#cq_" + vid).css("height", "20px");
        var attr_id = $(this).attr("attr_id");
        var tempQty = $("#cq_" + vid).html();

        var tempQ = Number(tempQty) + 1;

        $("#cq_" + vid).html(tempQ);

        //        $("#cq_" + vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");

        ATC(vid, qty, st, type, attr_id);


    });


    $(document).on("click", "div[name='cartminus']", function (event) {

        var data = $(this).attr("id");
        var arrData = data.split('_');
        var vid = arrData[1];
        var qty = 1;
        var st = "m";
        var type = "Product";
        var attr_id = $(this).attr("attr_id");
        //$("#cq_" + vid).css("height", "20px");

        //        $("#cq_" + vid).html("<img src='images/loader.gif' style='margin-top:5px'   alt='.....'/>");
        var tempQty = $("#cq_" + vid).html();

        var tempQ = Number(tempQty) - 1;
        if (tempQ <= 1) {
            tempQ = 1;
        }
        $("#cq_" + vid).html(tempQ);


        ATC(vid, qty, st, type, attr_id);

    });

    $(document).on("click", "#btnApplyPoints", function (event) {

        ApplyPoints(walletpoint);

    });
    $(document).on("click", "#btnRemoveVoucher1", function (event) {

        RemoveOfferPoint();

    });
    function RemoveOfferPoint() {




        $.ajax({
            type: "POST",
            data: '{}',
            url: "mycart.aspx/RemovePointOffer",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == -1) {
                    alert("No such Offer associated with this Order");
                }
                else if (obj.Status == 1) {

                    $("#dvpoints").html(obj.OfferHtml);

                }

               BindCart();
               //$("#add-discount-code").removeAttr('disabled');
            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {




            }

        });






    }
    function ApplyPoints(points) {

        if ($("#txtwallet").val().trim() == "") {
            $("#txtwallet").val('').focus();
            return;
        }

        if ($("#txtwallet").val() > points) {
            alert("Sorry! You cannot Redeem more than " + points + " Points");
            $("#txtwallet").val('').focus();
            return;
        }

        var wallet = $("#txtwallet").val();

        $.ajax({
            type: "POST",
            data: '{"DisAmt":"' + wallet + '"}',
            url: "mycart.aspx/ApplyOfferForPoints",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                //if (obj.Status == 1) {
                //    $("#dvpoints").html(obj.OfferHtml);

                //}


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {



                BindActiveOffer();
                BindActivePoints();

    

        

            }

        });


    }
    
    $(document.body).on('click', '#btnViewActiveOffers', function (e) {

        ActiveOffer();

    });

    function ActiveOffer() {

        $("#dvOffers").fadeIn(500);
        $("html, body").animate({ scrollTop: 0 }, 500);

    }

    function BindActiveOffer() {

        $.ajax({
            type: "POST",
            data: '{}',
            url: "mycart.aspx/BindActiveOffer",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {




                var obj = jQuery.parseJSON(msg.d);


                $("#PesOffers").html(obj.OfferHtml);
                //$("#<%=lblGiftMsg.ClientID%>").text(obj.Gift);
                $("#txtCouponCode").val(obj.CouponNo);
                //if (obj.CouponNo!='') {
                //    $("#add-discount-code").prop("disabled", "disabled");
                //}
          


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


                 BindCart();;


            }

        });

    }

    function ATC(vid, qty, st, type, attr_id) {

        $.ajax({
            type: "POST",
            data: '{"vid":"' + vid + '","qty":"' + qty + '","st":"' + st + '","type":"' + type + '","attr_id":"' + attr_id + '"}',
            url: "mycart.aspx/ATC",
            contentType: "application/json",
            dataType: "json",
            async: false,
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);

                if (obj.error == 1) {

                    alert("An Error occured during transaction. Please refresh the page and try again Later.");
                    return;
                }
                BindCart();


            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {

                $.uiUnlock();

            }

        });

    }


    $(document.body).on('click', '.responsiveBasket_removeItem', function (e) {

        Swal.fire({
            icon: 'warning',
            title: 'Do You Want To Remove This Item?',
            showDenyButton: true,
            toast: true,
            confirmButtonText: `Yes`,
            denyButtonText: `No`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                var attr_id = $(this).attr("attr_id");
        var productid = $(this).attr('id');
        DeleteFromCart('single', productid, attr_id)
        $(this).parent().parent().remove();
                
            }
        });
 
    });

    function DeleteFromCart(req, productid, attr_id) {


        //if (!confirm("Do you want to delete this Item?")) {
        //    return false;
        //}

        $.ajax({
            type: "POST",
            data: '{"req":"' + req + '","productid":"' + productid + '","attr_id":"' + attr_id + '"}',
            url: "mycart.aspx/DelFromCart",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);
                BindCart();


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {

            }

        });



    }
    $("#add-discount-code").click(function () {


        ApplyVouherNew("", 1);

    })
    function ApplyVouherNew(coupon, status, DisCountType) {

        var CouponNo = "";
        if (status == 1) {

            if ($("#txtCouponCode").val().trim() == "") {
                $("#txtCouponCode").val('').focus();
                return;
            }
            CouponNo = $("#txtCouponCode").val();
        }
        else {
            CouponNo = coupon

        }




        $.ajax({
            type: "POST",
            data: '{"CouponNo":"' + CouponNo + '"}',
            url: "mycart.aspx/GetGifts",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                // alert(obj.GiftHtml);

                if (obj.DiscountType == "Gifts") {
                    $("#dvGifts").html(obj.GiftHtml);
                    $("#dvPopup").dialog({ modal: true, closeOnEscape: false, width: 700 });
                    // $(".ui-dialog-titlebar").hide();

                }
                else {
                    ApplyVouher(coupon, 1, "");
                }



            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {

                BindActivePoints();



            }

        });

    }
    function BindActivePoints() {

        $.ajax({
            type: "POST",
            data: '{}',
            url: "mycart.aspx/BindActivePoints",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
             walletpoint= obj.walletpoints


                $("#dvpoints").html(obj.PointHtml);


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {



            }

        });

    }
    function ApplyVouher(coupon, status, DisCountType) {
        var GiftId = 0;
        var rates = "";
        if (DisCountType == "Gifts") {
            if (status == 1) {
                rates = document.getElementsByName('rbChooseGift');
            }
            else {

                rates = document.getElementsByName(coupon);
            }
            for (var i = 0; i < rates.length; i++) {
                if (rates[i].checked) {
                    GiftId = rates[i].value;
                }
            }

        }


        var CouponNo = "";
        if (status == 1) {

            if ($("#txtCouponCode").val().trim() == "") {
                $("#txtCouponCode").val('').focus();
                return;
            }
            CouponNo = $("#txtCouponCode").val();
        }
        else {
            CouponNo = coupon

        }




        $.ajax({
            type: "POST",
            data: '{"CouponNo":"' + CouponNo + '","GiftId":"' + GiftId + '"}',
            url: "mycart.aspx/ApplyCoupon",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                if (obj.Status == -1) {
                    alert(obj.Message);
                }
                else if (obj.Status == 1) {

                    $("#PesOffers").html(obj.OfferHtml);
                    if (status != 1) {
                        $("#dvOffers").fadeOut(500);

                    }
                }
                if (DisCountType != "Gifts") {
                    // BindCart();;
                    BindCart()
                }
                else {
                   // $("#<%=lblGiftMsg.ClientID%>").text(obj.GiftMessage);
                }

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


               // BindActivePoints();



            }

        });

    }


    function RemoveVoucher(coupon) {



    	$.ajax({
    		type: "POST",
    		data: '{"CouponNo":"' + coupon + '"}',
    		url: "mycart.aspx/RemoveCoupon",
    		contentType: "application/json",
    		dataType: "json",
    		success: function (msg) {

    			var obj = jQuery.parseJSON(msg.d);





    			if (obj.Status == -1) {
    				alert("No such Coupon is associated with this Order");
    			}
    			else if (obj.Status == 1) {
    				$("#PesOffers").empty();
    				//$("#PesOffers").html(obj.OfferHtml);

    			}

    			BindCart();

    		}, error: function (xhr, ajaxOptions, thrownError) {

    			var obj = jQuery.parseJSON(xhr.responseText);
    			alert(obj.Message);
    		},
    		complete: function (msg) {




    		}

    	});






    }

    $(document.body).on('click', '#btnRemoveVoucher', function (e) {
    

    	RemoveVoucher($("#txtCouponCode").val());
    	//$("#add-discount-code").removeAttr("disabled");

    })
});