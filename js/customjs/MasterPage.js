﻿/// <reference path="vendor/jquery-3.2.1.min.js" />


$(document).ready(function () {
    GetBrand();
    //BindProductCategories();
    BindCartCount();
    function BindCartCount() {

        $.ajax({
            type: "POST",
            data: '{}',
            url: "mycart.aspx/GetCartHTMLCount",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);

                $("#cartcount").text(obj.TotalItems);


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {

            }

        });

    }
    function GetBrand() {
        $.ajax({
            type: "POST",
            data: '{}',
            url: "Index.aspx/GetBrand",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                for (var i = 0; i < obj.ListOption.length; i++) {
                    $("#Brandtitle1").append('<li class="responsiveFlyoutMenu_levelTwoItem" data-subnav-level="subnav-level-two data-subnav-target="subnav-este-lauder"><a class="responsiveFlyoutMenu_levelTwoLink responsiveFlyoutMenu_levelTwo_brands-este-lauder responsiveFlyoutMenu_levelTwoLink_num responsiveFlyoutMenu_levelTwoLink_num-01" data-subnav-template="subnav-" href=list.aspx?b=' + obj.ListOption[i].BrandId + ' tabindex="-1" data-context="Estée Lauder" data-js-nav-level="2"><span class="responsiveFlyoutMenu_levelTwoLinkText">' + obj.ListOption[i].Title + '</span></a></li>');
                }



            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });


    }
    $('#txtSearch').keypress(function (e) {
        var key = e.which;
        if (key == 13)  // the enter key code
        {
            search_();
        }
    });

    $("#mob-search-btn").click(function () {
        search_();

    })

    function search_() {
        if ($('#txtSearch').val() != "")
            {
            window.location = 'list.aspx?search=' + $("#txtSearch").val() + ''
        }

    }
    function BindProductCategories() {
        $.ajax({
            type: "POST",
            data: '{}',
            url: "Index.aspx/BindProductCategories",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                for (var i = 0; i < obj.ListOption.length; i++) {
                    var subcat = BindProductSubCategories(obj.ListOption[i].CategoryId);
                    //$("#AllCategory").append('<li class="responsiveFlyoutMenu_levelTwoItem" data-subnav-level="subnav-level-two data-subnav-target="subnav-este-lauder"><a class="responsiveFlyoutMenu_levelTwoLink responsiveFlyoutMenu_levelTwo_brands-este-lauder responsiveFlyoutMenu_levelTwoLink_num responsiveFlyoutMenu_levelTwoLink_num-01" data-subnav-template="subnav-" href=list.aspx?c=' + obj.ListOption[i].CategoryId + ' tabindex="-1" data-context="Estée Lauder" data-js-nav-level="2"><span class="responsiveFlyoutMenu_levelTwoLinkText">' + obj.ListOption[i].Title + '</span></a>' + subcat + '</li>');
                }



            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {

                //html = "";
            }

        });


    }
    var html = "";

    function BindProductSubCategories(CategoryId) {
       
        $.ajax({
            type: "POST",
            data: '{"CategoryId":"' + CategoryId + '"}',
            url: "Index.aspx/BindProductSubCategories",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                for (var i = 0; i < obj.ListOption.length; i++) {
                    html = html + '<li> ' + obj.ListOption[i].Title + '</li>';
                }



            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {

                return html;
            }

        });

       
    }


});