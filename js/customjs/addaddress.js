﻿$(document).ready(function(){
    BindCities()
    function BindCities() {
        $.ajax({
            type: "POST",
            data: '',
            url: "/user/userdeliveryaddress.aspx/BindCity",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                $("#ddlCities").html(obj.CitiessOptions);

            },

            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                // $("#ddlCities option").removeAttr("selected");
                // $('#ddlCities option[value=' + obj.Customer.AccSubGroupId + ']').prop('selected', 'selected');


            }


        });
    }
    $("#submit").click(function () {
       
        Insert();
    })
    $("#txtGuestEmail").blur(function () {

        //if ($(this).val()!="") {

        //}
        $.ajax({
            type: "POST",
            data: '{"Email":"' + $(this).val() + '"}',
            url: "addaddress.aspx/checkmail",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                if (obj.CusType=="Customer")
                {

                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        toast: true,
                        title: 'Email Already Registerd!',
                        showConfirmButton: false,

                        timer: 3000
                    })
                    $("#txtGuestEmail").focus();
                    return;
                }
     
        
                $("#txtFirstName").val(obj.Firstname);
                $("#txtLastName").val(obj.Lastname);
                $("#txtAddress").text(obj.Address);
                $("#ddlCities").val(obj.City);
                $("#txtArea").val(obj.Area);
                $("#txtPinCode").val(obj.Pincode);
                $("#txtMobileNo").val(obj.Mobileno);



          

             

            },

            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                // $("#ddlCities option").removeAttr("selected");
                // $('#ddlCities option[value=' + obj.Customer.AccSubGroupId + ']').prop('selected', 'selected');


            }


        });


    })
function Insert() {
    //if (!validateForm("frmAddress")) {
    //    return;
    //}
   
    var m_AddressId = 0;
    var FName = $("#txtFirstName").val();
    if ($.trim(FName) == "") {
        $("#txtFirstName").focus();
        alert("Enter FirstName");
        return;
            
    }
  
   

    var LName = $("#txtLastName").val();
    if ($.trim(LName) == "") {
        $("#txtLastName").focus();
        alert("Enter Last Name");
        return;

    }
    var MobileNo = $("#txtMobileNo").val();
    if ($.trim(MobileNo) == "") {
        $("#txtMobileNo").focus();
        alert("Enter MobileNo");
        return;

    }

    var Telephone = 0;
    var street ="";
    var area = $("#txtArea").val();
    var pincode = $("#txtPinCode").val();
    var address = $("#txtAddress").val(); 

    if ($.trim(address) == "") {
        $("#txtAddress").focus();
        alert("Enter Address");
        return;

    }
    var Isprimary = false;
    var Email = $("#txtGuestEmail").val();
    if ($.trim(Email) == "") {
        $("#txtGuestEmail").focus();
        alert("Enter EmailId");
        return;

    }
   
    var city = $("#ddlCities").val();
   
    if ($.trim(city) == "") {
        $("#ddlCities").focus();
        alert("Choose city");
        return;
    }
    var CusType = "Guest";
   
    //var sector = $("#ddlSector").val();

    //if ($.trim(sector) == "") {
    //    $("#ddlSector").focus();
    //    alert("Choose Sector");
    //    return;
    //}
    //$.uiLock('');
    // btnAdd.html("<img src='img/loader.gif' alt='loading...'/>")

    
    $.ajax({
        type: "POST",
        data: '{"DeliveryAddressId":"' + m_AddressId + '","RecipientFirstName":"' + $.trim(FName) + '","RecipientLastName":"' + $.trim(LName) + '","MobileNo":"' + $.trim(MobileNo) + '","Telephone":"' + $.trim(Telephone) + '","CityId":"' + $.trim(city) + '","Area":"' + $.trim(area) + '","Street":"' + $.trim(street) + '","Address":"' + address + '","PinCode":"' + pincode + '","IsPrimary":"' + Isprimary + '","PinCodeId":"' + 0 + '","Email":"' + Email + '","CusType":"' + CusType + '"}',
        url: "/user/userdeliveryaddress.aspx/Insert",
        contentType: "application/json",
        datatype: "json",
        success: function (msg) {
            var obj = jQuery.parseJSON(msg.d);
            if (obj.Status == 0) {
                alert("Insertion failed,Plz try again");
                return;
            }

            insert_into_mstusercart(obj.Status);
            //$.uiUnlock('');

            //if (m_AddressId == "0") {
            
            //    alert("Information is saved successfully");

            //    ResetControls();
            //    return;
            //}
            //else {
              
            //    alert("Information is updated successfully");
            //    ResetControls();


            //    return;
            //}
        },
        error: function (xhr, ajaxOptions, thrownError) {

            var obj = jQuery.parseJSON(xhr.responseText);
            alert(obj.Message);
        },
        complete: function () {
            $.uiUnlock();

        }
    });
}
function insert_into_mstusercart(addressid) {
    $.ajax({
        type: "POST",
        data: '{"DeliveryAddressId":"' + addressid + '"}',
        url: "addaddress.aspx/insert_into_mstusercart",
        contentType: "application/json",
        datatype: "json",
        success: function (msg) {
            //var obj = jQuery.parseJSON(msg.d);
            //if (obj.Status == 0) {
            //    alert("Insertion failed,Plz try again");
            //    return;
            //}

            window.location = "paymentmode.aspx";

        },
        error: function (xhr, ajaxOptions, thrownError) {

            var obj = jQuery.parseJSON(xhr.responseText);
            alert(obj.Message);
        },
        complete: function () {
            $.uiUnlock();

        }
    });
}
})