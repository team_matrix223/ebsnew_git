﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class accountcreate : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{

	}

    protected void btnsubmit_Click(object sender, EventArgs e)
    {

        Users objUser = new Users()
        {
            UserId = 0,
            FirstName = firstName.Value.Trim(),
            LastName = lastName.Value.Trim(),
            EmailId = customerEmail.Value.Trim(),
            Password = customerPassword.Value.Trim(),
            IsActive = true,
            MobileNo = mobileNumber.Value.Trim(),
            AdminId = 0,
            RecipientFirstName ="",
            RecipientLastName ="",
            RMobileNo = "",
            Telephone ="",
            CityId = 0,
            Area = "",
            Street = "",
            Address = "",
            PinCode = "",
            IsPrimary = true,
            PinCodeId =0,
            CusType = "Customer",
        };
        int result = new UsersBLL().InsertUpdate(objUser);
        if (result == 0)
        {
            dv_alert.Visible = true;
            lblMsg.Text = "** User Registraion is not completed,Plz try again";
            //Response.Write("<script>alert('User Registraion is not completed,Plz try again')</script>");
            return;
        }
        else if (result == -1)
        {
            dv_alert.Visible = true;
            lblMsg.Text = "**  EmailId Already Exists";
            //Response.Write("<script>alert('Current EmailId is already In Use')</script>");
            customerEmail.Focus();
            return;
        }
        else if (result == -2)
        {
            dv_alert.Visible = true;
            lblMsg.Text = "**MobileNo Already Exists";
            //Response.Write("<script>alert('Current MobileNo is already In Use')</script>");
            mobileNumber.Focus();
            return;
        }
        else
        {
            Session[Constants.UserId] = result;
            Session[Constants.Email] = customerEmail.Value.Trim();
            Session[Constants.CustType] = "Customer";
            Response.Redirect("index.aspx");
            //if (ReturnUrl != "")
            //{
            //    Response.Redirect("../delivery.aspx");
            //}
            //else
            //{
            //    Response.Redirect("index.aspx");
            //}
            // ResetControls();
        }
    }
}