﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="productdetail.aspx.cs" Inherits="productdetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <!--<script src="js/jquery-1.9.1.min.js"></script>-->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
  	<link href="src/jquery.exzoom.css" rel="stylesheet" />
	<script src="src/jquery.exzoom.js"></script>

    <script src="js/customjs/productdetail.js"></script>
  <%--  <script src="js/customjs/AddToCart.js"></script>--%>
	<link href="customcss/product-page.css" rel="stylesheet" />

<%-- newsliderfile --%>
	 <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css" rel="stylesheet" type="text/css">
	<link href="customcss/jquery.jqZoom.css" rel="stylesheet" />

<%-- newsliderfile --%>
     <script>

         function GetData() {
  
             $("#dsp_image").empty();
       
             $("#image").empty();
             var attr_id = 0;
             try {
                 attr_id = $("#ddl_attri").val() == null ? 0 : $("#ddl_attri").val();
             } catch (e) {
                 attr_id = 0;
             }
        
            $.ajax({
                type: "POST",
                data: '{"ProductId":"' + $("#hdnProductId").val() + '","VariationId":"' + $("#hdnVariationId").val() + '","attr_id":"' + attr_id + '"}',
                url: "productdetail.aspx/GetData",
                contentType: "application/json",
                datatype: "json",
                success: function (msg) {
                    var obj = jQuery.parseJSON(msg.d);
                    $("#name").text(obj.Name);
                    $("#shortdesc").text(obj.ShortDesc);
                    $("#longdesc").text(obj.LongDesc);
                    $("#price").text(Number(obj.Price)); 

                    $(".btnAddToCart").attr('attr_id', obj.attr_id_db)
                    if (obj.total_left_stock < 1) {
                        $(".btnAddToCart").prop('disabled', true);
                        $(".btnAddToCart").text('OUT OF STOCK');
                    }
                  else {
                        $(".btnAddToCart").prop('disabled', false);
                        $(".btnAddToCart").text('ADD TO CART');
                    }
                    $("#image").append(obj.relImgUrl );
                    $("#dsp_image").append('<img src=images/product/' + obj.PhotoUrl + ' id="div-img">');
                  
                    if (obj.Attributes != "") {
                        
                        $("#dv_ddl_attri").show();
                        $("#ddl_attri").empty();
                        $("#ddl_attri").append(obj.Attributes);
                         if (attr_id>0)
                        {
                            $("#ddl_attri").val(attr_id);
                        }
                  
                    }
                    else {
                        $("#dv_ddl_attri").hide();
                    }
                  

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {


                }
            });
        }
     </script>
       <asp:HiddenField ID="hdnProductId" ClientIDMode="Static" Value="0" runat="server" />
    <asp:HiddenField ID="hdnVariationId" ClientIDMode="Static" Value="0"  runat="server" />
<div class="page-container">

			<div class="primary-wrap column-row">

				<div class="top-rail usp-enabled">
<div id="service-banner" class="service-banner">
	</div>
	<div class="uspBanner">
	
		<!-- widget internationalStripBanner start -->
		
<div class="stripBanner">
	<a class="stripBanner_text">
		Offers valid this week only
	</a>
</div>
		<!-- widget internationalStripBanner stop -->
	
						</div>
				
						<div class="constraint no-padding">
						
<%--<div class="breadcrumbs">

				<ul class="breadcrumbs_container">
					<li class="breadcrumbs_item">
						<a class="breadcrumbs_link" href="https://www.ebs1952.com/">Home</a>
					</li>
					<li class="breadcrumbs_item breadcrumbs_item-active">
							Rimmel Radiance Shimmer Brick 12g - 02
					</li>
					
				</ul>
			
</div>--%>
						</div>
					</div>
		
					<div class="middle-rail column-span24">

			
				<main id="mainContent" class="newYorkProductPage" data-component="newYorkProductPage">
<div class="addedToBasketModal" data-component="addedToBasketModal" data-cdn-url="https://s2.thcdn.com//" data-secure-url="https://www.ebs1952.com/" role="dialog" tabindex="-1">

<span data-elysium-property-name="maxQuantityReached" data-elysium-property-value="Item cannot be added to your basket. This product is limited to a quantity of %d per order."></span>
<span data-elysium-property-name="partialMaxQuantityReached" data-elysium-property-value="%1 items cannot be added to your basket. This product is limited to a quantity of %2 per order."></span>

<div class="addedToBasketModal_container" role="dialog" tabindex="-1" aria-labelledby="added-to-basket-modal-title0">
<div class="addedToBasketModal_titleContainer">
<h2 id="added-to-basket-modal-title0" class="addedToBasketModal_title">
Added to your basket
</h2>

<button type="button" class="addedToBasketModal_closeContainer" aria-label="Close" title="Close" data-close="">
<svg class="addedToBasketModal_close" viewBox="-3 -4 20 20" xmlns="http://www.w3.org/2000/svg">
<path d="M8.414 7l5.293-5.293c.391-.391.391-1.023 0-1.414s-1.023-.391-1.414 0l-5.293 5.293-5.293-5.293c-.391-.391-1.023-.391-1.414 0s-.391 1.023 0 1.414l5.293 5.293-5.293 5.293c-.391.391-.391 1.023 0 1.414.195.195.451.293.707.293.256 0 .512-.098.707-.293l5.293-5.293 5.293 5.293c.195.195.451.293.707.293.256 0 .512-.098.707-.293.391-.391.391-1.023 0-1.414l-5.293-5.293"></path>
</svg>

</button>
</div>

<div class="addedToBasketModal_error " data-error="">
Sorry, there seems to have been an error. Please try again.
</div>

<div class="addedToBasketModal_error " data-quantity-limit-reached="">
</div>

<div class="addedToBasketModal_warning" data-quantity-limit-partially-reached="">
</div>

<div class="addedToBasketModal_item">
<div class="addedToBasketModal_imageContainer">
<a href="" data-product-url="">
<img src="//:0" alt="Loading..." class="addedToBasketModal_image" data-product-image="">
</a>
</div>
<div class="addedToBasketModal_itemDetails">
<a href="" data-product-url="" class="addedToBasketModal_itemName" data-product-title="">Product Name</a>
<p class="addedToBasketModal_itemQuantity">
Quantity
<span class="addedToBasketModal_itemQuantity addedToBasketModal_itemQuantity-number" data-product-quantity=""></span>
</p>

 
<p class="addedToBasketModal_itemPrice" data-product-price=""></p>
</div>
</div>

<div class="addedToBasketModal_subtotal">
<p class="addedToBasket_subtotalTitle">
Subtotal:
<span class="addedToBasket_subtotalItemCount">
(<span class="addedToBasket_subtotalItemCount-number" data-basket-total-items=""></span>
items in your basket)
</span>
</p>

<p class="addedToBasket_subtotalAmount" data-basket-total=""></p>
</div>

<div class="addedToBasketModal_loyaltyPointsMessage">
</div>

<div class="addedToBasketModal_ctas">

<div class="addedToBasketModal_ctaContainerLeft">
<button type="button" class="addedToBasket_continueShoppingButton" data-continue-shopping="">
Continue Shopping
</button>
</div>
<div class="addedToBasketModal_ctaContainerRight">
<a href="/my.basket" class="addedToBasketModal_viewBasketButton js-e2e-quickView-basket" data-view-basket="">
View Basket
</a>
</div>
</div>

<div class="addedToBasketModal_productRecommendations" data-recommendations="">
<span class="addedToBasketModal_loading">
<span class="addedToBasketModal_loadingSpinny"></span>
</span>
</div>
</div>
</div>



<div class="newYorkProductPage_productReviewMessage">
<div class="productReviewsReviewMessage" data-component="productReviewsReviewMessage" aria-live="assertive">
</div>

</div>

<div class="newYorkProductPage_topRow">
<div class="newYorkProductPage_firstColumn">


<%-- new slider --%>
	<div class="prodcut-sldier-section">
		 <ul class="pro-img-ul" id="image">
             <asp:Literal ID="ltr_imglist" runat="server" Visible="false"></asp:Literal>
<%--			 <li> <img class="img" src="images/product/080Feb021051051230PM9031img.png" /></li>
			<li><img class="img" src="images/product/080Feb021051211290PM993apple.png" /> </li>
			 <li> <img class="img" src="images/slider/home120Feb0210121121010PM429add.jpg" /></li>
			 <li><img class="img"src="https://www.ebs1952.com/pub/media/wysiwyg/banner/1344X546freebies.jpg" /></li>--%>
			
		</ul>
		
	 <div class="pro-img-div " id="dsp_image">
			<%--	  <img src="" id="div-img" />--%>
			 </div>
		</div>
	
	<script>
	    $(document.body).on('click', '.rel_img', function (e) {
	        $("#div-img").removeAttr('src');
	        $("#div-img").attr('src');
	        $('#div-img').attr('src', $(this).attr('src'));
	        //$("#hdnVariationId").val($(this).attr('id'));
	        //try {
	        //    $("#color_dd").val($(this).attr('id'))
	        //} catch (e) {

	        //}
	  
	        //GetData();
	    });
	    //$(document.body).on('change', '#color_dd', function (e) {
	
	    //    //$("#div-img").removeAttr('src');
	    //    //$("#div-img").attr('src');
	    //    //$('#div-img').attr('src', $(this).attr('src'));
	    //    $("#hdnVariationId").val($(this).val());
	    //    //try {
	    //    //    $("#color_dd").val($(this).attr('id'))
	    //    //} catch (e) {

	    //    //}

	    //    GetData();
	    //});

</script>
	




	
<%--    <div class="container">
        <h1>Magnify Image On Hover Example</h1>
        <div class="example">
        <div class="zoom-box">
            <img class="new-img" src="https://source.unsplash.com/6TIpY5KqCYo/600x450" width="200" height="150" />
        </div>
    </div>
    </div>--%>


<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
<script src="js/jquery.jqZoom.js"></script>
<script>
    $(function(){
        $(".new-img").jqZoom({
            selectorWidth: 30,
            selectorHeight: 30,
            viewerWidth: 400,
            viewerHeight: 300
        });

    })
</script>



<%-- new slider --%>


<div class="newYorkProductPage_breakpoint-lg_productDescription">
<div class="productDescription desktop-desc" data-component="productDescription" data-string-template-path="components/productDescriptionList/productDescriptionList">
<div class="productDescription_contentPropertyList">
<div class="productDescription_contentPropertyListItem" data-item="">
<button class="productDescription_contentPropertyListItem_Control" id="product-description-heading-lg-1" type="button" data-item-control="">
<div class="productDescription_contentPropertyHeading">
Description
</div>
<span class="productDescription_icon " data-icon="expand">
<svg class="productDescription_expandChevron" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
<polygon points="16.137 9 17.5 10.39 12 16 6.5 10.39 7.863 9 12 13.22"></polygon>
</svg>

</span>
<span class="productDescription_icon  productDescription_icon-hide" data-icon="collapse">
<svg class="productDescription_collapseChevron" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
<polygon points="16.137 15 12 10.78 7.863 15 6.5 13.61 12 8 17.5 13.61"></polygon>
</svg>

</span>
</button>
<div class="productDescription_contentProperties productDescription_contentProperties_list productDescription_contentProperties-hide openAccordions" data-item-content="">
<div class="productDescription_synopsisContent">
<p id="longdesc">
    


</div>

</div>
</div>


<%--<div class="productDescription_contentPropertyListItem" data-item="">
<button class="productDescription_contentPropertyListItem_Control" id="product-description-heading-lg-2" type="button" data-item-control="">
<div class="productDescription_contentPropertyHeading">
Product Details
</div>
<span class="productDescription_icon " data-icon="expand">
<svg class="productDescription_expandChevron" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
<polygon points="16.137 9 17.5 10.39 12 16 6.5 10.39 7.863 9 12 13.22"></polygon>
</svg>

</span>
<span class="productDescription_icon  productDescription_icon-hide" data-icon="collapse">
<svg class="productDescription_collapseChevron" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
<polygon points="16.137 15 12 10.78 7.863 15 6.5 13.61 12 8 17.5 13.61"></polygon>
</svg>

</span>
</button>
<div class="productDescription_contentProperties productDescription_contentProperties_list productDescription_contentProperties-hide openAccordions" data-item-content="">
<div class="productDescription_contentWrapper">
<div class="productDescription_contentPropertyName">
<span class="productDescription_contentPropertyLabelStyle">Brand:</span>
</div>
<div class="productDescription_contentPropertyValue" data-information-component="brand">
<div>Rimmel</div>

</div>
</div>

<div class="productDescription_contentWrapper">
<div class="productDescription_contentPropertyName">
<span class="productDescription_contentPropertyLabelStyle">Directions:</span>
</div>
<div class="productDescription_contentPropertyValue" data-information-component="directions">
<div><p>Apply all over face, for a sun-kissed look or use single shades to contour &amp; highlight face features.</p>
</div>

</div>
</div>

<div class="productDescription_contentWrapper">
<div class="productDescription_contentPropertyName">
<span class="productDescription_contentPropertyLabelStyle">Ingredients:</span>
</div>
<div class="productDescription_contentPropertyValue" data-information-component="ingredients">
<div><p>Mica, Talc, Dimethicone, Caprylic/Capric Triglyceride, Magnesium Stearate, Synthetic Fluorphlogopite, Phenoxyethanol, Cocos Nucifera (Coconut) Oil, Caprylyl Glycol, Aloe Barbadensis Leaf Extract, [May Contain/Peut Contenir/+/-:Iron Oxides (Ci 77491, Ci 77492, Ci 77499), Titanium Dioxide (Ci 77891)].</p>
</div>

</div>
</div>

<div class="productDescription_contentWrapper">
<div class="productDescription_contentPropertyName">
<span class="productDescription_contentPropertyLabelStyle">Volume:</span>
</div>
<div class="productDescription_contentPropertyValue" data-information-component="volume">
<div>12g</div>
</div>
</div>
</div>
</div>--%>


</div>
</div>

</div>
</div>
<div class="newYorkProductPage_lastColumn">
<div class="newYorkProductPage_productDetailsContainer">
<div class="newYorkProductPage_productBrandLogo">
<div class="productBrandLogo">
<img src="https://s2.thcdn.com//design-assets/images/logos/shared-brands/colour/rimmel.gif" class="productBrandLogo_image" alt="Rimmel" title="Rimmel">
</div>

</div>
<div class="newYorkProductPage_productName">
<div class="productName" data-component="productName">
<h1 class="productName_title" data-product-name="title" id="name">
</h1>
</div>

</div>

<%--<div class="newYorkProductPage_freeDelivery">
FREE UK DELIVERY OVER £25
</div>--%>

<div class="newYorkProductPage_productRatingStars">
<!-- The empty div is needed when there are no reviews stars so that the js can replace it if a
different variation is clicked -->
<div data-component="productReviewStars">
<a href="#" class="productReviewStars" data-component-tracked-clicked="" data-context="review_star_indicator">
<svg class="productReviewStarsPresentational" role="img" aria-label="5.0 Stars" xmlns="http://www.w3.org/2000/svg" width="152" height="45" viewBox="0 0 152 45">
<linearGradient id="newYorkProductPage_productRatingStars" x1="0" x2="100%" y1="0" y2="0">
<stop class="productReviewStarsPresentational_score" offset="100.0%"></stop>
<stop class="productReviewStarsPresentational_base" offset="0"></stop>
</linearGradient>
<path fill="url(#newYorkProductPage_productRatingStars)" d="M14 10l-3.521 8.802-9.479.643 7.279 6.094-2.302 9.174 8.023-5.044 8.023 5.044-2.302-9.174 7.279-6.094-9.479-.643-3.521-8.802zm31 0l-3.521 8.802-9.479.643 7.279 6.094-2.302 9.174 8.023-5.044 8.023 5.044-2.302-9.174 7.279-6.094-9.479-.643-3.521-8.802zm31 0l-3.521 8.802-9.479.643 7.279 6.094-2.302 9.174 8.023-5.044 8.023 5.044-2.302-9.174 7.279-6.094-9.479-.643-3.521-8.802zm31 0l-3.521 8.802-9.479.643 7.279 6.094-2.302 9.174 8.023-5.044 8.023 5.044-2.302-9.174 7.279-6.094-9.479-.643-3.521-8.802zm31 0l-3.521 8.802-9.479.643 7.279 6.094-2.302 9.174 8.023-5.044 8.023 5.044-2.302-9.174 7.279-6.094-9.479-.643-3.521-8.802z"></path>
</svg>

<span class="productReviewStars_numberOfReviews">
5 Reviews
</span>
<span class="visually-hidden">, See all reviews</span>
</a>
</div>

</div>

<div class="newYorkProductPage_loyaltyProductPointsMessage">
</div>


<div class="newYorkProductPage_productPrice">
<div class="productPrice" data-component="productPrice" data-subscribe-and-save="" data-product-id="11496164">
<span class="productPrice_schema">GBP</span>
<span class="productPrice_schema" data-schema="price">6.99</span>
<div class="productPrice_priceWithBadge">
   <span class="productPrice_price">&#8377</span>
<span class="productPrice_priceInfo">
<p class="productPrice_price" data-product-price="price" id="price">

<span data-product-price-subscription-type="subscription" class="productPrice_monthText"></span>
</p>

</span>

</div>
      <div id="dv_color_dd"  runat="server" visible="false">
        <select id="color_dd" >
            <asp:Literal ID="ltr_color_dd" runat="server"></asp:Literal>
        </select>
    </div>
    <div id="dv_ddl_attri" style="display:none">
        <select id="ddl_attri" >

        </select>
    </div>

</div>             

</div>

<div class="newYorkProductPage_subscriptionComponentInfoMessage">
<div class="subscriptionComponentInfoMessage" data-component="subscriptionComponentInfoMessage">
<div class="recurringPaymentText" data-product-price="recurringPayment">
<p class="hideRecurringPaymentText" data-product-price="recurringPaymentText">
Payment will be taken every
<span data-product-price="paymentOptionTerm"></span>
<span class="hideMonth" data-product-price="optionMonthTextSingular">month.</span>
<span class="hideMonth" data-product-price="optionMonthTextPlural">months.</span>
</p>
</div>
</div>

</div>

<div class="newYorkProductPage_productVariations">
<div class="productVariations" data-master-id="11496164" data-component="productVariations" data-disable-wishlist="false">
<div data-variation-container="productVariations" data-child-id="11496164" data-linked-child-product-id="11496164" data-linked-product-id="0" data-information-url="rimmel-radiance-shimmer-brick-12g-02/11496164.html" data-has-selection="false" data-information-current-quantity-basket="0" data-information-maximum-allowed-quantity="0" data-enable-child-sku-description="" data-subscribe-and-save="">
</div>
</div>

</div>

<div class="newYorkProductPage_productQuantityInput" data-quantity-input-container="">
<label class="newYorkProductPage_quantityText" for="product-quantity">
Quantity
</label>

<div class="newYorkProductPage_quantityInput">
<div class="productQuantityInput_container" data-component="productQuantityInput">
<div class="productQuantityInput" data-product-quantity-input-block="">
<button type="button" class="productQuantityInput_decrease"   id="btn_decrease"
     data-quantity-button="decrease" aria-label="Decrease quantity" title="Decrease quantity">
<svg xmlns="http://www.w3.org/2000/svg" class="productQuantityInput_icon productQuantityInput_icon-decrease" width="24" height="24" viewBox="0 0 24 24">
<rect width="12" height="2" x="6" y="11"></rect>
</svg>

</button>

<input type="number" data-quantity-input="" class="productQuantityInput_input" value="1" min="1" max="99" aria-label="Quantity" title="Quantity" id="product-quantity">

<button type="button" class="productQuantityInput_increase" id="btn_increase" data-quantity-button="increase" aria-label="Increase quantity" title="Increase quantity">
<svg xmlns="http://www.w3.org/2000/svg" class="productQuantityInput_icon productQuantityInput_icon-increase" width="24" height="24" viewBox="0 0 24 24">
<polygon points="11 11 11 6 13 6 13 11 18 11 18 13 13 13 13 18 11 18 11 13 6 13 6 11"></polygon>
</svg>

</button>
</div>
</div>

<div id="product-quantity-message-block" class="productQuantityInput_messageBlock" data-message-block="">
<p class="productQuantityInput_messageBlockMessage productQuantityInput_messageBlockBasketLimit hideClass" data-message="basketLimit" data-quantity-message="maximumValueParent">Item limited to max quantity of
<span data-quantity-message="maximumValue">5000</span>
</p>

<p class="productQuantityInput_message productQuantityInput_messageBlockQuantities" data-message-block-quantities="">
<span class="productQuantityInput_messageBlockQuantitiesWrapper">
(
<span data-quantity-message="currentQuantityInBasket" class="productQuantityInput_currentQuantityInBasket">0</span>
<span class="productQuantityInput_messageBlockMessage productQuantityInput_messageBlockBasketSingleItem" data-message="basketSingleItem" data-quantity-message="currentQuantityInBasketParent"> item is in your basket</span>
<span class="productQuantityInput_messageBlockMessage productQuantityInput_messageBlockBasketMultipleItem" data-message="basketMultipleItems" data-quantity-message="currentQuantityInBasketParent"> items are in your basket</span>
)
</span>
</p>
</div>

</div>
</div>

<div class="newYorkProductPage_actions">
<div class="newYorkProductPage_productAddToBasket">
<span data-product-id="11496164" data-site-id="95" data-default-locale="en_GB" data-from-wishlist="false" data-subsite-code="en" data-component="productAddToBasketButton" data-context="" data-linked-product="false" data-has-quantity-input="true" data-subscription-contract-id="">
<span data-product-add-to-basket-button="">
<button class="productAddToBasket productAddToBasket-buyNow js-e2e-add-basket btnAddToCart" type="button" data-component="productAddToBasket" aria-label="Add to basket" id="a_<%=hdnVariationId.Value%>" data-product-id="0">
ADD TO CART
</button>
</span>

</span>


</div>
</div>

<div class="newYorkProductPage_productAddToWishlist">
<div class="productAddToWishlist" data-component="productAddToWishlist" data-customer-state="NOT_RECOGNISED" data-product-id="11496164" data-unavailable="false" data-show-modal="false" data-wishlist-page="false" data-translation-added="Saved to Wishlist" data-translation-removed="Product removed from wish list">

<div class="productAddToWishlist_button_wrapper">

<button class="productAddToWishlist_button_default" type="button" id="btnwishlist" data-selected="false" data-list-type="wishlist" data-context="addToWishlistButton" data-component-tracked-clicked="" aria-expanded="false" aria-label="Save to Wishlist" title="Save to Wishlist">

<span class="productAddToWishlist_basketButtonIcon">
<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" class="heart heartBasket">
<path stroke-width="2" d="M4.9,13.6c1.9,3,6.4,6.5,7.8,6.5c1.8,0,6.9-4.3,8.4-7.6
c1.9-4.3,0.3-8.2-3.2-8.8c-1.5-0.3-3.1,0.4-4.6,2.1l-0.6,0.7l-0.6-0.6c-1.5-1.7-3-2.4-4.6-2.1C4.9,4.1,3.3,6,3.4,8.9"></path>
<path stroke-width="2" d="M7.7,8.4l2.6,2.6l-2.6,2.5 M10.2,11H0.9"></path>
</svg>

</span>

<span class="productAddToWishlist_buttonIcon">
<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="heart heartUnfilled ">
<path stroke-width="2" d="M17.1771946,3.80075947 C15.634169,3.54841538 14.1191524,4.24328804 12.6032914,5.94067608 L12.0475025,6.56302173 L11.4877208,5.94426494 C9.98328314,4.28132899 8.47152577,3.58325658 6.9268411,3.79978408 C3.07950773,4.33908733 1.62539543,8.10071206 3.67140764,12.6178359 C5.08061189,15.7290344 10.3866231,20.25 12.0488058,20.25 C13.8690289,20.25 18.921013,15.9778795 20.4266084,12.6169382 C22.3452463,8.33396191 20.7713196,4.38853728 17.1771946,3.80075947 Z"></path>
</svg>

</span>

<span class="productAddToWishlist_basketPageButtonText">
Move to Wishlist
</span>

<span class="productAddToWishlist_text">
Save to Wishlist
</span>

</button>
</div>

<div class="productAddToWishlist_popup" data-component-tracked-hovered="" data-context="loginPopup">
<div class="productAddToWishlist_popup_text">
<a class="productAddToWishlist_login_button" data-component-tracked-clicked="" data-context="loginLink" href="">
Log in/sign up
</a>
&nbsp;
<span class="productAddToWishlist_login_text">to use Wishlists!</span>
</div>
<div class="productAddToWishlist_popup_close_button">
<button class="productAddToWishlist_popup_close" data-component-tracked-clicked="" data-context="loginPopupClose" aria-label="Close Save to Wishlist" title="Close Save to Wishlist">
<svg class="productAddToWishlist_cross" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g fill="none" fill-rule="evenodd">
<use fill="#E6E6E6" fill-rule="evenodd"></use>
<path d="M7.032 16.968L17 7M7 7l10 10" stroke="#333" stroke-width="2"></path>
</g>
</svg>

</button>
</div>
</div>

</div>

<button class="productAddToWishlist_deleteButton" data-component-tracked-clicked="" data-context="wishlistDelete" type="button" aria-label="Remove product from Wishlist" title="Remove product from Wishlist">
<span class="productAddToWishlist_deleteIcon"><svg class="productAddToWishlist_cross" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g fill="none" fill-rule="evenodd">
<use fill="#E6E6E6" fill-rule="evenodd"></use>
<path d="M7.032 16.968L17 7M7 7l10 10" stroke="#333" stroke-width="2"></path>
</g>
</svg>
</span>
</button>

</div>

<div class="newYorkProductPage_productStockInformation">
<div class="productStockInformation responsiveBasket_stockInfo  productStockInformation_instock " data-component="productStockInformation">
<div data-product-stock-information="">
<%--<p class="productStockInformation_prefix">In stock</p>--%>
<span class="productStockInformation_separator">-</span>
<p class="productStockInformation_suffix">Usually dispatched within 24 hours</p>

</div>
</div>

</div>

<div class="newYorkProductPage_papBanner">
<div data-component="pap">
</div>

</div>

<div class="newYorkProductPage_liveChat">
<div class="lp-panel" id="LP_DIV_1461163481737"></div>
<div class="liveChat" data-component="liveChat">
<span data-elysium-property-name="stateOnlineText" data-elysium-property-value="Our beauty experts are"></span>
<span data-elysium-property-name="stateOfflineText" data-elysium-property-value="Our operators are"></span>
<span data-elysium-property-name="offlineText" data-elysium-property-value="<span class=&quot;liveChat_statusText-offline&quot;>Offline</span>"></span>
<span data-elysium-property-name="onlineText" data-elysium-property-value="<span class=&quot;liveChat_statusText-online&quot;>Online</span>"></span>
<div class="liveChat_status">
<div class="liveChat_statusBrand">
<div class="liveChat_statusBrandLogo"></div>
<div data-js-element="statusIcon" class="liveChat_statusIcon liveChat_statusIcon-offline"></div>
</div>
<div class="liveChat_statusText">
<h2 class="liveChat_title">Live Chat</h2>
<p data-js-element="statusText" class="liveChat_statusText"></p>
<p class="liveChat_infoOpeningTimes liveChat_infoOpeningTimes-alt">Average connection time 25 secs</p>
</div>
</div>
<div class="liveChat_info">
<p class="liveChat_infoOpeningTimes">Average connection time 25 secs</p>
<button data-js-element="button" class="liveChat_button" disabled="disabled">
Start Chat
<i class="liveChat_buttonIcon"></i>
</button>
</div>
</div>

</div>

<%--<div class="newYorkProductPage_deliveryAndReturns">
<div class="productDeliveryAndReturns">
<h2 class="productDeliveryAndReturns_heading">Delivery &amp; Returns</h2>
<div class="productDeliveryAndReturns_message">
To see our available delivery options please <a href="#"> Click here </a>
</div>

<div class="productDeliveryAndReturns_message">
</div>

<div class="productDeliveryAndReturns_message">
<h3 class="productDeliveryAndReturns_notHappy_heading">If I'm not completely happy with my item?</h3>

<div class="productDeliveryAndReturns_message">
Please see our
<a title="Returns Policy" href="/returns-policy.info">
returns policy.
</a>
</div>
</div>
</div>

</div>--%>
</div>
<div class="newYorkProductPage_productDescription">
<div class="productDescription mob-desc" data-component="productDescription" data-string-template-path="components/productDescriptionList/productDescriptionList">
<div class="productDescription_contentPropertyList">
<div class="productDescription_contentPropertyListItem" data-item="">
<button type="button" class="productDescription_contentPropertyListItem_Control" id="product-description-heading-1" aria-expanded="false" data-item-control="">

<div class="productDescription_contentPropertyHeading">
Description
</div>
<span class="productDescription_icon " data-icon="expand">
<svg class="productDescription_expandChevron" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
<polygon points="16.137 9 17.5 10.39 12 16 6.5 10.39 7.863 9 12 13.22"></polygon>
</svg>

</span>
<span class="productDescription_icon  productDescription_icon-hide" data-icon="collapse">
<svg class="productDescription_collapseChevron" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
<polygon points="16.137 15 12 10.78 7.863 15 6.5 13.61 12 8 17.5 13.61"></polygon>
</svg>

</span>
</button>
<div id="mob-product-des" class="productDescription_contentProperties productDescription_contentProperties_list productDescription_contentProperties-hide openAccordions" data-item-content="">
<div class="productDescription_synopsisContent">

	<p>
		



	</p>


</div>

</div>
</div>


<div class="productDescription_contentPropertyListItem" data-item="">
<%--<button type="button" class="productDescription_contentPropertyListItem_Control" id="product-description-heading-2" aria-expanded="false" data-item-control="">
<%--<div class="productDescription_contentPropertyHeading">
Product Details
</div>--%>
<%--<span class="productDescription_icon " data-icon="expand">
<svg class="productDescription_expandChevron" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
<polygon points="16.137 9 17.5 10.39 12 16 6.5 10.39 7.863 9 12 13.22"></polygon>
</svg>

</span>--%>
<%--<span class="productDescription_icon  productDescription_icon-hide" data-icon="collapse">
<svg class="productDescription_collapseChevron" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
<polygon points="16.137 15 12 10.78 7.863 15 6.5 13.61 12 8 17.5 13.61"></polygon>
</svg>

</span>
</button>--%>
<%--<div id="mob-pro-desc" class="productDescription_contentProperties productDescription_contentProperties_list productDescription_contentProperties-hide openAccordions" data-item-content="">
<div class="productDescription_contentWrapper">
<div class="productDescription_contentPropertyName">
<span class="productDescription_contentPropertyLabelStyle">Brand:</span>
</div>
<div class="productDescription_contentPropertyValue" data-information-component="brand">
<div>Rimmel</div>

</div>
</div>

<div class="productDescription_contentWrapper">
<div class="productDescription_contentPropertyName">
<span class="productDescription_contentPropertyLabelStyle">Directions:</span>
</div>
<div class="productDescription_contentPropertyValue" data-information-component="directions">
<div><p>Apply all over face, for a sun-kissed look or use single shades to contour &amp; highlight face features.</p>
</div>

</div>
</div>

<div class="productDescription_contentWrapper">
<div class="productDescription_contentPropertyName">
<span class="productDescription_contentPropertyLabelStyle">Ingredients:</span>
</div>
<div class="productDescription_contentPropertyValue" data-information-component="ingredients">
<div><p>Mica, Talc, Dimethicone, Caprylic/Capric Triglyceride, Magnesium Stearate, Synthetic Fluorphlogopite, Phenoxyethanol, Cocos Nucifera (Coconut) Oil, Caprylyl Glycol, Aloe Barbadensis Leaf Extract, [May Contain/Peut Contenir/+/-:Iron Oxides (Ci 77491, Ci 77492, Ci 77499), Titanium Dioxide (Ci 77891)].</p>
</div>

</div>
</div>

<div class="productDescription_contentWrapper">
<div class="productDescription_contentPropertyName">
<span class="productDescription_contentPropertyLabelStyle">Volume:</span>
</div>
<div class="productDescription_contentPropertyValue" data-information-component="volume">
<div>12g</div>

</div>
</div>



</div>--%>
</div>


</div>
</div>

</div>
</div>
</div>

<div class="newYorkProductPage_productRecommendations">
<div class="productRecommendations" data-component="productRecommendations" data-context="default" data-component-tracked-viewed="">


<%--<ul class="productRecommendations_itemContainer" aria-labelledby="product-recommendation-header1">
<li class="productRecommendations_item" data-recommendations-item="" data-recommendation-product="product">
<div class="productBlock " data-component="productBlock" rel="10624829" role="group" aria-labelledby="productBlock_productName-106248292">
<div>

<span class="js-enhanced-ecommerce-data hidden" data-product-title="Daniel Sandler Body Shimmer - Billion Dollar (15g)" data-product-id="10624829" data-product-category="" data-product-is-master-product-id="false" data-product-master-product-id="10624829" data-product-brand="Daniel Sandler" data-product-price="£20.00" data-product-position="1">
</span>

<div class="productBlock_imageLinkWrapper">
<a class="productBlock_link" href="/daniel-sandler-body-shimmer-billion-dollar-15g/10624829.html?rctxt=default">
<div class="productBlock_imageContainer">
<img src="https://s4.thcdn.com/productimg/300/300/10624829-1444357869719259.jpg" data-alt-src="" class="productBlock_image" data-track="product-image" alt="Daniel Sandler Body Shimmer - Billion Dollar (15g)">
</div>
</a>

</div>

<a class="productBlock_link" href="/daniel-sandler-body-shimmer-billion-dollar-15g/10624829.html?rctxt=default">
<div class="productBlock_title">
<h3 class="productBlock_productName" data-track="product-title" id="productBlock_productName-106248292">
Daniel Sandler Body Shimmer - Billion Dollar (15g)
</h3>
</div>

</a>

<div class="papBannerWrapper" data-component="papBanner">
<button data-read-more-translation=", read more" class="papBanner papBanner_popupLink" data-js-element="popupLink" aria-label="Save 10% use code: LFWINTER, read more">
<span class="papBanner_text">
Save 10% use code: LFWINTER
</span>
</button>
<div class="papPopup" data-js-element="papPopup">
<div class="papPopup_container" data-pappopup-contents="">
<h3 data-popup-title="" class="papPopup_title">Save 10% use code: LFWINTER</h3>
<div class="papPopup_text">Save 10% when you use code: LFWINTER</div>

<a href="/offers/all-products/new-10.list" class="papPopup_link">Shop the offer</a>
</div>
</div>

</div>



<div class="productBlock_priceBlock" data-is-link="true">
<div class="productBlock_price">
<span class="productBlock_priceCurrency" content="GBP"></span>
<span class="productBlock_priceValue" content="£20.00">£20.00</span>
</div>

</div>
</div>


<div class="productBlock_actions">
<span data-component="productQuickbuy"></span>
<div class="productBlock_button productBlock_button-productQuickbuySimple">
<button data-product-id="10624829" data-site-id="95" data-default-locale="en_GB" data-subsite-code="en" data-from-wishlist="false" data-from-foundationfinder="false" class="productQuickbuySimple js-e2e-add-basket" data-component="productQuickbuySimple" href="/daniel-sandler-body-shimmer-billion-dollar-15g/10624829.html?rctxt=default">

Quick Buy
<span class="visually-hidden">
Daniel Sandler Body Shimmer - Billion Dollar (15g)
</span>

</button>


</div>
</div>
</div>


</li>
    </ul>--%>

	<div class="responsiveSlot2">
					<div data-component="fourBestSellers"class="fourBestSellers trackwidget" data-block-name="hOMEPAGE" data-widget-id="2330045" data-widget-gtm-tracking data-component-tracked-viewed data-component-tracked-clicked>
						<h2 class="fourBestSellers_title" id="fourBestSellers_title-2330045">
							Related Products:
						</h2>
						<div class="container-fluid"><!--data-items="1,3,5,6"--> 
						<div class="row">
							<div class="MultiCarousel cst-p related-products" data-items="1,2,3,4" data-slide="1" id="MultiCarousel" data-interval="1000">
								<div class="MultiCarousel-inner">
                                    
                                  
                                    <asp:Repeater ID="rpt_relatedpro" runat="server">
                                        <ItemTemplate>
                                        
									      
									<div class="item">
										<div class="pad15">
                                            <a href="productdetail.aspx?p=<%#Eval("productid") %>&v=<%#Eval("variationid") %>">
                                       		<img class="related-product-img" src="images/product/<%#Eval("PhotoUrl") %>"/>
                                                </a>
											<div class="productBlock_detailsContainer related-products ">

										<h3 class="productBlock_titleContainer trackwidget title-height" >
											<a class="productBlock_title trackwidget" href="p#"><%#Eval("Name") %></a>
										</h3>
											<div class="productBlock_rating">
											<span class="visually-hidden productBlock_rating_hiddenLabel">
												4.8 Stars 54 Reviews
											</span>
											<span class="productBlock_ratingStarsContainer">
												<div class="productBlock_ratingStars" style="width:96.0%">
													<span>
														<svg class="productBlock_reviewStar" viewBox="0 0 26 26" xmlns="http://www.w3.org/2000/svg">
															<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
														</svg>
													</span>
													<span>
														<svg class="productBlock_reviewStar" viewBox="0 0 26 26" xmlns="http://www.w3.org/2000/svg">
															<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
														</svg>
													</span>
													<span>
														<svg class="productBlock_reviewStar" viewBox="0 0 26 26" xmlns="http://www.w3.org/2000/svg">
															<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
														</svg>
													</span>
													<span>
														<svg class="productBlock_reviewStar" viewBox="0 0 26 26" xmlns="http://www.w3.org/2000/svg">
															<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
														</svg>
													</span>
													<span>
														<svg class="productBlock_reviewStar" viewBox="0 0 26 26" xmlns="http://www.w3.org/2000/svg">
															<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
														</svg>
													</span>
												</div>
											</span>
											<span class="productBlock_ratingValue" aria-hidden="true">4.8</span>
											<span class="productBlock_reviewCount" aria-hidden="true">54</span>
										</div>

										<div class="productBlock_priceBlock">
											<div class="productBlock_price">
												<span class="productBlock_priceCurrency" content=""></span>
												<span class="productBlock_priceValue" content="£145.00">
													₹<%#Eval("Price") %>
												</span>
											</div>
										</div>
										<div class="productBlockButtonLink trackwidget" data-block-name="hOMEPAGE" data-block-type="" data-widget-id="2330045">
											<span id="a_<%#Eval("variationid") %>" class="productBlock_button productBlock_button-buyNow btnAddToCart">
												ADD TO CART
										
											</span>
										</div>
									</div>

										</div>
									</div>

</ItemTemplate>

                                    </asp:Repeater>
									
                                  </div>
								<button class="btn btn-primary leftLst pro-left" type="button"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
								<button class="btn btn-primary rightLst pro-right" type="button"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
							</div>
						</div>

					</div>
					</div>
				</div>
    
</div>

</div>

<div class="newYorkProductPage_productReviews">
<div class="productReviews" id="productReviewsComponent" data-component="productReviews" data-product-id="11496164" data-reviews-per-page="10" data-url="https://www.ebs1952.com/" data-has-reviews="true">

<div class="productReviews_summary">

<div class="productReviews_panelHead">
<h2 id="productReviews-summaryTitle" class="productReviews_summaryTitle">Customer Reviews</h2>
</div>

<div class="productReviews_summary-columns">
<div class="productReviews_summary-left">
<div class="productReviews_aggregateRating productReviews_aggregateRating-summary">
<span class="visually-hidden productReviews_aggregateRating_hiddenLabel">Overall Rating : 5.0 / 5 (5 Reviews)</span>
<div class="productReviews_aggregateRatingValueAndStars" role="presentation" aria-hidden="true">
<span class="productReviews_aggregateRatingValue">
5.0
</span>

<div class="productReviews_aggregateRatingStars">
<svg class="productReviewStarsPresentational" role="img" aria-label=" Stars" xmlns="http://www.w3.org/2000/svg" width="152" height="45" viewBox="0 0 152 45">
<linearGradient id="productReviews_aggregateRating-summary" x1="0" x2="100%" y1="0" y2="0">
<stop class="productReviewStarsPresentational_score" offset="100.0%"></stop>
<stop class="productReviewStarsPresentational_base" offset="0"></stop>
</linearGradient>
<path fill="url(#productReviews_aggregateRating-summary)" d="M14 10l-3.521 8.802-9.479.643 7.279 6.094-2.302 9.174 8.023-5.044 8.023 5.044-2.302-9.174 7.279-6.094-9.479-.643-3.521-8.802zm31 0l-3.521 8.802-9.479.643 7.279 6.094-2.302 9.174 8.023-5.044 8.023 5.044-2.302-9.174 7.279-6.094-9.479-.643-3.521-8.802zm31 0l-3.521 8.802-9.479.643 7.279 6.094-2.302 9.174 8.023-5.044 8.023 5.044-2.302-9.174 7.279-6.094-9.479-.643-3.521-8.802zm31 0l-3.521 8.802-9.479.643 7.279 6.094-2.302 9.174 8.023-5.044 8.023 5.044-2.302-9.174 7.279-6.094-9.479-.643-3.521-8.802zm31 0l-3.521 8.802-9.479.643 7.279 6.094-2.302 9.174 8.023-5.044 8.023 5.044-2.302-9.174 7.279-6.094-9.479-.643-3.521-8.802z"></path>
</svg>

</div>

<p class="productReviews_reviewCount Auto" data-total-reviews="5">5 Reviews</p>

</div>
<ul class="productReviews_ratingBreakdownContainer">

<li class="productReviews_ratingBreakdown">
<span class="visually-hidden productReviews_ratingBreakdown_hiddenLabel">5 5 star reviews</span>
<div class="productReviews_ratingBreakdownValueAndStars" role="presentation" aria-hidden="true">
<span class="productReviews_ratingBreakdownValue">5</span>
<svg class="productReviews_ratingBreakdownStar" role="img" aria-label="Stars" viewBox="0 0 26 26" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<span class="productReviews_ratingBreakdownBar">
<span class="productReviews_ratingBreakdownBarContainer">
<span class="productReviews_ratingBreakdownBarFill" style="width: 100%" data-5-star-reviews="5" data-breakdown-bar-5=""></span>
<span class="productReviews_ratingBreakdownReviewCount">5</span>
</span>
</span>
</div>
</li>

<li class="productReviews_ratingBreakdown">
<span class="visually-hidden productReviews_ratingBreakdown_hiddenLabel">0 4 star reviews</span>
<div class="productReviews_ratingBreakdownValueAndStars" role="presentation" aria-hidden="true">
<span class="productReviews_ratingBreakdownValue">4</span>
<svg class="productReviews_ratingBreakdownStar" role="img" aria-label="Stars" viewBox="0 0 26 26" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<span class="productReviews_ratingBreakdownBar">
<span class="productReviews_ratingBreakdownBarContainer">
<span class="productReviews_ratingBreakdownBarFill" style="width: 0%" data-4-star-reviews="0" data-breakdown-bar-4=""></span>
<span class="productReviews_ratingBreakdownReviewCount">0</span>
</span>
</span>
</div>
</li>

<li class="productReviews_ratingBreakdown">
<span class="visually-hidden productReviews_ratingBreakdown_hiddenLabel">0 3 star reviews</span>
<div class="productReviews_ratingBreakdownValueAndStars" role="presentation" aria-hidden="true">
<span class="productReviews_ratingBreakdownValue">3</span>
<svg class="productReviews_ratingBreakdownStar" role="img" aria-label="Stars" viewBox="0 0 26 26" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<span class="productReviews_ratingBreakdownBar">
<span class="productReviews_ratingBreakdownBarContainer">
<span class="productReviews_ratingBreakdownBarFill" style="width: 0%" data-3-star-reviews="0" data-breakdown-bar-3=""></span>
<span class="productReviews_ratingBreakdownReviewCount">0</span>
</span>
</span>
</div>
</li>

<li class="productReviews_ratingBreakdown">
<span class="visually-hidden productReviews_ratingBreakdown_hiddenLabel">0 2 star reviews</span>
<div class="productReviews_ratingBreakdownValueAndStars" role="presentation" aria-hidden="true">
<span class="productReviews_ratingBreakdownValue">2</span>
<svg class="productReviews_ratingBreakdownStar" role="img" aria-label="Stars" viewBox="0 0 26 26" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<span class="productReviews_ratingBreakdownBar">
<span class="productReviews_ratingBreakdownBarContainer">
<span class="productReviews_ratingBreakdownBarFill" style="width: 0%" data-2-star-reviews="0" data-breakdown-bar-2=""></span>
<span class="productReviews_ratingBreakdownReviewCount">0</span>
</span>
</span>
</div>
</li>

<li class="productReviews_ratingBreakdown">
<span class="visually-hidden productReviews_ratingBreakdown_hiddenLabel">0 1 star reviews</span>
<div class="productReviews_ratingBreakdownValueAndStars" role="presentation" aria-hidden="true">
<span class="productReviews_ratingBreakdownValue">1</span>
<svg class="productReviews_ratingBreakdownStar" role="img" aria-label="Stars" viewBox="0 0 26 26" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<span class="productReviews_ratingBreakdownBar">
<span class="productReviews_ratingBreakdownBarContainer">
<span class="productReviews_ratingBreakdownBarFill" style="width: 0%" data-1-star-reviews="0" data-breakdown-bar-1=""></span>
<span class="productReviews_ratingBreakdownReviewCount">0</span>
</span>
</span>
</div>
</li>

</ul>

<div class="productReviews_cta">
<form action="https://www.ebs1952.com/addReview.account" method="get">
<input type="hidden" name="prodId" value="11496164">
<button type="submit" class="productReviews_createReviewButton">
Create a review
</button>
</form>
</div>

</div>

</div>

<div class="productReviews_summary-right">
<div class="productReviews_topReviews" data-js-element="openModal">
<h2 class="productReviews_topReviewsTitle">Top Customer Reviews</h2>

<p class="productReviews_disclaimer">Where reviews refer to foods or cosmetic products, results may vary from person to person.  Customer reviews are independent and do not represent the views of The Hut Group.</p>

<div class="productReviews_topReviewSingle">
<div class="productReviews_topReviewTitleContainer">

<h3 class="productReviews_topReviewTitle">
AMAZING 
</h3>

<div class="productReviews_topReviewsRating">
<div class="productReviews_topReviewsRatingStars">
<div class="productReviews_topReviewsRatingStarsContainer" role="img" aria-label="5 Stars">
<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

</div>
</div>
</div>
</div>

<p class="productReviews_topReviewsExcerpt">
I LOVE this shimmer brick, it adds the perfect amount of shimmer and bronze to your face. I use this on my cheeks and forehead with a small bit on the end of my nose for extra shimmer. Super affordable and a must have in my makeup bag! 
</p>

<div class="productReviews_footer">
<div class="productReviews_footerDateAndName">
<span data-js-element="createdDate">01/10/20</span>
by
<span>Sophie</span>
</div>

</div>

<div class="productReviews_controls">
<div class="productReviews_votingArea">
<p class="productReviews_votingAreaQuestion">Did you find this review helpful?</p>
<a rel="nofollow" href="#" class="productReviews_voteYes" data-js-element="voteYes" data-js-review-id="3508618">
<svg class="productReviews_thumbsUp" width="25" height="24" viewBox="0 0 25 24" xmlns="http://www.w3.org/2000/svg">
<path d="M0 12.452v9.61c0 .916.708 1.659 1.581 1.659h3.574v-12.928h-3.574c-.873 0-1.581.743-1.581 1.659zm24.969 1.875c0-.841-.457-1.572-1.135-1.966.403-.41.652-.973.652-1.593 0-1.256-1.018-2.274-2.274-2.274h-7.235l.461-1.84c.526-2.098.028-4.386-1.524-5.85-.088-.095-.186-.181-.291-.256-.319-.224-.696-.353-1.103-.353-.397 0-.765.121-1.07.329-.501.309-.836.861-.836 1.493v3.124l-3.28 5.043c-.091.139-.171.284-.239.434-.069.149-.127.304-.174.461-.093.315-.142.643-.142.975v11.667h13.078c1.256 0 2.274-1.018 2.274-2.274 0-.486-.154-.936-.414-1.305 1.12-.141 1.988-1.096 1.988-2.254 0-.509-.169-.978-.453-1.357.985-.249 1.716-1.139 1.716-2.202z"></path>
</svg>

Yes (0)
</a>
<a rel="nofollow" href="#" class="productReviews_voteNo" data-js-element="voteNo" data-js-review-id="3508618">
<svg class="productReviews_thumbsDown" width="25" height="24" viewBox="0 0 25 24" xmlns="http://www.w3.org/2000/svg">
<path d="M0 11.354v-9.61c0-.916.708-1.659 1.581-1.659h3.574v12.928h-3.574c-.873 0-1.581-.743-1.581-1.659zm24.969-1.875c0 .841-.457 1.572-1.135 1.966.403.41.652.973.652 1.593 0 1.256-1.018 2.274-2.274 2.274h-7.235l.461 1.84c.526 2.098.028 4.386-1.524 5.85-.088.095-.186.181-.291.256-.319.224-.696.353-1.103.353-.397 0-.765-.121-1.07-.329-.501-.309-.836-.861-.836-1.493v-3.124l-3.28-5.043c-.091-.139-.171-.284-.239-.434-.069-.149-.127-.304-.174-.461-.093-.315-.142-.643-.142-.975v-11.667h13.078c1.256 0 2.274 1.018 2.274 2.274 0 .486-.154.936-.414 1.305 1.12.141 1.988 1.096 1.988 2.254 0 .509-.169.978-.453 1.357.985.249 1.716 1.139 1.716 2.202z"></path>
</svg>

No (0)
</a>
</div>


<a rel="nofollow" href="#" class="productReviews_report">
Report this review
</a>
</div>

</div>
<div class="productReviews_topReviewSingle">
<div class="productReviews_topReviewTitleContainer">

<h3 class="productReviews_topReviewTitle">
Bargain Bronzer
</h3>

<div class="productReviews_topReviewsRating">
<div class="productReviews_topReviewsRatingStars">
<div class="productReviews_topReviewsRatingStarsContainer" role="img" aria-label="5 Stars">
<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

</div>
</div>
</div>
</div>

<p class="productReviews_topReviewsExcerpt">
Such a good bronzer at such a low price! You cannot go wrong with this! :)
</p>

<div class="productReviews_footer">
<div class="productReviews_footerDateAndName">
<span data-js-element="createdDate">15/09/20</span>
by
<span>Niamh</span>
</div>

</div>

<div class="productReviews_controls">
<div class="productReviews_votingArea">
<p class="productReviews_votingAreaQuestion">Did you find this review helpful?</p>
<a rel="nofollow" href="#" class="productReviews_voteYes" data-js-element="voteYes" data-js-review-id="3472935">
<svg class="productReviews_thumbsUp" width="25" height="24" viewBox="0 0 25 24" xmlns="http://www.w3.org/2000/svg">
<path d="M0 12.452v9.61c0 .916.708 1.659 1.581 1.659h3.574v-12.928h-3.574c-.873 0-1.581.743-1.581 1.659zm24.969 1.875c0-.841-.457-1.572-1.135-1.966.403-.41.652-.973.652-1.593 0-1.256-1.018-2.274-2.274-2.274h-7.235l.461-1.84c.526-2.098.028-4.386-1.524-5.85-.088-.095-.186-.181-.291-.256-.319-.224-.696-.353-1.103-.353-.397 0-.765.121-1.07.329-.501.309-.836.861-.836 1.493v3.124l-3.28 5.043c-.091.139-.171.284-.239.434-.069.149-.127.304-.174.461-.093.315-.142.643-.142.975v11.667h13.078c1.256 0 2.274-1.018 2.274-2.274 0-.486-.154-.936-.414-1.305 1.12-.141 1.988-1.096 1.988-2.254 0-.509-.169-.978-.453-1.357.985-.249 1.716-1.139 1.716-2.202z"></path>
</svg>

Yes (0)
</a>
<a rel="nofollow" href="#" class="productReviews_voteNo" data-js-element="voteNo" data-js-review-id="3472935">
<svg class="productReviews_thumbsDown" width="25" height="24" viewBox="0 0 25 24" xmlns="http://www.w3.org/2000/svg">
<path d="M0 11.354v-9.61c0-.916.708-1.659 1.581-1.659h3.574v12.928h-3.574c-.873 0-1.581-.743-1.581-1.659zm24.969-1.875c0 .841-.457 1.572-1.135 1.966.403.41.652.973.652 1.593 0 1.256-1.018 2.274-2.274 2.274h-7.235l.461 1.84c.526 2.098.028 4.386-1.524 5.85-.088.095-.186.181-.291.256-.319.224-.696.353-1.103.353-.397 0-.765-.121-1.07-.329-.501-.309-.836-.861-.836-1.493v-3.124l-3.28-5.043c-.091-.139-.171-.284-.239-.434-.069-.149-.127-.304-.174-.461-.093-.315-.142-.643-.142-.975v-11.667h13.078c1.256 0 2.274 1.018 2.274 2.274 0 .486-.154.936-.414 1.305 1.12.141 1.988 1.096 1.988 2.254 0 .509-.169.978-.453 1.357.985.249 1.716 1.139 1.716 2.202z"></path>
</svg>

No (0)
</a>
</div>


<a rel="nofollow" href="#" class="productReviews_report">
Report this review
</a>
</div>

</div>
<div class="productReviews_topReviewSingle">
<div class="productReviews_topReviewTitleContainer">

<h3 class="productReviews_topReviewTitle">
#Dosen"t always have to cost a bomb !!
</h3>

<div class="productReviews_topReviewsRating">
<div class="productReviews_topReviewsRatingStars">
<div class="productReviews_topReviewsRatingStarsContainer" role="img" aria-label="5 Stars">
<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

</div>
</div>
</div>
</div>

<p class="productReviews_topReviewsExcerpt">
I have used Rimmel products throughout out the years !! ( cough....cough !!) And you can never go far wrong. I'm trying to rack my brain if I have ever made a bad purchase..... Yes there was one a vile purple lipstick goodness what i was thinking at the time.

But this bringing is brill especially having the combination of colours,so and instant glow straight away with a large bronzing brush and then build it in the required areas.
So another 5*+++
Rimmel in my eyes you are completely reliable and for a consumer that is what you need. 
</p>

<div class="productReviews_footer">
<div class="productReviews_footerDateAndName">
<span data-js-element="createdDate">04/08/20</span>
by
<span>Nic76</span>
</div>

<div class="productReviews_footerVerified">Verified Purchase</div>
</div>

<div class="productReviews_controls">
<div class="productReviews_votingArea">
<p class="productReviews_votingAreaQuestion">Did you find this review helpful?</p>
<a rel="nofollow" href="#" class="productReviews_voteYes" data-js-element="voteYes" data-js-review-id="3379648">
<svg class="productReviews_thumbsUp" width="25" height="24" viewBox="0 0 25 24" xmlns="http://www.w3.org/2000/svg">
<path d="M0 12.452v9.61c0 .916.708 1.659 1.581 1.659h3.574v-12.928h-3.574c-.873 0-1.581.743-1.581 1.659zm24.969 1.875c0-.841-.457-1.572-1.135-1.966.403-.41.652-.973.652-1.593 0-1.256-1.018-2.274-2.274-2.274h-7.235l.461-1.84c.526-2.098.028-4.386-1.524-5.85-.088-.095-.186-.181-.291-.256-.319-.224-.696-.353-1.103-.353-.397 0-.765.121-1.07.329-.501.309-.836.861-.836 1.493v3.124l-3.28 5.043c-.091.139-.171.284-.239.434-.069.149-.127.304-.174.461-.093.315-.142.643-.142.975v11.667h13.078c1.256 0 2.274-1.018 2.274-2.274 0-.486-.154-.936-.414-1.305 1.12-.141 1.988-1.096 1.988-2.254 0-.509-.169-.978-.453-1.357.985-.249 1.716-1.139 1.716-2.202z"></path>
</svg>

Yes (0)
</a>
<a rel="nofollow" href="#" class="productReviews_voteNo" data-js-element="voteNo" data-js-review-id="3379648">
<svg class="productReviews_thumbsDown" width="25" height="24" viewBox="0 0 25 24" xmlns="http://www.w3.org/2000/svg">
<path d="M0 11.354v-9.61c0-.916.708-1.659 1.581-1.659h3.574v12.928h-3.574c-.873 0-1.581-.743-1.581-1.659zm24.969-1.875c0 .841-.457 1.572-1.135 1.966.403.41.652.973.652 1.593 0 1.256-1.018 2.274-2.274 2.274h-7.235l.461 1.84c.526 2.098.028 4.386-1.524 5.85-.088.095-.186.181-.291.256-.319.224-.696.353-1.103.353-.397 0-.765-.121-1.07-.329-.501-.309-.836-.861-.836-1.493v-3.124l-3.28-5.043c-.091-.139-.171-.284-.239-.434-.069-.149-.127-.304-.174-.461-.093-.315-.142-.643-.142-.975v-11.667h13.078c1.256 0 2.274 1.018 2.274 2.274 0 .486-.154.936-.414 1.305 1.12.141 1.988 1.096 1.988 2.254 0 .509-.169.978-.453 1.357.985.249 1.716 1.139 1.716 2.202z"></path>
</svg>

No (0)
</a>
</div>


<a rel="nofollow" href="#" class="productReviews_report">
Report this review
</a>
</div>

</div>
<div class="productReviews_topReviewSingle">
<div class="productReviews_topReviewTitleContainer">

<h3 class="productReviews_topReviewTitle">
Love it 
</h3>

<div class="productReviews_topReviewsRating">
<div class="productReviews_topReviewsRatingStars">
<div class="productReviews_topReviewsRatingStarsContainer" role="img" aria-label="5 Stars">
<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

</div>
</div>
</div>
</div>

<p class="productReviews_topReviewsExcerpt">
Just ran out of an expensive bronzer in lockdown, needed something quick and fast so ordered this! Have to say very impressed and really like it!! Give it a go, very reasonable.
</p>

<div class="productReviews_footer">
<div class="productReviews_footerDateAndName">
<span data-js-element="createdDate">13/06/20</span>
by
<span>Jo West Oxon </span>
</div>

<div class="productReviews_footerVerified">Verified Purchase</div>
</div>

<div class="productReviews_controls">
<div class="productReviews_votingArea">
<p class="productReviews_votingAreaQuestion">Did you find this review helpful?</p>
<a rel="nofollow" href="#" class="productReviews_voteYes" data-js-element="voteYes" data-js-review-id="3280990">
<svg class="productReviews_thumbsUp" width="25" height="24" viewBox="0 0 25 24" xmlns="http://www.w3.org/2000/svg">
<path d="M0 12.452v9.61c0 .916.708 1.659 1.581 1.659h3.574v-12.928h-3.574c-.873 0-1.581.743-1.581 1.659zm24.969 1.875c0-.841-.457-1.572-1.135-1.966.403-.41.652-.973.652-1.593 0-1.256-1.018-2.274-2.274-2.274h-7.235l.461-1.84c.526-2.098.028-4.386-1.524-5.85-.088-.095-.186-.181-.291-.256-.319-.224-.696-.353-1.103-.353-.397 0-.765.121-1.07.329-.501.309-.836.861-.836 1.493v3.124l-3.28 5.043c-.091.139-.171.284-.239.434-.069.149-.127.304-.174.461-.093.315-.142.643-.142.975v11.667h13.078c1.256 0 2.274-1.018 2.274-2.274 0-.486-.154-.936-.414-1.305 1.12-.141 1.988-1.096 1.988-2.254 0-.509-.169-.978-.453-1.357.985-.249 1.716-1.139 1.716-2.202z"></path>
</svg>

Yes (0)
</a>
<a rel="nofollow" href="#" class="productReviews_voteNo" data-js-element="voteNo" data-js-review-id="3280990">
<svg class="productReviews_thumbsDown" width="25" height="24" viewBox="0 0 25 24" xmlns="http://www.w3.org/2000/svg">
<path d="M0 11.354v-9.61c0-.916.708-1.659 1.581-1.659h3.574v12.928h-3.574c-.873 0-1.581-.743-1.581-1.659zm24.969-1.875c0 .841-.457 1.572-1.135 1.966.403.41.652.973.652 1.593 0 1.256-1.018 2.274-2.274 2.274h-7.235l.461 1.84c.526 2.098.028 4.386-1.524 5.85-.088.095-.186.181-.291.256-.319.224-.696.353-1.103.353-.397 0-.765-.121-1.07-.329-.501-.309-.836-.861-.836-1.493v-3.124l-3.28-5.043c-.091-.139-.171-.284-.239-.434-.069-.149-.127-.304-.174-.461-.093-.315-.142-.643-.142-.975v-11.667h13.078c1.256 0 2.274 1.018 2.274 2.274 0 .486-.154.936-.414 1.305 1.12.141 1.988 1.096 1.988 2.254 0 .509-.169.978-.453 1.357.985.249 1.716 1.139 1.716 2.202z"></path>
</svg>

No (0)
</a>
</div>


<a rel="nofollow" href="#" class="productReviews_report">
Report this review
</a>
</div>

</div>
<div class="productReviews_topReviewSingle">
<div class="productReviews_topReviewTitleContainer">

<h3 class="productReviews_topReviewTitle">
Bronzer 
</h3>

<div class="productReviews_topReviewsRating">
<div class="productReviews_topReviewsRatingStars">
<div class="productReviews_topReviewsRatingStarsContainer" role="img" aria-label="5 Stars">
<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

<svg class="productReviews_reviewRatingStar productReviews_reviewRatingScore5" width="20" height="20" viewBox="0 0 26 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<polygon points="13 0 9.47916667 8.80208333 0 9.4453125 7.27864583 15.5390625 4.9765625 24.7135417 13 19.6692708 21.0234375 24.7135417 18.7213542 15.5390625 26 9.4453125 16.5208333 8.80208333"></polygon>
</svg>

</div>
</div>
</div>
</div>

<p class="productReviews_topReviewsExcerpt">
I was using many bronzers from many brand and definitely I can recommend that one as a very good one. It’s perfect finish to your make up. It gives nice touch of sun which stay so long and looks so natural. Highly recommended. I will definitely keep Buying this one.

</p>

<div class="productReviews_footer">
<div class="productReviews_footerDateAndName">
<span data-js-element="createdDate">31/08/19</span>
by
<span>Dajana </span>
</div>

<div class="productReviews_footerVerified">Verified Purchase</div>
</div>

<div class="productReviews_controls">
<div class="productReviews_votingArea">
<p class="productReviews_votingAreaQuestion">Did you find this review helpful?</p>
<a rel="nofollow" href="#" class="productReviews_voteYes" data-js-element="voteYes" data-js-review-id="2740120">
<svg class="productReviews_thumbsUp" width="25" height="24" viewBox="0 0 25 24" xmlns="http://www.w3.org/2000/svg">
<path d="M0 12.452v9.61c0 .916.708 1.659 1.581 1.659h3.574v-12.928h-3.574c-.873 0-1.581.743-1.581 1.659zm24.969 1.875c0-.841-.457-1.572-1.135-1.966.403-.41.652-.973.652-1.593 0-1.256-1.018-2.274-2.274-2.274h-7.235l.461-1.84c.526-2.098.028-4.386-1.524-5.85-.088-.095-.186-.181-.291-.256-.319-.224-.696-.353-1.103-.353-.397 0-.765.121-1.07.329-.501.309-.836.861-.836 1.493v3.124l-3.28 5.043c-.091.139-.171.284-.239.434-.069.149-.127.304-.174.461-.093.315-.142.643-.142.975v11.667h13.078c1.256 0 2.274-1.018 2.274-2.274 0-.486-.154-.936-.414-1.305 1.12-.141 1.988-1.096 1.988-2.254 0-.509-.169-.978-.453-1.357.985-.249 1.716-1.139 1.716-2.202z"></path>
</svg>

Yes (4)
</a>
<a rel="nofollow" href="#" class="productReviews_voteNo" data-js-element="voteNo" data-js-review-id="2740120">
<svg class="productReviews_thumbsDown" width="25" height="24" viewBox="0 0 25 24" xmlns="http://www.w3.org/2000/svg">
<path d="M0 11.354v-9.61c0-.916.708-1.659 1.581-1.659h3.574v12.928h-3.574c-.873 0-1.581-.743-1.581-1.659zm24.969-1.875c0 .841-.457 1.572-1.135 1.966.403.41.652.973.652 1.593 0 1.256-1.018 2.274-2.274 2.274h-7.235l.461 1.84c.526 2.098.028 4.386-1.524 5.85-.088.095-.186.181-.291.256-.319.224-.696.353-1.103.353-.397 0-.765-.121-1.07-.329-.501-.309-.836-.861-.836-1.493v-3.124l-3.28-5.043c-.091-.139-.171-.284-.239-.434-.069-.149-.127-.304-.174-.461-.093-.315-.142-.643-.142-.975v-11.667h13.078c1.256 0 2.274 1.018 2.274 2.274 0 .486-.154.936-.414 1.305 1.12.141 1.988 1.096 1.988 2.254 0 .509-.169.978-.453 1.357.985.249 1.716 1.139 1.716 2.202z"></path>
</svg>

No (0)
</a>
</div>


<a rel="nofollow" href="#" class="productReviews_report">
Report this review
</a>
</div>

</div>

</div>

</div>
</div>
</div>

</div>

</div>

<div class="productImageZoom" data-hide="true" data-component="productImageZoom" data-is-transparent="" role="dialog" tabindex="-1">
<div class="productImageZoom_container" role="dialog" tabindex="-1" aria-labelledby="product-image-zoom-modal-title">
<h2 id="product-image-zoom-modal-title" class="visually-hidden">Zoom</h2>
<div class="productImageZoom_imageWrapper">
<div class="productImageZoom_loader"></div>
<img class="productImageZoom_image" alt="" src="//:0">

<button type="button" class="productImageZoom_icon zoomIn" aria-label="Zoom in" title="Zoom in">
</button>
<button type="button" class="productImageZoom_icon zoomOut" aria-label="Zoom out" title="Zoom out">

</button>
</div>

<div class="productImageZoom_thumbnails">
<ul class="productImageZoom_thumbnailContainer"></ul>
</div>

<button type="button" class="productImageZoom_exit" aria-label="Close" title="Close"></button>

</div>
<div class="productImageZoom_overlay"></div>
</div>

<script type="application/ld+json">
{"@type":"Product","@context":"https://schema.org","@id":"11496164","sku":"11496164","mpn":"34774643002","name":"Rimmel Radiance Shimmer Brick 12g - 02","brand":{"@type":"Thing","name":"Rimmel"},"description":"Achieve a soft shimmer effect with the Rimmel Radiance Shimmer Brick; an ultra-fine bronzing powder that delivers an instant, sun-kissed glow.  The multi-tonal shimmer brick boasts an array of complementary hues. Sweep a brush across the entire pan and apply to the visage for an all-over glow, or use the bronzer’s single shades to highlight and contour specific facial features. Finished with a subtle shimmer, skin appears bronzed, radiant and illuminated. ","aggregateRating":{"@type":"AggregateRating","ratingValue":"5.0","reviewCount":"5"},"offers":[{"@type":"Offer","price":"6.99","priceCurrency":"GBP","url":"https://www.lookfantastic.com/rimmel-radiance-shimmer-brick-12g-02/11496164.html?shippingcountry\u003dGB\u0026switchcurrency\u003dGBP","itemCondition":"https://schema.org/NewCondition","sku":"11496164","availability":"https://schema.org/InStock"}],"image":"https://s2.thcdn.com/productimg/300/300/11496164-3034627107774582.jpg"}
</script>

<script type="application/ld+json">
{"@context":"https://schema.org","@id":"11496164","review":[{"@type":"Review","description":"I LOVE this shimmer brick, it adds the perfect amount of shimmer and bronze to your face. I use this on my cheeks and forehead with a small bit on the end of my nose for extra shimmer. Super affordable and a must have in my makeup bag! ","datePublished":"2020-10-01","itemReviewed":{"@type":"Thing","name":"Rimmel Radiance Shimmer Brick 12g - 02"},"reviewRating":{"@type":"Rating","worstRating":0,"bestRating":5,"ratingValue":5},"author":{"@type":"Person","name":"Sophie"}},{"@type":"Review","description":"Such a good bronzer at such a low price! You cannot go wrong with this! :)","datePublished":"2020-09-15","itemReviewed":{"@type":"Thing","name":"Rimmel Radiance Shimmer Brick 12g - 02"},"reviewRating":{"@type":"Rating","worstRating":0,"bestRating":5,"ratingValue":5},"author":{"@type":"Person","name":"Niamh"}},{"@type":"Review","description":"I have used Rimmel products throughout out the years !! ( cough....cough !!) And you can never go far wrong. I\u0027m trying to rack my brain if I have ever made a bad purchase..... Yes there was one a vile purple lipstick goodness what i was thinking at the time.    But this bringing is brill especially having the combination of colours,so and instant glow straight away with a large bronzing brush and then build it in the required areas.  So another 5*+++  Rimmel in my eyes you are completely reliable and for a consumer that is what you need. ","datePublished":"2020-08-04","itemReviewed":{"@type":"Thing","name":"Rimmel Radiance Shimmer Brick 12g - 02"},"reviewRating":{"@type":"Rating","worstRating":0,"bestRating":5,"ratingValue":5},"author":{"@type":"Person","name":"Nic76"}},{"@type":"Review","description":"Just ran out of an expensive bronzer in lockdown, needed something quick and fast so ordered this! Have to say very impressed and really like it!! Give it a go, very reasonable.","datePublished":"2020-06-13","itemReviewed":{"@type":"Thing","name":"Rimmel Radiance Shimmer Brick 12g - 02"},"reviewRating":{"@type":"Rating","worstRating":0,"bestRating":5,"ratingValue":5},"author":{"@type":"Person","name":"Jo West Oxon "}},{"@type":"Review","description":"I was using many bronzers from many brand and definitely I can recommend that one as a very good one. It’s perfect finish to your make up. It gives nice touch of sun which stay so long and looks so natural. Highly recommended. I will definitely keep Buying this one.  ","datePublished":"2019-08-31","itemReviewed":{"@type":"Thing","name":"Rimmel Radiance Shimmer Brick 12g - 02"},"reviewRating":{"@type":"Rating","worstRating":0,"bestRating":5,"ratingValue":5},"author":{"@type":"Person","name":"Dajana "}}]}
</script>
<%--<script type="text/javascript">

    $('.container').imagesLoaded( function() {
  $("#exzoom").exzoom({
        autoPlay: false,
    });
  $("#exzoom").removeClass('hidden')
});

</script>--%>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script>$(document).ready(function () {
	
	$("#product-description-heading-1").click(function () {
		$("#mob-product-des").toggleClass("productDescription_contentProperties-hide");
		$("#product-description-heading-1 .productDescription_icon ").toggleClass("productDescription_icon-hide");
	});
	$("#product-description-heading-2").click(function () {
		
		$("#mob-pro-desc").toggleClass("productDescription_contentProperties-hide");
		$("#product-description-heading-2 .productDescription_icon ").toggleClass("productDescription_icon-hide");
	});
	$("#product-description-heading-lg-1").click(function () {
		$("#product-description-heading-lg-1 + .productDescription_contentProperties ").toggleClass("productDescription_contentProperties-hide");
		$("#product-description-heading-lg-1 .productDescription_icon ").toggleClass("productDescription_icon-hide");
	
	
	});
	$("#product-description-heading-lg-2").click(function () {
		$("#product-description-heading-lg-2 + .productDescription_contentProperties ").toggleClass("productDescription_contentProperties-hide");
		$("#product-description-heading-lg-2 .productDescription_icon ").toggleClass("productDescription_icon-hide");
	});
});
</script>
</main>
					</div>
					
					
			</div>

			
		</div>
</asp:Content>

