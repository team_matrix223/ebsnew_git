﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mycart : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{

	}

    [WebMethod]
    public static string GetCartHTML()
    {
        decimal totalval = 0;
        Calc objCalc = new Calc();
        string cart = new CartBLL().GetCartHTML(HttpContext.Current.Session[Constants.CartSession].ToString(), objCalc);
    
        totalval = (objCalc.NetAmount - (objCalc.CouponVal+ objCalc.PointsRedeemVal)) ;
        HttpContext.Current.Session[Constants.FreeDelAmt] = objCalc.FreeDeliveryAmt;
        if (totalval >= objCalc.FreeDeliveryAmt)
        {

            HttpContext.Current.Session[Constants.NetAmount] = totalval * 100;
        }
        else if (totalval < objCalc.FreeDeliveryAmt)
        {
            totalval += objCalc.DeliveryCharges;
           
        }

        HttpContext.Current.Session[Constants.NetAmount] = totalval * 100;
        var JsonData = new
        {
            
          
            MCOA = objCalc.MinimumCheckOutAmt,
            FRE = objCalc.FreeDeliveryAmt,
            DC = objCalc.DeliveryCharges=string.IsNullOrEmpty(objCalc.DeliveryCharges.ToString())?0: objCalc.DeliveryCharges,
            ST = objCalc.SubTotal,
            NA = totalval,
            DA = objCalc.DisAmt,
            CV = objCalc.CouponVal,
            PRV = objCalc.PointsRedeemVal,
            html = cart,
            TotalItems = objCalc.TotalItems
        };
 
      
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string BindActivePoints()
    {

        Settings objSetting = new Settings();
        objSetting = new SettingsDAL().GetSett();
        Wallet objwallet = new Wallet();
        {
            objwallet.UserId = Convert.ToInt16(HttpContext.Current.Session[Constants.UserId]);
        }

        new WalletBLL().GetWalletStatusCart(objwallet, HttpContext.Current.Session.SessionID);
        string ltpoints = "";
        Int64 Points = 0;
        Points = (Convert.ToInt64(objwallet.Total) - Convert.ToInt64(objwallet.Used));
        if (objwallet.Status == "-1")
        {


            ltpoints = "<div  class='row'><div class='col-md-12 point-col' style ='text-align:center;font-weight:bold;color:Red'> You Have " + Points + " EBS Points.1 Point = ₹" + objSetting.PointRate + ".</div></div>";
            ltpoints += " <div  class='row'><div class='col-md-12 how-many-col' style ='text-align:center;font-weight:bold'>Please Apply Coupon First And Then Apply Referral Points</div></div>";
            ltpoints += " <div  class='row' ><div class='col-md-3'></div><div class='col-md-9 apply-col' style ='text-align:center'><table><tr><td><input type='text' id='txtwallet' class='form-control' style='background:white' /></td><td><button type='button' id='btnApplyPoints'  class='btnVoucher' style='margin:5px'>Apply</div></td> </tr></table></div></div>";

        }
        else if (objwallet.Status == "1")
        {

            ltpoints = "<div class='row'><div class='col-md-12'>";


            ltpoints += "<div style='border:dashed 1px silver;padding:24px;width:225px;' class='ebds-point-div'> " +
    " <b style='font-size:20px' class='ebs-pont'>EBS Point</b><br><span class='point-value' style='font-weight:bold;font-size:24px'> " + " " + objwallet.Points + "Points</span><br/><span style='color:green' class='applied'> Applied Successfully</span><p style='font-weight:bold;color:green;font-size: 20px; ' class='offer'>"  +
    " EBS POINTS DISCOUNT OFFER</p><p style='font-size:20px;' class='cong'> Congratulations,You Got ₹" + objwallet.DisAmt + " Discount on Your Bill. " +
    "   </p><hr style='margin:5px'><table width='100%' class='point-table'> " +
    " <tbody><tr><td><div id='btnRemoveVoucher1' class='btnVoucher'  style='margin-top:0px;width:119px'>Remove </div> " +
    " </td></tr></tbody></table></div></div>";

    //        ltpoints += "<div class='col-md-8'>";

    //        ltpoints +="<p style='font-weight:bold;color:green;font-size: 20px; '>"  +
    //" EBS POINTS DISCOUNT OFFER</p><p style='font-size:20px;'> Congratulations,You Got ₹" + objwallet.DisAmt + " Discount on Your Bill. " +
    //"   </p><hr style='margin:5px'><table width='100%'> " +
    //" <tbody><tr><td><div id='btnRemoveVoucher1' class='btnVoucher'  style='margin-top:0px;width:119px'>Remove </div> " +
    //" </td></tr></tbody></table>";


    //        ltpoints += "</div></div>";
        }
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            PointHtml = ltpoints,
            walletpoints = Points

        };
        return ser.Serialize(JsonData);



    }



    [WebMethod]
    public static string ApplyOfferForPoints(decimal DisAmt)
    {
        decimal DisVal = 0;

        Settings objSetting = new Settings();
        objSetting = new SettingsDAL().GetSett();
        DisVal = (DisAmt * objSetting.PointRate);
        string GiftMsg = "";
        int Status = new OffersBLL().ApplyOfferForPoints(HttpContext.Current.Session.SessionID, Convert.ToInt64(HttpContext.Current.Session[Constants.UserId]), DisAmt);
        string offerHtml = "<div class='row'><div class='col-md-4'>";


        offerHtml += " <div style='border:dashed 1px silver;padding:24px;width:225px;'> " +
" <b style='font-size:20px'>EBS Point</b><br><span style='font-weight:bold;font-size:24px'> " + " " + DisAmt + "Points</span><br/><span style='color:green'> Applied Successfully</span></div></div>";

        offerHtml += "<div class='col-md-8'>";

        offerHtml += "<p style='font-weight:bold;color:green;font-size: 20px; '>" +
" EBS POINTS DISCOUNT OFFER</p><p style='font-size:20px;'> Congratulations,You Got Rs." + DisVal + "  Discount  on Your BIll. " +
"   </p><hr style='margin:5px'><table width='100%'> " +
" <tbody><tr><td><div id='btnRemoveVoucher' class='btnVoucher' onclick='RemoveOfferPoint()' style='margin-top:0px;width:119px'>Remove </div> " +
" </td></tr></tbody></table>";


        offerHtml += "</div></div>";



        var JsonData = new
        {
            Status = Status,
            OfferHtml = offerHtml

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string BindActiveOffer()
    {

        OfferStatus obj = new OfferStatus();
        obj = new OffersBLL().GetActiveOfferForUser(HttpContext.Current.Session.SessionID, Convert.ToInt64(HttpContext.Current.Session[Constants.UserId]));
        string ltPesOffers = "";
        string Gift = "";
        if (obj.Status == -1)
        {


            //ltPesOffers = "<div class='row'><div class='col-md-3'><b>e-Coupon Code:</b></div><div class='col-md-6'><table><tr><td><input type='text' id='txtCouponCode' class='form-control' style='background:white' /></td><td><div id='btnApplyCoupon'  class='btnVoucher' style='margin:5px'>Apply</div></td></tr></table></div>";

            //ltPesOffers += "<div class='col-md-3'><div id='btnViewActiveOffers'  style='cursor:pointer'><img src='images/btnSpecialOffer.png' style='width:150px'/></div></div></div>";
            //ltPesOffers += "<div class='row'><div class='col-md-12'><span style='color:Red'>*</span > A maximum of one voucher is applicable for an order. </div></div>";





        }
        else
        {


             ltPesOffers = "<div class='row'><div>";


            ltPesOffers += "<div style='border:dashed 2px #1e8ece;padding:0 24px;text-align:center;' class='col-md-12 col-sm-12 col-xs-12'> " +
    " <b style='font-size:20px' class='offer-b'>Offer</b><span style='font-weight:bold;font-size:24px' class='coup-no'> " +
    " ''" + obj.CouponNo + "''</span><span style='color:green' class='coupn-message'> Applied Successfully</span><span style='font-weight:bold;color:green;font-size: 20px;' class='cashback-p'>" +
    " ''" + obj.Title + "''</span><span style='font-size:20px;' class='cashback-desc'>" + obj.Description + " " +
    "</span><hr style='margin:5px;display:none;'><table width='100%' class='remove-table'> " +
    " <tbody><tr><td><button type='button' id='btnRemoveVoucher' class='btnVoucher'  style='margin-top:0px;width:119px'>Remove </button> " +
    " </td></tr></tbody></table></div></div>";

            if (obj.GiftName != "")
            {
                Gift = "Congratulation:You got " + obj.GiftName + "";
            }




        }

        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            OfferHtml = ltPesOffers,
            Gift = Gift,
           CouponNo= obj.CouponNo
        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string RemovePointOffer()
    {
        int Status = new OffersBLL().RemovePointOffer(HttpContext.Current.Session.SessionID);
        Settings objSetting = new Settings();
        objSetting = new SettingsDAL().GetSett();
        Wallet objwallet = new Wallet();
        {
            objwallet.UserId = Convert.ToInt16(HttpContext.Current.Session[Constants.UserId]);
        }

        new WalletBLL().GetBalanceByUser(objwallet);

        Int64 Points = 0;
        Points = (Convert.ToInt64(objwallet.Total) - Convert.ToInt64(objwallet.Used));


      
        string offerHtml = "<div  class='row'><div class='col-md-12 point-col' style ='text-align:center;font-weight:bold;color:Red'> You Have " + Points + " EBS Points.1 Point = ₹" + objSetting.PointRate + ".</div></div>";
        offerHtml += " <div  class='row'><div class='col-md-12 how-many-col' style ='text-align:center;font-weight:bold'>Please Apply Coupon First And Then Apply Referral Points</div></div>";
        offerHtml += " <div  class='row' ><div class='col-md-3'></div><div class='col-md-9 apply-col' style ='text-align:center'><table><tr><td><input type='text' id='txtwallet' class='form-control' style='background:white' /></td><td><button type='button' id='btnApplyPoints'  class='btnVoucher' style='margin:5px'>Apply</div></td> </tr></table></div></div>";
        var JsonData = new
        {
            Status = Status,
            OfferHtml = offerHtml

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string ATC(string vid, string qty, string st, string type,int attr_id)
    {
        string m_Status = "Plus";

        if (st == "m")
        {
            m_Status = "Minus";

        }
        Int16 IsError = 0;
        int m_Qty = CommonFunctions.ValidateQuantity(qty);
        if (!CommonFunctions.IsValidProductId(vid))
        {
            IsError = 1;
        }

        Calc objCalc = new Calc();
        UserCart objUserCart = new UserCart();
        string html = "";

        if (IsError == 0)
        {
            objUserCart.SessionId = HttpContext.Current.Session.SessionID;
            objUserCart.ProductId = 0;
            objUserCart.CustomerId = 0;
            objUserCart.VariationId = Convert.ToInt64(vid);
            objUserCart.ProductDesc = "";
            objUserCart.Qty = m_Qty;
            objUserCart.Price = 0;
            objUserCart.type = type;
            objUserCart.attr_id = attr_id;
            html = new CartBLL().UserCartIncrDecr(objUserCart, m_Status, objCalc);

        }

        var JsonData = new
        {

            qty = objUserCart.Qty,
            vid = vid,
            error = IsError,
            cartQtyHTML = html,
            Calc = objCalc
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);

    }
    [WebMethod]
    public static string GetCartHTMLCount()
    {
        Calc objCalc = new Calc();
        try
        {
            string cart = new CartBLL().GetCartHTMLCount(HttpContext.Current.Session[Constants.CartSession].ToString(), objCalc);
        }
        catch
        {

           
        }
 
        var JsonData = new
        {
            TotalItems = objCalc.TotalItems
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static void  DelFromCart(string req,int productid,int attr_id)
    {

      new CartBLL().UserCartDelete(HttpContext.Current.Session[Constants.CartSession].ToString(), req, productid, attr_id);
 
    }

    protected void lnk_btn_checkout_Click(object sender, EventArgs e)
    {


        if (Session[Constants.UserId] != null)
        {

            Response.Redirect("registeruser_address.aspx");
        }
        else
        {
          //  Response.Redirect("login.aspx");

            Response.Redirect("addaddress.aspx");
        }
      
    }

    [WebMethod]
    public static string GetGifts(string CouponNo)
    {
        string DisType = "";
        string html = new OffersBLL().GetGiftsHTMLByCouponNo(CouponNo, out DisType);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            GiftHtml = html,
            DiscountType = DisType
        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string ApplyCoupon(string CouponNo, Int32 GiftId)
    {
        string GiftMsg = "";
        OfferStatus obj = new OfferStatus();
        obj = new OffersBLL().ApplyOffer(CouponNo, HttpContext.Current.Session.SessionID, Convert.ToInt64(HttpContext.Current.Session[Constants.UserId]), GiftId);

        string offerHtml = "<div class='row'><div>";


        offerHtml += "<div style='border:dashed 2px #1e8ece;padding:0 24px;text-align:center;' class='col-md-12 col-sm-12 col-xs-12'> " +
" <b style='font-size:20px' class='offer-b'>Offer</b><span style='font-weight:bold;font-size:24px' class='coup-no'> " +
" ''" + obj.CouponNo + "''</span><span style='color:green' class='coupn-message'> Applied Successfully</span><span style='font-weight:bold;color:green;font-size: 20px;' class='cashback-p'>" +
" ''" + obj.Title + "''</span><span style='font-size:20px;' class='cashback-desc'>" + obj.Description + " " +
"</span><hr style='margin:5px;display:none;'><table width='100%' class='remove-table'> " +
" <tbody><tr><td><button type='button' id='btnRemoveVoucher' class='btnVoucher'  style='margin-top:0px;width:119px'>Remove </button> " +
" </td></tr></tbody></table></div></div>";

//        offerHtml += "<div style='border:dashed 1px silver;padding:24px;' class='col-md-6 col-sm-6 col-xs-12 remove-coupn'>";

////        offerHtml += "<p style='font-weight:bold;color:green;font-size: 20px;' class='cashback-p'>" +
////" " + obj.Title + "</p><p style='font-size:20px;' class='cashback-desc'>" + obj.Description + " " +
////"   </p><hr style='margin:5px'><table width='100%'> " +
////" <tbody><tr><td><button type='button' id='btnRemoveVoucher' class='btnVoucher'  style='margin-top:0px;width:119px'>Remove </button> " +
////" </td></tr></tbody></table>";


//        offerHtml += "</div></div>";
        if (GiftId != 0)
        {
            GiftMsg = "Congratulation:You got " + obj.GiftName + "";
        }

        var JsonData = new
        {
            Status = obj.Status,
            Message = obj.Message,
            OfferHtml = offerHtml,
            GiftMessage = GiftMsg
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string RemoveCoupon(string CouponNo)
    {


        int Status = new OffersBLL().RemoveOffer(CouponNo, HttpContext.Current.Session.SessionID);

        //       string offerHtml = "<table><tr><td style='font-weight:bold;padding-right:5px'>e-Coupon Code:</td>"+
        //"<td><input type='text' id='txtCouponCode' class='form-control' style='background:white' /></td>"+
        //"<td><div id='btnApplyCoupon' onclick='ApplyVouher(\"\",1)'  class='btnVoucher'>Apply</div></td><td>" +
        // "<div id='btnViewActiveOffers' onclick='ActiveOffer()'  style='cursor:pointer'>" +
        // "<img src='images/btnSpecialOffer.png'  style='width:150px'/></div></td></tr><tr><td colspan='100%' style='color:Red;font-style:italic'>" +
        // "<span style='color:Red'>*</span > A maximum of one voucher is applicable for an order. </td></tr>"+
        //"<tr><td colspan='100%'></td></tr></table>";
        //string offerHtml = "<div class='row'><div class='col-md-3'><b>e-Coupon Code:</b></div><div class='col-md-6'><table><tr><td><input type='text' id='txtCouponCode' class='form-control' style='background:white' /></td><td><div id='btnApplyCoupon' onclick='ApplyVouherNew(\"\",1)'  class='btnVoucher' style='margin:5px'>Apply</div></td></tr></table></div>";

        //offerHtml += "<div class='col-md-3'><div id='btnViewActiveOffers' onclick='ActiveOffer()' style='cursor:pointer'><img src='images/btnSpecialOffer.png' style='width:150px'/></div></div></div>";
        //offerHtml += "<div class='row'><div class='col-md-12'><span style='color:Red'>*</span > A maximum of one voucher is applicable for an order. </div></div>";


        var JsonData = new
        {
            Status = Status,
            //OfferHtml = offerHtml

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string BindOrderSummary()
    {
        Calc objCalc = new Calc();
        string SessionId = HttpContext.Current.Session.SessionID;
        string ItemList = new CartBLL().GetOrderSummary(SessionId, objCalc);
        string SubTotal = objCalc.SubTotal.ToString();
        string FreeDeliveryAmt = objCalc.FreeDeliveryAmt.ToString();
        string DeliveryCharges = objCalc.DeliveryCharges.ToString();
        decimal totalval = 0;
        string DisAmount = objCalc.DisAmt.ToString();
        decimal NetAmount = Convert.ToDecimal(Convert.ToDecimal(objCalc.NetAmount) - Convert.ToDecimal(objCalc.DisAmt.ToString()));
        var JsonData = new
        {
            IL = ItemList,
            ST = SubTotal,
            DC = DeliveryCharges,
            DA = DisAmount,
            NA = NetAmount,
            FRE = FreeDeliveryAmt,
        };

        if (objCalc.NetAmount >= objCalc.FreeDeliveryAmt)
        {

            HttpContext.Current.Session[Constants.NetAmount] = objCalc.NetAmount * 100;
        }
        else if (objCalc.NetAmount < objCalc.FreeDeliveryAmt)
        {
            totalval = objCalc.NetAmount + objCalc.DeliveryCharges;
            HttpContext.Current.Session[Constants.NetAmount] = totalval * 100;

        }
    
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);

    }
}