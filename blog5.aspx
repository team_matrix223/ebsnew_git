﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" CodeFile="blog5.aspx.cs" Inherits="blog5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="customcss/blog.css" rel="stylesheet" />
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9 col-sm-9 col-xs-12">
				<div class="blog">
					<h2 class="blog-title">DIY craft ideas and activities for adults & kids </h2>
					<div class="post-info">
							<div class="item post-posed-date">
							<span class="label">Posted:</span>
							<span class="value">January 09, 2021</span>
							</div>
							<div class="item post-categories">
							<span class="label">Categories:</span>
							<a title="Paper Stationery" href="#">Art and Craft</a>,<a href="#">Craft</a></div>
							<div class="item post-author">
							<span class="label">Author:</span>
							<span class="value">
							<a title="Varun Chaudhary" href="#">
							Varun Chaudhary
							</a>
							</span>
							</div>
					</div>
					<div class="blog-img-div">
						<img src="images/blog/blog5.jpeg" />
					</div>
					<div class="blog-desc">
						<p>Let's admit it: The year gone by (aka 2020) has given so many of us more days at home than we might have ever imagined. The advantage to becoming holed up at home? There was never a better time to accomplish DIY projects to your heart's content!&nbsp; You might be struggling with space, idea organization or just slightly lackadaisical designs concepts at the moment. Perhaps the white canvas or craft materials looking at you might seem a bit daunting. Whatever your situation, we are here to help you have crafty fun with 2020 being dubbed the official DIY Year. That's why we've compiled a list of the best arts and crafts to do at home for both kids and adults. All the materials used in the craft items below can be sourced from <a href="https://www.ebs1952.com/" target="_blank" rel="noopener">EBS- India's best online stationery store. </a>&nbsp;</p>
<h4><strong>Why should kids create crafts? </strong></h4>
<p>Craft can be fun! But maybe for each child they aren't. Not everybody is crafty or involved in crafting and that's alright.</p>
<p>Craft tutorials and lessons may be observed to a degree, or taken for an open-ended artistic session as a reference and encouragement. However, the crafts perfect for you are ones that deeply fulfil you! Some kids love following tutorials and completing a project that reflects the "original" – typically to achieve a sense of accomplishment, whereas others prefer best set up their own way. All of this is alright and should be promoted.</p>
<p>In general, people perceive crafts as being more transparent than arts due to their emphasis on the finished product, but many believe that the formation process should be open- ended too. Thus, even if they are not, it makes room for something more important altogether-creative fulfillment.</p>
<h4><strong>Craft ideas for kids </strong></h4>
<p>With basic step-by-step instructions we have hundreds of fun craft ideas for you and your children. From basic preparation crafts for you and your children or pupils, which take minutes for making them (and are suitable for a classroom) to the more drawn-out ones that need a lot of materials-we have something for everyone!</p>
<p>Take a look at some of the most popular craft ideas for kids at home:</p>
<h4><strong><a href="https://mymodernmet.com/history-of-origami-definition/#:~:text=Origami%20is%20the%20art%20of,taping%2C%20or%20even%20marking%20it." target="_blank" rel="noopener">Origami</a> </strong></h4>
<p>You can transform a regular sheet of paper into origami within no time. This has got to be one of the most frugally constructive materials to be lying around, even when you typically use standard origami paper. Origami is excellent for children to create because it helps develop fine motor skills, patience and making it especially fun.</p>
<h4><strong>Paper Rolls – Crafts using Toilet Paper Rolls, Paper Kitchen Towel Rolls etc. </strong></h4>
<p>Toilet paper roll crafts, is an umbrella term used for all kinds of paper rolls, you can even buy separate crafting rolls <a href="#" target="_blank" rel="noopener">from craft stores</a> if you cannot find toilet paper rolls. You can even cut your typical kitchen towel rolls to smaller rolls for crafting purposes. There are many cool things you can make by using paper rolls –from pen stands to bangle holders and whatever else your imagination comes up with!</p>
<h4><strong>Rock Arts and Crafts for Kids </strong></h4>
<p>Rocks are another beautiful material to craft with!&nbsp; You can forage for rocks on your next nature stroll, in the park or if you live near the river. If that is not an alternative for you, then in most online and regular shops, artificial rocks are available for craftwork.</p>
<h4><strong>Crafting with Paper Plates </strong></h4>
<p>Paper plates are thrifty, making them a splendid material for the practice of crafting at home. You can create wonderful creations with paper plates in a million ways.</p>
<h4><strong>Craft ideas for adults </strong></h4>
<p>Why do children get all the fun in the world of art and crafts? As a means of relaxing, imagination and manifesting inner creativity, adults may benefit from craft too. Alternatively, adult crafts can deliver amazing handmade outcomes as gifts, wardrobes, home decor, parties and entertainment, and so on. (Adult-coloring books have risen into popularity for just this valid reason.) You are not required to be an expert at crafting: Whether you are a beginner or a professionally skilled craftsman, we have gathered the best&nbsp; craft ideas from different sources to stimulate your imagination.</p>
<h4><strong>Paper Chain Wall hanging</strong></h4>
<p>This project is rooted in a basic notion of the paper chain that you may recall from your early childhood days. But each link is a sophisticated look at the idea, which is made to be rendered and admired against a colorful wall.</p>
<h4><strong>Colorful Coupe Cocktail Glasses </strong></h4>
<p>You can make those attractive yet expensive drink glasses at home with only a bit of scrap vinyl adhesive tape and some inexpensive cocktail glasses. This project is exceedingly easy, and yet your cocktail guests would definitely appreciate a sparkling outcome.</p>
<h4><strong>Graphic Art String </strong></h4>
<p>The graphic string craft item casts a cool shadow effect obtained with different shades of embroidery thread. This basic graphic string art item is severely easy to make too! You can make this item within just an hour to send home the most needed message of the hour- RELAX!</p>
<h4><strong>Balloon Garland </strong></h4>
<p>You will need to understand how to create one of the latest trends: balloon hanging, if you are planning to be a crafty host/hostess. It is very easy to produce this item with just a few products, and it is a nice decoration punch on a wall. Use any scale, form and colors to make your own balloon hanging.</p>
<h4><strong>DIY Hanging Shelf </strong></h4>
<p>A shelf can be available for just about any function, and the small craft DIY shelf is used for a lot of purposes: in the toilet, bedroom, kitchen or lounge above a small table.&nbsp; You need only a piece of wood, and a drill to create it. Adjust it to the size and specification you want.</p>
<h4><strong>To summarize </strong></h4>
<p>Art and craft are the building blocks of growth for kids and adults alike, helping to create an assessment of their artistic aesthetics. Art and craft aid children with their general growth. It helps make language learning simpler by encouraging new vocabulary to be picked up and their language to evolve.</p>

					</div>
			</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="recent-section">
					<div class="recent-title">
						<strong>Recent Posts</strong>
					</div>
					<div class="recent-des">
						<div class="recent-img"><a href="/blog.aspx"><img src="images/blog/blog1.jpeg" /></a>
							
						</div>
						<div class="recent-tle">
							<a href="/blog.aspx">Functional planners and journals for women </a>
						</div>
						<div class="blog-date">January 27,2021</div>
					</div>
					<div class="recent-des">
							<div class="recent-img"><a href="/blog3.aspx"><img src="images/blog/blog3.jpeg" /></a></div>
						<div class="recent-tle">
							<a href="/blog3.aspx">BACK TO SCHOOL SUPPLIES LIST 2021</a>
						</div>
						<div class="blog-date">January 20,2021</div>
					</div>
				
				
					<div class="recent-des">
							<div class="recent-img"><a href="/blog4.aspx"><img src="images/blog/blog4.jpeg" /></a></div>
						<div class="recent-tle">
							<a href="/blog4.aspx">FUN ART AND CRAFT DIY IDEAS TO DO AT HOME</a>
						</div>
						<div class="blog-date">January 13,2021</div>
					</div>
				
				
					<div class="recent-des">
							<div class="recent-img"><a href="/blog2.aspx"><img src="images/blog/blog2.jpeg" /></a></div>
						<div class="recent-tle">
							<a href="/blog2.aspx">EVERYTHING YOU NEED TO KNOW ABOUT MANDALA</a>
						</div>
						<div class="blog-date">November  18,2020</div>
					</div>
				
				</div>
			</div>
		</div>
	</div>
</asp:Content>

