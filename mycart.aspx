﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master"  EnableEventValidation="false" AutoEventWireup="true" CodeFile="mycart.aspx.cs" Inherits="mycart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="customcss/mycart.css" rel="stylesheet" />
    <link href="customcss/images_size.css" rel="stylesheet" />
    <script src="js/customjs/mycart.js"></script>

	<div class="container-fluid" id="emptycart" style="display:none">
		<div class="responsiveBasket_empty noItems empty-cart">
			<h1 class="responsiveBasket_headerTitle responsiveBasket_headerTitle-empty">Your Shopping Basket</h1>
			<p class="responsiveBasket_emptyBasketMessage">
			There are currently no items in your cart.
			</p>

			<div class="responsiveBasket_emptyButtonContainer">
			<a href="/index.aspx" class="responsiveBasket_emptyContinueShoppingButton checkout-btn">Continue Shopping</a>
			</div>
		</div>
	</div>
    	<div id="filledcart" style="display:none">
	<div class="container-fluid" >
		<div class="responsiveBasket_header cart-header">
				<h1 class="responsiveBasket_headerTitle">Your Shopping Cart</h1>
				
	        <asp:LinkButton ID="lnk_btn_checkout1" runat="server" class="responsiveBasket_basketButton responsiveBasketV2_basketButton responsiveBasket_basketButton-top checkout-btn" data-js-element="responsiveBasket_modalTriggerConditions" OnClick="lnk_btn_checkout_Click"><i class="fa fa-lock" aria-hidden="true"></i>

				Checkout Securely Now</asp:LinkButton>
			
		</div>
	</div>
<div class="container-fluid" >
	<div class="responsiveBasket_basket responsiveBasketV2_basket ">
	<div clasas="responsiveBasket_headerContainer  responsiveBasket_headerContainer-done" data-js-element="responsiveBasket_headerContainer">
	<div class="responsiveBasket_head responsiveBasket_row">
	<div class="responsiveBasket_headItem responsiveBasket_headItem-descriptionTitle">
	Items
	</div>
	<div class="responsiveBasket_headItem responsiveBasket_headItem-unitPriceTitle">
	Price
	</div>
	<div class="responsiveBasket_headItem responsiveBasket_headItem-quantityTitle">
	Quantity
	</div>
	<div class="responsiveBasket_headItem responsiveBasket_headItem-subTotalTitle">
	Subtotal
	</div>
	<div class="responsiveBasket_headItem responsiveBasket_removeColumn">
	&nbsp;
	</div>
	</div>
	<ul aria-label="" id="cartcontainer">

	</ul>

	<div data-js-element="responsiveBasket_basket_freeGift"></div>
<%--	<div class="responsiveBasket_freeGift_spinner"></div>--%>
	</div>

	<div class="responsiveBasket_foot ">
	<div class="responsiveBasket_total responsiveBasket_row">
	<div class="responsiveBasket_totalRewardPoints">
	</div>
		<div class="grand-total-section">
		<div class="sub-total-section">
			<span class="responsiveBasket_totalLabel">Sub Total: </span>
			<span class="responsiveBasket_totalValue " id="ltCartSubTotal"  style="font-weight: bold;">	</span>
		</div>

		<div class="delivery-section">
            <span class="responsiveBasket_totalLabel">Delivery Charges: </span>
			<span class="responsiveBasket_totalValue " id="ltCartDeliveryCharges"  style="font-weight: bold;"> 	</span>
		</div>
                    <div id="trPointsRedeemval" style="display:none">
                <span class="responsiveBasket_totalLabel">Points Discount: </span>
				 <span class="responsiveBasket_totalValue " id="ltPointsRedeemval"  style="font-weight: bold;"> 	</span>
            </div>
                                <div id="trCouponVal" style="display:none">
                <span class="responsiveBasket_totalLabel">Coupon Discount: </span>
				 <span class="responsiveBasket_totalValue " id="ltCouponVal"  style="font-weight: bold;"> 	</span>
            </div>
        <div id="trDisAmt">
                <span class="responsiveBasket_totalLabel">Discount: </span>
				 <span class="responsiveBasket_totalValue " id="ltCartDisAmount"  style="font-weight: bold;"> 	</span>
            </div>
		<div class="grand-section">
	<span class="responsiveBasket_totalLabel">Grand Total: </span>
	<span class="responsiveBasket_totalValue baskettotal" id="baskettotal"  style="font-weight: bold;">

	</span>
</div>
		</div>
	<div class="responsiveBasket_removeColumn">&nbsp;</div>
	</div>
		<div id="PesOffers"></div>
        <div id="dvpoints"></div>
	<div class="responsiveBasket_row">

	<form class="responsiveBasket_discountEntryForm" action="my.basket" method="post" id="discount-form">
	<fieldset class="responsiveBasket_discountEntryFieldset">
	<legend class="responsiveBasket_discountEntryLegend">
	Discount Code
	</legend>
	<div class="responsiveBasket_discountInputGroup">
		<div class="add-coupn-section">
	<input class="responsiveBasket_discountEntryInput" autocomplete="off" name="discountCode" id="txtCouponCode" type="text" aria-label="Got a discount code? Enter it here:" placeholder="Got a discount code? Enter it here:">
	<button class="responsiveBasket_discountEntryButton add-discount-btn" type="button" id="add-discount-code"  aria-label="Add coupon code" title="Add coupon code">
	Add
	</button>
			</div>

		
	</div>
        
	</fieldset>
	</form>


	</div>
	<div class="responsiveBasket_basketButtons_flexRow">
	<div class="responsiveBasket_basketButtons_flexItem responsiveBasket_basketButtons_flexItem-continueShopping">
	<a class="responsiveBasket_basketButton-continueShopping responsiveBasket_basketButton-bottom continue-shop" href="/index.aspx">
	Continue Shopping
	</a>
	</div>
	<div class="responsiveBasket_basketButtons_flexItem responsiveBasket_basketButtons_flexItem-checkoutSecurely" data-js-element="basketButton">
	<form class="responsiveBasket_basketButton_form checkoutStartForm" action="/checkoutStart.account" method="post">
<%--	<button class="responsiveBasket_basketButton responsiveBasketV2_basketButton responsiveBasket_basketButton-top checkout-btn" data-js-element="responsiveBasket_modalTriggerConditions" type="submit">
				<i class="fa fa-lock" aria-hidden="true"></i>

				Checkout Securely Now
				</button>--%>
        <asp:LinkButton ID="lnk_btn_checkout2" runat="server" class="responsiveBasket_basketButton responsiveBasketV2_basketButton responsiveBasket_basketButton-top checkout-btn" data-js-element="responsiveBasket_modalTriggerConditions" OnClick="lnk_btn_checkout_Click"><i class="fa fa-lock" aria-hidden="true"></i>

				Checkout Securely Now</asp:LinkButton>
	</form>
	</div>
<%--	<div class="presentationalPaymentTypes" data-component="presentationalPaymentTypes">
	<span class="presentationalPaymentTypes_deviceState" data-device-state=""></span>
	<a data-payment-type="clearpay" href="checkoutStart.account?paymentOption=CLEARPAY" class="presentationalPaymentTypes_paymentTypeLink presentationalPaymentTypes_paymentTypeLink_clearpay presentationalPaymentTypes_paymentTypeLink-show" title="Check out with clearpay" aria-label="Check out with clearpay">
	<div class="presentationalPaymentTypes_paymentIcon">
	<div class="responsiveBasket_paymentIcon">
	<svg xmlns="http://www.w3.org/2000/svg" width="60" height="40" viewBox="0 0 60 40"><title>Clearpay</title><rect width="59" height="39" x=".5" y=".5" fill="#FFF" stroke="#DDD" rx="5"></rect><path d="M46.366 17.29l-3.695 7.632h-1.502l1.382-2.854-2.193-4.778h1.502l1.397 3.2 1.502-3.2zM32.305 19.995a1.502 1.502 0 10-1.502 1.502 1.502 1.502 0 001.502-1.502m-4.28 4.912V17.29h1.336v.707a2.118 2.118 0 011.698-.797 2.659 2.659 0 012.614 2.795 2.704 2.704 0 01-2.644 2.809 2.088 2.088 0 01-1.638-.736v2.854zM38.48 19.995a1.502 1.502 0 10-3.005 0 1.502 1.502 0 103.005 0m0 2.704v-.707a2.178 2.178 0 01-1.713.797 2.644 2.644 0 01-2.599-2.794 2.704 2.704 0 012.644-2.81 2.118 2.118 0 011.668.781v-.676h1.337v5.424zM25.62 17.831a1.427 1.427 0 011.758-.51v1.382a1.622 1.622 0 00-.961-.21.946.946 0 00-.751 1.036v3.215h-1.367V17.29h1.321zM56.702 16.99l-1.577-.901-1.593-.917a1.592 1.592 0 00-2.373 1.382v.196a.3.3 0 00.15.27l.751.436a.3.3 0 00.45-.27v-.481a.315.315 0 01.496-.286l1.503.841 1.502.842a.315.315 0 010 .54l-1.502.842-1.503.826a.315.315 0 01-.48-.27v-.24a1.592 1.592 0 00-2.389-1.308l-1.637.842-1.503.916a1.577 1.577 0 000 2.749l1.578.901 1.592.917a1.577 1.577 0 002.374-1.382v-.196a.3.3 0 00-.15-.27l-.737-.436a.315.315 0 00-.465.27v.481a.315.315 0 01-.481.286l-1.502-.841-1.503-.842a.315.315 0 010-.54l1.503-.842 1.502-.826a.315.315 0 01.48.27v.24a1.592 1.592 0 002.374 1.383l1.593-.917 1.577-.916a1.577 1.577 0 00-.03-2.749zM8.99 20.686a2.72 2.72 0 01-2.748 2.103 2.794 2.794 0 110-5.589 2.704 2.704 0 012.704 2.073H7.579a1.502 1.502 0 00-1.307-.78 1.502 1.502 0 000 3.004 1.397 1.397 0 001.307-.857zM9.622 22.699v-7.632h1.322V22.7zM13.032 20.355a1.367 1.367 0 001.397 1.262 1.502 1.502 0 001.277-.706h1.397a2.659 2.659 0 01-2.704 1.878 2.659 2.659 0 01-2.78-2.794 2.734 2.734 0 012.81-2.81 2.704 2.704 0 012.779 2.81 1.292 1.292 0 010 .345zm2.719-.841a1.292 1.292 0 00-1.337-1.127 1.337 1.337 0 00-1.367 1.127zM22.075 22.699v-.691a2.208 2.208 0 01-1.712.78 2.644 2.644 0 01-2.614-2.793 2.689 2.689 0 012.644-2.795 2.163 2.163 0 011.682.781v-.69h1.322v5.408zm0-2.704a1.502 1.502 0 10-1.502 1.502 1.502 1.502 0 001.487-1.502z" stroke-width="1.502"></path></svg>

	</div>

	</div>
	</a>
	<a data-payment-type="alipay" href="checkoutStart.account?paymentOption=ALIPAY" class="presentationalPaymentTypes_paymentTypeLink presentationalPaymentTypes_paymentTypeLink_alipay presentationalPaymentTypes_paymentTypeLink-show" title="Check out with alipay" aria-label="Check out with alipay">
	<div class="presentationalPaymentTypes_paymentIcon">
	<div class="responsiveBasket_paymentIcon">
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 40" width="60" height="40"><path d="M21.3 22.8v-9.6c0-1.8-1.4-3.2-3.2-3.2H5.7c-1.8 0-3.2 1.4-3.2 3.2v12.4c0 1.8 1.4 3.2 3.2 3.2h12.4c1.8 0 3.2-1.4 3.2-3.1-.2-.1-6.6-2.8-7.4-3.3-.3.4-2.5 3.3-6.3 3.3-1.5 0-3.7-1.1-3.7-3.2 0-3 2.9-3.5 3.7-3.5 2.5 0 5.6 1.2 5.6 1.2.8-1.4 1.1-2.7 1.1-2.7H7v-.7h3.8v-1.4H6.2v-.7h4.5l.1-1.8c0-.2 0-.4.4-.4h1.7v2.2h4.5v.7h-4.5v1.4h3.7s-.8 3-1.6 4c0 0 5.5 1.9 6.3 2zm-8.4-1.1s-2.3-1.5-4.7-1.5c-1.4 0-3 .5-3 2.2 0 1.4 1.8 2.1 3 2.1 2.7 0 4.7-2.8 4.7-2.8z" fill="#0ae"></path><path d="M24.5 12.9h3.8v-1.2s0-.1.1-.2.2-.1.2-.1h1.8v1.5h3.9v.7h-3.9v1.2h3.3s-.4 1.5-1.3 2.7c-1 1.1-1.8 1.7-1.8 1.7s.5.3 1.4.8c.9.5 2.4 1 2.4 1v.8s-1.4-.1-3.3-1c-1.1-.5-1.7-.9-1.7-.9s-.7.5-2 1c-2.2.9-3 .9-3 .9v-.9s1.1-.3 2.2-.7c1-.5 1.7-1 1.7-1s-1-.7-1.6-1.3c-.8-.9-1.2-1.6-1.2-1.6h1.7l1 1.2c.5.5 1.2.9 1.2.9s.4-.2 1.2-1.3c.8-1 .9-1.5.9-1.5h-6.4v-.8h3.2v-1.2h-3.8v-.7zm12-1.4h1.9l-1.2 2.8h.6v7.4h-1.7v-5.6h-1.5l1.9-4.6zm2 1.8h4.2v-1.7h1.8v1.7h.7v.8h-.7V21s0 .2-.2.4-.5.2-.5.2h-2.3v-.8h1s.1 0 .2-.1v-6.6h-4.2v-.8zm.5 1.8h1.6s.1 0 .3.5c.1.3.9 3 .9 3h-1.6L39 15.1zm7.4-2.7H50v-1h2.1v1h3.1s.2 0 .4.1c.1.1.1.3.1.3v1.4h-2v-1.1h-5.4v1.1h-1.9v-1.8zm0 2.4h9.3v.8h-3.8V17h3.8v.7h-3.8v3h1.9l-.5-1.6H55s.1 0 .2.2c.1.1.2.4.2.4l.5 1.8h-9.5v-.8h3.7v-3h-3.7V17h3.7v-1.4h-3.7v-.8zM25.5 27.5l1.6-4.4.6.1 1.6 4.3h-.8l-.3-.9h-1.5l-.3.9h-.9zm1.3-1.4l.6-1.8h.1l.5 1.8h-1.2zm19.4 1.4l1.5-4.4.6.1 1.7 4.3h-.9l-.3-.9h-1.5l-.3.9h-.8zm1.3-1.4l.5-1.8h.1l.6 1.8h-1.2zm-15.7-2.9v4.3h2.7v-.6h-1.8v-3.7h-.9zm5.5 0v4.3h.8v-4.3h-.8zm3.9 0v4.3h.8v-1.7h1.2s1.1-.1 1.1-1.3c0-1.3-1.1-1.3-1.1-1.3h-2zm.8.6v1.4h1s.5-.1.5-.7c0-.7-.5-.7-.5-.7h-1zm9.7-.6h.8l.9 1.8.8-1.8h.9l-1.3 2.5v1.8H53v-1.8l-1.3-2.5z" fill="#3f3a39"></path><path d="M56.2.8c1.7 0 3 1.4 3 3.2v32c0 1.8-1.3 3.2-3 3.2H3.7c-1.6 0-3-1.4-3-3.2V4C.7 2.2 2.1.8 3.7.8h52.5m0-.8H3.7C1.7 0 0 1.8 0 4v32c0 2.2 1.7 4 3.7 4h52.5c2.1 0 3.8-1.8 3.8-4V4c0-2.2-1.7-4-3.8-4" fill="#ddd"></path><path d="M26.8 26.1l.6-1.8h.1l.5 1.8zM42 23.8v1.4h1s.5-.1.5-.7c0-.7-.5-.7-.5-.7h-1zm5.5 2.3l.5-1.8h.1l.6 1.8z" fill="#fff"></path></svg>

	</div>

	</div>
	</a>
	<a data-payment-type="wechatpay" href="checkoutStart.account?paymentOption=WECHATPAY" class="presentationalPaymentTypes_paymentTypeLink presentationalPaymentTypes_paymentTypeLink_wechatpay presentationalPaymentTypes_paymentTypeLink-show" title="Check out with wechatpay" aria-label="Check out with wechatpay">
	<div class="presentationalPaymentTypes_paymentIcon">
	<div class="responsiveBasket_paymentIcon">
	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="60" height="40" viewBox="0 0 60 40">
	<defs>
	<path id="a" d="M0 .105h13.074v11.21H0z"></path>
	<path id="c" d="M0 11.32h51.772V.11H0z"></path>
	</defs>
	<g fill="none" fill-rule="evenodd" transform="translate(-300 -370)">
	<g transform="translate(304 385.5)">
	<g transform="translate(0 .006)">
	<path fill="#22AB39" d="M4.755 7.195a.444.444 0 01-.576-.17l-.029-.061-1.19-2.538a.214.214 0 01-.02-.09c0-.116.097-.21.217-.21.05 0 .094.015.13.041l1.405.972a.664.664 0 00.58.064l6.605-2.856C10.693.99 8.743.105 6.537.105 2.927.105 0 2.475 0 5.399 0 6.994.88 8.429 2.259 9.4a.42.42 0 01.16.48l-.295 1.068c-.013.05-.035.102-.035.155 0 .116.098.211.218.211a.251.251 0 00.126-.04l1.431-.802a.7.7 0 01.347-.098c.067 0 .131.01.192.028a7.93 7.93 0 002.134.29c3.61 0 6.537-2.37 6.537-5.293 0-.886-.27-1.72-.744-2.453L4.803 7.169l-.048.026z"></path>
	</g>
	<path fill="#232222" d="M28.812 4.312h4.904v-.437h-4.904zm0 1.358h4.904v-.46h-4.904zm.599 3.06h3.707V7.005h-3.707V8.73zm-.474.427h4.655V6.578h-4.655v2.579zm-3.047-3.8l.261.437c.276-.397.567-.863.842-1.352v4.983h.474V3.491c.278-.582.524-1.21.725-1.845l-.439-.246c-.433 1.482-1.067 2.83-1.863 3.957m17.817 0l.262.437c.276-.397.567-.863.84-1.352v4.983h.475V3.491c.278-.582.524-1.21.725-1.845L45.57 1.4c-.432 1.482-1.067 2.83-1.863 3.957M31.656 2.336l.01-.006-.004-.011c-.035-.08-.127-.267-.224-.465-.09-.184-.184-.375-.229-.472l-.006-.014-.437.245.006.012c.157.286.291.548.411.801a.343.343 0 01.032.08h-2.883v.462h5.9v-.461H31.35l.306-.171zm-10.079-.179h-.44v1.502h-.83V1.523h-.44V3.66h-.865V2.157h-.44v1.93h3.015zM18.7 5.364h2.671v-.437H18.7zm3.258 2.25l-.212-.357-.009-.014-.674.562v-1.61h-2.26v.648c.064.888-.164 1.523-.678 1.888l-.01.007.244.412.009.014.012-.01c.64-.513.942-1.234.897-2.145v-.387h1.346v.954c-.017.332-.034.434-.152.593l-.005.007.27.454.013-.01c.237-.184.64-.52 1.2-.999l.009-.007zm-3.605-5.949l-.41-.23c-.424.84-.956 1.563-1.553 2.107l.265.447a8.723 8.723 0 001.698-2.324m-.045 2.192l-.425-.237-.006.014a8.912 8.912 0 01-1.6 2.586l-.007.007.244.41.01.017.012-.015c.26-.294.514-.605.756-.924v3.71h.44V5.011c.198-.344.39-.729.571-1.143l.005-.01z"></path>
	<path fill="#232222" d="M23.882 3.418c-.09 1.393-.328 2.51-.705 3.322-.29-.725-.505-1.712-.64-2.934.044-.128.1-.258.166-.388h1.18zm-1.03-.427c.169-.496.363-1.074.428-1.568h-.459c-.285 1.3-.721 2.428-1.282 3.319l.277.467.204-.396c.077-.13.139-.24.186-.328.137 1.152.377 2.079.714 2.756-.323.543-.82 1.106-1.477 1.676l-.009.007.254.426.009.015.012-.012c.696-.633 1.178-1.188 1.433-1.649.29.502.781 1.11 1.352 1.674l.013.013.26-.439.006-.009-.008-.007c-.612-.568-1.098-1.158-1.368-1.66.499-.885.81-2.183.925-3.858h.481v-.427h-1.952zm18.394 1.892s-.483 1.344-2.138 2.491c-.788-.583-1.6-1.44-2.122-2.49h4.26zM39.4 1.355h-.48V2.51h-3.714v.456h3.714v1.517h-2.408V4.9s.37.81 1.118 1.69c.3.352.656.721 1.11 1.084-.844.505-1.9.984-3.365 1.26l.277.467s1.738-.36 3.466-1.448c.233.144 1.3.973 3.297 1.436h.006l.008.003.194-.36s-.515-.131-1.296-.424a9.55 9.55 0 01-1.872-.938 6.81 6.81 0 002.351-2.968l-.39-.219H39.4V2.966h3.747V2.51H39.4V1.355zM50.498 3.41V1.434h-.476V3.41h-3.911v.46h3.911v4.282c0 .465-.296.683-.855.683l-.275-.001v.46l.414-.001c.465 0 .72-.097.918-.29.193-.188.285-.475.274-.852V3.87h1.274v-.46h-1.274z"></path>
	<path fill="#232222" d="M47.14 5.314c.289.548.612 1.217.966 1.996l.47-.267c-.251-.499-.589-1.16-1.006-1.973l-.43.244z"></path>
	</g>
	<rect width="59" height="39" x="300.5" y="370.5" stroke="#DDD" rx="5"></rect>
	</g>
	</svg>

	</div>

	</div>
	</a>
	<a data-payment-type="laybuy" href="checkoutStart.account?paymentOption=LAYBUY" class="presentationalPaymentTypes_paymentTypeLink presentationalPaymentTypes_paymentTypeLink_laybuy presentationalPaymentTypes_paymentTypeLink-show" title="Check out with laybuy" aria-label="Check out with laybuy">
	<div class="presentationalPaymentTypes_paymentIcon">
	<div class="responsiveBasket_paymentIcon">
	<svg xmlns="http://www.w3.org/2000/svg" width="61" height="41" viewBox="0 0 61 41">
	<g fill="none" fill-rule="evenodd">
	<rect width="59" height="39" x="1" y="1" fill="#FFF" fill-rule="nonzero" stroke="#DDD" rx="4"></rect>
	<g transform="translate(3 14)">
	<path fill="#000" fill-rule="nonzero" d="M22.7926073,8.59004995 L19.445547,8.59004995 L19.445547,3.20798111 C19.4377158,3.09095052 19.3403074,3.00001803 19.2227735,3.00001803 C19.1052396,3.00001803 19.0078312,3.09095052 19,3.20798111 L19,8.77819955 C19.0026285,8.89960345 19.1006223,8.99739505 19.2222774,9.00001803 L22.7945919,9.00001803 C22.9080357,9.00001803 23,8.90824355 23,8.79503395 C23,8.68182445 22.9080357,8.59004995 22.7945919,8.59004995 L22.7926073,8.59004995 Z M27.3217958,3.21133198 C27.2772546,3.08523949 27.1531631,3.00022223 27.0133653,3.00001803 L26.9958876,3.00001803 C26.8558841,2.99961446 26.7314578,3.08486105 26.6874572,3.21133198 L24.0349554,8.67998395 C24.012613,8.72185165 24.0006549,8.76803605 24,8.81498825 C24.0060924,8.92152645 24.1007635,9.00377735 24.212817,8.99988545 C24.315458,8.99848205 24.4053123,8.93396045 24.4348869,8.84042385 L25.1617546,7.31233182 L28.8207676,7.31233182 L29.5476354,8.82770605 C29.5824595,8.92234425 29.6728545,8.98839185 29.7779301,8.99597225 C29.8354679,8.99728505 29.8911956,8.97678665 29.9328454,8.93898955 C29.9744952,8.90119245 29.9986527,8.84919505 30,8.79444415 C29.9999377,8.74730185 29.9875732,8.70091205 29.9640164,8.65943985 L27.3217958,3.21133198 Z M25.3560658,6.91612358 L27.0010281,3.49797152 L28.6326251,6.91612358 L25.3560658,6.91612358 Z M34.783933,3.00001803 C34.7034653,3.00530204 34.6314053,3.05337139 34.5935656,3.12700719 L32.4995241,6.22692092 L30.4188083,3.12897602 C30.3800668,3.05300883 30.3047867,3.00448958 30.221778,3.00198686 C30.1031483,3.00338826 30.0060247,3.09995587 30,3.22249517 C30.0021287,3.27066327 30.0162251,3.31745099 30.040929,3.35834404 L32.2825052,6.62757665 L32.2825052,8.79328325 C32.2900171,8.90962265 32.3834525,9.00001803 32.4961927,9.00001803 C32.6089328,9.00001803 32.7023682,8.90962265 32.7098801,8.79328325 L32.7098801,6.61773253 L34.9343232,3.38196993 C34.9714508,3.33320496 34.9942476,3.27443333 35,3.21265105 C34.9916938,3.09479152 34.8981642,3.00274844 34.783933,3.00001803 Z M39.8962546,5.86042719 C40.3157557,5.61999305 40.6680977,5.24344535 40.6680977,4.5688388 L40.6680977,4.55194879 C40.6784946,4.16945512 40.5325087,3.79979231 40.265143,3.53159396 C39.9293475,3.18882627 39.4008345,3.00001803 38.7292433,3.00001803 L36.6463375,3.00001803 C36.4743641,2.9987164 36.3090674,3.06792745 36.1875518,3.19215282 C36.0660363,3.31637819 35.9984819,3.48521096 36,3.6607528 L36,8.34027695 C35.9987442,8.51564725 36.0664144,8.68422265 36.1879011,8.80823175 C36.3093879,8.93224095 36.4745343,9.00131625 36.6463375,9.00001803 L38.8002957,9.00001803 C40.1269315,9.00001803 41,8.45155225 41,7.35469562 L41,7.33681209 C40.9961067,6.52907257 40.5766056,6.12569958 39.8962546,5.86042719 Z M37.2595294,4.15453698 L38.5355525,4.15453698 C39.0815853,4.15453698 39.384288,4.3770876 39.384288,4.77151882 L39.384288,4.78840882 C39.384288,5.23450359 39.0231861,5.4232742 38.451847,5.4232742 L37.2595294,5.4232742 L37.2595294,4.15453698 Z M39.7113237,7.18877619 C39.7113237,7.63387742 39.3667682,7.83953803 38.7964024,7.83953803 L37.2595294,7.83953803 L37.2595294,6.52013081 L38.7545497,6.52013081 C39.4173809,6.52013081 39.7113237,6.76851319 39.7113237,7.17188618 L39.7113237,7.18877619 Z M46.3634646,3.00010748 C46.1957733,2.99880415 46.0345795,3.06620513 45.9160005,3.18720844 C45.7974214,3.30821175 45.7313708,3.47270106 45.732648,3.64382062 L45.732648,6.40383764 C45.732648,7.33159889 45.2661858,7.80856706 44.4976156,7.80856706 C43.7290454,7.80856706 43.2625832,7.31511828 43.2625832,6.3621514 L43.2625832,3.64769841 C43.2494248,3.3016353 42.9706766,3.02801084 42.6312916,3.02801084 C42.2919065,3.02801084 42.0131583,3.3016353 42,3.64769841 L42,6.3989904 C42,8.12169865 42.9433748,9.00001803 44.4833652,9.00001803 C46.0233556,9.00001803 47.0000182,8.12751525 47.0000182,6.35730416 L47.0000182,3.64382062 C47.0012848,3.47167997 46.9344558,3.30628712 46.8146338,3.18511039 C46.6948117,3.06393366 46.5321379,2.99722889 46.3634646,3.00010748 L46.3634646,3.00010748 Z M53.4178845,3.00087695 C53.1699007,3.00087695 52.9919994,3.15205023 52.8446467,3.39100155 L51.5040968,5.46939037 L50.1860093,3.40465591 C50.0386566,3.16960584 49.8679431,3.00965475 49.5965986,3.00965475 C49.4412543,3.00547057 49.2908148,3.06886788 49.1787641,3.18573607 C49.0667135,3.30260426 49.0023502,3.46324488 49,3.63190349 C49.0012909,3.79963329 49.0530339,3.96231187 49.1473527,4.09517646 L50.8994118,6.67780133 L50.8994118,8.35241115 C50.8994118,8.71007455 51.1665181,9.00001803 51.4960104,9.00001803 C51.8255027,9.00001803 52.092609,8.71007455 52.092609,8.35241115 L52.092609,6.65244323 L53.8446681,4.08639866 C53.937818,3.95563877 53.9917595,3.79699988 53.9992087,3.63190349 C54.0075928,3.46191788 53.9490435,3.29607145 53.8381543,3.17570133 C53.727265,3.0553312 53.5744813,2.99177605 53.4178845,3.00087695 L53.4178845,3.00087695 Z"></path>
	<path fill="#786DFF" d="M8.83457203,1.07768319 L7.48166009,2.41314348 C7.3879032,2.50569097 7.25289328,2.41314348 7.25289328,2.41314348 L5.51839078,0.672325178 C4.95860176,0.123394128 4.05490609,0.123394128 3.49511707,0.672325178 L3.49511707,0.672325178 C2.93798637,1.22447526 2.93798637,2.11734995 3.49511707,2.66950003 L6.36876579,5.5070061 C6.92866429,6.05655035 7.83307857,6.05655035 8.39297708,5.5070061 L10.7978413,3.13316296 L11.2188098,2.70651903 C11.3573853,2.56958374 11.5454236,2.49264189 11.7415045,2.49264189 C11.9375854,2.49264189 12.1256236,2.56958374 12.2641991,2.70651903 C12.4016252,2.83969968 12.4784978,3.02217579 12.477261,3.21227429 C12.4760243,3.40237279 12.3967838,3.58385927 12.2576361,3.71528668 L11.8235417,4.14655798 L7.45540816,8.46019653 C7.42138066,8.49598546 7.37387188,8.51628721 7.32414851,8.51628721 C7.27442514,8.51628721 7.22691636,8.49598546 7.19288886,8.46019653 L2.44128961,3.7698897 C1.88117988,3.22085107 0.977188044,3.22085107 0.417078318,3.7698897 L0.417078318,3.7698897 C-0.139026106,4.32245782 -0.139026106,5.21449643 0.417078318,5.76706455 L6.34157629,11.6151405 L6.31063652,11.5883017 C6.87042554,12.1372327 7.77412121,12.1372327 8.33391023,11.5883017 L13.8496282,6.14373283 L13.9077574,6.08635339 C14.838538,5.19673598 15.2111673,3.88090098 14.8826474,2.64380515 C14.5541274,1.40670931 13.5754886,0.440515698 12.3222872,0.11600484 C11.0690859,-0.208506022 9.73598686,0.159073438 8.83457203,1.07768319 L8.83457203,1.07768319 Z"></path>
	</g>
	</g>
	</svg>

	</div>

	</div>
	</a>
	<a data-payment-type="openpay" href="checkoutStart.account?paymentOption=OPENPAY" class="presentationalPaymentTypes_paymentTypeLink presentationalPaymentTypes_paymentTypeLink_openpay presentationalPaymentTypes_paymentTypeLink-show" title="Check out with openpay" aria-label="Check out with openpay">
	<div class="presentationalPaymentTypes_paymentIcon">
	<div class="responsiveBasket_paymentIcon">
	<svg width="60" height="41" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><rect x=".5" y=".5" width="59" height="39" rx="4" fill="#FFF" stroke="#DDD" transform="translate(0 .5)"></rect><g transform="translate(9.5 10.829)" fill-rule="nonzero"><rect fill="#FFB81C" width="41.2" height="20.6" rx="4"></rect><path d="M23.524 14.43v-1.321h1.292v1.32h-1.292zm-16.29 0v-1.321h1.291v1.32H7.234zm29.148 0v-1.321h1.291v1.32h-1.291zm.008-1.76v-.442a2.447 2.447 0 01-1.751-2.36V7.295h1.28v2.573a1.114 1.114 0 102.226 0V7.295h1.29v2.574a2.447 2.447 0 01-1.752 2.36v.442H36.39zm-12.866 0V9.688a2.596 2.596 0 012.558-2.618 2.564 2.564 0 012.405 1.715 2.656 2.656 0 01-.75 2.9c-.815.71-1.99.822-2.923.278v.706l-1.29.003zm1.28-2.99A1.298 1.298 0 0026.081 11a1.313 1.313 0 001.288-1.306 1.298 1.298 0 00-1.277-1.32h-.007a1.295 1.295 0 00-1.28 1.306zm-17.57 2.99V9.687A2.596 2.596 0 019.79 7.07a2.564 2.564 0 012.407 1.717 2.657 2.657 0 01-.75 2.898c-.814.71-1.99.821-2.923.277v.706l-1.291.003zm1.28-2.99A1.298 1.298 0 009.79 11a1.313 1.313 0 001.29-1.305 1.298 1.298 0 00-1.276-1.32h-.007A1.295 1.295 0 008.514 9.68zm-6.748.012a2.596 2.596 0 012.566-2.624A2.596 2.596 0 016.9 9.691a2.603 2.603 0 01-2.567 2.624 2.596 2.596 0 01-2.566-2.623zm1.283 0a1.295 1.295 0 001.283 1.314 1.312 1.312 0 000-2.624 1.295 1.295 0 00-1.283 1.31zm26.727 1.854a2.662 2.662 0 01-.412-3.149 2.54 2.54 0 012.86-1.24 2.608 2.608 0 011.924 2.496v2.53h-1.291v-.217a2.518 2.518 0 01-3.08-.422l-.001.002zm.9-2.784a1.32 1.32 0 00.916 2.24c.342.004.67-.132.91-.377.244-.247.38-.58.38-.927v-.045c-.02-.7-.589-1.26-1.29-1.27h-.012c-.34 0-.666.136-.904.38zm-15.408 3.549a2.59 2.59 0 01-2.56-2.414 2.613 2.613 0 012.144-2.8 2.568 2.568 0 012.905 1.96c.09.44.084.895-.02 1.333h-3.59l.022.034a1.295 1.295 0 001.752.43l.897.917c-.44.347-.983.536-1.543.537l-.007.003zm-1.152-3.27l-.018.032h2.338l-.015-.033a1.3 1.3 0 00-2.303 0h-.002zm7.642 3.137V9.522a1.115 1.115 0 10-2.229 0v2.656h-1.29V9.522a2.429 2.429 0 012.4-2.453 2.429 2.429 0 012.4 2.453v2.656h-1.28z" fill="#333332"></path></g></g></svg>
	</div>

	</div>
	</a>
	<a data-payment-type="splitit" href="checkoutStart.account?paymentOption=SPLITIT" class="presentationalPaymentTypes_paymentTypeLink presentationalPaymentTypes_paymentTypeLink_splitit presentationalPaymentTypes_paymentTypeLink-show" title="Check out with splitit" aria-label="Check out with splitit">
	<div class="presentationalPaymentTypes_paymentIcon">
	<div class="responsiveBasket_paymentIcon">
	</div>

	</div>
	</a>
	<a data-payment-type="googlepay" href="checkoutStart.account?paymentOption=GOOGLEPAY" class="presentationalPaymentTypes_paymentTypeLink presentationalPaymentTypes_paymentTypeLink_googlepay presentationalPaymentTypes_paymentTypeLink-show" title="Check out with googlepay" aria-label="Check out with googlepay">
	<div class="presentationalPaymentTypes_paymentIcon">
	<div class="responsiveBasket_paymentIcon">
	<svg width="60" height="38" viewBox="0 0 425 272" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><path d="M386.731.09H38.04c-1.453 0-2.908 0-4.358.007-1.224.01-2.445.024-3.67.057-2.666.072-5.355.228-7.988.703-2.674.48-5.164 1.264-7.591 2.5A25.465 25.465 0 0 0 3.267 14.516c-1.237 2.426-2.022 4.914-2.5 7.59-.477 2.633-.634 5.318-.705 7.98a174.803 174.803 0 0 0-.057 3.665C-.003 35.203 0 36.655 0 38.11v195.73c0 1.454-.003 2.903.005 4.357.008 1.221.023 2.445.057 3.665.071 2.66.228 5.348.705 7.98.478 2.674 1.263 5.162 2.5 7.59a25.451 25.451 0 0 0 4.696 6.464 25.4 25.4 0 0 0 6.47 4.693c2.427 1.239 4.917 2.024 7.59 2.506 2.634.47 5.323.628 7.99.7 1.224.028 2.445.046 3.67.051 1.45.01 2.904.01 4.357.01h348.69c1.45 0 2.905 0 4.355-.01 1.222-.005 2.443-.023 3.672-.051 2.662-.072 5.351-.23 7.99-.7 2.67-.482 5.16-1.267 7.59-2.506a25.432 25.432 0 0 0 6.467-4.693 25.576 25.576 0 0 0 4.696-6.463c1.24-2.43 2.025-4.917 2.5-7.592.477-2.631.631-5.319.703-7.979.033-1.22.049-2.444.056-3.665.01-1.454.01-2.903.01-4.357V38.11c0-1.454 0-2.906-.01-4.358a174.8 174.8 0 0 0-.056-3.665c-.072-2.662-.226-5.347-.703-7.98-.475-2.676-1.26-5.164-2.5-7.59a25.59 25.59 0 0 0-4.696-6.465 25.556 25.556 0 0 0-6.467-4.694c-2.43-1.236-4.92-2.02-7.59-2.5-2.639-.475-5.328-.631-7.99-.703-1.229-.033-2.45-.046-3.672-.057-1.45-.007-2.905-.007-4.355-.007" fill="#3C4043"></path><path d="M386.731 9.148l4.29.008c1.16.008 2.323.02 3.49.054 2.033.054 4.412.164 6.63.561 1.924.347 3.541.875 5.091 1.662a16.396 16.396 0 0 1 4.157 3.019 16.512 16.512 0 0 1 3.039 4.18c.785 1.54 1.309 3.145 1.655 5.084.395 2.193.506 4.575.56 6.62.033 1.15.048 2.305.053 3.487.01 1.429.01 2.855.01 4.286V233.84c0 1.431 0 2.857-.01 4.314-.005 1.154-.02 2.308-.053 3.465-.054 2.039-.165 4.421-.565 6.637-.341 1.913-.865 3.519-1.655 5.065a16.38 16.38 0 0 1-3.028 4.168 16.308 16.308 0 0 1-4.178 3.032c-1.54.784-3.152 1.313-5.058 1.654-2.264.402-4.74.515-6.606.564-1.173.028-2.34.044-3.539.049-1.424.01-2.856.01-4.283.01H37.983c-1.409 0-2.823 0-4.257-.01a190.26 190.26 0 0 1-3.467-.046c-1.91-.052-4.388-.165-6.631-.565-1.925-.343-3.537-.872-5.097-1.667a16.233 16.233 0 0 1-4.162-3.026 16.262 16.262 0 0 1-3.024-4.163c-.787-1.543-1.313-3.154-1.66-5.09-.398-2.214-.508-4.584-.564-6.612a174.96 174.96 0 0 1-.052-3.473l-.005-3.406V37.211l.005-3.398c.008-1.162.02-2.321.052-3.48.056-2.032.166-4.404.57-6.635.34-1.916.867-3.53 1.657-5.08a16.37 16.37 0 0 1 3.026-4.16 16.372 16.372 0 0 1 4.17-3.027c1.542-.788 3.159-1.313 5.084-1.66 2.217-.397 4.596-.507 6.633-.561a155.62 155.62 0 0 1 3.475-.054l4.304-.008h348.69" fill="#FFFFFE"></path><g fill="#3C4043"><path d="M204.506 142.25v38.733h-12.102V85.368h32.097c7.742-.152 15.259 2.822 20.746 8.311 10.974 10.37 11.65 27.831 1.353 39.04l-1.353 1.372c-5.638 5.413-12.553 8.158-20.746 8.158h-19.995zm0-45.14v33.397h20.295c4.51.153 8.87-1.677 11.952-4.956 6.314-6.633 6.164-17.308-.376-23.713-3.082-3.05-7.216-4.727-11.576-4.727h-20.295zm77.347 16.318c8.945 0 16.011 2.44 21.198 7.243 5.186 4.804 7.742 11.514 7.742 19.977v40.335h-11.576v-9.073h-.526c-5.036 7.472-11.651 11.208-19.995 11.208-7.065 0-13.079-2.135-17.814-6.405a20.346 20.346 0 0 1-7.141-16.012c0-6.786 2.555-12.123 7.592-16.164 5.036-4.041 11.801-6.024 20.22-6.024 7.216 0 13.079 1.373 17.74 3.965v-2.821a14.42 14.42 0 0 0-4.962-10.903c-3.232-2.898-7.366-4.5-11.65-4.5-6.766 0-12.103 2.898-16.011 8.693l-10.674-6.786c5.713-8.463 14.357-12.733 25.857-12.733zM266.22 160.93c0 3.203 1.503 6.176 3.983 8.006 2.706 2.135 6.014 3.279 9.396 3.203 5.112 0 9.998-2.06 13.606-5.719 3.984-3.812 6.013-8.311 6.013-13.496-3.758-3.05-9.02-4.575-15.785-4.499-4.886 0-9.02 1.22-12.328 3.584-3.232 2.364-4.885 5.338-4.885 8.921z" fill-rule="nonzero"></path><path d="M377.241 115.563l-40.44 94.166h-12.478l15.034-32.94-26.535-61.226h13.155l19.167 46.968h.226l18.717-46.968z"></path></g><path d="M155.572 133.862c0-3.736-.3-7.472-.902-11.132h-51.039v21.12h29.24c-1.202 6.787-5.11 12.887-10.824 16.699v13.725h17.44c10.222-9.531 16.085-23.637 16.085-40.412z" fill="#4285F4"></path><path d="M103.631 187.54c14.583 0 26.91-4.88 35.855-13.266l-17.439-13.725c-4.886 3.355-11.124 5.261-18.416 5.261-14.131 0-26.083-9.683-30.367-22.646H55.299v14.183c9.17 18.528 27.887 30.194 48.332 30.194z" fill="#34A853"></path><path d="M73.264 143.164c-2.255-6.786-2.255-14.182 0-21.044v-14.106H55.299a55.131 55.131 0 0 0 0 49.256l17.965-14.106z" fill="#FBBC04"></path><path d="M103.631 99.474c7.743-.152 15.184 2.821 20.747 8.235l15.484-15.707c-9.847-9.302-22.776-14.41-36.23-14.258-20.446 0-39.163 11.742-48.333 30.27l17.965 14.182c4.284-13.038 16.236-22.722 30.367-22.722z" fill="#EA4335"></path></g></svg>

	</div>

	</div>
	</a>
	<a data-payment-type="paypal" href="checkoutStart.account?paymentOption=PAYPAL" class="presentationalPaymentTypes_paymentTypeLink presentationalPaymentTypes_paymentTypeLink_paypal presentationalPaymentTypes_paymentTypeLink-show" title="Check out with paypal" aria-label="Check out with paypal">
	<div class="presentationalPaymentTypes_paymentIcon">
	<div class="responsiveBasket_paymentIcon">
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 40" width="60" height="40"><path fill="#0093c7" d="M39.8 16.4c-.3 1.6-1.4 1.6-2.5 1.6h-.6l.4-2.9c.1-.2.2-.3.4-.3h.3c.7 0 1.4 0 1.8.4.2.3.2.7.2 1.2zm-.5-4h-4.1c-.3 0-.5.2-.6.5L33 24c-.1.2.1.4.3.4h2.1c.2 0 .4-.2.4-.4l.5-3.1c0-.3.2-.5.5-.5h1.3c2.7 0 4.3-1.4 4.7-4.1.2-1.2 0-2.1-.5-2.8-.6-.7-1.6-1.1-3-1.1z"></path><path fill="#213170" d="M10.4 16.4C10.2 18 9.1 18 8 18h-.6l.4-2.9c0-.2.2-.3.3-.3h.3c.8 0 1.5 0 1.8.4.3.3.3.7.2 1.2zm-.4-4H5.9c-.3 0-.6.2-.6.5L3.6 24c0 .2.2.4.4.4h1.9c.3 0 .5-.2.6-.5l.4-3c.1-.3.3-.5.6-.5h1.3c2.7 0 4.3-1.4 4.7-4.1.2-1.2 0-2.1-.5-2.8-.6-.7-1.7-1.1-3-1.1zm9.5 8c-.2 1.2-1.1 2-2.2 2-.6 0-1-.2-1.3-.6-.3-.3-.4-.8-.3-1.4.1-1.2 1-2 2.2-2 .5 0 1 .2 1.3.6.3.3.4.8.3 1.4zm2.8-4h-2c-.2 0-.3.1-.4.3v.6l-.2-.2c-.4-.7-1.4-.9-2.3-.9-2.2 0-4 1.7-4.4 4.2-.2 1.2.1 2.3.7 3.1.6.8 1.5 1.1 2.5 1.1 1.8 0 2.7-1.2 2.7-1.2v.6c-.1.2.1.4.3.4H21c.2 0 .5-.2.5-.5l1.1-7.1c0-.2-.1-.4-.3-.4z"></path><path fill="#0093c7" d="M48.8 20.4c-.2 1.2-1 2-2.2 2-.6 0-1-.2-1.3-.6-.3-.3-.4-.8-.3-1.4.2-1.2 1.1-2 2.2-2 .5 0 1 .2 1.3.6.3.3.4.8.3 1.4zm2.8-4h-2c-.2 0-.3.1-.3.3l-.1.6-.2-.2c-.4-.7-1.3-.9-2.3-.9-2.2 0-4 1.7-4.4 4.2-.2 1.2.1 2.3.8 3.1.6.8 1.4 1.1 2.4 1.1 1.8 0 2.8-1.2 2.8-1.2l-.1.6c-.1.2.1.4.3.4h1.8c.3 0 .5-.2.6-.5l1-7.1c0-.2-.1-.4-.3-.4z"></path><path fill="#213170" d="M32.7 16.4h-1.9c-.2 0-.4.1-.5.3l-2.7 4.2-1.2-4.1c-.1-.2-.3-.4-.5-.4h-2c-.2 0-.4.2-.3.5l2.2 6.7-2.1 3c-.1.3 0 .6.3.6h2c.2 0 .3-.1.4-.3L33 17c.2-.3 0-.6-.3-.6"></path><path fill="#0093c7" d="M53.9 12.7L52.2 24c0 .2.1.4.4.4h1.6c.3 0 .6-.2.6-.5l1.7-11.1c0-.2-.2-.4-.4-.4h-1.9c-.1 0-.3.1-.3.3"></path><path d="M56.2.8c1.7 0 3 1.4 3 3.2v32c0 1.8-1.3 3.2-3 3.2H3.7c-1.6 0-3-1.4-3-3.2V4C.7 2.2 2.1.8 3.7.8h52.5m0-.8H3.7C1.7 0 0 1.8 0 4v32c0 2.2 1.7 4 3.7 4h52.5c2.1 0 3.8-1.8 3.8-4V4c0-2.2-1.7-4-3.8-4" fill="#ddd"></path></svg>

	</div>

	</div>
	</a>

	</div>--%>

	<div class="responsiveBasket_basketButtons_flexItem responsiveBasket_basketButtons_flexItem-continueShopping-Mobile">
	<a class="responsiveBasket_basketButton-continueShopping responsiveBasket_basketButton-bottom continue-shop" href="/index.aspx">
	Continue Shopping
	</a>
	</div>
	</div>
	</div>

	<div class="responsiveBasket_liveChat ">
	<div class="lp-panel" id="LP_DIV_1461163363009"><div id="LPMcontainer-1613109601152-0" class="LPMcontainer LPMoverlay" role="button" tabindex="0" style="margin: 1px; padding: 0px; border-style: solid; border-width: 0px; font-style: normal; font-weight: normal; font-variant: normal; list-style: outside none none; letter-spacing: normal; line-height: normal; text-decoration: none; vertical-align: baseline; white-space: normal; word-spacing: normal; background-repeat: repeat-x; background-position: left bottom; background-color: rgb(255, 255, 255); width: 220px; height: 40px; cursor: pointer; display: block; position: relative; top: 0px; left: 0px;"><img src="https://s2.thcdn.com/design-assets/images/lookfantasticgroup/lp/basket-checkout5//reponline.gif" id="LPMimage-1613109601153-1" alt="" class="LPMimage" style="margin: 0px; padding: 0px; border-style: none; border-width: 0px; font-style: normal; font-weight: normal; font-variant: normal; list-style: outside none none; letter-spacing: normal; line-height: normal; text-decoration: none; vertical-align: baseline; white-space: normal; word-spacing: normal; position: absolute; top: 0px; left: 0px; z-index: 600;"></div></div>
<%--	<div class="liveChat liveChat-show" data-component="liveChat">
	<span data-elysium-property-name="stateOnlineText" data-elysium-property-value="Our beauty experts are"></span>
	<span data-elysium-property-name="stateOfflineText" data-elysium-property-value="Our operators are"></span>
	<span data-elysium-property-name="offlineText" data-elysium-property-value="<span class=&quot;liveChat_statusText-offline&quot;>Offline</span>"></span>
	<span data-elysium-property-name="onlineText" data-elysium-property-value="<span class=&quot;liveChat_statusText-online&quot;>Online</span>"></span>
	<div class="liveChat_status">
	<div class="liveChat_statusBrand">
	<div class="liveChat_statusBrandLogo"></div>
	<div data-js-element="statusIcon" class="liveChat_statusIcon liveChat_statusIcon-online"></div>
	</div>
	<div class="liveChat_statusText">
	<h2 class="liveChat_title">Live Chat</h2>
	<p data-js-element="statusText" class="liveChat_statusText">Our beauty experts are <span class="liveChat_statusText-online">Online</span></p>
	<p class="liveChat_infoOpeningTimes liveChat_infoOpeningTimes-alt">Average connection time 25 secs</p>
	</div>
	</div>
	<div class="liveChat_info">
	<p class="liveChat_infoOpeningTimes">Average connection time 25 secs</p>
	<button data-js-element="button" class="liveChat_button checkout-btn">
	Start Chat
	<i class="liveChat_buttonIcon"></i>
	</button>
	</div>
	</div>--%>

	</div>


	</div>
</div>
    </div>
</asp:Content>

