﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class testreport : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{

	}
    public void Getdata()
    {
        connection conn = new connection();
        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {

            ///////MainReport Start/////////
            con.Open();
            SqlCommand cmd = new SqlCommand("getemployees", con);
            cmd.CommandType = CommandType.StoredProcedure;
           
            DataTable dt = new DataTable("dt_table");
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);

            ///////MainReport Start/////////
           
          


            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));



        }



    }
    protected void btnGetRecords_Click(object sender, EventArgs e)
    {
        ReportViewer1.Reset();
        ReportViewer1.LocalReport.ReportPath = "testRpt.rdlc";
        Getdata();
        ReportViewer1.LocalReport.Refresh();


    }


    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }


}