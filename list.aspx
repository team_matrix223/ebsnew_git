﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="list.aspx.cs" Inherits="list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <link href="customcss/shop-page.css" rel="stylesheet" />
  <link href="customcss/list.css" rel="stylesheet" />
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="js/customjs/list.js"></script>
<asp:HiddenField ID="hdnLevel1" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="hdnLevel2" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="hdnLevel3" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="hdnBrandId" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="hdnGroupId" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="hdnSubGroupId" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="hdnGetMethod" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="hdnsearch" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="hdnamt_type" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="hdnamt" runat="server" ClientIDMode="Static"/>
    	<div class="constraint no-padding">
		<div class="breadcrumbs">
			<ul class="breadcrumbs_container">
				<li class="breadcrumbs_item">
					<a class="breadcrumbs_link" href="/index.aspx">Home</a>
				</li>
				<li class="breadcrumbs_item breadcrumbs_item-active">Products</li>
			</ul>
		</div>
	</div>

	<div data-component="ajaxFacets" data-componentload="helper" class="ajax-facets cf">



		<div class="js-widgets-wrapper widgets-wrapper">
			<div class="panel-head">


			</div>
		</div>





		<div data-component="" data-componentload="helper" data-product-list-wrapper="">
			<span data-elysium-property-name="facetsMobileEditRefinementText" data-elysium-property-value="Edit Refinement"></span>
			<span data-elysium-property-name="facetsMobileRefineText" data-elysium-property-value="Refine"></span>

			<div class="responsiveProductListPage" data-component="responsiveProductListPage" data-section-path="health-beauty/trends/glossy-make-up" data-is-search="false" data-price-facet-category="" data-horizontal-facets="false" data-nav-offset="48">

				<main id="mainContent" class="responsiveProductListPage_mainContent responsiveProductListPage_mainContent_withFacets" aria-labelledby="responsive-product-list-title">
				<%--	<div class="responsiveProductListHeader">
						<div class="responsiveProductListHeader_wrapper">
							<h1 id="responsive-product-list-title" class="responsiveProductListHeader_titles">
								Glossy Make-Up
							</h1>
                         
							<p class="responsiveProductListHeader_resultsCount" aria-live="polite">
								956 results
							</p>
						</div>
						<div class="responsiveProductListHeader_description">
							<div class="readmore" style="" data-max-height="50" data-read-more-text="Read More" data-read-less-text="Read Less" data-component="readmore">
								<div class="readmore_content"><p>Dewy skin is always in. Glow all year round with our edit of the best glossy makeup products with everything from primers to highlighters which promise to add a gorgeous glossy sheen to your look. </p></div>
								<div class="readmore_footer hide">
									<button type="button" class="readmore_footerButton" aria-expanded="false">Read More</button>
								</div>
							</div>

						</div>
					</div>--%>

					<div class="responsiveProductListPage_sortAndPagination ">
						<div class="responsiveProductListPage_sort">
							<div class="responsiveSort">
								<span class="responsiveSort_label">Sort by</span>
								<svg class="responsiveSort_selectSVG" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<polygon points="16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793"></polygon>
								</svg>

								<select class="responsiveSort_select" data-sort-by="" aria-label="Sort by" id="ddsort">
								<%--	<option value="default" selected="">Default</option>--%>
								<%--	<option value="salesRank">Best Selling</option>--%>
									<option value="out_of_stock asc,Price desc">Price: High to low</option>
									<option value="out_of_stock asc,Price asc">Price: Low to high</option>
									<option value="out_of_stock asc,Name asc">A - Z</option>
                                    <option value="out_of_stock asc,Name desc">Z - A</option>
									<%--<option value="releaseDate">Newest Arrivals</option>
									<option value="percentageDiscount">Percentage Discount</option>
									<option value="reviewCount_auto_int">Review Count</option>--%>

								</select>
							</div>
						</div>
						<button type="button" class="visually-hidden responsiveProductListPage_goToRefineSectionButton">Go to refine section</button>
			
						<div class="responsiveProductListPage_refine ">

							<div class="mob-ref">Refine<i class="fa fa-bars"></i></div>
							<div class="mob-side-menu">
								<button type="button" class="price responsiveFacets_overrideButtonStyle responsiveFacets_sectionHead" data-selected="false" aria-expanded="true" tabindex="0">
									<h3 class="responsiveFacets_sectionTitle"><span class="visually-hidden"> </span>Price</h3><i class="fa fa-angle-down" aria-hidden="true"></i>

								</button>
								<div class="responsiveFacets_sectionContentWrapper price-wrap">
									<fieldset class="responsiveFacets_sectionContent " aria-hidden="false">
										<legend class="visually-hidden responsiveFacets_sectionContent_legend"> Price</legend>
										<label class="responsiveFacets_sectionItemLabel">
																 <div class="price" >
        
    <p style="padding:10px 0 5px 0">
  
  <input type="text" id="amount2" readonly style="border:0; color:BLACK; font-weight:bold;FONT-WEIGHT: NORMAL">
</p>
 
<div id="slider-range2"></div>
												</label>
									

									</fieldset>
								</div>
								
								<button type="button" class="brand responsiveFacets_overrideButtonStyle responsiveFacets_sectionHead" data-selected="false" aria-expanded="true" tabindex="0">
									<h3 class="responsiveFacets_sectionTitle"><span class="isually-hidden"> </span>Brand</h3>
									<i class="fa fa-angle-down" aria-hidden="true"></i>
								</button>
								<div class="responsiveFacets_sectionContentWrapper brand-wrap">
								<fieldset class="responsiveFacets_sectionContent " aria-hidden="false" id="brandlist2">
                                    </fieldset>
									</div>					
</div>

						</div>
					</div>


						<div class="productListProducts" data-horizontal-facets="false">
						<h2 id="product-list-heading" class="visually-hidden">Products</h2>
                            <div id="facet-loading-wrap" style="display:none;text-align: center;"><div id="facet-loading"><img src="images/loading_brown.gif"><span>Loading...</span></div><div id="facet-loading-mask"></div></div>
						<ul class="productListProducts_products" id="productlist" aria-labelledby="product-list-heading">
                        
							
						
					


						</ul>
                            <div  style="text-align: center;">
                             <img id="loader" alt="" src="images/loading.gif" style="display: none;" />
                                </div>
					</div>

                   	<div class="responsiveProductListPage_bottomPagination">

						<nav class="responsivePaginationPages" id="paging_li" data-total-pages="23" data-responsive-pagination="" aria-label="Pages Bottom">
						
		<%--						<li>
									<button type="button" disabled="disabled" class="responsivePaginationNavigationButton paginationNavigationButtonPrevious" data-direction="previous" id="prevpage"  aria-label="Previous page" title="Previous page">
										<svg class="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<polygon points="16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793"></polygon>
										</svg>

									</button>
								</li>--%>
                       
								<!-- All but last page -->
					<%--			<li>
									<a class="responsivePaginationButton responsivePageSelector  responsivePageSelectorActive " data-page-number="1" href="?pageNumber=1" data-page-active="" aria-label="Page, 1" aria-current="true">
										1
									</a>
								</li>


								<li>
									<a class="responsivePaginationButton responsivePageSelector  " data-page-number="2" href="?pageNumber=2" aria-label="Go to page  2">
										2
									</a>
								</li>--%>


							<%--	<li class="responsivePageSelectorSpacer">...</li>--%>



								<!-- Last page -->
					<%--			<li>
									<a class="responsivePaginationButton responsivePageSelector   responsivePaginationButton--last" data-page-number="23" href="?pageNumber=23" aria-label="Go to page  23">
										23
									</a>
								</li>--%>

<%--


								<li>
									<button id="nxtpage"  type="button" class="responsivePaginationNavigationButton paginationNavigationButtonNext" data-direction="next" aria-label="Next page" title="Next page">
										<svg class="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<polygon points="16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793"></polygon>
										</svg>

									</button>
								</li>--%>
							
						</nav>
                           <div>
                            Page:   <label id="no_ofpage"></label> of   <label id="total_pages"></label> 
                           </div>
					</div>
					<div class="responsiveProductListPage_loaderOverlay" data-show="false">
						<div class="responsiveProductListPage_loader"></div>
					</div>
				</main>

				<aside class="responsiveProductListPage_facets" aria-labelledby="responsive-facets-title">
					<div class="responsiveFacets">

						<div class="responsiveFacets_container responsiveFacets_container-transitioned" data-show="false" data-child-open="false">
							<div class="responsiveFacets_head">
								<h2 id="responsive-facets-title" class="responsiveFacets_title">Refine</h2>

								<button type="button" class="responsiveFacets_overrideButtonStyle responsiveFacets_menuClose" aria-label="Close" title="Close" tabindex="0">
									<svg width="40" height="40" xmlns="http://www.w3.org/2000/svg">
										<g fill-rule="nonzero" stroke-width="2" fill="none">
											<path d="M13 13l14 14M27 13L13.004 27">
											</path>
										</g>
									</svg>
								</button>
							</div>

							<button type="button" class="visually-hidden responsiveFacets_goToProductSectionButton" tabindex="0">Go to product section</button>

							<div class="responsiveFacets_content">

								<div class="responsiveFacets_section">
									<button type="button" class="price responsiveFacets_overrideButtonStyle responsiveFacets_sectionHead-mobile" tabindex="0">
										Price
										<%--<svg class="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<polygon points="16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793"></polygon>
										</svg>--%>


									</button>

									<div class="responsiveFacets_sectionContainer accordionWidget_expanded" data-show="false" data-closed="false" data-selected="false" style="max-height: none;">

										<div class="responsiveFacets_sectionHeadWrapper">

											<button type="button" class="price responsiveFacets_overrideButtonStyle responsiveFacets_sectionHead" data-selected="false" aria-expanded="true" tabindex="0">
												<h3 class="responsiveFacets_sectionTitle"><span class="visually-hidden"> </span>Price </h3>
												<span class="price-icon">
														<i class="fa fa-plus" aria-hidden="true"></i>
												</span>
												<%--<span class="responsiveFacets_sectionCaret" data-closed="false">
													<svg class="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
														<polygon points="16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793"></polygon>
													</svg>
													
												</span>--%>
											</button>

											<div class="responsiveFacets_mobileSectionTitle">
												<span class="responsiveFacets_sectionTitle">
													Price
												</span>

												<span class="responsiveFacets_sectionCaret" data-closed="false">
													<svg class="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
														<polygon points="16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793"></polygon>
													</svg>

												</span>
											</div>

											<button type="button" class="responsiveFacets_overrideButtonStyle responsiveFacets_sectionBackArrow" tabindex="0" aria-label="Back" title="Back">
												<svg class="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<polygon points="16.1371072 15 12 10.7802414 7.86289277 15 6.5 13.6098793 12 8 17.5 13.6098793"></polygon>
												</svg>

											</button>

										</div>

										<div class="responsiveFacets_sectionContentWrapper price-wrap">
											<fieldset class="responsiveFacets_sectionContent " aria-hidden="false">
												<legend class="visually-hidden responsiveFacets_sectionContent_legend"> Price</legend>
												<label class="responsiveFacets_sectionItemLabel">
																 <div class="price" >
        
    <p style="padding:10px 0 5px 0">
  
  <input type="text" id="amount" readonly style="border:0; color:BLACK; font-weight:bold;FONT-WEIGHT: NORMAL">
</p>
 
<div id="slider-range"></div>
												</label>
	
                                                		</div>
	
                                                		</fieldset>
											    </div>
	

									
										</div>
									</div>
									<button type="button" class="brand responsiveFacets_overrideButtonStyle responsiveFacets_sectionHead-mobile" tabindex="0">
										Brand
									</button>

									<div class="responsiveFacets_sectionContainer accordionWidget_expanded" data-show="false" data-closed="false" data-selected="false" style="max-height: none;">

										<div class="responsiveFacets_sectionHeadWrapper">

											<button type="button" class="brand responsiveFacets_overrideButtonStyle responsiveFacets_sectionHead" data-selected="false" aria-expanded="true" tabindex="0">
												<h3 class="responsiveFacets_sectionTitle"><span class="visually-hidden"> </span>Brand</h3>
													<span class="brand-icon">
													
													<i class="fa fa-plus" aria-hidden="true"></i>
												</span>
											</button>

										</div>

										<div class="responsiveFacets_sectionContentWrapper brand-wrap">
										<fieldset class="responsiveFacets_sectionContent " aria-hidden="false" id="brandlist"></fieldset>
                                                       
										</div>
									</div>

                            <div id="attr">

                            </div>
								       <div id="color">

                            </div>	

									
					

								</div>
							</div>

							<div class="responsiveFacets_saveContainer">
								<button type="button" class="responsiveFacets_save" tabindex="0">Save &amp; View</button>
							</div>

							<div class="responsiveFacets_error " data-show="false" aria-hidden="true">We're sorry, we couldn't load the products. Please try again.</div>
						</div>
						<button type="button" class="visually-hidden responsiveFacets_goToProductSectionButton responsiveFacets_goToProductSectionButton_bottom">Go to product section</button>
					</div>

				</aside>
			</div>




			<span data-component="productQuickbuy"></span>
			<div class="addedToBasketModal" data-component="addedToBasketModal" data-cdn-url="https://s2.thcdn.com//" data-secure-url="https://www.ebs1952.com/" role="dialog" tabindex="-1">

				<span data-elysium-property-name="maxQuantityReached" data-elysium-property-value="Item cannot be added to your basket. This product is limited to a quantity of %d per order."></span>
				<span data-elysium-property-name="partialMaxQuantityReached" data-elysium-property-value="%1 items cannot be added to your basket. This product is limited to a quantity of %2 per order."></span>

				<div class="addedToBasketModal_container" role="dialog" tabindex="-1" aria-labelledby="added-to-basket-modal-title0">
					<div class="addedToBasketModal_titleContainer">
						<h2 id="added-to-basket-modal-title0" class="addedToBasketModal_title">
							Added to your basket
						</h2>

						<button type="button" class="addedToBasketModal_closeContainer" aria-label="Close" title="Close" data-close="">
							<svg class="addedToBasketModal_close" viewBox="-3 -4 20 20" xmlns="http://www.w3.org/2000/svg">
								<path d="M8.414 7l5.293-5.293c.391-.391.391-1.023 0-1.414s-1.023-.391-1.414 0l-5.293 5.293-5.293-5.293c-.391-.391-1.023-.391-1.414 0s-.391 1.023 0 1.414l5.293 5.293-5.293 5.293c-.391.391-.391 1.023 0 1.414.195.195.451.293.707.293.256 0 .512-.098.707-.293l5.293-5.293 5.293 5.293c.195.195.451.293.707.293.256 0 .512-.098.707-.293.391-.391.391-1.023 0-1.414l-5.293-5.293"></path>
							</svg>

						</button>
					</div>

					<div class="addedToBasketModal_error " data-error="">
						Sorry, there seems to have been an error. Please try again.
					</div>

					<div class="addedToBasketModal_error " data-quantity-limit-reached="">
					</div>

					<div class="addedToBasketModal_warning" data-quantity-limit-partially-reached="">
					</div>

					<div class="addedToBasketModal_item">
						<div class="addedToBasketModal_imageContainer">
							<a href="" data-product-url="">
								<img src="//:0" alt="Loading..." class="addedToBasketModal_image" data-product-image="">
							</a>
						</div>
						<div class="addedToBasketModal_itemDetails">
							<a href="" data-product-url="" class="addedToBasketModal_itemName" data-product-title="">Product Name</a>
							<p class="addedToBasketModal_itemQuantity">
								Quantity
								<span class="addedToBasketModal_itemQuantity addedToBasketModal_itemQuantity-number" data-product-quantity=""></span>
							</p>


							<p class="addedToBasketModal_itemPrice" data-product-price=""></p>
						</div>
					</div>

					<div class="addedToBasketModal_subtotal">
						<p class="addedToBasket_subtotalTitle">
							Subtotal:
							<span class="addedToBasket_subtotalItemCount">
								(<span class="addedToBasket_subtotalItemCount-number" data-basket-total-items=""></span>
								items in your basket)
							</span>
						</p>

						<p class="addedToBasket_subtotalAmount" data-basket-total=""></p>
					</div>

					<div class="addedToBasketModal_loyaltyPointsMessage">
					</div>

					<div class="addedToBasketModal_ctas">

						<div class="addedToBasketModal_ctaContainerLeft">
							<button type="button" class="addedToBasket_continueShoppingButton" data-continue-shopping="">
								Continue Shopping
							</button>
						</div>
						<div class="addedToBasketModal_ctaContainerRight">
							<a href="/my.basket" class="addedToBasketModal_viewBasketButton js-e2e-quickView-basket" data-view-basket="">
								View Basket
							</a>
						</div>
					</div>

					<div class="addedToBasketModal_productRecommendations" data-recommendations="">
						<span class="addedToBasketModal_loading">
							<span class="addedToBasketModal_loadingSpinny"></span>
						</span>
					</div>
				</div>
			</div>




	



		</div>



	</div>
	<script>
		$(document).ready(function () {
			var price_val = 0;
			var brand_val = 0;
			var save_val = 0;
			var skin_val = 0;
			var s_tool_val = 0;
			var s_con_val = 0;
			var make_p_val = 0;
			var make_t_val = 0;
			var make_c_val = 0;
			var body_p_val = 0;
			var body_t_val = 0;
			var mob_icon_val = 0;
			var m_price_val = 0;
			var m_brand_val = 0;
			var m_save_val = 0;
			var m_skin_val = 0;
			var m_s_tool_val = 0;
			var m_s_con_val = 0;
			var m_make_p_val = 0;
			var m_make_t_val = 0;
			var m_make_c_val = 0;
			var m_body_p_val = 0;
			var m_body_t_val = 0;
			

  //$(".price").click(function(){
  //	$(".price-wrap").toggle();
  //	if (price_val == 0) {
  //		$(".price").attr("aria-expanded", "false");
  //		price_val = 1;
  //	}
  //	else  {
  //		$(".price").attr("aria-expanded", "true");
  //		price_val = 0;
  //	}
  
  //});
  //$(".brand").click(function () {

  //	$(".brand-wrap").toggle();
  //	if (brand_val == 0) {
  //		$(".brand").attr("aria-expanded", "false");
  //		brand_val = 1;
  //	}
  //	else {
  //		$(".brand").attr("aria-expanded", "true");
  //		brand_val = 0;
  //	}
  
  //});
  $(".save").click(function () {
  	$(".save-wrap").toggle();
  	if (save_val == 0) {
  		$(".save").attr("aria-expanded", "false");
  		save_val = 1;
  	}
  	else {
  		$(".save").attr("aria-expanded", "true");
  		save_val = 0;
  	}

  });
  $(".skin").click(function () {
  	$(".skin-wrap").toggle();
  	if (skin_val == 0) {
  		$(".skin").attr("aria-expanded", "false");
  		skin_val = 1;
  	}
  	else {
  		$(".skin").attr("aria-expanded", "true");
  		skin_val = 0;
  	}

  });
  $(".s-tool").click(function () {
  	$(".s-tool-wrap").toggle();
  	if (s_tool_val == 0) {
  		$(".s-tool").attr("aria-expanded", "false");
  		s_tool_val = 1;
  	}
  	else {
  		$(".s-tool").attr("aria-expanded", "true");
  		s_tool_val = 0;
  	}

  });
  $(".s-con").click(function () {
  	$(".s-con-wrap").toggle();
  	if (s_con_val == 0) {
  		$(".s-con").attr("aria-expanded", "false");
  		s_con_val = 1;
  	}
  	else {
  		$(".s-con").attr("aria-expanded", "true");
  		s_con_val = 0;
  	}
  });
  $(".make-p").click(function () {
  	$(".make-p-wrap").toggle();
  	if (make_p_val == 0) {
  		$(".make-p").attr("aria-expanded", "false");
  		make_p_val = 1;
  	}
  	else {
  		$(".make-p").attr("aria-expanded", "true");
  		make_p_val = 0;
  	}
  });
  $(".make-t").click(function () {
  	$(".make-t-wrap").toggle();
  	if (make_t_val == 0) {
  		$(".make-t").attr("aria-expanded", "false");
  		make_t_val = 1;
  	}
  	else {
  		$(".make-t").attr("aria-expanded", "true");
  		make_t_val = 0;
  	}
  });
  $(".make-c").click(function () {
  	$(".make-c-wrap").toggle();
  	if (make_c_val == 0) {
  		$(".make-c").attr("aria-expanded", "false");
  		make_c_val = 1;
  	}
  	else {
  		$(".make-c").attr("aria-expanded", "true");
  		make_c_val = 0;
  	}
  });
  $(".body-p").click(function () {
  	$(".body-p-wrap").toggle();
  	if (body_p_val == 0) {
  		$(".body-p").attr("aria-expanded", "false");
  		body_p_val = 1;
  	}
  	else {
  		$(".body-p").attr("aria-expanded", "true");
  		body_p_val = 0;
  	}
  });
  $(".body-t").click(function () {
  	$(".body-t-wrap").toggle();
  	if (body_t_val == 0) {
  		$(".body-t").attr("aria-expanded", "false");
  		body_t_val = 1;
  	}
  	else {
  		$(".body-t").attr("aria-expanded", "true");
  		body_t_val = 0;
  	}
  });

  $(".mob-ref").click(function () {
  	$(".responsiveFacets_container.responsiveFacets_container-transitioned").toggle();
  	if(mob_icon_val==0)
  	{
  		
  		$(".mob-ref i").attr("class", "fa fa-times");
  		mob_icon_val = 1;
  	}
  	else {
  		mob_icon_val = 1;
  		$(".mob-ref i").attr("class", "fa fa-bars");
  		mob_icon_val = 0;	
  	}
  });
  $(".mob-side-menu .price").click(function () {
  
  	if (m_price_val == 0) {
  		$(".mob-side-menu .price i").attr("class", "fa fa-angle-up");
  		m_price_val = 1;
  	}
  	else {
  		$(".mob-side-menu  .price i").attr("class", "fa fa-angle-down");
  		m_price_val = 0;
  	}

  });
  $(".mob-side-menu .brand").click(function () {

  	if (m_brand_val == 0) {
  		$(".mob-side-menu .brand i").attr("class", "fa fa-angle-up");
  		m_brand_val = 1;
  	}
  	else {
  		$(".mob-side-menu  .brand i").attr("class", "fa fa-angle-down");
  		m_brand_val = 0;
  	}

  });
  $(".mob-side-menu .save").click(function () {

  	if (m_save_val == 0) {
  		$(".mob-side-menu .save i").attr("class", "fa fa-angle-up");
  		m_save_val = 1;
  	}
  	else {
  		$(".mob-side-menu  .save i").attr("class", "fa fa-angle-down");
  		m_save_val = 0;
  	}

  });
  $(".mob-side-menu .skin").click(function () {

  	if (m_skin_val == 0) {
  		$(".mob-side-menu .skin i").attr("class", "fa fa-angle-up");
  		m_skin_val = 1;
  	}
  	else {
  		$(".mob-side-menu  .skin i").attr("class", "fa fa-angle-down");
  		m_skin_val = 0;
  	}

  });
  $(".mob-side-menu .s-tool").click(function () {

  	if (m_s_tool_val == 0) {
  		$(".mob-side-menu .s-tool i").attr("class", "fa fa-angle-up");
  		m_s_tool_val = 1;
  	}
  	else {
  		$(".mob-side-menu  .s-tool i").attr("class", "fa fa-angle-down");
  		m_s_tool_val = 0;
  	}

  });
  $(".mob-side-menu .s-con").click(function () {

  	if (m_s_con_val == 0) {
  		$(".mob-side-menu .s-con i").attr("class", "fa fa-angle-up");
  		m_s_con_val = 1;
  	}
  	else {
  		$(".mob-side-menu  .s-con i").attr("class", "fa fa-angle-down");
  		m_s_con_val = 0;
  	}

  });
  $(".mob-side-menu .make-p").click(function () {

  	if (m_make_p_val == 0) {
  		$(".mob-side-menu .make-p i").attr("class", "fa fa-angle-up");
  		m_make_p_val = 1;
  	}
  	else {
  		$(".mob-side-menu .make-p i").attr("class", "fa fa-angle-down");
  		m_make_p_val = 0;
  	}

  });
  $(".mob-side-menu .make-t").click(function () {

  	if (m_make_t_val == 0) {
  		$(".mob-side-menu .make-t i").attr("class", "fa fa-angle-up");
  		m_make_t_val = 1;
  	}
  	else {
  		$(".mob-side-menu .make-t i").attr("class", "fa fa-angle-down");
  		m_make_t_val = 0;
  	}

  });
  $(".mob-side-menu .make-c").click(function () {

  	if (m_make_c_val == 0) {
  		$(".mob-side-menu .make-c i").attr("class", "fa fa-angle-up");
  		m_make_c_val = 1;
  	}
  	else {
  		$(".mob-side-menu .make-c i").attr("class", "fa fa-angle-down");
  		m_make_c_val = 0;
  	}

  });
  $(".mob-side-menu .body-p").click(function () {

  	if (m_body_p_val == 0) {
  		$(".mob-side-menu .body-p i").attr("class", "fa fa-angle-up");
  		m_body_p_val = 1;
  	}
  	else {
  		$(".mob-side-menu .body-p i").attr("class", "fa fa-angle-down");
  		m_body_p_val = 0;
  	}

  });
  $(".mob-side-menu .body-t").click(function () {

  	if (m_body_t_val == 0) {
  		$(".mob-side-menu .body-t i").attr("class", "fa fa-angle-up");
  		m_body_t_val = 1;
  	}
  	else {
  		$(".mob-side-menu .body-p i").attr("class", "fa fa-angle-down");
  		m_body_t_val = 0;
  	}

  });

});	</script>

<script>
	//filterscript
	$(document).ready(function () {
		var filter_price_val = 0;
		var filter_brand_val = 0;
		$(".price-icon").click(function () {
			$(".price-wrap").toggle();
			if (filter_price_val == 0) {
				$(".price-icon i").attr("class", "fa fa-minus");
				filter_price_val = 1;
			}
			else {
				$(".price-icon i").attr("class", "fa fa-plus");
				filter_price_val = 0;
			}
		});
		$(".brand-icon").click(function () {
			$(".brand-wrap").toggle();
			if (filter_brand_val == 0) {
				$(".brand-icon i").attr("class", "fa fa-minus");
				filter_brand_val = 1;
			}
			else {
				$(".brand-icon i").attr("class", "fa fa-plus");
				filter_brand_val = 0;
			}
		});
		
	});
</script>


	
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

	<!-- Include all compiled plugins (below), or include individual files as needed -->

</asp:Content>

