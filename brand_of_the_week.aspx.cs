﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class brand_of_the_week : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
        if (!IsPostBack)
        {
            GetBrands();
        }
	}

    public void GetBrands() {

        List<Products> ListBrands = new ProductsBLL().GetAllHomeBrands();
        rpt_getbrands.DataSource = ListBrands.Where(x => x.Type == "S");
        rpt_getbrands.DataBind();
    }
}