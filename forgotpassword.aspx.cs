﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class forgotpassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        DataSet ds = new UsersBLL().ResetPassword(tbemailid.Text.Trim(), tbpwd.Text.Trim());

        if (ds.Tables[0].Rows.Count==0)
        {
            Response.Write("<script>alert('User Not Registerd!')</script>");
        }
        else
        {
          

            Response.Redirect("login.aspx?q=1");
        }

    }
}