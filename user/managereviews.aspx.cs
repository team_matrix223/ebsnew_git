﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class user_managereviews : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string Insert(string Title, string Description, string Rating)
    {
        Int32 uId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());
        Reviews objReviews = new Reviews()
        {
            
            UserId = uId,
            Title = Title,
            Description = Description,
            Rating = Rating,
            IsApproved = false,
          

        };
        int status = new ReviewsBLL().InsertUpdate(objReviews);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            DelAdd = objReviews,
            Status = status
        };
        return ser.Serialize(JsonData);
    }
    
}