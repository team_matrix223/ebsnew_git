﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class user_managedeliveryaddressess : System.Web.UI.Page
{
    string Mode = "";
   static Int64 DId = 0;
   Boolean primarystatus ;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
            DId = 0;
            BindCities();
            BindGrid();
        }
    }
    public void ResetControls()
    {
        chkprimary.Visible = false;
        ddlCities.Text = "--Select City--";
        btnAdd.Text = "Add Information";
        DId = 0;
        txtAddress.Text = "";
        txtArea.Text = "";
        txtFirstName.Text = "";
        txtLastName.Text = "";
        txtMobileNo.Text = "";
        txtPinCode.Text = "";
        txtStreet.Text = "";
        txtTelephone.Text = "";
        btn_Cancel.Visible = false;
        chkprimary.Visible = true;
    }
    public void BindGrid()
    {
        int UserId = Convert.ToInt32(Session[Constants.UserId]);
        List<DeliveryAddress> objlist = new DeliveryAddressBLL().GetAllByUserId(UserId);
        
        Grid_User.DataSource = objlist;
        Grid_User.DataBind();
    }
    public void BindCities()
    {
        ddlCities.DataSource = new CitiesBLL().GetAll();
        ddlCities.DataTextField = "Title";
        ddlCities.DataValueField = "CityId";
        ddlCities.DataBind();
        ddlCities.Items.Insert(0, "--Select City--");
       
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (Mode == "Edit")
        {
            DeliveryAddress objDelAdd = new DeliveryAddress()
            {
                DeliveryAddressId = DId,
                UserId = Convert.ToInt32(Session[Constants.UserId]),
                RecipientFirstName = txtFirstName.Text,
                RecipientLastName = txtLastName.Text,
                MobileNo = txtMobileNo.Text,
                Telephone = txtTelephone.Text,
                CityId = Convert.ToInt32(ddlCities.SelectedValue.ToString()),
                Area = txtArea.Text,
                Street = txtStreet.Text,
                Address = txtAddress.Text,
                PinCode = txtPinCode.Text,
                IsPrimary = primarystatus,

            };
            int status = new DeliveryAddressBLL().InsertUpdate(objDelAdd);
            if (status == 0)
            {
                Response.Write("<script>alert('Insertion failed,Plz try again');</script>");
            }
            else
            {
                Response.Write("<script>alert('Information is saved successfully');</script>");
                ResetControls();
                BindGrid();
            }
        }
        else
        {
            DeliveryAddress objDelAdd = new DeliveryAddress()
            {
                DeliveryAddressId = DId,
                UserId = Convert.ToInt32(Session[Constants.UserId]),
                RecipientFirstName = txtFirstName.Text,
                RecipientLastName = txtLastName.Text,
                MobileNo = txtMobileNo.Text,
                Telephone = txtTelephone.Text,
                CityId = Convert.ToInt32(ddlCities.SelectedValue.ToString()),
                Area = txtArea.Text,
                Street = txtStreet.Text,
                Address = txtAddress.Text,
                PinCode = txtPinCode.Text,
                IsPrimary = chkprimary.Checked,

            };
            int status = new DeliveryAddressBLL().InsertUpdate(objDelAdd);
            if (status == 0)
            {
                Response.Write("<script>alert('Insertion failed,Plz try again');</script>");
            }
            else
            {
                Response.Write("<script>alert('Information is saved successfully');</script>");
                ResetControls();
                BindGrid();
            }
        }
       
       
    }
    
    protected void Grid_User_RowEditing(object sender, GridViewEditEventArgs e)
    {
        Grid_User.EditIndex = e.NewEditIndex;
        HiddenField DelivId = (HiddenField)Grid_User.Rows[Grid_User.EditIndex].FindControl("hdId");
        DId  =Convert.ToInt64( DelivId.Value);

        Label FName =(Label) Grid_User.Rows[Grid_User.EditIndex].FindControl("lblFirstName");
        txtFirstName.Text = FName.Text;

        Label LName = (Label)Grid_User.Rows[Grid_User.EditIndex].FindControl("lblLastName");
        txtLastName.Text = LName.Text;

        Label Mobile = (Label)Grid_User.Rows[Grid_User.EditIndex].FindControl("lblMobileNo");
        txtMobileNo.Text = Mobile.Text;

        Label TPhone = (Label)Grid_User.Rows[Grid_User.EditIndex].FindControl("lblTelephone");
        txtTelephone.Text = TPhone.Text;

        Label CityId = (Label)Grid_User.Rows[Grid_User.EditIndex].FindControl("lblCityId");
        ddlCities.SelectedValue = CityId.Text;

        Label Area = (Label)Grid_User.Rows[Grid_User.EditIndex].FindControl("lblArea");
        txtArea.Text = Area.Text;

        Label Street = (Label)Grid_User.Rows[Grid_User.EditIndex].FindControl("lblStreet");
        txtStreet.Text = Street.Text;

        Label Address = (Label)Grid_User.Rows[Grid_User.EditIndex].FindControl("lblAddress");
        txtAddress.Text = Address.Text;

        Label PinCode = (Label)Grid_User.Rows[Grid_User.EditIndex].FindControl("lblPinCode");
        txtPinCode.Text = PinCode.Text;

        CheckBox Primary = (CheckBox)Grid_User.Rows[Grid_User.EditIndex].FindControl("chkprimaryadress");
        chkprimary.Checked = Primary.Checked;

        
        


        if (Primary.Checked == true)
        {
            chkprimary.Visible = false;
           // Del.Visible = false;
            primarystatus = Primary.Checked;
        }
        else
        {
           // Del.Visible = true;
            chkprimary.Visible = true;
            primarystatus = Primary.Checked;
        }
        Mode = "Edit";
        btnAdd.Text = "Update Information";
        btn_Cancel.Visible = true;
    }

    protected void Grid_User_RowDeleting(object sender, GridViewDeleteEventArgs e)
    { int UId = Convert.ToInt32(Session[Constants.UserId]);
        HiddenField DelivId = (HiddenField)Grid_User.Rows[e.RowIndex].FindControl("hdId");
        DId = Convert.ToInt64(DelivId.Value);
        DeliveryAddress objdel = new DeliveryAddress()
        {
            UserId=UId,
            DeliveryAddressId=DId,
        };
        int status = new DeliveryAddressBLL().DeleteById(objdel);
        if (status == 0)
        {
            Response.Write("<script>alert('Deleteion failed,Plz try again');</script>");
        }
        else
        {
            Response.Write("<script>alert('Current Record is deleted');</script>");
            BindGrid();
        }
    }
    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        ResetControls();
    }


    protected void Grid_User_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
         
            CheckBox Primary = (CheckBox) e.Row.FindControl("chkprimaryadress");
                Boolean rType = Primary.Checked;
                if (rType == true)
                {
                    LinkButton Del = (LinkButton)e.Row.FindControl("lkDelete");// <-- You can hide or show as per your requirement
                    Del.Visible = false;
                }
                else
                {
                    LinkButton Del = (LinkButton)e.Row.FindControl("lkDelete");// <-- You can hide or show as per your requirement
                    Del.Visible = true;
                }
            
        }
    }
}