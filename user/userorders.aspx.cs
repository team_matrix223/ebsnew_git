﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;


public partial class user_userorders : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtFromDate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
        }
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        Int64 UserId = Convert.ToInt32(Session[Constants.UserId]);
        repOrders.DataSource = new OrderBLL().GetOrderByUserId(UserId,Convert.ToDateTime(txtFromDate.Text ),Convert.ToDateTime(txtToDate.Text),ddlStatus.SelectedItem .Text );
        repOrders.DataBind();
    }
    public List<OrderDetail> BindOrderDetailList(object OrderId, object Status)
    {
        if (Convert.ToString(Status) == "Canceled")
        {
            int OId = Convert.ToInt32(OrderId);
            List<OrderDetail> lst = new OrderBLL().GetCancelOrderDetailById(OId);
            return lst;
        }
        else
        {
            int OId = Convert.ToInt32(OrderId);
            List<OrderDetail> lst = new OrderBLL().GetOrderDetailById(OId);
            return lst;
        }
    }


    [WebMethod]

    public static string InsertCancelOrder(Int32 OrderId, string CancelRemarks)
    {
        CancelOrders objCancelOrders = new CancelOrders()
        {
            OrderId = Convert.ToInt32(OrderId),
            CancelRemarks = CancelRemarks,

        };


        JavaScriptSerializer ser = new JavaScriptSerializer();

        new CancelOrderBLL().CancelOrder(objCancelOrders);
        var JsonData = new
        {
            Order = objCancelOrders

        };
        return ser.Serialize(JsonData);
    }




    protected void repOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Literal lt = (Literal)e.Item.FindControl("ltData");
            HiddenField hdn = (HiddenField)e.Item.FindControl("hdnOrderId");

            HiddenField hdnStatus = (HiddenField)e.Item.FindControl("hdnStatus");
            lt.Text = "";
            if (hdnStatus.Value == "Canceled" || hdnStatus.Value == "Billed" || hdnStatus.Value == "Dispatched")
            {
                lt.Text = "";
            }
            else
            {

                lt.Text = "<div   onclick='javascript:CancelOrder(" + hdn.Value + ")' style='font-weight:bold;cursor:pointer;border:1px solid #aaa'> Cancel</div>";
            }

        }
    }

}