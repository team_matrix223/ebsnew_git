﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class user_referfriend : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            GetSettings();
        }
    }

    void GetSettings()
    {

        Settings objSettings = new SettingsDAL().GetSett();

        Users objUsers = new Users();
        DeliveryAddress objDelivery=new DeliveryAddress();
        objUsers.UserId = Convert.ToInt32(Session[Constants.UserId]);
        new UsersBLL().GetById(objUsers, objDelivery);
        ltReferarCode.Text = objUsers.Code;

        ltReferrarPoint.Text = objSettings.RefereePoint.ToString();
        ltRefreePoint.Text = objSettings.RefereePoint.ToString();
    }
}