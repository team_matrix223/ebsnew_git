﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class user_Settings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    [WebMethod]
    public static string UpdateSubscription(bool NewsLetter)
     {
        Int32 uId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());
        Users objUsers = new Users()
        {

            UserId = uId,
            NewsLetter = NewsLetter,


        };
        int status = new UsersBLL().UpdateNewlettersubscription(objUsers);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            DelAdd = objUsers,
            Status = status
        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string Filldetail()
    {

        Int32 uId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());

        Users objUsers = new Users()
        {

            UserId = uId,
          

        };
        new UsersBLL().GetDetailByUserId(objUsers);
        JavaScriptSerializer ser = new JavaScriptSerializer();
       
        var JsonData = new
        {

           Users = objUsers,

        };
        return ser.Serialize(JsonData);
    }

}