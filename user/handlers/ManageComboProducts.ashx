﻿<%@ WebHandler Language="C#" Class="ManageComboProducts" %>

using System;
using System.Web;

public class ManageComboProducts : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        Int32  CatId =Convert.ToInt32( context.Request.QueryString["CatId"].ToString());
        Int32 SubCatId =Convert.ToInt32( context.Request.QueryString["SubCatId"].ToString());
        Int32 SubSubCatId =Convert.ToInt32( context.Request.QueryString["SubSubCatId"].ToString());
        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;

        if (strOperation == null)
        {
            //oper = null which means its first load.
            Products objProduct = new Products() { CategoryId = CatId, SubCategoryId = SubCatId, CategoryLevel3 = SubSubCatId };
            
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var x = jsonSerializer.Serialize(

                new ProductsBLL().ProductGetByCatIdSubCatId(objProduct));

            context.Response.Write(x);
        }
        else if (strOperation == "del")
        {


        } 
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}