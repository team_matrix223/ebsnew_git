﻿<%@ WebHandler Language="C#" Class="usercombo" %>

using System;
using System.Web;
using System.Web.SessionState;
public class usercombo : IHttpHandler, IRequiresSessionState
{
    
    public void ProcessRequest (HttpContext context) {
        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;
        if (strOperation == null)
        {
        
            Int32 UserId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId]);
            //oper = null which means its first load.
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var x = jsonSerializer.Serialize(

                new UserComboBLL().GetAllByUserId(UserId));

            context.Response.Write(x);
        }
        else if (strOperation == "del")
        {


        }  
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}