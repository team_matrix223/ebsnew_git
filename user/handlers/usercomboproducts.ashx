﻿<%@ WebHandler Language="C#" Class="usercomboproducts" %>

using System;
using System.Web;

public class usercomboproducts : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;

        if (strOperation == null)
        {
            string CatId = context.Request.QueryString["CatId"];

            //oper = null which means its first load.
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            jsonSerializer.MaxJsonLength = int.MaxValue;
            var xss = jsonSerializer.Serialize(
            new ProductsBLL().GetByCategoryId(Convert.ToInt32(CatId))

               );

            context.Response.Write(xss);
        }

        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}