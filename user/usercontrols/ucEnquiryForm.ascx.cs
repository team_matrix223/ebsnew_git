﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usercontrols_ucEnquiryForm : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnAsk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Enquiries objEnquiries = new Enquiries()
            {
                PersonName = txtPersonName.Text,
                EmailId = txtEmail.Text,
                Enquiry = txtMessage.Text,
                MobileNo = txtMobile.Text,

            };


            int Status = new EnquiriesBLL().InsertUpdate(objEnquiries);
            if (Status == 0)
            {
                Response.Write("<script>alert('Some Issues Occured');</script>");
            }
            else
            {
                Response.Write("<script>alert('Enquiry Submit Successfully');</script>");
            }
        }

    }
}