﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/main.master" AutoEventWireup="true" CodeFile="usercombo.aspx.cs" Inherits="user_usercombo" %>
<asp:Content ID="cntBreadCrumbs" ContentPlaceHolderID="cntBreadCrumbs" runat="server">
      <div class="breadcrumbs_container">
            <article class="breadcrumbs">
                <a href="dashboard.aspx">Dashboard</a>
                <div class="breadcrumb_divider">
                </div>
                <a href="usercombo.aspx" class="current">My Combo</a>
            
            </article>
        </div>
<a href="../usercontrols/">../usercontrols/</a>

</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>

      
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
    <script src="../js/customValidation.js"></script>
   <script src="js/jquery.uilock.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript">
    var m_ComboId = 0;
    var ComboPhotoUrl = "";
 
    function DeleteTemp() {
        $.ajax({
            type: "POST",
            data: '',
            url: "usercombo.aspx/DeleteTemp",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

                $.uiUnlock();
            }



        });
    }
    function ResetControls() {
        ComboPhotoUrl = "";
        m_ComboId = 0;
        var txtTitle = $("#txtTitle");
        var btnAdd = $("#btnAdd");
        var btnUpdate = $("#btnUpdate");
        $("#<%=ddlCategory.ClientID%> option[value= 0 ]").prop("selected", true);
        txtTitle.focus();
        txtTitle.val("");
        txtTitle.focus();
        btnAdd.css({ "display": "block" });
        btnAdd.html("Add Combo");
        btnUpdate.css({ "display": "none" });
        btnUpdate.html("Update Combo");
        $("#btnReset").css({ "display": "none" });
        $("#hdnId").val("0");
        validateForm("detach");
        $("#FileUpload1").val("");
        $("#cartcontainer").html("");
        $("#subtotal").html(" Rs. " +0);
        $("#sp_TotalItems").html(0);
        DeleteTemp();
    }
    function ATC(pid, vid, qty, st) {

        $.ajax({
            type: "POST",
            data: '{"pid":"' + pid + '","vid":"' + vid + '","qty":"' + qty + '","st":"' + st + '"}',
            url: "usercombo.aspx/ATC",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);
                $("#cq_" + obj.pid + vid).html(qty);


                if (obj.error == 1) {

                    alert("An Error occured during transaction. Please refresh the page and try again Later.");
                    return;
                }
                $("#subtotal").html(" Rs. " + obj.Calc.SubTotal);
                $("#cq_" + obj.pid + obj.Calc.VariationId).html(obj.Calc.Qty);
                $("#span" + obj.pid + obj.Calc.VariationId).html(obj.Calc.ProductSubTotal);


                if (obj.Calc.Qty == 0) {
                    $("#tp_" + obj.pid + obj.Calc.VariationId).remove();
                }

                BindTempComboDetail();
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {

                $.uiUnlock();

            }

        });

    }
    function TakeMeTop() {
        $("html, body").animate({ scrollTop: 0 }, 500);
    }
    function RefreshGrid() {
        $('#jQGridDemo').trigger('reloadGrid');

    }
    function BindTempComboDetail() {
        $.ajax({
            type: "POST",
            data: '{}',
            url: "usercombo.aspx/GetComboHTML",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);

                $("#cartcontainer").html(obj.html);
                $("#subtotal").html(" Rs. " + obj.ST);
                $("#sp_TotalItems").html(obj.TotalItems);

            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {
            }

        });
    }
    function DeleteCombo() {
        if (m_ComboId == 0) {
            return;
        }
        if (!confirm("Are you sure to delete this combo")) {
            return;
        }
        $.ajax({
            type: "POST",
            data: '{"ComboId":"'+ m_ComboId+'"}',
            url: "usercombo.aspx/DeleteCombo",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                var obj = jQuery.parseJSON(msg.d);
                BindGrid();
                ResetControls();
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
             
                alert(obj.Message);

            },
            complete: function () {

                $.uiUnlock();
            }



        });
    }
    function BindMainList() {
        var PId = 0;
        var PName = 0;
        var Photo = "";
        var Unit = "";
        var Desc = "";
        var Variation = 0;
        var Qty = 1;
        var Price = 0;
        var DisPer = 0;
        var DisAmt = 0;
        var VatPer = 0;
        var VatAmt = 0;
        var ServiceTaxPer = 0;
        var ServiceTaxAmt = 0;
        var ServiceChargePer = 0;
        var ServiceChargeAmt = 0;
        var Value = $("input[name=rbVariation]:checked").val();
        PId = $('#jQGridProductList').jqGrid('getCell', selectedrowid, 'ProductId');
        PName = $('#jQGridProductList').jqGrid('getCell', selectedrowid, 'Name');
        Photo = $('#jQGridProductList').jqGrid('getCell', selectedrowid, 'PhotoUrl');
        if (Value.trim() == "a") {
            Unit = $('#jQGridProductList').jqGrid('getCell', selectedrowid, 'Unit1');
            Qty = $('#jQGridProductList').jqGrid('getCell', selectedrowid, 'Qty1');
            Price = $('#jQGridProductList').jqGrid('getCell', selectedrowid, 'Price1');
            Desc = $('#jQGridProductList').jqGrid('getCell', selectedrowid, 'Desc1');
        }

        if (Value.trim() == "b") {
            Unit = $('#jQGridProductList').jqGrid('getCell', selectedrowid, 'Unit2');
            Qty = $('#jQGridProductList').jqGrid('getCell', selectedrowid, 'Qty2');
            Price = $('#jQGridProductList').jqGrid('getCell', selectedrowid, 'Price2');
            Desc = $('#jQGridProductList').jqGrid('getCell', selectedrowid, 'Desc2');
        }


        if (Value.trim() == "c") {
            Unit = $('#jQGridProductList').jqGrid('getCell', selectedrowid, 'Unit3');
            Qty = $('#jQGridProductList').jqGrid('getCell', selectedrowid, 'Qty3');
            Price = $('#jQGridProductList').jqGrid('getCell', selectedrowid, 'Price3');
            Desc = $('#jQGridProductList').jqGrid('getCell', selectedrowid, 'Desc3');
        }
        Variation = Value;
        $.ajax({
            type: "POST",
            data: '{"ProductId":"' + PId + '", "Qty": "' + Qty + '","VariationId": "' + Variation + '"}',
            url: "usercombo.aspx/InsertTemp",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == 0) {

                    alert("Insertion Failed.Combo with duplicate name already exists.");
                    return;
                }
                BindTempComboDetail();    
                var dialogDiv = $('#dvPopupProduct');
                dialogDiv.dialog("option", "position", [350, 200]);
                dialogDiv.dialog('close');               
                return false;
               
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });
    }

    function InsertUpdate() {
        if (!validateForm("frmCombo")) {
            return;
        }
        var Id = m_ComboId;
        var Title = $("#txtTitle").val();
        if ($.trim(Title) == "") {
            $("#txtTitle").focus();
            return;
        }
        var photourl = "";
        var fileUpload = $("#FileUpload1").get(0);
        var files = fileUpload.files;

        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            if (files[i].name.length > 0) {
                photourl = files[i].name;
            }

            data.append(files[i].name, files[i]);
        }

        if (Id == 0 && photourl == "") {
            alert("Please select Image for Combo");
            return;
        }
        var TotalItems = $("#sp_TotalItems").html();
        if (TotalItems.trim() == "" || TotalItems.trim() == 0) {
            alert("Please select atleast one Product");
            return;
        }
        var options = {};
        options.url = "handlers/FileUploader.ashx";
        options.type = "POST";
        options.async = false;
        options.data = data;
        options.contentType = false;
        options.processData = false;
        options.success = function (msg) {

            photourl = msg;

        };
        options.error = function (err) { };
        $.ajax(options);
        $.uiLock('');
        if (photourl.length > 0) {
            ComboPhotoUrl = photourl;
        }
        $.ajax({
            type: "POST",
            data: '{"ComboId":"' + Id + '", "Title": "' + Title + '","PhotoUrl":"' + ComboPhotoUrl + '"}',
            url: "usercombo.aspx/Insert",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == 0) {

                    alert("Insertion Failed.Combo with duplicate name already exists.");
                    return;
                }

                if (Id == "0") {
                    jQuery("#jQGridProductList").GridUnload();
                    ResetControls();
                    BindGrid();
                    alert("Combo is added successfully.");
                }
                else {
                    jQuery("#jQGridProductList").GridUnload();
                    ResetControls();
                    BindGrid();
                    alert("Combo is Updated successfully.");
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });
    }
    $(document).on("click", "div[name='cartminus']", function (event) {
        var pid = $(this).attr("id");
        var arrPid = pid.split('_');
        var vid = $(this).attr("v");
        var qty = 1;
        var st = "m";
        $("#cq_" + arrPid[1] + vid).html("<img src='images/progressbar.gif' style='margin-top:4px' alt='...'/>");
        ATC(pid, vid, qty, st);
    });

    $(document).on("click", "div[name='cartadd']", function (event) {
        var pid = $(this).attr("id");
        var arrPid = pid.split('_');
        var vid = $(this).attr("v");
        var qty = 1;
        var st = "p";
        $("#cq_" + arrPid[1] + vid).html("<img src='images/progressbar.gif' style='margin-top:4px'  alt='...'/>");
        ATC(pid, vid, qty, st);
    });

    function BindProductList(CatId) {
        jQuery("#jQGridProductList").GridUnload();
        jQuery("#jQGridProductList").jqGrid({
            url: 'handlers/usercomboproducts.ashx?CatId=' + CatId,
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",


            colNames: ['ProductId', 'Image', 'Name', 'Unit1', 'QTY1', 'Price1', 'MRP1', 'Unit1Desc', 'Unit2', 'QTY2', 'Price2', 'MRP2', 'Unit2Desc', 'Unit3', 'QTY3', 'Price3', 'MRP3', 'Unit3Desc'],
            colModel: [
                       { name: 'ProductId', key: true, index: 'ProductId', width: '100px', stype: 'text', sorttype: 'int', hidden: true },
                         { name: 'PhotoUrl', index: 'PhotoUrl', width: '60px', align: "center", editable: true, formatter: imageFormat, unformat: imageUnFormat },
                        { name: 'Name', index: 'Name', width: '190px', stype: 'text', sortable: true, editable: true, editrules: { required: true } },

                        { name: 'Unit1', index: 'Unit1', width: '50px', stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true }, hidden: true },
   		             { name: 'Qty1', index: 'Qty1', width: '50px', stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
   		               { name: 'Price1', index: 'Price1', width: '50px', stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
   		               { name: 'Mrp1', index: 'Mrp1', width: '50px', stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                        { name: 'Desc1', index: 'Desc1', width: '50px', stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },

                      { name: 'Unit2', index: 'Unit2', width: '50px', stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true }, hidden: true },
   		             { name: 'Qty2', index: 'Qty2', width: '50px', stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
   		               { name: 'Price2', index: 'Price2', width: '50px', stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
   		               { name: 'Mrp2', index: 'Mrp2', width: '50px', stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                        { name: 'Desc2', index: 'Desc2', width: '50px', stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },

                     { name: 'Unit3', index: 'Unit3', width: '50px', stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true }, hidden: true },
   		             { name: 'Qty3', index: 'Qty3', width: '50px', stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
   		               { name: 'Price3', index: 'Price3', width: '50px', stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
   		               { name: 'Mrp3', index: 'Mrp3', width: '50px', stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                        { name: 'Desc3', index: 'Desc3', width: '50px', stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
            ],
            rowNum: 10,

            mtype: 'GET',
            toolbar: [true, "top"],
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridProductListPager',
            sortname: 'ProductId',
            viewrecords: true,
            height: "100%",
            width: "500px",
            sortorder: 'asc',
            caption: "Product List",
            ignoreCase: true,
            editurl: 'handlers/ManageComboProducts.ashx',
        });

        function imageFormat(cellvalue, options, rowObject) {
            return '<img src="../ProductImages/T_' + cellvalue + '" style="width:20px;height:20px"/>';
        }
        function imageUnFormat(cellvalue, options, cell) {
            return $('img', cell).attr('src');
        }


        $("#jQGridProductList").jqGrid('setGridParam',
{
    onSelectRow: function (rowid, iRow, iCol, e) {
        ProductId = 0;
        selectedrowid = rowid;
        ProductId = $('#jQGridProductList').jqGrid('getCell', rowid, 'ProductId');
        ProductName = $('#jQGridProductList').jqGrid('getCell', rowid, 'Name');
        PhotoUrl = $('#jQGridProductList').jqGrid('getCell', rowid, 'PhotoUrl');
        var Unit1 = $('#jQGridProductList').jqGrid('getCell', rowid, 'Unit1');


        var Unit2 = $('#jQGridProductList').jqGrid('getCell', rowid, 'Unit2');
        var Unit3 = $('#jQGridProductList').jqGrid('getCell', rowid, 'Unit3');
        var Unit1Desc = $('#jQGridProductList').jqGrid('getCell', rowid, 'Desc1');

        var Unit2Desc = $('#jQGridProductList').jqGrid('getCell', rowid, 'Desc2');
        var Unit3Desc = $('#jQGridProductList').jqGrid('getCell', rowid, 'Desc3');
        var Price1 = $('#jQGridProductList').jqGrid('getCell', rowid, 'Price1');
        var Price2 = $('#jQGridProductList').jqGrid('getCell', rowid, 'Price2');
        var Price3 = $('#jQGridProductList').jqGrid('getCell', rowid, 'Price3');
        var html = "";

        if (Unit1 != "") {
            html = html + "<tr><td><input type='radio' name='rbVariation' value='a' checked='checked'>" + Unit1Desc + "</td><td style='padding-left:5px'>Rs. "+ Price1+"</td></tr>";


        }

        if (Unit2 != "") {
            html = html + "<tr><td><input type='radio' name='rbVariation' value='b'>" + Unit2Desc + "</td><td style='padding-left:5px'>Rs. " + Price2 + "</td></tr>";


        }

        if (Unit3 != "") {
            html = html + "<tr><td><input type='radio' name='rbVariation' value='c'>" + Unit3Desc + "</td><td style='padding-left:5px'>Rs. " + Price3 + "</td></tr>";


        }

        $("#tbProductVariation").html(html);



        $('#dvPopupProduct').dialog(
      {
          autoOpen: false,

          width: 250,
          height: 200,
          resizable: false,
          modal: true,

      });
        linkObj = $(this);
        var dialogDiv = $('#dvPopupProduct');
        dialogDiv.dialog("option", "position", [750, 200]);
        dialogDiv.dialog('open');
        return false;
        TakeMeTop();
    }
});

        var DataGrid = jQuery('#jQGridProductList');
        DataGrid.jqGrid('setGridWidth', '250');

        var $grid = $("#jQGridProductList");
        // fill top toolbar
        $('#t_' + $.jgrid.jqID($grid[0].id))
            .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
        $("#globalSearchText").keypress(function (e) {
            var key = e.charCode || e.keyCode || 0;
            if (key === $.ui.keyCode.ENTER) { // 13
                $("#globalSearch").click();
            }
        });
        $("#globalSearch").button({
            icons: { primary: "ui-icon-search" },
            text: false
        }).click(function () {
            var postData = $grid.jqGrid("getGridParam", "postData"),
                colModel = $grid.jqGrid("getGridParam", "colModel"),
                rules = [],
                searchText = $("#globalSearchText").val(),
                l = colModel.length,
                i,
                cm;
            for (i = 0; i < l; i++) {
                cm = colModel[i];
                if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                    rules.push({
                        field: cm.name,
                        op: "cn",
                        data: searchText
                    });
                }
            }
            postData.filters = JSON.stringify({
                groupOp: "OR",
                rules: rules
            });
            $grid.jqGrid("setGridParam", { search: true });
            $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
            return false;
        });

        $('#jQGridProductList').jqGrid('navGrid', '#jQGridProductListPager',
                   {
                       refresh: false,
                       edit: false,
                       add: false,
                       del: false,
                       search: false,
                       searchtext: "Search",
                       addtext: "Add",
                   },

                   {//SEARCH
                       closeOnEscape: true

                   }

                     );


    }

    $(document).ready(
    function () {
        BindGrid();

        $("#btnClose").click(
            function () {
                var dialogDiv = $('#dvPopupGrid');
                dialogDiv.dialog("option", "position", [350, 200]);
                dialogDiv.dialog('close');
                $("#<%=ddlCategory.ClientID%> option[value= 0 ]").prop("selected", true);
            });
        $("#<%=ddlCategory.ClientID%>").change(
        function () {
            BindProductList($("#<%=ddlCategory.ClientID%>").val());
            $('#dvPopupGrid').dialog(
          {
              autoOpen: false,

              width: 350,
              height: 300,
              resizable: false,
              modal: true,

          });
            linkObj = $(this);
            var dialogDiv = $('#dvPopupGrid');
            dialogDiv.dialog("option", "position", [750, 200]);
            dialogDiv.dialog('open');
            return false;         

           }

       );
        $("#btnCancelQty").click(
function () {
    var dialogDiv = $('#dvPopupProduct');
    dialogDiv.dialog("option", "position", [500, 200]);
    dialogDiv.dialog('close');
    return false;

}
);
        $("#btnSelectQty").click(
function () {
    BindMainList();
}
);
     


        $("#btnAdd").click(
        function () {

            m_ComboId = 0;
            InsertUpdate();
        }
        );


        $("#btnUpdate").click(
        function () {
            InsertUpdate();
        }
        );
        $("#btnDelete").click(
       function () {
           DeleteCombo();
       }
       );
        $('#txtTitle').focus();
        $('#txtTitle').keypress(function (event) {


            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {

                InsertUpdate();
            }


        });


        $("#btnReset").click(
        function () {

            ResetControls();

        }
        );


    }
    );

</script>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
<div id="content" style="padding:6px">
       			<div id="rightnow">
                    <h3 class="reallynow">
                        <span>Add/Update Combo</span>
                     
                        <br />
                    </h3>
				  <div style="margin:5px;border:solid 1px silver;float:left;width:650px;padding:5px;box-shadow:1px 1px 2px black;padding-top:15px">

                   <table>
                   <tr>
                   <td>
                   <table cellpadding="0" cellspacing="0" border="0" id="frmCombo" style="width:100%">

                   
                     <tr><td class="headings" style="padding-bottom:10px">Title:</td><td style="padding-bottom:10px">  <input type="text"  name="txtTitle" class="inputtext"    data-index="1" id="txtTitle" style="width: 264px" /></td></tr>
                                       
                        <tr><td class="headings" style="padding-bottom:10px">Category Level 1:</td><td style="padding-bottom:10px;padding-left :8px">  
                     
                     <asp:DropDownList ID="ddlCategory"    runat="server"  style="width:265px;height:35px"></asp:DropDownList>
                     </td></tr>
                    
     <tr><td class="headings">Image:</td><td><input type="file" id="FileUpload1"  class="btn btn-success" />
    <button id="btnUpload"  style="display:none">Upload</button></td></tr>
                     </table>
                   
                   </td>
                   <td valign="top">
                       <table>
                           <tr>
                               <td>
                                       <div id="dvPopupGrid"  style="display:none;">
                    	          <table id="jQGridProductList" >
    </table>
    <div id="jQGridProductListPager" >
    </div>
                                           <div  style="padding-top:10px" id="btnClose"  class="btn btn-success">Close</div>
                                           </div>

                               </td>
                           </tr>
                        
                           <tr>
                               <td>
                                   <div id="dvPopupProduct"  style="display:none;">

<h3 class="reallynow">
                <span>Product Options</span>
                <br />
            </h3>
<table id="tbProductVariation">
 
</table>
 <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td style="padding-right:10px;padding-top:5px"> <div id="btnSelectQty"  class="btn btn-success">Add</div></td><td><div id="btnCancelQty" class="btn btn-success" >Cancel</div></td>
                                               
                                            </tr>
                                            </table>
</div>
                                                                 </td>
                           </tr>
                       </table>
            
  
                  </td>
                   </tr>

                   <tr>
                   <td colspan="100%">
                   
                   <table>
                  
  
                       <tr><td>
                            <div class="col-md-12">
<span style="color:#58595b;font-size:16px;font-weight:bold;">Your Basket (<span id="sp_TotalItems"></span> Items)</span>
 <hr  style="margin:2px;"/>

 <table style="width:100%">
 <thead>
 <tr style="background:black;color:White;font-weight:bold;"><td></td><td style="padding:10px;width:300px">ITEM</td><td>UNIT PRICE</td><td style="padding-left:8px">QTY</td><td>SUB TOTAL</td></tr>
 </thead>
 <tbody id="cartcontainer">
 </tbody>
 </table>


 </div>
                           </td></tr>
                                           <tr>

<td>
 <div style="margin:5px;border:solid 1px silver;float:right;width:160px;padding:5px;box-shadow:1px 1px 2px black">
<table style="float:right;margin-right:0px">
 <tr><td style="font-weight:bold;text-align:right;padding-right:10px">Sub Total: </td><td> <span id="subtotal"></span></td></tr>
 
 </table>
  </div>
</td>
 </tr>
                       <tr>
                                            
                                            <td colspan="100%" style="padding-bottom:15px" >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd"   class="btn btn-success" >Save Combo</div></td>
                                            <td style="padding-left:17px"><div id="btnUpdate"   class="btn btn-success" style="display:none;" >Update Combo</div></td>
                                            <td style="padding-left:17px"><div id="btnReset"  class="btn btn-success" style="display:none;width:77px;" >Cancel</div></td>
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>
                        </table>
                   
                   </td>
                   </tr>
                   </table>


                   
                    			 
       
                    </div>
			  </div>
               


           <div style="margin:5px;border:solid 1px silver;float:left;width:650px;padding:5px;box-shadow:1px 1px 2px black;padding-top:15px">
                    <h3 class="reallynow"><b>
                        <span style="padding-left:29px">Manage Combo </span>
                      </b>
                        <br />
                    </h3>
				    <div class="youhave" style="padding-left:30px;padding-top:15px">
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
      <table>
               <tr>
                                            <td style="padding-top:20px"> <div id="btnDelete"  class="btn btn-success" >Delete Combo</div></td>
                   <td>&nbsp;</td>
               </tr>
      </table>
                    </div>
			  </div>

            </div>
</form>

            <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/usercombo.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Combo Id', 'Image', 'Title'],
                        colModel: [
                                    { name: 'ComboId', key: true, index: 'ComboId', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                    { name: 'PhotoUrl', index: 'PhotoUrl', width: '80px', align: "center", editable: true, formatter: imageFormat, unformat: imageUnFormat },
                                    { name: 'Title', index: 'Title', width: '320', stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                          ],
                        rowNum: 10,

                        mtype: 'GET',
                        toolbar: [true, "top"],
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'ComboId',
                        viewrecords: true,
                        height: "100%",
                        width: "500px",
                        sortorder: 'asc',
                        caption: "Combo List",
                        ignoreCase: true,
                        editurl: 'handlers/usercombo.ashx',
                        rowattr: function (rd) {
                            if (rd.IsActive == false) {
                                return { "class": "myAltRowClass" };
                            }
                        }
                    });

                    function imageFormat(cellvalue, options, rowObject) {
                        return '<img alt="N/A" src="../ComboImages/T_' + cellvalue + '" style="width:20px;height:20px"/>';
                    }
                    function imageUnFormat(cellvalue, options, cell) {
                        return $('img', cell).attr('src');
                    }

                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                  
                    m_ComboId = 0;
                     validateForm("detach");
                    var txtTitle = $("#txtTitle");
                    m_ComboId = $('#jQGridDemo').jqGrid('getCell', rowid, 'ComboId');
                    ComboPhotoUrl = $('#jQGridDemo').jqGrid('getCell', rowid, 'PhotoUrl');
                    ComboPhotoUrl = ComboPhotoUrl.toString().substring(17, ComboPhotoUrl.toString().length);
                    txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Title'));
                    $.ajax({
                        type: "POST",
                        data: '{ "ComboId": "' + m_ComboId + '"}',
                        url: "usercombo.aspx/GetProductDetail",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {
                            var obj = jQuery.parseJSON(msg.d);
                            BindTempComboDetail();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {

                            $.uiUnlock();
                        }



                    });
                    txtTitle.focus();
                    $("#btnAdd").css({ "display": "none" });
                    $("#btnUpdate").css({ "display": "block" });
                    $("#btnReset").css({ "display": "block" });
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '400');

            var $grid = $("#jQGridDemo");
                    // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                       {
                           refresh: false,
                           edit: false,
                           add: false,
                           del: false,
                           search: false,
                           searchtext: "Search",
                           addtext: "Add",
                       },

                       {//SEARCH
                           closeOnEscape: true

                       }

                         );



        }





            </script>

</asp:Content>


