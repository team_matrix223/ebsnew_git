﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class user_managewallet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string GetBalance()
    {
        Int32 uId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());
        Wallet objWallet = new Wallet()
        {

            UserId = uId,
          

        };
        new WalletBLL().GetBalanceByUser(objWallet);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            Balance = objWallet,
            
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string BindGifts()
    {
             
        string GiftHTML = new ReimbursementGiftsBLL().GetGiftsHTML();

        return GiftHTML;
    }



    [WebMethod]
    public static string Insert(Int32 GiftId)
    {
        Int32 uId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());
        Wallet objWallet = new Wallet()
        {

            UserId = uId,
            GiftId = GiftId,

        };
        int status = new WalletBLL().InsertUpdate(objWallet);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
           
            Status = status
        };
        return ser.Serialize(JsonData);
    }
    
}