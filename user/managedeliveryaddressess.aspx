﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/main.master" AutoEventWireup="true" CodeFile="managedeliveryaddressess.aspx.cs" Inherits="user_managedeliveryaddressess" %>

<asp:Content ID="cntBreadCrumbs" ContentPlaceHolderID="cntBreadCrumbs" runat="server">
      <div class="breadcrumbs_container">
            <article class="breadcrumbs">
                <a href="dashboard.aspx">Dashboard</a>
                <div class="breadcrumb_divider">
                </div>
                <a href="managedeliveryaddressess.aspx" class="current">Delivery Addresses</a>
            
            </article>
        </div>


</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
      <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
 <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
  <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
  <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
   <script src="js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>


   <form id="frmMain" runat="server">
   <asp:HiddenField ID="hdnId" runat="server"/>
<div id="content" style="padding:6px">
       			
				   <div class="youhave" style="padding-left:30px;">
            <div  class="col-md-12" style="margin-bottom:10px;margin-top: 0px; border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:5px 5px 5px">
 
            
                 <table cellpadding="0" cellspacing="0" width="99%" >
<tr>
<td  class="headingStyle" colspan="100%">ADD/EDIT DELIVERY ADDRESS</td>
</tr>

<tr><td > First Name:</td><td>
    <asp:TextBox ID="txtFirstName" runat="server" class="inputtext"></asp:TextBox></td><td style="padding-left:10px;padding-right:10px">
    <asp:RequiredFieldValidator ID="reqFirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="*" Font-Bold="True" ForeColor="#990000"  ValidationGroup="a"></asp:RequiredFieldValidator>
    </td><td>Last Name:</td><td>
        <asp:TextBox ID="txtLastName" runat="server" class="inputtext"></asp:TextBox></td><td></td></tr>
<tr><td> Mobile:</td><td>
    <asp:TextBox ID="txtMobileNo" runat="server" class="inputtext"></asp:TextBox></td>
     <td style="padding-left:10px;padding-right:10px">
        <asp:RequiredFieldValidator ID="reqMobileNo" runat="server" ErrorMessage="*" ControlToValidate="txtMobileNo" Font-Bold="True" ForeColor="#990000" ValidationGroup="a"></asp:RequiredFieldValidator></td>
    <td>Telephone:</td><td><asp:TextBox ID="txtTelephone" runat="server"  class="inputtext"></asp:TextBox></td>   <td></td>                                                                                                                                                                                       
                                                                                    </tr>
<tr><td> City:</td><td>
    <asp:DropDownList ID="ddlCities" runat="server" Width="100%" style="margin-left:10px;height:30px"></asp:DropDownList> </td>
     <td style="padding-left:10px;padding-right:10px">
        <asp:RequiredFieldValidator ID="reqCities" runat="server" ErrorMessage="*" ControlToValidate="ddlCities" InitialValue="--Select City--" Font-Bold="True" ForeColor="#990000" ValidationGroup="a"></asp:RequiredFieldValidator></td>

    <td>Street:</td><td>
            <asp:TextBox ID="txtStreet" runat="server"  class="inputtext"></asp:TextBox></td><td></td></tr>
<tr><td> Area:</td><td>
    <asp:TextBox ID="txtArea" runat="server" class="inputtext"></asp:TextBox></td><td></td><td>PinCode:</td><td>
        <asp:TextBox ID="txtPinCode" runat="server" class="inputtext"></asp:TextBox> </td><td></td></tr>
<tr><td> Address:</td><td>
    <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" class="inputtext"></asp:TextBox></td>
    <td style="padding-left:10px">
        <asp:RequiredFieldValidator ID="regAddress" runat="server" ErrorMessage="*" ControlToValidate="txtAddress" Font-Bold="True" ForeColor="#990000" ValidationGroup="a"></asp:RequiredFieldValidator></td>
    <td>
        <asp:CheckBox ID="chkprimary"  runat="server" Text= "PrimaryAddress" />
    </td><td></td><td></td>
</tr>
<tr><td></td><td><table><tr><td style="padding-right:10px;padding-top:10px" >
     <asp:Button ID="btnAdd" runat="server"  class="btn btn-success"  Text="Add Information" OnClick="btnAdd_Click" ValidationGroup="a" /></td>
    <td style="padding-right:10px;padding-top:10px" >
     <asp:Button ID="btn_Cancel" runat="server"  class="btn btn-success" Text="Cancel" Visible="false" OnClick="btn_Cancel_Click"  />
                            </td></tr></table>
   
</td></tr>
</table>
 </div>

 <div  class="col-md-12" style="margin-bottom:20px;margin-top: 0px; border-radius: 5px; border:solid 1px silver ;background:linear-gradient(to bottom, #fff 0px, #fff 73%, #f6f6f8 89%, #e4e4e4 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);box-shadow:5px 5px 5px rgba(0, 0, 0, 0.2);padding:5px 5px 5px">
   <table style="width:100%">
   
   <tr>
<td  class="headingStyle" colspan="100%">DELIVERY ADDRESS LIST</td>
</tr>
   <tr><td>
       <asp:GridView ID="Grid_User" Width="100%" runat="server" 
           AutoGenerateColumns ="False"          CellPadding="4" ForeColor="#333333" 
           GridLines="None"    OnRowDataBound="Grid_User_RowDataBound" 
           OnRowEditing="Grid_User_RowEditing" OnRowDeleting="Grid_User_RowDeleting" >
           <AlternatingRowStyle BackColor="White"   />
     <Columns>
     
     <asp:TemplateField HeaderText ="First Name" >
      <ItemTemplate >
        <asp:HiddenField ID="hdId" Value='<%# Eval("DeliveryAddressId") %>'  runat="server" />
 
          <asp:Label ID="lblFirstName" Runat="Server" 
                 Text='<%# Eval("RecipientFirstName") %>' />
                 </ItemTemplate> 
     </asp:TemplateField>
     <asp:TemplateField HeaderText ="Last Name">
      <ItemTemplate>
         <asp:Label ID="lblLastName" Runat="Server" 
                 Text='<%# Eval("RecipientLastName") %>' />
                 </ItemTemplate>
     </asp:TemplateField>
          <asp:TemplateField HeaderText ="MobileNo">
      <ItemTemplate>
         <asp:Label ID="lblMobileNo" Runat="Server" 
                 Text='<%# Eval("MobileNo") %>' />
                 </ItemTemplate>
     </asp:TemplateField>
     <asp:TemplateField HeaderText ="Telephone">
      <ItemTemplate>
         <asp:Label ID="lblTelephone" Runat="Server" 
                 Text='<%# Eval("Telephone") %>' />
                 </ItemTemplate>
     </asp:TemplateField>
      <asp:TemplateField HeaderText ="CityId" visible="false" >
       <ItemTemplate>
        <asp:Label ID="lblCityId" Runat="Server" 
                 Text='<%# Eval("CityId") %>' />
                 </ItemTemplate>
     </asp:TemplateField>
         <asp:TemplateField HeaderText ="City"  >
       <ItemTemplate>
        <asp:Label ID="lblCityName" Runat="Server" 
                 Text='<%# Eval("CityName") %>' />
                 </ItemTemplate>
     </asp:TemplateField>
         <asp:TemplateField HeaderText ="Area" Visible="false" >
       <ItemTemplate>
        <asp:Label ID="lblArea" Runat="Server" 
                 Text='<%# Eval("Area") %>' />
                 </ItemTemplate>
     </asp:TemplateField>
          <asp:TemplateField HeaderText ="Street" Visible="false"  >
       <ItemTemplate>
        <asp:Label ID="lblStreet" Runat="Server" 
                 Text='<%# Eval("Street") %>' />
                 </ItemTemplate>
     </asp:TemplateField>
           <asp:TemplateField HeaderText ="Address" Visible="false"  >
       <ItemTemplate>
        <asp:Label ID="lblAddress" Runat="Server" 
                 Text='<%# Eval("Address") %>' />
                 </ItemTemplate>
     </asp:TemplateField>
          <asp:TemplateField HeaderText ="PinCode" Visible="false" >
       <ItemTemplate>
        <asp:Label ID="lblPinCode" Runat="Server" 
                 Text='<%# Eval("PinCode") %>' />
                 </ItemTemplate>
     </asp:TemplateField>

            <asp:TemplateField HeaderText ="PrimaryAddress"    >
       <ItemTemplate>
        <asp:CheckBox ID="chkprimaryadress" Runat="Server" 
                 Checked ='<%# Eval("IsPrimary") %>'  Enabled="false"    />
                 </ItemTemplate>
     </asp:TemplateField>
    <asp:TemplateField HeaderText ="Edit">
    <ItemTemplate>
        <asp:LinkButton ID="lkEdit" ForeColor="Black"  Font-Bold="true" style="text-decoration:underline" runat="server" CommandName="Edit" CausesValidation="false">Edit</asp:LinkButton>
    </ItemTemplate>
    </asp:TemplateField>
          <asp:TemplateField HeaderText ="Delete">
    <ItemTemplate>
        <asp:LinkButton ID="lkDelete"  ForeColor="Black" Font-Bold="true" style="text-decoration:underline"  runat="server" CommandName="Delete" CausesValidation="false" OnClientClick="return confirm('Are you sure to delete?')">Delete</asp:LinkButton>
    </ItemTemplate>
    </asp:TemplateField>
     </Columns>
         <FooterStyle BackColor="#990000" ForeColor="White" Font-Bold="True" />
         <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
         <PagerStyle BackColor="#FFCC66" ForeColor="#333333" 
             HorizontalAlign="Center" />
           <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
         <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" Font-Bold="True" />
         <SortedAscendingCellStyle BackColor="#FDF5AC" />
         <SortedAscendingHeaderStyle BackColor="#4D0000" />
         <SortedDescendingCellStyle BackColor="#FCF6C0" />
         <SortedDescendingHeaderStyle BackColor="#820000" />
     </asp:GridView>  
                           </td></tr></table>
 </div>
                    </div>
			  </div>
               
       </form>
</asp:Content>
