﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class user_usercombo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindCategories();
                    ddlCategory.SelectedValue = "0";
                }
            }
            catch (Exception ex)
            {
            }
        }

    }
    [WebMethod]
    public static string InsertTemp(int ProductId, int Qty, string  VariationId)
    {
       Int32 UserId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());
        UserComboDetail objCombo = new UserComboDetail()
        {
            CustomerId = UserId ,
            ProductId = ProductId,
            Qty = Qty,
            VariationId = VariationId,
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        int status = new UserComboBLL().InsertTemp(objCombo);
        var JsonData = new
        {
            Status = status
        };
        return ser.Serialize(JsonData);

    }
    [WebMethod]
    public static string Insert(int ComboId, string Title, string PhotoUrl)
    {
        Int32 UserId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());
        UserCombo objCombo = new UserCombo()
        {
            ComboId = ComboId,
            CustomerId = UserId,
            Title = Title.Trim(),
            IsActive = true ,
            PhotoUrl = PhotoUrl,
        };
           JavaScriptSerializer ser = new JavaScriptSerializer();
        int status = new UserComboBLL().InsertUpdate(objCombo);
        var JsonData = new
        {
            Role = objCombo,
            Status = status
        };
        return ser.Serialize(JsonData);

    }
    public void reset()
    {
        ddlCategory.SelectedValue = "0";
    }

    public void BindCategories()
    {
        try
        {
            ddlCategory.DataSource = new CategoryBLL().GetByLevel(1);
            ddlCategory.DataValueField = "CategoryId";
            ddlCategory.DataTextField = "Title";
            ddlCategory.DataBind();
            ListItem li = new ListItem();
            li.Text = "--Select Category--";
            li.Value = "0";
            ddlCategory.Items.Insert(0, li);

        }
        finally { }

    }


    [WebMethod]
    public static string DeleteCombo(Int64 ComboId)
    {
        Int64 UserId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());
        Int16 status = new UserComboBLL().DeleteCombo(ComboId);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            Status = status,
        };
        return ser.Serialize(JsonData);

    }
    [WebMethod]
    public static string DeleteTemp()
    {
        Int64 UserId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());
       Int16 status=  new UserComboBLL().DeleteTemp(UserId );
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            Status = status,
        };
        return ser.Serialize(JsonData);

    }
    [WebMethod]
    public static string GetProductDetail(int ComboId)
    {
        Int32 UserId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());
        UserComboDetail objCombo = new UserComboDetail() { ComboId = ComboId,CustomerId= UserId };

new UserComboBLL().GetProductDetailByID(objCombo);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            MasterData = objCombo,
        };
        return ser.Serialize(JsonData);

    }
    [WebMethod]
    public static string GetComboHTML()
    {
        Int64 UserId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());
        Calc objCalc = new Calc();
        string cart = new UserComboBLL().GetUserComboHTML(UserId, objCalc);

        var JsonData = new
        {

            DC = objCalc.DeliveryCharges,
            ST = objCalc.SubTotal,
            NA = objCalc.NetAmount,
            html = cart,
            TotalItems = objCalc.TotalItems
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string ATC(string pid, string vid, string qty, string st)
    {
        string m_Status = "Plus";
        string m_VariationId = "a";
        if (st == "m")
        {
            m_Status = "Minus";

        }

        if (vid == "a" || vid == "b" || vid == "c")
        {
            m_VariationId = vid;
        }

        Int16 IsError = 0;
        string[] arr_p = pid.Split('_');

        string ProductId = arr_p.Length > 1 ? Convert.ToString(arr_p[1]) : "";

        int m_Qty = CommonFunctions.ValidateQuantity(qty);
        if (!CommonFunctions.IsValidProductId(ProductId))
        {
            IsError = 1;
        }

        Calc objCalc = new Calc();
        UserComboDetail objCombo = new UserComboDetail();
        string html = "";
        Int32 UserId = Convert.ToInt32(HttpContext.Current.Session[Constants.UserId].ToString());
        if (IsError == 0)
        {
            objCombo.ProductId = Convert.ToInt64(ProductId);
            objCombo.CustomerId = UserId ;
            objCombo.VariationId = m_VariationId;
            objCombo.Qty = m_Qty;
            html = new UserComboBLL().UserComboIncrDecr(objCombo, m_Status, objCalc, 2);

        }



        var JsonData = new
        {
            pid = ProductId,
            qty = objCombo.Qty,
            vid = vid,
            error = IsError,
            cartQtyHTML = html,
            Calc = objCalc
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);

    }
}