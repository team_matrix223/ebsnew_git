﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class wishlists : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
        GetWishlist();

    }

    public void GetWishlist() {

    
     DataSet ds = new ProductsBLL().GetWishLIst(Convert.ToInt32(HttpContext.Current.Session[Constants.UserId]));
        rpt_wishlist.DataSource = ds;
        rpt_wishlist.DataBind();

    }

    [WebMethod]
    public static void delete(string req,int Variationid)
    {
        int userid = HttpContext.Current.Session[Constants.UserId] != null ? Convert.ToInt32(HttpContext.Current.Session[Constants.UserId]) : 0;
  
            new ProductsDAL().InsertWishList(req, 0, Variationid, userid.ToString());
     


    }
}