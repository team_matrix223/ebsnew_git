﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="testreport.aspx.cs" Inherits="testreport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
            <div class="col-md-2 col-sm-2 col-xs-12 bill-detailed-report-cst-col">
		<div class="bill-detailed-button-section">
			<asp:Button ID="btnGetRecords" runat="server" CssClass="btn btn-success"  Text="Submit" onclick="btnGetRecords_Click"/>
			<button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button>
		</div>
	</div>

             <div class="sale_report">

                   <rsweb:ReportViewer ID="ReportViewer1" CssClass="bill-detailed-rpt-cls" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="1000px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1100px" PageCountMode="Actual">

    </rsweb:ReportViewer>
       </div>
        </div>
    </form>
</body>
</html>
