﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class login : System.Web.UI.Page
{
    string Qs = "";
	protected void Page_Load(object sender, EventArgs e)
	{
        Qs = Request.QueryString["q"] != null ? Request.QueryString["q"] : "0";
        if (Qs=="1")
        {
            Response.Write("<script>alert('Password Change Successfully.Please Login Again!')</script>");
        }
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        Users objuser = new Users()
        {
            EmailId = username.Value.Trim(),
            Password = password.Value.Trim()
        };

        string status = new UsersBLL().UserLoginCheck(objuser, HttpContext.Current.Session.SessionID);


        if (status.ToString() == "-3")
        {


            Response.Redirect("forgotpassword.aspx");
        }
        if (status.ToString() == "-1")
        {
            dv_alert.Visible = true;
            lblLogin.Text = "** Invalid User Name";
            //  Response.Write("<script>alert('Invalid User Name');</script>");
            return;


        }
        else if (status.ToString() == "-2")
        {
            dv_alert.Visible = true;
            lblLogin.Text = "** Invalid Password";
            // Response.Write("<script>alert('Invalid Password');</script>");
            return;
        }
        else if (status.ToString() == "0")
        {
            dv_alert.Visible = true;
            lblLogin.Text = "** First Register,Then SignIn";
            //Response.Write("<script>alert('First Register,Then SignIn');</script>");
            return;

        }

        else
        {

            Session["Roles"] = status;
     
            Session[Constants.UserId] = status;
            Session[Constants.Email] = username.Value.Trim();
            Session[Constants.CustType] = "Customer";
            Response.Redirect("/index.aspx");

            //if (ReturnUrl != "")
            //{
            //    Response.Redirect("../delivery.aspx");
            //}
            //else
            //{
            //    Response.Redirect("/index.aspx");
        }
    }
}