﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Master_Page : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindProductCategories();
            //try
            //{
            //    // BindSelProductCategories();
            //    HttpContext.Current.Session[Constants.CartSession] = HttpContext.Current.Session[Constants.CartSession] != null ? HttpContext.Current.Session[Constants.CartSession] : HttpContext.Current.Session.SessionID;
            //}
            //catch
            //{
            //    HttpContext.Current.Session[Constants.CartSession] = HttpContext.Current.Session.SessionID;
            //}
            if (Session[Constants.CartSession] != null)
            {
                HttpContext.Current.Session[Constants.CartSession] = HttpContext.Current.Session[Constants.CartSession];
            }
            else
            {
                HttpContext.Current.Session[Constants.CartSession] = HttpContext.Current.Session.SessionID;
            }
            if (Session[Constants.UserId] != null)
            {
                li_login.Visible = false;
                li_register.Visible = false;
                li_wishlist.Visible = true;
                li_myorder.Visible = true;
                li_manageacct.Visible = true;
                li_signout.Visible = true;
            }
            else
            {
                li_login.Visible = true;
                li_register.Visible = true;
                li_wishlist.Visible = false;
                li_myorder.Visible = false;
                li_manageacct.Visible = false;
                li_signout.Visible = false;
            }
     
          
        }

    }
    public void BindProductCategories()
    {
        string strCat = "";
        List<Category> lst = new CategoryBLL().GetByParentId(0);
        string subcat = "";
        foreach (var item in lst)
        {

			
		subcat = BindProductSubCategories(item.CategoryId);
            if (subcat!= "<ul></ul>")
            {
            strCat += "<li class='responsiveFlyoutMenu_levelTwoItem cat-li' data-subnav-level='subnav-level-two data-subnav-target='subnav-este-lauder'><a class='responsiveFlyoutMenu_levelTwoLink responsiveFlyoutMenu_levelTwo_brands-este-lauder responsiveFlyoutMenu_levelTwoLink_num responsiveFlyoutMenu_levelTwoLink_num-01' data-subnav-template='subnav-' href=list.aspx?c=" + item.CategoryId +" tabindex='-1' data-context='Estée Lauder' data-js-nav-level='2'><span class='responsiveFlyoutMenu_levelTwoLinkText'>" + item.Title + "</span></a><span class='cat-arrow' ><svg class='responsiveFlyoutMenu_chevronRight' width='24' height='24' viewBox='0 0 24 24'><polygon points = '9 16.137 13.22 12 9 7.863 10.39 6.5 16 12 10.39 17.5' /></svg></span >" + subcat + "</li>";
            }
        }
        catsubcat.Text = strCat;

	}
	


	public string BindProductSubCategories(int CategoryId)
    {
        string strSubCat = "";
        List<SubCategory> lst = new SubCategoryBLL().GetByCategoryId(CategoryId);
        strSubCat = "<ul class='ul-sub-cat'>";
        foreach (var item in lst)
        {
            strSubCat += "<li><a class='responsiveFlyoutMenu_levelTwoItem' data-subnav-level='subnav-level-two data-subnav-target='subnav-este-lauder'><a class='responsiveFlyoutMenu_levelTwoLink responsiveFlyoutMenu_levelTwo_brands-este-lauder responsiveFlyoutMenu_levelTwoLink_num responsiveFlyoutMenu_levelTwoLink_num-01' data-subnav-template='subnav-' href=list.aspx?s=" + item.SubCategoryId + " tabindex='-1' data-context='Estée Lauder' data-js-nav-level='2'><span>" + item.Title + "</span></a></li>";
        }

        strSubCat += "</ul>";
        return strSubCat;

   }
    void BindSelProductCategories()
    {
        int[] strIDs = new[] { 178, 180, 192 };
        repSelCategories.DataSource = new CategoryBLL().GetByParentId(0).Where(x => strIDs.Contains(x.CategoryId));
        repSelCategories.DataBind();
     

    }
}
