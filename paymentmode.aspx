﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="paymentmode.aspx.cs" Inherits="paymentmode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script>

        $(document).ready(function () {
            $("#rdo_online").click(function () {
                $("#rdo_cod").prop('checked', false)

            })

            $("#rdo_cod").click(function () {

                $("#rdo_online").prop('checked', false)
            })

            $("#btnmakepay").click(function () {
                $(this).hide();

            })
        })
    </script>
	<link href="customcss/paymentmode.css" rel="stylesheet" />
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h2 class="payment-heading">Payment Method</h2>
				<div class="form-horizontal payment-frm">
					 <div class="form-group">
						<div class="radio">
						  <label>
					<%--		<input type="radio" name="optionsRadios" id="" value="online" runat="server" checked>--%>
                              <asp:RadioButton ID="rdo_online" name="optionsRadios" ClientIDMode="Static"  runat="server" Checked="true"/>
							Credit Card, Debit Card, Netbanking, Wallets, UPI & More.
						  </label>
						</div>
						  <div class="radio" runat="server" id ="dv_rdo_cod">
						  <label>
					<%--		<input type="radio" name="optionsRadios" id="rdo_cod" runat="server" value="cod" >--%>
                                       <asp:RadioButton ID="rdo_cod"   name="optionsRadios" ClientIDMode="Static"   runat="server"/>
							Cash on Delivery
						  </label>
						</div>
                        
					  </div>
                    <div> <asp:Label ID="lblwarning" runat="server" Text="" ForeColor="Red"></asp:Label></div>
					<%--<div class="form-group">
						<div class="checkbox">
						  <label>
							<input type="checkbox" value="">
							My billing and shipping address are the same
						  </label>
						</div>
					</div>--%>
					<%--<div class="form-group">
						<p>Name</p>
						<p>Street Address</p>
						<p>City,State Pincode</p>
						<p>Country</p>
						<p>Mobile no</p>
					</div>--%>
					 <div class="form-group">
						<div class=" col-sm-12">
						 <%-- <button type="submit" class="btn ">Make Payment</button>--%>
                            <asp:Button ID="btnmakepay" ClientIDMode="Static" runat="server" CssClass="btn"  Text="Make Payment" OnClick="btnmakepay_Click" />
						</div>
					  </div>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12" style="display:none">
				<h2 class="order-head">Order Summary</h2>
				<div class="order-summary">
					<div class="order-cart">
						<div class="order-cart-subtotal">Cart Subtotal</div>
						<div class="order-cart-price">₹140.00</div>
						<div class="order-cart-subtotal">Shipping</div>
						<div class="order-cart-price">₹140.00</div>
					</div>
					<div class="order-tax">
						<div class="order-tax-div">Tax</div>
						<div class="order-tax-price">₹140.00</div>
						<div class="order-tax-div">CGST-12(6%)</div>
						<div class="order-tax-price">₹140.00</div>
						<div class="order-tax-div">SGST-12(6%)</div>
						<div class="order-tax-price">₹140.00</div>
					</div>
					<div class="order-tax-detail">
						<div class="order-tax-detail-div">Order Total Incl. Tax</div>
						<div class="order-tax-detail-price">₹140.00</div>
						<div class="order-tax-detail-div">Order Total Excl. Tax</div>
						<div class="order-tax-detail-price">₹140.00</div>
					</div>

					<%--<div class="order-summary-detail">
					
						<h3>1 Item in Cart</h3>
						<div class="order-details">
							<div class="order-img">
								<img src="images/product/080Feb021051051230PM9031img.png" class="small-img" />
							</div>
							<div class="order-desc">
								<h2>ADD GEL SILVER DIAMOND ROLLER GEL PEN </h2>
								<p>Qty:1</p>
							</div>
							<div class="order-price">₹140.00</div>
						</div>
					</div>--%>
				</div>
				
				
				<%--<div class="order-ship-to">
					<h2>Ship To:</h2>
					<div class="ship-address">
						<p>Name</p>
						<p>Street Address</p>
						<p>City,State Pincode</p>
						<p>Country</p>
						<p>Mobile no</p>
					</div>
				</div>--%>
			</div>

		</div>
	</div>
</asp:Content>

