﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class paymentmode : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
        decimal netamt = Convert.ToDecimal(HttpContext.Current.Session[Constants.NetAmount]);
        decimal FreeDelAmt = Convert.ToDecimal(HttpContext.Current.Session[Constants.FreeDelAmt]);
        if ((netamt/100)< FreeDelAmt)
        {
            dv_rdo_cod.Visible = false;
            lblwarning.Text = "COD is Only Available On Order Above ₹"+ FreeDelAmt + "";
        }

    }

    protected void btnmakepay_Click(object sender, EventArgs e)
    {
        btnmakepay.Visible = false;
        string PayMode = "";
        int status = 0;
        if (rdo_online.Checked)
        {
            PayMode = "Online Payment";
        }
        else if ((rdo_cod.Checked))
        {
            PayMode = "Cash On Delivery";
        }
        status = new OrderBLL().InsertOrder(Session.SessionID, Convert.ToInt32(Session[Constants.UserId]), "", PayMode);

        if (PayMode == "Online Payment")
        {
            Response.Redirect("/Payments/Payment.aspx?oid=" + status + "");
        }
        else if (PayMode == "Cash On Delivery")
        {
            Response.Redirect("/Payments/PaymentSuccess.aspx?oid=" + status + "");
        }
    }
}