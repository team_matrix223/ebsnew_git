﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master_Page.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<link href="customcss/login.css" rel="stylesheet" />
   <div class="middle-rail column-span24">
      <main role="main" id="mainContent" class="accountLogin" data-component="accountLogin">
         <div class="accountLogin_wrapper">
            <div class="accountLogin_card ">
               <div class="accountLogin_cardContent">
                  <h1 class="accountLogin_title">Existing Customers</h1>
                   	<div class="alert alert-danger" runat="server" id="dv_alert"  visible="false" role="alert">
  <asp:Label ID="lblLogin" runat="server" Text="" Font-Bold="true"></asp:Label>
</div>
					<div class="col-md-12 col-sm-12 col-xs-12">

						 <label class="accountLogin_label">
						 Email address
						 <input id="username" runat="server" required="required" data-e2e="usernameField" type="text" name="elysium_username" class="accountLogin_input" value="">
						 </label>
				   </div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						 <label class="accountLogin_label">
						 Password
						 <input id="password" runat="server" required="required" data-e2e="passwordField" name="elysium_password" class="accountLogin_input" type="password" data-show-password-target="">
						 </label>
						</div>
                     <input name="returnTo" value="" type="hidden">
                     <input type="hidden" name="csrf_token" value="10402904851274460190">
                     <%--<label class="showPasswordToggleComponent" data-component="showPasswordToggleComponent" data-hidden-message="Password is now hidden" data-visible-message="Password is now visible">
                     Show Password
                     <input type="checkbox" class="showPasswordToggleComponent_checkbox">
                     <span class="showPasswordToggleComponent_switch showpass"></span>
                     </label>--%>
                     <div class="forgottenPasswordModal">
                        <div class="modal" data-component="modal">
                           <div class="modal_trigger">
                              <button type="button" class="forgottenPasswordModal_trigger">
                              ForForgotten your password?
                              </button>
                           </div>
                        </div>
                     </div>
                <%--     <button id="login-submit" data-e2e="loginSubmit" type="submit" class="accountLogin_button">Sign In</button>--%>
                      <asp:Button ID="btnLogin" data-e2e="loginSubmit"  runat="server" class="accountLogin_button btn-login" Text="Sign In" OnClick="btnLogin_Click" />
                  <a href="/forgotpassword.aspx" class="forget-pass">  For Forgotten your password?</a>
				      <!-- Social-login-Component -->
                        <span class="socialProviderButtons_headerTextLine">
                     <%--<p class="socialProviderButtons_header">
                        <span class="socialProviderButtons_headerText">
                        Or, Continue with
                    
                        </span>
                     </p>--%>
                     <%--<ul class="socialProviderButtons" data-component="socialProviderButtons">
                        <style>
                           .socialProviderButtons_providerButton-facebook {
                           background-color: #3b5998;
                           }
                           .socialProviderButtons_providerButton-facebook:hover {
                           background-color: #5472b1;
                           }
                           .socialProviderButtons_providerButton-facebook .socialProviderButtons_providerButtonTitle {
                           color: #ffffff;
                           }
                           .socialProviderButtons_providerButton-google {
                           background-color: #ffffff;
                           }
                           .socialProviderButtons_providerButton-google:hover {
                           background-color: #eeeeee;
                           }
                           .socialProviderButtons_providerButton-google .socialProviderButtons_providerButtonTitle {
                           color: #000000;
                           }
                        </style>
                        <li>
                           <a href="#" class="socialProviderButtons_providerButton socialProviderButtons_providerButton-facebook" data-js-element="button" data-component-tracked-clicked="" data-component-tracked-viewed="" data-social-login-type="facebook" aria-label="Continue with Facebook">
                              <div class="socialProviderButtons_providerButtonInner">
                                 <div class="socialProviderButtons_providerButtonLogo">
                                    <img src="data:image/svg+xml;base64,PHN2ZyBjbGFzcz0ic29jaWFsTG9naW5fc3ZnSWNvbi1mYWNlYm9vayIgd2lkdGg9IjIwIiBoZWlnaHQ9IjIwIiB2aWV3Qm94PSIwIDAgMjkgMjkiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTSAxNy45OTYsMzJMIDEyLDMyIEwgMTIsMTYgbC00LDAgbDAtNS41MTQgbCA0LTAuMDAybC0wLjAwNi0zLjI0OEMgMTEuOTkzLDIuNzM3LCAxMy4yMTMsMCwgMTguNTEyLDBsIDQuNDEyLDAgbDAsNS41MTUgbC0yLjc1NywwIGMtMi4wNjMsMC0yLjE2MywwLjc3LTIuMTYzLDIuMjA5bC0wLjAwOCwyLjc2bCA0Ljk1OSwwIGwtMC41ODUsNS41MTRMIDE4LDE2TCAxNy45OTYsMzJ6IiBmaWxsPSJ3aGl0ZSI+PC9wYXRoPjwvc3ZnPg==" class="socialProviderButtons_providerButtonLogoImage" alt="" role="presentation">
                                 </div>
                                 <span class="socialProviderButtons_providerButtonTitle">
                                 Facebook
                                 </span>
                              </div>
                           </a>
                        </li>
                        <li>
                           <a href="#" class="socialProviderButtons_providerButton socialProviderButtons_providerButton-google" data-js-element="button" data-component-tracked-clicked="" data-component-tracked-viewed="" data-social-login-type="google" aria-label="Continue with Google">
                              <div class="socialProviderButtons_providerButtonInner">
                                 <div class="socialProviderButtons_providerButtonLogo">
                                    <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4bWxuczpza2V0Y2g9Imh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaC9ucyIgdmlld0JveD0iMCAwIDE4IDE4IiB2ZXJzaW9uPSIxLjEiPgogIDxnIGlkPSJHb29nbGUtQnV0dG9uIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4KICAgIDxwYXRoIGQ9Ik0xNy42NCw5LjIwNDU0NTQ1IEMxNy42NCw4LjU2NjM2MzY0IDE3LjU4MjcyNzMsNy45NTI3MjcyNyAxNy40NzYzNjM2LDcuMzYzNjM2MzYgTDksNy4zNjM2MzYzNiBMOSwxMC44NDUgTDEzLjg0MzYzNjQsMTAuODQ1IEMxMy42MzUsMTEuOTcgMTMuMDAwOTA5MSwxMi45MjMxODE4IDEyLjA0NzcyNzMsMTMuNTYxMzYzNiBMMTIuMDQ3NzI3MywxNS44MTk1NDU1IEwxNC45NTYzNjM2LDE1LjgxOTU0NTUgQzE2LjY1ODE4MTgsMTQuMjUyNzI3MyAxNy42NCwxMS45NDU0NTQ1IDE3LjY0LDkuMjA0NTQ1NDUgTDE3LjY0LDkuMjA0NTQ1NDUgWiIgZmlsbD0iIzQyODVGNCIvPgogICAgPHBhdGggZD0iTTksMTggQzExLjQzLDE4IDEzLjQ2NzI3MjcsMTcuMTk0MDkwOSAxNC45NTYzNjM2LDE1LjgxOTU0NTUgTDEyLjA0NzcyNzMsMTMuNTYxMzYzNiBDMTEuMjQxODE4MiwxNC4xMDEzNjM2IDEwLjIxMDkwOTEsMTQuNDIwNDU0NSA5LDE0LjQyMDQ1NDUgQzYuNjU1OTA5MDksMTQuNDIwNDU0NSA0LjY3MTgxODE4LDEyLjgzNzI3MjcgMy45NjQwOTA5MSwxMC43MSBMMC45NTcyNzI3MjcsMTAuNzEgTDAuOTU3MjcyNzI3LDEzLjA0MTgxODIgQzIuNDM4MTgxODIsMTUuOTgzMTgxOCA1LjQ4MTgxODE4LDE4IDksMTggTDksMTggWiIgZmlsbD0iIzM0QTg1MyIvPgogICAgPHBhdGggZD0iTTMuOTY0MDkwOTEsMTAuNzEgQzMuNzg0MDkwOTEsMTAuMTcgMy42ODE4MTgxOCw5LjU5MzE4MTgyIDMuNjgxODE4MTgsOSBDMy42ODE4MTgxOCw4LjQwNjgxODE4IDMuNzg0MDkwOTEsNy44MyAzLjk2NDA5MDkxLDcuMjkgTDMuOTY0MDkwOTEsNC45NTgxODE4MiBMMC45NTcyNzI3MjcsNC45NTgxODE4MiBDMC4zNDc3MjcyNzMsNi4xNzMxODE4MiAwLDcuNTQ3NzI3MjcgMCw5IEMwLDEwLjQ1MjI3MjcgMC4zNDc3MjcyNzMsMTEuODI2ODE4MiAwLjk1NzI3MjcyNywxMy4wNDE4MTgyIEwzLjk2NDA5MDkxLDEwLjcxIEwzLjk2NDA5MDkxLDEwLjcxIFoiIGZpbGw9IiNGQkJDMDUiLz4KICAgIDxwYXRoIGQ9Ik05LDMuNTc5NTQ1NDUgQzEwLjMyMTM2MzYsMy41Nzk1NDU0NSAxMS41MDc3MjczLDQuMDMzNjM2MzYgMTIuNDQwNDU0NSw0LjkyNTQ1NDU1IEwxNS4wMjE4MTgyLDIuMzQ0MDkwOTEgQzEzLjQ2MzE4MTgsMC44OTE4MTgxODIgMTEuNDI1OTA5MSwwIDksMCBDNS40ODE4MTgxOCwwIDIuNDM4MTgxODIsMi4wMTY4MTgxOCAwLjk1NzI3MjcyNyw0Ljk1ODE4MTgyIEwzLjk2NDA5MDkxLDcuMjkgQzQuNjcxODE4MTgsNS4xNjI3MjcyNyA2LjY1NTkwOTA5LDMuNTc5NTQ1NDUgOSwzLjU3OTU0NTQ1IEw5LDMuNTc5NTQ1NDUgWiIgZmlsbD0iI0VBNDMzNSIvPgogIDwvZz4KPC9zdmc+" class="socialProviderButtons_providerButtonLogoImage" alt="" role="presentation">
                                 </div>
                                 <span class="socialProviderButtons_providerButtonTitle">
                                 Google
                                 </span>
                              </div>
                           </a>
                        </li>
                     </ul>--%>
               
               </div>
            </div>
            <div class="accountLogin_card">
               <div class="accountLogin_cardContent">
                  <h2 class="accountLogin_title">New Customers</h2>
                  <p></p>
                
                   
                     <a href="/accountcreate.aspx" class="accountLogin_newAccountButton new-user-continue">Continue</a>
               
               </div>
            </div>
         </div>
      </main>
   </div>
		<script>
		$(document).ready(function () {
			var pass_val = 0;
			$(".showpass").click(function () {
				
				if (pass_val == 0) {
					$("#password").attr("type", "text");
					pass_val = 1;
				}
				else {
					$("#password").attr("type", "password");
					pass_val = 0;
				}

			});
		});
	</script>
    </span>
</asp:Content>